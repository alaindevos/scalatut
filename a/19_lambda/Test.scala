// Pattern matching behaves like switch-case
@main def test()=main()
def main()=

  def sum1(a:Int,b:Int):Int=a+b
  end sum1

  def sum2:(Int,Int)=>Int=(a,b)=>a+b
  end sum2

  def sum3:(Int,Int)=>Int= _ + _
  end sum3

  val ints=List(1,2,3)
  val d1=ints.map(_ * 2)
  val d2=ints.map( (i:Int)=> i*2)
  val d3=for(i<-ints) yield {i*2}
  val d4=ints.filter(_>2)
  def doubleme(i:Int)={i*2}
  val d5=ints.map(doubleme)


end main
