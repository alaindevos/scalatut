// case class, compiler creates code, use for pattern matching
@main def test()=main()
def main()=

  case class Person(name:String,age:Int)
  end Person

  object Person:
    def unapply(p:Person):Some[(String,Int)]=Some((p.name,p.age))
  end Person

  def getname(p:Person):String=p match
    case Person("Alain",_) => "Noname"
    case Person(name,age) => name

  val p:Person=Person("Alain",20)
  val a:Int=p.age
  val p2:Person=p.copy()

  if p.hashCode==p2.hashCode then
    println("Same content")
  else
    ()
  end if

  if p.equals(p) then
    print("Identical")
  else
    ()
  end if

  val p4:Person=Person("Alain",20)
  val p5:Person=Person.apply("Alain",20)
  val p6:Person=Person("Alain",20)
  val p7:Person=Person(age=20,name="Alain")
  val p8:Person=p7.copy()

  val unap:Some[(String,Int)]=Person.unapply(p7)
  val (aname:String,anage:Int)=unap getOrElse ("",0)

  case object USD
  val so:String=USD.toString()


end main
