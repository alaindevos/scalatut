@main def test()=main()
def main()=
  // A comment
  /*
   * A comment
  */
  val b1:Boolean=true
  val b:Byte=1
  val s1:Short=1
  val x:Int=1
  val l:Long=1
  val f:Float=1.0
  val d:Double=1.0
  val cx:Char='a'
  val s7:String="Alain"
  val bi=BigInt(123456)
  val bd=BigDecimal(123.456)

  var s="Hello"
  val ca=s.toCharArray
  val s2=1.toString
  val greeting:String=null
  s=s.toUpperCase
  printf("Hello World: %s \n",s)
  val c1:Char="Hello"(4)
  val c2:Char="Hello".apply(4)
  var i:Int=1+2
  i=1.+(3)
  for(c<-"Hello") println(c)
  end for
  val s3:String= s"AAA $s $s2 ZZZ"  //String interpolation
  val fl:Float=42.12
  val s4:String= f" $fl%2.2f"   //formatted string
  val s5:String= raw"Hello World \n Hello World"   //raw , i.e. not escaped

  val fn="Alain"
  val ln="De Vos"
  val full=fn+ln
  val full2=s"$fn $ln"
end main
