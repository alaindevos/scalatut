@main def test()=main()
def main()=

  def myisdigit:(Char=>Boolean)= x => x.isDigit
  end myisdigit

  def countit(s:String, predicate:(Char=>Boolean)):Int=s.count(predicate)
  end countit

  def countdigits(s:String):Int=countit(s,myisdigit)
  end countdigits

  val i:Int=countdigits("123viervijf")

end main
