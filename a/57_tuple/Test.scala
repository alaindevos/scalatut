import scala.collection.immutable.SortedMap
@main
def test()=

  val t1:Tuple3[Int,Double,java.lang.String]=(1,3.14,"Fred")
  val t2:(Int,Double,java.lang.String)=(1,3.14,"Fred")
  val t3:Tuple2[String,Int]=("John",22)
  val t4=t3 ++ t3
  val t5=1 *: "a" *: 2.2 *:EmptyTuple

  class Person(var name:String)
  val t=(11,"Eleven",new Person("Eleven"))
  println(t._1)
  println(t._2)
  println(t._3.name)
  val (anum,astr,aperson)=t

  case class Car(owner:String,model:String,price:Int)

  def getModelPrice(c:Car):(String,Int)=
    (c.model,c.price)
  end getModelPrice

  val c=Car("Alain","Audi",1234)
  val (model1,price1)=getModelPrice(c) //Value decomposition
  val tx=Tuple.fromProductTyped(c)
  val (model2,price2)= tx match
   case (o,m,p) => (m,p)
  val (model3:String,price3:Int)=(tx.productElement(1),tx.productElement(2))
  val (_, m:String, p:Int)=tx  //Value decomposition

  val a:(String,Int)="Alain"->20

  case class Person2(name:String,age:Int)

  object Person2:
    def unapply(p:Person2):Option[(String,Int)]=
      Some(p.name,p.age)
    end unapply
  end Person2

  def getName(p:Person2):String=p match
    case Person2(name,age) => name   //Uses unapply
  end getName

  val li:List[(Int,Int)]=List((1->2),(2->3),(3->4))
  val l2:List[Int]=li.map(e=>e._1+e._2)  //Unpack with positional accessors



end test
