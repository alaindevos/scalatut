@main
def test()=
  abstract class aCounter:
    var value:Int
  end aCounter

  class Counter extends aCounter:
    var value:Int=0
    var mypublic:Int=0  // Accessible everywhere
    protected var myprotected:Int=0 //Also accessible in subclass
    private var myprivate:Int=0 // Only accessible in class

    def addone()={value+=1;()}
    def getval()=value
    def setval(value:Int)={this.value=value;()}

    def this(aval:Int)=
      this()
      this.value=aval
    end this

  end Counter

  class Counter2 extends Counter:
    var xxx:Int=0
  end Counter2

  var c:Counter=new Counter()
  c.setval(5)
  c.addone()
  val i:Int=c.getval()
  c.mypublic=7;
  val i2:Int=c.mypublic
  c=new Counter(7)

  class Name1(var name:String):  //define field & constructor
    def printname()=
      println(name)
    end printname
  end Name1

  val n:Name1=Name1("Alain")
  println(n.name)
  n.name="Eddy"
  println(n.name)

  class Name2(s:String):   //Constructor
    println("Begin constructor")
    var name:String=s
    def printname={println(name)}
    println("End constructor")
  end Name2

  val n2:Name2=Name2("Alain")
  println(n2.name)
  n2.name="Eddy"
  println(n.name)


  class Name3:
    var _name:String=""
    def this(name:String)={ //Auxiliary constructor
      this()
      this._name=name
    }

    def name:String="My name is:"+_name
    def name_=(aName:String):Unit={_name="Set the name to "+aName}  // _= refers to assignment operator
    def printname={println(_name)}
  end Name3

  var n3:Name3=Name3("Alain")
  println(n3.name)
  n3.name="Eddy"    // assignment operator
  println(n3.name)
  n3._name="Jan"
  println(n3.name)

end test
