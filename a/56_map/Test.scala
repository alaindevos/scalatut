// map: immutable key-value structure
import scala.collection.immutable.SortedMap

@main
def test()=
  val mx1=Map()   // Empty map
  val mx2=Map("Alain"->20,"Eddy"->30)
  val mx3=mx2 + ("Jan"->40)
  val mx4=mx3 - "Jan"
  val mx5=mx2 ++ mx3 //Merge
  val si1:Option[Int]=mx3.get("Alain")
  val s1=mx3.getOrElse("Alain",-1)
  val x:Iterable[String]=mx3.keys
  val y:Iterable[Int]=mx3.values
  val i:Int=mx3.size
  mx5.foreach{
    _ match
      case (k,v) =>
        println(k+" "+v.toString)
  }
  println("Hi")
  val score1:Map[String,Int]=Map("Alain"->10,"Eddy"->20) //Immutable HashTable
  import scala.collection.mutable.Map
  val score2=Map("Alain"->10,"Eddy"->20) //Mutable
  import scala.collection.mutable.HashMap
  val score3=new HashMap[String,Int]
  val score4=Map(("Alain",10) , ("Eddy",20))
  val s:Int=score1("Alain")
  if score1.contains("Alain") then
    print("OK")
  else
    ()
  end if
  val myset1:Set[String]=score1.keySet
  val myset2:Iterable[Int]=score1.values
  val scores5=SortedMap("Alain"->10,"Eddy"->20)   // Immutable TreeMap
  val name:Array[String]=Array("Alain","Eddy")
  val age:Array[Int]=Array(10,20)
  val z:Array[(String,Int)]=name.zip(age)
  val m1:Map[String,Int]=Map()  //Empty map
  val l1=List(("Alain",20),("Eddy",30))
  val ma=l1.toMap

  import collection.mutable.{Map=>MMap}
  val ma2:MMap[String,Int]=ma.to(collection.mutable.Map)
  val i3:Option[Int]=ma.get("Alain")
  val m2:MMap[String,Int]=ma2++ (Map("Piet"->40)).to(collection.mutable.Map)
  var m3:MMap[String,Int]=ma2 ++ ma2
  m3("Piet")=50
  import scala.collection.mutable.Iterable
  val li1:Iterable[Int]=ma2.map { case (key,value) => value }

  val ages=Map("Alain"->20,"Eddy"->30)
  for((name,age)<-ages)println(s"$name $age")
  ages.foreach{
    case(name,age)=>println(s"$name $age")
  }

end test
