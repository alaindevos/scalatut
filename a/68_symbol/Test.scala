@main def test()=main()
def main()=
  val salain:Symbol=Symbol("Alain")
  val seddy:Symbol=Symbol("Eddy")

  def getname(s:Symbol):String= s match
    case Symbol("Alain") => "Alain"
    case Symbol("Eddy") => "Eddy"
    case null   => ""

  val t:String=getname(salain)

end main
