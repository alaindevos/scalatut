// Pattern matching behaves like switch-case
@main def test()=main()
def main()=

  def weekendday(n:Int):String=n match
    case 6=>"Saterday"
    case 7=>"Sunday"
    case _=>"Unknown"
  end weekendday

  def showinfo(p:Any):String= p match
    case n:Int => "Int "+n.toString
    case s:String => "String "+s
  end showinfo

  sealed trait Currency
  object USD extends Currency
  object EUR extends Currency

  def exchangerate(c:Currency):Double=
    c match
      case USD => 1.0
      case EUR => 1.5
  end exchangerate

end main
