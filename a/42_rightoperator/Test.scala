@main def test()=main()
def main()=
  class Printer:
    def >>(i:Int)= println(s"$i")
    def >>:(i:Int)=println(s"$i")
  end Printer
  val p=Printer()
  p.>>(42)
  p.>>:(42)
  p >> 42
  42 >>: p   //Right associative
end main
