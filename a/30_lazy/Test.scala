@main def test()=main()
def main()=
  var res=0
  import scala.annotation._
  @tailrec
  def sumit(i: => Int):Unit=
    if i<1 then
      ()
    else
      res=res+i
      sumit(i-1)
  end sumit
  sumit(10)
  println(res)

  lazy val c:String= {println("Hallo1");"Hallo2"}
  println(c)

  import java.util.Calendar
  import java.util.Date
  lazy val lazyTime:Date=Calendar.getInstance.getTime
  val eagerTime:Date=Calendar.getInstance.getTime
  Thread.sleep(2000)
  println(lazyTime)
  println(eagerTime)
end main
