
@main
def test()=
  println("Hi")

  def sqrtOption(n:Int):Option[Double]=
    if n>=0 then
      Some(Math.sqrt(n))
    else
      Option.empty[Double]
    end if
  end sqrtOption

  val a=sqrtOption(-4)
  val b=sqrtOption(+4)

  def sqrtOrZero(n:Int):Double=
    sqrtOption(n).getOrElse(default=0.0D)
  end sqrtOrZero

  println(sqrtOrZero(-4))

  println(classOf[Option[Int]])  //class scala.Option

  sealed trait OptionInt
  case class SomeInt(value:Int) extends OptionInt
  case object MyNone extends OptionInt

  val opInt2:OptionInt=SomeInt(1)
  val opInt3:OptionInt=MyNone
end test
