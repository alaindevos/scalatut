@main
def test()=main()
def main()=
  import scala.concurrent.Future
  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.util.{Failure,Success}
  import scala.annotation._

  var sum:Int=0
  def count(n:Int):Int=
    Thread.sleep(500)
    println(n)
    if n>5 then ()
    else {
          sum=sum+n
          count(n+1)
    }
    sum
  end count


  println("Start")
  val f:Future[Int]=Future.apply(count(0))
  var complete:Boolean=false
  while !complete do
    Thread.sleep(100)
    complete=f.isCompleted
  end while
  println("Stop")
  println(f)
  f.onComplete{
    case Success(v)=>println(s"$v")
    case Failure(e) => println("")
  }
end main
