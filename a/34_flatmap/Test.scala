@main def test()=main()
def main()=
  val m=Map(1->2,2->3,3->4)
  val p=m.flatMap{
    case (a,b)=>
      if (a>1)&(b>1)
        then Some(a*b)
        else None
      end if
  }
end main
