@main def test()=main()
def main()=

  enum WeekendSimple:
    case SatS
    case SunS
  end WeekendSimple

  enum Weekend(val name:String):
    case Sat extends Weekend("Saterday")
    case Sun extends Weekend("Sunday")
    def isSunday:(()=>Boolean)=
      () => ( this == Sun)
    end isSunday
  end Weekend

  import WeekendSimple.*
  import Weekend.*

  val v1:Array[Weekend]=Weekend.values
  val v2:Array[Weekend]=v1.sortBy(_.ordinal)
  val x:Seq[Weekend]=v2.toSeq
  val s:String=Sun.name
  val i:Int=Sun.ordinal

  val b:Boolean=Sun.isSunday()
  val w:Weekend=Weekend.valueOf("Sat")

end main
