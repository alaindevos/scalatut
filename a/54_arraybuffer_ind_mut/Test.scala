// Array : collection of mutable indexed elements
@main def test()=main()
def main()=
    import scala.collection.mutable.ArrayBuffer
    import scala.util.Sorting.quickSort
    //Fixed length
    val a1=Array[Int](20)
    //Variable length
    val a2=ArrayBuffer[Int]()
    a2 += 1
    a2 += 2
    a2 += 3
    a2.remove(1)
    a2.dropRightInPlace(1)
    a2.insert(1,7)
    val a3:Array[Int]=a2.toArray
    for(i<-0 until a2.length)
      println(a2(i))
    end for
    val a4 =Array(1,2,3,4,5,6,7,8,9,10)
    val a5=for(elem<-a4) yield {(2*elem)}
    for(i<-0 until a5.length)
      print(a5(i))
    end for
    for(elem <- a5)print(elem)
    val a6=a5.filter(_ % 2 == 0)
    val a7:Array[Int]=a6.map(5*_)
    val i1=a6.max
    val i2=a6.sum
    quickSort(a7)
    val s1=a7.mkString(" ")
    val s2=a7.toString
    //2-dim
    val m1=Array.ofDim[Int](3,4)
    m1(0)(0)=4
    val triangle=new Array[Array[Int]](10)
    for(i<-0 until triangle.length)
      triangle(i)=Array[Int](i+1)
    end for
    triangle(0)(0)=5
    val ia=IArray(1,2,3,4,5) // Immutable elements
    import scala.util.Sorting.quickSort
    val a9=quickSort(Array(4,3,2,1))

    var ab=ArrayBuffer(1,2,3)
    ab.mapInPlace (_ * 2)
    ab=ab ++ List(4,5)
    ab=ab ++ Vector(5,6)
    ab.prepend(0)
    ab.update(0,123)
    ab(1)=456
    ab.remove(0)
    ab.remove(1,3)

    val ar1=Array[Int]()
    val ar2=ar1 :+ 1
    val ar3=ar2 ++ Seq(2,3)
    val ar4=0 +: ar3
    val ar5=(1 to 5).toArray
    ar5(0)=123
    println(ar5(0))

end main
