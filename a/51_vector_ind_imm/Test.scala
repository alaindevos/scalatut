// Vector : fixed-size immutable indexed sequence
@main def test()=main()
def main()=
  val v:Vector[Int]=Vector(1,2,3,4)
  val v8=v :+ 5
  val v9=v ++ Vector(5,6)
  val i:Int=v(0)
  val v2=v.updated(2,10)
  val v3=v.filter( _ > 2)
  val v4=v.map (_ * 10)
  val v5=(1 to 100).toVector
  val sv1:Vector[String]=Vector[String]()
  val sv2:Vector[String]=sv1 ++ List("Alain","Eddy")
  val sv3:Vector[String]=sv2 ++ Seq("Jan","Piet")
end main
