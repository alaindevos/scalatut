// Identify by name or id
case class Name(name: String)
case class Id(id: Int)
type NameOrId = Name | Id

@main def test()=main()
def main()=
  def convert(id: NameOrId):String =
    id match
      case Name(name) => name
      case Id(id)   => id.toString
  end convert
  val x:NameOrId=Id(5)
  val y:NameOrId=Name("Fred")
  println("Id")
  println(convert(x))
  println("Name")
  println(convert(y))
end main
