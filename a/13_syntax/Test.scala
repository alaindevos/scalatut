@main def test()=main()
def main()=
  print("Hello")

  var i:Int=1
  if i>0 then
    println("World")
  else
    ()
  end if

  while i<10 do
    i+=1
  end while

  for j:Int<-0 to 10 do
    print(j)
  end for

  val str=Seq("Alain","Eddy")
          .map(_.toLowerCase)
          .mkString("[",",","]")
  print(str)

  val seq:Seq[String]=Seq("Alain","Eddy")      //Elements immutable
  val arr:Array[String]=Array("Alain","Eddy")  //Elements mutable
  arr(1)="Piet"

  val r1=1 to 10
  val r2=1 until 10 // not 10

  import scala.annotation.tailrec
  def countTo(n:Int):Unit=
    @tailrec
    def count(i:Int):Unit=
      if(i>n) then
        ()
      else
          println(i);
          count(i+1)
    end count
    count(1)
  end countTo
  println("Start counting")
  countTo(5)
  println("End counting")
end main
