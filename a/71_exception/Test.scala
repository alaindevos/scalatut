@main def test()=main()
def main()=

  try {
  throw Exception("My execption")
  }
  catch {
    case ex:Exception =>
      // ex.printStackTrace()
      println("Exception 1")
  }
  finally{
    print("Hallo")
  }


  class MyException(s:String) extends Exception(s)
  end MyException

  try {
  throw MyException("My execption")
  }
  catch {
    case ex:MyException =>
      // ex.printStackTrace()
      println("Exception 2")
  }
  finally{
    print("Hallo")
  }
end main
