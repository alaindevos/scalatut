// Mutable
@main def test()=main()
def main()=
  import scala.collection.mutable.ListBuffer
  val x=ListBuffer(1,2,3,4)
  x = x - 4 //Remove element
  x = x -- Seq(1,2) // Remove multiple elements
  x = x ++ Seq(1,2,3,4)
  val a:ListBuffer=x.remove(0) // Remove first element
end main
