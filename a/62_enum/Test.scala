// Object , only one, static, created at first use

object Color extends Enumeration:
  type Color=Value // Needed Alias
  val Red=Value
  val Yellow=Value
  val Green=Value
end Color

enum Color2:
  case Red2,Yellow2,Green2
end Color2

enum Color3(val code:String):
  case Red3 extends Color3("red")
  case Yellow3 extends Color3("Yellow")
  case Green3 extends Color3("Green")
end Color3

@main def test()=main()
def main()=
  import Color._
  val r:Color=Red
  val y:Color=Yellow
  import Color2._
  val c1:Color2=Red2
  val ar:Array[Color2]=Array(Red2,Yellow2,Green2)
  val c2:Color2=Color2.fromOrdinal(0)
end main
