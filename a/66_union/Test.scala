sealed trait IntOrStringOrBool[T]
object IntOrStringOrBool:
  implicit val intInstance: IntOrStringOrBool[Int] =
    new IntOrStringOrBool[Int] {}
  implicit val strInstance: IntOrStringOrBool[String] =
    new IntOrStringOrBool[String] {}
  implicit val boolInstance: IntOrStringOrBool[Boolean] =
    new IntOrStringOrBool[Boolean] {}
end IntOrStringOrBool

@main def test()=main()
def main()=

  def isIntOrStringOrBool[T: IntOrStringOrBool](t: T): String = t match
    case i: Int => "%d is an Integer".format(i)
    case s: String => "%s is a String".format(s)
    case b: Boolean => "%b a Boolean".format(b)
  end isIntOrStringOrBool

  print(isIntOrStringOrBool(10)) // prints "10 is an Integer"
  print(isIntOrStringOrBool("hello")) // prints "hello is a String"
  print(isIntOrStringOrBool(true)) // prints "true is a Boolean"

end main
