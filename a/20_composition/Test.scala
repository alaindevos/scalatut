// Pattern matching behaves like switch-case
@main def test()=main()
def main()=

  def mylen:(String=>Int)= s  => s.size
  end mylen

  def mybig2:(Int=>Boolean)= d => d > 2
  end mybig2

  def comp1:(String=>Boolean)= s => mybig2(mylen(s))
  end comp1

  def comp2:(String=>Boolean) = s => mylen.andThen(mybig2)(s)
  end comp2

end main
