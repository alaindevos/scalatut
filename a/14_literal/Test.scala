@main def test()=main()
def main()=
  val i:Int=123
  val x:Long=123L
  val f:Float=123_456.789
  val d:Double=123_456.789
  val y:BigInt=123_456
  val z:BigDecimal=123_456.789
  val b1:Boolean=true
  val b0:Boolean=false
  val c:Char='\n'
  val s:String="Alain"

  val f1:(Int,Double)=>String =
    (i,d)=>(i+d).toString
  end f1

  val tup:(String,Int)=("Hello",1)
  val (ss:String,ii:Int)=tup

  val pair:(String,Int)="Hello"->1
end main
