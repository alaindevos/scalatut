object SealedADT:
  sealed trait IntList2
  final case class MyCons2(elem:Int,right:IntList2) extends IntList2
  final case class MyNil2(dummy:Unit) extends IntList2
end SealedADT

object EnumADT:
  enum IntList:
    case MyCons(elem:Int,right:IntList)
    case MyNil(dummy:Unit)
  end IntList
end EnumADT

@main def test()=main()
def main()=
  import EnumADT.IntList.*
  import EnumADT.IntList
  val t=MyCons(1,MyCons(2,MyNil(())))

  def sum(lst:IntList):Int= lst match {
    case MyNil(()) => 0
    case MyCons(elem,right)=>elem+sum(right)
  }


  import SealedADT.*
  val t2=MyCons2(1,MyCons2(2,MyNil2(())))

end main
