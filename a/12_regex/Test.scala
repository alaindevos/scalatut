@main def test()=main()
def main()=
  import scala.util.matching.Regex
  import scala.util.matching.Regex.MatchIterator
  val pat:Regex="""[0-9]+""".r() // """:raw string
  val s:String="99 98 97"
  val it1:MatchIterator=pat.findAllIn(s)
  for(f<-it1)
    print(f)
  end for
  val it2:MatchIterator=pat.findAllIn(s)
  val ar:Array[String]=it2.toArray
  val m1:Option[String]=pat.findFirstIn(s)
  val m2:String=m1.getOrElse("")
  print(m2)
  val s2:String=pat.replaceAllIn(s,"XX")
  print(s2)
end main
