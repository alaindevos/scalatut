val fibs: LazyList[BigInt] =
  BigInt(0) #:: BigInt(1) #::
    fibs.zip(fibs.tail).map{ n =>
      println(s"Adding ${n._1} and ${n._2}")
      n._1 + n._2
    }

@main def test()=main()
def main()=

  fibs.take(5).foreach(println)
  fibs.take(6).foreach(println)

  val xs=(1 to 100).to(LazyList)
  println(xs(5))
end main
