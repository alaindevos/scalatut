@main def test()=main()
def main()=
  trait List[T] {
    def isEmpty: Boolean
    def head:T
    def tail:List[T]
  }
end main
