//FIFO:Queue
@main
def test()=
  import scala.collection.mutable.Queue
  val q1=Queue(1,2,3)
  q1 +=4
  q1 ++=q1
  //Take an element from the head
  println(q1.dequeue)
end test
