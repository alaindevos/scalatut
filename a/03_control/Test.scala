@main def test()=main()
def main()=

  val i1= if 1>0 then 1 else -1
  val i2= if 1>0 then 1
          else if 1==0 then 0
          else -1
  val i3={ println("b") ; 1 }

  print("a")
  println("b")
  printf("%s","c\n")

  var r=1
  var n=5
  while n>0 do
    r=r*n
    n=n-1
  end while

  r=1
  n=5
  for(i<-1 to n)
    r=r*i
  end for

  for(c<-"Hello") println(c)

  val s="Hello"
  for(i<-0 until s.length) // no include of upperbound : s.length
    printf("%c",s(i))
  end for

  //Construct collection/vector
  var x= for(i<-1 to 10) yield i

  def pluseen(i:Int):Int={i+1}
  def fac(n:Int):Int=
    if n<1 then
      1
    else
      0
    end if
  end fac

  //default and named argument
  def myfun(x:Int , y:Int=5)={x+y}
  val i=myfun(x=7);

  //Procedure
  def proc(s:String):Unit={println(s)};

  def int2string(n:Int):String=
    if n==0
    then "neutral"
    else  if n<0
    then "negative"
    else "positive"
    end if


end main
