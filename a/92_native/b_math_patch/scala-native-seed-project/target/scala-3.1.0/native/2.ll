declare i32 @llvm.eh.typeid.for(i8*)
declare i32 @__gxx_personality_v0(...)
declare i8* @__cxa_begin_catch(i8*)
declare void @__cxa_end_catch()
@_ZTIN11scalanative16ExceptionWrapperE = external constant { i8*, i8*, i8* }

@"_SM7__constG1-0" = private unnamed_addr constant { i8*, i32, i32, [13 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 13, i32 0, [13 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 77, i16 97, i16 112 ] }
@"_SM7__constG1-1" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [13 x i16] }* @"_SM7__constG1-0" to i8*), i32 0, i32 13, i32 -1383349348 }
@"_SM7__constG1-2" = private unnamed_addr constant { i8*, i32, i32, [14 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 14, i32 0, [14 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 105, i16 111, i16 46, i16 82, i16 101, i16 97, i16 100, i16 101, i16 114 ] }
@"_SM7__constG1-3" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [14 x i16] }* @"_SM7__constG1-2" to i8*), i32 0, i32 14, i32 -1359732257 }
@"_SM7__constG1-4" = private unnamed_addr constant [2 x i64] [ i64 0, i64 -1 ]
@"_SM7__constG1-5" = private unnamed_addr constant { i8*, i32, i32, [14 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 14, i32 0, [14 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 105, i16 111, i16 46, i16 87, i16 114, i16 105, i16 116, i16 101, i16 114 ] }
@"_SM7__constG1-6" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [14 x i16] }* @"_SM7__constG1-5" to i8*), i32 0, i32 14, i32 -1204327025 }
@"_SM7__constG1-7" = private unnamed_addr constant { i8*, i32, i32, [14 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 14, i32 0, [14 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 80, i16 114, i16 111, i16 100, i16 117, i16 99, i16 116, i16 50 ] }
@"_SM7__constG1-8" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [14 x i16] }* @"_SM7__constG1-7" to i8*), i32 0, i32 14, i32 1782459627 }
@"_SM7__constG1-9" = private unnamed_addr constant { i8*, i32, i32, [15 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 15, i32 0, [15 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 66, i16 117, i16 102, i16 102, i16 101, i16 114 ] }
@"_SM7__constG2-10" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [15 x i16] }* @"_SM7__constG1-9" to i8*), i32 0, i32 15, i32 814868678 }
@"_SM7__constG2-11" = private unnamed_addr constant [1 x i64] [ i64 -1 ]
@"_SM7__constG2-12" = private unnamed_addr constant { i8*, i32, i32, [19 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 19, i32 0, [19 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 105, i16 111, i16 46, i16 73, i16 110, i16 112, i16 117, i16 116, i16 83, i16 116, i16 114, i16 101, i16 97, i16 109 ] }
@"_SM7__constG2-13" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [19 x i16] }* @"_SM7__constG2-12" to i8*), i32 0, i32 19, i32 833723470 }
@"_SM7__constG2-14" = private unnamed_addr constant { i8*, i32, i32, [19 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 19, i32 0, [19 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 71, i16 101, i16 110, i16 66, i16 117, i16 102, i16 102, i16 101, i16 114, i16 36 ] }
@"_SM7__constG2-15" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [19 x i16] }* @"_SM7__constG2-14" to i8*), i32 0, i32 19, i32 -1159085062 }
@"_SM7__constG2-16" = private unnamed_addr constant { i8*, i32, i32, [19 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 19, i32 0, [19 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 116, i16 101, i16 114 ] }
@"_SM7__constG2-17" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [19 x i16] }* @"_SM7__constG2-16" to i8*), i32 0, i32 19, i32 2122653450 }
@"_SM7__constG2-18" = private unnamed_addr constant [4 x i64] [ i64 0, i64 2, i64 3, i64 -1 ]
@"_SM7__constG2-19" = private unnamed_addr constant { i8*, i32, i32, [20 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 20, i32 0, [20 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 65, i16 112, i16 112, i16 101, i16 110, i16 100, i16 97, i16 98, i16 108, i16 101 ] }
@"_SM7__constG2-20" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [20 x i16] }* @"_SM7__constG2-19" to i8*), i32 0, i32 20, i32 1429132232 }
@"_SM7__constG2-21" = private unnamed_addr constant { i8*, i32, i32, [20 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 20, i32 0, [20 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 77, i16 97, i16 112 ] }
@"_SM7__constG2-22" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [20 x i16] }* @"_SM7__constG2-21" to i8*), i32 0, i32 20, i32 -911155308 }
@"_SM7__constG2-23" = private unnamed_addr constant { i8*, i32, i32, [21 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 21, i32 0, [21 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 84, i16 104, i16 114, i16 101, i16 97, i16 100, i16 76, i16 111, i16 99, i16 97, i16 108 ] }
@"_SM7__constG2-24" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [21 x i16] }* @"_SM7__constG2-23" to i8*), i32 0, i32 21, i32 856626605 }
@"_SM7__constG2-25" = private unnamed_addr constant [3 x i64] [ i64 0, i64 1, i64 -1 ]
@"_SM7__constG2-26" = private unnamed_addr constant { i8*, i32, i32, [21 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 21, i32 0, [21 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 112, i16 97, i16 99, i16 107, i16 97, i16 103, i16 101, i16 36, i16 66, i16 111, i16 120 ] }
@"_SM7__constG2-27" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [21 x i16] }* @"_SM7__constG2-26" to i8*), i32 0, i32 21, i32 1817524909 }
@"_SM7__constG2-28" = private unnamed_addr constant { i8*, i32, i32, [21 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 21, i32 0, [21 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 80, i16 97, i16 114, i16 116, i16 105, i16 97, i16 108, i16 70, i16 117, i16 110, i16 99, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG2-29" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [21 x i16] }* @"_SM7__constG2-28" to i8*), i32 0, i32 21, i32 286647217 }
@"_SM7__constG2-30" = private unnamed_addr constant { i8*, i32, i32, [21 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 21, i32 0, [21 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 86, i16 105, i16 101, i16 119 ] }
@"_SM7__constG2-31" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [21 x i16] }* @"_SM7__constG2-30" to i8*), i32 0, i32 21, i32 1819232109 }
@"_SM7__constG2-32" = private unnamed_addr constant { i8*, i32, i32, [23 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 23, i32 0, [23 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 83, i16 101, i16 113, i16 79, i16 112, i16 115 ] }
@"_SM7__constG2-33" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [23 x i16] }* @"_SM7__constG2-32" to i8*), i32 0, i32 23, i32 141108379 }
@"_SM7__constG2-34" = private unnamed_addr constant { i8*, i32, i32, [24 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 24, i32 0, [24 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 99, i16 104, i16 97, i16 114, i16 115, i16 101, i16 116, i16 46, i16 67, i16 104, i16 97, i16 114, i16 115, i16 101, i16 116 ] }
@"_SM7__constG2-35" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [24 x i16] }* @"_SM7__constG2-34" to i8*), i32 0, i32 24, i32 1479543012 }
@"_SM7__constG2-36" = private unnamed_addr constant { i8*, i32, i32, [24 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 24, i32 0, [24 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 83, i16 101, i16 113, i16 86, i16 105, i16 101, i16 119 ] }
@"_SM7__constG2-37" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [24 x i16] }* @"_SM7__constG2-36" to i8*), i32 0, i32 24, i32 79593948 }
@"_SM7__constG2-38" = private unnamed_addr constant { i8*, i32, i32, [25 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 25, i32 0, [25 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 83, i16 116, i16 114, i16 105, i16 110, i16 103, i16 67, i16 104, i16 97, i16 114, i16 66, i16 117, i16 102, i16 102, i16 101, i16 114 ] }
@"_SM7__constG2-39" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [25 x i16] }* @"_SM7__constG2-38" to i8*), i32 0, i32 25, i32 1233042221 }
@"_SM7__constG2-40" = private unnamed_addr constant [3 x i64] [ i64 3, i64 4, i64 -1 ]
@"_SM7__constG2-41" = private unnamed_addr constant { i8*, i32, i32, [25 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 25, i32 0, [25 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 101, i16 102, i16 108, i16 101, i16 99, i16 116, i16 46, i16 79, i16 112, i16 116, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116 ] }
@"_SM7__constG2-42" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [25 x i16] }* @"_SM7__constG2-41" to i8*), i32 0, i32 25, i32 -668135543 }
@"_SM7__constG2-43" = private unnamed_addr constant { i8*, i32, i32, [27 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 27, i32 0, [27 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 83, i16 116, i16 97, i16 99, i16 107, i16 84, i16 114, i16 97, i16 99, i16 101, i16 69, i16 108, i16 101, i16 109, i16 101, i16 110, i16 116 ] }
@"_SM7__constG2-44" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [27 x i16] }* @"_SM7__constG2-43" to i8*), i32 0, i32 27, i32 2006054347 }
@"_SM7__constG2-45" = private unnamed_addr constant [4 x i64] [ i64 1, i64 2, i64 3, i64 -1 ]
@"_SM7__constG2-46" = private unnamed_addr constant { i8*, i32, i32, [27 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 27, i32 0, [27 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 83, i16 121, i16 115, i16 116, i16 101, i16 109, i16 36, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 51 ] }
@"_SM7__constG2-47" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [27 x i16] }* @"_SM7__constG2-46" to i8*), i32 0, i32 27, i32 -276044297 }
@"_SM7__constG2-48" = private unnamed_addr constant { i8*, i32, i32, [27 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 27, i32 0, [27 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 83, i16 101, i16 113, i16 86, i16 105, i16 101, i16 119, i16 36, i16 73, i16 100 ] }
@"_SM7__constG2-49" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [27 x i16] }* @"_SM7__constG2-48" to i8*), i32 0, i32 27, i32 361394435 }
@"_SM7__constG2-50" = private unnamed_addr constant { i8*, i32, i32, [27 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 27, i32 0, [27 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 83, i16 116, i16 114, i16 105, i16 110, i16 103, i16 79, i16 112, i16 115, i16 36 ] }
@"_SM7__constG2-51" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [27 x i16] }* @"_SM7__constG2-50" to i8*), i32 0, i32 27, i32 588767787 }
@"_SM7__constG2-52" = private unnamed_addr constant { i8*, i32, i32, [28 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 28, i32 0, [28 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 83, i16 121, i16 115, i16 116, i16 101, i16 109, i16 36, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49, i16 53 ] }
@"_SM7__constG2-53" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [28 x i16] }* @"_SM7__constG2-52" to i8*), i32 0, i32 28, i32 32561376 }
@"_SM7__constG2-54" = private unnamed_addr constant { i8*, i32, i32, [28 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 28, i32 0, [28 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 77, i16 97, i16 112 ] }
@"_SM7__constG2-55" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [28 x i16] }* @"_SM7__constG2-54" to i8*), i32 0, i32 28, i32 -876845876 }
@"_SM7__constG2-56" = private unnamed_addr constant { i8*, i32, i32, [29 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 29, i32 0, [29 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 105, i16 111, i16 46, i16 80, i16 114, i16 105, i16 110, i16 116, i16 83, i16 116, i16 114, i16 101, i16 97, i16 109, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 53 ] }
@"_SM7__constG2-57" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [29 x i16] }* @"_SM7__constG2-56" to i8*), i32 0, i32 29, i32 263893737 }
@"_SM7__constG2-58" = private unnamed_addr constant { i8*, i32, i32, [29 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 29, i32 0, [29 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 65, i16 114, i16 105, i16 116, i16 104, i16 109, i16 101, i16 116, i16 105, i16 99, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG2-59" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [29 x i16] }* @"_SM7__constG2-58" to i8*), i32 0, i32 29, i32 -823400207 }
@"_SM7__constG2-60" = private unnamed_addr constant [4 x i64] [ i64 0, i64 1, i64 2, i64 -1 ]
@"_SM7__constG2-61" = private unnamed_addr constant { i8*, i32, i32, [29 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 29, i32 0, [29 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 108, i16 105, i16 98, i16 99, i16 46, i16 101, i16 114, i16 114, i16 110, i16 111, i16 36 ] }
@"_SM7__constG2-62" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [29 x i16] }* @"_SM7__constG2-61" to i8*), i32 0, i32 29, i32 680500277 }
@"_SM7__constG2-63" = private unnamed_addr constant { i8*, i32, i32, [30 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 30, i32 0, [30 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 78, i16 117, i16 108, i16 108, i16 80, i16 111, i16 105, i16 110, i16 116, i16 101, i16 114, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG2-64" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [30 x i16] }* @"_SM7__constG2-63" to i8*), i32 0, i32 30, i32 1879291277 }
@"_SM7__constG2-65" = private unnamed_addr constant { i8*, i32, i32, [30 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 30, i32 0, [30 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 109, i16 97, i16 116, i16 104, i16 46, i16 82, i16 111, i16 117, i16 110, i16 100, i16 105, i16 110, i16 103, i16 77, i16 111, i16 100, i16 101, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 52 ] }
@"_SM7__constG2-66" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [30 x i16] }* @"_SM7__constG2-65" to i8*), i32 0, i32 30, i32 -1496511571 }
@"_SM7__constG2-67" = private unnamed_addr constant [2 x i64] [ i64 1, i64 -1 ]
@"_SM7__constG2-68" = private unnamed_addr constant { i8*, i32, i32, [30 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 30, i32 0, [30 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 73, i16 116, i16 101, i16 114, i16 97, i16 98, i16 108, i16 101, i16 79, i16 110, i16 99, i16 101, i16 36 ] }
@"_SM7__constG2-69" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [30 x i16] }* @"_SM7__constG2-68" to i8*), i32 0, i32 30, i32 -389429079 }
@"_SM7__constG2-70" = private unnamed_addr constant { i8*, i32, i32, [31 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 31, i32 0, [31 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 99, i16 104, i16 97, i16 114, i16 115, i16 101, i16 116, i16 46, i16 67, i16 104, i16 97, i16 114, i16 115, i16 101, i16 116, i16 68, i16 101, i16 99, i16 111, i16 100, i16 101, i16 114 ] }
@"_SM7__constG2-71" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [31 x i16] }* @"_SM7__constG2-70" to i8*), i32 0, i32 31, i32 1592472256 }
@"_SM7__constG2-72" = private unnamed_addr constant [5 x i64] [ i64 1, i64 3, i64 4, i64 5, i64 -1 ]
@"_SM7__constG2-73" = private unnamed_addr constant { i8*, i32, i32, [31 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 31, i32 0, [31 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 99, i16 104, i16 97, i16 114, i16 115, i16 101, i16 116, i16 46, i16 67, i16 104, i16 97, i16 114, i16 115, i16 101, i16 116, i16 69, i16 110, i16 99, i16 111, i16 100, i16 101, i16 114 ] }
@"_SM7__constG2-74" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [31 x i16] }* @"_SM7__constG2-73" to i8*), i32 0, i32 31, i32 -1557329000 }
@"_SM7__constG2-75" = private unnamed_addr constant [5 x i64] [ i64 1, i64 2, i64 3, i64 5, i64 -1 ]
@"_SM7__constG2-76" = private unnamed_addr constant { i8*, i32, i32, [31 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 31, i32 0, [31 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 77, i16 97, i16 112, i16 36, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49 ] }
@"_SM7__constG2-77" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [31 x i16] }* @"_SM7__constG2-76" to i8*), i32 0, i32 31, i32 1071114820 }
@"_SM7__constG2-78" = private unnamed_addr constant { i8*, i32, i32, [31 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 31, i32 0, [31 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 77, i16 97, i16 112, i16 36 ] }
@"_SM7__constG2-79" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [31 x i16] }* @"_SM7__constG2-78" to i8*), i32 0, i32 31, i32 -176676356 }
@"_SM7__constG2-80" = private unnamed_addr constant { i8*, i32, i32, [31 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 31, i32 0, [31 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 100, i16 101, i16 114, i16 105, i16 118, i16 105, i16 110, i16 103, i16 46, i16 77, i16 105, i16 114, i16 114, i16 111, i16 114, i16 36, i16 83, i16 105, i16 110, i16 103, i16 108, i16 101, i16 116, i16 111, i16 110 ] }
@"_SM7__constG2-81" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [31 x i16] }* @"_SM7__constG2-80" to i8*), i32 0, i32 31, i32 -1713943224 }
@"_SM7__constG2-82" = private unnamed_addr constant { i8*, i32, i32, [32 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 32, i32 0, [32 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 80, i16 114, i16 111, i16 99, i16 101, i16 115, i16 115, i16 66, i16 117, i16 105, i16 108, i16 100, i16 101, i16 114, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 51 ] }
@"_SM7__constG2-83" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [32 x i16] }* @"_SM7__constG2-82" to i8*), i32 0, i32 32, i32 1896204635 }
@"_SM7__constG2-84" = private unnamed_addr constant { i8*, i32, i32, [32 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 32, i32 0, [32 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 73, i16 116, i16 101, i16 114, i16 97, i16 98, i16 108, i16 101, i16 79, i16 110, i16 99, i16 101, i16 79, i16 112, i16 115 ] }
@"_SM7__constG2-85" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [32 x i16] }* @"_SM7__constG2-84" to i8*), i32 0, i32 32, i32 -579145257 }
@"_SM7__constG2-86" = private unnamed_addr constant { i8*, i32, i32, [33 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 33, i32 0, [33 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 116, i16 101, i16 114, i16 73, i16 109, i16 112, i16 108, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49 ] }
@"_SM7__constG2-87" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [33 x i16] }* @"_SM7__constG2-86" to i8*), i32 0, i32 33, i32 1074689790 }
@"_SM7__constG2-88" = private unnamed_addr constant { i8*, i32, i32, [33 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 33, i32 0, [33 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 110, i16 99, i16 117, i16 114, i16 114, i16 101, i16 110, i16 116, i16 46, i16 69, i16 120, i16 101, i16 99, i16 117, i16 116, i16 105, i16 111, i16 110, i16 67, i16 111, i16 110, i16 116, i16 101, i16 120, i16 116 ] }
@"_SM7__constG2-89" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [33 x i16] }* @"_SM7__constG2-88" to i8*), i32 0, i32 33, i32 1192064582 }
@"_SM7__constG2-90" = private unnamed_addr constant { i8*, i32, i32, [34 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 34, i32 0, [34 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 116, i16 101, i16 114, i16 67, i16 108, i16 111, i16 115, i16 101, i16 100, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG2-91" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [34 x i16] }* @"_SM7__constG2-90" to i8*), i32 0, i32 34, i32 682839001 }
@"_SM7__constG2-92" = private unnamed_addr constant { i8*, i32, i32, [34 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 34, i32 0, [34 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 102, i16 117, i16 110, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 74, i16 80, i16 114, i16 111, i16 99, i16 101, i16 100, i16 117, i16 114, i16 101, i16 50 ] }
@"_SM7__constG2-93" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [34 x i16] }* @"_SM7__constG2-92" to i8*), i32 0, i32 34, i32 232275601 }
@"_SM7__constG2-94" = private unnamed_addr constant { i8*, i32, i32, [34 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 34, i32 0, [34 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 73, i16 110, i16 116, i16 65, i16 114, i16 114, i16 97, i16 121 ] }
@"_SM7__constG2-95" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [34 x i16] }* @"_SM7__constG2-94" to i8*), i32 0, i32 34, i32 1259979193 }
@"_SM7__constG2-96" = private unnamed_addr constant { i8*, i32, i32, [34 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 34, i32 0, [34 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 77, i16 111, i16 110, i16 105, i16 116, i16 111, i16 114, i16 36 ] }
@"_SM7__constG2-97" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [34 x i16] }* @"_SM7__constG2-96" to i8*), i32 0, i32 34, i32 394012697 }
@"_SM7__constG2-98" = private unnamed_addr constant { i8*, i32, i32, [34 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 34, i32 0, [34 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 67, i16 70, i16 117, i16 110, i16 99, i16 80, i16 116, i16 114, i16 55 ] }
@"_SM7__constG2-99" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [34 x i16] }* @"_SM7__constG2-98" to i8*), i32 0, i32 34, i32 1379985313 }
@"_SM7__constG3-100" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 99, i16 104, i16 97, i16 114, i16 115, i16 101, i16 116, i16 46, i16 67, i16 111, i16 100, i16 105, i16 110, i16 103, i16 69, i16 114, i16 114, i16 111, i16 114, i16 65, i16 99, i16 116, i16 105, i16 111, i16 110, i16 36 ] }
@"_SM7__constG3-101" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-100" to i8*), i32 0, i32 35, i32 -1939806344 }
@"_SM7__constG3-102" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 65, i16 99, i16 99, i16 101, i16 115, i16 115, i16 68, i16 101, i16 110, i16 105, i16 101, i16 100, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-103" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-102" to i8*), i32 0, i32 35, i32 -847649980 }
@"_SM7__constG3-104" = private unnamed_addr constant [7 x i64] [ i64 0, i64 1, i64 2, i64 4, i64 5, i64 6, i64 -1 ]
@"_SM7__constG3-105" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 116, i16 101, i16 114, i16 73, i16 109, i16 112, i16 108, i16 36, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 84, i16 111, i16 107, i16 101, i16 110 ] }
@"_SM7__constG3-106" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-105" to i8*), i32 0, i32 35, i32 1628790472 }
@"_SM7__constG3-107" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 77, i16 97, i16 112, i16 70, i16 97, i16 99, i16 116, i16 111, i16 114, i16 121, i16 68, i16 101, i16 102, i16 97, i16 117, i16 108, i16 116, i16 115 ] }
@"_SM7__constG3-108" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-107" to i8*), i32 0, i32 35, i32 -1886119096 }
@"_SM7__constG3-109" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 67, i16 70, i16 117, i16 110, i16 99, i16 80, i16 116, i16 114, i16 49, i16 55 ] }
@"_SM7__constG3-110" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-109" to i8*), i32 0, i32 35, i32 -170128388 }
@"_SM7__constG3-111" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 67, i16 70, i16 117, i16 110, i16 99, i16 80, i16 116, i16 114, i16 50, i16 48 ] }
@"_SM7__constG3-112" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-111" to i8*), i32 0, i32 35, i32 -170128364 }
@"_SM7__constG3-113" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 67, i16 70, i16 117, i16 110, i16 99, i16 80, i16 116, i16 114, i16 54, i16 36 ] }
@"_SM7__constG3-114" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-113" to i8*), i32 0, i32 35, i32 -170128252 }
@"_SM7__constG3-115" = private unnamed_addr constant { i8*, i32, i32, [36 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 36, i32 0, [36 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 66, i16 105, i16 103, i16 86, i16 101, i16 99, i16 116, i16 111, i16 114 ] }
@"_SM7__constG3-116" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [36 x i16] }* @"_SM7__constG3-115" to i8*), i32 0, i32 36, i32 311074607 }
@"_SM7__constG3-117" = private unnamed_addr constant [3 x i64] [ i64 0, i64 2, i64 -1 ]
@"_SM7__constG3-118" = private unnamed_addr constant { i8*, i32, i32, [36 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 36, i32 0, [36 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 67, i16 70, i16 117, i16 110, i16 99, i16 80, i16 116, i16 114, i16 49, i16 52, i16 36 ] }
@"_SM7__constG3-119" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [36 x i16] }* @"_SM7__constG3-118" to i8*), i32 0, i32 36, i32 -979012789 }
@"_SM7__constG3-120" = private unnamed_addr constant { i8*, i32, i32, [37 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 37, i32 0, [37 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 73, i16 108, i16 108, i16 101, i16 103, i16 97, i16 108, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 87, i16 105, i16 100, i16 116, i16 104, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-121" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [37 x i16] }* @"_SM7__constG3-120" to i8*), i32 0, i32 37, i32 865023958 }
@"_SM7__constG3-122" = private unnamed_addr constant { i8*, i32, i32, [37 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 37, i32 0, [37 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 99, i16 111, i16 110, i16 99, i16 117, i16 114, i16 114, i16 101, i16 110, i16 116, i16 46, i16 84, i16 105, i16 109, i16 101, i16 85, i16 110, i16 105, i16 116, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 50 ] }
@"_SM7__constG3-123" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [37 x i16] }* @"_SM7__constG3-122" to i8*), i32 0, i32 37, i32 -873736286 }
@"_SM7__constG3-124" = private unnamed_addr constant { i8*, i32, i32, [37 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 37, i32 0, [37 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 73, i16 110, i16 100, i16 101, i16 120, i16 101, i16 100, i16 83, i16 101, i16 113 ] }
@"_SM7__constG3-125" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [37 x i16] }* @"_SM7__constG3-124" to i8*), i32 0, i32 37, i32 -730067358 }
@"_SM7__constG3-126" = private unnamed_addr constant { i8*, i32, i32, [38 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 38, i32 0, [38 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 112, i16 97, i16 99, i16 107, i16 97, i16 103, i16 101, i16 36, i16 67, i16 111, i16 109, i16 112, i16 97, i16 114, i16 101, i16 78, i16 117, i16 108, i16 108, i16 97, i16 98, i16 108, i16 101, i16 115, i16 79, i16 112, i16 115, i16 36 ] }
@"_SM7__constG3-127" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [38 x i16] }* @"_SM7__constG3-126" to i8*), i32 0, i32 38, i32 1777242749 }
@"_SM7__constG3-128" = private unnamed_addr constant { i8*, i32, i32, [38 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 38, i32 0, [38 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 73, i16 110, i16 100, i16 101, i16 120, i16 101, i16 100, i16 66, i16 117, i16 102, i16 102, i16 101, i16 114 ] }
@"_SM7__constG3-129" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [38 x i16] }* @"_SM7__constG3-128" to i8*), i32 0, i32 38, i32 -98384895 }
@"_SM7__constG3-130" = private unnamed_addr constant { i8*, i32, i32, [39 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 39, i32 0, [39 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 76, i16 105, i16 110, i16 101, i16 97, i16 114, i16 83, i16 101, i16 113, i16 79, i16 112, i16 115 ] }
@"_SM7__constG3-131" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [39 x i16] }* @"_SM7__constG3-130" to i8*), i32 0, i32 39, i32 -1650189044 }
@"_SM7__constG3-132" = private unnamed_addr constant { i8*, i32, i32, [39 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 39, i32 0, [39 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 80, i16 114, i16 105, i16 109, i16 105, i16 116, i16 105, i16 118, i16 101, i16 66, i16 121, i16 116, i16 101 ] }
@"_SM7__constG3-133" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [39 x i16] }* @"_SM7__constG3-132" to i8*), i32 0, i32 39, i32 -875835936 }
@"_SM7__constG3-134" = private unnamed_addr constant { i8*, i32, i32, [40 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 40, i32 0, [40 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 65, i16 114, i16 114, i16 97, i16 121, i16 73, i16 110, i16 100, i16 101, i16 120, i16 79, i16 117, i16 116, i16 79, i16 102, i16 66, i16 111, i16 117, i16 110, i16 100, i16 115, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-135" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [40 x i16] }* @"_SM7__constG3-134" to i8*), i32 0, i32 40, i32 1625905794 }
@"_SM7__constG3-136" = private unnamed_addr constant { i8*, i32, i32, [40 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 40, i32 0, [40 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 83, i16 116, i16 97, i16 110, i16 100, i16 97, i16 114, i16 100, i16 79, i16 112, i16 101, i16 110, i16 79, i16 112, i16 116, i16 105, i16 111, i16 110, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 50 ] }
@"_SM7__constG3-137" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [40 x i16] }* @"_SM7__constG3-136" to i8*), i32 0, i32 40, i32 1087941890 }
@"_SM7__constG3-138" = private unnamed_addr constant { i8*, i32, i32, [40 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 40, i32 0, [40 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 71, i16 114, i16 111, i16 119, i16 97, i16 98, i16 108, i16 101, i16 66, i16 117, i16 105, i16 108, i16 100, i16 101, i16 114 ] }
@"_SM7__constG3-139" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [40 x i16] }* @"_SM7__constG3-138" to i8*), i32 0, i32 40, i32 -1026725282 }
@"_SM7__constG3-140" = private unnamed_addr constant { i8*, i32, i32, [41 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 41, i32 0, [41 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 83, i16 116, i16 97, i16 110, i16 100, i16 97, i16 114, i16 100, i16 79, i16 112, i16 101, i16 110, i16 79, i16 112, i16 116, i16 105, i16 111, i16 110, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 49, i16 48 ] }
@"_SM7__constG3-141" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [41 x i16] }* @"_SM7__constG3-140" to i8*), i32 0, i32 41, i32 -633539761 }
@"_SM7__constG3-142" = private unnamed_addr constant { i8*, i32, i32, [41 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 41, i32 0, [41 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 116, i16 101, i16 114, i16 36, i16 66, i16 105, i16 103, i16 68, i16 101, i16 99, i16 105, i16 109, i16 97, i16 108, i16 76, i16 97, i16 121, i16 111, i16 117, i16 116, i16 70, i16 111, i16 114, i16 109, i16 36 ] }
@"_SM7__constG3-143" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [41 x i16] }* @"_SM7__constG3-142" to i8*), i32 0, i32 41, i32 865039787 }
@"_SM7__constG3-144" = private unnamed_addr constant { i8*, i32, i32, [41 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 41, i32 0, [41 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 112, i16 111, i16 115, i16 105, i16 120, i16 46, i16 112, i16 119, i16 100, i16 79, i16 112, i16 115, i16 36, i16 112, i16 97, i16 115, i16 115, i16 119, i16 100, i16 79, i16 112, i16 115, i16 36 ] }
@"_SM7__constG3-145" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [41 x i16] }* @"_SM7__constG3-144" to i8*), i32 0, i32 41, i32 1465656283 }
@"_SM7__constG3-146" = private unnamed_addr constant { i8*, i32, i32, [42 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 42, i32 0, [42 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 73, i16 108, i16 108, i16 101, i16 103, i16 97, i16 108, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 67, i16 111, i16 110, i16 118, i16 101, i16 114, i16 115, i16 105, i16 111, i16 110, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-147" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [42 x i16] }* @"_SM7__constG3-146" to i8*), i32 0, i32 42, i32 -676933300 }
@"_SM7__constG3-148" = private unnamed_addr constant [5 x i64] [ i64 0, i64 1, i64 2, i64 4, i64 -1 ]
@"_SM7__constG3-149" = private unnamed_addr constant { i8*, i32, i32, [42 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 42, i32 0, [42 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 85, i16 110, i16 107, i16 110, i16 111, i16 119, i16 110, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 67, i16 111, i16 110, i16 118, i16 101, i16 114, i16 115, i16 105, i16 111, i16 110, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-150" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [42 x i16] }* @"_SM7__constG3-149" to i8*), i32 0, i32 42, i32 1309675256 }
@"_SM7__constG3-151" = private unnamed_addr constant { i8*, i32, i32, [42 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 42, i32 0, [42 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 80, i16 114, i16 105, i16 109, i16 105, i16 116, i16 105, i16 118, i16 101, i16 66, i16 111, i16 111, i16 108, i16 101, i16 97, i16 110 ] }
@"_SM7__constG3-152" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [42 x i16] }* @"_SM7__constG3-151" to i8*), i32 0, i32 42, i32 -392646576 }
@"_SM7__constG3-153" = private unnamed_addr constant { i8*, i32, i32, [43 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 43, i32 0, [43 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 116, i16 101, i16 114, i16 73, i16 109, i16 112, i16 108, i16 36, i16 80, i16 97, i16 114, i16 115, i16 101, i16 114, i16 83, i16 116, i16 97, i16 116, i16 101, i16 77, i16 97, i16 99, i16 104, i16 105, i16 110, i16 101, i16 36 ] }
@"_SM7__constG3-154" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [43 x i16] }* @"_SM7__constG3-153" to i8*), i32 0, i32 43, i32 567701269 }
@"_SM7__constG3-155" = private unnamed_addr constant { i8*, i32, i32, [43 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 43, i32 0, [43 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 101, i16 102, i16 108, i16 101, i16 99, i16 116, i16 46, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116, i16 70, i16 97, i16 99, i16 116, i16 111, i16 114, i16 121, i16 36, i16 70, i16 108, i16 111, i16 97, i16 116, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116 ] }
@"_SM7__constG3-156" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [43 x i16] }* @"_SM7__constG3-155" to i8*), i32 0, i32 43, i32 -1933157047 }
@"_SM7__constG3-157" = private unnamed_addr constant { i8*, i32, i32, [43 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 43, i32 0, [43 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 101, i16 102, i16 108, i16 101, i16 99, i16 116, i16 46, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116, i16 70, i16 97, i16 99, i16 116, i16 111, i16 114, i16 121, i16 36, i16 83, i16 104, i16 111, i16 114, i16 116, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116 ] }
@"_SM7__constG3-158" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [43 x i16] }* @"_SM7__constG3-157" to i8*), i32 0, i32 43, i32 -1014522007 }
@"_SM7__constG3-159" = private unnamed_addr constant { i8*, i32, i32, [44 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 44, i32 0, [44 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 65, i16 114, i16 114, i16 97, i16 121, i16 66, i16 117, i16 102, i16 102, i16 101, i16 114, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 49 ] }
@"_SM7__constG3-160" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [44 x i16] }* @"_SM7__constG3-159" to i8*), i32 0, i32 44, i32 839895874 }
@"_SM7__constG3-161" = private unnamed_addr constant { i8*, i32, i32, [44 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 44, i32 0, [44 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 65, i16 114, i16 114, i16 97, i16 121, i16 83, i16 101, i16 113, i16 36, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49 ] }
@"_SM7__constG3-162" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [44 x i16] }* @"_SM7__constG3-161" to i8*), i32 0, i32 44, i32 -241353022 }
@"_SM7__constG3-163" = private unnamed_addr constant { i8*, i32, i32, [44 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 44, i32 0, [44 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 101, i16 102, i16 108, i16 101, i16 99, i16 116, i16 46, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116, i16 70, i16 97, i16 99, i16 116, i16 111, i16 114, i16 121, i16 36, i16 68, i16 111, i16 117, i16 98, i16 108, i16 101, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116 ] }
@"_SM7__constG3-164" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [44 x i16] }* @"_SM7__constG3-163" to i8*), i32 0, i32 44, i32 1449819202 }
@"_SM7__constG3-165" = private unnamed_addr constant { i8*, i32, i32, [45 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 45, i32 0, [45 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 83, i16 99, i16 97, i16 108, i16 97, i16 79, i16 112, i16 115, i16 36, i16 74, i16 97, i16 118, i16 97, i16 73, i16 116, i16 101, i16 114, i16 97, i16 116, i16 111, i16 114, i16 79, i16 112, i16 115, i16 36, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 50 ] }
@"_SM7__constG3-166" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [45 x i16] }* @"_SM7__constG3-165" to i8*), i32 0, i32 45, i32 1062676399 }
@"_SM7__constG3-167" = private unnamed_addr constant { i8*, i32, i32, [50 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 50, i32 0, [50 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 67, i16 108, i16 97, i16 115, i16 115, i16 84, i16 97, i16 103, i16 83, i16 101, i16 113, i16 70, i16 97, i16 99, i16 116, i16 111, i16 114, i16 121, i16 36, i16 65, i16 110, i16 121, i16 83, i16 101, i16 113, i16 68, i16 101, i16 108, i16 101, i16 103, i16 97, i16 116, i16 101 ] }
@"_SM7__constG3-168" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [50 x i16] }* @"_SM7__constG3-167" to i8*), i32 0, i32 50, i32 699396871 }
@"_SM7__constG3-169" = private unnamed_addr constant { i8*, i32, i32, [50 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 50, i32 0, [50 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 83, i16 116, i16 114, i16 105, i16 99, i16 116, i16 79, i16 112, i16 116, i16 105, i16 109, i16 105, i16 122, i16 101, i16 100, i16 67, i16 108, i16 97, i16 115, i16 115, i16 84, i16 97, i16 103, i16 83, i16 101, i16 113, i16 70, i16 97, i16 99, i16 116, i16 111, i16 114, i16 121 ] }
@"_SM7__constG3-170" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [50 x i16] }* @"_SM7__constG3-169" to i8*), i32 0, i32 50, i32 -176896637 }
@"_SM7__constG3-171" = private unnamed_addr constant { i8*, i32, i32, [51 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 51, i32 0, [51 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 97, i16 116, i16 116, i16 114, i16 105, i16 98, i16 117, i16 116, i16 101, i16 46, i16 80, i16 111, i16 115, i16 105, i16 120, i16 70, i16 105, i16 108, i16 101, i16 80, i16 101, i16 114, i16 109, i16 105, i16 115, i16 115, i16 105, i16 111, i16 110, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 54 ] }
@"_SM7__constG3-172" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [51 x i16] }* @"_SM7__constG3-171" to i8*), i32 0, i32 51, i32 489634478 }
@"_SM7__constG3-173" = private unnamed_addr constant { i8*, i32, i32, [66 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 66, i32 0, [66 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 105, i16 101, i16 101, i16 101, i16 55, i16 53, i16 52, i16 116, i16 111, i16 115, i16 116, i16 114, i16 105, i16 110, i16 103, i16 46, i16 114, i16 121, i16 117, i16 46, i16 82, i16 121, i16 117, i16 68, i16 111, i16 117, i16 98, i16 108, i16 101, i16 36, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 51 ] }
@"_SM7__constG3-174" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [66 x i16] }* @"_SM7__constG3-173" to i8*), i32 0, i32 66, i32 581473586 }
@"_SM7__constG3-175" = private unnamed_addr constant [5 x i64] [ i64 0, i64 1, i64 3, i64 4, i64 -1 ]
@"_SM7__constG3-176" = private unnamed_addr constant { i8*, i32, i32, [4 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 4, i32 0, [4 x i16] [ i16 110, i16 117, i16 108, i16 108 ] }
@"_SM7__constG3-177" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [4 x i16] }* @"_SM7__constG3-176" to i8*), i32 0, i32 4, i32 3392903 }
@"_SM7__constG3-178" = private unnamed_addr constant { i8*, i32, i32, [0 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 0, i32 0, [0 x i16] [  ] }
@"_SM7__constG3-179" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [0 x i16] }* @"_SM7__constG3-178" to i8*), i32 0, i32 0, i32 0 }
@"_SM7__constG3-180" = private unnamed_addr constant { i8*, i32, i32, [32 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 32, i32 0, [32 x i16] [ i16 32, i16 105, i16 115, i16 32, i16 111, i16 117, i16 116, i16 32, i16 111, i16 102, i16 32, i16 98, i16 111, i16 117, i16 110, i16 100, i16 115, i16 32, i16 40, i16 109, i16 105, i16 110, i16 32, i16 48, i16 44, i16 32, i16 109, i16 97, i16 120, i16 32, i16 49, i16 41 ] }
@"_SM7__constG3-181" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [32 x i16] }* @"_SM7__constG3-180" to i8*), i32 0, i32 32, i32 10411116 }
@"_SM7__constG3-182" = private unnamed_addr constant { i8*, i32, i32, [5 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 5, i32 0, [5 x i16] [ i16 91, i16 112, i16 111, i16 115, i16 61 ] }
@"_SM7__constG3-183" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [5 x i16] }* @"_SM7__constG3-182" to i8*), i32 0, i32 5, i32 87487300 }
@"_SM7__constG3-184" = private unnamed_addr constant { i8*, i32, i32, [5 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 5, i32 0, [5 x i16] [ i16 32, i16 108, i16 105, i16 109, i16 61 ] }
@"_SM7__constG3-185" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [5 x i16] }* @"_SM7__constG3-184" to i8*), i32 0, i32 5, i32 32874445 }
@"_SM7__constG3-186" = private unnamed_addr constant { i8*, i32, i32, [5 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 5, i32 0, [5 x i16] [ i16 32, i16 99, i16 97, i16 112, i16 61 ] }
@"_SM7__constG3-187" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [5 x i16] }* @"_SM7__constG3-186" to i8*), i32 0, i32 5, i32 32598731 }
@"_SM7__constG3-188" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 93 ] }
@"_SM7__constG3-189" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-188" to i8*), i32 0, i32 1, i32 93 }
@"_SM7__constG3-190" = private unnamed_addr constant { i8*, i32, i32, [3 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 3, i32 0, [3 x i16] [ i16 77, i16 97, i16 112 ] }
@"_SM7__constG3-191" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [3 x i16] }* @"_SM7__constG3-190" to i8*), i32 0, i32 3, i32 77116 }
@"_SM7__constG3-192" = private unnamed_addr constant { i8*, i32, i32, [3 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 3, i32 0, [3 x i16] [ i16 66, i16 111, i16 120 ] }
@"_SM7__constG3-193" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [3 x i16] }* @"_SM7__constG3-192" to i8*), i32 0, i32 3, i32 66987 }
@"_SM7__constG3-194" = private unnamed_addr constant { i8*, i32, i32, [16 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 16, i32 0, [16 x i16] [ i16 40, i16 60, i16 110, i16 111, i16 116, i16 32, i16 99, i16 111, i16 109, i16 112, i16 117, i16 116, i16 101, i16 100, i16 62, i16 41 ] }
@"_SM7__constG3-195" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [16 x i16] }* @"_SM7__constG3-194" to i8*), i32 0, i32 16, i32 -816888583 }
@"_SM7__constG3-196" = private unnamed_addr constant { i8*, i32, i32, [14 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 14, i32 0, [14 x i16] [ i16 85, i16 110, i16 107, i16 110, i16 111, i16 119, i16 110, i16 32, i16 83, i16 111, i16 117, i16 114, i16 99, i16 101 ] }
@"_SM7__constG3-197" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [14 x i16] }* @"_SM7__constG3-196" to i8*), i32 0, i32 14, i32 1469797617 }
@"_SM7__constG3-198" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 58 ] }
@"_SM7__constG3-199" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-198" to i8*), i32 0, i32 1, i32 58 }
@"_SM7__constG3-200" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 46 ] }
@"_SM7__constG3-201" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-200" to i8*), i32 0, i32 1, i32 46 }
@"_SM7__constG3-202" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 40 ] }
@"_SM7__constG3-203" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-202" to i8*), i32 0, i32 1, i32 40 }
@"_SM7__constG3-204" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 41 ] }
@"_SM7__constG3-205" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-204" to i8*), i32 0, i32 1, i32 41 }
@"_SM7__constG3-206" = private unnamed_addr constant { i8*, i32, i32, [19 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 19, i32 0, [19 x i16] [ i16 115, i16 104, i16 111, i16 117, i16 108, i16 100, i16 32, i16 110, i16 111, i16 116, i16 32, i16 103, i16 101, i16 116, i16 32, i16 104, i16 101, i16 114, i16 101 ] }
@"_SM7__constG3-207" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [19 x i16] }* @"_SM7__constG3-206" to i8*), i32 0, i32 19, i32 -849617292 }
@"_SM7__constG3-208" = private unnamed_addr constant { i8*, i32, i32, [22 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 22, i32 0, [22 x i16] [ i16 110, i16 117, i16 108, i16 108, i16 32, i16 67, i16 111, i16 100, i16 105, i16 110, i16 103, i16 69, i16 114, i16 114, i16 111, i16 114, i16 65, i16 99, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-209" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [22 x i16] }* @"_SM7__constG3-208" to i8*), i32 0, i32 22, i32 -646354405 }
@"_SM7__constG3-210" = private unnamed_addr constant { i8*, i32, i32, [6 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 6, i32 0, [6 x i16] [ i16 73, i16 71, i16 78, i16 79, i16 82, i16 69 ] }
@"_SM7__constG3-211" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [6 x i16] }* @"_SM7__constG3-210" to i8*), i32 0, i32 6, i32 -2137067054 }
@"_SM7__constG3-212" = private unnamed_addr constant { i8*, i32, i32, [7 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 7, i32 0, [7 x i16] [ i16 82, i16 69, i16 80, i16 76, i16 65, i16 67, i16 69 ] }
@"_SM7__constG3-213" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [7 x i16] }* @"_SM7__constG3-212" to i8*), i32 0, i32 7, i32 1812479636 }
@"_SM7__constG3-214" = private unnamed_addr constant { i8*, i32, i32, [6 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 6, i32 0, [6 x i16] [ i16 82, i16 69, i16 80, i16 79, i16 82, i16 84 ] }
@"_SM7__constG3-215" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [6 x i16] }* @"_SM7__constG3-214" to i8*), i32 0, i32 6, i32 -1881192140 }
@"_SM7__constG3-216" = private unnamed_addr constant { i8*, i32, i32, [10 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 10, i32 0, [10 x i16] [ i16 83, i16 67, i16 73, i16 69, i16 78, i16 84, i16 73, i16 70, i16 73, i16 67 ] }
@"_SM7__constG3-217" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [10 x i16] }* @"_SM7__constG3-216" to i8*), i32 0, i32 10, i32 1826124841 }
@"_SM7__constG3-218" = private unnamed_addr constant { i8*, i32, i32, [13 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 13, i32 0, [13 x i16] [ i16 68, i16 69, i16 67, i16 73, i16 77, i16 65, i16 76, i16 95, i16 70, i16 76, i16 79, i16 65, i16 84 ] }
@"_SM7__constG3-219" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [13 x i16] }* @"_SM7__constG3-218" to i8*), i32 0, i32 13, i32 1639007150 }
@"_SM7__constG3-220" = private unnamed_addr constant { i8*, i32, i32, [4 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 4, i32 0, [4 x i16] [ i16 32, i16 33, i16 61, i16 32 ] }
@"_SM7__constG3-221" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [4 x i16] }* @"_SM7__constG3-220" to i8*), i32 0, i32 4, i32 986948 }
@"_SM7__constG3-222" = private unnamed_addr constant { i8*, i32, i32, [14 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 14, i32 0, [14 x i16] [ i16 67, i16 111, i16 110, i16 118, i16 101, i16 114, i16 115, i16 105, i16 111, i16 110, i16 32, i16 61, i16 32, i16 39 ] }
@"_SM7__constG3-223" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [14 x i16] }* @"_SM7__constG3-222" to i8*), i32 0, i32 14, i32 -13956326 }
@"_SM7__constG3-224" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 39 ] }
@"_SM7__constG3-225" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-224" to i8*), i32 0, i32 1, i32 39 }

declare dereferenceable_or_null(32) i8* @"_SM21scala.collection.Map$G4load"() noinline
@"_SM36scala.scalanative.runtime.CharArray$G8instance" = external constant { i8* }

declare i1 @"_SM28scala.collection.AbstractSeqD6equalsL16java.lang.ObjectzEO"(i8*, i8*)
@"_SM18java.util.Objects$G8instance" = external constant { i8* }
@"_SM13scala.Tuple2$G8instance" = external constant { i8* }

declare dereferenceable_or_null(8) i8* @"_SM34scala.collection.mutable.ArraySeq$D21$anonfun$newBuilder$1L16java.lang.ObjectL33scala.collection.mutable.ArraySeqEPT34scala.collection.mutable.ArraySeq$"(i8*, i8*)

declare dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8*, i16)
@"_SM38scala.scalanative.unsafe.Tag$CStruct5$G8instance" = external constant { i8* }
@"_SM31java.lang.IllegalStateExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare i1 @"_SM35java.util.ScalaOps$JavaIteratorOps$D27forall$extension$$anonfun$1L15scala.Function1L16java.lang.ObjectzEPT35java.util.ScalaOps$JavaIteratorOps$"(i8*, i8*, i8*)

declare dereferenceable_or_null(16) i8* @"_SM38scala.collection.immutable.IndexedSeq$G4load"() noinline
@"_SM34java.lang.IllegalArgumentExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare i1 @"_SM13scala.Predef$D15Boolean2booleanL17java.lang.BooleanzEO"(i8*, i8*)
@"_SM34scala.scalanative.unsafe.Tag$UInt$G8instance" = external constant { i8* }

declare nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8*)
@"_SM25scala.collection.IterableG4type" = external global { i8*, i32, i32, i8* }

declare nonnull dereferenceable(8) i8* @"_SM28scala.collection.mutable.SeqD6$init$uEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM15java.lang.ClassD7getNameL16java.lang.StringEO"(i8*)

declare i1 @"_SM28java.nio.charset.CoderResultD12isUnmappablezEO"(i8*) inlinehint

declare i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8*) noinline
@"_SM23scala.reflect.ClassTag$G8instance" = external constant { i8* }

declare nonnull dereferenceable(8) i8* @"_SM27scala.runtime.ScalaRunTime$D12array_updateL16java.lang.ObjectiL16java.lang.ObjectuEO"(i8*, i8*, i32, i8*) inlinehint

declare dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.ObjectL23java.lang.StringBuilderEO"(i8*, i8*)

declare nonnull dereferenceable(56) i8* @"_SM17niocharset.UTF_8$D10newDecoderL31java.nio.charset.CharsetDecoderEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM38scala.collection.mutable.IndexedSeqOpsD6$init$uEO"(i8*)

declare dereferenceable_or_null(8) i8* @"_SM36scala.scalanative.runtime.CharArray$D5allociL35scala.scalanative.runtime.CharArrayEO"(i8*, i32) inlinehint

declare dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToBooleanzL17java.lang.BooleanEO"(i8*, i1)
@"_SM23scala.runtime.LazyVals$G8instance" = external constant { i8* }
@"_SM38scala.collection.StringOps$$$Lambda$13G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM56scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$D25doubleToString$$anonfun$3L21scala.runtime.LongRefiLAc_L20scala.runtime.IntRefiiuEPT56scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$"(i8*, i8*, i32, i8*, i8*, i32, i32)
@"_SM28scala.runtime.Scala3RunTime$G8instance" = external constant { i8* }

declare dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD4flipL19java.nio.CharBufferEO"(i8*) inlinehint

declare i32 @"_SM19java.nio.CharBufferD12_arrayOffsetiEO"(i8*) alwaysinline

declare nonnull dereferenceable(8) i8* @"_SM31scala.collection.mutable.SeqOpsD6$init$uEO"(i8*)

declare i32 @"_SM30scala.util.hashing.MurmurHash3D12finalizeHashiiiEO"(i8*, i32, i32)

declare nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8*)
@"_SM35scala.scalanative.runtime.LazyVals$G8instance" = external constant { i8* }

declare nonnull dereferenceable(8) i8* @"_SM35scala.scalanative.runtime.LazyVals$D7setFlagR_iiuEO"(i8*, i8*, i32, i32) inlinehint

declare i8* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO"(i8*, i32) noinline
@"_SM16scala.MatchErrorG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }
@"_SM23java.lang.StringBuilderG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM23java.util.FormatterImplD8toStringL16java.lang.StringEO"(i8*)

declare nonnull dereferenceable(16) i8* @"_SM38scala.collection.mutable.ArrayBuilder$D4makeL22scala.reflect.ClassTagL37scala.collection.mutable.ArrayBuilderEO"(i8*, i8*) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM15java.lang.FloatD4TYPEL15java.lang.ClassEo"() inlinehint

declare nonnull dereferenceable(8) i8* @"_SM38java.nio.charset.CoderMalfunctionErrorRL19java.lang.ExceptionE"(i8*, i8*)
@"_SM34scala.scalanative.unsafe.CFuncPtr6G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.GrowableD13$plus$plus$eqL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8*, i8*) inlinehint

declare i32 @"_SM28java.nio.charset.CoderResultD6lengthiEO"(i8*) inlinehint

declare dereferenceable_or_null(16) i8* @"_SM24niocharset.UTF_8$DecoderD10decodeLoopL19java.nio.ByteBufferL19java.nio.CharBufferL28java.nio.charset.CoderResultEO"(i8*, i8*, i8*)

declare i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8*)
@"_SM16java.lang.StringG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM35java.lang.IndexOutOfBoundsExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD9substringiiL16java.lang.StringEO"(i8*, i32, i32)

declare nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.BuilderD6$init$uEO"(i8*)

declare nonnull dereferenceable(24) i8* @"_SM13scala.Tuple2$D5applyL16java.lang.ObjectL16java.lang.ObjectL12scala.Tuple2EO"(i8*, i8*, i8*)

declare nonnull dereferenceable(48) i8* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$D5applyL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL37scala.scalanative.unsafe.Tag$CStruct5EO"(i8*, i8*, i8*, i8*, i8*, i8*)

declare dereferenceable_or_null(32) i8* @"_SM28scala.reflect.AnyValManifestD8toStringL16java.lang.StringEO"(i8*) alwaysinline
@"_SM19scala.math.package$G8instance" = external constant { i8* }

declare i32 @"_SM28scala.collection.AbstractSeqD8hashCodeiEO"(i8*)

declare dereferenceable_or_null(16) i8* @"_SM24niocharset.UTF_8$EncoderD10encodeLoopL19java.nio.CharBufferL19java.nio.ByteBufferL28java.nio.charset.CoderResultEO"(i8*, i8*, i8*)

declare i16 @"_SM16java.lang.StringD6charAticEO"(i8*, i32)

declare i64 @"_SM10scala.Int$D8int2longijEO"(i8*, i32)

declare i32 @"_SM16java.lang.ObjectD8hashCodeiEO"(i8*) inlinehint
@"_SM33java.nio.BufferUnderflowExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare i1 @"_SM17java.lang.System$D36getUserCountry$$anonfun$1$$anonfun$1czEPT17java.lang.System$"(i8*, i16)
@"_SM39java.util.DuplicateFormatFlagsExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }
@"_SM29scala.collection.IterableOnceG4type" = external global { i8*, i32, i32, i8* }

declare dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD3putL16java.lang.StringL19java.nio.CharBufferEO"(i8*, i8*)

declare i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8*, i8*, i8*) noinline

declare dereferenceable_or_null(8) i8* @"_SM19java.nio.CharBufferD6_arrayL16java.lang.ObjectEO"(i8*) alwaysinline

declare i32 @"_SM19java.nio.CharBufferD8hashCodeiEO"(i8*) noinline

declare i32 @"_SM14java.lang.MathD3miniiiEo"(i32, i32) inlinehint

declare i8* @"scalanative_throw"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8*)

declare i32 @"_SM21scala.runtime.StaticsD7anyHashL16java.lang.ObjectiEo"(i8*) inlinehint

declare i16 @"_SM27scala.runtime.BoxesRunTime$D11unboxToCharL16java.lang.ObjectcEO"(i8*, i8*)
@"__modules" = external global [194 x i8*]

declare i1 @"_SM27scala.runtime.BoxesRunTime$D14unboxToBooleanL16java.lang.ObjectzEO"(i8*, i8*)
@"_SM38scala.collection.mutable.ArrayBuilder$G8instance" = external constant { i8* }

declare i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8*, i32)

declare nonnull dereferenceable(8) i8* @"_SM27scala.collection.MapFactoryD6$init$uEO"(i8*)

declare i32 @"_SM19scala.math.package$D3miniiiEO"(i8*, i32, i32)

declare i32 @"_SM16java.lang.StringD6lengthiEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8*)

declare i1 @"_SM19java.nio.CharBufferD6equalsL16java.lang.ObjectzEO"(i8*, i8*)

declare dereferenceable_or_null(32) i8* @"_SM33java.nio.file.FileSystemExceptionD10getMessageL16java.lang.StringEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8*)

declare nonnull dereferenceable(16) i8* @"_SM13scala.Predef$D11int2IntegeriL17java.lang.IntegerEO"(i8*, i32)

declare i1 @"_SM35scala.scalanative.runtime.LazyVals$D3CASR_jiizEO"(i8*, i8*, i64, i32, i32) inlinehint

declare i32 @"_SM27scala.runtime.ScalaRunTime$D12array_lengthL16java.lang.ObjectiEO"(i8*, i8*)

declare dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"() noinline

declare dereferenceable_or_null(32) i8* @"_SM19java.lang.ThrowableD10getMessageL16java.lang.StringEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO"(i8*) inlinehint

declare i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8*, i8*) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM17java.lang.IntegerD8toStringL16java.lang.StringEO"(i8*) inlinehint
@"_SM30scala.collection.Map$$Lambda$1G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i8* @"_SM35scala.scalanative.runtime.LazyVals$D17wait4NotificationR_jiuEO"(i8*, i8*, i64, i32)

declare nonnull dereferenceable(16) i8* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8*, i8*) inlinehint
@"_SM32java.nio.ReadOnlyBufferExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(16) i8* @"_SM29java.nio.charset.CoderResult$D18malformedForLengthiL28java.nio.charset.CoderResultEO"(i8*, i32) inlinehint

declare i32 @"_SM22scala.runtime.RichInt$D13min$extensioniiiEO"(i8*, i32, i32)

declare i1 @"_SM28java.nio.charset.CoderResultD11isUnderflowzEO"(i8*) inlinehint

declare nonnull dereferenceable(16) i8* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8*, i8*) inlinehint
@"_SM37scala.scalanative.runtime.ObjectArrayG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }
@"_SM10scala.Int$G8instance" = external constant { i8* }

declare dereferenceable_or_null(24) i8* @"_SM40java.util.Formatter$BigDecimalLayoutFormD10SCIENTIFICL40java.util.Formatter$BigDecimalLayoutFormEo"() inlinehint
@"_SM20java.nio.CharBuffer$G8instance" = external constant { i8* }

declare i8* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO"(i8*) noinline

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD8toStringL16java.lang.StringEO"(i8*)

declare i1 @"_SM18java.util.Objects$D6equalsL16java.lang.ObjectL16java.lang.ObjectzEO"(i8*, i8*, i8*) inlinehint
@"_SM38scala.scalanative.runtime.DoubleArray$G8instance" = external constant { i8* }

declare dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8*)
@"_SM28scala.scalanative.unsafe.PtrG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(24) i8* @"_SM40java.util.Formatter$BigDecimalLayoutFormD13DECIMAL_FLOATL40java.util.Formatter$BigDecimalLayoutFormEo"() inlinehint

declare nonnull dereferenceable(24) i8* @"_SM32scala.collection.mutable.BuilderD9mapResultL15scala.Function1L32scala.collection.mutable.BuilderEO"(i8*, i8*)

declare nonnull dereferenceable(8) i8* @"_SM30scala.collection.Map$$Lambda$1RL20scala.collection.MapL20scala.collection.MapE"(i8*, i8*, i8*)

declare i1 @"_SM28java.nio.charset.CoderResultD10isOverflowzEO"(i8*) inlinehint
@"_SM35scala.scalanative.unsafe.CFuncPtr14G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" = external constant { i8* }
@"_SM37scala.scalanative.runtime.ShortArray$G8instance" = external constant { i8* }

declare dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD3putL19java.nio.CharBufferL19java.nio.CharBufferEO"(i8*, i8*) noinline
@"_SM34scala.scalanative.runtime.package$G8instance" = external constant { i8* }

declare i32 @"_SM17java.lang.IntegerD8parseIntL16java.lang.StringiEo"(i8*) inlinehint

declare i1 @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO"(i8*, i8*) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM29scala.collection.AbstractViewD8toStringL16java.lang.StringEO"(i8*)
@"_SM22scala.runtime.RichInt$G8instance" = external constant { i8* }

declare i32 @"_SM19scala.math.package$D3maxiiiEO"(i8*, i32, i32)

declare nonnull dereferenceable(8) i8* @"_SM26java.io.OutputStreamWriterD5writeLAc_iiuEO"(i8*, i8*, i32, i32)

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD6formatL16java.lang.StringLAL16java.lang.Object_L16java.lang.StringEo"(i8*, i8*) inlinehint
@"_SM38java.nio.charset.CoderMalfunctionErrorG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }
@"_SM19java.lang.ThrowableG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM23scala.reflect.ClassTag$D6AnyRefL22scala.reflect.ClassTagEO"(i8*) inlinehint

declare i8* @"_SM28java.nio.charset.CoderResultD14throwExceptionuEO"(i8*)
@"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" = external constant { i8* }

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.ObjectD8getClassL15java.lang.ClassEO"(i8*) inlinehint
@"_SM28java.lang.ClassCastExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }
@"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" = external constant { i8* }
@"_SM39java.lang.UnsupportedOperationExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"() noinline

declare dereferenceable_or_null(32) i8* @"_SM28scala.collection.AbstractSeqD8toStringL16java.lang.StringEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM19java.lang.CharacterD8toStringL16java.lang.StringEO"(i8*) inlinehint
@"_SM37scala.scalanative.runtime.FloatArray$G8instance" = external constant { i8* }

declare i1 @"_SM33scala.collection.AbstractIterableD6forallL15scala.Function1zEO"(i8*, i8*)

declare nonnull dereferenceable(8) i8* @"_SM33scala.collection.mutable.GrowableD6$init$uEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM23java.util.FormatterImplD21sendToDest$$anonfun$1L30scala.collection.immutable.SequEPT23java.util.FormatterImpl"(i8*, i8*)

declare dereferenceable_or_null(8) i8* @"_SM37scala.scalanative.runtime.FloatArray$D5allociL36scala.scalanative.runtime.FloatArrayEO"(i8*, i32) inlinehint
@"_SM35scala.scalanative.runtime.CharArrayG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }

declare i32 @"_SM28scala.reflect.AnyValManifestD8hashCodeiEO"(i8*) alwaysinline

declare dereferenceable_or_null(8) i8* @"_SM18java.util.package$D19CompareNullablesOpsL16java.lang.ObjectL16java.lang.ObjectEO"(i8*, i8*)

declare i64 @"_SM23scala.runtime.LazyVals$D5STATEjijEO"(i8*, i64, i32)

declare nonnull dereferenceable(8) i8* @"_SM19java.io.PrintStreamD22printString$$anonfun$1L16java.lang.StringuEPT19java.io.PrintStream"(i8*, i8*)

declare dereferenceable_or_null(32) i8* @"_SM38scala.collection.mutable.StringBuilderD6resultL16java.lang.StringEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO"(i8*, i8*)

declare i64 @"_SM32scala.scalanative.unsigned.ULongD6toLongjEO"(i8*) inlinehint

declare i1 @"_SM28scala.collection.AbstractMapD8canEqualL16java.lang.ObjectzEO"(i8*, i8*)

declare dereferenceable_or_null(8) i8* @"_SM37scala.scalanative.runtime.ShortArray$D5allociL36scala.scalanative.runtime.ShortArrayEO"(i8*, i32) inlinehint
@"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" = external constant { i8* }

declare dereferenceable_or_null(32) i8* @"_SM27scala.runtime.ScalaRunTime$D9_toStringL13scala.ProductL16java.lang.StringEO"(i8*, i8*)

declare dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD8positioniL19java.nio.CharBufferEO"(i8*, i32) inlinehint

declare i32 @"_SM22scala.runtime.RichInt$D13max$extensioniiiEO"(i8*, i32, i32)
@"_SM35scala.scalanative.unsigned.package$G8instance" = external constant { i8* }

declare dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.GrowableD8$plus$eqL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8*, i8*) inlinehint

declare i8* @"scalanative_alloc_small"(i8*, i64)

declare i1 @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO"(i8*, i8*) inlinehint

declare nonnull dereferenceable(24) i8* @"_SM13scala.ProductD15productIteratorL25scala.collection.IteratorEO"(i8*)

declare dereferenceable_or_null(48) i8* @"_SM19java.nio.ByteBufferD8positioniL19java.nio.ByteBufferEO"(i8*, i32) inlinehint
@"_SM18java.lang.Integer$G8instance" = external constant { i8* }
@"_SM25java.nio.charset.Charset$G8instance" = external constant { i8* }
@"_SM19java.nio.CharBufferG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [17 x i8*] }
@"_ST17__class_has_trait" = external constant [657 x [122 x i1]]

declare nonnull dereferenceable(8) i8* @"_SM34scala.collection.mutable.CloneableD6$init$uEO"(i8*)

declare dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.StringL23java.lang.StringBuilderEO"(i8*, i8*)

declare i8* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO"(i8*, i32)

declare nonnull dereferenceable(16) i8* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8*, i8*) inlinehint
@"_ST10__dispatch" = external constant [4582 x i8*]

declare nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD9substringiL16java.lang.StringEO"(i8*, i32)

declare nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8*)
@"_SM22scala.math.ScalaNumberG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD5limitiL19java.nio.CharBufferEO"(i8*, i32) inlinehint
@"_SM34java.nio.charset.CodingErrorActionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"_SM31scala.util.hashing.MurmurHash3$D7mapHashL20scala.collection.MapiEO"(i8*, i8*)

declare dereferenceable_or_null(8) i8* @"_SM38scala.scalanative.runtime.ObjectArray$D5allociL37scala.scalanative.runtime.ObjectArrayEO"(i8*, i32) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM25scala.collection.IterableD8toStringL16java.lang.StringEO"(i8*)

declare i32 @"_SM17java.lang.IntegerD9compareToL17java.lang.IntegeriEO"(i8*, i8*) inlinehint

declare nonnull dereferenceable(16) i8* @"_SM13scala.Predef$D15boolean2BooleanzL17java.lang.BooleanEO"(i8*, i1)

declare nonnull dereferenceable(8) i8* @"_SM16java.lang.StringD11toCharArrayLAc_EO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM25java.nio.charset.Charset$D14defaultCharsetL24java.nio.charset.CharsetEO"(i8*)
@"_SM38scala.collection.mutable.StringBuilderG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(16) i8* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO"(i8*, i8*)

declare i32 @"_SM27scala.runtime.BoxesRunTime$D10unboxToIntL16java.lang.ObjectiEO"(i8*, i8*)

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.DoubleD4TYPEL15java.lang.ClassEo"() inlinehint

declare nonnull dereferenceable(8) i8* @"_SM33scala.collection.mutable.IterableD6$init$uEO"(i8*)

declare dereferenceable_or_null(8) i8* @"_SM38scala.scalanative.runtime.DoubleArray$D5allociL37scala.scalanative.runtime.DoubleArrayEO"(i8*, i32) inlinehint

declare dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8*, i8*) inlinehint
@"_SM37scala.collection.StringOps$$$Lambda$6G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8*, i32) inlinehint

declare dereferenceable_or_null(24) i8* @"_SM31scala.util.hashing.MurmurHash3$G4load"() noinline
@"_SM38scala.collection.StringOps$$$Lambda$14G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM18java.util.package$G8instance" = external constant { i8* }
@"_SM28scala.scalanative.unsafe.TagG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }
@"_SM27java.util.Formatter$$anon$1G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM15java.lang.ClassG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"_SM32scala.scalanative.unsigned.ULongD5toIntiEO"(i8*) inlinehint

declare dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8*, i32)
@"_SM27scala.runtime.ScalaRunTime$G8instance" = external constant { i8* }

declare nonnull dereferenceable(32) i8* @"_SM18java.lang.Integer$D8toStringiL16java.lang.StringEO"(i8*, i32)
@"_SM38scala.scalanative.runtime.ObjectArray$G8instance" = external constant { i8* }

declare dereferenceable_or_null(8) i8* @"_SM40scala.collection.immutable.Map$EmptyMap$G4load"() noinline

declare i32 @"_SM30scala.util.hashing.MurmurHash3D3mixiiiEO"(i8*, i32, i32)
@"_SM27java.util.Formatter$$anon$2G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM34scala.util.DynamicVariable$$anon$1D12initialValueL16java.lang.ObjectEO"(i8*)

declare dereferenceable_or_null(16) i8* @"_SM29java.nio.charset.CoderResult$D8OVERFLOWL28java.nio.charset.CoderResultEO"(i8*) alwaysinline

declare dereferenceable_or_null(48) i8* @"_SM19java.nio.ByteBufferD3putLAb_L19java.nio.ByteBufferEO"(i8*, i8*)

declare i1 @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO"(i8*, i8*)

declare nonnull dereferenceable(8) i8* @"_SM16scala.MatchErrorRL16java.lang.ObjectE"(i8*, i8*)

declare i32 @"_SM26scala.LowPriorityImplicitsD10intWrapperiiEO"(i8*, i32) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM15java.lang.ShortD4TYPEL15java.lang.ClassEo"() inlinehint

declare i64 @"_SM35scala.scalanative.runtime.LazyVals$D3getR_jEO"(i8*, i8*) alwaysinline
@"_SM32java.nio.BufferOverflowExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM21scala.collection.Map$D28$anonfun$DefaultSentinelFn$1L16java.lang.ObjectEPT21scala.collection.Map$"(i8*)

declare dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D25loadProperties$$anonfun$2L20java.util.PropertiesL16java.lang.StringL16java.lang.ObjectEPT17java.lang.System$"(i8*, i8*, i8*)

declare dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD8positioniL15java.nio.BufferEO"(i8*, i32) alwaysinline

declare i8* @"_SM28scala.runtime.Scala3RunTime$D12assertFailednEO"(i8*)
@"_SM24java.lang.AssertionErrorG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare i1 @"_SM16java.lang.StringD6equalsL16java.lang.ObjectzEO"(i8*, i8*)

declare dereferenceable_or_null(40) i8* @"_SM20java.nio.CharBuffer$D8allocateiL19java.nio.CharBufferEO"(i8*, i32)

declare nonnull dereferenceable(8) i8* @"_SM35scala.collection.mutable.IndexedSeqD6$init$uEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO"(i8*)
@"_SM19java.nio.GenBuffer$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM19java.nio.GenBuffer$G4type" to i8*) }
@"_SM27scala.collection.StringOps$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27scala.collection.StringOps$G4type" to i8*) }
@"_SM30scala.collection.IterableOnce$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM30scala.collection.IterableOnce$G4type" to i8*) }
@"_SM34scala.scalanative.runtime.Monitor$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM34scala.scalanative.runtime.Monitor$G4type" to i8*) }
@"_SM35scala.scalanative.unsafe.CFuncPtr6$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM35scala.scalanative.unsafe.CFuncPtr6$G4type" to i8*) }
@"_SM36scala.scalanative.unsafe.CFuncPtr14$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM36scala.scalanative.unsafe.CFuncPtr14$G4type" to i8*) }
@"_SM38java.util.package$CompareNullablesOps$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM38java.util.package$CompareNullablesOps$G4type" to i8*) }
@"_SM41scala.scalanative.posix.pwdOps$passwdOps$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM41scala.scalanative.posix.pwdOps$passwdOps$G4type" to i8*) }
@"_SM13java.util.MapG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -2, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG1-1" to i8*) }
@"_SM14java.io.ReaderG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 16, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG1-3" to i8*) }, i32 16, i32 18, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-4" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM14java.io.WriterG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 19, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG1-6" to i8*) }, i32 16, i32 20, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-4" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM14scala.Product2G4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -5, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG1-8" to i8*) }
@"_SM15java.nio.BufferG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 91, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-10" to i8*) }, i32 24, i32 96, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [12 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM15java.nio.BufferD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* null, i8* null, i8* null, i8* null, i8* null, i8* bitcast (i8* (i8*, i32)* @"_SM15java.nio.BufferD8positioniL15java.nio.BufferEO" to i8*), i8* null, i8* null ] }
@"_SM19java.io.InputStreamG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 123, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-13" to i8*) }, i32 8, i32 124, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM19java.nio.GenBuffer$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 176, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-15" to i8*) }, i32 8, i32 176, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM19java.util.FormatterG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 219, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-17" to i8*) }, i32 40, i32 219, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-18" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM23java.util.FormatterImplD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM20java.lang.AppendableG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -22, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-20" to i8*) }
@"_SM20scala.collection.MapG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -26, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-22" to i8*) }
@"_SM21java.lang.ThreadLocalG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 194, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-24" to i8*) }, i32 24, i32 196, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG2-25" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM21java.util.package$BoxG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 199, i32 61, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-27" to i8*) }, i32 16, i32 199, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-4" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM21java.util.package$BoxD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM21java.util.package$BoxD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM21java.util.package$BoxD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM21java.util.package$BoxD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM21scala.PartialFunctionG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -30, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-29" to i8*) }
@"_SM21scala.collection.ViewG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -31, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-31" to i8*) }
@"_SM23scala.collection.SeqOpsG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -38, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-33" to i8*) }
@"_SM24java.nio.charset.CharsetG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 231, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-35" to i8*) }, i32 32, i32 232, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG2-25" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM24java.nio.charset.CharsetD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM24java.nio.charset.CharsetD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM24java.nio.charset.CharsetD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM24java.nio.charset.CharsetD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM24scala.collection.SeqViewG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -42, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-37" to i8*) }
@"_SM25java.nio.StringCharBufferG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [17 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 96, i32 56, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-39" to i8*) }, i32 56, i32 96, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG2-40" to i8*) }, [17 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM19java.nio.CharBufferD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM25java.nio.StringCharBufferD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM19java.nio.CharBufferD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM19java.nio.CharBufferD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*, i32, i8*)* @"_SM25java.nio.StringCharBufferD5storeiL16java.lang.ObjectuEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.nio.CharBufferD6_arrayL16java.lang.ObjectEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM19java.nio.CharBufferD12_arrayOffsetiEO" to i8*), i8* bitcast (i8* (i8*, i32, i8*, i32, i32)* @"_SM25java.nio.StringCharBufferD5storeiL16java.lang.ObjectiiuEO" to i8*), i8* bitcast (i8* (i8*, i32, i8*, i32, i32)* @"_SM25java.nio.StringCharBufferD4loadiL16java.lang.ObjectiiuEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM19java.nio.CharBufferD8positioniL15java.nio.BufferEO" to i8*), i8* bitcast (i1 (i8*)* @"_SM25java.nio.StringCharBufferD10isReadOnlyzEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM25java.nio.StringCharBufferD4loadiL16java.lang.ObjectEO" to i8*), i8* bitcast (i8* (i8*, i8*, i32, i32)* @"_SM25java.nio.StringCharBufferD3getLAc_iiL19java.nio.CharBufferEO" to i8*), i8* bitcast (i16 (i8*, i32)* @"_SM25java.nio.StringCharBufferD3geticEO" to i8*), i8* bitcast (i16 (i8*)* @"_SM25java.nio.StringCharBufferD3getcEO" to i8*), i8* bitcast (i8* (i8*, i32, i32)* @"_SM25java.nio.StringCharBufferD11subSequenceiiL22java.lang.CharSequenceEO" to i8*), i8* bitcast (i8* (i8*, i16)* @"_SM25java.nio.StringCharBufferD3putcL19java.nio.CharBufferEO" to i8*) ] }
@"_SM25scala.reflect.OptManifestG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -47, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-42" to i8*) }
@"_SM27java.lang.StackTraceElementG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 244, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-44" to i8*) }, i32 40, i32 244, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-45" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM27java.lang.StackTraceElementD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM27java.lang.StackTraceElementD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM27java.lang.StackTraceElementD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM27java.lang.StackTraceElementD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM27java.lang.System$$$Lambda$3G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 247, i32 68, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-47" to i8*) }, i32 24, i32 247, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG2-25" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM27scala.collection.SeqView$IdG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 431, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-49" to i8*) }, i32 16, i32 432, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-4" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM29scala.collection.AbstractViewD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM27scala.collection.StringOps$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 258, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-51" to i8*) }, i32 8, i32 258, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM28java.lang.System$$$Lambda$15G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 266, i32 80, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-53" to i8*) }, i32 16, i32 266, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-4" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM28scala.collection.mutable.MapG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -54, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-55" to i8*) }
@"_SM29java.io.PrintStream$$Lambda$5G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 305, i32 96, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-57" to i8*) }, i32 24, i32 305, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG2-25" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM29java.lang.ArithmeticExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 145, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-59" to i8*) }, i32 40, i32 145, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-60" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM29scala.scalanative.libc.errno$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 316, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-62" to i8*) }, i32 8, i32 316, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM30java.lang.NullPointerExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 147, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-64" to i8*) }, i32 40, i32 147, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-60" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM30java.math.RoundingMode$$anon$4G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 26, i32 8, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-66" to i8*) }, i32 24, i32 26, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-67" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM30scala.collection.IterableOnce$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 325, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-69" to i8*) }, i32 8, i32 325, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM31java.nio.charset.CharsetDecoderG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 335, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-71" to i8*) }, i32 56, i32 336, { i8* } { i8* bitcast ([5 x i64]* @"_SM7__constG2-72" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM31java.nio.charset.CharsetEncoderG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 337, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-74" to i8*) }, i32 56, i32 338, { i8* } { i8* bitcast ([5 x i64]* @"_SM7__constG2-75" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM31scala.collection.Map$$$Lambda$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 343, i32 110, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-77" to i8*) }, i32 16, i32 343, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-4" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM31scala.collection.immutable.Map$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 344, i32 111, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-79" to i8*) }, i32 8, i32 344, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM31scala.deriving.Mirror$SingletonG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -68, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-81" to i8*) }
@"_SM32java.lang.ProcessBuilder$$anon$3G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 66, i32 41, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-83" to i8*) }, i32 24, i32 66, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-67" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM32scala.collection.IterableOnceOpsG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -71, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-85" to i8*) }
@"_SM33java.util.FormatterImpl$$Lambda$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 374, i32 113, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-87" to i8*) }, i32 24, i32 374, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG2-25" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM33scala.concurrent.ExecutionContextG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -77, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-89" to i8*) }
@"_SM34java.util.FormatterClosedExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 149, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-91" to i8*) }, i32 40, i32 149, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-60" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM34scala.runtime.function.JProcedure2G4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -81, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-93" to i8*) }
@"_SM34scala.scalanative.runtime.IntArrayG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 348, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-95" to i8*) }, i32 8, i32 348, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [8 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.IntArrayD6strideL32scala.scalanative.unsigned.ULongEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.IntArrayD5applyiL16java.lang.ObjectEO" to i8*), i8* bitcast (i8* (i8*, i32, i8*)* @"_SM34scala.scalanative.runtime.IntArrayD6updateiL16java.lang.ObjectuEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.IntArrayD5atRawiR_EO" to i8*) ] }
@"_SM34scala.scalanative.runtime.Monitor$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 498, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-97" to i8*) }, i32 8, i32 498, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM34scala.scalanative.unsafe.CFuncPtr7G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 474, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-99" to i8*) }, i32 16, i32 474, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM35java.nio.charset.CodingErrorAction$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 500, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-101" to i8*) }, i32 32, i32 500, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-60" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM35java.nio.file.AccessDeniedExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 135, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-103" to i8*) }, i32 64, i32 135, { i8* } { i8* bitcast ([7 x i64]* @"_SM7__constG3-104" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM33java.nio.file.FileSystemExceptionD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM35java.util.FormatterImpl$FormatTokenG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 501, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-106" to i8*) }, i32 48, i32 501, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-67" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM35scala.collection.MapFactoryDefaultsG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -84, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-108" to i8*) }
@"_SM35scala.scalanative.unsafe.CFuncPtr17G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 484, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-110" to i8*) }, i32 16, i32 484, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM35scala.scalanative.unsafe.CFuncPtr20G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 487, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-112" to i8*) }, i32 16, i32 487, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM35scala.scalanative.unsafe.CFuncPtr6$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 513, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-114" to i8*) }, i32 8, i32 513, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM36scala.collection.immutable.BigVectorG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 411, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-116" to i8*) }, i32 32, i32 417, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG3-117" to i8*) }, [6 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractSeqD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.collection.AbstractSeqD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.collection.AbstractSeqD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractSeqD6equalsL16java.lang.ObjectzEO" to i8*), i8* null, i8* null ] }
@"_SM36scala.scalanative.unsafe.CFuncPtr14$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 533, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-119" to i8*) }, i32 8, i32 533, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM37java.util.IllegalFormatWidthExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 157, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-121" to i8*) }, i32 40, i32 157, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-60" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM37java.util.IllegalFormatWidthExceptionD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM37java.util.concurrent.TimeUnit$$anon$2G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 42, i32 20, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-123" to i8*) }, i32 24, i32 42, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-67" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM37scala.collection.immutable.IndexedSeqG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -93, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-125" to i8*) }
@"_SM38java.util.package$CompareNullablesOps$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 562, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-127" to i8*) }, i32 8, i32 562, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM38scala.collection.mutable.IndexedBufferG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -99, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-129" to i8*) }
@"_SM39scala.collection.immutable.LinearSeqOpsG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -105, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-131" to i8*) }
@"_SM39scala.scalanative.runtime.PrimitiveByteG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 576, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-133" to i8*) }, i32 8, i32 576, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM40java.lang.ArrayIndexOutOfBoundsExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 167, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-135" to i8*) }, i32 40, i32 167, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-60" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM40java.nio.file.StandardOpenOption$$anon$2G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 54, i32 30, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-137" to i8*) }, i32 24, i32 54, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-67" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM40scala.collection.mutable.GrowableBuilderG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 583, i32 210, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-139" to i8*) }, i32 16, i32 584, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-4" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM41java.nio.file.StandardOpenOption$$anon$10G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 62, i32 38, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-141" to i8*) }, i32 24, i32 62, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-67" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM41java.util.Formatter$BigDecimalLayoutForm$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 587, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-143" to i8*) }, i32 32, i32 587, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-60" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM41scala.scalanative.posix.pwdOps$passwdOps$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 598, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-145" to i8*) }, i32 8, i32 598, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM42java.util.IllegalFormatConversionExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 163, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-147" to i8*) }, i32 56, i32 163, { i8* } { i8* bitcast ([5 x i64]* @"_SM7__constG3-148" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM42java.util.IllegalFormatConversionExceptionD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM42java.util.UnknownFormatConversionExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 164, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-150" to i8*) }, i32 48, i32 164, { i8* } { i8* bitcast ([5 x i64]* @"_SM7__constG3-148" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM42java.util.UnknownFormatConversionExceptionD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM42scala.scalanative.runtime.PrimitiveBooleanG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 605, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-152" to i8*) }, i32 8, i32 605, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM43java.util.FormatterImpl$ParserStateMachine$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 612, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-154" to i8*) }, i32 16, i32 612, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG2-11" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM43scala.reflect.ManifestFactory$FloatManifestG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 285, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-156" to i8*) }, i32 24, i32 286, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-67" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.reflect.AnyValManifestD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.reflect.AnyValManifestD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM43scala.reflect.ManifestFactory$ShortManifestG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 287, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-158" to i8*) }, i32 24, i32 288, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-67" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.reflect.AnyValManifestD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.reflect.AnyValManifestD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM44scala.collection.mutable.ArrayBuffer$$anon$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 584, i32 211, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-160" to i8*) }, i32 16, i32 584, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-4" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM44scala.collection.mutable.ArraySeq$$$Lambda$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 618, i32 223, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-162" to i8*) }, i32 16, i32 618, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-4" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM44scala.reflect.ManifestFactory$DoubleManifestG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 289, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-164" to i8*) }, i32 24, i32 290, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-67" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.reflect.AnyValManifestD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.reflect.AnyValManifestD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM45java.util.ScalaOps$JavaIteratorOps$$$Lambda$2G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 620, i32 224, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-166" to i8*) }, i32 24, i32 620, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG2-25" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM50scala.collection.ClassTagSeqFactory$AnySeqDelegateG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 642, i32 234, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-168" to i8*) }, i32 16, i32 642, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-4" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM50scala.collection.StrictOptimizedClassTagSeqFactoryG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -122, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-170" to i8*) }
@"_SM51java.nio.file.attribute.PosixFilePermission$$anon$6G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 78, i32 51, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-172" to i8*) }, i32 24, i32 78, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-67" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM66scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$$$Lambda$3G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 653, i32 242, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-174" to i8*) }, i32 56, i32 653, { i8* } { i8* bitcast ([5 x i64]* @"_SM7__constG3-175" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }

declare i32 @"scalanative_errno"()

define dereferenceable_or_null(16) i8* @"_SM14java.io.ReaderD17$init$$$anonfun$1L14java.io.ReaderEPT14java.io.Reader"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* %_1
}

define dereferenceable_or_null(16) i8* @"_SM14java.io.WriterD17$init$$$anonfun$1L14java.io.WriterEPT14java.io.Writer"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* %_1
}

define nonnull dereferenceable(8) i8* @"_SM14java.io.WriterD5writeL16java.lang.StringuEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30004 = bitcast i8* bitcast (i8* (i8*)* @"_SM16java.lang.StringD11toCharArrayLAc_EO" to i8*) to i8* (i8*)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30004(i8* dereferenceable_or_null(32) %_2)
  call nonnull dereferenceable(8) i8* @"_SM14java.io.WriterD5writeLAc_uEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_30001)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(8) i8* @"_SM14java.io.WriterD5writeLAc_uEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30005 = icmp ne i8* %_2, null
  br i1 %_30005, label %_30003.0, label %_30004.0
_30003.0:
  %_30009 = bitcast i8* %_2 to { i8*, i32 }*
  %_30010 = getelementptr { i8*, i32 }, { i8*, i32 }* %_30009, i32 0, i32 1
  %_30006 = bitcast i32* %_30010 to i8*
  %_30011 = bitcast i8* %_30006 to i32*
  %_30001 = load i32, i32* %_30011
  %_30012 = bitcast i8* bitcast (i8* (i8*, i8*, i32, i32)* @"_SM26java.io.OutputStreamWriterD5writeLAc_iiuEO" to i8*) to i8* (i8*, i8*, i32, i32)*
  call nonnull dereferenceable(8) i8* %_30012(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_2, i32 0, i32 %_30001)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_30004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM14java.io.WriterD6appendL22java.lang.CharSequenceL14java.io.WriterEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_2, null
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  %_60005 = icmp ne i8* %_2, null
  br i1 %_60005, label %_60003.0, label %_60004.0
_60003.0:
  %_60010 = bitcast i8* %_2 to i8**
  %_60006 = load i8*, i8** %_60010
  %_60011 = bitcast i8* %_60006 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_60012 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_60011, i32 0, i32 4, i32 1
  %_60007 = bitcast i8** %_60012 to i8*
  %_60013 = bitcast i8* %_60007 to i8**
  %_50002 = load i8*, i8** %_60013
  %_60014 = bitcast i8* %_50002 to i8* (i8*)*
  %_50003 = call dereferenceable_or_null(32) i8* %_60014(i8* dereferenceable_or_null(8) %_2)
  br label %_60000.0
_60000.0:
  %_60001 = phi i8* [%_50003, %_60003.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_40000.0]
  call nonnull dereferenceable(8) i8* @"_SM14java.io.WriterD5writeL16java.lang.StringuEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(32) %_60001)
  ret i8* %_1
_60004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM14java.io.WriterD6appendL22java.lang.CharSequenceL20java.lang.AppendableEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(16) i8* @"_SM14java.io.WriterD6appendL22java.lang.CharSequenceL14java.io.WriterEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define i32 @"_SM14scala.Product2D12productArityiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i32 2
}

define dereferenceable_or_null(8) i8* @"_SM14scala.Product2D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  switch i32 %_2, label %_40000.0 [
    i32 0, label %_50000.0
    i32 1, label %_60000.0
  ]
_40000.0:
  %_40005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*), null
  br i1 %_40005, label %_70000.0, label %_80000.0
_70000.0:
  br label %_90000.0
_80000.0:
  br label %_90000.0
_90000.0:
  %_90001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*), %_80000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_70000.0]
  %_90004 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_2)
  %_90005 = icmp eq i8* %_90004, null
  br i1 %_90005, label %_100000.0, label %_110000.0
_100000.0:
  br label %_120000.0
_110000.0:
  %_310018 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.lang.IntegerD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_110001 = call dereferenceable_or_null(32) i8* %_310018(i8* nonnull dereferenceable(16) %_90004)
  br label %_120000.0
_120000.0:
  %_120001 = phi i8* [%_110001, %_110000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_100000.0]
  %_310019 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_120002 = call dereferenceable_or_null(32) i8* %_310019(i8* nonnull dereferenceable(32) %_90001, i8* dereferenceable_or_null(32) %_120001)
  %_120004 = icmp eq i8* %_120002, null
  br i1 %_120004, label %_130000.0, label %_140000.0
_130000.0:
  br label %_150000.0
_140000.0:
  br label %_150000.0
_150000.0:
  %_150001 = phi i8* [%_120002, %_140000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_130000.0]
  %_150005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-181" to i8*), null
  br i1 %_150005, label %_160000.0, label %_170000.0
_160000.0:
  br label %_180000.0
_170000.0:
  br label %_180000.0
_50000.0:
  %_290001 = bitcast i8* %_1 to i8*
  %_310005 = icmp ne i8* %_290001, null
  br i1 %_310005, label %_310003.0, label %_310004.0
_310003.0:
  %_310020 = bitcast i8* %_290001 to { i8*, i8*, i8* }*
  %_310021 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_310020, i32 0, i32 2
  %_310006 = bitcast i8** %_310021 to i8*
  %_310022 = bitcast i8* %_310006 to i8**
  %_290002 = load i8*, i8** %_310022, !dereferenceable_or_null !{i64 8}
  br label %_300000.0
_60000.0:
  %_310001 = bitcast i8* %_1 to i8*
  %_310008 = icmp ne i8* %_310001, null
  br i1 %_310008, label %_310007.0, label %_310004.0
_310007.0:
  %_310023 = bitcast i8* %_310001 to { i8*, i8*, i8* }*
  %_310024 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_310023, i32 0, i32 1
  %_310009 = bitcast i8** %_310024 to i8*
  %_310025 = bitcast i8* %_310009 to i8**
  %_310002 = load i8*, i8** %_310025, !dereferenceable_or_null !{i64 8}
  br label %_300000.0
_300000.0:
  %_300001 = phi i8* [%_310002, %_310007.0], [%_290002, %_310003.0]
  ret i8* %_300001
_180000.0:
  %_180001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-181" to i8*), %_170000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_160000.0]
  %_310026 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_180002 = call dereferenceable_or_null(32) i8* %_310026(i8* dereferenceable_or_null(32) %_150001, i8* nonnull dereferenceable(32) %_180001)
  br label %_270000.0
_270000.0:
  %_270001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM35java.lang.IndexOutOfBoundsExceptionG4type" to i8*), i64 40)
  %_310027 = bitcast i8* %_270001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_310028 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_310027, i32 0, i32 5
  %_310011 = bitcast i1* %_310028 to i8*
  %_310029 = bitcast i8* %_310011 to i1*
  store i1 true, i1* %_310029
  %_310030 = bitcast i8* %_270001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_310031 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_310030, i32 0, i32 4
  %_310013 = bitcast i1* %_310031 to i8*
  %_310032 = bitcast i8* %_310013 to i1*
  store i1 true, i1* %_310032
  %_310033 = bitcast i8* %_270001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_310034 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_310033, i32 0, i32 3
  %_310015 = bitcast i8** %_310034 to i8*
  %_310035 = bitcast i8* %_310015 to i8**
  store i8* %_180002, i8** %_310035
  %_270005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_270001)
  br label %_280000.0
_280000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_270001)
  unreachable
_310004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM14scala.Product2D6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM15java.nio.BufferD12hasRemainingzEO"(i8* %_1) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM15java.nio.BufferD8positioniEO"(i8* dereferenceable_or_null(24) %_1)
  %_20002 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_1)
  %_20004 = icmp ne i32 %_20001, %_20002
  ret i1 %_20004
}

define i32 @"_SM15java.nio.BufferD13validateIndexiiEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp slt i32 %_2, 0
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  %_50001 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_1)
  %_50003 = icmp sge i32 %_2, %_50001
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_50003, %_50000.0], [true, %_40000.0]
  br i1 %_60001, label %_70000.0, label %_80000.0
_80000.0:
  br label %_200000.0
_200000.0:
  ret i32 %_2
_70000.0:
  br label %_180000.0
_180000.0:
  %_180001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM35java.lang.IndexOutOfBoundsExceptionG4type" to i8*), i64 40)
  %_200006 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_200007 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_200006, i32 0, i32 5
  %_200002 = bitcast i1* %_200007 to i8*
  %_200008 = bitcast i8* %_200002 to i1*
  store i1 true, i1* %_200008
  %_200009 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_200010 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_200009, i32 0, i32 4
  %_200004 = bitcast i1* %_200010 to i8*
  %_200011 = bitcast i8* %_200004 to i1*
  store i1 true, i1* %_200011
  %_180004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_180001)
  br label %_190000.0
_190000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_180001)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM15java.nio.BufferD17ensureNotReadOnlyuEO"(i8* %_1) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_160003 = icmp ne i8* %_1, null
  br i1 %_160003, label %_160001.0, label %_160002.0
_160001.0:
  %_160012 = bitcast i8* %_1 to i8**
  %_160004 = load i8*, i8** %_160012
  %_160013 = bitcast i8* %_160004 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_160014 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_160013, i32 0, i32 4, i32 10
  %_160005 = bitcast i8** %_160014 to i8*
  %_160015 = bitcast i8* %_160005 to i8**
  %_20002 = load i8*, i8** %_160015
  %_160016 = bitcast i8* %_20002 to i1 (i8*)*
  %_20003 = call i1 %_160016(i8* dereferenceable_or_null(24) %_1)
  br i1 %_20003, label %_30000.0, label %_40000.0
_40000.0:
  br label %_160000.0
_160000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_30000.0:
  br label %_140000.0
_140000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.ReadOnlyBufferExceptionG4type" to i8*), i64 40)
  %_160017 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160018 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160017, i32 0, i32 5
  %_160007 = bitcast i1* %_160018 to i8*
  %_160019 = bitcast i8* %_160007 to i1*
  store i1 true, i1* %_160019
  %_160020 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160021 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160020, i32 0, i32 4
  %_160009 = bitcast i1* %_160021 to i8*
  %_160022 = bitcast i8* %_160009 to i1*
  store i1 true, i1* %_160022
  %_140004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_140001)
  br label %_150000.0
_150000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_140001)
  unreachable
_160002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM15java.nio.BufferD20getPosAndAdvanceReadiEO"(i8* %_1) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_150006 = icmp ne i8* %_1, null
  br i1 %_150006, label %_150004.0, label %_150005.0
_150004.0:
  %_150018 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_150019 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_150018, i32 0, i32 3
  %_150007 = bitcast i32* %_150019 to i8*
  %_150020 = bitcast i8* %_150007 to i32*
  %_20001 = load i32, i32* %_150020
  %_20002 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_1)
  %_20004 = icmp eq i32 %_20001, %_20002
  br i1 %_20004, label %_30000.0, label %_40000.0
_40000.0:
  br label %_150000.0
_150000.0:
  %_150002 = add i32 %_20001, 1
  %_150010 = icmp ne i8* %_1, null
  br i1 %_150010, label %_150009.0, label %_150005.0
_150009.0:
  %_150021 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_150022 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_150021, i32 0, i32 3
  %_150011 = bitcast i32* %_150022 to i8*
  %_150023 = bitcast i8* %_150011 to i32*
  store i32 %_150002, i32* %_150023
  ret i32 %_20001
_30000.0:
  br label %_130000.0
_130000.0:
  %_130001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM33java.nio.BufferUnderflowExceptionG4type" to i8*), i64 40)
  %_150024 = bitcast i8* %_130001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_150025 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_150024, i32 0, i32 5
  %_150013 = bitcast i1* %_150025 to i8*
  %_150026 = bitcast i8* %_150013 to i1*
  store i1 true, i1* %_150026
  %_150027 = bitcast i8* %_130001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_150028 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_150027, i32 0, i32 4
  %_150015 = bitcast i1* %_150028 to i8*
  %_150029 = bitcast i8* %_150015 to i1*
  store i1 true, i1* %_150029
  %_130004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_130001)
  br label %_140000.0
_140000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_130001)
  unreachable
_150005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM15java.nio.BufferD20getPosAndAdvanceReadiiEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_160004 = icmp ne i8* %_1, null
  br i1 %_160004, label %_160002.0, label %_160003.0
_160002.0:
  %_160016 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_160017 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_160016, i32 0, i32 3
  %_160005 = bitcast i32* %_160017 to i8*
  %_160018 = bitcast i8* %_160005 to i32*
  %_30001 = load i32, i32* %_160018
  %_30003 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_1)
  %_30005 = add i32 %_30001, %_2
  %_30006 = icmp sgt i32 %_30005, %_30003
  br i1 %_30006, label %_40000.0, label %_50000.0
_50000.0:
  br label %_160000.0
_160000.0:
  %_160008 = icmp ne i8* %_1, null
  br i1 %_160008, label %_160007.0, label %_160003.0
_160007.0:
  %_160019 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_160020 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_160019, i32 0, i32 3
  %_160009 = bitcast i32* %_160020 to i8*
  %_160021 = bitcast i8* %_160009 to i32*
  store i32 %_30005, i32* %_160021
  ret i32 %_30001
_40000.0:
  br label %_140000.0
_140000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM33java.nio.BufferUnderflowExceptionG4type" to i8*), i64 40)
  %_160022 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160023 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160022, i32 0, i32 5
  %_160011 = bitcast i1* %_160023 to i8*
  %_160024 = bitcast i8* %_160011 to i1*
  store i1 true, i1* %_160024
  %_160025 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160026 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160025, i32 0, i32 4
  %_160013 = bitcast i1* %_160026 to i8*
  %_160027 = bitcast i8* %_160013 to i1*
  store i1 true, i1* %_160027
  %_140004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_140001)
  br label %_150000.0
_150000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_140001)
  unreachable
_160003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM15java.nio.BufferD21getPosAndAdvanceWriteiEO"(i8* %_1) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_150006 = icmp ne i8* %_1, null
  br i1 %_150006, label %_150004.0, label %_150005.0
_150004.0:
  %_150018 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_150019 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_150018, i32 0, i32 3
  %_150007 = bitcast i32* %_150019 to i8*
  %_150020 = bitcast i8* %_150007 to i32*
  %_20001 = load i32, i32* %_150020
  %_20002 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_1)
  %_20004 = icmp eq i32 %_20001, %_20002
  br i1 %_20004, label %_30000.0, label %_40000.0
_40000.0:
  br label %_150000.0
_150000.0:
  %_150002 = add i32 %_20001, 1
  %_150010 = icmp ne i8* %_1, null
  br i1 %_150010, label %_150009.0, label %_150005.0
_150009.0:
  %_150021 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_150022 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_150021, i32 0, i32 3
  %_150011 = bitcast i32* %_150022 to i8*
  %_150023 = bitcast i8* %_150011 to i32*
  store i32 %_150002, i32* %_150023
  ret i32 %_20001
_30000.0:
  br label %_130000.0
_130000.0:
  %_130001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.BufferOverflowExceptionG4type" to i8*), i64 40)
  %_150024 = bitcast i8* %_130001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_150025 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_150024, i32 0, i32 5
  %_150013 = bitcast i1* %_150025 to i8*
  %_150026 = bitcast i8* %_150013 to i1*
  store i1 true, i1* %_150026
  %_150027 = bitcast i8* %_130001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_150028 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_150027, i32 0, i32 4
  %_150015 = bitcast i1* %_150028 to i8*
  %_150029 = bitcast i8* %_150015 to i1*
  store i1 true, i1* %_150029
  %_130004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_130001)
  br label %_140000.0
_140000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_130001)
  unreachable
_150005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM15java.nio.BufferD21getPosAndAdvanceWriteiiEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_160004 = icmp ne i8* %_1, null
  br i1 %_160004, label %_160002.0, label %_160003.0
_160002.0:
  %_160016 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_160017 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_160016, i32 0, i32 3
  %_160005 = bitcast i32* %_160017 to i8*
  %_160018 = bitcast i8* %_160005 to i32*
  %_30001 = load i32, i32* %_160018
  %_30003 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_1)
  %_30005 = add i32 %_30001, %_2
  %_30006 = icmp sgt i32 %_30005, %_30003
  br i1 %_30006, label %_40000.0, label %_50000.0
_50000.0:
  br label %_160000.0
_160000.0:
  %_160008 = icmp ne i8* %_1, null
  br i1 %_160008, label %_160007.0, label %_160003.0
_160007.0:
  %_160019 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_160020 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_160019, i32 0, i32 3
  %_160009 = bitcast i32* %_160020 to i8*
  %_160021 = bitcast i8* %_160009 to i32*
  store i32 %_30005, i32* %_160021
  ret i32 %_30001
_40000.0:
  br label %_140000.0
_140000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.BufferOverflowExceptionG4type" to i8*), i64 40)
  %_160022 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160023 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160022, i32 0, i32 5
  %_160011 = bitcast i1* %_160023 to i8*
  %_160024 = bitcast i8* %_160011 to i1*
  store i1 true, i1* %_160024
  %_160025 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160026 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160025, i32 0, i32 4
  %_160013 = bitcast i1* %_160026 to i8*
  %_160027 = bitcast i8* %_160013 to i1*
  store i1 true, i1* %_160027
  %_140004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_140001)
  br label %_150000.0
_150000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_140001)
  unreachable
_160003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM15java.nio.BufferD23validateArrayIndexRangeL16java.lang.ObjectiiuEO"(i8* %_1, i8* %_2, i32 %_3, i32 %_4) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_50002 = icmp slt i32 %_3, 0
  br i1 %_50002, label %_60000.0, label %_70000.0
_60000.0:
  br label %_80000.0
_70000.0:
  %_70002 = icmp slt i32 %_4, 0
  br label %_80000.0
_80000.0:
  %_80001 = phi i1 [%_70002, %_70000.0], [true, %_60000.0]
  br i1 %_80001, label %_90000.0, label %_100000.0
_90000.0:
  br label %_110000.0
_100000.0:
  %_100002 = call i32 @"_SM27scala.runtime.ScalaRunTime$D12array_lengthL16java.lang.ObjectiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(8) %_2)
  %_100005 = sub i32 %_100002, %_4
  %_100006 = icmp sgt i32 %_3, %_100005
  br label %_110000.0
_110000.0:
  %_110001 = phi i1 [%_100006, %_100000.0], [true, %_90000.0]
  br i1 %_110001, label %_120000.0, label %_130000.0
_130000.0:
  br label %_250000.0
_250000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_120000.0:
  br label %_230000.0
_230000.0:
  %_230001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM35java.lang.IndexOutOfBoundsExceptionG4type" to i8*), i64 40)
  %_250006 = bitcast i8* %_230001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_250007 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_250006, i32 0, i32 5
  %_250002 = bitcast i1* %_250007 to i8*
  %_250008 = bitcast i8* %_250002 to i1*
  store i1 true, i1* %_250008
  %_250009 = bitcast i8* %_230001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_250010 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_250009, i32 0, i32 4
  %_250004 = bitcast i1* %_250010 to i8*
  %_250011 = bitcast i8* %_250004 to i1*
  store i1 true, i1* %_250011
  %_230004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_230001)
  br label %_240000.0
_240000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_230001)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM15java.nio.BufferD4flipL15java.nio.BufferEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30005 = icmp ne i8* %_1, null
  br i1 %_30005, label %_30003.0, label %_30004.0
_30003.0:
  %_30019 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_30020 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_30019, i32 0, i32 4
  %_30006 = bitcast i32* %_30020 to i8*
  %_30021 = bitcast i8* %_30006 to i32*
  store i32 -1, i32* %_30021
  %_30008 = icmp ne i8* %_1, null
  br i1 %_30008, label %_30007.0, label %_30004.0
_30007.0:
  %_30022 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_30023 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_30022, i32 0, i32 3
  %_30009 = bitcast i32* %_30023 to i8*
  %_30024 = bitcast i8* %_30009 to i32*
  %_20001 = load i32, i32* %_30024
  %_30012 = icmp ne i8* %_1, null
  br i1 %_30012, label %_30011.0, label %_30004.0
_30011.0:
  %_30025 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_30026 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_30025, i32 0, i32 1
  %_30013 = bitcast i32* %_30026 to i8*
  %_30027 = bitcast i8* %_30013 to i32*
  store i32 %_20001, i32* %_30027
  %_30016 = icmp ne i8* %_1, null
  br i1 %_30016, label %_30015.0, label %_30004.0
_30015.0:
  %_30028 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_30029 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_30028, i32 0, i32 3
  %_30017 = bitcast i32* %_30029 to i8*
  %_30030 = bitcast i8* %_30017 to i32*
  store i32 0, i32* %_30030
  ret i8* %_1
_30004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM15java.nio.BufferD5clearL15java.nio.BufferEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30005 = icmp ne i8* %_1, null
  br i1 %_30005, label %_30003.0, label %_30004.0
_30003.0:
  %_30016 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_30017 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_30016, i32 0, i32 4
  %_30006 = bitcast i32* %_30017 to i8*
  %_30018 = bitcast i8* %_30006 to i32*
  store i32 -1, i32* %_30018
  %_30009 = icmp ne i8* %_1, null
  br i1 %_30009, label %_30008.0, label %_30004.0
_30008.0:
  %_30019 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_30020 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_30019, i32 0, i32 3
  %_30010 = bitcast i32* %_30020 to i8*
  %_30021 = bitcast i8* %_30010 to i32*
  store i32 0, i32* %_30021
  %_20002 = call i32 @"_SM15java.nio.BufferD8capacityiEO"(i8* dereferenceable_or_null(24) %_1)
  %_30013 = icmp ne i8* %_1, null
  br i1 %_30013, label %_30012.0, label %_30004.0
_30012.0:
  %_30022 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_30023 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_30022, i32 0, i32 1
  %_30014 = bitcast i32* %_30023 to i8*
  %_30024 = bitcast i8* %_30014 to i32*
  store i32 %_20002, i32* %_30024
  ret i8* %_1
_30004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM15java.nio.BufferD5limitiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_20008 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_20007, i32 0, i32 1
  %_20005 = bitcast i32* %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i32*
  %_20001 = load i32, i32* %_20009
  ret i32 %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM15java.nio.BufferD5limitiL15java.nio.BufferEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp slt i32 %_2, 0
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  %_50001 = call i32 @"_SM15java.nio.BufferD8capacityiEO"(i8* dereferenceable_or_null(24) %_1)
  %_50003 = icmp sgt i32 %_2, %_50001
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_50003, %_50000.0], [true, %_40000.0]
  br i1 %_60001, label %_70000.0, label %_80000.0
_80000.0:
  br label %_190000.0
_190000.0:
  %_270004 = icmp ne i8* %_1, null
  br i1 %_270004, label %_270002.0, label %_270003.0
_270002.0:
  %_270026 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_270027 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_270026, i32 0, i32 1
  %_270005 = bitcast i32* %_270027 to i8*
  %_270028 = bitcast i8* %_270005 to i32*
  store i32 %_2, i32* %_270028
  %_270007 = icmp ne i8* %_1, null
  br i1 %_270007, label %_270006.0, label %_270003.0
_270006.0:
  %_270029 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_270030 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_270029, i32 0, i32 3
  %_270008 = bitcast i32* %_270030 to i8*
  %_270031 = bitcast i8* %_270008 to i32*
  %_190002 = load i32, i32* %_270031
  %_190004 = icmp sgt i32 %_190002, %_2
  br i1 %_190004, label %_200000.0, label %_210000.0
_200000.0:
  %_270011 = icmp ne i8* %_1, null
  br i1 %_270011, label %_270010.0, label %_270003.0
_270010.0:
  %_270032 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_270033 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_270032, i32 0, i32 3
  %_270012 = bitcast i32* %_270033 to i8*
  %_270034 = bitcast i8* %_270012 to i32*
  store i32 %_2, i32* %_270034
  %_270014 = icmp ne i8* %_1, null
  br i1 %_270014, label %_270013.0, label %_270003.0
_270013.0:
  %_270035 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_270036 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_270035, i32 0, i32 4
  %_270015 = bitcast i32* %_270036 to i8*
  %_270037 = bitcast i8* %_270015 to i32*
  %_220001 = load i32, i32* %_270037
  %_200003 = icmp sgt i32 %_220001, %_2
  br i1 %_200003, label %_230000.0, label %_240000.0
_230000.0:
  %_270018 = icmp ne i8* %_1, null
  br i1 %_270018, label %_270017.0, label %_270003.0
_270017.0:
  %_270038 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_270039 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_270038, i32 0, i32 4
  %_270019 = bitcast i32* %_270039 to i8*
  %_270040 = bitcast i8* %_270019 to i32*
  store i32 -1, i32* %_270040
  br label %_260000.0
_240000.0:
  br label %_260000.0
_260000.0:
  br label %_270000.0
_210000.0:
  br label %_270000.0
_270000.0:
  ret i8* %_1
_70000.0:
  br label %_170000.0
_170000.0:
  %_170001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM34java.lang.IllegalArgumentExceptionG4type" to i8*), i64 40)
  %_270041 = bitcast i8* %_170001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_270042 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_270041, i32 0, i32 5
  %_270021 = bitcast i1* %_270042 to i8*
  %_270043 = bitcast i8* %_270021 to i1*
  store i1 true, i1* %_270043
  %_270044 = bitcast i8* %_170001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_270045 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_270044, i32 0, i32 4
  %_270023 = bitcast i1* %_270045 to i8*
  %_270046 = bitcast i8* %_270023 to i1*
  store i1 true, i1* %_270046
  %_170004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_170001)
  br label %_180000.0
_180000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_170001)
  unreachable
_270003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM15java.nio.BufferD6rewindL15java.nio.BufferEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30005 = icmp ne i8* %_1, null
  br i1 %_30005, label %_30003.0, label %_30004.0
_30003.0:
  %_30012 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_30013 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_30012, i32 0, i32 4
  %_30006 = bitcast i32* %_30013 to i8*
  %_30014 = bitcast i8* %_30006 to i32*
  store i32 -1, i32* %_30014
  %_30009 = icmp ne i8* %_1, null
  br i1 %_30009, label %_30008.0, label %_30004.0
_30008.0:
  %_30015 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_30016 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_30015, i32 0, i32 3
  %_30010 = bitcast i32* %_30016 to i8*
  %_30017 = bitcast i8* %_30010 to i32*
  store i32 0, i32* %_30017
  ret i8* %_1
_30004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM15java.nio.BufferD8capacityiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_30008 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_30007, i32 0, i32 2
  %_30005 = bitcast i32* %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i32*
  %_30001 = load i32, i32* %_30009
  ret i32 %_30001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM15java.nio.BufferD8positioniEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_20008 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_20007, i32 0, i32 3
  %_20005 = bitcast i32* %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i32*
  %_20001 = load i32, i32* %_20009
  ret i32 %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM15java.nio.BufferD8positioniL15java.nio.BufferEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp slt i32 %_2, 0
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  %_50001 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_1)
  %_50003 = icmp sgt i32 %_2, %_50001
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_50003, %_50000.0], [true, %_40000.0]
  br i1 %_60001, label %_70000.0, label %_80000.0
_80000.0:
  br label %_190000.0
_190000.0:
  %_240004 = icmp ne i8* %_1, null
  br i1 %_240004, label %_240002.0, label %_240003.0
_240002.0:
  %_240019 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_240020 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_240019, i32 0, i32 3
  %_240005 = bitcast i32* %_240020 to i8*
  %_240021 = bitcast i8* %_240005 to i32*
  store i32 %_2, i32* %_240021
  %_240007 = icmp ne i8* %_1, null
  br i1 %_240007, label %_240006.0, label %_240003.0
_240006.0:
  %_240022 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_240023 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_240022, i32 0, i32 4
  %_240008 = bitcast i32* %_240023 to i8*
  %_240024 = bitcast i8* %_240008 to i32*
  %_200001 = load i32, i32* %_240024
  %_190003 = icmp sgt i32 %_200001, %_2
  br i1 %_190003, label %_210000.0, label %_220000.0
_210000.0:
  %_240011 = icmp ne i8* %_1, null
  br i1 %_240011, label %_240010.0, label %_240003.0
_240010.0:
  %_240025 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32 }*
  %_240026 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_240025, i32 0, i32 4
  %_240012 = bitcast i32* %_240026 to i8*
  %_240027 = bitcast i8* %_240012 to i32*
  store i32 -1, i32* %_240027
  br label %_240000.0
_220000.0:
  br label %_240000.0
_240000.0:
  ret i8* %_1
_70000.0:
  br label %_170000.0
_170000.0:
  %_170001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM34java.lang.IllegalArgumentExceptionG4type" to i8*), i64 40)
  %_240028 = bitcast i8* %_170001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_240029 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_240028, i32 0, i32 5
  %_240014 = bitcast i1* %_240029 to i8*
  %_240030 = bitcast i8* %_240014 to i1*
  store i1 true, i1* %_240030
  %_240031 = bitcast i8* %_170001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_240032 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_240031, i32 0, i32 4
  %_240016 = bitcast i1* %_240032 to i8*
  %_240033 = bitcast i8* %_240016 to i1*
  store i1 true, i1* %_240033
  %_170004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_170001)
  br label %_180000.0
_180000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_170001)
  unreachable
_240003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM15java.nio.BufferD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*), null
  br i1 %_20004, label %_30000.0, label %_40000.0
_30000.0:
  br label %_50000.0
_40000.0:
  br label %_50000.0
_50000.0:
  %_50001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*), %_40000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_30000.0]
  %_50002 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.ObjectD8getClassL15java.lang.ClassEO"(i8* dereferenceable_or_null(24) %_1)
  %_50003 = call dereferenceable_or_null(32) i8* @"_SM15java.lang.ClassD7getNameL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_50002)
  %_50005 = icmp eq i8* %_50003, null
  br i1 %_50005, label %_60000.0, label %_70000.0
_60000.0:
  br label %_80000.0
_70000.0:
  br label %_80000.0
_80000.0:
  %_80001 = phi i8* [%_50003, %_70000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_60000.0]
  %_500003 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_80002 = call dereferenceable_or_null(32) i8* %_500003(i8* nonnull dereferenceable(32) %_50001, i8* dereferenceable_or_null(32) %_80001)
  %_80004 = icmp eq i8* %_80002, null
  br i1 %_80004, label %_90000.0, label %_100000.0
_90000.0:
  br label %_110000.0
_100000.0:
  br label %_110000.0
_110000.0:
  %_110001 = phi i8* [%_80002, %_100000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_90000.0]
  %_110005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-183" to i8*), null
  br i1 %_110005, label %_120000.0, label %_130000.0
_120000.0:
  br label %_140000.0
_130000.0:
  br label %_140000.0
_140000.0:
  %_140001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-183" to i8*), %_130000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_120000.0]
  %_500004 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_140002 = call dereferenceable_or_null(32) i8* %_500004(i8* dereferenceable_or_null(32) %_110001, i8* nonnull dereferenceable(32) %_140001)
  %_140004 = icmp eq i8* %_140002, null
  br i1 %_140004, label %_150000.0, label %_160000.0
_150000.0:
  br label %_170000.0
_160000.0:
  br label %_170000.0
_170000.0:
  %_170001 = phi i8* [%_140002, %_160000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_150000.0]
  %_170002 = call i32 @"_SM15java.nio.BufferD8positioniEO"(i8* dereferenceable_or_null(24) %_1)
  %_170005 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_170002)
  %_170006 = icmp eq i8* %_170005, null
  br i1 %_170006, label %_180000.0, label %_190000.0
_180000.0:
  br label %_200000.0
_190000.0:
  %_500005 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.lang.IntegerD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_190001 = call dereferenceable_or_null(32) i8* %_500005(i8* nonnull dereferenceable(16) %_170005)
  br label %_200000.0
_200000.0:
  %_200001 = phi i8* [%_190001, %_190000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_180000.0]
  %_500006 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_200002 = call dereferenceable_or_null(32) i8* %_500006(i8* dereferenceable_or_null(32) %_170001, i8* dereferenceable_or_null(32) %_200001)
  %_200004 = icmp eq i8* %_200002, null
  br i1 %_200004, label %_210000.0, label %_220000.0
_210000.0:
  br label %_230000.0
_220000.0:
  br label %_230000.0
_230000.0:
  %_230001 = phi i8* [%_200002, %_220000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_210000.0]
  %_230005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-185" to i8*), null
  br i1 %_230005, label %_240000.0, label %_250000.0
_240000.0:
  br label %_260000.0
_250000.0:
  br label %_260000.0
_260000.0:
  %_260001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-185" to i8*), %_250000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_240000.0]
  %_500007 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_260002 = call dereferenceable_or_null(32) i8* %_500007(i8* dereferenceable_or_null(32) %_230001, i8* nonnull dereferenceable(32) %_260001)
  %_260004 = icmp eq i8* %_260002, null
  br i1 %_260004, label %_270000.0, label %_280000.0
_270000.0:
  br label %_290000.0
_280000.0:
  br label %_290000.0
_290000.0:
  %_290001 = phi i8* [%_260002, %_280000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_270000.0]
  %_290002 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_1)
  %_290005 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_290002)
  %_290006 = icmp eq i8* %_290005, null
  br i1 %_290006, label %_300000.0, label %_310000.0
_300000.0:
  br label %_320000.0
_310000.0:
  %_500008 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.lang.IntegerD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_310001 = call dereferenceable_or_null(32) i8* %_500008(i8* nonnull dereferenceable(16) %_290005)
  br label %_320000.0
_320000.0:
  %_320001 = phi i8* [%_310001, %_310000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_300000.0]
  %_500009 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_320002 = call dereferenceable_or_null(32) i8* %_500009(i8* dereferenceable_or_null(32) %_290001, i8* dereferenceable_or_null(32) %_320001)
  %_320004 = icmp eq i8* %_320002, null
  br i1 %_320004, label %_330000.0, label %_340000.0
_330000.0:
  br label %_350000.0
_340000.0:
  br label %_350000.0
_350000.0:
  %_350001 = phi i8* [%_320002, %_340000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_330000.0]
  %_350005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-187" to i8*), null
  br i1 %_350005, label %_360000.0, label %_370000.0
_360000.0:
  br label %_380000.0
_370000.0:
  br label %_380000.0
_380000.0:
  %_380001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-187" to i8*), %_370000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_360000.0]
  %_500010 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_380002 = call dereferenceable_or_null(32) i8* %_500010(i8* dereferenceable_or_null(32) %_350001, i8* nonnull dereferenceable(32) %_380001)
  %_380004 = icmp eq i8* %_380002, null
  br i1 %_380004, label %_390000.0, label %_400000.0
_390000.0:
  br label %_410000.0
_400000.0:
  br label %_410000.0
_410000.0:
  %_410001 = phi i8* [%_380002, %_400000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_390000.0]
  %_410002 = call i32 @"_SM15java.nio.BufferD8capacityiEO"(i8* dereferenceable_or_null(24) %_1)
  %_410005 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_410002)
  %_410006 = icmp eq i8* %_410005, null
  br i1 %_410006, label %_420000.0, label %_430000.0
_420000.0:
  br label %_440000.0
_430000.0:
  %_500011 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.lang.IntegerD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_430001 = call dereferenceable_or_null(32) i8* %_500011(i8* nonnull dereferenceable(16) %_410005)
  br label %_440000.0
_440000.0:
  %_440001 = phi i8* [%_430001, %_430000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_420000.0]
  %_500012 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_440002 = call dereferenceable_or_null(32) i8* %_500012(i8* dereferenceable_or_null(32) %_410001, i8* dereferenceable_or_null(32) %_440001)
  %_440004 = icmp eq i8* %_440002, null
  br i1 %_440004, label %_450000.0, label %_460000.0
_450000.0:
  br label %_470000.0
_460000.0:
  br label %_470000.0
_470000.0:
  %_470001 = phi i8* [%_440002, %_460000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_450000.0]
  %_470005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-189" to i8*), null
  br i1 %_470005, label %_480000.0, label %_490000.0
_480000.0:
  br label %_500000.0
_490000.0:
  br label %_500000.0
_500000.0:
  %_500001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-189" to i8*), %_490000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_480000.0]
  %_500013 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_500002 = call dereferenceable_or_null(32) i8* %_500013(i8* dereferenceable_or_null(32) %_470001, i8* nonnull dereferenceable(32) %_500001)
  ret i8* %_500002
}

define i32 @"_SM15java.nio.BufferD9remainingiEO"(i8* %_1) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_1)
  %_20002 = call i32 @"_SM15java.nio.BufferD8positioniEO"(i8* dereferenceable_or_null(24) %_1)
  %_20004 = sub i32 %_20001, %_20002
  ret i32 %_20004
}

define dereferenceable_or_null(8) i8* @"_SM19java.nio.GenBuffer$D21generic_get$extensionL15java.nio.BufferL16java.lang.ObjectEO"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i32 @"_SM15java.nio.BufferD20getPosAndAdvanceReadiEO"(i8* dereferenceable_or_null(24) %_2)
  %_30007 = icmp ne i8* %_2, null
  br i1 %_30007, label %_30005.0, label %_30006.0
_30005.0:
  %_30011 = bitcast i8* %_2 to i8**
  %_30008 = load i8*, i8** %_30011
  %_30012 = bitcast i8* %_30008 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_30013 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_30012, i32 0, i32 4, i32 11
  %_30009 = bitcast i8** %_30013 to i8*
  %_30014 = bitcast i8* %_30009 to i8**
  %_30003 = load i8*, i8** %_30014
  %_30015 = bitcast i8* %_30003 to i8* (i8*, i32)*
  %_30004 = call dereferenceable_or_null(8) i8* %_30015(i8* dereferenceable_or_null(24) %_2, i32 %_30001)
  ret i8* %_30004
_30006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM19java.nio.GenBuffer$D21generic_get$extensionL15java.nio.BufferL16java.lang.ObjectiiL15java.nio.BufferEO"(i8* %_1, i8* %_2, i8* %_3, i32 %_4, i32 %_5) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  call nonnull dereferenceable(8) i8* @"_SM15java.nio.BufferD23validateArrayIndexRangeL16java.lang.ObjectiiuEO"(i8* dereferenceable_or_null(24) %_2, i8* dereferenceable_or_null(8) %_3, i32 %_4, i32 %_5)
  %_60002 = call i32 @"_SM15java.nio.BufferD20getPosAndAdvanceReadiiEO"(i8* dereferenceable_or_null(24) %_2, i32 %_5)
  %_60009 = icmp ne i8* %_2, null
  br i1 %_60009, label %_60007.0, label %_60008.0
_60007.0:
  %_60014 = bitcast i8* %_2 to i8**
  %_60010 = load i8*, i8** %_60014
  %_60015 = bitcast i8* %_60010 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_60016 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_60015, i32 0, i32 4, i32 8
  %_60011 = bitcast i8** %_60016 to i8*
  %_60017 = bitcast i8* %_60011 to i8**
  %_60004 = load i8*, i8** %_60017
  %_60018 = bitcast i8* %_60004 to i8* (i8*, i32, i8*, i32, i32)*
  call nonnull dereferenceable(8) i8* %_60018(i8* dereferenceable_or_null(24) %_2, i32 %_60002, i8* dereferenceable_or_null(8) %_3, i32 %_4, i32 %_5)
  ret i8* %_2
_60008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM19java.nio.GenBuffer$D21generic_get$extensionL15java.nio.BufferiL16java.lang.ObjectEO"(i8* %_1, i8* %_2, i32 %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call i32 @"_SM15java.nio.BufferD13validateIndexiiEO"(i8* dereferenceable_or_null(24) %_2, i32 %_3)
  %_40007 = icmp ne i8* %_2, null
  br i1 %_40007, label %_40005.0, label %_40006.0
_40005.0:
  %_40011 = bitcast i8* %_2 to i8**
  %_40008 = load i8*, i8** %_40011
  %_40012 = bitcast i8* %_40008 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_40013 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_40012, i32 0, i32 4, i32 11
  %_40009 = bitcast i8** %_40013 to i8*
  %_40014 = bitcast i8* %_40009 to i8**
  %_40003 = load i8*, i8** %_40014
  %_40015 = bitcast i8* %_40003 to i8* (i8*, i32)*
  %_40004 = call dereferenceable_or_null(8) i8* %_40015(i8* dereferenceable_or_null(24) %_2, i32 %_40001)
  ret i8* %_40004
_40006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM19java.nio.GenBuffer$D21generic_put$extensionL15java.nio.BufferL15java.nio.BufferL15java.nio.BufferEO"(i8* %_1, i8* %_2, i8* %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40004 = icmp eq i8* %_3, %_2
  br i1 %_40004, label %_50000.0, label %_60000.0
_60000.0:
  br label %_170000.0
_170000.0:
  call nonnull dereferenceable(8) i8* @"_SM15java.nio.BufferD17ensureNotReadOnlyuEO"(i8* dereferenceable_or_null(24) %_2)
  %_170002 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_3)
  %_170003 = call i32 @"_SM15java.nio.BufferD8positioniEO"(i8* dereferenceable_or_null(24) %_3)
  %_170005 = sub i32 %_170002, %_170003
  %_170006 = call i32 @"_SM15java.nio.BufferD21getPosAndAdvanceWriteiiEO"(i8* dereferenceable_or_null(24) %_2, i32 %_170005)
  %_230004 = icmp ne i8* %_3, null
  br i1 %_230004, label %_230002.0, label %_230003.0
_230002.0:
  %_230035 = bitcast i8* %_3 to i8**
  %_230005 = load i8*, i8** %_230035
  %_230036 = bitcast i8* %_230005 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_230037 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_230036, i32 0, i32 4, i32 9
  %_230006 = bitcast i8** %_230037 to i8*
  %_230038 = bitcast i8* %_230006 to i8**
  %_170008 = load i8*, i8** %_230038
  %_230039 = bitcast i8* %_170008 to i8* (i8*, i32)*
  %_170009 = call dereferenceable_or_null(24) i8* %_230039(i8* dereferenceable_or_null(24) %_3, i32 %_170002)
  %_230008 = icmp ne i8* %_3, null
  br i1 %_230008, label %_230007.0, label %_230003.0
_230007.0:
  %_230040 = bitcast i8* %_3 to i8**
  %_230009 = load i8*, i8** %_230040
  %_230041 = bitcast i8* %_230009 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_230042 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_230041, i32 0, i32 4, i32 5
  %_230010 = bitcast i8** %_230042 to i8*
  %_230043 = bitcast i8* %_230010 to i8**
  %_170011 = load i8*, i8** %_230043
  %_230044 = bitcast i8* %_170011 to i8* (i8*)*
  %_170012 = call dereferenceable_or_null(8) i8* %_230044(i8* dereferenceable_or_null(24) %_3)
  %_170015 = icmp eq i8* %_170012, null
  %_170016 = xor i1 %_170015, true
  br i1 %_170016, label %_180000.0, label %_190000.0
_180000.0:
  %_230012 = icmp ne i8* %_3, null
  br i1 %_230012, label %_230011.0, label %_230003.0
_230011.0:
  %_230045 = bitcast i8* %_3 to i8**
  %_230013 = load i8*, i8** %_230045
  %_230046 = bitcast i8* %_230013 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_230047 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_230046, i32 0, i32 4, i32 6
  %_230014 = bitcast i8** %_230047 to i8*
  %_230048 = bitcast i8* %_230014 to i8**
  %_180002 = load i8*, i8** %_230048
  %_230049 = bitcast i8* %_180002 to i32 (i8*)*
  %_180003 = call i32 %_230049(i8* dereferenceable_or_null(24) %_3)
  %_230016 = icmp ne i8* %_2, null
  br i1 %_230016, label %_230015.0, label %_230003.0
_230015.0:
  %_230050 = bitcast i8* %_2 to i8**
  %_230017 = load i8*, i8** %_230050
  %_230051 = bitcast i8* %_230017 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_230052 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_230051, i32 0, i32 4, i32 7
  %_230018 = bitcast i8** %_230052 to i8*
  %_230053 = bitcast i8* %_230018 to i8**
  %_180006 = load i8*, i8** %_230053
  %_180007 = add i32 %_180003, %_170003
  %_230054 = bitcast i8* %_180006 to i8* (i8*, i32, i8*, i32, i32)*
  call nonnull dereferenceable(8) i8* %_230054(i8* dereferenceable_or_null(24) %_2, i32 %_170006, i8* dereferenceable_or_null(8) %_170012, i32 %_180007, i32 %_170005)
  br label %_200000.0
_190000.0:
  br label %_210000.0
_210000.0:
  %_210001 = phi i32 [%_170006, %_190000.0], [%_220009, %_230024.0]
  %_210002 = phi i32 [%_170003, %_190000.0], [%_220010, %_230024.0]
  %_210004 = icmp ne i32 %_210002, %_170002
  br i1 %_210004, label %_220000.0, label %_230000.0
_220000.0:
  %_230021 = icmp ne i8* %_3, null
  br i1 %_230021, label %_230020.0, label %_230003.0
_230020.0:
  %_230055 = bitcast i8* %_3 to i8**
  %_230022 = load i8*, i8** %_230055
  %_230056 = bitcast i8* %_230022 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_230057 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_230056, i32 0, i32 4, i32 11
  %_230023 = bitcast i8** %_230057 to i8*
  %_230058 = bitcast i8* %_230023 to i8**
  %_220002 = load i8*, i8** %_230058
  %_230059 = bitcast i8* %_220002 to i8* (i8*, i32)*
  %_220003 = call dereferenceable_or_null(8) i8* %_230059(i8* dereferenceable_or_null(24) %_3, i32 %_210002)
  %_230025 = icmp ne i8* %_2, null
  br i1 %_230025, label %_230024.0, label %_230003.0
_230024.0:
  %_230060 = bitcast i8* %_2 to i8**
  %_230026 = load i8*, i8** %_230060
  %_230061 = bitcast i8* %_230026 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_230062 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_230061, i32 0, i32 4, i32 4
  %_230027 = bitcast i8** %_230062 to i8*
  %_230063 = bitcast i8* %_230027 to i8**
  %_220005 = load i8*, i8** %_230063
  %_230064 = bitcast i8* %_220005 to i8* (i8*, i32, i8*)*
  call nonnull dereferenceable(8) i8* %_230064(i8* dereferenceable_or_null(24) %_2, i32 %_210001, i8* dereferenceable_or_null(8) %_220003)
  %_220009 = add i32 %_210001, 1
  %_220010 = add i32 %_210002, 1
  br label %_210000.0
_230000.0:
  br label %_200000.0
_200000.0:
  %_200001 = phi i32 [%_210001, %_230000.0], [%_170006, %_230015.0]
  %_200002 = phi i32 [%_210002, %_230000.0], [%_170003, %_230015.0]
  ret i8* %_2
_50000.0:
  br label %_150000.0
_150000.0:
  %_150001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM34java.lang.IllegalArgumentExceptionG4type" to i8*), i64 40)
  %_230065 = bitcast i8* %_150001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_230066 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_230065, i32 0, i32 5
  %_230030 = bitcast i1* %_230066 to i8*
  %_230067 = bitcast i8* %_230030 to i1*
  store i1 true, i1* %_230067
  %_230068 = bitcast i8* %_150001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_230069 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_230068, i32 0, i32 4
  %_230032 = bitcast i1* %_230069 to i8*
  %_230070 = bitcast i8* %_230032 to i1*
  store i1 true, i1* %_230070
  %_150004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_150001)
  br label %_160000.0
_160000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_150001)
  unreachable
_230003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM19java.nio.GenBuffer$D21generic_put$extensionL15java.nio.BufferL16java.lang.ObjectL15java.nio.BufferEO"(i8* %_1, i8* %_2, i8* %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  call nonnull dereferenceable(8) i8* @"_SM15java.nio.BufferD17ensureNotReadOnlyuEO"(i8* dereferenceable_or_null(24) %_2)
  %_40002 = call i32 @"_SM15java.nio.BufferD21getPosAndAdvanceWriteiEO"(i8* dereferenceable_or_null(24) %_2)
  %_40009 = icmp ne i8* %_2, null
  br i1 %_40009, label %_40007.0, label %_40008.0
_40007.0:
  %_40014 = bitcast i8* %_2 to i8**
  %_40010 = load i8*, i8** %_40014
  %_40015 = bitcast i8* %_40010 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_40016 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_40015, i32 0, i32 4, i32 4
  %_40011 = bitcast i8** %_40016 to i8*
  %_40017 = bitcast i8* %_40011 to i8**
  %_40004 = load i8*, i8** %_40017
  %_40018 = bitcast i8* %_40004 to i8* (i8*, i32, i8*)*
  call nonnull dereferenceable(8) i8* %_40018(i8* dereferenceable_or_null(24) %_2, i32 %_40002, i8* dereferenceable_or_null(8) %_3)
  ret i8* %_2
_40008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM19java.nio.GenBuffer$D21generic_put$extensionL15java.nio.BufferL16java.lang.ObjectiiL15java.nio.BufferEO"(i8* %_1, i8* %_2, i8* %_3, i32 %_4, i32 %_5) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  call nonnull dereferenceable(8) i8* @"_SM15java.nio.BufferD17ensureNotReadOnlyuEO"(i8* dereferenceable_or_null(24) %_2)
  call nonnull dereferenceable(8) i8* @"_SM15java.nio.BufferD23validateArrayIndexRangeL16java.lang.ObjectiiuEO"(i8* dereferenceable_or_null(24) %_2, i8* dereferenceable_or_null(8) %_3, i32 %_4, i32 %_5)
  %_60003 = call i32 @"_SM15java.nio.BufferD21getPosAndAdvanceWriteiiEO"(i8* dereferenceable_or_null(24) %_2, i32 %_5)
  %_60011 = icmp ne i8* %_2, null
  br i1 %_60011, label %_60009.0, label %_60010.0
_60009.0:
  %_60016 = bitcast i8* %_2 to i8**
  %_60012 = load i8*, i8** %_60016
  %_60017 = bitcast i8* %_60012 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_60018 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_60017, i32 0, i32 4, i32 7
  %_60013 = bitcast i8** %_60018 to i8*
  %_60019 = bitcast i8* %_60013 to i8**
  %_60005 = load i8*, i8** %_60019
  %_60020 = bitcast i8* %_60005 to i8* (i8*, i32, i8*, i32, i32)*
  call nonnull dereferenceable(8) i8* %_60020(i8* dereferenceable_or_null(24) %_2, i32 %_60003, i8* dereferenceable_or_null(8) %_3, i32 %_4, i32 %_5)
  ret i8* %_2
_60010.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM19java.nio.GenBuffer$D22generic_load$extensionL15java.nio.BufferiL16java.lang.ObjectiiuEO"(i8* %_1, i8* %_2, i32 %_3, i8* %_4, i32 %_5, i32 %_6) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_70000.0:
  %_70004 = add i32 %_3, %_6
  br label %_80000.0
_80000.0:
  %_80001 = phi i32 [%_5, %_70000.0], [%_90008, %_100001.0]
  %_80002 = phi i32 [%_3, %_70000.0], [%_90009, %_100001.0]
  %_80003 = phi i32 [%_70004, %_70000.0], [%_80003, %_100001.0]
  %_80005 = icmp ne i32 %_80002, %_80003
  br i1 %_80005, label %_90000.0, label %_100000.0
_90000.0:
  %_100003 = icmp ne i8* %_2, null
  br i1 %_100003, label %_100001.0, label %_100002.0
_100001.0:
  %_100008 = bitcast i8* %_2 to i8**
  %_100004 = load i8*, i8** %_100008
  %_100009 = bitcast i8* %_100004 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_100010 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_100009, i32 0, i32 4, i32 11
  %_100005 = bitcast i8** %_100010 to i8*
  %_100011 = bitcast i8* %_100005 to i8**
  %_90003 = load i8*, i8** %_100011
  %_100012 = bitcast i8* %_90003 to i8* (i8*, i32)*
  %_90004 = call dereferenceable_or_null(8) i8* %_100012(i8* dereferenceable_or_null(24) %_2, i32 %_80002)
  call nonnull dereferenceable(8) i8* @"_SM27scala.runtime.ScalaRunTime$D12array_updateL16java.lang.ObjectiL16java.lang.ObjectuEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(8) %_4, i32 %_80001, i8* dereferenceable_or_null(8) %_90004)
  %_90008 = add i32 %_80001, 1
  %_90009 = add i32 %_80002, 1
  br label %_80000.0
_100000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_100002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM19java.nio.GenBuffer$D23generic_array$extensionL15java.nio.BufferL16java.lang.ObjectEO"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_300003 = icmp ne i8* %_2, null
  br i1 %_300003, label %_300001.0, label %_300002.0
_300001.0:
  %_300021 = bitcast i8* %_2 to i8**
  %_300004 = load i8*, i8** %_300021
  %_300022 = bitcast i8* %_300004 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_300023 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_300022, i32 0, i32 4, i32 5
  %_300005 = bitcast i8** %_300023 to i8*
  %_300024 = bitcast i8* %_300005 to i8**
  %_30002 = load i8*, i8** %_300024
  %_300025 = bitcast i8* %_30002 to i8* (i8*)*
  %_30003 = call dereferenceable_or_null(8) i8* %_300025(i8* dereferenceable_or_null(24) %_2)
  %_30005 = icmp eq i8* %_30003, null
  br i1 %_30005, label %_40000.0, label %_50000.0
_50000.0:
  br label %_160000.0
_160000.0:
  %_300007 = icmp ne i8* %_2, null
  br i1 %_300007, label %_300006.0, label %_300002.0
_300006.0:
  %_300026 = bitcast i8* %_2 to i8**
  %_300008 = load i8*, i8** %_300026
  %_300027 = bitcast i8* %_300008 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_300028 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_300027, i32 0, i32 4, i32 10
  %_300009 = bitcast i8** %_300028 to i8*
  %_300029 = bitcast i8* %_300009 to i8**
  %_160002 = load i8*, i8** %_300029
  %_300030 = bitcast i8* %_160002 to i1 (i8*)*
  %_160003 = call i1 %_300030(i8* dereferenceable_or_null(24) %_2)
  br i1 %_160003, label %_170000.0, label %_180000.0
_180000.0:
  br label %_300000.0
_300000.0:
  ret i8* %_30003
_40000.0:
  br label %_140000.0
_140000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM39java.lang.UnsupportedOperationExceptionG4type" to i8*), i64 40)
  %_300031 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_300032 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_300031, i32 0, i32 5
  %_300011 = bitcast i1* %_300032 to i8*
  %_300033 = bitcast i8* %_300011 to i1*
  store i1 true, i1* %_300033
  %_300034 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_300035 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_300034, i32 0, i32 4
  %_300013 = bitcast i1* %_300035 to i8*
  %_300036 = bitcast i8* %_300013 to i1*
  store i1 true, i1* %_300036
  %_140004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_140001)
  br label %_150000.0
_150000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_140001)
  unreachable
_170000.0:
  br label %_280000.0
_280000.0:
  %_280001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.ReadOnlyBufferExceptionG4type" to i8*), i64 40)
  %_300037 = bitcast i8* %_280001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_300038 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_300037, i32 0, i32 5
  %_300016 = bitcast i1* %_300038 to i8*
  %_300039 = bitcast i8* %_300016 to i1*
  store i1 true, i1* %_300039
  %_300040 = bitcast i8* %_280001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_300041 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_300040, i32 0, i32 4
  %_300018 = bitcast i1* %_300041 to i8*
  %_300042 = bitcast i8* %_300018 to i1*
  store i1 true, i1* %_300042
  %_280004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_280001)
  br label %_290000.0
_290000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_280001)
  unreachable
_300002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM19java.nio.GenBuffer$D26generic_hasArray$extensionL15java.nio.BufferzEO"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_60004 = icmp ne i8* %_2, null
  br i1 %_60004, label %_60002.0, label %_60003.0
_60002.0:
  %_60012 = bitcast i8* %_2 to i8**
  %_60005 = load i8*, i8** %_60012
  %_60013 = bitcast i8* %_60005 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_60014 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_60013, i32 0, i32 4, i32 5
  %_60006 = bitcast i8** %_60014 to i8*
  %_60015 = bitcast i8* %_60006 to i8**
  %_30002 = load i8*, i8** %_60015
  %_60016 = bitcast i8* %_30002 to i8* (i8*)*
  %_30003 = call dereferenceable_or_null(8) i8* %_60016(i8* dereferenceable_or_null(24) %_2)
  %_30006 = icmp eq i8* %_30003, null
  %_30007 = xor i1 %_30006, true
  br i1 %_30007, label %_40000.0, label %_50000.0
_40000.0:
  %_60008 = icmp ne i8* %_2, null
  br i1 %_60008, label %_60007.0, label %_60003.0
_60007.0:
  %_60017 = bitcast i8* %_2 to i8**
  %_60009 = load i8*, i8** %_60017
  %_60018 = bitcast i8* %_60009 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_60019 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_60018, i32 0, i32 4, i32 10
  %_60010 = bitcast i8** %_60019 to i8*
  %_60020 = bitcast i8* %_60010 to i8**
  %_40002 = load i8*, i8** %_60020
  %_60021 = bitcast i8* %_40002 to i1 (i8*)*
  %_40003 = call i1 %_60021(i8* dereferenceable_or_null(24) %_2)
  %_40005 = xor i1 %_40003, true
  br label %_60000.0
_50000.0:
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [false, %_50000.0], [%_40005, %_60007.0]
  ret i1 %_60001
_60003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM19java.nio.GenBuffer$D26generic_hashCode$extensionL15java.nio.BufferiiEO"(i8* %_1, i8* %_2, i32 %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40003 = call i32 @"_SM15java.nio.BufferD8positioniEO"(i8* dereferenceable_or_null(24) %_2)
  %_40004 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_2)
  br label %_50000.0
_50000.0:
  %_50001 = phi i32 [%_40003, %_40000.0], [%_60008, %_70005.0]
  %_50002 = phi i32 [%_3, %_40000.0], [%_60006, %_70005.0]
  %_50004 = icmp ne i32 %_50001, %_40004
  br i1 %_50004, label %_60000.0, label %_70000.0
_60000.0:
  %_60001 = call dereferenceable_or_null(24) i8* @"_SM31scala.util.hashing.MurmurHash3$G4load"()
  %_70007 = icmp ne i8* %_2, null
  br i1 %_70007, label %_70005.0, label %_70006.0
_70005.0:
  %_70011 = bitcast i8* %_2 to i8**
  %_70008 = load i8*, i8** %_70011
  %_70012 = bitcast i8* %_70008 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_70013 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_70012, i32 0, i32 4, i32 11
  %_70009 = bitcast i8** %_70013 to i8*
  %_70014 = bitcast i8* %_70009 to i8**
  %_60003 = load i8*, i8** %_70014
  %_70015 = bitcast i8* %_60003 to i8* (i8*, i32)*
  %_60004 = call dereferenceable_or_null(8) i8* %_70015(i8* dereferenceable_or_null(24) %_2, i32 %_50001)
  %_60005 = call i32 @"_SM21scala.runtime.StaticsD7anyHashL16java.lang.ObjectiEo"(i8* dereferenceable_or_null(8) %_60004)
  %_60006 = call i32 @"_SM30scala.util.hashing.MurmurHash3D3mixiiiEO"(i8* nonnull dereferenceable(24) %_60001, i32 %_50002, i32 %_60005)
  %_60008 = add i32 %_50001, 1
  br label %_50000.0
_70000.0:
  %_70001 = call dereferenceable_or_null(24) i8* @"_SM31scala.util.hashing.MurmurHash3$G4load"()
  %_70003 = sub i32 %_40004, %_40003
  %_70004 = call i32 @"_SM30scala.util.hashing.MurmurHash3D12finalizeHashiiiEO"(i8* nonnull dereferenceable(24) %_70001, i32 %_50002, i32 %_70003)
  ret i32 %_70004
_70006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM19java.nio.GenBuffer$D27generic_compareTo$extensionL15java.nio.BufferL15java.nio.BufferL15scala.Function2iEO"(i8* %_1, i8* %_2, i8* %_3, i8* %_4) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_50003 = icmp eq i8* %_2, %_3
  br i1 %_50003, label %_60000.0, label %_70000.0
_60000.0:
  br label %_80000.0
_70000.0:
  %_70001 = call i32 @"_SM15java.nio.BufferD8positioniEO"(i8* dereferenceable_or_null(24) %_2)
  %_70002 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_2)
  %_70004 = call i32 @"_SM15java.nio.BufferD8positioniEO"(i8* dereferenceable_or_null(24) %_3)
  %_70005 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(24) %_3)
  %_90002 = sub i32 %_70002, %_70001
  %_90003 = sub i32 %_70005, %_70004
  %_90004 = icmp slt i32 %_90002, %_90003
  br i1 %_90004, label %_100000.0, label %_110000.0
_100000.0:
  br label %_120000.0
_110000.0:
  br label %_120000.0
_120000.0:
  %_120001 = phi i32 [%_90003, %_110000.0], [%_90002, %_100000.0]
  br label %_130000.0
_130000.0:
  %_130001 = phi i32 [0, %_120000.0], [%_180002, %_180000.0]
  %_130003 = icmp ne i32 %_130001, %_120001
  br i1 %_130003, label %_140000.0, label %_150000.0
_140000.0:
  %_180005 = icmp ne i8* %_2, null
  br i1 %_180005, label %_180003.0, label %_180004.0
_180003.0:
  %_180020 = bitcast i8* %_2 to i8**
  %_180006 = load i8*, i8** %_180020
  %_180021 = bitcast i8* %_180006 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_180022 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_180021, i32 0, i32 4, i32 11
  %_180007 = bitcast i8** %_180022 to i8*
  %_180023 = bitcast i8* %_180007 to i8**
  %_140003 = load i8*, i8** %_180023
  %_140004 = add i32 %_70001, %_130001
  %_180024 = bitcast i8* %_140003 to i8* (i8*, i32)*
  %_140005 = call dereferenceable_or_null(8) i8* %_180024(i8* dereferenceable_or_null(24) %_2, i32 %_140004)
  %_180009 = icmp ne i8* %_3, null
  br i1 %_180009, label %_180008.0, label %_180004.0
_180008.0:
  %_180025 = bitcast i8* %_3 to i8**
  %_180010 = load i8*, i8** %_180025
  %_180026 = bitcast i8* %_180010 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_180027 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_180026, i32 0, i32 4, i32 11
  %_180011 = bitcast i8** %_180027 to i8*
  %_180028 = bitcast i8* %_180011 to i8**
  %_140008 = load i8*, i8** %_180028
  %_140009 = add i32 %_70004, %_130001
  %_180029 = bitcast i8* %_140008 to i8* (i8*, i32)*
  %_140010 = call dereferenceable_or_null(8) i8* %_180029(i8* dereferenceable_or_null(24) %_3, i32 %_140009)
  %_180013 = icmp ne i8* %_4, null
  br i1 %_180013, label %_180012.0, label %_180004.0
_180012.0:
  %_180030 = bitcast i8* %_4 to i8**
  %_180014 = load i8*, i8** %_180030
  %_180031 = bitcast i8* %_180014 to { i8*, i32, i32, i8* }*
  %_180032 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_180031, i32 0, i32 2
  %_180015 = bitcast i32* %_180032 to i8*
  %_180033 = bitcast i8* %_180015 to i32*
  %_180016 = load i32, i32* %_180033
  %_180034 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_180035 = getelementptr i8*, i8** %_180034, i32 2146
  %_180017 = bitcast i8** %_180035 to i8*
  %_180036 = bitcast i8* %_180017 to i8**
  %_180037 = getelementptr i8*, i8** %_180036, i32 %_180016
  %_180018 = bitcast i8** %_180037 to i8*
  %_180038 = bitcast i8* %_180018 to i8**
  %_140012 = load i8*, i8** %_180038
  %_180039 = bitcast i8* %_140012 to i8* (i8*, i8*, i8*)*
  %_140013 = call dereferenceable_or_null(8) i8* %_180039(i8* dereferenceable_or_null(8) %_4, i8* dereferenceable_or_null(8) %_140005, i8* dereferenceable_or_null(8) %_140010)
  %_180040 = bitcast i8* bitcast (i32 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D10unboxToIntL16java.lang.ObjectiEO" to i8*) to i32 (i8*, i8*)*
  %_140014 = call i32 %_180040(i8* null, i8* dereferenceable_or_null(8) %_140013)
  %_140016 = icmp ne i32 %_140014, 0
  br i1 %_140016, label %_160000.0, label %_170000.0
_160000.0:
  ret i32 %_140014
_170000.0:
  br label %_180000.0
_180000.0:
  %_180002 = add i32 %_130001, 1
  br label %_130000.0
_150000.0:
  %_150002 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_180041 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM13scala.Predef$D11int2IntegeriL17java.lang.IntegerEO" to i8*) to i8* (i8*, i32)*
  %_150003 = call dereferenceable_or_null(16) i8* %_180041(i8* nonnull dereferenceable(16) %_150002, i32 %_90002)
  %_180042 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM13scala.Predef$D11int2IntegeriL17java.lang.IntegerEO" to i8*) to i8* (i8*, i32)*
  %_150004 = call dereferenceable_or_null(16) i8* %_180042(i8* nonnull dereferenceable(16) %_150002, i32 %_90003)
  %_150005 = call i32 @"_SM17java.lang.IntegerD9compareToL17java.lang.IntegeriEO"(i8* dereferenceable_or_null(16) %_150003, i8* dereferenceable_or_null(16) %_150004)
  br label %_80000.0
_80000.0:
  %_80001 = phi i32 [%_130001, %_150000.0], [0, %_60000.0]
  %_80002 = phi i32 [%_150005, %_150000.0], [0, %_60000.0]
  ret i32 %_80002
_180004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM19java.nio.GenBuffer$D29generic_arrayOffset$extensionL15java.nio.BufferiEO"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_300003 = icmp ne i8* %_2, null
  br i1 %_300003, label %_300001.0, label %_300002.0
_300001.0:
  %_300021 = bitcast i8* %_2 to i8**
  %_300004 = load i8*, i8** %_300021
  %_300022 = bitcast i8* %_300004 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_300023 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_300022, i32 0, i32 4, i32 6
  %_300005 = bitcast i8** %_300023 to i8*
  %_300024 = bitcast i8* %_300005 to i8**
  %_30002 = load i8*, i8** %_300024
  %_300025 = bitcast i8* %_30002 to i32 (i8*)*
  %_30003 = call i32 %_300025(i8* dereferenceable_or_null(24) %_2)
  %_30005 = icmp eq i32 %_30003, -1
  br i1 %_30005, label %_40000.0, label %_50000.0
_50000.0:
  br label %_160000.0
_160000.0:
  %_300007 = icmp ne i8* %_2, null
  br i1 %_300007, label %_300006.0, label %_300002.0
_300006.0:
  %_300026 = bitcast i8* %_2 to i8**
  %_300008 = load i8*, i8** %_300026
  %_300027 = bitcast i8* %_300008 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_300028 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_300027, i32 0, i32 4, i32 10
  %_300009 = bitcast i8** %_300028 to i8*
  %_300029 = bitcast i8* %_300009 to i8**
  %_160002 = load i8*, i8** %_300029
  %_300030 = bitcast i8* %_160002 to i1 (i8*)*
  %_160003 = call i1 %_300030(i8* dereferenceable_or_null(24) %_2)
  br i1 %_160003, label %_170000.0, label %_180000.0
_180000.0:
  br label %_300000.0
_300000.0:
  ret i32 %_30003
_40000.0:
  br label %_140000.0
_140000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM39java.lang.UnsupportedOperationExceptionG4type" to i8*), i64 40)
  %_300031 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_300032 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_300031, i32 0, i32 5
  %_300011 = bitcast i1* %_300032 to i8*
  %_300033 = bitcast i8* %_300011 to i1*
  store i1 true, i1* %_300033
  %_300034 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_300035 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_300034, i32 0, i32 4
  %_300013 = bitcast i1* %_300035 to i8*
  %_300036 = bitcast i8* %_300013 to i1*
  store i1 true, i1* %_300036
  %_140004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_140001)
  br label %_150000.0
_150000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_140001)
  unreachable
_170000.0:
  br label %_280000.0
_280000.0:
  %_280001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.ReadOnlyBufferExceptionG4type" to i8*), i64 40)
  %_300037 = bitcast i8* %_280001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_300038 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_300037, i32 0, i32 5
  %_300016 = bitcast i1* %_300038 to i8*
  %_300039 = bitcast i8* %_300016 to i1*
  store i1 true, i1* %_300039
  %_300040 = bitcast i8* %_280001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_300041 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_300040, i32 0, i32 4
  %_300018 = bitcast i1* %_300041 to i8*
  %_300042 = bitcast i8* %_300018 to i1*
  store i1 true, i1* %_300042
  %_280004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_280001)
  br label %_290000.0
_290000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_280001)
  unreachable
_300002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM19java.nio.GenBuffer$D5applyL15java.nio.BufferL15java.nio.BufferEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i8* %_2
}

define nonnull dereferenceable(8) i8* @"_SM19java.nio.GenBuffer$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(32) i8* @"_SM20scala.collection.MapD12stringPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-191" to i8*)
}

define i1 @"_SM20scala.collection.MapD17$anonfun$equals$1L20scala.collection.MapL12scala.Tuple2zEPT20scala.collection.Map"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_110004 = icmp ne i8* %_3, null
  br i1 %_110004, label %_110002.0, label %_110003.0
_110002.0:
  %_110025 = bitcast i8* %_3 to { i8*, i8*, i8* }*
  %_110026 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_110025, i32 0, i32 2
  %_110005 = bitcast i8** %_110026 to i8*
  %_110027 = bitcast i8* %_110005 to i8**
  %_50001 = load i8*, i8** %_110027, !dereferenceable_or_null !{i64 8}
  %_40001 = call dereferenceable_or_null(32) i8* @"_SM21scala.collection.Map$G4load"()
  %_110028 = bitcast i8* %_40001 to { i8*, i8*, i8*, i8* }*
  %_110029 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_110028, i32 0, i32 2
  %_110006 = bitcast i8** %_110029 to i8*
  %_110030 = bitcast i8* %_110006 to i8**
  %_60001 = load i8*, i8** %_110030, !dereferenceable_or_null !{i64 8}
  %_110008 = icmp ne i8* %_2, null
  br i1 %_110008, label %_110007.0, label %_110003.0
_110007.0:
  %_110031 = bitcast i8* %_2 to i8**
  %_110009 = load i8*, i8** %_110031
  %_110032 = bitcast i8* %_110009 to { i8*, i32, i32, i8* }*
  %_110033 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110032, i32 0, i32 2
  %_110010 = bitcast i32* %_110033 to i8*
  %_110034 = bitcast i8* %_110010 to i32*
  %_110011 = load i32, i32* %_110034
  %_110035 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_110036 = getelementptr i8*, i8** %_110035, i32 1524
  %_110012 = bitcast i8** %_110036 to i8*
  %_110037 = bitcast i8* %_110012 to i8**
  %_110038 = getelementptr i8*, i8** %_110037, i32 %_110011
  %_110013 = bitcast i8** %_110038 to i8*
  %_110039 = bitcast i8* %_110013 to i8**
  %_40003 = load i8*, i8** %_110039
  %_110040 = bitcast i8* %_40003 to i8* (i8*, i8*, i8*)*
  %_40004 = call dereferenceable_or_null(8) i8* %_110040(i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_50001, i8* dereferenceable_or_null(8) %_60001)
  %_40006 = icmp eq i8* %_40004, null
  br i1 %_40006, label %_70000.0, label %_80000.0
_70000.0:
  %_110015 = icmp ne i8* %_3, null
  br i1 %_110015, label %_110014.0, label %_110003.0
_110014.0:
  %_110041 = bitcast i8* %_3 to { i8*, i8*, i8* }*
  %_110042 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_110041, i32 0, i32 1
  %_110016 = bitcast i8** %_110042 to i8*
  %_110043 = bitcast i8* %_110016 to i8**
  %_90001 = load i8*, i8** %_110043, !dereferenceable_or_null !{i64 8}
  %_70002 = icmp eq i8* %_90001, null
  br label %_100000.0
_80000.0:
  %_110018 = icmp ne i8* %_3, null
  br i1 %_110018, label %_110017.0, label %_110003.0
_110017.0:
  %_110044 = bitcast i8* %_3 to { i8*, i8*, i8* }*
  %_110045 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_110044, i32 0, i32 1
  %_110019 = bitcast i8** %_110045 to i8*
  %_110046 = bitcast i8* %_110019 to i8**
  %_110001 = load i8*, i8** %_110046, !dereferenceable_or_null !{i64 8}
  %_110021 = icmp ne i8* %_40004, null
  br i1 %_110021, label %_110020.0, label %_110003.0
_110020.0:
  %_110047 = bitcast i8* %_40004 to i8**
  %_110022 = load i8*, i8** %_110047
  %_110048 = bitcast i8* %_110022 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_110049 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_110048, i32 0, i32 4, i32 0
  %_110023 = bitcast i8** %_110049 to i8*
  %_110050 = bitcast i8* %_110023 to i8**
  %_80002 = load i8*, i8** %_110050
  %_110051 = bitcast i8* %_80002 to i1 (i8*, i8*)*
  %_80003 = call i1 %_110051(i8* dereferenceable_or_null(8) %_40004, i8* dereferenceable_or_null(8) %_110001)
  br label %_100000.0
_100000.0:
  %_100001 = phi i1 [%_80003, %_110020.0], [%_70002, %_110014.0]
  ret i1 %_100001
_110003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM20scala.collection.MapD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM20scala.collection.MapD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_3.0:
  %_8 = icmp eq i8* %_1, %_2
  br i1 %_8, label %_4.0, label %_5.0
_4.0:
  br label %_6.0
_5.0:
  br label %_9.0
_9.0:
  %_61 = icmp eq i8* %_2, null
  br i1 %_61, label %_58.0, label %_59.0
_58.0:
  br label %_60.0
_59.0:
  %_123 = bitcast i8* %_2 to i8**
  %_62 = load i8*, i8** %_123
  %_124 = bitcast i8* %_62 to { i8*, i32, i32, i8* }*
  %_125 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_124, i32 0, i32 1
  %_63 = bitcast i32* %_125 to i8*
  %_126 = bitcast i8* %_63 to i32*
  %_64 = load i32, i32* %_126
  %_127 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_128 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_127, i32 0, i32 %_64, i32 25
  %_65 = bitcast i1* %_128 to i8*
  %_129 = bitcast i8* %_65 to i1*
  %_66 = load i1, i1* %_129
  br label %_60.0
_60.0:
  %_16 = phi i1 [%_66, %_59.0], [false, %_58.0]
  br i1 %_16, label %_12.0, label %_13.0
_12.0:
  %_70 = icmp eq i8* %_2, null
  br i1 %_70, label %_68.0, label %_67.0
_67.0:
  %_130 = bitcast i8* %_2 to i8**
  %_71 = load i8*, i8** %_130
  %_131 = bitcast i8* %_71 to { i8*, i32, i32, i8* }*
  %_132 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_131, i32 0, i32 1
  %_72 = bitcast i32* %_132 to i8*
  %_133 = bitcast i8* %_72 to i32*
  %_73 = load i32, i32* %_133
  %_134 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_135 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_134, i32 0, i32 %_73, i32 25
  %_74 = bitcast i1* %_135 to i8*
  %_136 = bitcast i8* %_74 to i1*
  %_75 = load i1, i1* %_136
  br i1 %_75, label %_68.0, label %_69.0
_68.0:
  %_17 = bitcast i8* %_2 to i8*
  %_78 = icmp ne i8* %_17, null
  br i1 %_78, label %_76.0, label %_77.0
_76.0:
  %_137 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractMapD8canEqualL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_23 = call i1 %_137(i8* dereferenceable_or_null(8) %_17, i8* dereferenceable_or_null(8) %_1)
  br i1 %_23, label %_18.0, label %_19.0
_18.0:
  %_80 = icmp ne i8* %_1, null
  br i1 %_80, label %_79.0, label %_77.0
_79.0:
  %_138 = bitcast i8* %_1 to i8**
  %_81 = load i8*, i8** %_138
  %_139 = bitcast i8* %_81 to { i8*, i32, i32, i8* }*
  %_140 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_139, i32 0, i32 2
  %_82 = bitcast i32* %_140 to i8*
  %_141 = bitcast i8* %_82 to i32*
  %_83 = load i32, i32* %_141
  %_142 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_143 = getelementptr i8*, i8** %_142, i32 1698
  %_84 = bitcast i8** %_143 to i8*
  %_144 = bitcast i8* %_84 to i8**
  %_145 = getelementptr i8*, i8** %_144, i32 %_83
  %_85 = bitcast i8** %_145 to i8*
  %_146 = bitcast i8* %_85 to i8**
  %_28 = load i8*, i8** %_146
  %_147 = bitcast i8* %_28 to i32 (i8*)*
  %_29 = call i32 %_147(i8* dereferenceable_or_null(8) %_1)
  %_87 = icmp ne i8* %_17, null
  br i1 %_87, label %_86.0, label %_77.0
_86.0:
  %_148 = bitcast i8* %_17 to i8**
  %_88 = load i8*, i8** %_148
  %_149 = bitcast i8* %_88 to { i8*, i32, i32, i8* }*
  %_150 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_149, i32 0, i32 2
  %_89 = bitcast i32* %_150 to i8*
  %_151 = bitcast i8* %_89 to i32*
  %_90 = load i32, i32* %_151
  %_152 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_153 = getelementptr i8*, i8** %_152, i32 1698
  %_91 = bitcast i8** %_153 to i8*
  %_154 = bitcast i8* %_91 to i8**
  %_155 = getelementptr i8*, i8** %_154, i32 %_90
  %_92 = bitcast i8** %_155 to i8*
  %_156 = bitcast i8* %_92 to i8**
  %_30 = load i8*, i8** %_156
  %_157 = bitcast i8* %_30 to i32 (i8*)*
  %_31 = call i32 %_157(i8* dereferenceable_or_null(8) %_17)
  %_32 = icmp eq i32 %_29, %_31
  br i1 %_32, label %_24.0, label %_25.0
_24.0:
  br label %_35.0
_35.0:
  %_40 = invoke i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM30scala.collection.Map$$Lambda$1G4type" to i8*), i64 24) to label %_35.1 unwind label %_94.landingpad
_35.1:
  invoke nonnull dereferenceable(8) i8* @"_SM30scala.collection.Map$$Lambda$1RL20scala.collection.MapL20scala.collection.MapE"(i8* nonnull dereferenceable(24) %_40, i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_17) to label %_35.2 unwind label %_97.landingpad
_35.2:
  %_103 = icmp ne i8* %_1, null
  br i1 %_103, label %_100.0, label %_101.0
_100.0:
  %_158 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM33scala.collection.AbstractIterableD6forallL15scala.Function1zEO" to i8*) to i1 (i8*, i8*)*
  %_46 = invoke i1 %_158(i8* dereferenceable_or_null(8) %_1, i8* nonnull dereferenceable(24) %_40) to label %_100.1 unwind label %_106.landingpad
_100.1:
  br label %_36.0
_33.0:
  %_37 = phi i8* [%_45, %_105.0], [%_43, %_99.0], [%_41, %_95.0], [%_39, %_93.0]
  %_110 = icmp eq i8* %_37, null
  br i1 %_110, label %_107.0, label %_108.0
_107.0:
  br label %_109.0
_108.0:
  %_159 = bitcast i8* %_37 to i8**
  %_111 = load i8*, i8** %_159
  %_112 = icmp eq i8* %_111, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM28java.lang.ClassCastExceptionG4type" to i8*)
  br label %_109.0
_109.0:
  %_47 = phi i1 [%_112, %_108.0], [false, %_107.0]
  br i1 %_47, label %_48.0, label %_49.0
_48.0:
  br label %_36.0
_49.0:
  %_114 = icmp ne i8* %_37, null
  br i1 %_114, label %_113.0, label %_77.0
_113.0:
  call i8* @"scalanative_throw"(i8* dereferenceable_or_null(8) %_37)
  unreachable
_36.0:
  %_38 = phi i1 [%_46, %_100.1], [false, %_48.0]
  br label %_26.0
_25.0:
  br label %_26.0
_26.0:
  %_27 = phi i1 [false, %_25.0], [%_38, %_36.0]
  br label %_11.0
_19.0:
  br label %_10.0
_13.0:
  br label %_10.0
_10.0:
  br label %_11.0
_11.0:
  %_57 = phi i1 [false, %_10.0], [%_27, %_26.0]
  br label %_6.0
_6.0:
  %_7 = phi i1 [%_57, %_11.0], [true, %_4.0]
  ret i1 %_7
_77.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_101.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_101.1 unwind label %_117.landingpad
_101.1:
  unreachable
_69.0:
  %_119 = phi i8* [%_2, %_67.0]
  %_120 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM20scala.collection.MapG4type" to i8*), %_67.0]
  %_160 = bitcast i8* %_119 to i8**
  %_121 = load i8*, i8** %_160
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_121, i8* %_120)
  unreachable
_93.0:
  %_39 = phi i8* [%_94, %_94.landingpad.succ]
  br label %_33.0
_95.0:
  %_41 = phi i8* [%_97, %_97.landingpad.succ]
  br label %_33.0
_99.0:
  %_43 = phi i8* [%_102, %_102.landingpad.succ], [%_117, %_117.landingpad.succ], [%_104, %_104.landingpad.succ]
  br label %_33.0
_105.0:
  %_45 = phi i8* [%_106, %_106.landingpad.succ]
  br label %_33.0
_94.landingpad:
  %_161 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_162 = extractvalue { i8*, i32 } %_161, 0
  %_163 = extractvalue { i8*, i32 } %_161, 1
  %_164 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_165 = icmp eq i32 %_163, %_164
  br i1 %_165, label %_94.landingpad.succ, label %_94.landingpad.fail
_94.landingpad.succ:
  %_166 = call i8* @__cxa_begin_catch(i8* %_162)
  %_167 = bitcast i8* %_166 to i8**
  %_168 = getelementptr i8*, i8** %_167, i32 1
  %_94 = load i8*, i8** %_168
  call void @__cxa_end_catch()
  br label %_93.0
_94.landingpad.fail:
  resume { i8*, i32 } %_161
_97.landingpad:
  %_169 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_170 = extractvalue { i8*, i32 } %_169, 0
  %_171 = extractvalue { i8*, i32 } %_169, 1
  %_172 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_173 = icmp eq i32 %_171, %_172
  br i1 %_173, label %_97.landingpad.succ, label %_97.landingpad.fail
_97.landingpad.succ:
  %_174 = call i8* @__cxa_begin_catch(i8* %_170)
  %_175 = bitcast i8* %_174 to i8**
  %_176 = getelementptr i8*, i8** %_175, i32 1
  %_97 = load i8*, i8** %_176
  call void @__cxa_end_catch()
  br label %_95.0
_97.landingpad.fail:
  resume { i8*, i32 } %_169
_102.landingpad:
  %_177 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_178 = extractvalue { i8*, i32 } %_177, 0
  %_179 = extractvalue { i8*, i32 } %_177, 1
  %_180 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_181 = icmp eq i32 %_179, %_180
  br i1 %_181, label %_102.landingpad.succ, label %_102.landingpad.fail
_102.landingpad.succ:
  %_182 = call i8* @__cxa_begin_catch(i8* %_178)
  %_183 = bitcast i8* %_182 to i8**
  %_184 = getelementptr i8*, i8** %_183, i32 1
  %_102 = load i8*, i8** %_184
  call void @__cxa_end_catch()
  br label %_99.0
_102.landingpad.fail:
  resume { i8*, i32 } %_177
_104.landingpad:
  %_185 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_186 = extractvalue { i8*, i32 } %_185, 0
  %_187 = extractvalue { i8*, i32 } %_185, 1
  %_188 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_189 = icmp eq i32 %_187, %_188
  br i1 %_189, label %_104.landingpad.succ, label %_104.landingpad.fail
_104.landingpad.succ:
  %_190 = call i8* @__cxa_begin_catch(i8* %_186)
  %_191 = bitcast i8* %_190 to i8**
  %_192 = getelementptr i8*, i8** %_191, i32 1
  %_104 = load i8*, i8** %_192
  call void @__cxa_end_catch()
  br label %_99.0
_104.landingpad.fail:
  resume { i8*, i32 } %_185
_106.landingpad:
  %_193 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_194 = extractvalue { i8*, i32 } %_193, 0
  %_195 = extractvalue { i8*, i32 } %_193, 1
  %_196 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_197 = icmp eq i32 %_195, %_196
  br i1 %_197, label %_106.landingpad.succ, label %_106.landingpad.fail
_106.landingpad.succ:
  %_198 = call i8* @__cxa_begin_catch(i8* %_194)
  %_199 = bitcast i8* %_198 to i8**
  %_200 = getelementptr i8*, i8** %_199, i32 1
  %_106 = load i8*, i8** %_200
  call void @__cxa_end_catch()
  br label %_105.0
_106.landingpad.fail:
  resume { i8*, i32 } %_193
_117.landingpad:
  %_201 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_202 = extractvalue { i8*, i32 } %_201, 0
  %_203 = extractvalue { i8*, i32 } %_201, 1
  %_204 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_205 = icmp eq i32 %_203, %_204
  br i1 %_205, label %_117.landingpad.succ, label %_117.landingpad.fail
_117.landingpad.succ:
  %_206 = call i8* @__cxa_begin_catch(i8* %_202)
  %_207 = bitcast i8* %_206 to i8**
  %_208 = getelementptr i8*, i8** %_207, i32 1
  %_117 = load i8*, i8** %_208
  call void @__cxa_end_catch()
  br label %_99.0
_117.landingpad.fail:
  resume { i8*, i32 } %_201
}

define i1 @"_SM20scala.collection.MapD8canEqualL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i1 true
}

define i32 @"_SM20scala.collection.MapD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(24) i8* @"_SM31scala.util.hashing.MurmurHash3$G4load"()
  %_20002 = call i32 @"_SM31scala.util.hashing.MurmurHash3$D7mapHashL20scala.collection.MapiEO"(i8* nonnull dereferenceable(24) %_20001, i8* dereferenceable_or_null(8) %_1)
  ret i32 %_20002
}

define dereferenceable_or_null(32) i8* @"_SM20scala.collection.MapD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM25scala.collection.IterableD8toStringL16java.lang.StringEO"(i8* dereferenceable_or_null(8) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM21java.lang.ThreadLocalD3getL16java.lang.ObjectEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_50004 = icmp ne i8* %_1, null
  br i1 %_50004, label %_50002.0, label %_50003.0
_50002.0:
  %_50011 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_50012 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_50011, i32 0, i32 2
  %_50005 = bitcast i8** %_50012 to i8*
  %_50013 = bitcast i8* %_50005 to i8**
  %_20002 = load i8*, i8** %_50013, !dereferenceable_or_null !{i64 16}
  %_20003 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_20004 = call i1 @"_SM13scala.Predef$D15Boolean2booleanL17java.lang.BooleanzEO"(i8* nonnull dereferenceable(16) %_20003, i8* dereferenceable_or_null(16) %_20002)
  %_20006 = xor i1 %_20004, true
  br i1 %_20006, label %_30000.0, label %_40000.0
_30000.0:
  %_50014 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.util.DynamicVariable$$anon$1D12initialValueL16java.lang.ObjectEO" to i8*) to i8* (i8*)*
  %_30001 = call dereferenceable_or_null(8) i8* %_50014(i8* dereferenceable_or_null(24) %_1)
  call nonnull dereferenceable(8) i8* @"_SM21java.lang.ThreadLocalD3setL16java.lang.ObjectuEO"(i8* dereferenceable_or_null(24) %_1, i8* dereferenceable_or_null(8) %_30001)
  br label %_50000.0
_40000.0:
  br label %_50000.0
_50000.0:
  %_50008 = icmp ne i8* %_1, null
  br i1 %_50008, label %_50007.0, label %_50003.0
_50007.0:
  %_50015 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_50016 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_50015, i32 0, i32 1
  %_50009 = bitcast i8** %_50016 to i8*
  %_50017 = bitcast i8* %_50009 to i8**
  %_50001 = load i8*, i8** %_50017, !dereferenceable_or_null !{i64 8}
  ret i8* %_50001
_50003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM21java.lang.ThreadLocalD3setL16java.lang.ObjectuEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30009 = icmp ne i8* %_1, null
  br i1 %_30009, label %_30007.0, label %_30008.0
_30007.0:
  %_30016 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_30017 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_30016, i32 0, i32 1
  %_30010 = bitcast i8** %_30017 to i8*
  %_30018 = bitcast i8* %_30010 to i8**
  store i8* %_2, i8** %_30018
  %_30003 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_30019 = bitcast i8* bitcast (i8* (i8*, i1)* @"_SM13scala.Predef$D15boolean2BooleanzL17java.lang.BooleanEO" to i8*) to i8* (i8*, i1)*
  %_30004 = call dereferenceable_or_null(16) i8* %_30019(i8* nonnull dereferenceable(16) %_30003, i1 true)
  %_30013 = icmp ne i8* %_1, null
  br i1 %_30013, label %_30012.0, label %_30008.0
_30012.0:
  %_30020 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_30021 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_30020, i32 0, i32 2
  %_30014 = bitcast i8** %_30021 to i8*
  %_30022 = bitcast i8* %_30014 to i8**
  store i8* %_30004, i8** %_30022
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_30008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM21java.util.package$BoxD12productArityiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i32 1
}

define nonnull dereferenceable(32) i8* @"_SM21java.util.package$BoxD13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-193" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM21java.util.package$BoxD14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_40002 = icmp eq i32 %_2, 0
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_50001 = call dereferenceable_or_null(8) i8* @"_SM21java.util.package$BoxD2_1L16java.lang.ObjectEO"(i8* dereferenceable_or_null(16) %_1)
  br label %_70000.0
_60000.0:
  br label %_80000.0
_70000.0:
  ret i8* %_50001
_80000.0:
  %_80003 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_2)
  %_180008 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.lang.IntegerD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_80004 = call dereferenceable_or_null(32) i8* %_180008(i8* nonnull dereferenceable(16) %_80003)
  br label %_170000.0
_170000.0:
  %_170001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM35java.lang.IndexOutOfBoundsExceptionG4type" to i8*), i64 40)
  %_180009 = bitcast i8* %_170001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_180010 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_180009, i32 0, i32 5
  %_180002 = bitcast i1* %_180010 to i8*
  %_180011 = bitcast i8* %_180002 to i1*
  store i1 true, i1* %_180011
  %_180012 = bitcast i8* %_170001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_180013 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_180012, i32 0, i32 4
  %_180004 = bitcast i1* %_180013 to i8*
  %_180014 = bitcast i8* %_180004 to i1*
  store i1 true, i1* %_180014
  %_180015 = bitcast i8* %_170001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_180016 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_180015, i32 0, i32 3
  %_180006 = bitcast i8** %_180016 to i8*
  %_180017 = bitcast i8* %_180006 to i8**
  store i8* %_80004, i8** %_180017
  %_170005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_170001)
  br label %_180000.0
_180000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_170001)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM21java.util.package$BoxD15productIteratorL25scala.collection.IteratorEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = bitcast i8* bitcast (i8* (i8*)* @"_SM13scala.ProductD15productIteratorL25scala.collection.IteratorEO" to i8*) to i8* (i8*)*
  %_20001 = call dereferenceable_or_null(8) i8* %_20002(i8* dereferenceable_or_null(16) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM21java.util.package$BoxD2_1L16java.lang.ObjectEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i8* }*
  %_30008 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30007, i32 0, i32 1
  %_30005 = bitcast i8** %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i8**
  %_30001 = load i8*, i8** %_30009, !dereferenceable_or_null !{i64 8}
  ret i8* %_30001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM21java.util.package$BoxD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_100004 = icmp eq i8* %_2, null
  br i1 %_100004, label %_100001.0, label %_100002.0
_100001.0:
  br label %_100003.0
_100002.0:
  %_100025 = bitcast i8* %_2 to i8**
  %_100005 = load i8*, i8** %_100025
  %_100006 = icmp eq i8* %_100005, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM21java.util.package$BoxG4type" to i8*)
  br label %_100003.0
_100003.0:
  %_40002 = phi i1 [%_100006, %_100002.0], [false, %_100001.0]
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_100010 = icmp eq i8* %_2, null
  br i1 %_100010, label %_100008.0, label %_100007.0
_100007.0:
  %_100026 = bitcast i8* %_2 to i8**
  %_100011 = load i8*, i8** %_100026
  %_100012 = icmp eq i8* %_100011, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM21java.util.package$BoxG4type" to i8*)
  br i1 %_100012, label %_100008.0, label %_100009.0
_100008.0:
  %_50001 = bitcast i8* %_2 to i8*
  %_100015 = icmp ne i8* %_1, null
  br i1 %_100015, label %_100013.0, label %_100014.0
_100013.0:
  %_100027 = bitcast i8* %_1 to { i8*, i8* }*
  %_100028 = getelementptr { i8*, i8* }, { i8*, i8* }* %_100027, i32 0, i32 1
  %_100016 = bitcast i8** %_100028 to i8*
  %_100029 = bitcast i8* %_100016 to i8**
  %_70001 = load i8*, i8** %_100029, !dereferenceable_or_null !{i64 8}
  %_50004 = call dereferenceable_or_null(8) i8* @"_SM18java.util.package$D19CompareNullablesOpsL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM18java.util.package$G8instance" to i8*), i8* dereferenceable_or_null(8) %_70001)
  %_100018 = icmp ne i8* %_50001, null
  br i1 %_100018, label %_100017.0, label %_100014.0
_100017.0:
  %_100030 = bitcast i8* %_50001 to { i8*, i8* }*
  %_100031 = getelementptr { i8*, i8* }, { i8*, i8* }* %_100030, i32 0, i32 1
  %_100019 = bitcast i8** %_100031 to i8*
  %_100032 = bitcast i8* %_100019 to i8**
  %_80001 = load i8*, i8** %_100032, !dereferenceable_or_null !{i64 8}
  %_50005 = call i1 @"_SM38java.util.package$CompareNullablesOps$D19$eq$eq$eq$extensionL16java.lang.ObjectL16java.lang.ObjectzEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM38java.util.package$CompareNullablesOps$G8instance" to i8*), i8* dereferenceable_or_null(8) %_50004, i8* dereferenceable_or_null(8) %_80001)
  br label %_90000.0
_60000.0:
  br label %_100000.0
_100000.0:
  br label %_90000.0
_90000.0:
  %_90001 = phi i1 [false, %_100000.0], [%_50005, %_100017.0]
  ret i1 %_90001
_100014.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_100009.0:
  %_100021 = phi i8* [%_2, %_100007.0]
  %_100022 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM21java.util.package$BoxG4type" to i8*), %_100007.0]
  %_100033 = bitcast i8* %_100021 to i8**
  %_100023 = load i8*, i8** %_100033
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_100023, i8* %_100022)
  unreachable
}

define i32 @"_SM21java.util.package$BoxD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_70004 = icmp ne i8* %_1, null
  br i1 %_70004, label %_70002.0, label %_70003.0
_70002.0:
  %_70014 = bitcast i8* %_1 to { i8*, i8* }*
  %_70015 = getelementptr { i8*, i8* }, { i8*, i8* }* %_70014, i32 0, i32 1
  %_70005 = bitcast i8** %_70015 to i8*
  %_70016 = bitcast i8* %_70005 to i8**
  %_30001 = load i8*, i8** %_70016, !dereferenceable_or_null !{i64 8}
  %_20002 = icmp eq i8* %_30001, null
  br i1 %_20002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  %_70007 = icmp ne i8* %_1, null
  br i1 %_70007, label %_70006.0, label %_70003.0
_70006.0:
  %_70017 = bitcast i8* %_1 to { i8*, i8* }*
  %_70018 = getelementptr { i8*, i8* }, { i8*, i8* }* %_70017, i32 0, i32 1
  %_70008 = bitcast i8** %_70018 to i8*
  %_70019 = bitcast i8* %_70008 to i8**
  %_70001 = load i8*, i8** %_70019, !dereferenceable_or_null !{i64 8}
  %_70010 = icmp ne i8* %_70001, null
  br i1 %_70010, label %_70009.0, label %_70003.0
_70009.0:
  %_70020 = bitcast i8* %_70001 to i8**
  %_70011 = load i8*, i8** %_70020
  %_70021 = bitcast i8* %_70011 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_70022 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_70021, i32 0, i32 4, i32 2
  %_70012 = bitcast i8** %_70022 to i8*
  %_70023 = bitcast i8* %_70012 to i8**
  %_50002 = load i8*, i8** %_70023
  %_70024 = bitcast i8* %_50002 to i32 (i8*)*
  %_50003 = call i32 %_70024(i8* dereferenceable_or_null(8) %_70001)
  br label %_60000.0
_60000.0:
  %_60001 = phi i32 [%_50003, %_70009.0], [0, %_40000.0]
  ret i32 %_60001
_70003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM21java.util.package$BoxD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = call dereferenceable_or_null(32) i8* @"_SM27scala.runtime.ScalaRunTime$D9_toStringL13scala.ProductL16java.lang.StringEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(16) %_1)
  ret i8* %_20002
}

define nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(8) i8* @"_SM21scala.collection.ViewD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(32) i8* @"_SM21scala.collection.ViewD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_80005 = icmp ne i8* %_1, null
  br i1 %_80005, label %_80003.0, label %_80004.0
_80003.0:
  %_80012 = bitcast i8* %_1 to i8**
  %_80006 = load i8*, i8** %_80012
  %_80013 = bitcast i8* %_80006 to { i8*, i32, i32, i8* }*
  %_80014 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_80013, i32 0, i32 2
  %_80007 = bitcast i32* %_80014 to i8*
  %_80015 = bitcast i8* %_80007 to i32*
  %_80008 = load i32, i32* %_80015
  %_80016 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_80017 = getelementptr i8*, i8** %_80016, i32 2609
  %_80009 = bitcast i8** %_80017 to i8*
  %_80018 = bitcast i8* %_80009 to i8**
  %_80019 = getelementptr i8*, i8** %_80018, i32 %_80008
  %_80010 = bitcast i8** %_80019 to i8*
  %_80020 = bitcast i8* %_80010 to i8**
  %_20002 = load i8*, i8** %_80020
  %_80021 = bitcast i8* %_20002 to i8* (i8*)*
  %_20003 = call dereferenceable_or_null(32) i8* %_80021(i8* dereferenceable_or_null(8) %_1)
  %_20005 = icmp eq i8* %_20003, null
  br i1 %_20005, label %_30000.0, label %_40000.0
_30000.0:
  br label %_50000.0
_40000.0:
  br label %_50000.0
_50000.0:
  %_50001 = phi i8* [%_20003, %_40000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_30000.0]
  %_50005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-195" to i8*), null
  br i1 %_50005, label %_60000.0, label %_70000.0
_60000.0:
  br label %_80000.0
_70000.0:
  br label %_80000.0
_80000.0:
  %_80001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-195" to i8*), %_70000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_60000.0]
  %_80022 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_80002 = call dereferenceable_or_null(32) i8* %_80022(i8* dereferenceable_or_null(32) %_50001, i8* nonnull dereferenceable(32) %_80001)
  ret i8* %_80002
_80004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM23scala.collection.SeqOpsD12sameElementsL29scala.collection.IterableOncezEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_120004 = icmp ne i8* %_1, null
  br i1 %_120004, label %_120002.0, label %_120003.0
_120002.0:
  %_120032 = bitcast i8* %_1 to i8**
  %_120005 = load i8*, i8** %_120032
  %_120033 = bitcast i8* %_120005 to { i8*, i32, i32, i8* }*
  %_120034 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_120033, i32 0, i32 2
  %_120006 = bitcast i32* %_120034 to i8*
  %_120035 = bitcast i8* %_120006 to i32*
  %_120007 = load i32, i32* %_120035
  %_120036 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_120037 = getelementptr i8*, i8** %_120036, i32 669
  %_120008 = bitcast i8** %_120037 to i8*
  %_120038 = bitcast i8* %_120008 to i8**
  %_120039 = getelementptr i8*, i8** %_120038, i32 %_120007
  %_120009 = bitcast i8** %_120039 to i8*
  %_120040 = bitcast i8* %_120009 to i8**
  %_30002 = load i8*, i8** %_120040
  %_120041 = bitcast i8* %_30002 to i32 (i8*)*
  %_30003 = call i32 %_120041(i8* dereferenceable_or_null(8) %_1)
  %_30005 = icmp ne i32 %_30003, -1
  br i1 %_30005, label %_40000.0, label %_50000.0
_40000.0:
  %_120011 = icmp ne i8* %_2, null
  br i1 %_120011, label %_120010.0, label %_120003.0
_120010.0:
  %_120042 = bitcast i8* %_2 to i8**
  %_120012 = load i8*, i8** %_120042
  %_120043 = bitcast i8* %_120012 to { i8*, i32, i32, i8* }*
  %_120044 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_120043, i32 0, i32 2
  %_120013 = bitcast i32* %_120044 to i8*
  %_120045 = bitcast i8* %_120013 to i32*
  %_120014 = load i32, i32* %_120045
  %_120046 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_120047 = getelementptr i8*, i8** %_120046, i32 669
  %_120015 = bitcast i8** %_120047 to i8*
  %_120048 = bitcast i8* %_120015 to i8**
  %_120049 = getelementptr i8*, i8** %_120048, i32 %_120014
  %_120016 = bitcast i8** %_120049 to i8*
  %_120050 = bitcast i8* %_120016 to i8**
  %_40002 = load i8*, i8** %_120050
  %_120051 = bitcast i8* %_40002 to i32 (i8*)*
  %_40003 = call i32 %_120051(i8* dereferenceable_or_null(8) %_2)
  %_40005 = icmp ne i32 %_40003, -1
  br i1 %_40005, label %_60000.0, label %_70000.0
_60000.0:
  %_60002 = icmp ne i32 %_30003, %_40003
  br label %_80000.0
_70000.0:
  br label %_80000.0
_80000.0:
  %_80001 = phi i1 [false, %_70000.0], [%_60002, %_60000.0]
  br label %_90000.0
_50000.0:
  br label %_90000.0
_90000.0:
  %_90001 = phi i1 [false, %_50000.0], [%_80001, %_80000.0]
  %_90003 = xor i1 %_90001, true
  br i1 %_90003, label %_100000.0, label %_110000.0
_100000.0:
  %_120018 = icmp ne i8* %_1, null
  br i1 %_120018, label %_120017.0, label %_120003.0
_120017.0:
  %_120052 = bitcast i8* %_1 to i8**
  %_120019 = load i8*, i8** %_120052
  %_120053 = bitcast i8* %_120019 to { i8*, i32, i32, i8* }*
  %_120054 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_120053, i32 0, i32 2
  %_120020 = bitcast i32* %_120054 to i8*
  %_120055 = bitcast i8* %_120020 to i32*
  %_120021 = load i32, i32* %_120055
  %_120056 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_120057 = getelementptr i8*, i8** %_120056, i32 446
  %_120022 = bitcast i8** %_120057 to i8*
  %_120058 = bitcast i8* %_120022 to i8**
  %_120059 = getelementptr i8*, i8** %_120058, i32 %_120021
  %_120023 = bitcast i8** %_120059 to i8*
  %_120060 = bitcast i8* %_120023 to i8**
  %_100002 = load i8*, i8** %_120060
  %_120061 = bitcast i8* %_100002 to i8* (i8*)*
  %_100003 = call dereferenceable_or_null(8) i8* %_120061(i8* dereferenceable_or_null(8) %_1)
  %_120025 = icmp ne i8* %_100003, null
  br i1 %_120025, label %_120024.0, label %_120003.0
_120024.0:
  %_120062 = bitcast i8* %_100003 to i8**
  %_120026 = load i8*, i8** %_120062
  %_120063 = bitcast i8* %_120026 to { i8*, i32, i32, i8* }*
  %_120064 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_120063, i32 0, i32 2
  %_120027 = bitcast i32* %_120064 to i8*
  %_120065 = bitcast i8* %_120027 to i32*
  %_120028 = load i32, i32* %_120065
  %_120066 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_120067 = getelementptr i8*, i8** %_120066, i32 3139
  %_120029 = bitcast i8** %_120067 to i8*
  %_120068 = bitcast i8* %_120029 to i8**
  %_120069 = getelementptr i8*, i8** %_120068, i32 %_120028
  %_120030 = bitcast i8** %_120069 to i8*
  %_120070 = bitcast i8* %_120030 to i8**
  %_100005 = load i8*, i8** %_120070
  %_120071 = bitcast i8* %_100005 to i1 (i8*, i8*)*
  %_100006 = call i1 %_120071(i8* dereferenceable_or_null(8) %_100003, i8* dereferenceable_or_null(8) %_2)
  br label %_120000.0
_110000.0:
  br label %_120000.0
_120000.0:
  %_120001 = phi i1 [false, %_110000.0], [%_100006, %_120024.0]
  ret i1 %_120001
_120003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM23scala.collection.SeqOpsD13lengthCompareiiEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30006 = icmp ne i8* %_1, null
  br i1 %_30006, label %_30004.0, label %_30005.0
_30004.0:
  %_30013 = bitcast i8* %_1 to i8**
  %_30007 = load i8*, i8** %_30013
  %_30014 = bitcast i8* %_30007 to { i8*, i32, i32, i8* }*
  %_30015 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30014, i32 0, i32 2
  %_30008 = bitcast i32* %_30015 to i8*
  %_30016 = bitcast i8* %_30008 to i32*
  %_30009 = load i32, i32* %_30016
  %_30017 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30018 = getelementptr i8*, i8** %_30017, i32 4349
  %_30010 = bitcast i8** %_30018 to i8*
  %_30019 = bitcast i8* %_30010 to i8**
  %_30020 = getelementptr i8*, i8** %_30019, i32 %_30009
  %_30011 = bitcast i8** %_30020 to i8*
  %_30021 = bitcast i8* %_30011 to i8**
  %_30002 = load i8*, i8** %_30021
  %_30022 = bitcast i8* %_30002 to i32 (i8*, i32)*
  %_30003 = call i32 %_30022(i8* dereferenceable_or_null(8) %_1, i32 %_2)
  ret i32 %_30003
_30005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM23scala.collection.SeqOpsD4sizeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20006 = icmp ne i8* %_1, null
  br i1 %_20006, label %_20004.0, label %_20005.0
_20004.0:
  %_20013 = bitcast i8* %_1 to i8**
  %_20007 = load i8*, i8** %_20013
  %_20014 = bitcast i8* %_20007 to { i8*, i32, i32, i8* }*
  %_20015 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_20014, i32 0, i32 2
  %_20008 = bitcast i32* %_20015 to i8*
  %_20016 = bitcast i8* %_20008 to i32*
  %_20009 = load i32, i32* %_20016
  %_20017 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_20018 = getelementptr i8*, i8** %_20017, i32 4009
  %_20010 = bitcast i8** %_20018 to i8*
  %_20019 = bitcast i8* %_20010 to i8**
  %_20020 = getelementptr i8*, i8** %_20019, i32 %_20009
  %_20011 = bitcast i8** %_20020 to i8*
  %_20021 = bitcast i8* %_20011 to i8**
  %_20002 = load i8*, i8** %_20021
  %_20022 = bitcast i8* %_20002 to i32 (i8*)*
  %_20003 = call i32 %_20022(i8* dereferenceable_or_null(8) %_1)
  ret i32 %_20003
_20005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM23scala.collection.SeqOpsD7isEmptyzEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20008 = icmp ne i8* %_1, null
  br i1 %_20008, label %_20006.0, label %_20007.0
_20006.0:
  %_20015 = bitcast i8* %_1 to i8**
  %_20009 = load i8*, i8** %_20015
  %_20016 = bitcast i8* %_20009 to { i8*, i32, i32, i8* }*
  %_20017 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_20016, i32 0, i32 2
  %_20010 = bitcast i32* %_20017 to i8*
  %_20018 = bitcast i8* %_20010 to i32*
  %_20011 = load i32, i32* %_20018
  %_20019 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_20020 = getelementptr i8*, i8** %_20019, i32 2474
  %_20012 = bitcast i8** %_20020 to i8*
  %_20021 = bitcast i8* %_20012 to i8**
  %_20022 = getelementptr i8*, i8** %_20021, i32 %_20011
  %_20013 = bitcast i8** %_20022 to i8*
  %_20023 = bitcast i8* %_20013 to i8**
  %_20002 = load i8*, i8** %_20023
  %_20024 = bitcast i8* %_20002 to i32 (i8*, i32)*
  %_20003 = call i32 %_20024(i8* dereferenceable_or_null(8) %_1, i32 0)
  %_20005 = icmp eq i32 %_20003, 0
  ret i1 %_20005
_20007.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(56) i8* @"_SM24java.nio.charset.CharsetD13cachedDecoderL31java.nio.charset.CharsetDecoderEPT24java.nio.charset.Charset"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_2.0:
  br label %_3.0
_3.0:
  br i1 true, label %_4.0, label %_5.0
_4.0:
  %_84 = icmp ne i8* %_1, null
  br i1 %_84, label %_82.0, label %_83.0
_82.0:
  %_197 = bitcast i8* %_1 to { i8*, i8*, i8*, i64 }*
  %_198 = getelementptr { i8*, i8*, i8*, i64 }, { i8*, i8*, i8*, i64 }* %_197, i32 0, i32 3
  %_85 = bitcast i64* %_198 to i8*
  %_8 = call i64 @"_SM35scala.scalanative.runtime.LazyVals$D3getR_jEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.runtime.LazyVals$G8instance" to i8*), i8* %_85)
  %_10 = call i64 @"_SM23scala.runtime.LazyVals$D5STATEjijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM23scala.runtime.LazyVals$G8instance" to i8*), i64 %_8, i32 0)
  %_15 = sext i32 3 to i64
  %_16 = icmp eq i64 %_10, %_15
  br i1 %_16, label %_11.0, label %_12.0
_11.0:
  %_87 = icmp ne i8* %_1, null
  br i1 %_87, label %_86.0, label %_83.0
_86.0:
  %_199 = bitcast i8* %_1 to { i8*, i8*, i8*, i64 }*
  %_200 = getelementptr { i8*, i8*, i8*, i64 }, { i8*, i8*, i8*, i64 }* %_199, i32 0, i32 2
  %_88 = bitcast i8** %_200 to i8*
  %_201 = bitcast i8* %_88 to i8**
  %_17 = load i8*, i8** %_201, !dereferenceable_or_null !{i64 56}
  ret i8* %_17
_12.0:
  %_23 = sext i32 0 to i64
  %_24 = icmp eq i64 %_10, %_23
  br i1 %_24, label %_19.0, label %_20.0
_19.0:
  %_90 = icmp ne i8* %_1, null
  br i1 %_90, label %_89.0, label %_83.0
_89.0:
  %_202 = bitcast i8* %_1 to { i8*, i8*, i8*, i64 }*
  %_203 = getelementptr { i8*, i8*, i8*, i64 }, { i8*, i8*, i8*, i64 }* %_202, i32 0, i32 3
  %_91 = bitcast i64* %_203 to i8*
  %_31 = call i1 @"_SM35scala.scalanative.runtime.LazyVals$D3CASR_jiizEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.runtime.LazyVals$G8instance" to i8*), i8* %_91, i64 %_8, i32 1, i32 0)
  br i1 %_31, label %_25.0, label %_26.0
_25.0:
  br label %_34.0
_34.0:
  %_96 = icmp ne i8* %_1, null
  br i1 %_96, label %_93.0, label %_94.0
_93.0:
  %_204 = bitcast i8* bitcast (i8* (i8*)* @"_SM17niocharset.UTF_8$D10newDecoderL31java.nio.charset.CharsetDecoderEO" to i8*) to i8* (i8*)*
  %_41 = invoke dereferenceable_or_null(56) i8* %_204(i8* dereferenceable_or_null(32) %_1) to label %_93.1 unwind label %_99.landingpad
_93.1:
  %_43 = invoke dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"() to label %_93.2 unwind label %_101.landingpad
_93.2:
  %_45 = invoke dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D7REPLACEL34java.nio.charset.CodingErrorActionEO"(i8* nonnull dereferenceable(32) %_43) to label %_93.3 unwind label %_103.landingpad
_93.3:
  %_108 = icmp ne i8* %_41, null
  br i1 %_108, label %_105.0, label %_106.0
_105.0:
  %_205 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM31java.nio.charset.CharsetDecoderD16onMalformedInputL34java.nio.charset.CodingErrorActionL31java.nio.charset.CharsetDecoderEO" to i8*) to i8* (i8*, i8*)*
  %_49 = invoke dereferenceable_or_null(56) i8* %_205(i8* dereferenceable_or_null(56) %_41, i8* dereferenceable_or_null(16) %_45) to label %_105.1 unwind label %_111.landingpad
_105.1:
  %_51 = invoke dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"() to label %_105.2 unwind label %_113.landingpad
_105.2:
  %_53 = invoke dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D7REPLACEL34java.nio.charset.CodingErrorActionEO"(i8* nonnull dereferenceable(32) %_51) to label %_105.3 unwind label %_115.landingpad
_105.3:
  %_120 = icmp ne i8* %_49, null
  br i1 %_120, label %_117.0, label %_118.0
_117.0:
  %_206 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM31java.nio.charset.CharsetDecoderD21onUnmappableCharacterL34java.nio.charset.CodingErrorActionL31java.nio.charset.CharsetDecoderEO" to i8*) to i8* (i8*, i8*)*
  %_57 = invoke dereferenceable_or_null(56) i8* %_206(i8* dereferenceable_or_null(56) %_49, i8* dereferenceable_or_null(16) %_53) to label %_117.1 unwind label %_123.landingpad
_117.1:
  %_129 = icmp ne i8* %_1, null
  br i1 %_129, label %_126.0, label %_127.0
_126.0:
  %_207 = bitcast i8* %_1 to { i8*, i8*, i8*, i64 }*
  %_208 = getelementptr { i8*, i8*, i8*, i64 }, { i8*, i8*, i8*, i64 }* %_207, i32 0, i32 2
  %_131 = bitcast i8** %_208 to i8*
  %_209 = bitcast i8* %_131 to i8**
  store i8* %_57, i8** %_209
  %_140 = icmp ne i8* %_1, null
  br i1 %_140, label %_137.0, label %_138.0
_137.0:
  %_210 = bitcast i8* %_1 to { i8*, i8*, i8*, i64 }*
  %_211 = getelementptr { i8*, i8*, i8*, i64 }, { i8*, i8*, i8*, i64 }* %_210, i32 0, i32 3
  %_142 = bitcast i64* %_211 to i8*
  invoke nonnull dereferenceable(8) i8* @"_SM35scala.scalanative.runtime.LazyVals$D7setFlagR_iiuEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.runtime.LazyVals$G8instance" to i8*), i8* %_142, i32 3, i32 0) to label %_137.1 unwind label %_146.landingpad
_137.1:
  ret i8* %_57
_32.0:
  %_36 = phi i8* [%_64, %_144.0], [%_62, %_136.0], [%_60, %_134.0], [%_58, %_124.0], [%_56, %_122.0], [%_54, %_116.0], [%_52, %_114.0], [%_50, %_112.0], [%_48, %_110.0], [%_46, %_104.0], [%_44, %_102.0], [%_42, %_100.0], [%_40, %_98.0], [%_38, %_92.0]
  %_151 = icmp eq i8* %_36, null
  br i1 %_151, label %_148.0, label %_149.0
_148.0:
  br label %_150.0
_149.0:
  %_212 = bitcast i8* %_36 to i8**
  %_152 = load i8*, i8** %_212
  %_213 = bitcast i8* %_152 to { i8*, i32, i32, i8* }*
  %_214 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_213, i32 0, i32 1
  %_153 = bitcast i32* %_214 to i8*
  %_215 = bitcast i8* %_153 to i32*
  %_154 = load i32, i32* %_215
  %_155 = icmp sle i32 126, %_154
  %_156 = icmp sle i32 %_154, 175
  %_157 = and i1 %_155, %_156
  br label %_150.0
_150.0:
  %_67 = phi i1 [%_157, %_149.0], [false, %_148.0]
  br i1 %_67, label %_68.0, label %_69.0
_68.0:
  %_161 = icmp eq i8* %_36, null
  br i1 %_161, label %_159.0, label %_158.0
_158.0:
  %_216 = bitcast i8* %_36 to i8**
  %_162 = load i8*, i8** %_216
  %_217 = bitcast i8* %_162 to { i8*, i32, i32, i8* }*
  %_218 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_217, i32 0, i32 1
  %_163 = bitcast i32* %_218 to i8*
  %_219 = bitcast i8* %_163 to i32*
  %_164 = load i32, i32* %_219
  %_165 = icmp sle i32 126, %_164
  %_166 = icmp sle i32 %_164, 175
  %_167 = and i1 %_165, %_166
  br i1 %_167, label %_159.0, label %_160.0
_159.0:
  %_72 = bitcast i8* %_36 to i8*
  %_169 = icmp ne i8* %_1, null
  br i1 %_169, label %_168.0, label %_83.0
_168.0:
  %_220 = bitcast i8* %_1 to { i8*, i8*, i8*, i64 }*
  %_221 = getelementptr { i8*, i8*, i8*, i64 }, { i8*, i8*, i8*, i64 }* %_220, i32 0, i32 3
  %_170 = bitcast i64* %_221 to i8*
  call nonnull dereferenceable(8) i8* @"_SM35scala.scalanative.runtime.LazyVals$D7setFlagR_iiuEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.runtime.LazyVals$G8instance" to i8*), i8* %_170, i32 0, i32 0)
  %_173 = icmp ne i8* %_72, null
  br i1 %_173, label %_172.0, label %_83.0
_172.0:
  call i8* @"scalanative_throw"(i8* dereferenceable_or_null(40) %_72)
  unreachable
_69.0:
  %_176 = icmp ne i8* %_36, null
  br i1 %_176, label %_175.0, label %_83.0
_175.0:
  call i8* @"scalanative_throw"(i8* dereferenceable_or_null(8) %_36)
  unreachable
_26.0:
  br label %_27.0
_27.0:
  %_28 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_26.0]
  br label %_21.0
_20.0:
  %_179 = icmp ne i8* %_1, null
  br i1 %_179, label %_178.0, label %_83.0
_178.0:
  %_222 = bitcast i8* %_1 to { i8*, i8*, i8*, i64 }*
  %_223 = getelementptr { i8*, i8*, i8*, i64 }, { i8*, i8*, i8*, i64 }* %_222, i32 0, i32 3
  %_180 = bitcast i64* %_223 to i8*
  %_224 = bitcast i8* bitcast (i8* (i8*, i8*, i64, i32)* @"_SM35scala.scalanative.runtime.LazyVals$D17wait4NotificationR_jiuEO" to i8*) to i8* (i8*, i8*, i64, i32)*
  call nonnull dereferenceable(8) i8* %_224(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.runtime.LazyVals$G8instance" to i8*), i8* %_180, i64 %_8, i32 0)
  br label %_21.0
_21.0:
  %_22 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_178.0], [%_28, %_27.0]
  br label %_13.0
_13.0:
  %_14 = phi i8* [%_22, %_21.0]
  br label %_3.0
_5.0:
  ret i8* zeroinitializer
_83.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_94.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_94.1 unwind label %_183.landingpad
_94.1:
  unreachable
_106.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_106.1 unwind label %_185.landingpad
_106.1:
  unreachable
_118.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_118.1 unwind label %_187.landingpad
_118.1:
  unreachable
_127.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_127.1 unwind label %_189.landingpad
_127.1:
  unreachable
_138.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_138.1 unwind label %_191.landingpad
_138.1:
  unreachable
_160.0:
  %_193 = phi i8* [%_36, %_158.0]
  %_194 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM19java.lang.ThrowableG4type" to i8*), %_158.0]
  %_225 = bitcast i8* %_193 to i8**
  %_195 = load i8*, i8** %_225
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_195, i8* %_194)
  unreachable
_92.0:
  %_38 = phi i8* [%_95, %_95.landingpad.succ], [%_183, %_183.landingpad.succ], [%_97, %_97.landingpad.succ]
  br label %_32.0
_98.0:
  %_40 = phi i8* [%_99, %_99.landingpad.succ]
  br label %_32.0
_100.0:
  %_42 = phi i8* [%_101, %_101.landingpad.succ]
  br label %_32.0
_102.0:
  %_44 = phi i8* [%_103, %_103.landingpad.succ]
  br label %_32.0
_104.0:
  %_46 = phi i8* [%_107, %_107.landingpad.succ], [%_185, %_185.landingpad.succ], [%_109, %_109.landingpad.succ]
  br label %_32.0
_110.0:
  %_48 = phi i8* [%_111, %_111.landingpad.succ]
  br label %_32.0
_112.0:
  %_50 = phi i8* [%_113, %_113.landingpad.succ]
  br label %_32.0
_114.0:
  %_52 = phi i8* [%_115, %_115.landingpad.succ]
  br label %_32.0
_116.0:
  %_54 = phi i8* [%_119, %_119.landingpad.succ], [%_187, %_187.landingpad.succ], [%_121, %_121.landingpad.succ]
  br label %_32.0
_122.0:
  %_56 = phi i8* [%_123, %_123.landingpad.succ]
  br label %_32.0
_124.0:
  %_58 = phi i8* [%_128, %_128.landingpad.succ], [%_189, %_189.landingpad.succ], [%_130, %_130.landingpad.succ], [%_132, %_132.landingpad.succ]
  br label %_32.0
_134.0:
  %_60 = phi i8* [%_135, %_135.landingpad.succ]
  br label %_32.0
_136.0:
  %_62 = phi i8* [%_139, %_139.landingpad.succ], [%_191, %_191.landingpad.succ], [%_141, %_141.landingpad.succ], [%_143, %_143.landingpad.succ]
  br label %_32.0
_144.0:
  %_64 = phi i8* [%_146, %_146.landingpad.succ]
  br label %_32.0
_95.landingpad:
  %_226 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_227 = extractvalue { i8*, i32 } %_226, 0
  %_228 = extractvalue { i8*, i32 } %_226, 1
  %_229 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_230 = icmp eq i32 %_228, %_229
  br i1 %_230, label %_95.landingpad.succ, label %_95.landingpad.fail
_95.landingpad.succ:
  %_231 = call i8* @__cxa_begin_catch(i8* %_227)
  %_232 = bitcast i8* %_231 to i8**
  %_233 = getelementptr i8*, i8** %_232, i32 1
  %_95 = load i8*, i8** %_233
  call void @__cxa_end_catch()
  br label %_92.0
_95.landingpad.fail:
  resume { i8*, i32 } %_226
_97.landingpad:
  %_234 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_235 = extractvalue { i8*, i32 } %_234, 0
  %_236 = extractvalue { i8*, i32 } %_234, 1
  %_237 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_238 = icmp eq i32 %_236, %_237
  br i1 %_238, label %_97.landingpad.succ, label %_97.landingpad.fail
_97.landingpad.succ:
  %_239 = call i8* @__cxa_begin_catch(i8* %_235)
  %_240 = bitcast i8* %_239 to i8**
  %_241 = getelementptr i8*, i8** %_240, i32 1
  %_97 = load i8*, i8** %_241
  call void @__cxa_end_catch()
  br label %_92.0
_97.landingpad.fail:
  resume { i8*, i32 } %_234
_99.landingpad:
  %_242 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_243 = extractvalue { i8*, i32 } %_242, 0
  %_244 = extractvalue { i8*, i32 } %_242, 1
  %_245 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_246 = icmp eq i32 %_244, %_245
  br i1 %_246, label %_99.landingpad.succ, label %_99.landingpad.fail
_99.landingpad.succ:
  %_247 = call i8* @__cxa_begin_catch(i8* %_243)
  %_248 = bitcast i8* %_247 to i8**
  %_249 = getelementptr i8*, i8** %_248, i32 1
  %_99 = load i8*, i8** %_249
  call void @__cxa_end_catch()
  br label %_98.0
_99.landingpad.fail:
  resume { i8*, i32 } %_242
_101.landingpad:
  %_250 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_251 = extractvalue { i8*, i32 } %_250, 0
  %_252 = extractvalue { i8*, i32 } %_250, 1
  %_253 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_254 = icmp eq i32 %_252, %_253
  br i1 %_254, label %_101.landingpad.succ, label %_101.landingpad.fail
_101.landingpad.succ:
  %_255 = call i8* @__cxa_begin_catch(i8* %_251)
  %_256 = bitcast i8* %_255 to i8**
  %_257 = getelementptr i8*, i8** %_256, i32 1
  %_101 = load i8*, i8** %_257
  call void @__cxa_end_catch()
  br label %_100.0
_101.landingpad.fail:
  resume { i8*, i32 } %_250
_103.landingpad:
  %_258 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_259 = extractvalue { i8*, i32 } %_258, 0
  %_260 = extractvalue { i8*, i32 } %_258, 1
  %_261 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_262 = icmp eq i32 %_260, %_261
  br i1 %_262, label %_103.landingpad.succ, label %_103.landingpad.fail
_103.landingpad.succ:
  %_263 = call i8* @__cxa_begin_catch(i8* %_259)
  %_264 = bitcast i8* %_263 to i8**
  %_265 = getelementptr i8*, i8** %_264, i32 1
  %_103 = load i8*, i8** %_265
  call void @__cxa_end_catch()
  br label %_102.0
_103.landingpad.fail:
  resume { i8*, i32 } %_258
_107.landingpad:
  %_266 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_267 = extractvalue { i8*, i32 } %_266, 0
  %_268 = extractvalue { i8*, i32 } %_266, 1
  %_269 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_270 = icmp eq i32 %_268, %_269
  br i1 %_270, label %_107.landingpad.succ, label %_107.landingpad.fail
_107.landingpad.succ:
  %_271 = call i8* @__cxa_begin_catch(i8* %_267)
  %_272 = bitcast i8* %_271 to i8**
  %_273 = getelementptr i8*, i8** %_272, i32 1
  %_107 = load i8*, i8** %_273
  call void @__cxa_end_catch()
  br label %_104.0
_107.landingpad.fail:
  resume { i8*, i32 } %_266
_109.landingpad:
  %_274 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_275 = extractvalue { i8*, i32 } %_274, 0
  %_276 = extractvalue { i8*, i32 } %_274, 1
  %_277 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_278 = icmp eq i32 %_276, %_277
  br i1 %_278, label %_109.landingpad.succ, label %_109.landingpad.fail
_109.landingpad.succ:
  %_279 = call i8* @__cxa_begin_catch(i8* %_275)
  %_280 = bitcast i8* %_279 to i8**
  %_281 = getelementptr i8*, i8** %_280, i32 1
  %_109 = load i8*, i8** %_281
  call void @__cxa_end_catch()
  br label %_104.0
_109.landingpad.fail:
  resume { i8*, i32 } %_274
_111.landingpad:
  %_282 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_283 = extractvalue { i8*, i32 } %_282, 0
  %_284 = extractvalue { i8*, i32 } %_282, 1
  %_285 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_286 = icmp eq i32 %_284, %_285
  br i1 %_286, label %_111.landingpad.succ, label %_111.landingpad.fail
_111.landingpad.succ:
  %_287 = call i8* @__cxa_begin_catch(i8* %_283)
  %_288 = bitcast i8* %_287 to i8**
  %_289 = getelementptr i8*, i8** %_288, i32 1
  %_111 = load i8*, i8** %_289
  call void @__cxa_end_catch()
  br label %_110.0
_111.landingpad.fail:
  resume { i8*, i32 } %_282
_113.landingpad:
  %_290 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_291 = extractvalue { i8*, i32 } %_290, 0
  %_292 = extractvalue { i8*, i32 } %_290, 1
  %_293 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_294 = icmp eq i32 %_292, %_293
  br i1 %_294, label %_113.landingpad.succ, label %_113.landingpad.fail
_113.landingpad.succ:
  %_295 = call i8* @__cxa_begin_catch(i8* %_291)
  %_296 = bitcast i8* %_295 to i8**
  %_297 = getelementptr i8*, i8** %_296, i32 1
  %_113 = load i8*, i8** %_297
  call void @__cxa_end_catch()
  br label %_112.0
_113.landingpad.fail:
  resume { i8*, i32 } %_290
_115.landingpad:
  %_298 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_299 = extractvalue { i8*, i32 } %_298, 0
  %_300 = extractvalue { i8*, i32 } %_298, 1
  %_301 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_302 = icmp eq i32 %_300, %_301
  br i1 %_302, label %_115.landingpad.succ, label %_115.landingpad.fail
_115.landingpad.succ:
  %_303 = call i8* @__cxa_begin_catch(i8* %_299)
  %_304 = bitcast i8* %_303 to i8**
  %_305 = getelementptr i8*, i8** %_304, i32 1
  %_115 = load i8*, i8** %_305
  call void @__cxa_end_catch()
  br label %_114.0
_115.landingpad.fail:
  resume { i8*, i32 } %_298
_119.landingpad:
  %_306 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_307 = extractvalue { i8*, i32 } %_306, 0
  %_308 = extractvalue { i8*, i32 } %_306, 1
  %_309 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_310 = icmp eq i32 %_308, %_309
  br i1 %_310, label %_119.landingpad.succ, label %_119.landingpad.fail
_119.landingpad.succ:
  %_311 = call i8* @__cxa_begin_catch(i8* %_307)
  %_312 = bitcast i8* %_311 to i8**
  %_313 = getelementptr i8*, i8** %_312, i32 1
  %_119 = load i8*, i8** %_313
  call void @__cxa_end_catch()
  br label %_116.0
_119.landingpad.fail:
  resume { i8*, i32 } %_306
_121.landingpad:
  %_314 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_315 = extractvalue { i8*, i32 } %_314, 0
  %_316 = extractvalue { i8*, i32 } %_314, 1
  %_317 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_318 = icmp eq i32 %_316, %_317
  br i1 %_318, label %_121.landingpad.succ, label %_121.landingpad.fail
_121.landingpad.succ:
  %_319 = call i8* @__cxa_begin_catch(i8* %_315)
  %_320 = bitcast i8* %_319 to i8**
  %_321 = getelementptr i8*, i8** %_320, i32 1
  %_121 = load i8*, i8** %_321
  call void @__cxa_end_catch()
  br label %_116.0
_121.landingpad.fail:
  resume { i8*, i32 } %_314
_123.landingpad:
  %_322 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_323 = extractvalue { i8*, i32 } %_322, 0
  %_324 = extractvalue { i8*, i32 } %_322, 1
  %_325 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_326 = icmp eq i32 %_324, %_325
  br i1 %_326, label %_123.landingpad.succ, label %_123.landingpad.fail
_123.landingpad.succ:
  %_327 = call i8* @__cxa_begin_catch(i8* %_323)
  %_328 = bitcast i8* %_327 to i8**
  %_329 = getelementptr i8*, i8** %_328, i32 1
  %_123 = load i8*, i8** %_329
  call void @__cxa_end_catch()
  br label %_122.0
_123.landingpad.fail:
  resume { i8*, i32 } %_322
_128.landingpad:
  %_330 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_331 = extractvalue { i8*, i32 } %_330, 0
  %_332 = extractvalue { i8*, i32 } %_330, 1
  %_333 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_334 = icmp eq i32 %_332, %_333
  br i1 %_334, label %_128.landingpad.succ, label %_128.landingpad.fail
_128.landingpad.succ:
  %_335 = call i8* @__cxa_begin_catch(i8* %_331)
  %_336 = bitcast i8* %_335 to i8**
  %_337 = getelementptr i8*, i8** %_336, i32 1
  %_128 = load i8*, i8** %_337
  call void @__cxa_end_catch()
  br label %_124.0
_128.landingpad.fail:
  resume { i8*, i32 } %_330
_130.landingpad:
  %_338 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_339 = extractvalue { i8*, i32 } %_338, 0
  %_340 = extractvalue { i8*, i32 } %_338, 1
  %_341 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_342 = icmp eq i32 %_340, %_341
  br i1 %_342, label %_130.landingpad.succ, label %_130.landingpad.fail
_130.landingpad.succ:
  %_343 = call i8* @__cxa_begin_catch(i8* %_339)
  %_344 = bitcast i8* %_343 to i8**
  %_345 = getelementptr i8*, i8** %_344, i32 1
  %_130 = load i8*, i8** %_345
  call void @__cxa_end_catch()
  br label %_124.0
_130.landingpad.fail:
  resume { i8*, i32 } %_338
_132.landingpad:
  %_346 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_347 = extractvalue { i8*, i32 } %_346, 0
  %_348 = extractvalue { i8*, i32 } %_346, 1
  %_349 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_350 = icmp eq i32 %_348, %_349
  br i1 %_350, label %_132.landingpad.succ, label %_132.landingpad.fail
_132.landingpad.succ:
  %_351 = call i8* @__cxa_begin_catch(i8* %_347)
  %_352 = bitcast i8* %_351 to i8**
  %_353 = getelementptr i8*, i8** %_352, i32 1
  %_132 = load i8*, i8** %_353
  call void @__cxa_end_catch()
  br label %_124.0
_132.landingpad.fail:
  resume { i8*, i32 } %_346
_135.landingpad:
  %_354 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_355 = extractvalue { i8*, i32 } %_354, 0
  %_356 = extractvalue { i8*, i32 } %_354, 1
  %_357 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_358 = icmp eq i32 %_356, %_357
  br i1 %_358, label %_135.landingpad.succ, label %_135.landingpad.fail
_135.landingpad.succ:
  %_359 = call i8* @__cxa_begin_catch(i8* %_355)
  %_360 = bitcast i8* %_359 to i8**
  %_361 = getelementptr i8*, i8** %_360, i32 1
  %_135 = load i8*, i8** %_361
  call void @__cxa_end_catch()
  br label %_134.0
_135.landingpad.fail:
  resume { i8*, i32 } %_354
_139.landingpad:
  %_362 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_363 = extractvalue { i8*, i32 } %_362, 0
  %_364 = extractvalue { i8*, i32 } %_362, 1
  %_365 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_366 = icmp eq i32 %_364, %_365
  br i1 %_366, label %_139.landingpad.succ, label %_139.landingpad.fail
_139.landingpad.succ:
  %_367 = call i8* @__cxa_begin_catch(i8* %_363)
  %_368 = bitcast i8* %_367 to i8**
  %_369 = getelementptr i8*, i8** %_368, i32 1
  %_139 = load i8*, i8** %_369
  call void @__cxa_end_catch()
  br label %_136.0
_139.landingpad.fail:
  resume { i8*, i32 } %_362
_141.landingpad:
  %_370 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_371 = extractvalue { i8*, i32 } %_370, 0
  %_372 = extractvalue { i8*, i32 } %_370, 1
  %_373 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_374 = icmp eq i32 %_372, %_373
  br i1 %_374, label %_141.landingpad.succ, label %_141.landingpad.fail
_141.landingpad.succ:
  %_375 = call i8* @__cxa_begin_catch(i8* %_371)
  %_376 = bitcast i8* %_375 to i8**
  %_377 = getelementptr i8*, i8** %_376, i32 1
  %_141 = load i8*, i8** %_377
  call void @__cxa_end_catch()
  br label %_136.0
_141.landingpad.fail:
  resume { i8*, i32 } %_370
_143.landingpad:
  %_378 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_379 = extractvalue { i8*, i32 } %_378, 0
  %_380 = extractvalue { i8*, i32 } %_378, 1
  %_381 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_382 = icmp eq i32 %_380, %_381
  br i1 %_382, label %_143.landingpad.succ, label %_143.landingpad.fail
_143.landingpad.succ:
  %_383 = call i8* @__cxa_begin_catch(i8* %_379)
  %_384 = bitcast i8* %_383 to i8**
  %_385 = getelementptr i8*, i8** %_384, i32 1
  %_143 = load i8*, i8** %_385
  call void @__cxa_end_catch()
  br label %_136.0
_143.landingpad.fail:
  resume { i8*, i32 } %_378
_146.landingpad:
  %_386 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_387 = extractvalue { i8*, i32 } %_386, 0
  %_388 = extractvalue { i8*, i32 } %_386, 1
  %_389 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_390 = icmp eq i32 %_388, %_389
  br i1 %_390, label %_146.landingpad.succ, label %_146.landingpad.fail
_146.landingpad.succ:
  %_391 = call i8* @__cxa_begin_catch(i8* %_387)
  %_392 = bitcast i8* %_391 to i8**
  %_393 = getelementptr i8*, i8** %_392, i32 1
  %_146 = load i8*, i8** %_393
  call void @__cxa_end_catch()
  br label %_144.0
_146.landingpad.fail:
  resume { i8*, i32 } %_386
_183.landingpad:
  %_394 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_395 = extractvalue { i8*, i32 } %_394, 0
  %_396 = extractvalue { i8*, i32 } %_394, 1
  %_397 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_398 = icmp eq i32 %_396, %_397
  br i1 %_398, label %_183.landingpad.succ, label %_183.landingpad.fail
_183.landingpad.succ:
  %_399 = call i8* @__cxa_begin_catch(i8* %_395)
  %_400 = bitcast i8* %_399 to i8**
  %_401 = getelementptr i8*, i8** %_400, i32 1
  %_183 = load i8*, i8** %_401
  call void @__cxa_end_catch()
  br label %_92.0
_183.landingpad.fail:
  resume { i8*, i32 } %_394
_185.landingpad:
  %_402 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_403 = extractvalue { i8*, i32 } %_402, 0
  %_404 = extractvalue { i8*, i32 } %_402, 1
  %_405 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_406 = icmp eq i32 %_404, %_405
  br i1 %_406, label %_185.landingpad.succ, label %_185.landingpad.fail
_185.landingpad.succ:
  %_407 = call i8* @__cxa_begin_catch(i8* %_403)
  %_408 = bitcast i8* %_407 to i8**
  %_409 = getelementptr i8*, i8** %_408, i32 1
  %_185 = load i8*, i8** %_409
  call void @__cxa_end_catch()
  br label %_104.0
_185.landingpad.fail:
  resume { i8*, i32 } %_402
_187.landingpad:
  %_410 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_411 = extractvalue { i8*, i32 } %_410, 0
  %_412 = extractvalue { i8*, i32 } %_410, 1
  %_413 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_414 = icmp eq i32 %_412, %_413
  br i1 %_414, label %_187.landingpad.succ, label %_187.landingpad.fail
_187.landingpad.succ:
  %_415 = call i8* @__cxa_begin_catch(i8* %_411)
  %_416 = bitcast i8* %_415 to i8**
  %_417 = getelementptr i8*, i8** %_416, i32 1
  %_187 = load i8*, i8** %_417
  call void @__cxa_end_catch()
  br label %_116.0
_187.landingpad.fail:
  resume { i8*, i32 } %_410
_189.landingpad:
  %_418 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_419 = extractvalue { i8*, i32 } %_418, 0
  %_420 = extractvalue { i8*, i32 } %_418, 1
  %_421 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_422 = icmp eq i32 %_420, %_421
  br i1 %_422, label %_189.landingpad.succ, label %_189.landingpad.fail
_189.landingpad.succ:
  %_423 = call i8* @__cxa_begin_catch(i8* %_419)
  %_424 = bitcast i8* %_423 to i8**
  %_425 = getelementptr i8*, i8** %_424, i32 1
  %_189 = load i8*, i8** %_425
  call void @__cxa_end_catch()
  br label %_124.0
_189.landingpad.fail:
  resume { i8*, i32 } %_418
_191.landingpad:
  %_426 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_427 = extractvalue { i8*, i32 } %_426, 0
  %_428 = extractvalue { i8*, i32 } %_426, 1
  %_429 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_430 = icmp eq i32 %_428, %_429
  br i1 %_430, label %_191.landingpad.succ, label %_191.landingpad.fail
_191.landingpad.succ:
  %_431 = call i8* @__cxa_begin_catch(i8* %_427)
  %_432 = bitcast i8* %_431 to i8**
  %_433 = getelementptr i8*, i8** %_432, i32 1
  %_191 = load i8*, i8** %_433
  call void @__cxa_end_catch()
  br label %_136.0
_191.landingpad.fail:
  resume { i8*, i32 } %_426
}

define dereferenceable_or_null(32) i8* @"_SM24java.nio.charset.CharsetD14defaultCharsetL24java.nio.charset.CharsetEo"() inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_10000.0:
  %_10002 = call dereferenceable_or_null(32) i8* @"_SM25java.nio.charset.Charset$D14defaultCharsetL24java.nio.charset.CharsetEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM25java.nio.charset.Charset$G8instance" to i8*))
  ret i8* %_10002
}

define dereferenceable_or_null(32) i8* @"_SM24java.nio.charset.CharsetD4nameL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i8*, i8*, i64 }*
  %_20008 = getelementptr { i8*, i8*, i8*, i64 }, { i8*, i8*, i8*, i64 }* %_20007, i32 0, i32 1
  %_20005 = bitcast i8** %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i8**
  %_20001 = load i8*, i8** %_20009, !dereferenceable_or_null !{i64 32}
  ret i8* %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(40) i8* @"_SM24java.nio.charset.CharsetD6decodeL19java.nio.ByteBufferL19java.nio.CharBufferEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(56) i8* @"_SM24java.nio.charset.CharsetD13cachedDecoderL31java.nio.charset.CharsetDecoderEPT24java.nio.charset.Charset"(i8* dereferenceable_or_null(32) %_1)
  %_30002 = call dereferenceable_or_null(40) i8* @"_SM31java.nio.charset.CharsetDecoderD6decodeL19java.nio.ByteBufferL19java.nio.CharBufferEO"(i8* dereferenceable_or_null(56) %_30001, i8* dereferenceable_or_null(48) %_2)
  ret i8* %_30002
}

define i1 @"_SM24java.nio.charset.CharsetD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_110004 = icmp eq i8* %_2, null
  br i1 %_110004, label %_110001.0, label %_110002.0
_110001.0:
  br label %_110003.0
_110002.0:
  %_110025 = bitcast i8* %_2 to i8**
  %_110005 = load i8*, i8** %_110025
  %_110026 = bitcast i8* %_110005 to { i8*, i32, i32, i8* }*
  %_110027 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110026, i32 0, i32 1
  %_110006 = bitcast i32* %_110027 to i8*
  %_110028 = bitcast i8* %_110006 to i32*
  %_110007 = load i32, i32* %_110028
  %_110008 = icmp sle i32 231, %_110007
  %_110009 = icmp sle i32 %_110007, 232
  %_110010 = and i1 %_110008, %_110009
  br label %_110003.0
_110003.0:
  %_40002 = phi i1 [%_110010, %_110002.0], [false, %_110001.0]
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_110014 = icmp eq i8* %_2, null
  br i1 %_110014, label %_110012.0, label %_110011.0
_110011.0:
  %_110029 = bitcast i8* %_2 to i8**
  %_110015 = load i8*, i8** %_110029
  %_110030 = bitcast i8* %_110015 to { i8*, i32, i32, i8* }*
  %_110031 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110030, i32 0, i32 1
  %_110016 = bitcast i32* %_110031 to i8*
  %_110032 = bitcast i8* %_110016 to i32*
  %_110017 = load i32, i32* %_110032
  %_110018 = icmp sle i32 231, %_110017
  %_110019 = icmp sle i32 %_110017, 232
  %_110020 = and i1 %_110018, %_110019
  br i1 %_110020, label %_110012.0, label %_110013.0
_110012.0:
  %_50001 = bitcast i8* %_2 to i8*
  %_50002 = call dereferenceable_or_null(32) i8* @"_SM24java.nio.charset.CharsetD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_1)
  %_50004 = icmp eq i8* %_50002, null
  br i1 %_50004, label %_70000.0, label %_80000.0
_70000.0:
  %_70001 = call dereferenceable_or_null(32) i8* @"_SM24java.nio.charset.CharsetD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_50001)
  %_70003 = icmp eq i8* %_70001, null
  br label %_90000.0
_80000.0:
  %_80001 = call dereferenceable_or_null(32) i8* @"_SM24java.nio.charset.CharsetD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_50001)
  %_110033 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.StringD6equalsL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_80002 = call i1 %_110033(i8* dereferenceable_or_null(32) %_50002, i8* dereferenceable_or_null(32) %_80001)
  br label %_90000.0
_90000.0:
  %_90001 = phi i1 [%_80002, %_80000.0], [%_70003, %_70000.0]
  br label %_100000.0
_60000.0:
  br label %_110000.0
_110000.0:
  br label %_100000.0
_100000.0:
  %_100001 = phi i1 [false, %_110000.0], [%_90001, %_90000.0]
  ret i1 %_100001
_110013.0:
  %_110021 = phi i8* [%_2, %_110011.0]
  %_110022 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM24java.nio.charset.CharsetG4type" to i8*), %_110011.0]
  %_110034 = bitcast i8* %_110021 to i8**
  %_110023 = load i8*, i8** %_110034
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_110023, i8* %_110022)
  unreachable
}

define i32 @"_SM24java.nio.charset.CharsetD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM24java.nio.charset.CharsetD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_1)
  %_20002 = call i32 @"_SM21scala.runtime.StaticsD7anyHashL16java.lang.ObjectiEo"(i8* dereferenceable_or_null(32) %_20001)
  ret i32 %_20002
}

define dereferenceable_or_null(32) i8* @"_SM24java.nio.charset.CharsetD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM24java.nio.charset.CharsetD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_1)
  ret i8* %_20001
}

define nonnull dereferenceable(8) i8* @"_SM24scala.collection.SeqViewD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM25java.nio.StringCharBufferD10isReadOnlyzEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i1 true
}

define nonnull dereferenceable(56) i8* @"_SM25java.nio.StringCharBufferD11subSequenceiiL19java.nio.CharBufferEO"(i8* %_1, i32 %_2, i32 %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40002 = icmp slt i32 %_2, 0
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  br label %_70000.0
_60000.0:
  %_60002 = icmp slt i32 %_3, %_2
  br label %_70000.0
_70000.0:
  %_70001 = phi i1 [%_60002, %_60000.0], [true, %_50000.0]
  br i1 %_70001, label %_80000.0, label %_90000.0
_80000.0:
  br label %_100000.0
_90000.0:
  %_90001 = call i32 @"_SM15java.nio.BufferD9remainingiEO"(i8* dereferenceable_or_null(56) %_1)
  %_90003 = icmp sgt i32 %_3, %_90001
  br label %_100000.0
_100000.0:
  %_100001 = phi i1 [%_90003, %_90000.0], [true, %_80000.0]
  br i1 %_100001, label %_110000.0, label %_120000.0
_120000.0:
  br label %_240000.0
_240000.0:
  %_240002 = call i32 @"_SM15java.nio.BufferD8capacityiEO"(i8* dereferenceable_or_null(56) %_1)
  %_280012 = icmp ne i8* %_1, null
  br i1 %_280012, label %_280010.0, label %_280011.0
_280010.0:
  %_280037 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }*
  %_280038 = getelementptr { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }, { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }* %_280037, i32 0, i32 7
  %_280013 = bitcast i8** %_280038 to i8*
  %_280039 = bitcast i8* %_280013 to i8**
  %_240003 = load i8*, i8** %_280039, !dereferenceable_or_null !{i64 8}
  %_280015 = icmp ne i8* %_1, null
  br i1 %_280015, label %_280014.0, label %_280011.0
_280014.0:
  %_280040 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }*
  %_280041 = getelementptr { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }, { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }* %_280040, i32 0, i32 8
  %_280016 = bitcast i32* %_280041 to i8*
  %_280042 = bitcast i8* %_280016 to i32*
  %_240004 = load i32, i32* %_280042
  %_240005 = call i32 @"_SM15java.nio.BufferD8positioniEO"(i8* dereferenceable_or_null(56) %_1)
  %_240007 = call i32 @"_SM15java.nio.BufferD8positioniEO"(i8* dereferenceable_or_null(56) %_1)
  %_280001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [17 x i8*] }* @"_SM25java.nio.StringCharBufferG4type" to i8*), i64 56)
  %_280043 = bitcast i8* %_280001 to { i8*, i32, i32, i32, i32 }*
  %_280044 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_280043, i32 0, i32 2
  %_280018 = bitcast i32* %_280044 to i8*
  %_280045 = bitcast i8* %_280018 to i32*
  store i32 %_240002, i32* %_280045
  %_280046 = bitcast i8* %_280001 to { i8*, i32, i32, i32, i32, i32, i8* }*
  %_280047 = getelementptr { i8*, i32, i32, i32, i32, i32, i8* }, { i8*, i32, i32, i32, i32, i32, i8* }* %_280046, i32 0, i32 5
  %_280020 = bitcast i32* %_280047 to i8*
  %_280048 = bitcast i8* %_280020 to i32*
  store i32 -1, i32* %_280048
  %_280049 = bitcast i8* %_280001 to { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }*
  %_280050 = getelementptr { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }, { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }* %_280049, i32 0, i32 8
  %_280022 = bitcast i32* %_280050 to i8*
  %_280051 = bitcast i8* %_280022 to i32*
  store i32 %_240004, i32* %_280051
  %_280052 = bitcast i8* %_280001 to { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }*
  %_280053 = getelementptr { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }, { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }* %_280052, i32 0, i32 7
  %_280024 = bitcast i8** %_280053 to i8*
  %_280054 = bitcast i8* %_280024 to i8**
  store i8* %_240003, i8** %_280054
  %_280006 = call i32 @"_SM15java.nio.BufferD8capacityiEO"(i8* nonnull dereferenceable(56) %_280001)
  %_280055 = bitcast i8* %_280001 to { i8*, i32, i32, i32, i32 }*
  %_280056 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_280055, i32 0, i32 1
  %_280026 = bitcast i32* %_280056 to i8*
  %_280057 = bitcast i8* %_280026 to i32*
  store i32 %_280006, i32* %_280057
  %_280058 = bitcast i8* %_280001 to { i8*, i32, i32, i32, i32 }*
  %_280059 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_280058, i32 0, i32 3
  %_280028 = bitcast i32* %_280059 to i8*
  %_280060 = bitcast i8* %_280028 to i32*
  store i32 0, i32* %_280060
  %_280061 = bitcast i8* %_280001 to { i8*, i32, i32, i32, i32 }*
  %_280062 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_280061, i32 0, i32 4
  %_280030 = bitcast i32* %_280062 to i8*
  %_280063 = bitcast i8* %_280030 to i32*
  store i32 -1, i32* %_280063
  %_250001 = add i32 %_240005, %_2
  %_250002 = call dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD8positioniL19java.nio.CharBufferEO"(i8* nonnull dereferenceable(56) %_280001, i32 %_250001)
  %_250003 = add i32 %_240007, %_3
  %_250004 = call dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD5limitiL19java.nio.CharBufferEO"(i8* nonnull dereferenceable(56) %_280001, i32 %_250003)
  ret i8* %_280001
_110000.0:
  br label %_220000.0
_220000.0:
  %_220001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM35java.lang.IndexOutOfBoundsExceptionG4type" to i8*), i64 40)
  %_280064 = bitcast i8* %_220001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_280065 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_280064, i32 0, i32 5
  %_280032 = bitcast i1* %_280065 to i8*
  %_280066 = bitcast i8* %_280032 to i1*
  store i1 true, i1* %_280066
  %_280067 = bitcast i8* %_220001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_280068 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_280067, i32 0, i32 4
  %_280034 = bitcast i1* %_280068 to i8*
  %_280069 = bitcast i8* %_280034 to i1*
  store i1 true, i1* %_280069
  %_220004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_220001)
  br label %_230000.0
_230000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_220001)
  unreachable
_280011.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(40) i8* @"_SM25java.nio.StringCharBufferD11subSequenceiiL22java.lang.CharSequenceEO"(i8* %_1, i32 %_2, i32 %_3) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40002 = bitcast i8* bitcast (i8* (i8*, i32, i32)* @"_SM25java.nio.StringCharBufferD11subSequenceiiL19java.nio.CharBufferEO" to i8*) to i8* (i8*, i32, i32)*
  %_40001 = call dereferenceable_or_null(40) i8* %_40002(i8* dereferenceable_or_null(56) %_1, i32 %_2, i32 %_3)
  ret i8* %_40001
}

define dereferenceable_or_null(40) i8* @"_SM25java.nio.StringCharBufferD3getLAc_iiL19java.nio.CharBufferEO"(i8* %_1, i8* %_2, i32 %_3, i32 %_4) noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_50001 = call dereferenceable_or_null(40) i8* @"_SM25java.nio.StringCharBufferD9genBufferL19java.nio.CharBufferEPT25java.nio.StringCharBuffer"(i8* dereferenceable_or_null(56) %_1)
  %_50003 = call dereferenceable_or_null(24) i8* @"_SM19java.nio.GenBuffer$D21generic_get$extensionL15java.nio.BufferL16java.lang.ObjectiiL15java.nio.BufferEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19java.nio.GenBuffer$G8instance" to i8*), i8* dereferenceable_or_null(40) %_50001, i8* dereferenceable_or_null(8) %_2, i32 %_3, i32 %_4)
  %_50008 = icmp eq i8* %_50003, null
  br i1 %_50008, label %_50006.0, label %_50005.0
_50005.0:
  %_50019 = bitcast i8* %_50003 to i8**
  %_50009 = load i8*, i8** %_50019
  %_50020 = bitcast i8* %_50009 to { i8*, i32, i32, i8* }*
  %_50021 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_50020, i32 0, i32 1
  %_50010 = bitcast i32* %_50021 to i8*
  %_50022 = bitcast i8* %_50010 to i32*
  %_50011 = load i32, i32* %_50022
  %_50012 = icmp sle i32 94, %_50011
  %_50013 = icmp sle i32 %_50011, 96
  %_50014 = and i1 %_50012, %_50013
  br i1 %_50014, label %_50006.0, label %_50007.0
_50006.0:
  %_50004 = bitcast i8* %_50003 to i8*
  ret i8* %_50004
_50007.0:
  %_50015 = phi i8* [%_50003, %_50005.0]
  %_50016 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [17 x i8*] }* @"_SM19java.nio.CharBufferG4type" to i8*), %_50005.0]
  %_50023 = bitcast i8* %_50015 to i8**
  %_50017 = load i8*, i8** %_50023
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_50017, i8* %_50016)
  unreachable
}

define i16 @"_SM25java.nio.StringCharBufferD3getcEO"(i8* %_1) noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(40) i8* @"_SM25java.nio.StringCharBufferD9genBufferL19java.nio.CharBufferEPT25java.nio.StringCharBuffer"(i8* dereferenceable_or_null(56) %_1)
  %_20003 = call dereferenceable_or_null(8) i8* @"_SM19java.nio.GenBuffer$D21generic_get$extensionL15java.nio.BufferL16java.lang.ObjectEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19java.nio.GenBuffer$G8instance" to i8*), i8* dereferenceable_or_null(40) %_20001)
  %_20005 = bitcast i8* bitcast (i16 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D11unboxToCharL16java.lang.ObjectcEO" to i8*) to i16 (i8*, i8*)*
  %_20004 = call i16 %_20005(i8* null, i8* dereferenceable_or_null(8) %_20003)
  ret i16 %_20004
}

define i16 @"_SM25java.nio.StringCharBufferD3geticEO"(i8* %_1, i32 %_2) noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(40) i8* @"_SM25java.nio.StringCharBufferD9genBufferL19java.nio.CharBufferEPT25java.nio.StringCharBuffer"(i8* dereferenceable_or_null(56) %_1)
  %_30003 = call dereferenceable_or_null(8) i8* @"_SM19java.nio.GenBuffer$D21generic_get$extensionL15java.nio.BufferiL16java.lang.ObjectEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19java.nio.GenBuffer$G8instance" to i8*), i8* dereferenceable_or_null(40) %_30001, i32 %_2)
  %_30005 = bitcast i8* bitcast (i16 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D11unboxToCharL16java.lang.ObjectcEO" to i8*) to i16 (i8*, i8*)*
  %_30004 = call i16 %_30005(i8* null, i8* dereferenceable_or_null(8) %_30003)
  ret i16 %_30004
}

define i8* @"_SM25java.nio.StringCharBufferD3putcL19java.nio.CharBufferEO"(i8* %_1, i16 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_130000.0
_130000.0:
  %_130001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.ReadOnlyBufferExceptionG4type" to i8*), i64 40)
  %_140006 = bitcast i8* %_130001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_140007 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_140006, i32 0, i32 5
  %_140002 = bitcast i1* %_140007 to i8*
  %_140008 = bitcast i8* %_140002 to i1*
  store i1 true, i1* %_140008
  %_140009 = bitcast i8* %_130001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_140010 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_140009, i32 0, i32 4
  %_140004 = bitcast i1* %_140010 to i8*
  %_140011 = bitcast i8* %_140004 to i1*
  store i1 true, i1* %_140011
  %_130004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_130001)
  br label %_140000.0
_140000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_130001)
  unreachable
}

define nonnull dereferenceable(16) i8* @"_SM25java.nio.StringCharBufferD4loadiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i16 @"_SM25java.nio.StringCharBufferD4loadicEO"(i8* dereferenceable_or_null(56) %_1, i32 %_2)
  %_30003 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8* null, i16 %_30001)
  ret i8* %_30003
}

define nonnull dereferenceable(8) i8* @"_SM25java.nio.StringCharBufferD4loadiL16java.lang.ObjectiiuEO"(i8* %_1, i32 %_2, i8* %_3, i32 %_4, i32 %_5) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  %_60006 = icmp eq i8* %_3, null
  br i1 %_60006, label %_60004.0, label %_60003.0
_60003.0:
  %_60014 = bitcast i8* %_3 to i8**
  %_60007 = load i8*, i8** %_60014
  %_60008 = icmp eq i8* %_60007, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*)
  br i1 %_60008, label %_60004.0, label %_60005.0
_60004.0:
  %_60001 = bitcast i8* %_3 to i8*
  call nonnull dereferenceable(8) i8* @"_SM25java.nio.StringCharBufferD4loadiLAc_iiuEO"(i8* dereferenceable_or_null(56) %_1, i32 %_2, i8* dereferenceable_or_null(8) %_60001, i32 %_4, i32 %_5)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_60005.0:
  %_60010 = phi i8* [%_3, %_60003.0]
  %_60011 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), %_60003.0]
  %_60015 = bitcast i8* %_60010 to i8**
  %_60012 = load i8*, i8** %_60015
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_60012, i8* %_60011)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM25java.nio.StringCharBufferD4loadiLAc_iiuEO"(i8* %_1, i32 %_2, i8* %_3, i32 %_4, i32 %_5) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  %_60001 = call dereferenceable_or_null(40) i8* @"_SM25java.nio.StringCharBufferD9genBufferL19java.nio.CharBufferEPT25java.nio.StringCharBuffer"(i8* dereferenceable_or_null(56) %_1)
  call nonnull dereferenceable(8) i8* @"_SM19java.nio.GenBuffer$D22generic_load$extensionL15java.nio.BufferiL16java.lang.ObjectiiuEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19java.nio.GenBuffer$G8instance" to i8*), i8* dereferenceable_or_null(40) %_60001, i32 %_2, i8* dereferenceable_or_null(8) %_3, i32 %_4, i32 %_5)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i16 @"_SM25java.nio.StringCharBufferD4loadicEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30010 = icmp ne i8* %_1, null
  br i1 %_30010, label %_30008.0, label %_30009.0
_30008.0:
  %_30023 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }*
  %_30024 = getelementptr { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }, { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }* %_30023, i32 0, i32 7
  %_30011 = bitcast i8** %_30024 to i8*
  %_30025 = bitcast i8* %_30011 to i8**
  %_30001 = load i8*, i8** %_30025, !dereferenceable_or_null !{i64 8}
  %_30013 = icmp ne i8* %_1, null
  br i1 %_30013, label %_30012.0, label %_30009.0
_30012.0:
  %_30026 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }*
  %_30027 = getelementptr { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }, { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }* %_30026, i32 0, i32 8
  %_30014 = bitcast i32* %_30027 to i8*
  %_30028 = bitcast i8* %_30014 to i32*
  %_30002 = load i32, i32* %_30028
  %_30016 = icmp ne i8* %_30001, null
  br i1 %_30016, label %_30015.0, label %_30009.0
_30015.0:
  %_30029 = bitcast i8* %_30001 to i8**
  %_30017 = load i8*, i8** %_30029
  %_30030 = bitcast i8* %_30017 to { i8*, i32, i32, i8* }*
  %_30031 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30030, i32 0, i32 2
  %_30018 = bitcast i32* %_30031 to i8*
  %_30032 = bitcast i8* %_30018 to i32*
  %_30019 = load i32, i32* %_30032
  %_30033 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30034 = getelementptr i8*, i8** %_30033, i32 1589
  %_30020 = bitcast i8** %_30034 to i8*
  %_30035 = bitcast i8* %_30020 to i8**
  %_30036 = getelementptr i8*, i8** %_30035, i32 %_30019
  %_30021 = bitcast i8** %_30036 to i8*
  %_30037 = bitcast i8* %_30021 to i8**
  %_30005 = load i8*, i8** %_30037
  %_30006 = add i32 %_30002, %_2
  %_30038 = bitcast i8* %_30005 to i16 (i8*, i32)*
  %_30007 = call i16 %_30038(i8* dereferenceable_or_null(8) %_30001, i32 %_30006)
  ret i16 %_30007
_30009.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM25java.nio.StringCharBufferD5storeiL16java.lang.ObjectiiuEO"(i8* %_1, i32 %_2, i8* %_3, i32 %_4, i32 %_5) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  %_60006 = icmp eq i8* %_3, null
  br i1 %_60006, label %_60004.0, label %_60003.0
_60003.0:
  %_60014 = bitcast i8* %_3 to i8**
  %_60007 = load i8*, i8** %_60014
  %_60008 = icmp eq i8* %_60007, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*)
  br i1 %_60008, label %_60004.0, label %_60005.0
_60004.0:
  %_60001 = bitcast i8* %_3 to i8*
  %_60015 = bitcast i8* bitcast (i8* (i8*, i32, i8*, i32, i32)* @"_SM25java.nio.StringCharBufferD5storeiLAc_iiuEO" to i8*) to i8* (i8*, i32, i8*, i32, i32)*
  call nonnull dereferenceable(8) i8* %_60015(i8* dereferenceable_or_null(56) %_1, i32 %_2, i8* dereferenceable_or_null(8) %_60001, i32 %_4, i32 %_5)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_60005.0:
  %_60010 = phi i8* [%_3, %_60003.0]
  %_60011 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), %_60003.0]
  %_60016 = bitcast i8* %_60010 to i8**
  %_60012 = load i8*, i8** %_60016
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_60012, i8* %_60011)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM25java.nio.StringCharBufferD5storeiL16java.lang.ObjectuEO"(i8* %_1, i32 %_2, i8* %_3) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40004 = bitcast i8* bitcast (i16 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D11unboxToCharL16java.lang.ObjectcEO" to i8*) to i16 (i8*, i8*)*
  %_40001 = call i16 %_40004(i8* null, i8* dereferenceable_or_null(8) %_3)
  %_40005 = bitcast i8* bitcast (i8* (i8*, i32, i16)* @"_SM25java.nio.StringCharBufferD5storeicuEO" to i8*) to i8* (i8*, i32, i16)*
  call nonnull dereferenceable(8) i8* %_40005(i8* dereferenceable_or_null(56) %_1, i32 %_2, i16 %_40001)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i8* @"_SM25java.nio.StringCharBufferD5storeiLAc_iiuEO"(i8* %_1, i32 %_2, i8* %_3, i32 %_4, i32 %_5) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  br label %_160000.0
_160000.0:
  %_160001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.ReadOnlyBufferExceptionG4type" to i8*), i64 40)
  %_170006 = bitcast i8* %_160001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_170007 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_170006, i32 0, i32 5
  %_170002 = bitcast i1* %_170007 to i8*
  %_170008 = bitcast i8* %_170002 to i1*
  store i1 true, i1* %_170008
  %_170009 = bitcast i8* %_160001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_170010 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_170009, i32 0, i32 4
  %_170004 = bitcast i1* %_170010 to i8*
  %_170011 = bitcast i8* %_170004 to i1*
  store i1 true, i1* %_170011
  %_160004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_160001)
  br label %_170000.0
_170000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_160001)
  unreachable
}

define i8* @"_SM25java.nio.StringCharBufferD5storeicuEO"(i8* %_1, i32 %_2, i16 %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  br label %_140000.0
_140000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.ReadOnlyBufferExceptionG4type" to i8*), i64 40)
  %_150006 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_150007 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_150006, i32 0, i32 5
  %_150002 = bitcast i1* %_150007 to i8*
  %_150008 = bitcast i8* %_150002 to i1*
  store i1 true, i1* %_150008
  %_150009 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_150010 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_150009, i32 0, i32 4
  %_150004 = bitcast i1* %_150010 to i8*
  %_150011 = bitcast i8* %_150004 to i1*
  store i1 true, i1* %_150011
  %_140004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_140001)
  br label %_150000.0
_150000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_140001)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM25java.nio.StringCharBufferD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20017 = icmp ne i8* %_1, null
  br i1 %_20017, label %_20015.0, label %_20016.0
_20015.0:
  %_20034 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }*
  %_20035 = getelementptr { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }, { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }* %_20034, i32 0, i32 8
  %_20018 = bitcast i32* %_20035 to i8*
  %_20036 = bitcast i8* %_20018 to i32*
  %_20001 = load i32, i32* %_20036
  %_20020 = icmp ne i8* %_1, null
  br i1 %_20020, label %_20019.0, label %_20016.0
_20019.0:
  %_20037 = bitcast i8* %_1 to { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }*
  %_20038 = getelementptr { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }, { i8*, i32, i32, i32, i32, i32, i8*, i8*, i32 }* %_20037, i32 0, i32 7
  %_20021 = bitcast i8** %_20038 to i8*
  %_20039 = bitcast i8* %_20021 to i8**
  %_20002 = load i8*, i8** %_20039, !dereferenceable_or_null !{i64 8}
  %_20003 = call i32 @"_SM15java.nio.BufferD8positioniEO"(i8* dereferenceable_or_null(56) %_1)
  %_20005 = call i32 @"_SM15java.nio.BufferD5limitiEO"(i8* dereferenceable_or_null(56) %_1)
  %_20023 = icmp ne i8* %_20002, null
  br i1 %_20023, label %_20022.0, label %_20016.0
_20022.0:
  %_20040 = bitcast i8* %_20002 to i8**
  %_20024 = load i8*, i8** %_20040
  %_20041 = bitcast i8* %_20024 to { i8*, i32, i32, i8* }*
  %_20042 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_20041, i32 0, i32 2
  %_20025 = bitcast i32* %_20042 to i8*
  %_20043 = bitcast i8* %_20025 to i32*
  %_20026 = load i32, i32* %_20043
  %_20044 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_20045 = getelementptr i8*, i8** %_20044, i32 2868
  %_20027 = bitcast i8** %_20045 to i8*
  %_20046 = bitcast i8* %_20027 to i8**
  %_20047 = getelementptr i8*, i8** %_20046, i32 %_20026
  %_20028 = bitcast i8** %_20047 to i8*
  %_20048 = bitcast i8* %_20028 to i8**
  %_20008 = load i8*, i8** %_20048
  %_20009 = add i32 %_20003, %_20001
  %_20010 = add i32 %_20005, %_20001
  %_20049 = bitcast i8* %_20008 to i8* (i8*, i32, i32)*
  %_20011 = call dereferenceable_or_null(8) i8* %_20049(i8* dereferenceable_or_null(8) %_20002, i32 %_20009, i32 %_20010)
  %_20030 = icmp ne i8* %_20011, null
  br i1 %_20030, label %_20029.0, label %_20016.0
_20029.0:
  %_20050 = bitcast i8* %_20011 to i8**
  %_20031 = load i8*, i8** %_20050
  %_20051 = bitcast i8* %_20031 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_20052 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_20051, i32 0, i32 4, i32 1
  %_20032 = bitcast i8** %_20052 to i8*
  %_20053 = bitcast i8* %_20032 to i8**
  %_20013 = load i8*, i8** %_20053
  %_20054 = bitcast i8* %_20013 to i8* (i8*)*
  %_20014 = call dereferenceable_or_null(32) i8* %_20054(i8* dereferenceable_or_null(8) %_20011)
  ret i8* %_20014
_20016.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(40) i8* @"_SM25java.nio.StringCharBufferD9genBufferL19java.nio.CharBufferEPT25java.nio.StringCharBuffer"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = call dereferenceable_or_null(24) i8* @"_SM19java.nio.GenBuffer$D5applyL15java.nio.BufferL15java.nio.BufferEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19java.nio.GenBuffer$G8instance" to i8*), i8* dereferenceable_or_null(56) %_1)
  %_20007 = icmp eq i8* %_20002, null
  br i1 %_20007, label %_20005.0, label %_20004.0
_20004.0:
  %_20018 = bitcast i8* %_20002 to i8**
  %_20008 = load i8*, i8** %_20018
  %_20019 = bitcast i8* %_20008 to { i8*, i32, i32, i8* }*
  %_20020 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_20019, i32 0, i32 1
  %_20009 = bitcast i32* %_20020 to i8*
  %_20021 = bitcast i8* %_20009 to i32*
  %_20010 = load i32, i32* %_20021
  %_20011 = icmp sle i32 94, %_20010
  %_20012 = icmp sle i32 %_20010, 96
  %_20013 = and i1 %_20011, %_20012
  br i1 %_20013, label %_20005.0, label %_20006.0
_20005.0:
  %_20003 = bitcast i8* %_20002 to i8*
  ret i8* %_20003
_20006.0:
  %_20014 = phi i8* [%_20002, %_20004.0]
  %_20015 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [17 x i8*] }* @"_SM19java.nio.CharBufferG4type" to i8*), %_20004.0]
  %_20022 = bitcast i8* %_20014 to i8**
  %_20016 = load i8*, i8** %_20022
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_20016, i8* %_20015)
  unreachable
}

define i1 @"_SM27java.lang.StackTraceElementD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_370004 = icmp eq i8* %_2, null
  br i1 %_370004, label %_370001.0, label %_370002.0
_370001.0:
  br label %_370003.0
_370002.0:
  %_370052 = bitcast i8* %_2 to i8**
  %_370005 = load i8*, i8** %_370052
  %_370006 = icmp eq i8* %_370005, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.StackTraceElementG4type" to i8*)
  br label %_370003.0
_370003.0:
  %_40002 = phi i1 [%_370006, %_370002.0], [false, %_370001.0]
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_370010 = icmp eq i8* %_2, null
  br i1 %_370010, label %_370008.0, label %_370007.0
_370007.0:
  %_370053 = bitcast i8* %_2 to i8**
  %_370011 = load i8*, i8** %_370053
  %_370012 = icmp eq i8* %_370011, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.StackTraceElementG4type" to i8*)
  br i1 %_370012, label %_370008.0, label %_370009.0
_370008.0:
  %_50001 = bitcast i8* %_2 to i8*
  %_370015 = icmp ne i8* %_1, null
  br i1 %_370015, label %_370013.0, label %_370014.0
_370013.0:
  %_370054 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_370055 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370054, i32 0, i32 4
  %_370016 = bitcast i8** %_370055 to i8*
  %_370056 = bitcast i8* %_370016 to i8**
  %_70001 = load i8*, i8** %_370056, !dereferenceable_or_null !{i64 32}
  %_50003 = icmp eq i8* %_70001, null
  br i1 %_50003, label %_80000.0, label %_90000.0
_80000.0:
  %_370018 = icmp ne i8* %_50001, null
  br i1 %_370018, label %_370017.0, label %_370014.0
_370017.0:
  %_370057 = bitcast i8* %_50001 to { i8*, i32, i8*, i8*, i8* }*
  %_370058 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370057, i32 0, i32 4
  %_370019 = bitcast i8** %_370058 to i8*
  %_370059 = bitcast i8* %_370019 to i8**
  %_100001 = load i8*, i8** %_370059, !dereferenceable_or_null !{i64 32}
  %_80002 = icmp eq i8* %_100001, null
  br label %_110000.0
_90000.0:
  %_370021 = icmp ne i8* %_50001, null
  br i1 %_370021, label %_370020.0, label %_370014.0
_370020.0:
  %_370060 = bitcast i8* %_50001 to { i8*, i32, i8*, i8*, i8* }*
  %_370061 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370060, i32 0, i32 4
  %_370022 = bitcast i8** %_370061 to i8*
  %_370062 = bitcast i8* %_370022 to i8**
  %_120001 = load i8*, i8** %_370062, !dereferenceable_or_null !{i64 32}
  %_370063 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.StringD6equalsL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_90001 = call i1 %_370063(i8* dereferenceable_or_null(32) %_70001, i8* dereferenceable_or_null(32) %_120001)
  br label %_110000.0
_110000.0:
  %_110001 = phi i1 [%_90001, %_370020.0], [%_80002, %_370017.0]
  br i1 %_110001, label %_130000.0, label %_140000.0
_130000.0:
  %_370024 = icmp ne i8* %_1, null
  br i1 %_370024, label %_370023.0, label %_370014.0
_370023.0:
  %_370064 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_370065 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370064, i32 0, i32 3
  %_370025 = bitcast i8** %_370065 to i8*
  %_370066 = bitcast i8* %_370025 to i8**
  %_150001 = load i8*, i8** %_370066, !dereferenceable_or_null !{i64 32}
  %_130002 = icmp eq i8* %_150001, null
  br i1 %_130002, label %_160000.0, label %_170000.0
_160000.0:
  %_370027 = icmp ne i8* %_50001, null
  br i1 %_370027, label %_370026.0, label %_370014.0
_370026.0:
  %_370067 = bitcast i8* %_50001 to { i8*, i32, i8*, i8*, i8* }*
  %_370068 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370067, i32 0, i32 3
  %_370028 = bitcast i8** %_370068 to i8*
  %_370069 = bitcast i8* %_370028 to i8**
  %_180001 = load i8*, i8** %_370069, !dereferenceable_or_null !{i64 32}
  %_160002 = icmp eq i8* %_180001, null
  br label %_190000.0
_170000.0:
  %_370030 = icmp ne i8* %_50001, null
  br i1 %_370030, label %_370029.0, label %_370014.0
_370029.0:
  %_370070 = bitcast i8* %_50001 to { i8*, i32, i8*, i8*, i8* }*
  %_370071 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370070, i32 0, i32 3
  %_370031 = bitcast i8** %_370071 to i8*
  %_370072 = bitcast i8* %_370031 to i8**
  %_200001 = load i8*, i8** %_370072, !dereferenceable_or_null !{i64 32}
  %_370073 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.StringD6equalsL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_170001 = call i1 %_370073(i8* dereferenceable_or_null(32) %_150001, i8* dereferenceable_or_null(32) %_200001)
  br label %_190000.0
_190000.0:
  %_190001 = phi i1 [%_170001, %_370029.0], [%_160002, %_370026.0]
  br label %_210000.0
_140000.0:
  br label %_210000.0
_210000.0:
  %_210001 = phi i1 [false, %_140000.0], [%_190001, %_190000.0]
  br i1 %_210001, label %_220000.0, label %_230000.0
_220000.0:
  %_370033 = icmp ne i8* %_1, null
  br i1 %_370033, label %_370032.0, label %_370014.0
_370032.0:
  %_370074 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_370075 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370074, i32 0, i32 2
  %_370034 = bitcast i8** %_370075 to i8*
  %_370076 = bitcast i8* %_370034 to i8**
  %_240001 = load i8*, i8** %_370076, !dereferenceable_or_null !{i64 32}
  %_220002 = icmp eq i8* %_240001, null
  br i1 %_220002, label %_250000.0, label %_260000.0
_250000.0:
  %_370036 = icmp ne i8* %_50001, null
  br i1 %_370036, label %_370035.0, label %_370014.0
_370035.0:
  %_370077 = bitcast i8* %_50001 to { i8*, i32, i8*, i8*, i8* }*
  %_370078 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370077, i32 0, i32 2
  %_370037 = bitcast i8** %_370078 to i8*
  %_370079 = bitcast i8* %_370037 to i8**
  %_270001 = load i8*, i8** %_370079, !dereferenceable_or_null !{i64 32}
  %_250002 = icmp eq i8* %_270001, null
  br label %_280000.0
_260000.0:
  %_370039 = icmp ne i8* %_50001, null
  br i1 %_370039, label %_370038.0, label %_370014.0
_370038.0:
  %_370080 = bitcast i8* %_50001 to { i8*, i32, i8*, i8*, i8* }*
  %_370081 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370080, i32 0, i32 2
  %_370040 = bitcast i8** %_370081 to i8*
  %_370082 = bitcast i8* %_370040 to i8**
  %_290001 = load i8*, i8** %_370082, !dereferenceable_or_null !{i64 32}
  %_370083 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.StringD6equalsL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_260001 = call i1 %_370083(i8* dereferenceable_or_null(32) %_240001, i8* dereferenceable_or_null(32) %_290001)
  br label %_280000.0
_280000.0:
  %_280001 = phi i1 [%_260001, %_370038.0], [%_250002, %_370035.0]
  br label %_300000.0
_230000.0:
  br label %_300000.0
_300000.0:
  %_300001 = phi i1 [false, %_230000.0], [%_280001, %_280000.0]
  br i1 %_300001, label %_310000.0, label %_320000.0
_310000.0:
  %_370042 = icmp ne i8* %_1, null
  br i1 %_370042, label %_370041.0, label %_370014.0
_370041.0:
  %_370084 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_370085 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370084, i32 0, i32 1
  %_370043 = bitcast i32* %_370085 to i8*
  %_370086 = bitcast i8* %_370043 to i32*
  %_330001 = load i32, i32* %_370086
  %_370045 = icmp ne i8* %_50001, null
  br i1 %_370045, label %_370044.0, label %_370014.0
_370044.0:
  %_370087 = bitcast i8* %_50001 to { i8*, i32, i8*, i8*, i8* }*
  %_370088 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_370087, i32 0, i32 1
  %_370046 = bitcast i32* %_370088 to i8*
  %_370089 = bitcast i8* %_370046 to i32*
  %_340001 = load i32, i32* %_370089
  %_310002 = icmp eq i32 %_330001, %_340001
  br label %_350000.0
_320000.0:
  br label %_350000.0
_350000.0:
  %_350001 = phi i1 [false, %_320000.0], [%_310002, %_370044.0]
  br label %_360000.0
_60000.0:
  br label %_370000.0
_370000.0:
  br label %_360000.0
_360000.0:
  %_360001 = phi i1 [false, %_370000.0], [%_350001, %_350000.0]
  ret i1 %_360001
_370014.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_370009.0:
  %_370048 = phi i8* [%_2, %_370007.0]
  %_370049 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.StackTraceElementG4type" to i8*), %_370007.0]
  %_370090 = bitcast i8* %_370048 to i8**
  %_370050 = load i8*, i8** %_370090
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_370050, i8* %_370049)
  unreachable
}

define i32 @"_SM27java.lang.StackTraceElementD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM27java.lang.StackTraceElementD8toStringL16java.lang.StringEO"(i8* dereferenceable_or_null(40) %_1)
  %_20002 = call i32 @"_SM21scala.runtime.StaticsD7anyHashL16java.lang.ObjectiEo"(i8* dereferenceable_or_null(32) %_20001)
  ret i32 %_20002
}

define dereferenceable_or_null(32) i8* @"_SM27java.lang.StackTraceElementD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_650005 = icmp ne i8* %_1, null
  br i1 %_650005, label %_650003.0, label %_650004.0
_650003.0:
  %_650047 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_650048 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_650047, i32 0, i32 2
  %_650006 = bitcast i8** %_650048 to i8*
  %_650049 = bitcast i8* %_650006 to i8**
  %_30001 = load i8*, i8** %_650049, !dereferenceable_or_null !{i64 32}
  %_20002 = icmp eq i8* %_30001, null
  br i1 %_20002, label %_40000.0, label %_50000.0
_40000.0:
  %_650050 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM13scala.Tuple2$D5applyL16java.lang.ObjectL16java.lang.ObjectL12scala.Tuple2EO" to i8*) to i8* (i8*, i8*, i8*)*
  %_40006 = call dereferenceable_or_null(24) i8* %_650050(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM13scala.Tuple2$G8instance" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-197" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*))
  br label %_60000.0
_50000.0:
  %_650008 = icmp ne i8* %_1, null
  br i1 %_650008, label %_650007.0, label %_650004.0
_650007.0:
  %_650051 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_650052 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_650051, i32 0, i32 1
  %_650009 = bitcast i32* %_650052 to i8*
  %_650053 = bitcast i8* %_650009 to i32*
  %_70001 = load i32, i32* %_650053
  %_50002 = icmp sle i32 %_70001, 0
  br i1 %_50002, label %_80000.0, label %_90000.0
_80000.0:
  %_650011 = icmp ne i8* %_1, null
  br i1 %_650011, label %_650010.0, label %_650004.0
_650010.0:
  %_650054 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_650055 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_650054, i32 0, i32 2
  %_650012 = bitcast i8** %_650055 to i8*
  %_650056 = bitcast i8* %_650012 to i8**
  %_100001 = load i8*, i8** %_650056, !dereferenceable_or_null !{i64 32}
  %_650057 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM13scala.Tuple2$D5applyL16java.lang.ObjectL16java.lang.ObjectL12scala.Tuple2EO" to i8*) to i8* (i8*, i8*, i8*)*
  %_80004 = call dereferenceable_or_null(24) i8* %_650057(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM13scala.Tuple2$G8instance" to i8*), i8* dereferenceable_or_null(32) %_100001, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*))
  br label %_110000.0
_90000.0:
  %_650014 = icmp ne i8* %_1, null
  br i1 %_650014, label %_650013.0, label %_650004.0
_650013.0:
  %_650058 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_650059 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_650058, i32 0, i32 2
  %_650015 = bitcast i8** %_650059 to i8*
  %_650060 = bitcast i8* %_650015 to i8**
  %_120001 = load i8*, i8** %_650060, !dereferenceable_or_null !{i64 32}
  %_90005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-199" to i8*), null
  br i1 %_90005, label %_130000.0, label %_140000.0
_130000.0:
  br label %_150000.0
_140000.0:
  br label %_150000.0
_150000.0:
  %_150001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-199" to i8*), %_140000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_130000.0]
  %_650017 = icmp ne i8* %_1, null
  br i1 %_650017, label %_650016.0, label %_650004.0
_650016.0:
  %_650061 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_650062 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_650061, i32 0, i32 1
  %_650018 = bitcast i32* %_650062 to i8*
  %_650063 = bitcast i8* %_650018 to i32*
  %_160001 = load i32, i32* %_650063
  %_150004 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_160001)
  %_150005 = icmp eq i8* %_150004, null
  br i1 %_150005, label %_170000.0, label %_180000.0
_170000.0:
  br label %_190000.0
_180000.0:
  %_650064 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.lang.IntegerD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_180001 = call dereferenceable_or_null(32) i8* %_650064(i8* nonnull dereferenceable(16) %_150004)
  br label %_190000.0
_190000.0:
  %_190001 = phi i8* [%_180001, %_180000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_170000.0]
  %_650065 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_190002 = call dereferenceable_or_null(32) i8* %_650065(i8* nonnull dereferenceable(32) %_150001, i8* dereferenceable_or_null(32) %_190001)
  %_650066 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM13scala.Tuple2$D5applyL16java.lang.ObjectL16java.lang.ObjectL12scala.Tuple2EO" to i8*) to i8* (i8*, i8*, i8*)*
  %_190003 = call dereferenceable_or_null(24) i8* %_650066(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM13scala.Tuple2$G8instance" to i8*), i8* dereferenceable_or_null(32) %_120001, i8* dereferenceable_or_null(32) %_190002)
  br label %_110000.0
_110000.0:
  %_110001 = phi i8* [%_190003, %_190000.0], [%_80004, %_650010.0]
  br label %_60000.0
_60000.0:
  %_60001 = phi i8* [%_110001, %_110000.0], [%_40006, %_40000.0]
  %_650020 = icmp ne i8* %_60001, null
  br i1 %_650020, label %_650019.0, label %_650004.0
_650019.0:
  %_650067 = bitcast i8* %_60001 to { i8*, i8*, i8* }*
  %_650068 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_650067, i32 0, i32 2
  %_650021 = bitcast i8** %_650068 to i8*
  %_650069 = bitcast i8* %_650021 to i8**
  %_200001 = load i8*, i8** %_650069, !dereferenceable_or_null !{i64 8}
  %_650025 = icmp eq i8* %_200001, null
  br i1 %_650025, label %_650023.0, label %_650022.0
_650022.0:
  %_650070 = bitcast i8* %_200001 to i8**
  %_650026 = load i8*, i8** %_650070
  %_650027 = icmp eq i8* %_650026, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*)
  br i1 %_650027, label %_650023.0, label %_650024.0
_650023.0:
  %_60002 = bitcast i8* %_200001 to i8*
  %_650029 = icmp ne i8* %_60001, null
  br i1 %_650029, label %_650028.0, label %_650004.0
_650028.0:
  %_650071 = bitcast i8* %_60001 to { i8*, i8*, i8* }*
  %_650072 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_650071, i32 0, i32 1
  %_650030 = bitcast i8** %_650072 to i8*
  %_650073 = bitcast i8* %_650030 to i8**
  %_210001 = load i8*, i8** %_650073, !dereferenceable_or_null !{i64 8}
  %_650033 = icmp eq i8* %_210001, null
  br i1 %_650033, label %_650032.0, label %_650031.0
_650031.0:
  %_650074 = bitcast i8* %_210001 to i8**
  %_650034 = load i8*, i8** %_650074
  %_650035 = icmp eq i8* %_650034, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*)
  br i1 %_650035, label %_650032.0, label %_650024.0
_650032.0:
  %_60003 = bitcast i8* %_210001 to i8*
  %_60007 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*), null
  br i1 %_60007, label %_220000.0, label %_230000.0
_220000.0:
  br label %_240000.0
_230000.0:
  br label %_240000.0
_240000.0:
  %_240001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*), %_230000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_220000.0]
  %_650037 = icmp ne i8* %_1, null
  br i1 %_650037, label %_650036.0, label %_650004.0
_650036.0:
  %_650075 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_650076 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_650075, i32 0, i32 4
  %_650038 = bitcast i8** %_650076 to i8*
  %_650077 = bitcast i8* %_650038 to i8**
  %_250001 = load i8*, i8** %_650077, !dereferenceable_or_null !{i64 32}
  %_240003 = icmp eq i8* %_250001, null
  br i1 %_240003, label %_260000.0, label %_270000.0
_260000.0:
  br label %_280000.0
_270000.0:
  br label %_280000.0
_280000.0:
  %_280001 = phi i8* [%_250001, %_270000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_260000.0]
  %_650078 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_280002 = call dereferenceable_or_null(32) i8* %_650078(i8* nonnull dereferenceable(32) %_240001, i8* dereferenceable_or_null(32) %_280001)
  %_280004 = icmp eq i8* %_280002, null
  br i1 %_280004, label %_290000.0, label %_300000.0
_290000.0:
  br label %_310000.0
_300000.0:
  br label %_310000.0
_310000.0:
  %_310001 = phi i8* [%_280002, %_300000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_290000.0]
  %_310005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-201" to i8*), null
  br i1 %_310005, label %_320000.0, label %_330000.0
_320000.0:
  br label %_340000.0
_330000.0:
  br label %_340000.0
_340000.0:
  %_340001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-201" to i8*), %_330000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_320000.0]
  %_650079 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_340002 = call dereferenceable_or_null(32) i8* %_650079(i8* dereferenceable_or_null(32) %_310001, i8* nonnull dereferenceable(32) %_340001)
  %_340004 = icmp eq i8* %_340002, null
  br i1 %_340004, label %_350000.0, label %_360000.0
_350000.0:
  br label %_370000.0
_360000.0:
  br label %_370000.0
_370000.0:
  %_370001 = phi i8* [%_340002, %_360000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_350000.0]
  %_650040 = icmp ne i8* %_1, null
  br i1 %_650040, label %_650039.0, label %_650004.0
_650039.0:
  %_650080 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8* }*
  %_650081 = getelementptr { i8*, i32, i8*, i8*, i8* }, { i8*, i32, i8*, i8*, i8* }* %_650080, i32 0, i32 3
  %_650041 = bitcast i8** %_650081 to i8*
  %_650082 = bitcast i8* %_650041 to i8**
  %_380001 = load i8*, i8** %_650082, !dereferenceable_or_null !{i64 32}
  %_370003 = icmp eq i8* %_380001, null
  br i1 %_370003, label %_390000.0, label %_400000.0
_390000.0:
  br label %_410000.0
_400000.0:
  br label %_410000.0
_410000.0:
  %_410001 = phi i8* [%_380001, %_400000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_390000.0]
  %_650083 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_410002 = call dereferenceable_or_null(32) i8* %_650083(i8* dereferenceable_or_null(32) %_370001, i8* dereferenceable_or_null(32) %_410001)
  %_410004 = icmp eq i8* %_410002, null
  br i1 %_410004, label %_420000.0, label %_430000.0
_420000.0:
  br label %_440000.0
_430000.0:
  br label %_440000.0
_440000.0:
  %_440001 = phi i8* [%_410002, %_430000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_420000.0]
  %_440005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-203" to i8*), null
  br i1 %_440005, label %_450000.0, label %_460000.0
_450000.0:
  br label %_470000.0
_460000.0:
  br label %_470000.0
_470000.0:
  %_470001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-203" to i8*), %_460000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_450000.0]
  %_650084 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_470002 = call dereferenceable_or_null(32) i8* %_650084(i8* dereferenceable_or_null(32) %_440001, i8* nonnull dereferenceable(32) %_470001)
  %_470004 = icmp eq i8* %_470002, null
  br i1 %_470004, label %_480000.0, label %_490000.0
_480000.0:
  br label %_500000.0
_490000.0:
  br label %_500000.0
_500000.0:
  %_500001 = phi i8* [%_470002, %_490000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_480000.0]
  %_500003 = icmp eq i8* %_60002, null
  br i1 %_500003, label %_510000.0, label %_520000.0
_510000.0:
  br label %_530000.0
_520000.0:
  br label %_530000.0
_530000.0:
  %_530001 = phi i8* [%_60002, %_520000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_510000.0]
  %_650085 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_530002 = call dereferenceable_or_null(32) i8* %_650085(i8* dereferenceable_or_null(32) %_500001, i8* dereferenceable_or_null(32) %_530001)
  %_530004 = icmp eq i8* %_530002, null
  br i1 %_530004, label %_540000.0, label %_550000.0
_540000.0:
  br label %_560000.0
_550000.0:
  br label %_560000.0
_560000.0:
  %_560001 = phi i8* [%_530002, %_550000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_540000.0]
  %_560003 = icmp eq i8* %_60003, null
  br i1 %_560003, label %_570000.0, label %_580000.0
_570000.0:
  br label %_590000.0
_580000.0:
  br label %_590000.0
_590000.0:
  %_590001 = phi i8* [%_60003, %_580000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_570000.0]
  %_650086 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_590002 = call dereferenceable_or_null(32) i8* %_650086(i8* dereferenceable_or_null(32) %_560001, i8* dereferenceable_or_null(32) %_590001)
  %_590004 = icmp eq i8* %_590002, null
  br i1 %_590004, label %_600000.0, label %_610000.0
_600000.0:
  br label %_620000.0
_610000.0:
  br label %_620000.0
_620000.0:
  %_620001 = phi i8* [%_590002, %_610000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_600000.0]
  %_620005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-205" to i8*), null
  br i1 %_620005, label %_630000.0, label %_640000.0
_630000.0:
  br label %_650000.0
_640000.0:
  br label %_650000.0
_650000.0:
  %_650001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-205" to i8*), %_640000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_630000.0]
  %_650087 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_650002 = call dereferenceable_or_null(32) i8* %_650087(i8* dereferenceable_or_null(32) %_620001, i8* nonnull dereferenceable(32) %_650001)
  ret i8* %_650002
_650004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_650024.0:
  %_650043 = phi i8* [%_200001, %_650022.0], [%_210001, %_650031.0]
  %_650044 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), %_650022.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), %_650031.0]
  %_650088 = bitcast i8* %_650043 to i8**
  %_650045 = load i8*, i8** %_650088
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_650045, i8* %_650044)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM27java.lang.System$$$Lambda$3D5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30008 = icmp ne i8* %_1, null
  br i1 %_30008, label %_30006.0, label %_30007.0
_30006.0:
  %_30014 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_30015 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_30014, i32 0, i32 1
  %_30009 = bitcast i8** %_30015 to i8*
  %_30016 = bitcast i8* %_30009 to i8**
  %_30002 = load i8*, i8** %_30016, !dereferenceable_or_null !{i64 56}
  %_30011 = icmp ne i8* %_1, null
  br i1 %_30011, label %_30010.0, label %_30007.0
_30010.0:
  %_30017 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_30018 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_30017, i32 0, i32 2
  %_30012 = bitcast i8** %_30018 to i8*
  %_30019 = bitcast i8* %_30012 to i8**
  %_30003 = load i8*, i8** %_30019, !dereferenceable_or_null !{i64 24}
  %_30004 = bitcast i8* %_2 to i8*
  %_30005 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D25loadProperties$$anonfun$2L20java.util.PropertiesL16java.lang.StringL16java.lang.ObjectEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_30002, i8* dereferenceable_or_null(24) %_30003, i8* dereferenceable_or_null(32) %_30004)
  ret i8* %_30005
_30007.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM27scala.collection.SeqView$IdD5applyiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30007 = icmp ne i8* %_1, null
  br i1 %_30007, label %_30005.0, label %_30006.0
_30005.0:
  %_30017 = bitcast i8* %_1 to { i8*, i8* }*
  %_30018 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30017, i32 0, i32 1
  %_30008 = bitcast i8** %_30018 to i8*
  %_30019 = bitcast i8* %_30008 to i8**
  %_30001 = load i8*, i8** %_30019, !dereferenceable_or_null !{i64 8}
  %_30010 = icmp ne i8* %_30001, null
  br i1 %_30010, label %_30009.0, label %_30006.0
_30009.0:
  %_30020 = bitcast i8* %_30001 to i8**
  %_30011 = load i8*, i8** %_30020
  %_30021 = bitcast i8* %_30011 to { i8*, i32, i32, i8* }*
  %_30022 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30021, i32 0, i32 2
  %_30012 = bitcast i32* %_30022 to i8*
  %_30023 = bitcast i8* %_30012 to i32*
  %_30013 = load i32, i32* %_30023
  %_30024 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30025 = getelementptr i8*, i8** %_30024, i32 4209
  %_30014 = bitcast i8** %_30025 to i8*
  %_30026 = bitcast i8* %_30014 to i8**
  %_30027 = getelementptr i8*, i8** %_30026, i32 %_30013
  %_30015 = bitcast i8** %_30027 to i8*
  %_30028 = bitcast i8* %_30015 to i8**
  %_30003 = load i8*, i8** %_30028
  %_30029 = bitcast i8* %_30003 to i8* (i8*, i32)*
  %_30004 = call dereferenceable_or_null(8) i8* %_30029(i8* dereferenceable_or_null(8) %_30001, i32 %_2)
  ret i8* %_30004
_30006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM27scala.collection.SeqView$IdD6lengthiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20007 = icmp ne i8* %_1, null
  br i1 %_20007, label %_20005.0, label %_20006.0
_20005.0:
  %_20017 = bitcast i8* %_1 to { i8*, i8* }*
  %_20018 = getelementptr { i8*, i8* }, { i8*, i8* }* %_20017, i32 0, i32 1
  %_20008 = bitcast i8** %_20018 to i8*
  %_20019 = bitcast i8* %_20008 to i8**
  %_20001 = load i8*, i8** %_20019, !dereferenceable_or_null !{i64 8}
  %_20010 = icmp ne i8* %_20001, null
  br i1 %_20010, label %_20009.0, label %_20006.0
_20009.0:
  %_20020 = bitcast i8* %_20001 to i8**
  %_20011 = load i8*, i8** %_20020
  %_20021 = bitcast i8* %_20011 to { i8*, i32, i32, i8* }*
  %_20022 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_20021, i32 0, i32 2
  %_20012 = bitcast i32* %_20022 to i8*
  %_20023 = bitcast i8* %_20012 to i32*
  %_20013 = load i32, i32* %_20023
  %_20024 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_20025 = getelementptr i8*, i8** %_20024, i32 4009
  %_20014 = bitcast i8** %_20025 to i8*
  %_20026 = bitcast i8* %_20014 to i8**
  %_20027 = getelementptr i8*, i8** %_20026, i32 %_20013
  %_20015 = bitcast i8** %_20027 to i8*
  %_20028 = bitcast i8* %_20015 to i8**
  %_20003 = load i8*, i8** %_20028
  %_20029 = bitcast i8* %_20003 to i32 (i8*)*
  %_20004 = call i32 %_20029(i8* dereferenceable_or_null(8) %_20001)
  ret i32 %_20004
_20006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM27scala.collection.SeqView$IdD7isEmptyzEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20007 = icmp ne i8* %_1, null
  br i1 %_20007, label %_20005.0, label %_20006.0
_20005.0:
  %_20017 = bitcast i8* %_1 to { i8*, i8* }*
  %_20018 = getelementptr { i8*, i8* }, { i8*, i8* }* %_20017, i32 0, i32 1
  %_20008 = bitcast i8** %_20018 to i8*
  %_20019 = bitcast i8* %_20008 to i8**
  %_20001 = load i8*, i8** %_20019, !dereferenceable_or_null !{i64 8}
  %_20010 = icmp ne i8* %_20001, null
  br i1 %_20010, label %_20009.0, label %_20006.0
_20009.0:
  %_20020 = bitcast i8* %_20001 to i8**
  %_20011 = load i8*, i8** %_20020
  %_20021 = bitcast i8* %_20011 to { i8*, i32, i32, i8* }*
  %_20022 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_20021, i32 0, i32 2
  %_20012 = bitcast i32* %_20022 to i8*
  %_20023 = bitcast i8* %_20012 to i32*
  %_20013 = load i32, i32* %_20023
  %_20024 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_20025 = getelementptr i8*, i8** %_20024, i32 223
  %_20014 = bitcast i8** %_20025 to i8*
  %_20026 = bitcast i8* %_20014 to i8**
  %_20027 = getelementptr i8*, i8** %_20026, i32 %_20013
  %_20015 = bitcast i8** %_20027 to i8*
  %_20028 = bitcast i8* %_20015 to i8**
  %_20003 = load i8*, i8** %_20028
  %_20029 = bitcast i8* %_20003 to i1 (i8*)*
  %_20004 = call i1 %_20029(i8* dereferenceable_or_null(8) %_20001)
  ret i1 %_20004
_20006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D14drop$extensionL16java.lang.StringiL16java.lang.StringEO"(i8* %_1, i8* %_2, i32 %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40003 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* dereferenceable_or_null(32) %_2)
  %_40004 = call i32 @"_SM19scala.math.package$D3miniiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19scala.math.package$G8instance" to i8*), i32 %_3, i32 %_40003)
  %_40005 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* dereferenceable_or_null(32) %_2)
  %_40006 = call dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D15slice$extensionL16java.lang.StringiiL16java.lang.StringEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_2, i32 %_40004, i32 %_40005)
  ret i8* %_40006
}

define dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D15slice$extensionL16java.lang.StringiiL16java.lang.StringEO"(i8* %_1, i8* %_2, i32 %_3, i32 %_4) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_50003 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_50004 = call i32 @"_SM26scala.LowPriorityImplicitsD10intWrapperiiEO"(i8* nonnull dereferenceable(16) %_50003, i32 %_3)
  %_50005 = call i32 @"_SM22scala.runtime.RichInt$D13max$extensioniiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM22scala.runtime.RichInt$G8instance" to i8*), i32 %_50004, i32 0)
  %_50006 = call i32 @"_SM26scala.LowPriorityImplicitsD10intWrapperiiEO"(i8* nonnull dereferenceable(16) %_50003, i32 %_4)
  %_50007 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* dereferenceable_or_null(32) %_2)
  %_50008 = call i32 @"_SM22scala.runtime.RichInt$D13min$extensioniiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM22scala.runtime.RichInt$G8instance" to i8*), i32 %_50006, i32 %_50007)
  %_50010 = icmp sge i32 %_50005, %_50008
  br i1 %_50010, label %_60000.0, label %_70000.0
_60000.0:
  br label %_80000.0
_70000.0:
  %_70001 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD9substringiiL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_2, i32 %_50005, i32 %_50008)
  br label %_80000.0
_80000.0:
  %_80001 = phi i8* [%_70001, %_70000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*), %_60000.0]
  ret i8* %_80001
}

define i32 @"_SM27scala.collection.StringOps$D15toInt$extensionL16java.lang.StringiEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i32 @"_SM17java.lang.IntegerD8parseIntL16java.lang.StringiEo"(i8* dereferenceable_or_null(32) %_2)
  ret i32 %_30001
}

define dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D16format$extensionL16java.lang.StringL30scala.collection.immutable.SeqL16java.lang.StringEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40018 = icmp ne i8* %_3, null
  br i1 %_40018, label %_40016.0, label %_40017.0
_40016.0:
  %_40054 = bitcast i8* %_3 to i8**
  %_40019 = load i8*, i8** %_40054
  %_40055 = bitcast i8* %_40019 to { i8*, i32, i32, i8* }*
  %_40056 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40055, i32 0, i32 2
  %_40020 = bitcast i32* %_40056 to i8*
  %_40057 = bitcast i8* %_40020 to i32*
  %_40021 = load i32, i32* %_40057
  %_40058 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_40059 = getelementptr i8*, i8** %_40058, i32 4421
  %_40022 = bitcast i8** %_40059 to i8*
  %_40060 = bitcast i8* %_40022 to i8**
  %_40061 = getelementptr i8*, i8** %_40060, i32 %_40021
  %_40023 = bitcast i8** %_40061 to i8*
  %_40062 = bitcast i8* %_40023 to i8**
  %_40003 = load i8*, i8** %_40062
  %_40004 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM37scala.collection.StringOps$$$Lambda$6G4type" to i8*), i64 24)
  %_40063 = bitcast i8* %_40004 to { i8*, i8*, i8* }*
  %_40064 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_40063, i32 0, i32 2
  %_40025 = bitcast i8** %_40064 to i8*
  %_40065 = bitcast i8* %_40025 to i8**
  store i8* %_2, i8** %_40065
  %_40066 = bitcast i8* %_40004 to { i8*, i8*, i8* }*
  %_40067 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_40066, i32 0, i32 1
  %_40027 = bitcast i8** %_40067 to i8*
  %_40068 = bitcast i8* %_40027 to i8**
  store i8* %_1, i8** %_40068
  %_40069 = bitcast i8* %_40003 to i8* (i8*, i8*)*
  %_40007 = call dereferenceable_or_null(8) i8* %_40069(i8* dereferenceable_or_null(8) %_3, i8* nonnull dereferenceable(24) %_40004)
  %_40031 = icmp eq i8* %_40007, null
  br i1 %_40031, label %_40029.0, label %_40028.0
_40028.0:
  %_40070 = bitcast i8* %_40007 to i8**
  %_40032 = load i8*, i8** %_40070
  %_40071 = bitcast i8* %_40032 to { i8*, i32, i32, i8* }*
  %_40072 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40071, i32 0, i32 1
  %_40033 = bitcast i32* %_40072 to i8*
  %_40073 = bitcast i8* %_40033 to i32*
  %_40034 = load i32, i32* %_40073
  %_40074 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_40075 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_40074, i32 0, i32 %_40034, i32 70
  %_40035 = bitcast i1* %_40075 to i8*
  %_40076 = bitcast i8* %_40035 to i1*
  %_40036 = load i1, i1* %_40076
  br i1 %_40036, label %_40029.0, label %_40030.0
_40029.0:
  %_40008 = bitcast i8* %_40007 to i8*
  %_40077 = bitcast i8* bitcast (i8* (i8*)* @"_SM23scala.reflect.ClassTag$D6AnyRefL22scala.reflect.ClassTagEO" to i8*) to i8* (i8*)*
  %_40010 = call dereferenceable_or_null(8) i8* %_40077(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM23scala.reflect.ClassTag$G8instance" to i8*))
  %_40038 = icmp ne i8* %_40008, null
  br i1 %_40038, label %_40037.0, label %_40017.0
_40037.0:
  %_40078 = bitcast i8* %_40008 to i8**
  %_40039 = load i8*, i8** %_40078
  %_40079 = bitcast i8* %_40039 to { i8*, i32, i32, i8* }*
  %_40080 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40079, i32 0, i32 2
  %_40040 = bitcast i32* %_40080 to i8*
  %_40081 = bitcast i8* %_40040 to i32*
  %_40041 = load i32, i32* %_40081
  %_40082 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_40083 = getelementptr i8*, i8** %_40082, i32 332
  %_40042 = bitcast i8** %_40083 to i8*
  %_40084 = bitcast i8* %_40042 to i8**
  %_40085 = getelementptr i8*, i8** %_40084, i32 %_40041
  %_40043 = bitcast i8** %_40085 to i8*
  %_40086 = bitcast i8* %_40043 to i8**
  %_40012 = load i8*, i8** %_40086
  %_40087 = bitcast i8* %_40012 to i8* (i8*, i8*)*
  %_40013 = call dereferenceable_or_null(8) i8* %_40087(i8* dereferenceable_or_null(8) %_40008, i8* dereferenceable_or_null(8) %_40010)
  %_40046 = icmp eq i8* %_40013, null
  br i1 %_40046, label %_40045.0, label %_40044.0
_40044.0:
  %_40088 = bitcast i8* %_40013 to i8**
  %_40047 = load i8*, i8** %_40088
  %_40048 = icmp eq i8* %_40047, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.ObjectArrayG4type" to i8*)
  br i1 %_40048, label %_40045.0, label %_40030.0
_40045.0:
  %_40014 = bitcast i8* %_40013 to i8*
  %_40015 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD6formatL16java.lang.StringLAL16java.lang.Object_L16java.lang.StringEo"(i8* dereferenceable_or_null(32) %_2, i8* dereferenceable_or_null(8) %_40014)
  ret i8* %_40015
_40017.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_40030.0:
  %_40050 = phi i8* [%_40007, %_40028.0], [%_40013, %_40044.0]
  %_40051 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM32scala.collection.IterableOnceOpsG4type" to i8*), %_40028.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.ObjectArrayG4type" to i8*), %_40044.0]
  %_40089 = bitcast i8* %_40050 to i8**
  %_40052 = load i8*, i8** %_40089
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_40052, i8* %_40051)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM27scala.collection.StringOps$D17$anonfun$format$1L16java.lang.StringL16java.lang.ObjectL16java.lang.ObjectEPT27scala.collection.StringOps$"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40002 = call dereferenceable_or_null(8) i8* @"_SM27scala.collection.StringOps$D19unwrapArg$extensionL16java.lang.StringL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_2, i8* dereferenceable_or_null(8) %_3)
  ret i8* %_40002
}

define dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D19dropWhile$extensionL16java.lang.StringL15scala.Function1L16java.lang.StringEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40003 = call i32 @"_SM27scala.collection.StringOps$D30indexWhere$default$2$extensionL16java.lang.StringiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_2)
  %_40004 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM38scala.collection.StringOps$$$Lambda$13G4type" to i8*), i64 24)
  %_90006 = bitcast i8* %_40004 to { i8*, i8*, i8* }*
  %_90007 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_90006, i32 0, i32 2
  %_90003 = bitcast i8** %_90007 to i8*
  %_90008 = bitcast i8* %_90003 to i8**
  store i8* %_3, i8** %_90008
  %_90009 = bitcast i8* %_40004 to { i8*, i8*, i8* }*
  %_90010 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_90009, i32 0, i32 1
  %_90005 = bitcast i8** %_90010 to i8*
  %_90011 = bitcast i8* %_90005 to i8**
  store i8* %_1, i8** %_90011
  %_40007 = call i32 @"_SM27scala.collection.StringOps$D20indexWhere$extensionL16java.lang.StringL15scala.Function1iiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_2, i8* nonnull dereferenceable(24) %_40004, i32 %_40003)
  switch i32 %_40007, label %_70000.0 [
    i32 -1, label %_80000.0
  ]
_70000.0:
  %_70001 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD9substringiL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_2, i32 %_40007)
  br label %_90000.0
_80000.0:
  br label %_90000.0
_90000.0:
  %_90001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*), %_80000.0], [%_70001, %_70000.0]
  ret i8* %_90001
}

define dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D19takeWhile$extensionL16java.lang.StringL15scala.Function1L16java.lang.StringEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40003 = call i32 @"_SM27scala.collection.StringOps$D30indexWhere$default$2$extensionL16java.lang.StringiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_2)
  %_40004 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM38scala.collection.StringOps$$$Lambda$14G4type" to i8*), i64 24)
  %_90006 = bitcast i8* %_40004 to { i8*, i8*, i8* }*
  %_90007 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_90006, i32 0, i32 2
  %_90003 = bitcast i8** %_90007 to i8*
  %_90008 = bitcast i8* %_90003 to i8**
  store i8* %_3, i8** %_90008
  %_90009 = bitcast i8* %_40004 to { i8*, i8*, i8* }*
  %_90010 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_90009, i32 0, i32 1
  %_90005 = bitcast i8** %_90010 to i8*
  %_90011 = bitcast i8* %_90005 to i8**
  store i8* %_1, i8** %_90011
  %_40007 = call i32 @"_SM27scala.collection.StringOps$D20indexWhere$extensionL16java.lang.StringL15scala.Function1iiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_2, i8* nonnull dereferenceable(24) %_40004, i32 %_40003)
  switch i32 %_40007, label %_70000.0 [
    i32 -1, label %_80000.0
  ]
_70000.0:
  %_70001 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD9substringiiL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_2, i32 0, i32 %_40007)
  br label %_90000.0
_80000.0:
  br label %_90000.0
_90000.0:
  %_90001 = phi i8* [%_2, %_80000.0], [%_70001, %_70000.0]
  ret i8* %_90001
}

define dereferenceable_or_null(8) i8* @"_SM27scala.collection.StringOps$D19unwrapArg$extensionL16java.lang.StringL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  br label %_50000.0
_50000.0:
  %_90004 = icmp eq i8* %_3, null
  br i1 %_90004, label %_90001.0, label %_90002.0
_90001.0:
  br label %_90003.0
_90002.0:
  %_90023 = bitcast i8* %_3 to i8**
  %_90005 = load i8*, i8** %_90023
  %_90006 = icmp eq i8* %_90005, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM22scala.math.ScalaNumberG4type" to i8*)
  br label %_90003.0
_90003.0:
  %_50002 = phi i1 [%_90006, %_90002.0], [false, %_90001.0]
  br i1 %_50002, label %_60000.0, label %_70000.0
_70000.0:
  br label %_80000.0
_80000.0:
  br label %_90000.0
_90000.0:
  ret i8* %_3
_60000.0:
  %_90010 = icmp eq i8* %_3, null
  br i1 %_90010, label %_90008.0, label %_90007.0
_90007.0:
  %_90024 = bitcast i8* %_3 to i8**
  %_90011 = load i8*, i8** %_90024
  %_90012 = icmp eq i8* %_90011, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM22scala.math.ScalaNumberG4type" to i8*)
  br i1 %_90012, label %_90008.0, label %_90009.0
_90008.0:
  %_60001 = bitcast i8* %_3 to i8*
  %_90015 = icmp ne i8* %_60001, null
  br i1 %_90015, label %_90013.0, label %_90014.0
_90013.0:
  br label %_90016.0
_90014.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_90009.0:
  %_90018 = phi i8* [%_3, %_90007.0]
  %_90019 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM22scala.math.ScalaNumberG4type" to i8*), %_90007.0]
  %_90025 = bitcast i8* %_90018 to i8**
  %_90020 = load i8*, i8** %_90025
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_90020, i8* %_90019)
  unreachable
_90016.0:
  %_90026 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_90026(i8* null)
  unreachable
}

define i1 @"_SM27scala.collection.StringOps$D20$anonfun$dropWhile$1L15scala.Function1czEPT27scala.collection.StringOps$"(i8* %_1, i8* %_2, i16 %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40011 = icmp ne i8* %_2, null
  br i1 %_40011, label %_40009.0, label %_40010.0
_40009.0:
  %_40018 = bitcast i8* %_2 to i8**
  %_40012 = load i8*, i8** %_40018
  %_40019 = bitcast i8* %_40012 to { i8*, i32, i32, i8* }*
  %_40020 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40019, i32 0, i32 2
  %_40013 = bitcast i32* %_40020 to i8*
  %_40021 = bitcast i8* %_40013 to i32*
  %_40014 = load i32, i32* %_40021
  %_40022 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_40023 = getelementptr i8*, i8** %_40022, i32 1010
  %_40015 = bitcast i8** %_40023 to i8*
  %_40024 = bitcast i8* %_40015 to i8**
  %_40025 = getelementptr i8*, i8** %_40024, i32 %_40014
  %_40016 = bitcast i8** %_40025 to i8*
  %_40026 = bitcast i8* %_40016 to i8**
  %_40003 = load i8*, i8** %_40026
  %_40004 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8* null, i16 %_3)
  %_40027 = bitcast i8* %_40003 to i8* (i8*, i8*)*
  %_40005 = call dereferenceable_or_null(8) i8* %_40027(i8* dereferenceable_or_null(8) %_2, i8* nonnull dereferenceable(16) %_40004)
  %_40028 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D14unboxToBooleanL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_40006 = call i1 %_40028(i8* null, i8* dereferenceable_or_null(8) %_40005)
  %_40008 = xor i1 %_40006, true
  ret i1 %_40008
_40010.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM27scala.collection.StringOps$D20$anonfun$takeWhile$1L15scala.Function1czEPT27scala.collection.StringOps$"(i8* %_1, i8* %_2, i16 %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40011 = icmp ne i8* %_2, null
  br i1 %_40011, label %_40009.0, label %_40010.0
_40009.0:
  %_40018 = bitcast i8* %_2 to i8**
  %_40012 = load i8*, i8** %_40018
  %_40019 = bitcast i8* %_40012 to { i8*, i32, i32, i8* }*
  %_40020 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40019, i32 0, i32 2
  %_40013 = bitcast i32* %_40020 to i8*
  %_40021 = bitcast i8* %_40013 to i32*
  %_40014 = load i32, i32* %_40021
  %_40022 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_40023 = getelementptr i8*, i8** %_40022, i32 1010
  %_40015 = bitcast i8** %_40023 to i8*
  %_40024 = bitcast i8* %_40015 to i8**
  %_40025 = getelementptr i8*, i8** %_40024, i32 %_40014
  %_40016 = bitcast i8** %_40025 to i8*
  %_40026 = bitcast i8* %_40016 to i8**
  %_40003 = load i8*, i8** %_40026
  %_40004 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8* null, i16 %_3)
  %_40027 = bitcast i8* %_40003 to i8* (i8*, i8*)*
  %_40005 = call dereferenceable_or_null(8) i8* %_40027(i8* dereferenceable_or_null(8) %_2, i8* nonnull dereferenceable(16) %_40004)
  %_40028 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D14unboxToBooleanL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_40006 = call i1 %_40028(i8* null, i8* dereferenceable_or_null(8) %_40005)
  %_40008 = xor i1 %_40006, true
  ret i1 %_40008
_40010.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM27scala.collection.StringOps$D20indexWhere$extensionL16java.lang.StringL15scala.Function1iiEO"(i8* %_1, i8* %_2, i8* %_3, i32 %_4) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_50002 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* dereferenceable_or_null(32) %_2)
  br label %_60000.0
_60000.0:
  %_60001 = phi i32 [%_4, %_50000.0], [%_110002, %_110000.0]
  %_60003 = icmp slt i32 %_60001, %_50002
  br i1 %_60003, label %_70000.0, label %_80000.0
_70000.0:
  %_70001 = call i16 @"_SM16java.lang.StringD6charAticEO"(i8* dereferenceable_or_null(32) %_2, i32 %_60001)
  %_120003 = icmp ne i8* %_3, null
  br i1 %_120003, label %_120001.0, label %_120002.0
_120001.0:
  %_120010 = bitcast i8* %_3 to i8**
  %_120004 = load i8*, i8** %_120010
  %_120011 = bitcast i8* %_120004 to { i8*, i32, i32, i8* }*
  %_120012 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_120011, i32 0, i32 2
  %_120005 = bitcast i32* %_120012 to i8*
  %_120013 = bitcast i8* %_120005 to i32*
  %_120006 = load i32, i32* %_120013
  %_120014 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_120015 = getelementptr i8*, i8** %_120014, i32 1010
  %_120007 = bitcast i8** %_120015 to i8*
  %_120016 = bitcast i8* %_120007 to i8**
  %_120017 = getelementptr i8*, i8** %_120016, i32 %_120006
  %_120008 = bitcast i8** %_120017 to i8*
  %_120018 = bitcast i8* %_120008 to i8**
  %_70004 = load i8*, i8** %_120018
  %_70005 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8* null, i16 %_70001)
  %_120019 = bitcast i8* %_70004 to i8* (i8*, i8*)*
  %_70006 = call dereferenceable_or_null(8) i8* %_120019(i8* dereferenceable_or_null(8) %_3, i8* nonnull dereferenceable(16) %_70005)
  %_120020 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D14unboxToBooleanL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_70007 = call i1 %_120020(i8* null, i8* dereferenceable_or_null(8) %_70006)
  br i1 %_70007, label %_90000.0, label %_100000.0
_90000.0:
  ret i32 %_60001
_100000.0:
  br label %_110000.0
_110000.0:
  %_110002 = add i32 %_60001, 1
  br label %_60000.0
_80000.0:
  br label %_120000.0
_120000.0:
  ret i32 -1
_120002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM27scala.collection.StringOps$D30indexWhere$default$2$extensionL16java.lang.StringiEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i32 0
}

define nonnull dereferenceable(8) i8* @"_SM27scala.collection.StringOps$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(16) i8* @"_SM28java.lang.System$$$Lambda$15D5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_40005 = icmp ne i8* %_1, null
  br i1 %_40005, label %_40003.0, label %_40004.0
_40003.0:
  %_40008 = bitcast i8* %_1 to { i8*, i8* }*
  %_40009 = getelementptr { i8*, i8* }, { i8*, i8* }* %_40008, i32 0, i32 1
  %_40006 = bitcast i8** %_40009 to i8*
  %_40010 = bitcast i8* %_40006 to i8**
  %_30001 = load i8*, i8** %_40010, !dereferenceable_or_null !{i64 56}
  %_40011 = bitcast i8* bitcast (i16 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D11unboxToCharL16java.lang.ObjectcEO" to i8*) to i16 (i8*, i8*)*
  %_40001 = call i16 %_40011(i8* null, i8* dereferenceable_or_null(8) %_2)
  %_40002 = call i1 @"_SM17java.lang.System$D36getUserCountry$$anonfun$1$$anonfun$1czEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_30001, i16 %_40001)
  %_30003 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToBooleanzL17java.lang.BooleanEO"(i8* null, i1 %_40002)
  ret i8* %_30003
_40004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM28scala.collection.mutable.MapD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(8) i8* @"_SM29java.io.PrintStream$$Lambda$5D12apply$mcV$spuEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20006 = icmp ne i8* %_1, null
  br i1 %_20006, label %_20004.0, label %_20005.0
_20004.0:
  %_20013 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_20014 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_20013, i32 0, i32 1
  %_20007 = bitcast i8** %_20014 to i8*
  %_20015 = bitcast i8* %_20007 to i8**
  %_20001 = load i8*, i8** %_20015, !dereferenceable_or_null !{i64 56}
  %_20009 = icmp ne i8* %_1, null
  br i1 %_20009, label %_20008.0, label %_20005.0
_20008.0:
  %_20016 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_20017 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_20016, i32 0, i32 2
  %_20010 = bitcast i8** %_20017 to i8*
  %_20018 = bitcast i8* %_20010 to i8**
  %_20002 = load i8*, i8** %_20018, !dereferenceable_or_null !{i64 32}
  call nonnull dereferenceable(8) i8* @"_SM19java.io.PrintStreamD22printString$$anonfun$1L16java.lang.StringuEPT19java.io.PrintStream"(i8* dereferenceable_or_null(56) %_20001, i8* dereferenceable_or_null(32) %_20002)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_20005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM30java.math.RoundingMode$$anon$4D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM30java.math.RoundingMode$$anon$4D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM30java.math.RoundingMode$$anon$4D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define i32 @"_SM30scala.collection.IterableOnce$D16copyElemsToArrayL29scala.collection.IterableOnceL16java.lang.ObjectiiiEO"(i8* %_1, i8* %_2, i8* %_3, i32 %_4, i32 %_5) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  br label %_70000.0
_70000.0:
  %_110010 = icmp eq i8* %_2, null
  br i1 %_110010, label %_110007.0, label %_110008.0
_110007.0:
  br label %_110009.0
_110008.0:
  %_110052 = bitcast i8* %_2 to i8**
  %_110011 = load i8*, i8** %_110052
  %_110053 = bitcast i8* %_110011 to { i8*, i32, i32, i8* }*
  %_110054 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110053, i32 0, i32 1
  %_110012 = bitcast i32* %_110054 to i8*
  %_110055 = bitcast i8* %_110012 to i32*
  %_110013 = load i32, i32* %_110055
  %_110056 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_110057 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_110056, i32 0, i32 %_110013, i32 43
  %_110014 = bitcast i1* %_110057 to i8*
  %_110058 = bitcast i8* %_110014 to i1*
  %_110015 = load i1, i1* %_110058
  br label %_110009.0
_110009.0:
  %_70002 = phi i1 [%_110015, %_110008.0], [false, %_110007.0]
  br i1 %_70002, label %_80000.0, label %_90000.0
_80000.0:
  %_110019 = icmp eq i8* %_2, null
  br i1 %_110019, label %_110017.0, label %_110016.0
_110016.0:
  %_110059 = bitcast i8* %_2 to i8**
  %_110020 = load i8*, i8** %_110059
  %_110060 = bitcast i8* %_110020 to { i8*, i32, i32, i8* }*
  %_110061 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110060, i32 0, i32 1
  %_110021 = bitcast i32* %_110061 to i8*
  %_110062 = bitcast i8* %_110021 to i32*
  %_110022 = load i32, i32* %_110062
  %_110063 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_110064 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_110063, i32 0, i32 %_110022, i32 43
  %_110023 = bitcast i1* %_110064 to i8*
  %_110065 = bitcast i8* %_110023 to i1*
  %_110024 = load i1, i1* %_110065
  br i1 %_110024, label %_110017.0, label %_110018.0
_110017.0:
  %_80001 = bitcast i8* %_2 to i8*
  %_110027 = icmp ne i8* %_80001, null
  br i1 %_110027, label %_110025.0, label %_110026.0
_110025.0:
  %_110066 = bitcast i8* %_80001 to i8**
  %_110028 = load i8*, i8** %_110066
  %_110067 = bitcast i8* %_110028 to { i8*, i32, i32, i8* }*
  %_110068 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110067, i32 0, i32 2
  %_110029 = bitcast i32* %_110068 to i8*
  %_110069 = bitcast i8* %_110029 to i32*
  %_110030 = load i32, i32* %_110069
  %_110070 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_110071 = getelementptr i8*, i8** %_110070, i32 555
  %_110031 = bitcast i8** %_110071 to i8*
  %_110072 = bitcast i8* %_110031 to i8**
  %_110073 = getelementptr i8*, i8** %_110072, i32 %_110030
  %_110032 = bitcast i8** %_110073 to i8*
  %_110074 = bitcast i8* %_110032 to i8**
  %_80003 = load i8*, i8** %_110074
  %_110075 = bitcast i8* %_80003 to i32 (i8*, i8*, i32, i32)*
  %_80004 = call i32 %_110075(i8* dereferenceable_or_null(8) %_80001, i8* dereferenceable_or_null(8) %_3, i32 %_4, i32 %_5)
  br label %_100000.0
_90000.0:
  br label %_110000.0
_110000.0:
  %_110034 = icmp ne i8* %_2, null
  br i1 %_110034, label %_110033.0, label %_110026.0
_110033.0:
  %_110076 = bitcast i8* %_2 to i8**
  %_110035 = load i8*, i8** %_110076
  %_110077 = bitcast i8* %_110035 to { i8*, i32, i32, i8* }*
  %_110078 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110077, i32 0, i32 2
  %_110036 = bitcast i32* %_110078 to i8*
  %_110079 = bitcast i8* %_110036 to i32*
  %_110037 = load i32, i32* %_110079
  %_110080 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_110081 = getelementptr i8*, i8** %_110080, i32 446
  %_110038 = bitcast i8** %_110081 to i8*
  %_110082 = bitcast i8* %_110038 to i8**
  %_110083 = getelementptr i8*, i8** %_110082, i32 %_110037
  %_110039 = bitcast i8** %_110083 to i8*
  %_110084 = bitcast i8* %_110039 to i8**
  %_110002 = load i8*, i8** %_110084
  %_110085 = bitcast i8* %_110002 to i8* (i8*)*
  %_110003 = call dereferenceable_or_null(8) i8* %_110085(i8* dereferenceable_or_null(8) %_2)
  %_110041 = icmp ne i8* %_110003, null
  br i1 %_110041, label %_110040.0, label %_110026.0
_110040.0:
  %_110086 = bitcast i8* %_110003 to i8**
  %_110042 = load i8*, i8** %_110086
  %_110087 = bitcast i8* %_110042 to { i8*, i32, i32, i8* }*
  %_110088 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110087, i32 0, i32 2
  %_110043 = bitcast i32* %_110088 to i8*
  %_110089 = bitcast i8* %_110043 to i32*
  %_110044 = load i32, i32* %_110089
  %_110090 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_110091 = getelementptr i8*, i8** %_110090, i32 555
  %_110045 = bitcast i8** %_110091 to i8*
  %_110092 = bitcast i8* %_110045 to i8**
  %_110093 = getelementptr i8*, i8** %_110092, i32 %_110044
  %_110046 = bitcast i8** %_110093 to i8*
  %_110094 = bitcast i8* %_110046 to i8**
  %_110005 = load i8*, i8** %_110094
  %_110095 = bitcast i8* %_110005 to i32 (i8*, i8*, i32, i32)*
  %_110006 = call i32 %_110095(i8* dereferenceable_or_null(8) %_110003, i8* dereferenceable_or_null(8) %_3, i32 %_4, i32 %_5)
  br label %_100000.0
_100000.0:
  %_100001 = phi i32 [%_110006, %_110040.0], [%_80004, %_110025.0]
  ret i32 %_100001
_110026.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_110018.0:
  %_110048 = phi i8* [%_2, %_110016.0]
  %_110049 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM25scala.collection.IterableG4type" to i8*), %_110016.0]
  %_110096 = bitcast i8* %_110048 to i8**
  %_110050 = load i8*, i8** %_110096
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_110050, i8* %_110049)
  unreachable
}

define i32 @"_SM30scala.collection.IterableOnce$D18elemsToCopyToArrayiiiiiEO"(i8* %_1, i32 %_2, i32 %_3, i32 %_4, i32 %_5) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  %_60002 = call i32 @"_SM19scala.math.package$D3miniiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19scala.math.package$G8instance" to i8*), i32 %_5, i32 %_2)
  %_60004 = sub i32 %_3, %_4
  %_60005 = call i32 @"_SM19scala.math.package$D3miniiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19scala.math.package$G8instance" to i8*), i32 %_60002, i32 %_60004)
  %_60006 = call i32 @"_SM19scala.math.package$D3maxiiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19scala.math.package$G8instance" to i8*), i32 %_60005, i32 0)
  ret i32 %_60006
}

define i32 @"_SM30scala.collection.IterableOnce$D26copyElemsToArray$default$3iEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i32 0
}

define i32 @"_SM30scala.collection.IterableOnce$D26copyElemsToArray$default$4iEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i32 2147483647
}

define nonnull dereferenceable(8) i8* @"_SM30scala.collection.IterableOnce$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(40) i8* @"_SM31java.nio.charset.CharsetDecoderD11loopFlush$1L19java.nio.CharBufferL19java.nio.CharBufferEPT31java.nio.charset.CharsetDecoder"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_40001 = phi i8* [%_2, %_30000.0], [%_100001, %_120000.0]
  br label %_50000.0
_50000.0:
  br label %_60000.0
_60000.0:
  %_60001 = call dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetDecoderD5flushL19java.nio.CharBufferL28java.nio.charset.CoderResultEO"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(40) %_40001)
  %_60002 = call i1 @"_SM28java.nio.charset.CoderResultD11isUnderflowzEO"(i8* dereferenceable_or_null(16) %_60001)
  br i1 %_60002, label %_70000.0, label %_80000.0
_70000.0:
  br label %_90000.0
_80000.0:
  %_80001 = call i1 @"_SM28java.nio.charset.CoderResultD10isOverflowzEO"(i8* dereferenceable_or_null(16) %_60001)
  br i1 %_80001, label %_100000.0, label %_110000.0
_100000.0:
  %_100001 = call dereferenceable_or_null(40) i8* @"_SM31java.nio.charset.CharsetDecoderD6grow$1L19java.nio.CharBufferL19java.nio.CharBufferEpT31java.nio.charset.CharsetDecoder"(i8* dereferenceable_or_null(40) %_40001)
  br label %_120000.0
_90000.0:
  ret i8* %_40001
_120000.0:
  br label %_40000.0
_110000.0:
  %_280009 = bitcast i8* bitcast (i8* (i8*)* @"_SM28java.nio.charset.CoderResultD14throwExceptionuEO" to i8*) to i8* (i8*)*
  call nonnull dereferenceable(8) i8* %_280009(i8* dereferenceable_or_null(16) %_60001)
  %_280010 = bitcast i8* bitcast (i8* (i8*)* @"_SM16java.lang.StringD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_210001 = call dereferenceable_or_null(32) i8* %_280010(i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-207" to i8*))
  br label %_270000.0
_270000.0:
  %_270001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM24java.lang.AssertionErrorG4type" to i8*), i64 40)
  %_280011 = bitcast i8* %_270001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_280012 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_280011, i32 0, i32 5
  %_280003 = bitcast i1* %_280012 to i8*
  %_280013 = bitcast i8* %_280003 to i1*
  store i1 true, i1* %_280013
  %_280014 = bitcast i8* %_270001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_280015 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_280014, i32 0, i32 4
  %_280005 = bitcast i1* %_280015 to i8*
  %_280016 = bitcast i8* %_280005 to i1*
  store i1 true, i1* %_280016
  %_280017 = bitcast i8* %_270001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_280018 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_280017, i32 0, i32 3
  %_280007 = bitcast i8** %_280018 to i8*
  %_280019 = bitcast i8* %_280007 to i8**
  store i8* %_210001, i8** %_280019
  %_270005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_270001)
  br label %_280000.0
_280000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_270001)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM31java.nio.charset.CharsetDecoderD11replacementL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_20008 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_20007, i32 0, i32 5
  %_20005 = bitcast i8** %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i8**
  %_20001 = load i8*, i8** %_20009, !dereferenceable_or_null !{i64 32}
  ret i8* %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(40) i8* @"_SM31java.nio.charset.CharsetDecoderD12loopDecode$1L19java.nio.ByteBufferL19java.nio.CharBufferL19java.nio.CharBufferEPT31java.nio.charset.CharsetDecoder"(i8* %_1, i8* %_2, i8* %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  br label %_50000.0
_50000.0:
  %_50001 = phi i8* [%_3, %_40000.0], [%_140001, %_160000.0]
  br label %_60000.0
_60000.0:
  br label %_70000.0
_70000.0:
  %_70001 = call dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetDecoderD6decodeL19java.nio.ByteBufferL19java.nio.CharBufferzL28java.nio.charset.CoderResultEO"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(48) %_2, i8* dereferenceable_or_null(40) %_50001, i1 true)
  %_70002 = call i1 @"_SM28java.nio.charset.CoderResultD11isUnderflowzEO"(i8* dereferenceable_or_null(16) %_70001)
  br i1 %_70002, label %_80000.0, label %_90000.0
_80000.0:
  %_80001 = call i1 @"_SM15java.nio.BufferD12hasRemainingzEO"(i8* dereferenceable_or_null(48) %_2)
  %_80004 = xor i1 %_80001, true
  %_80005 = xor i1 %_80004, true
  br i1 %_80005, label %_100000.0, label %_110000.0
_110000.0:
  br label %_120000.0
_120000.0:
  br label %_130000.0
_90000.0:
  %_90001 = call i1 @"_SM28java.nio.charset.CoderResultD10isOverflowzEO"(i8* dereferenceable_or_null(16) %_70001)
  br i1 %_90001, label %_140000.0, label %_150000.0
_140000.0:
  %_140001 = call dereferenceable_or_null(40) i8* @"_SM31java.nio.charset.CharsetDecoderD6grow$1L19java.nio.CharBufferL19java.nio.CharBufferEpT31java.nio.charset.CharsetDecoder"(i8* dereferenceable_or_null(40) %_50001)
  br label %_160000.0
_130000.0:
  ret i8* %_50001
_160000.0:
  br label %_50000.0
_100000.0:
  call i8* @"_SM28scala.runtime.Scala3RunTime$D12assertFailednEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM28scala.runtime.Scala3RunTime$G8instance" to i8*))
  br label %_320002.0
_150000.0:
  %_320013 = bitcast i8* bitcast (i8* (i8*)* @"_SM28java.nio.charset.CoderResultD14throwExceptionuEO" to i8*) to i8* (i8*)*
  call nonnull dereferenceable(8) i8* %_320013(i8* dereferenceable_or_null(16) %_70001)
  %_320014 = bitcast i8* bitcast (i8* (i8*)* @"_SM16java.lang.StringD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_250001 = call dereferenceable_or_null(32) i8* %_320014(i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-207" to i8*))
  br label %_310000.0
_310000.0:
  %_310001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM24java.lang.AssertionErrorG4type" to i8*), i64 40)
  %_320015 = bitcast i8* %_310001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_320016 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_320015, i32 0, i32 5
  %_320006 = bitcast i1* %_320016 to i8*
  %_320017 = bitcast i8* %_320006 to i1*
  store i1 true, i1* %_320017
  %_320018 = bitcast i8* %_310001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_320019 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_320018, i32 0, i32 4
  %_320008 = bitcast i1* %_320019 to i8*
  %_320020 = bitcast i8* %_320008 to i1*
  store i1 true, i1* %_320020
  %_320021 = bitcast i8* %_310001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_320022 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_320021, i32 0, i32 3
  %_320010 = bitcast i8** %_320022 to i8*
  %_320023 = bitcast i8* %_320010 to i8**
  store i8* %_250001, i8** %_320023
  %_310005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_310001)
  br label %_320000.0
_320000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_310001)
  unreachable
_320002.0:
  %_320024 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_320024(i8* null)
  unreachable
}

define dereferenceable_or_null(56) i8* @"_SM31java.nio.charset.CharsetDecoderD16onMalformedInputL34java.nio.charset.CodingErrorActionL31java.nio.charset.CharsetDecoderEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_2, null
  br i1 %_30002, label %_40000.0, label %_50000.0
_50000.0:
  br label %_160000.0
_160000.0:
  %_160006 = icmp ne i8* %_1, null
  br i1 %_160006, label %_160004.0, label %_160005.0
_160004.0:
  %_160017 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_160018 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_160017, i32 0, i32 6
  %_160007 = bitcast i8** %_160018 to i8*
  %_160019 = bitcast i8* %_160007 to i8**
  store i8* %_2, i8** %_160019
  call nonnull dereferenceable(8) i8* @"_SM31java.nio.charset.CharsetDecoderD20implOnMalformedInputL34java.nio.charset.CodingErrorActionuEO"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(16) %_2)
  ret i8* %_1
_40000.0:
  br label %_140000.0
_140000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM34java.lang.IllegalArgumentExceptionG4type" to i8*), i64 40)
  %_160020 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160021 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160020, i32 0, i32 5
  %_160010 = bitcast i1* %_160021 to i8*
  %_160022 = bitcast i8* %_160010 to i1*
  store i1 true, i1* %_160022
  %_160023 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160024 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160023, i32 0, i32 4
  %_160012 = bitcast i1* %_160024 to i8*
  %_160025 = bitcast i8* %_160012 to i1*
  store i1 true, i1* %_160025
  %_160026 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160027 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160026, i32 0, i32 3
  %_160014 = bitcast i8** %_160027 to i8*
  %_160028 = bitcast i8* %_160014 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-209" to i8*), i8** %_160028
  %_140005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_140001)
  br label %_150000.0
_150000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_140001)
  unreachable
_160005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define float @"_SM31java.nio.charset.CharsetDecoderD19averageCharsPerBytefEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_20008 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_20007, i32 0, i32 2
  %_20005 = bitcast float* %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to float*
  %_20001 = load float, float* %_20009
  ret float %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM31java.nio.charset.CharsetDecoderD20implOnMalformedInputL34java.nio.charset.CodingErrorActionuEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetDecoderD20malformedInputActionL34java.nio.charset.CodingErrorActionEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_20008 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_20007, i32 0, i32 6
  %_20005 = bitcast i8** %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i8**
  %_20001 = load i8*, i8** %_20009, !dereferenceable_or_null !{i64 16}
  ret i8* %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(56) i8* @"_SM31java.nio.charset.CharsetDecoderD21onUnmappableCharacterL34java.nio.charset.CodingErrorActionL31java.nio.charset.CharsetDecoderEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_2, null
  br i1 %_30002, label %_40000.0, label %_50000.0
_50000.0:
  br label %_160000.0
_160000.0:
  %_160006 = icmp ne i8* %_1, null
  br i1 %_160006, label %_160004.0, label %_160005.0
_160004.0:
  %_160017 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_160018 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_160017, i32 0, i32 7
  %_160007 = bitcast i8** %_160018 to i8*
  %_160019 = bitcast i8* %_160007 to i8**
  store i8* %_2, i8** %_160019
  call nonnull dereferenceable(8) i8* @"_SM31java.nio.charset.CharsetDecoderD25implOnUnmappableCharacterL34java.nio.charset.CodingErrorActionuEO"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(16) %_2)
  ret i8* %_1
_40000.0:
  br label %_140000.0
_140000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM34java.lang.IllegalArgumentExceptionG4type" to i8*), i64 40)
  %_160020 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160021 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160020, i32 0, i32 5
  %_160010 = bitcast i1* %_160021 to i8*
  %_160022 = bitcast i8* %_160010 to i1*
  store i1 true, i1* %_160022
  %_160023 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160024 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160023, i32 0, i32 4
  %_160012 = bitcast i1* %_160024 to i8*
  %_160025 = bitcast i8* %_160012 to i1*
  store i1 true, i1* %_160025
  %_160026 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160027 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160026, i32 0, i32 3
  %_160014 = bitcast i8** %_160027 to i8*
  %_160028 = bitcast i8* %_160014 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-209" to i8*), i8** %_160028
  %_140005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_140001)
  br label %_150000.0
_150000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_140001)
  unreachable
_160005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM31java.nio.charset.CharsetDecoderD25implOnUnmappableCharacterL34java.nio.charset.CodingErrorActionuEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetDecoderD25unmappableCharacterActionL34java.nio.charset.CodingErrorActionEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_20008 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_20007, i32 0, i32 7
  %_20005 = bitcast i8** %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i8**
  %_20001 = load i8*, i8** %_20009, !dereferenceable_or_null !{i64 16}
  ret i8* %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetDecoderD5flushL19java.nio.CharBufferL28java.nio.charset.CoderResultEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_250003 = icmp ne i8* %_1, null
  br i1 %_250003, label %_250001.0, label %_250002.0
_250001.0:
  %_250016 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_250017 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_250016, i32 0, i32 1
  %_250004 = bitcast i32* %_250017 to i8*
  %_250018 = bitcast i8* %_250004 to i32*
  %_40001 = load i32, i32* %_250018
  %_40003 = icmp eq i32 %_40001, 3
  br i1 %_40003, label %_50000.0, label %_60000.0
_50000.0:
  %_50001 = call dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetDecoderD9implFlushL19java.nio.CharBufferL28java.nio.charset.CoderResultEO"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(40) %_2)
  %_50002 = call i1 @"_SM28java.nio.charset.CoderResultD11isUnderflowzEO"(i8* dereferenceable_or_null(16) %_50001)
  br i1 %_50002, label %_70000.0, label %_80000.0
_70000.0:
  %_250007 = icmp ne i8* %_1, null
  br i1 %_250007, label %_250006.0, label %_250002.0
_250006.0:
  %_250019 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_250020 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_250019, i32 0, i32 1
  %_250008 = bitcast i32* %_250020 to i8*
  %_250021 = bitcast i8* %_250008 to i32*
  store i32 4, i32* %_250021
  br label %_90000.0
_80000.0:
  br label %_90000.0
_90000.0:
  br label %_100000.0
_60000.0:
  br label %_110000.0
_110000.0:
  %_110002 = icmp eq i32 %_40001, 4
  br i1 %_110002, label %_120000.0, label %_130000.0
_120000.0:
  %_120001 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_250022 = bitcast i8* %_120001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_250023 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_250022, i32 0, i32 12
  %_250009 = bitcast i8** %_250023 to i8*
  %_250024 = bitcast i8* %_250009 to i8**
  %_140001 = load i8*, i8** %_250024, !dereferenceable_or_null !{i64 16}
  br label %_100000.0
_130000.0:
  br label %_150000.0
_100000.0:
  %_100001 = phi i8* [%_140001, %_120000.0], [%_50001, %_90000.0]
  ret i8* %_100001
_150000.0:
  br label %_240000.0
_240000.0:
  %_240001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM31java.lang.IllegalStateExceptionG4type" to i8*), i64 40)
  %_250025 = bitcast i8* %_240001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_250026 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_250025, i32 0, i32 5
  %_250011 = bitcast i1* %_250026 to i8*
  %_250027 = bitcast i8* %_250011 to i1*
  store i1 true, i1* %_250027
  %_250028 = bitcast i8* %_240001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_250029 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_250028, i32 0, i32 4
  %_250013 = bitcast i1* %_250029 to i8*
  %_250030 = bitcast i8* %_250013 to i1*
  store i1 true, i1* %_250030
  %_240004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_240001)
  br label %_250000.0
_250000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_240001)
  unreachable
_250002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(56) i8* @"_SM31java.nio.charset.CharsetDecoderD5resetL31java.nio.charset.CharsetDecoderEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20006 = icmp ne i8* %_1, null
  br i1 %_20006, label %_20004.0, label %_20005.0
_20004.0:
  %_20010 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_20011 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_20010, i32 0, i32 1
  %_20007 = bitcast i32* %_20011 to i8*
  %_20012 = bitcast i8* %_20007 to i32*
  store i32 1, i32* %_20012
  call nonnull dereferenceable(8) i8* @"_SM31java.nio.charset.CharsetDecoderD9implResetuEO"(i8* dereferenceable_or_null(56) %_1)
  ret i8* %_1
_20005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(40) i8* @"_SM31java.nio.charset.CharsetDecoderD6decodeL19java.nio.ByteBufferL19java.nio.CharBufferEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(56) i8* @"_SM31java.nio.charset.CharsetDecoderD5resetL31java.nio.charset.CharsetDecoderEO"(i8* dereferenceable_or_null(56) %_1)
  %_30002 = call i32 @"_SM15java.nio.BufferD9remainingiEO"(i8* dereferenceable_or_null(48) %_2)
  %_30004 = call float @"_SM31java.nio.charset.CharsetDecoderD19averageCharsPerBytefEO"(i8* dereferenceable_or_null(56) %_1)
  %_30009 = sitofp i32 %_30002 to double
  %_30010 = fpext float %_30004 to double
  %_30011 = fmul double %_30009, %_30010
  %_30024 = fcmp une double %_30011, %_30011
  br i1 %_30024, label %_30017.0, label %_30018.0
_30017.0:
  br label %_30023.0
_30018.0:
  %_30025 = fcmp ole double %_30011, 0xc1e0000000000000
  br i1 %_30025, label %_30019.0, label %_30020.0
_30019.0:
  br label %_30023.0
_30020.0:
  %_30026 = fcmp oge double %_30011, 0x41dfffffffc00000
  br i1 %_30026, label %_30021.0, label %_30022.0
_30021.0:
  br label %_30023.0
_30022.0:
  %_30027 = fptosi double %_30011 to i32
  br label %_30023.0
_30023.0:
  %_30012 = phi i32 [%_30027, %_30022.0], [2147483647, %_30021.0], [-2147483648, %_30019.0], [zeroinitializer, %_30017.0]
  %_30013 = call dereferenceable_or_null(40) i8* @"_SM20java.nio.CharBuffer$D8allocateiL19java.nio.CharBufferEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM20java.nio.CharBuffer$G8instance" to i8*), i32 %_30012)
  %_30014 = call dereferenceable_or_null(40) i8* @"_SM31java.nio.charset.CharsetDecoderD12loopDecode$1L19java.nio.ByteBufferL19java.nio.CharBufferL19java.nio.CharBufferEPT31java.nio.charset.CharsetDecoder"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(48) %_2, i8* dereferenceable_or_null(40) %_30013)
  %_30015 = call dereferenceable_or_null(40) i8* @"_SM31java.nio.charset.CharsetDecoderD11loopFlush$1L19java.nio.CharBufferL19java.nio.CharBufferEPT31java.nio.charset.CharsetDecoder"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(40) %_30014)
  %_30016 = call dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD4flipL19java.nio.CharBufferEO"(i8* dereferenceable_or_null(40) %_30015)
  ret i8* %_30015
}

define dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetDecoderD6decodeL19java.nio.ByteBufferL19java.nio.CharBufferzL28java.nio.charset.CoderResultEO"(i8* %_1, i8* %_2, i8* %_3, i1 %_4) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_270006 = icmp ne i8* %_1, null
  br i1 %_270006, label %_270004.0, label %_270005.0
_270004.0:
  %_270021 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_270022 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_270021, i32 0, i32 1
  %_270007 = bitcast i32* %_270022 to i8*
  %_270023 = bitcast i8* %_270007 to i32*
  %_50001 = load i32, i32* %_270023
  %_50003 = icmp eq i32 %_50001, 4
  br i1 %_50003, label %_60000.0, label %_70000.0
_60000.0:
  br label %_80000.0
_70000.0:
  %_70002 = xor i1 %_4, true
  br i1 %_70002, label %_90000.0, label %_100000.0
_90000.0:
  %_270009 = icmp ne i8* %_1, null
  br i1 %_270009, label %_270008.0, label %_270005.0
_270008.0:
  %_270024 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_270025 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_270024, i32 0, i32 1
  %_270010 = bitcast i32* %_270025 to i8*
  %_270026 = bitcast i8* %_270010 to i32*
  %_90001 = load i32, i32* %_270026
  %_90003 = icmp eq i32 %_90001, 3
  br label %_110000.0
_100000.0:
  br label %_110000.0
_110000.0:
  %_110001 = phi i1 [false, %_100000.0], [%_90003, %_270008.0]
  br label %_80000.0
_80000.0:
  %_80001 = phi i1 [%_110001, %_110000.0], [true, %_60000.0]
  br i1 %_80001, label %_120000.0, label %_130000.0
_130000.0:
  br label %_240000.0
_240000.0:
  br i1 %_4, label %_250000.0, label %_260000.0
_250000.0:
  br label %_270000.0
_260000.0:
  br label %_270000.0
_270000.0:
  %_270001 = phi i32 [2, %_260000.0], [3, %_250000.0]
  %_270013 = icmp ne i8* %_1, null
  br i1 %_270013, label %_270012.0, label %_270005.0
_270012.0:
  %_270027 = bitcast i8* %_1 to { i8*, i32, float, i8*, float, i8*, i8*, i8* }*
  %_270028 = getelementptr { i8*, i32, float, i8*, float, i8*, i8*, i8* }, { i8*, i32, float, i8*, float, i8*, i8*, i8* }* %_270027, i32 0, i32 1
  %_270014 = bitcast i32* %_270028 to i8*
  %_270029 = bitcast i8* %_270014 to i32*
  store i32 %_270001, i32* %_270029
  %_270003 = call dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetDecoderD6loop$1L19java.nio.ByteBufferL19java.nio.CharBufferzL28java.nio.charset.CoderResultEPT31java.nio.charset.CharsetDecoder"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(48) %_2, i8* dereferenceable_or_null(40) %_3, i1 %_4)
  ret i8* %_270003
_120000.0:
  br label %_220000.0
_220000.0:
  %_220001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM31java.lang.IllegalStateExceptionG4type" to i8*), i64 40)
  %_270030 = bitcast i8* %_220001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_270031 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_270030, i32 0, i32 5
  %_270016 = bitcast i1* %_270031 to i8*
  %_270032 = bitcast i8* %_270016 to i1*
  store i1 true, i1* %_270032
  %_270033 = bitcast i8* %_220001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_270034 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_270033, i32 0, i32 4
  %_270018 = bitcast i1* %_270034 to i8*
  %_270035 = bitcast i8* %_270018 to i1*
  store i1 true, i1* %_270035
  %_220004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_220001)
  br label %_230000.0
_230000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_220001)
  unreachable
_270005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(40) i8* @"_SM31java.nio.charset.CharsetDecoderD6grow$1L19java.nio.CharBufferL19java.nio.CharBufferEpT31java.nio.charset.CharsetDecoder"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM15java.nio.BufferD8capacityiEO"(i8* dereferenceable_or_null(40) %_1)
  %_20003 = icmp eq i32 %_20001, 0
  br i1 %_20003, label %_30000.0, label %_40000.0
_30000.0:
  %_30002 = call dereferenceable_or_null(40) i8* @"_SM20java.nio.CharBuffer$D8allocateiL19java.nio.CharBufferEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM20java.nio.CharBuffer$G8instance" to i8*), i32 1)
  br label %_50000.0
_40000.0:
  %_40002 = call i32 @"_SM15java.nio.BufferD8capacityiEO"(i8* dereferenceable_or_null(40) %_1)
  %_50002 = and i32 1, 31
  %_40004 = shl i32 %_40002, %_50002
  %_40005 = call dereferenceable_or_null(40) i8* @"_SM20java.nio.CharBuffer$D8allocateiL19java.nio.CharBufferEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM20java.nio.CharBuffer$G8instance" to i8*), i32 %_40004)
  %_40006 = call dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD4flipL19java.nio.CharBufferEO"(i8* dereferenceable_or_null(40) %_1)
  %_40007 = call dereferenceable_or_null(40) i8* @"_SM19java.nio.CharBufferD3putL19java.nio.CharBufferL19java.nio.CharBufferEO"(i8* dereferenceable_or_null(40) %_40005, i8* dereferenceable_or_null(40) %_1)
  br label %_50000.0
_50000.0:
  %_50001 = phi i8* [%_40005, %_40000.0], [%_30002, %_30000.0]
  ret i8* %_50001
}

define dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetDecoderD6loop$1L19java.nio.ByteBufferL19java.nio.CharBufferzL28java.nio.charset.CoderResultEPT31java.nio.charset.CharsetDecoder"(i8* %_4, i8* %_1, i8* %_2, i1 %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_5.0:
  br label %_6.0
_6.0:
  br i1 true, label %_7.0, label %_8.0
_7.0:
  br label %_9.0
_9.0:
  br label %_14.0
_14.0:
  %_171 = icmp ne i8* %_4, null
  br i1 %_171, label %_168.0, label %_169.0
_168.0:
  %_257 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM24niocharset.UTF_8$DecoderD10decodeLoopL19java.nio.ByteBufferL19java.nio.CharBufferL28java.nio.charset.CoderResultEO" to i8*) to i8* (i8*, i8*, i8*)*
  %_21 = invoke dereferenceable_or_null(16) i8* %_257(i8* dereferenceable_or_null(56) %_4, i8* dereferenceable_or_null(48) %_1, i8* dereferenceable_or_null(40) %_2) to label %_168.1 unwind label %_174.landingpad
_168.1:
  br label %_15.0
_12.0:
  %_16 = phi i8* [%_20, %_173.0], [%_18, %_167.0]
  %_178 = icmp eq i8* %_16, null
  br i1 %_178, label %_175.0, label %_176.0
_175.0:
  br label %_177.0
_176.0:
  %_258 = bitcast i8* %_16 to i8**
  %_179 = load i8*, i8** %_258
  %_180 = icmp eq i8* %_179, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.BufferOverflowExceptionG4type" to i8*)
  br label %_177.0
_177.0:
  %_22 = phi i1 [%_180, %_176.0], [false, %_175.0]
  br i1 %_22, label %_23.0, label %_24.0
_23.0:
  %_184 = icmp eq i8* %_16, null
  br i1 %_184, label %_182.0, label %_181.0
_181.0:
  %_259 = bitcast i8* %_16 to i8**
  %_185 = load i8*, i8** %_259
  %_186 = icmp eq i8* %_185, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.BufferOverflowExceptionG4type" to i8*)
  br i1 %_186, label %_182.0, label %_183.0
_182.0:
  %_27 = bitcast i8* %_16 to i8*
  %_28 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM38java.nio.charset.CoderMalfunctionErrorG4type" to i8*), i64 40)
  call nonnull dereferenceable(8) i8* @"_SM38java.nio.charset.CoderMalfunctionErrorRL19java.lang.ExceptionE"(i8* nonnull dereferenceable(40) %_28, i8* dereferenceable_or_null(40) %_27)
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_28)
  unreachable
_24.0:
  %_192 = icmp eq i8* %_16, null
  br i1 %_192, label %_189.0, label %_190.0
_189.0:
  br label %_191.0
_190.0:
  %_260 = bitcast i8* %_16 to i8**
  %_193 = load i8*, i8** %_260
  %_194 = icmp eq i8* %_193, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM33java.nio.BufferUnderflowExceptionG4type" to i8*)
  br label %_191.0
_191.0:
  %_32 = phi i1 [%_194, %_190.0], [false, %_189.0]
  br i1 %_32, label %_33.0, label %_34.0
_33.0:
  %_197 = icmp eq i8* %_16, null
  br i1 %_197, label %_196.0, label %_195.0
_195.0:
  %_261 = bitcast i8* %_16 to i8**
  %_198 = load i8*, i8** %_261
  %_199 = icmp eq i8* %_198, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM33java.nio.BufferUnderflowExceptionG4type" to i8*)
  br i1 %_199, label %_196.0, label %_183.0
_196.0:
  %_37 = bitcast i8* %_16 to i8*
  %_38 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM38java.nio.charset.CoderMalfunctionErrorG4type" to i8*), i64 40)
  call nonnull dereferenceable(8) i8* @"_SM38java.nio.charset.CoderMalfunctionErrorRL19java.lang.ExceptionE"(i8* nonnull dereferenceable(40) %_38, i8* dereferenceable_or_null(40) %_37)
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_38)
  unreachable
_34.0:
  %_204 = icmp ne i8* %_16, null
  br i1 %_204, label %_202.0, label %_203.0
_202.0:
  call i8* @"scalanative_throw"(i8* dereferenceable_or_null(8) %_16)
  unreachable
_15.0:
  %_17 = phi i8* [%_21, %_168.1]
  %_207 = icmp ne i8* %_17, null
  br i1 %_207, label %_206.0, label %_203.0
_206.0:
  %_262 = bitcast i8* bitcast (i1 (i8*)* @"_SM28java.nio.charset.CoderResultD11isUnderflowzEO" to i8*) to i1 (i8*)*
  %_48 = call i1 %_262(i8* dereferenceable_or_null(16) %_17)
  br i1 %_48, label %_43.0, label %_44.0
_43.0:
  %_209 = icmp ne i8* %_1, null
  br i1 %_209, label %_208.0, label %_203.0
_208.0:
  %_263 = bitcast i8* bitcast (i32 (i8*)* @"_SM15java.nio.BufferD9remainingiEO" to i8*) to i32 (i8*)*
  %_50 = call i32 %_263(i8* dereferenceable_or_null(48) %_1)
  br i1 %_3, label %_55.0, label %_56.0
_55.0:
  %_59 = icmp sgt i32 %_50, 0
  br label %_57.0
_56.0:
  br label %_57.0
_57.0:
  %_58 = phi i1 [false, %_56.0], [%_59, %_55.0]
  br i1 %_58, label %_51.0, label %_52.0
_51.0:
  %_60 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_61 = call dereferenceable_or_null(16) i8* @"_SM29java.nio.charset.CoderResult$D18malformedForLengthiL28java.nio.charset.CoderResultEO"(i8* nonnull dereferenceable(104) %_60, i32 %_50)
  br label %_53.0
_52.0:
  br label %_53.0
_53.0:
  %_54 = phi i8* [%_17, %_52.0], [%_61, %_51.0]
  br label %_45.0
_44.0:
  br label %_45.0
_45.0:
  %_46 = phi i8* [%_17, %_44.0], [%_54, %_53.0]
  %_211 = icmp ne i8* %_46, null
  br i1 %_211, label %_210.0, label %_203.0
_210.0:
  %_264 = bitcast i8* bitcast (i1 (i8*)* @"_SM28java.nio.charset.CoderResultD11isUnderflowzEO" to i8*) to i1 (i8*)*
  %_71 = call i1 %_264(i8* dereferenceable_or_null(16) %_46)
  br i1 %_71, label %_66.0, label %_67.0
_66.0:
  br label %_68.0
_67.0:
  %_213 = icmp ne i8* %_46, null
  br i1 %_213, label %_212.0, label %_203.0
_212.0:
  %_265 = bitcast i8* bitcast (i1 (i8*)* @"_SM28java.nio.charset.CoderResultD10isOverflowzEO" to i8*) to i1 (i8*)*
  %_73 = call i1 %_265(i8* dereferenceable_or_null(16) %_46)
  br label %_68.0
_68.0:
  %_69 = phi i1 [%_73, %_212.0], [true, %_66.0]
  br i1 %_69, label %_62.0, label %_63.0
_62.0:
  br label %_64.0
_63.0:
  %_215 = icmp ne i8* %_46, null
  br i1 %_215, label %_214.0, label %_203.0
_214.0:
  %_266 = bitcast i8* bitcast (i1 (i8*)* @"_SM28java.nio.charset.CoderResultD12isUnmappablezEO" to i8*) to i1 (i8*)*
  %_79 = call i1 %_266(i8* dereferenceable_or_null(16) %_46)
  br i1 %_79, label %_74.0, label %_75.0
_74.0:
  %_217 = icmp ne i8* %_4, null
  br i1 %_217, label %_216.0, label %_203.0
_216.0:
  %_267 = bitcast i8* bitcast (i8* (i8*)* @"_SM31java.nio.charset.CharsetDecoderD25unmappableCharacterActionL34java.nio.charset.CodingErrorActionEO" to i8*) to i8* (i8*)*
  %_81 = call dereferenceable_or_null(16) i8* %_267(i8* dereferenceable_or_null(56) %_4)
  br label %_76.0
_75.0:
  %_219 = icmp ne i8* %_4, null
  br i1 %_219, label %_218.0, label %_203.0
_218.0:
  %_268 = bitcast i8* bitcast (i8* (i8*)* @"_SM31java.nio.charset.CharsetDecoderD20malformedInputActionL34java.nio.charset.CodingErrorActionEO" to i8*) to i8* (i8*)*
  %_83 = call dereferenceable_or_null(16) i8* %_268(i8* dereferenceable_or_null(56) %_4)
  br label %_76.0
_76.0:
  %_77 = phi i8* [%_83, %_218.0], [%_81, %_216.0]
  br label %_84.0
_84.0:
  %_91 = call dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"()
  %_92 = call dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D7REPLACEL34java.nio.charset.CodingErrorActionEO"(i8* nonnull dereferenceable(32) %_91)
  %_97 = icmp eq i8* %_92, null
  br i1 %_97, label %_93.0, label %_94.0
_93.0:
  %_98 = icmp eq i8* %_77, null
  br label %_95.0
_94.0:
  %_221 = icmp ne i8* %_92, null
  br i1 %_221, label %_220.0, label %_203.0
_220.0:
  %_269 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_100 = call i1 %_269(i8* dereferenceable_or_null(16) %_92, i8* dereferenceable_or_null(16) %_77)
  br label %_95.0
_95.0:
  %_96 = phi i1 [%_100, %_220.0], [%_98, %_93.0]
  br i1 %_96, label %_87.0, label %_88.0
_87.0:
  %_223 = icmp ne i8* %_2, null
  br i1 %_223, label %_222.0, label %_203.0
_222.0:
  %_270 = bitcast i8* bitcast (i32 (i8*)* @"_SM15java.nio.BufferD9remainingiEO" to i8*) to i32 (i8*)*
  %_106 = call i32 %_270(i8* dereferenceable_or_null(40) %_2)
  %_225 = icmp ne i8* %_4, null
  br i1 %_225, label %_224.0, label %_203.0
_224.0:
  %_271 = bitcast i8* bitcast (i8* (i8*)* @"_SM31java.nio.charset.CharsetDecoderD11replacementL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_108 = call dereferenceable_or_null(32) i8* %_271(i8* dereferenceable_or_null(56) %_4)
  %_227 = icmp ne i8* %_108, null
  br i1 %_227, label %_226.0, label %_203.0
_226.0:
  %_272 = bitcast i8* bitcast (i32 (i8*)* @"_SM16java.lang.StringD6lengthiEO" to i8*) to i32 (i8*)*
  %_110 = call i32 %_272(i8* dereferenceable_or_null(32) %_108)
  %_111 = icmp slt i32 %_106, %_110
  br i1 %_111, label %_101.0, label %_102.0
_101.0:
  %_112 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_113 = call dereferenceable_or_null(16) i8* @"_SM29java.nio.charset.CoderResult$D8OVERFLOWL28java.nio.charset.CoderResultEO"(i8* nonnull dereferenceable(104) %_112)
  br label %_103.0
_102.0:
  %_229 = icmp ne i8* %_4, null
  br i1 %_229, label %_228.0, label %_203.0
_228.0:
  %_273 = bitcast i8* bitcast (i8* (i8*)* @"_SM31java.nio.charset.CharsetDecoderD11replacementL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_115 = call dereferenceable_or_null(32) i8* %_273(i8* dereferenceable_or_null(56) %_4)
  %_231 = icmp ne i8* %_2, null
  br i1 %_231, label %_230.0, label %_203.0
_230.0:
  %_274 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM19java.nio.CharBufferD3putL16java.lang.StringL19java.nio.CharBufferEO" to i8*) to i8* (i8*, i8*)*
  %_117 = call dereferenceable_or_null(40) i8* %_274(i8* dereferenceable_or_null(40) %_2, i8* dereferenceable_or_null(32) %_115)
  %_233 = icmp ne i8* %_1, null
  br i1 %_233, label %_232.0, label %_203.0
_232.0:
  %_275 = bitcast i8* bitcast (i32 (i8*)* @"_SM15java.nio.BufferD8positioniEO" to i8*) to i32 (i8*)*
  %_119 = call i32 %_275(i8* dereferenceable_or_null(48) %_1)
  %_235 = icmp ne i8* %_46, null
  br i1 %_235, label %_234.0, label %_203.0
_234.0:
  %_276 = bitcast i8* bitcast (i32 (i8*)* @"_SM28java.nio.charset.CoderResultD6lengthiEO" to i8*) to i32 (i8*)*
  %_121 = call i32 %_276(i8* dereferenceable_or_null(16) %_46)
  %_122 = add i32 %_119, %_121
  %_237 = icmp ne i8* %_1, null
  br i1 %_237, label %_236.0, label %_203.0
_236.0:
  %_277 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM19java.nio.ByteBufferD8positioniL19java.nio.ByteBufferEO" to i8*) to i8* (i8*, i32)*
  %_124 = call dereferenceable_or_null(48) i8* %_277(i8* dereferenceable_or_null(48) %_1, i32 %_122)
  br label %_10.0
_103.0:
  %_104 = phi i8* [%_113, %_101.0]
  br label %_85.0
_88.0:
  br label %_89.0
_89.0:
  %_90 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_88.0]
  %_131 = call dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"()
  %_132 = call dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D6REPORTL34java.nio.charset.CodingErrorActionEO"(i8* nonnull dereferenceable(32) %_131)
  %_137 = icmp eq i8* %_132, null
  br i1 %_137, label %_133.0, label %_134.0
_133.0:
  %_138 = icmp eq i8* %_77, null
  br label %_135.0
_134.0:
  %_239 = icmp ne i8* %_132, null
  br i1 %_239, label %_238.0, label %_203.0
_238.0:
  %_278 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_140 = call i1 %_278(i8* dereferenceable_or_null(16) %_132, i8* dereferenceable_or_null(16) %_77)
  br label %_135.0
_135.0:
  %_136 = phi i1 [%_140, %_238.0], [%_138, %_133.0]
  br i1 %_136, label %_127.0, label %_128.0
_127.0:
  br label %_85.0
_128.0:
  br label %_129.0
_129.0:
  %_130 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_128.0]
  %_146 = call dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"()
  %_147 = call dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D6IGNOREL34java.nio.charset.CodingErrorActionEO"(i8* nonnull dereferenceable(32) %_146)
  %_152 = icmp eq i8* %_147, null
  br i1 %_152, label %_148.0, label %_149.0
_148.0:
  %_153 = icmp eq i8* %_77, null
  br label %_150.0
_149.0:
  %_241 = icmp ne i8* %_147, null
  br i1 %_241, label %_240.0, label %_203.0
_240.0:
  %_279 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_155 = call i1 %_279(i8* dereferenceable_or_null(16) %_147, i8* dereferenceable_or_null(16) %_77)
  br label %_150.0
_150.0:
  %_151 = phi i1 [%_155, %_240.0], [%_153, %_148.0]
  br i1 %_151, label %_142.0, label %_143.0
_142.0:
  %_243 = icmp ne i8* %_1, null
  br i1 %_243, label %_242.0, label %_203.0
_242.0:
  %_280 = bitcast i8* bitcast (i32 (i8*)* @"_SM15java.nio.BufferD8positioniEO" to i8*) to i32 (i8*)*
  %_157 = call i32 %_280(i8* dereferenceable_or_null(48) %_1)
  %_245 = icmp ne i8* %_46, null
  br i1 %_245, label %_244.0, label %_203.0
_244.0:
  %_281 = bitcast i8* bitcast (i32 (i8*)* @"_SM28java.nio.charset.CoderResultD6lengthiEO" to i8*) to i32 (i8*)*
  %_159 = call i32 %_281(i8* dereferenceable_or_null(16) %_46)
  %_160 = add i32 %_157, %_159
  %_247 = icmp ne i8* %_1, null
  br i1 %_247, label %_246.0, label %_203.0
_246.0:
  %_282 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM19java.nio.ByteBufferD8positioniL19java.nio.ByteBufferEO" to i8*) to i8* (i8*, i32)*
  %_162 = call dereferenceable_or_null(48) i8* %_282(i8* dereferenceable_or_null(48) %_1, i32 %_160)
  br label %_10.0
_143.0:
  br label %_144.0
_144.0:
  %_145 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_143.0]
  %_165 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM16scala.MatchErrorG4type" to i8*), i64 64)
  call nonnull dereferenceable(8) i8* @"_SM16scala.MatchErrorRL16java.lang.ObjectE"(i8* nonnull dereferenceable(64) %_165, i8* dereferenceable_or_null(16) %_77)
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(64) %_165)
  unreachable
_85.0:
  %_86 = phi i8* [%_46, %_127.0], [%_104, %_103.0]
  br label %_64.0
_64.0:
  %_65 = phi i8* [%_86, %_85.0], [%_46, %_62.0]
  ret i8* %_65
_10.0:
  %_11 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_246.0], [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_236.0]
  br label %_6.0
_8.0:
  ret i8* zeroinitializer
_169.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_169.1 unwind label %_250.landingpad
_169.1:
  unreachable
_203.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_183.0:
  %_253 = phi i8* [%_16, %_195.0], [%_16, %_181.0]
  %_254 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM33java.nio.BufferUnderflowExceptionG4type" to i8*), %_195.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.BufferOverflowExceptionG4type" to i8*), %_181.0]
  %_283 = bitcast i8* %_253 to i8**
  %_255 = load i8*, i8** %_283
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_255, i8* %_254)
  unreachable
_167.0:
  %_18 = phi i8* [%_170, %_170.landingpad.succ], [%_250, %_250.landingpad.succ], [%_172, %_172.landingpad.succ]
  br label %_12.0
_173.0:
  %_20 = phi i8* [%_174, %_174.landingpad.succ]
  br label %_12.0
_170.landingpad:
  %_284 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_285 = extractvalue { i8*, i32 } %_284, 0
  %_286 = extractvalue { i8*, i32 } %_284, 1
  %_287 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_288 = icmp eq i32 %_286, %_287
  br i1 %_288, label %_170.landingpad.succ, label %_170.landingpad.fail
_170.landingpad.succ:
  %_289 = call i8* @__cxa_begin_catch(i8* %_285)
  %_290 = bitcast i8* %_289 to i8**
  %_291 = getelementptr i8*, i8** %_290, i32 1
  %_170 = load i8*, i8** %_291
  call void @__cxa_end_catch()
  br label %_167.0
_170.landingpad.fail:
  resume { i8*, i32 } %_284
_172.landingpad:
  %_292 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_293 = extractvalue { i8*, i32 } %_292, 0
  %_294 = extractvalue { i8*, i32 } %_292, 1
  %_295 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_296 = icmp eq i32 %_294, %_295
  br i1 %_296, label %_172.landingpad.succ, label %_172.landingpad.fail
_172.landingpad.succ:
  %_297 = call i8* @__cxa_begin_catch(i8* %_293)
  %_298 = bitcast i8* %_297 to i8**
  %_299 = getelementptr i8*, i8** %_298, i32 1
  %_172 = load i8*, i8** %_299
  call void @__cxa_end_catch()
  br label %_167.0
_172.landingpad.fail:
  resume { i8*, i32 } %_292
_174.landingpad:
  %_300 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_301 = extractvalue { i8*, i32 } %_300, 0
  %_302 = extractvalue { i8*, i32 } %_300, 1
  %_303 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_304 = icmp eq i32 %_302, %_303
  br i1 %_304, label %_174.landingpad.succ, label %_174.landingpad.fail
_174.landingpad.succ:
  %_305 = call i8* @__cxa_begin_catch(i8* %_301)
  %_306 = bitcast i8* %_305 to i8**
  %_307 = getelementptr i8*, i8** %_306, i32 1
  %_174 = load i8*, i8** %_307
  call void @__cxa_end_catch()
  br label %_173.0
_174.landingpad.fail:
  resume { i8*, i32 } %_300
_250.landingpad:
  %_308 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_309 = extractvalue { i8*, i32 } %_308, 0
  %_310 = extractvalue { i8*, i32 } %_308, 1
  %_311 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_312 = icmp eq i32 %_310, %_311
  br i1 %_312, label %_250.landingpad.succ, label %_250.landingpad.fail
_250.landingpad.succ:
  %_313 = call i8* @__cxa_begin_catch(i8* %_309)
  %_314 = bitcast i8* %_313 to i8**
  %_315 = getelementptr i8*, i8** %_314, i32 1
  %_250 = load i8*, i8** %_315
  call void @__cxa_end_catch()
  br label %_167.0
_250.landingpad.fail:
  resume { i8*, i32 } %_308
}

define dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetDecoderD9implFlushL19java.nio.CharBufferL28java.nio.charset.CoderResultEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_40003 = bitcast i8* %_30001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_40004 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_40003, i32 0, i32 12
  %_40002 = bitcast i8** %_40004 to i8*
  %_40005 = bitcast i8* %_40002 to i8**
  %_40001 = load i8*, i8** %_40005, !dereferenceable_or_null !{i64 16}
  ret i8* %_40001
}

define nonnull dereferenceable(8) i8* @"_SM31java.nio.charset.CharsetDecoderD9implResetuEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM31java.nio.charset.CharsetEncoderD11replacementLAb_EO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8*, float, float, i8* }*
  %_20008 = getelementptr { i8*, i32, i8*, i8*, i8*, float, float, i8* }, { i8*, i32, i8*, i8*, i8*, float, float, i8* }* %_20007, i32 0, i32 7
  %_20005 = bitcast i8** %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i8**
  %_20001 = load i8*, i8** %_20009, !dereferenceable_or_null !{i64 8}
  ret i8* %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(56) i8* @"_SM31java.nio.charset.CharsetEncoderD16onMalformedInputL34java.nio.charset.CodingErrorActionL31java.nio.charset.CharsetEncoderEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_2, null
  br i1 %_30002, label %_40000.0, label %_50000.0
_50000.0:
  br label %_160000.0
_160000.0:
  %_160006 = icmp ne i8* %_1, null
  br i1 %_160006, label %_160004.0, label %_160005.0
_160004.0:
  %_160017 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8*, float, float, i8* }*
  %_160018 = getelementptr { i8*, i32, i8*, i8*, i8*, float, float, i8* }, { i8*, i32, i8*, i8*, i8*, float, float, i8* }* %_160017, i32 0, i32 3
  %_160007 = bitcast i8** %_160018 to i8*
  %_160019 = bitcast i8* %_160007 to i8**
  store i8* %_2, i8** %_160019
  call nonnull dereferenceable(8) i8* @"_SM31java.nio.charset.CharsetEncoderD20implOnMalformedInputL34java.nio.charset.CodingErrorActionuEO"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(16) %_2)
  ret i8* %_1
_40000.0:
  br label %_140000.0
_140000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM34java.lang.IllegalArgumentExceptionG4type" to i8*), i64 40)
  %_160020 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160021 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160020, i32 0, i32 5
  %_160010 = bitcast i1* %_160021 to i8*
  %_160022 = bitcast i8* %_160010 to i1*
  store i1 true, i1* %_160022
  %_160023 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160024 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160023, i32 0, i32 4
  %_160012 = bitcast i1* %_160024 to i8*
  %_160025 = bitcast i8* %_160012 to i1*
  store i1 true, i1* %_160025
  %_160026 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160027 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160026, i32 0, i32 3
  %_160014 = bitcast i8** %_160027 to i8*
  %_160028 = bitcast i8* %_160014 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-209" to i8*), i8** %_160028
  %_140005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_140001)
  br label %_150000.0
_150000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_140001)
  unreachable
_160005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM31java.nio.charset.CharsetEncoderD20implOnMalformedInputL34java.nio.charset.CodingErrorActionuEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetEncoderD20malformedInputActionL34java.nio.charset.CodingErrorActionEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8*, float, float, i8* }*
  %_20008 = getelementptr { i8*, i32, i8*, i8*, i8*, float, float, i8* }, { i8*, i32, i8*, i8*, i8*, float, float, i8* }* %_20007, i32 0, i32 3
  %_20005 = bitcast i8** %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i8**
  %_20001 = load i8*, i8** %_20009, !dereferenceable_or_null !{i64 16}
  ret i8* %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(56) i8* @"_SM31java.nio.charset.CharsetEncoderD21onUnmappableCharacterL34java.nio.charset.CodingErrorActionL31java.nio.charset.CharsetEncoderEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_2, null
  br i1 %_30002, label %_40000.0, label %_50000.0
_50000.0:
  br label %_160000.0
_160000.0:
  %_160006 = icmp ne i8* %_1, null
  br i1 %_160006, label %_160004.0, label %_160005.0
_160004.0:
  %_160017 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8*, float, float, i8* }*
  %_160018 = getelementptr { i8*, i32, i8*, i8*, i8*, float, float, i8* }, { i8*, i32, i8*, i8*, i8*, float, float, i8* }* %_160017, i32 0, i32 2
  %_160007 = bitcast i8** %_160018 to i8*
  %_160019 = bitcast i8* %_160007 to i8**
  store i8* %_2, i8** %_160019
  call nonnull dereferenceable(8) i8* @"_SM31java.nio.charset.CharsetEncoderD25implOnUnmappableCharacterL34java.nio.charset.CodingErrorActionuEO"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(16) %_2)
  ret i8* %_1
_40000.0:
  br label %_140000.0
_140000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM34java.lang.IllegalArgumentExceptionG4type" to i8*), i64 40)
  %_160020 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160021 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160020, i32 0, i32 5
  %_160010 = bitcast i1* %_160021 to i8*
  %_160022 = bitcast i8* %_160010 to i1*
  store i1 true, i1* %_160022
  %_160023 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160024 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160023, i32 0, i32 4
  %_160012 = bitcast i1* %_160024 to i8*
  %_160025 = bitcast i8* %_160012 to i1*
  store i1 true, i1* %_160025
  %_160026 = bitcast i8* %_140001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_160027 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_160026, i32 0, i32 3
  %_160014 = bitcast i8** %_160027 to i8*
  %_160028 = bitcast i8* %_160014 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-209" to i8*), i8** %_160028
  %_140005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_140001)
  br label %_150000.0
_150000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_140001)
  unreachable
_160005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM31java.nio.charset.CharsetEncoderD25implOnUnmappableCharacterL34java.nio.charset.CodingErrorActionuEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetEncoderD25unmappableCharacterActionL34java.nio.charset.CodingErrorActionEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8*, float, float, i8* }*
  %_20008 = getelementptr { i8*, i32, i8*, i8*, i8*, float, float, i8* }, { i8*, i32, i8*, i8*, i8*, float, float, i8* }* %_20007, i32 0, i32 2
  %_20005 = bitcast i8** %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i8**
  %_20001 = load i8*, i8** %_20009, !dereferenceable_or_null !{i64 16}
  ret i8* %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetEncoderD6encodeL19java.nio.CharBufferL19java.nio.ByteBufferzL28java.nio.charset.CoderResultEO"(i8* %_1, i8* %_2, i8* %_3, i1 %_4) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_270006 = icmp ne i8* %_1, null
  br i1 %_270006, label %_270004.0, label %_270005.0
_270004.0:
  %_270021 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8*, float, float, i8* }*
  %_270022 = getelementptr { i8*, i32, i8*, i8*, i8*, float, float, i8* }, { i8*, i32, i8*, i8*, i8*, float, float, i8* }* %_270021, i32 0, i32 1
  %_270007 = bitcast i32* %_270022 to i8*
  %_270023 = bitcast i8* %_270007 to i32*
  %_50001 = load i32, i32* %_270023
  %_50003 = icmp eq i32 %_50001, 3
  br i1 %_50003, label %_60000.0, label %_70000.0
_60000.0:
  br label %_80000.0
_70000.0:
  %_70002 = xor i1 %_4, true
  br i1 %_70002, label %_90000.0, label %_100000.0
_90000.0:
  %_270009 = icmp ne i8* %_1, null
  br i1 %_270009, label %_270008.0, label %_270005.0
_270008.0:
  %_270024 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8*, float, float, i8* }*
  %_270025 = getelementptr { i8*, i32, i8*, i8*, i8*, float, float, i8* }, { i8*, i32, i8*, i8*, i8*, float, float, i8* }* %_270024, i32 0, i32 1
  %_270010 = bitcast i32* %_270025 to i8*
  %_270026 = bitcast i8* %_270010 to i32*
  %_90001 = load i32, i32* %_270026
  %_90003 = icmp eq i32 %_90001, 2
  br label %_110000.0
_100000.0:
  br label %_110000.0
_110000.0:
  %_110001 = phi i1 [false, %_100000.0], [%_90003, %_270008.0]
  br label %_80000.0
_80000.0:
  %_80001 = phi i1 [%_110001, %_110000.0], [true, %_60000.0]
  br i1 %_80001, label %_120000.0, label %_130000.0
_130000.0:
  br label %_240000.0
_240000.0:
  br i1 %_4, label %_250000.0, label %_260000.0
_250000.0:
  br label %_270000.0
_260000.0:
  br label %_270000.0
_270000.0:
  %_270001 = phi i32 [1, %_260000.0], [2, %_250000.0]
  %_270013 = icmp ne i8* %_1, null
  br i1 %_270013, label %_270012.0, label %_270005.0
_270012.0:
  %_270027 = bitcast i8* %_1 to { i8*, i32, i8*, i8*, i8*, float, float, i8* }*
  %_270028 = getelementptr { i8*, i32, i8*, i8*, i8*, float, float, i8* }, { i8*, i32, i8*, i8*, i8*, float, float, i8* }* %_270027, i32 0, i32 1
  %_270014 = bitcast i32* %_270028 to i8*
  %_270029 = bitcast i8* %_270014 to i32*
  store i32 %_270001, i32* %_270029
  %_270003 = call dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetEncoderD6loop$2L19java.nio.CharBufferL19java.nio.ByteBufferzL28java.nio.charset.CoderResultEPT31java.nio.charset.CharsetEncoder"(i8* dereferenceable_or_null(56) %_1, i8* dereferenceable_or_null(40) %_2, i8* dereferenceable_or_null(48) %_3, i1 %_4)
  ret i8* %_270003
_120000.0:
  br label %_220000.0
_220000.0:
  %_220001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM31java.lang.IllegalStateExceptionG4type" to i8*), i64 40)
  %_270030 = bitcast i8* %_220001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_270031 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_270030, i32 0, i32 5
  %_270016 = bitcast i1* %_270031 to i8*
  %_270032 = bitcast i8* %_270016 to i1*
  store i1 true, i1* %_270032
  %_270033 = bitcast i8* %_220001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_270034 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_270033, i32 0, i32 4
  %_270018 = bitcast i1* %_270034 to i8*
  %_270035 = bitcast i8* %_270018 to i1*
  store i1 true, i1* %_270035
  %_220004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_220001)
  br label %_230000.0
_230000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_220001)
  unreachable
_270005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM31java.nio.charset.CharsetEncoderD6loop$2L19java.nio.CharBufferL19java.nio.ByteBufferzL28java.nio.charset.CoderResultEPT31java.nio.charset.CharsetEncoder"(i8* %_4, i8* %_1, i8* %_2, i1 %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_5.0:
  br label %_6.0
_6.0:
  br i1 true, label %_7.0, label %_8.0
_7.0:
  br label %_9.0
_9.0:
  br label %_14.0
_14.0:
  %_170 = icmp ne i8* %_4, null
  br i1 %_170, label %_167.0, label %_168.0
_167.0:
  %_257 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM24niocharset.UTF_8$EncoderD10encodeLoopL19java.nio.CharBufferL19java.nio.ByteBufferL28java.nio.charset.CoderResultEO" to i8*) to i8* (i8*, i8*, i8*)*
  %_21 = invoke dereferenceable_or_null(16) i8* %_257(i8* dereferenceable_or_null(56) %_4, i8* dereferenceable_or_null(40) %_1, i8* dereferenceable_or_null(48) %_2) to label %_167.1 unwind label %_173.landingpad
_167.1:
  br label %_15.0
_12.0:
  %_16 = phi i8* [%_20, %_172.0], [%_18, %_166.0]
  %_177 = icmp eq i8* %_16, null
  br i1 %_177, label %_174.0, label %_175.0
_174.0:
  br label %_176.0
_175.0:
  %_258 = bitcast i8* %_16 to i8**
  %_178 = load i8*, i8** %_258
  %_179 = icmp eq i8* %_178, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.BufferOverflowExceptionG4type" to i8*)
  br label %_176.0
_176.0:
  %_22 = phi i1 [%_179, %_175.0], [false, %_174.0]
  br i1 %_22, label %_23.0, label %_24.0
_23.0:
  %_183 = icmp eq i8* %_16, null
  br i1 %_183, label %_181.0, label %_180.0
_180.0:
  %_259 = bitcast i8* %_16 to i8**
  %_184 = load i8*, i8** %_259
  %_185 = icmp eq i8* %_184, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.BufferOverflowExceptionG4type" to i8*)
  br i1 %_185, label %_181.0, label %_182.0
_181.0:
  %_27 = bitcast i8* %_16 to i8*
  %_28 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM38java.nio.charset.CoderMalfunctionErrorG4type" to i8*), i64 40)
  call nonnull dereferenceable(8) i8* @"_SM38java.nio.charset.CoderMalfunctionErrorRL19java.lang.ExceptionE"(i8* nonnull dereferenceable(40) %_28, i8* dereferenceable_or_null(40) %_27)
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_28)
  unreachable
_24.0:
  %_191 = icmp eq i8* %_16, null
  br i1 %_191, label %_188.0, label %_189.0
_188.0:
  br label %_190.0
_189.0:
  %_260 = bitcast i8* %_16 to i8**
  %_192 = load i8*, i8** %_260
  %_193 = icmp eq i8* %_192, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM33java.nio.BufferUnderflowExceptionG4type" to i8*)
  br label %_190.0
_190.0:
  %_32 = phi i1 [%_193, %_189.0], [false, %_188.0]
  br i1 %_32, label %_33.0, label %_34.0
_33.0:
  %_196 = icmp eq i8* %_16, null
  br i1 %_196, label %_195.0, label %_194.0
_194.0:
  %_261 = bitcast i8* %_16 to i8**
  %_197 = load i8*, i8** %_261
  %_198 = icmp eq i8* %_197, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM33java.nio.BufferUnderflowExceptionG4type" to i8*)
  br i1 %_198, label %_195.0, label %_182.0
_195.0:
  %_37 = bitcast i8* %_16 to i8*
  %_38 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM38java.nio.charset.CoderMalfunctionErrorG4type" to i8*), i64 40)
  call nonnull dereferenceable(8) i8* @"_SM38java.nio.charset.CoderMalfunctionErrorRL19java.lang.ExceptionE"(i8* nonnull dereferenceable(40) %_38, i8* dereferenceable_or_null(40) %_37)
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_38)
  unreachable
_34.0:
  %_203 = icmp ne i8* %_16, null
  br i1 %_203, label %_201.0, label %_202.0
_201.0:
  call i8* @"scalanative_throw"(i8* dereferenceable_or_null(8) %_16)
  unreachable
_15.0:
  %_17 = phi i8* [%_21, %_167.1]
  %_206 = icmp ne i8* %_17, null
  br i1 %_206, label %_205.0, label %_202.0
_205.0:
  %_262 = bitcast i8* bitcast (i1 (i8*)* @"_SM28java.nio.charset.CoderResultD11isUnderflowzEO" to i8*) to i1 (i8*)*
  %_48 = call i1 %_262(i8* dereferenceable_or_null(16) %_17)
  br i1 %_48, label %_43.0, label %_44.0
_43.0:
  %_208 = icmp ne i8* %_1, null
  br i1 %_208, label %_207.0, label %_202.0
_207.0:
  %_263 = bitcast i8* bitcast (i32 (i8*)* @"_SM15java.nio.BufferD9remainingiEO" to i8*) to i32 (i8*)*
  %_50 = call i32 %_263(i8* dereferenceable_or_null(40) %_1)
  br i1 %_3, label %_55.0, label %_56.0
_55.0:
  %_59 = icmp sgt i32 %_50, 0
  br label %_57.0
_56.0:
  br label %_57.0
_57.0:
  %_58 = phi i1 [false, %_56.0], [%_59, %_55.0]
  br i1 %_58, label %_51.0, label %_52.0
_51.0:
  %_60 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_61 = call dereferenceable_or_null(16) i8* @"_SM29java.nio.charset.CoderResult$D18malformedForLengthiL28java.nio.charset.CoderResultEO"(i8* nonnull dereferenceable(104) %_60, i32 %_50)
  br label %_53.0
_52.0:
  br label %_53.0
_53.0:
  %_54 = phi i8* [%_17, %_52.0], [%_61, %_51.0]
  br label %_45.0
_44.0:
  br label %_45.0
_45.0:
  %_46 = phi i8* [%_17, %_44.0], [%_54, %_53.0]
  %_210 = icmp ne i8* %_46, null
  br i1 %_210, label %_209.0, label %_202.0
_209.0:
  %_264 = bitcast i8* bitcast (i1 (i8*)* @"_SM28java.nio.charset.CoderResultD11isUnderflowzEO" to i8*) to i1 (i8*)*
  %_71 = call i1 %_264(i8* dereferenceable_or_null(16) %_46)
  br i1 %_71, label %_66.0, label %_67.0
_66.0:
  br label %_68.0
_67.0:
  %_212 = icmp ne i8* %_46, null
  br i1 %_212, label %_211.0, label %_202.0
_211.0:
  %_265 = bitcast i8* bitcast (i1 (i8*)* @"_SM28java.nio.charset.CoderResultD10isOverflowzEO" to i8*) to i1 (i8*)*
  %_73 = call i1 %_265(i8* dereferenceable_or_null(16) %_46)
  br label %_68.0
_68.0:
  %_69 = phi i1 [%_73, %_211.0], [true, %_66.0]
  br i1 %_69, label %_62.0, label %_63.0
_62.0:
  br label %_64.0
_63.0:
  %_214 = icmp ne i8* %_46, null
  br i1 %_214, label %_213.0, label %_202.0
_213.0:
  %_266 = bitcast i8* bitcast (i1 (i8*)* @"_SM28java.nio.charset.CoderResultD12isUnmappablezEO" to i8*) to i1 (i8*)*
  %_79 = call i1 %_266(i8* dereferenceable_or_null(16) %_46)
  br i1 %_79, label %_74.0, label %_75.0
_74.0:
  %_216 = icmp ne i8* %_4, null
  br i1 %_216, label %_215.0, label %_202.0
_215.0:
  %_267 = bitcast i8* bitcast (i8* (i8*)* @"_SM31java.nio.charset.CharsetEncoderD25unmappableCharacterActionL34java.nio.charset.CodingErrorActionEO" to i8*) to i8* (i8*)*
  %_81 = call dereferenceable_or_null(16) i8* %_267(i8* dereferenceable_or_null(56) %_4)
  br label %_76.0
_75.0:
  %_218 = icmp ne i8* %_4, null
  br i1 %_218, label %_217.0, label %_202.0
_217.0:
  %_268 = bitcast i8* bitcast (i8* (i8*)* @"_SM31java.nio.charset.CharsetEncoderD20malformedInputActionL34java.nio.charset.CodingErrorActionEO" to i8*) to i8* (i8*)*
  %_83 = call dereferenceable_or_null(16) i8* %_268(i8* dereferenceable_or_null(56) %_4)
  br label %_76.0
_76.0:
  %_77 = phi i8* [%_83, %_217.0], [%_81, %_215.0]
  br label %_84.0
_84.0:
  %_91 = call dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"()
  %_92 = call dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D7REPLACEL34java.nio.charset.CodingErrorActionEO"(i8* nonnull dereferenceable(32) %_91)
  %_97 = icmp eq i8* %_92, null
  br i1 %_97, label %_93.0, label %_94.0
_93.0:
  %_98 = icmp eq i8* %_77, null
  br label %_95.0
_94.0:
  %_220 = icmp ne i8* %_92, null
  br i1 %_220, label %_219.0, label %_202.0
_219.0:
  %_269 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_100 = call i1 %_269(i8* dereferenceable_or_null(16) %_92, i8* dereferenceable_or_null(16) %_77)
  br label %_95.0
_95.0:
  %_96 = phi i1 [%_100, %_219.0], [%_98, %_93.0]
  br i1 %_96, label %_87.0, label %_88.0
_87.0:
  %_222 = icmp ne i8* %_2, null
  br i1 %_222, label %_221.0, label %_202.0
_221.0:
  %_270 = bitcast i8* bitcast (i32 (i8*)* @"_SM15java.nio.BufferD9remainingiEO" to i8*) to i32 (i8*)*
  %_106 = call i32 %_270(i8* dereferenceable_or_null(48) %_2)
  %_224 = icmp ne i8* %_4, null
  br i1 %_224, label %_223.0, label %_202.0
_223.0:
  %_271 = bitcast i8* bitcast (i8* (i8*)* @"_SM31java.nio.charset.CharsetEncoderD11replacementLAb_EO" to i8*) to i8* (i8*)*
  %_108 = call dereferenceable_or_null(8) i8* %_271(i8* dereferenceable_or_null(56) %_4)
  %_226 = icmp ne i8* %_108, null
  br i1 %_226, label %_225.0, label %_202.0
_225.0:
  %_272 = bitcast i8* %_108 to { i8*, i32 }*
  %_273 = getelementptr { i8*, i32 }, { i8*, i32 }* %_272, i32 0, i32 1
  %_227 = bitcast i32* %_273 to i8*
  %_274 = bitcast i8* %_227 to i32*
  %_109 = load i32, i32* %_274
  %_110 = icmp slt i32 %_106, %_109
  br i1 %_110, label %_101.0, label %_102.0
_101.0:
  %_111 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_112 = call dereferenceable_or_null(16) i8* @"_SM29java.nio.charset.CoderResult$D8OVERFLOWL28java.nio.charset.CoderResultEO"(i8* nonnull dereferenceable(104) %_111)
  br label %_103.0
_102.0:
  %_229 = icmp ne i8* %_4, null
  br i1 %_229, label %_228.0, label %_202.0
_228.0:
  %_275 = bitcast i8* bitcast (i8* (i8*)* @"_SM31java.nio.charset.CharsetEncoderD11replacementLAb_EO" to i8*) to i8* (i8*)*
  %_114 = call dereferenceable_or_null(8) i8* %_275(i8* dereferenceable_or_null(56) %_4)
  %_231 = icmp ne i8* %_2, null
  br i1 %_231, label %_230.0, label %_202.0
_230.0:
  %_276 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM19java.nio.ByteBufferD3putLAb_L19java.nio.ByteBufferEO" to i8*) to i8* (i8*, i8*)*
  %_116 = call dereferenceable_or_null(48) i8* %_276(i8* dereferenceable_or_null(48) %_2, i8* dereferenceable_or_null(8) %_114)
  %_233 = icmp ne i8* %_1, null
  br i1 %_233, label %_232.0, label %_202.0
_232.0:
  %_277 = bitcast i8* bitcast (i32 (i8*)* @"_SM15java.nio.BufferD8positioniEO" to i8*) to i32 (i8*)*
  %_118 = call i32 %_277(i8* dereferenceable_or_null(40) %_1)
  %_235 = icmp ne i8* %_46, null
  br i1 %_235, label %_234.0, label %_202.0
_234.0:
  %_278 = bitcast i8* bitcast (i32 (i8*)* @"_SM28java.nio.charset.CoderResultD6lengthiEO" to i8*) to i32 (i8*)*
  %_120 = call i32 %_278(i8* dereferenceable_or_null(16) %_46)
  %_121 = add i32 %_118, %_120
  %_237 = icmp ne i8* %_1, null
  br i1 %_237, label %_236.0, label %_202.0
_236.0:
  %_279 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM19java.nio.CharBufferD8positioniL19java.nio.CharBufferEO" to i8*) to i8* (i8*, i32)*
  %_123 = call dereferenceable_or_null(40) i8* %_279(i8* dereferenceable_or_null(40) %_1, i32 %_121)
  br label %_10.0
_103.0:
  %_104 = phi i8* [%_112, %_101.0]
  br label %_85.0
_88.0:
  br label %_89.0
_89.0:
  %_90 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_88.0]
  %_130 = call dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"()
  %_131 = call dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D6REPORTL34java.nio.charset.CodingErrorActionEO"(i8* nonnull dereferenceable(32) %_130)
  %_136 = icmp eq i8* %_131, null
  br i1 %_136, label %_132.0, label %_133.0
_132.0:
  %_137 = icmp eq i8* %_77, null
  br label %_134.0
_133.0:
  %_239 = icmp ne i8* %_131, null
  br i1 %_239, label %_238.0, label %_202.0
_238.0:
  %_280 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_139 = call i1 %_280(i8* dereferenceable_or_null(16) %_131, i8* dereferenceable_or_null(16) %_77)
  br label %_134.0
_134.0:
  %_135 = phi i1 [%_139, %_238.0], [%_137, %_132.0]
  br i1 %_135, label %_126.0, label %_127.0
_126.0:
  br label %_85.0
_127.0:
  br label %_128.0
_128.0:
  %_129 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_127.0]
  %_145 = call dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"()
  %_146 = call dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D6IGNOREL34java.nio.charset.CodingErrorActionEO"(i8* nonnull dereferenceable(32) %_145)
  %_151 = icmp eq i8* %_146, null
  br i1 %_151, label %_147.0, label %_148.0
_147.0:
  %_152 = icmp eq i8* %_77, null
  br label %_149.0
_148.0:
  %_241 = icmp ne i8* %_146, null
  br i1 %_241, label %_240.0, label %_202.0
_240.0:
  %_281 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_154 = call i1 %_281(i8* dereferenceable_or_null(16) %_146, i8* dereferenceable_or_null(16) %_77)
  br label %_149.0
_149.0:
  %_150 = phi i1 [%_154, %_240.0], [%_152, %_147.0]
  br i1 %_150, label %_141.0, label %_142.0
_141.0:
  %_243 = icmp ne i8* %_1, null
  br i1 %_243, label %_242.0, label %_202.0
_242.0:
  %_282 = bitcast i8* bitcast (i32 (i8*)* @"_SM15java.nio.BufferD8positioniEO" to i8*) to i32 (i8*)*
  %_156 = call i32 %_282(i8* dereferenceable_or_null(40) %_1)
  %_245 = icmp ne i8* %_46, null
  br i1 %_245, label %_244.0, label %_202.0
_244.0:
  %_283 = bitcast i8* bitcast (i32 (i8*)* @"_SM28java.nio.charset.CoderResultD6lengthiEO" to i8*) to i32 (i8*)*
  %_158 = call i32 %_283(i8* dereferenceable_or_null(16) %_46)
  %_159 = add i32 %_156, %_158
  %_247 = icmp ne i8* %_1, null
  br i1 %_247, label %_246.0, label %_202.0
_246.0:
  %_284 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM19java.nio.CharBufferD8positioniL19java.nio.CharBufferEO" to i8*) to i8* (i8*, i32)*
  %_161 = call dereferenceable_or_null(40) i8* %_284(i8* dereferenceable_or_null(40) %_1, i32 %_159)
  br label %_10.0
_142.0:
  br label %_143.0
_143.0:
  %_144 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_142.0]
  %_164 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM16scala.MatchErrorG4type" to i8*), i64 64)
  call nonnull dereferenceable(8) i8* @"_SM16scala.MatchErrorRL16java.lang.ObjectE"(i8* nonnull dereferenceable(64) %_164, i8* dereferenceable_or_null(16) %_77)
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(64) %_164)
  unreachable
_85.0:
  %_86 = phi i8* [%_46, %_126.0], [%_104, %_103.0]
  br label %_64.0
_64.0:
  %_65 = phi i8* [%_86, %_85.0], [%_46, %_62.0]
  ret i8* %_65
_10.0:
  %_11 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_246.0], [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_236.0]
  br label %_6.0
_8.0:
  ret i8* zeroinitializer
_168.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_168.1 unwind label %_250.landingpad
_168.1:
  unreachable
_202.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_182.0:
  %_253 = phi i8* [%_16, %_194.0], [%_16, %_180.0]
  %_254 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM33java.nio.BufferUnderflowExceptionG4type" to i8*), %_194.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.nio.BufferOverflowExceptionG4type" to i8*), %_180.0]
  %_285 = bitcast i8* %_253 to i8**
  %_255 = load i8*, i8** %_285
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_255, i8* %_254)
  unreachable
_166.0:
  %_18 = phi i8* [%_169, %_169.landingpad.succ], [%_250, %_250.landingpad.succ], [%_171, %_171.landingpad.succ]
  br label %_12.0
_172.0:
  %_20 = phi i8* [%_173, %_173.landingpad.succ]
  br label %_12.0
_169.landingpad:
  %_286 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_287 = extractvalue { i8*, i32 } %_286, 0
  %_288 = extractvalue { i8*, i32 } %_286, 1
  %_289 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_290 = icmp eq i32 %_288, %_289
  br i1 %_290, label %_169.landingpad.succ, label %_169.landingpad.fail
_169.landingpad.succ:
  %_291 = call i8* @__cxa_begin_catch(i8* %_287)
  %_292 = bitcast i8* %_291 to i8**
  %_293 = getelementptr i8*, i8** %_292, i32 1
  %_169 = load i8*, i8** %_293
  call void @__cxa_end_catch()
  br label %_166.0
_169.landingpad.fail:
  resume { i8*, i32 } %_286
_171.landingpad:
  %_294 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_295 = extractvalue { i8*, i32 } %_294, 0
  %_296 = extractvalue { i8*, i32 } %_294, 1
  %_297 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_298 = icmp eq i32 %_296, %_297
  br i1 %_298, label %_171.landingpad.succ, label %_171.landingpad.fail
_171.landingpad.succ:
  %_299 = call i8* @__cxa_begin_catch(i8* %_295)
  %_300 = bitcast i8* %_299 to i8**
  %_301 = getelementptr i8*, i8** %_300, i32 1
  %_171 = load i8*, i8** %_301
  call void @__cxa_end_catch()
  br label %_166.0
_171.landingpad.fail:
  resume { i8*, i32 } %_294
_173.landingpad:
  %_302 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_303 = extractvalue { i8*, i32 } %_302, 0
  %_304 = extractvalue { i8*, i32 } %_302, 1
  %_305 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_306 = icmp eq i32 %_304, %_305
  br i1 %_306, label %_173.landingpad.succ, label %_173.landingpad.fail
_173.landingpad.succ:
  %_307 = call i8* @__cxa_begin_catch(i8* %_303)
  %_308 = bitcast i8* %_307 to i8**
  %_309 = getelementptr i8*, i8** %_308, i32 1
  %_173 = load i8*, i8** %_309
  call void @__cxa_end_catch()
  br label %_172.0
_173.landingpad.fail:
  resume { i8*, i32 } %_302
_250.landingpad:
  %_310 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_311 = extractvalue { i8*, i32 } %_310, 0
  %_312 = extractvalue { i8*, i32 } %_310, 1
  %_313 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_314 = icmp eq i32 %_312, %_313
  br i1 %_314, label %_250.landingpad.succ, label %_250.landingpad.fail
_250.landingpad.succ:
  %_315 = call i8* @__cxa_begin_catch(i8* %_311)
  %_316 = bitcast i8* %_315 to i8**
  %_317 = getelementptr i8*, i8** %_316, i32 1
  %_250 = load i8*, i8** %_317
  call void @__cxa_end_catch()
  br label %_166.0
_250.landingpad.fail:
  resume { i8*, i32 } %_310
}

define dereferenceable_or_null(8) i8* @"_SM31scala.collection.Map$$$Lambda$1D5applyL16java.lang.ObjectEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20005 = icmp ne i8* %_1, null
  br i1 %_20005, label %_20003.0, label %_20004.0
_20003.0:
  %_20008 = bitcast i8* %_1 to { i8*, i8* }*
  %_20009 = getelementptr { i8*, i8* }, { i8*, i8* }* %_20008, i32 0, i32 1
  %_20006 = bitcast i8** %_20009 to i8*
  %_20010 = bitcast i8* %_20006 to i8**
  %_20001 = load i8*, i8** %_20010, !dereferenceable_or_null !{i64 32}
  %_20002 = call dereferenceable_or_null(8) i8* @"_SM21scala.collection.Map$D28$anonfun$DefaultSentinelFn$1L16java.lang.ObjectEPT21scala.collection.Map$"(i8* dereferenceable_or_null(32) %_20001)
  ret i8* %_20002
_20004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM31scala.collection.immutable.Map$D5emptyL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = bitcast i8* bitcast (i8* (i8*)* @"_SM31scala.collection.immutable.Map$D5emptyL30scala.collection.immutable.MapEO" to i8*) to i8* (i8*)*
  %_20001 = call dereferenceable_or_null(8) i8* %_20002(i8* dereferenceable_or_null(8) %_1)
  ret i8* %_20001
}

define nonnull dereferenceable(8) i8* @"_SM31scala.collection.immutable.Map$D5emptyL30scala.collection.immutable.MapEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(8) i8* @"_SM40scala.collection.immutable.Map$EmptyMap$G4load"()
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM31scala.collection.immutable.Map$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 96
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 8}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM31scala.collection.immutable.Map$G4type" to i8*), i64 8)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM31scala.collection.immutable.Map$RE"(i8* dereferenceable_or_null(8) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM31scala.collection.immutable.Map$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.MapFactoryD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i32 @"_SM32java.lang.ProcessBuilder$$anon$3D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM32java.lang.ProcessBuilder$$anon$3D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM32java.lang.ProcessBuilder$$anon$3D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define i32 @"_SM32scala.collection.IterableOnceOpsD11copyToArrayL16java.lang.ObjectiEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30006 = icmp ne i8* %_1, null
  br i1 %_30006, label %_30004.0, label %_30005.0
_30004.0:
  %_30013 = bitcast i8* %_1 to i8**
  %_30007 = load i8*, i8** %_30013
  %_30014 = bitcast i8* %_30007 to { i8*, i32, i32, i8* }*
  %_30015 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30014, i32 0, i32 2
  %_30008 = bitcast i32* %_30015 to i8*
  %_30016 = bitcast i8* %_30008 to i32*
  %_30009 = load i32, i32* %_30016
  %_30017 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30018 = getelementptr i8*, i8** %_30017, i32 555
  %_30010 = bitcast i8** %_30018 to i8*
  %_30019 = bitcast i8* %_30010 to i8**
  %_30020 = getelementptr i8*, i8** %_30019, i32 %_30009
  %_30011 = bitcast i8** %_30020 to i8*
  %_30021 = bitcast i8* %_30011 to i8**
  %_30002 = load i8*, i8** %_30021
  %_30022 = bitcast i8* %_30002 to i32 (i8*, i8*, i32, i32)*
  %_30003 = call i32 %_30022(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2, i32 0, i32 2147483647)
  ret i32 %_30003
_30005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM32scala.collection.IterableOnceOpsD11copyToArrayL16java.lang.ObjectiiEO"(i8* %_1, i8* %_2, i32 %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40006 = icmp ne i8* %_1, null
  br i1 %_40006, label %_40004.0, label %_40005.0
_40004.0:
  %_40013 = bitcast i8* %_1 to i8**
  %_40007 = load i8*, i8** %_40013
  %_40014 = bitcast i8* %_40007 to { i8*, i32, i32, i8* }*
  %_40015 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40014, i32 0, i32 2
  %_40008 = bitcast i32* %_40015 to i8*
  %_40016 = bitcast i8* %_40008 to i32*
  %_40009 = load i32, i32* %_40016
  %_40017 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_40018 = getelementptr i8*, i8** %_40017, i32 555
  %_40010 = bitcast i8** %_40018 to i8*
  %_40019 = bitcast i8* %_40010 to i8**
  %_40020 = getelementptr i8*, i8** %_40019, i32 %_40009
  %_40011 = bitcast i8** %_40020 to i8*
  %_40021 = bitcast i8* %_40011 to i8**
  %_40002 = load i8*, i8** %_40021
  %_40022 = bitcast i8* %_40002 to i32 (i8*, i8*, i32, i32)*
  %_40003 = call i32 %_40022(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2, i32 %_3, i32 2147483647)
  ret i32 %_40003
_40005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM32scala.collection.IterableOnceOpsD11copyToArrayL16java.lang.ObjectiiiEO"(i8* %_1, i8* %_2, i32 %_3, i32 %_4) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_120006 = icmp eq i8* %_1, null
  br i1 %_120006, label %_120004.0, label %_120003.0
_120003.0:
  %_120040 = bitcast i8* %_1 to i8**
  %_120007 = load i8*, i8** %_120040
  %_120041 = bitcast i8* %_120007 to { i8*, i32, i32, i8* }*
  %_120042 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_120041, i32 0, i32 1
  %_120008 = bitcast i32* %_120042 to i8*
  %_120043 = bitcast i8* %_120008 to i32*
  %_120009 = load i32, i32* %_120043
  %_120044 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_120045 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_120044, i32 0, i32 %_120009, i32 57
  %_120010 = bitcast i1* %_120045 to i8*
  %_120046 = bitcast i8* %_120010 to i1*
  %_120011 = load i1, i1* %_120046
  br i1 %_120011, label %_120004.0, label %_120005.0
_120004.0:
  %_50002 = bitcast i8* %_1 to i8*
  %_120014 = icmp ne i8* %_50002, null
  br i1 %_120014, label %_120012.0, label %_120013.0
_120012.0:
  %_120047 = bitcast i8* %_50002 to i8**
  %_120015 = load i8*, i8** %_120047
  %_120048 = bitcast i8* %_120015 to { i8*, i32, i32, i8* }*
  %_120049 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_120048, i32 0, i32 2
  %_120016 = bitcast i32* %_120049 to i8*
  %_120050 = bitcast i8* %_120016 to i32*
  %_120017 = load i32, i32* %_120050
  %_120051 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_120052 = getelementptr i8*, i8** %_120051, i32 446
  %_120018 = bitcast i8** %_120052 to i8*
  %_120053 = bitcast i8* %_120018 to i8**
  %_120054 = getelementptr i8*, i8** %_120053, i32 %_120017
  %_120019 = bitcast i8** %_120054 to i8*
  %_120055 = bitcast i8* %_120019 to i8**
  %_50004 = load i8*, i8** %_120055
  %_120056 = bitcast i8* %_50004 to i8* (i8*)*
  %_50005 = call dereferenceable_or_null(8) i8* %_120056(i8* dereferenceable_or_null(8) %_50002)
  %_50008 = call i32 @"_SM27scala.runtime.ScalaRunTime$D12array_lengthL16java.lang.ObjectiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(8) %_2)
  %_50010 = sub i32 %_50008, %_3
  %_50011 = call i32 @"_SM19scala.math.package$D3miniiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19scala.math.package$G8instance" to i8*), i32 %_4, i32 %_50010)
  %_50013 = add i32 %_3, %_50011
  br label %_60000.0
_60000.0:
  %_60001 = phi i32 [%_3, %_120012.0], [%_100006, %_120027.0]
  %_60002 = phi i32 [%_50013, %_120012.0], [%_60002, %_120027.0]
  %_60004 = icmp slt i32 %_60001, %_60002
  br i1 %_60004, label %_70000.0, label %_80000.0
_70000.0:
  %_120021 = icmp ne i8* %_50005, null
  br i1 %_120021, label %_120020.0, label %_120013.0
_120020.0:
  %_120057 = bitcast i8* %_50005 to i8**
  %_120022 = load i8*, i8** %_120057
  %_120058 = bitcast i8* %_120022 to { i8*, i32, i32, i8* }*
  %_120059 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_120058, i32 0, i32 2
  %_120023 = bitcast i32* %_120059 to i8*
  %_120060 = bitcast i8* %_120023 to i32*
  %_120024 = load i32, i32* %_120060
  %_120061 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_120062 = getelementptr i8*, i8** %_120061, i32 4359
  %_120025 = bitcast i8** %_120062 to i8*
  %_120063 = bitcast i8* %_120025 to i8**
  %_120064 = getelementptr i8*, i8** %_120063, i32 %_120024
  %_120026 = bitcast i8** %_120064 to i8*
  %_120065 = bitcast i8* %_120026 to i8**
  %_70002 = load i8*, i8** %_120065
  %_120066 = bitcast i8* %_70002 to i1 (i8*)*
  %_70003 = call i1 %_120066(i8* dereferenceable_or_null(8) %_50005)
  br label %_90000.0
_80000.0:
  br label %_90000.0
_90000.0:
  %_90001 = phi i1 [false, %_80000.0], [%_70003, %_120020.0]
  br i1 %_90001, label %_100000.0, label %_110000.0
_100000.0:
  %_120028 = icmp ne i8* %_50005, null
  br i1 %_120028, label %_120027.0, label %_120013.0
_120027.0:
  %_120067 = bitcast i8* %_50005 to i8**
  %_120029 = load i8*, i8** %_120067
  %_120068 = bitcast i8* %_120029 to { i8*, i32, i32, i8* }*
  %_120069 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_120068, i32 0, i32 2
  %_120030 = bitcast i32* %_120069 to i8*
  %_120070 = bitcast i8* %_120030 to i32*
  %_120031 = load i32, i32* %_120070
  %_120071 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_120072 = getelementptr i8*, i8** %_120071, i32 4219
  %_120032 = bitcast i8** %_120072 to i8*
  %_120073 = bitcast i8* %_120032 to i8**
  %_120074 = getelementptr i8*, i8** %_120073, i32 %_120031
  %_120033 = bitcast i8** %_120074 to i8*
  %_120075 = bitcast i8* %_120033 to i8**
  %_100002 = load i8*, i8** %_120075
  %_120076 = bitcast i8* %_100002 to i8* (i8*)*
  %_100003 = call dereferenceable_or_null(8) i8* %_120076(i8* dereferenceable_or_null(8) %_50005)
  call nonnull dereferenceable(8) i8* @"_SM27scala.runtime.ScalaRunTime$D12array_updateL16java.lang.ObjectiL16java.lang.ObjectuEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(8) %_2, i32 %_60001, i8* dereferenceable_or_null(8) %_100003)
  %_100006 = add i32 %_60001, 1
  br label %_60000.0
_110000.0:
  br label %_120000.0
_120000.0:
  %_120002 = sub i32 %_60001, %_3
  ret i32 %_120002
_120013.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_120005.0:
  %_120036 = phi i8* [%_1, %_120003.0]
  %_120037 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM29scala.collection.IterableOnceG4type" to i8*), %_120003.0]
  %_120077 = bitcast i8* %_120036 to i8**
  %_120038 = load i8*, i8** %_120077
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_120038, i8* %_120037)
  unreachable
}

define i32 @"_SM32scala.collection.IterableOnceOpsD4sizeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_90004 = icmp eq i8* %_1, null
  br i1 %_90004, label %_90002.0, label %_90001.0
_90001.0:
  %_90067 = bitcast i8* %_1 to i8**
  %_90005 = load i8*, i8** %_90067
  %_90068 = bitcast i8* %_90005 to { i8*, i32, i32, i8* }*
  %_90069 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90068, i32 0, i32 1
  %_90006 = bitcast i32* %_90069 to i8*
  %_90070 = bitcast i8* %_90006 to i32*
  %_90007 = load i32, i32* %_90070
  %_90071 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_90072 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_90071, i32 0, i32 %_90007, i32 57
  %_90008 = bitcast i1* %_90072 to i8*
  %_90073 = bitcast i8* %_90008 to i1*
  %_90009 = load i1, i1* %_90073
  br i1 %_90009, label %_90002.0, label %_90003.0
_90002.0:
  %_20002 = bitcast i8* %_1 to i8*
  %_90012 = icmp ne i8* %_20002, null
  br i1 %_90012, label %_90010.0, label %_90011.0
_90010.0:
  %_90074 = bitcast i8* %_20002 to i8**
  %_90013 = load i8*, i8** %_90074
  %_90075 = bitcast i8* %_90013 to { i8*, i32, i32, i8* }*
  %_90076 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90075, i32 0, i32 2
  %_90014 = bitcast i32* %_90076 to i8*
  %_90077 = bitcast i8* %_90014 to i32*
  %_90015 = load i32, i32* %_90077
  %_90078 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_90079 = getelementptr i8*, i8** %_90078, i32 669
  %_90016 = bitcast i8** %_90079 to i8*
  %_90080 = bitcast i8* %_90016 to i8**
  %_90081 = getelementptr i8*, i8** %_90080, i32 %_90015
  %_90017 = bitcast i8** %_90081 to i8*
  %_90082 = bitcast i8* %_90017 to i8**
  %_20004 = load i8*, i8** %_90082
  %_90083 = bitcast i8* %_20004 to i32 (i8*)*
  %_20005 = call i32 %_90083(i8* dereferenceable_or_null(8) %_20002)
  %_20007 = icmp sge i32 %_20005, 0
  br i1 %_20007, label %_30000.0, label %_40000.0
_30000.0:
  %_90020 = icmp eq i8* %_1, null
  br i1 %_90020, label %_90019.0, label %_90018.0
_90018.0:
  %_90084 = bitcast i8* %_1 to i8**
  %_90021 = load i8*, i8** %_90084
  %_90085 = bitcast i8* %_90021 to { i8*, i32, i32, i8* }*
  %_90086 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90085, i32 0, i32 1
  %_90022 = bitcast i32* %_90086 to i8*
  %_90087 = bitcast i8* %_90022 to i32*
  %_90023 = load i32, i32* %_90087
  %_90088 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_90089 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_90088, i32 0, i32 %_90023, i32 57
  %_90024 = bitcast i1* %_90089 to i8*
  %_90090 = bitcast i8* %_90024 to i1*
  %_90025 = load i1, i1* %_90090
  br i1 %_90025, label %_90019.0, label %_90003.0
_90019.0:
  %_30001 = bitcast i8* %_1 to i8*
  %_90027 = icmp ne i8* %_30001, null
  br i1 %_90027, label %_90026.0, label %_90011.0
_90026.0:
  %_90091 = bitcast i8* %_30001 to i8**
  %_90028 = load i8*, i8** %_90091
  %_90092 = bitcast i8* %_90028 to { i8*, i32, i32, i8* }*
  %_90093 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90092, i32 0, i32 2
  %_90029 = bitcast i32* %_90093 to i8*
  %_90094 = bitcast i8* %_90029 to i32*
  %_90030 = load i32, i32* %_90094
  %_90095 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_90096 = getelementptr i8*, i8** %_90095, i32 669
  %_90031 = bitcast i8** %_90096 to i8*
  %_90097 = bitcast i8* %_90031 to i8**
  %_90098 = getelementptr i8*, i8** %_90097, i32 %_90030
  %_90032 = bitcast i8** %_90098 to i8*
  %_90099 = bitcast i8* %_90032 to i8**
  %_30003 = load i8*, i8** %_90099
  %_90100 = bitcast i8* %_30003 to i32 (i8*)*
  %_30004 = call i32 %_90100(i8* dereferenceable_or_null(8) %_30001)
  br label %_50000.0
_40000.0:
  %_90035 = icmp eq i8* %_1, null
  br i1 %_90035, label %_90034.0, label %_90033.0
_90033.0:
  %_90101 = bitcast i8* %_1 to i8**
  %_90036 = load i8*, i8** %_90101
  %_90102 = bitcast i8* %_90036 to { i8*, i32, i32, i8* }*
  %_90103 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90102, i32 0, i32 1
  %_90037 = bitcast i32* %_90103 to i8*
  %_90104 = bitcast i8* %_90037 to i32*
  %_90038 = load i32, i32* %_90104
  %_90105 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_90106 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_90105, i32 0, i32 %_90038, i32 57
  %_90039 = bitcast i1* %_90106 to i8*
  %_90107 = bitcast i8* %_90039 to i1*
  %_90040 = load i1, i1* %_90107
  br i1 %_90040, label %_90034.0, label %_90003.0
_90034.0:
  %_40001 = bitcast i8* %_1 to i8*
  %_90042 = icmp ne i8* %_40001, null
  br i1 %_90042, label %_90041.0, label %_90011.0
_90041.0:
  %_90108 = bitcast i8* %_40001 to i8**
  %_90043 = load i8*, i8** %_90108
  %_90109 = bitcast i8* %_90043 to { i8*, i32, i32, i8* }*
  %_90110 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90109, i32 0, i32 2
  %_90044 = bitcast i32* %_90110 to i8*
  %_90111 = bitcast i8* %_90044 to i32*
  %_90045 = load i32, i32* %_90111
  %_90112 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_90113 = getelementptr i8*, i8** %_90112, i32 446
  %_90046 = bitcast i8** %_90113 to i8*
  %_90114 = bitcast i8* %_90046 to i8**
  %_90115 = getelementptr i8*, i8** %_90114, i32 %_90045
  %_90047 = bitcast i8** %_90115 to i8*
  %_90116 = bitcast i8* %_90047 to i8**
  %_40003 = load i8*, i8** %_90116
  %_90117 = bitcast i8* %_40003 to i8* (i8*)*
  %_40004 = call dereferenceable_or_null(8) i8* %_90117(i8* dereferenceable_or_null(8) %_40001)
  br label %_60000.0
_60000.0:
  %_60001 = phi i32 [0, %_90041.0], [%_70005, %_90055.0]
  %_90049 = icmp ne i8* %_40004, null
  br i1 %_90049, label %_90048.0, label %_90011.0
_90048.0:
  %_90118 = bitcast i8* %_40004 to i8**
  %_90050 = load i8*, i8** %_90118
  %_90119 = bitcast i8* %_90050 to { i8*, i32, i32, i8* }*
  %_90120 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90119, i32 0, i32 2
  %_90051 = bitcast i32* %_90120 to i8*
  %_90121 = bitcast i8* %_90051 to i32*
  %_90052 = load i32, i32* %_90121
  %_90122 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_90123 = getelementptr i8*, i8** %_90122, i32 4359
  %_90053 = bitcast i8** %_90123 to i8*
  %_90124 = bitcast i8* %_90053 to i8**
  %_90125 = getelementptr i8*, i8** %_90124, i32 %_90052
  %_90054 = bitcast i8** %_90125 to i8*
  %_90126 = bitcast i8* %_90054 to i8**
  %_60003 = load i8*, i8** %_90126
  %_90127 = bitcast i8* %_60003 to i1 (i8*)*
  %_60004 = call i1 %_90127(i8* dereferenceable_or_null(8) %_40004)
  br i1 %_60004, label %_70000.0, label %_80000.0
_70000.0:
  %_90056 = icmp ne i8* %_40004, null
  br i1 %_90056, label %_90055.0, label %_90011.0
_90055.0:
  %_90128 = bitcast i8* %_40004 to i8**
  %_90057 = load i8*, i8** %_90128
  %_90129 = bitcast i8* %_90057 to { i8*, i32, i32, i8* }*
  %_90130 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90129, i32 0, i32 2
  %_90058 = bitcast i32* %_90130 to i8*
  %_90131 = bitcast i8* %_90058 to i32*
  %_90059 = load i32, i32* %_90131
  %_90132 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_90133 = getelementptr i8*, i8** %_90132, i32 4219
  %_90060 = bitcast i8** %_90133 to i8*
  %_90134 = bitcast i8* %_90060 to i8**
  %_90135 = getelementptr i8*, i8** %_90134, i32 %_90059
  %_90061 = bitcast i8** %_90135 to i8*
  %_90136 = bitcast i8* %_90061 to i8**
  %_70003 = load i8*, i8** %_90136
  %_90137 = bitcast i8* %_70003 to i8* (i8*)*
  %_70004 = call dereferenceable_or_null(8) i8* %_90137(i8* dereferenceable_or_null(8) %_40004)
  %_70005 = add i32 %_60001, 1
  br label %_60000.0
_80000.0:
  br label %_90000.0
_90000.0:
  br label %_50000.0
_50000.0:
  %_50001 = phi i32 [%_60001, %_90000.0], [0, %_90026.0]
  %_50002 = phi i32 [%_60001, %_90000.0], [%_30004, %_90026.0]
  ret i32 %_50002
_90011.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_90003.0:
  %_90063 = phi i8* [%_1, %_90001.0], [%_1, %_90033.0], [%_1, %_90018.0]
  %_90064 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM29scala.collection.IterableOnceG4type" to i8*), %_90001.0], [bitcast ({ i8*, i32, i32, i8* }* @"_SM29scala.collection.IterableOnceG4type" to i8*), %_90033.0], [bitcast ({ i8*, i32, i32, i8* }* @"_SM29scala.collection.IterableOnceG4type" to i8*), %_90018.0]
  %_90138 = bitcast i8* %_90063 to i8**
  %_90065 = load i8*, i8** %_90138
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_90065, i8* %_90064)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM32scala.collection.IterableOnceOpsD6forallL15scala.Function1zEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_100004 = icmp eq i8* %_1, null
  br i1 %_100004, label %_100002.0, label %_100001.0
_100001.0:
  %_100044 = bitcast i8* %_1 to i8**
  %_100005 = load i8*, i8** %_100044
  %_100045 = bitcast i8* %_100005 to { i8*, i32, i32, i8* }*
  %_100046 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_100045, i32 0, i32 1
  %_100006 = bitcast i32* %_100046 to i8*
  %_100047 = bitcast i8* %_100006 to i32*
  %_100007 = load i32, i32* %_100047
  %_100048 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_100049 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_100048, i32 0, i32 %_100007, i32 57
  %_100008 = bitcast i1* %_100049 to i8*
  %_100050 = bitcast i8* %_100008 to i1*
  %_100009 = load i1, i1* %_100050
  br i1 %_100009, label %_100002.0, label %_100003.0
_100002.0:
  %_30002 = bitcast i8* %_1 to i8*
  %_100012 = icmp ne i8* %_30002, null
  br i1 %_100012, label %_100010.0, label %_100011.0
_100010.0:
  %_100051 = bitcast i8* %_30002 to i8**
  %_100013 = load i8*, i8** %_100051
  %_100052 = bitcast i8* %_100013 to { i8*, i32, i32, i8* }*
  %_100053 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_100052, i32 0, i32 2
  %_100014 = bitcast i32* %_100053 to i8*
  %_100054 = bitcast i8* %_100014 to i32*
  %_100015 = load i32, i32* %_100054
  %_100055 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_100056 = getelementptr i8*, i8** %_100055, i32 446
  %_100016 = bitcast i8** %_100056 to i8*
  %_100057 = bitcast i8* %_100016 to i8**
  %_100058 = getelementptr i8*, i8** %_100057, i32 %_100015
  %_100017 = bitcast i8** %_100058 to i8*
  %_100059 = bitcast i8* %_100017 to i8**
  %_30004 = load i8*, i8** %_100059
  %_100060 = bitcast i8* %_30004 to i8* (i8*)*
  %_30005 = call dereferenceable_or_null(8) i8* %_100060(i8* dereferenceable_or_null(8) %_30002)
  br label %_40000.0
_40000.0:
  %_40001 = phi i1 [true, %_100010.0], [%_70007, %_100032.0]
  br i1 %_40001, label %_50000.0, label %_90000.0
_50000.0:
  %_100019 = icmp ne i8* %_30005, null
  br i1 %_100019, label %_100018.0, label %_100011.0
_100018.0:
  %_100061 = bitcast i8* %_30005 to i8**
  %_100020 = load i8*, i8** %_100061
  %_100062 = bitcast i8* %_100020 to { i8*, i32, i32, i8* }*
  %_100063 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_100062, i32 0, i32 2
  %_100021 = bitcast i32* %_100063 to i8*
  %_100064 = bitcast i8* %_100021 to i32*
  %_100022 = load i32, i32* %_100064
  %_100065 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_100066 = getelementptr i8*, i8** %_100065, i32 4359
  %_100023 = bitcast i8** %_100066 to i8*
  %_100067 = bitcast i8* %_100023 to i8**
  %_100068 = getelementptr i8*, i8** %_100067, i32 %_100022
  %_100024 = bitcast i8** %_100068 to i8*
  %_100069 = bitcast i8* %_100024 to i8**
  %_50002 = load i8*, i8** %_100069
  %_100070 = bitcast i8* %_50002 to i1 (i8*)*
  %_50003 = call i1 %_100070(i8* dereferenceable_or_null(8) %_30005)
  br label %_60000.0
_90000.0:
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [false, %_90000.0], [%_50003, %_100018.0]
  br i1 %_60001, label %_70000.0, label %_80000.0
_70000.0:
  %_100026 = icmp ne i8* %_30005, null
  br i1 %_100026, label %_100025.0, label %_100011.0
_100025.0:
  %_100071 = bitcast i8* %_30005 to i8**
  %_100027 = load i8*, i8** %_100071
  %_100072 = bitcast i8* %_100027 to { i8*, i32, i32, i8* }*
  %_100073 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_100072, i32 0, i32 2
  %_100028 = bitcast i32* %_100073 to i8*
  %_100074 = bitcast i8* %_100028 to i32*
  %_100029 = load i32, i32* %_100074
  %_100075 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_100076 = getelementptr i8*, i8** %_100075, i32 4219
  %_100030 = bitcast i8** %_100076 to i8*
  %_100077 = bitcast i8* %_100030 to i8**
  %_100078 = getelementptr i8*, i8** %_100077, i32 %_100029
  %_100031 = bitcast i8** %_100078 to i8*
  %_100079 = bitcast i8* %_100031 to i8**
  %_70002 = load i8*, i8** %_100079
  %_100080 = bitcast i8* %_70002 to i8* (i8*)*
  %_70003 = call dereferenceable_or_null(8) i8* %_100080(i8* dereferenceable_or_null(8) %_30005)
  %_100033 = icmp ne i8* %_2, null
  br i1 %_100033, label %_100032.0, label %_100011.0
_100032.0:
  %_100081 = bitcast i8* %_2 to i8**
  %_100034 = load i8*, i8** %_100081
  %_100082 = bitcast i8* %_100034 to { i8*, i32, i32, i8* }*
  %_100083 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_100082, i32 0, i32 2
  %_100035 = bitcast i32* %_100083 to i8*
  %_100084 = bitcast i8* %_100035 to i32*
  %_100036 = load i32, i32* %_100084
  %_100085 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_100086 = getelementptr i8*, i8** %_100085, i32 1010
  %_100037 = bitcast i8** %_100086 to i8*
  %_100087 = bitcast i8* %_100037 to i8**
  %_100088 = getelementptr i8*, i8** %_100087, i32 %_100036
  %_100038 = bitcast i8** %_100088 to i8*
  %_100089 = bitcast i8* %_100038 to i8**
  %_70005 = load i8*, i8** %_100089
  %_100090 = bitcast i8* %_70005 to i8* (i8*, i8*)*
  %_70006 = call dereferenceable_or_null(8) i8* %_100090(i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_70003)
  %_100091 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D14unboxToBooleanL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_70007 = call i1 %_100091(i8* null, i8* dereferenceable_or_null(8) %_70006)
  br label %_40000.0
_80000.0:
  br label %_100000.0
_100000.0:
  ret i1 %_40001
_100011.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_100003.0:
  %_100040 = phi i8* [%_1, %_100001.0]
  %_100041 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM29scala.collection.IterableOnceG4type" to i8*), %_100001.0]
  %_100092 = bitcast i8* %_100040 to i8**
  %_100042 = load i8*, i8** %_100092
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_100042, i8* %_100041)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD7foreachL15scala.Function1uEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_70004 = icmp eq i8* %_1, null
  br i1 %_70004, label %_70002.0, label %_70001.0
_70001.0:
  %_70044 = bitcast i8* %_1 to i8**
  %_70005 = load i8*, i8** %_70044
  %_70045 = bitcast i8* %_70005 to { i8*, i32, i32, i8* }*
  %_70046 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_70045, i32 0, i32 1
  %_70006 = bitcast i32* %_70046 to i8*
  %_70047 = bitcast i8* %_70006 to i32*
  %_70007 = load i32, i32* %_70047
  %_70048 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_70049 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_70048, i32 0, i32 %_70007, i32 57
  %_70008 = bitcast i1* %_70049 to i8*
  %_70050 = bitcast i8* %_70008 to i1*
  %_70009 = load i1, i1* %_70050
  br i1 %_70009, label %_70002.0, label %_70003.0
_70002.0:
  %_30001 = bitcast i8* %_1 to i8*
  %_70012 = icmp ne i8* %_30001, null
  br i1 %_70012, label %_70010.0, label %_70011.0
_70010.0:
  %_70051 = bitcast i8* %_30001 to i8**
  %_70013 = load i8*, i8** %_70051
  %_70052 = bitcast i8* %_70013 to { i8*, i32, i32, i8* }*
  %_70053 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_70052, i32 0, i32 2
  %_70014 = bitcast i32* %_70053 to i8*
  %_70054 = bitcast i8* %_70014 to i32*
  %_70015 = load i32, i32* %_70054
  %_70055 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_70056 = getelementptr i8*, i8** %_70055, i32 446
  %_70016 = bitcast i8** %_70056 to i8*
  %_70057 = bitcast i8* %_70016 to i8**
  %_70058 = getelementptr i8*, i8** %_70057, i32 %_70015
  %_70017 = bitcast i8** %_70058 to i8*
  %_70059 = bitcast i8* %_70017 to i8**
  %_30003 = load i8*, i8** %_70059
  %_70060 = bitcast i8* %_30003 to i8* (i8*)*
  %_30004 = call dereferenceable_or_null(8) i8* %_70060(i8* dereferenceable_or_null(8) %_30001)
  br label %_40000.0
_40000.0:
  %_70019 = icmp ne i8* %_30004, null
  br i1 %_70019, label %_70018.0, label %_70011.0
_70018.0:
  %_70061 = bitcast i8* %_30004 to i8**
  %_70020 = load i8*, i8** %_70061
  %_70062 = bitcast i8* %_70020 to { i8*, i32, i32, i8* }*
  %_70063 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_70062, i32 0, i32 2
  %_70021 = bitcast i32* %_70063 to i8*
  %_70064 = bitcast i8* %_70021 to i32*
  %_70022 = load i32, i32* %_70064
  %_70065 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_70066 = getelementptr i8*, i8** %_70065, i32 4359
  %_70023 = bitcast i8** %_70066 to i8*
  %_70067 = bitcast i8* %_70023 to i8**
  %_70068 = getelementptr i8*, i8** %_70067, i32 %_70022
  %_70024 = bitcast i8** %_70068 to i8*
  %_70069 = bitcast i8* %_70024 to i8**
  %_40002 = load i8*, i8** %_70069
  %_70070 = bitcast i8* %_40002 to i1 (i8*)*
  %_40003 = call i1 %_70070(i8* dereferenceable_or_null(8) %_30004)
  br i1 %_40003, label %_50000.0, label %_60000.0
_50000.0:
  %_70026 = icmp ne i8* %_30004, null
  br i1 %_70026, label %_70025.0, label %_70011.0
_70025.0:
  %_70071 = bitcast i8* %_30004 to i8**
  %_70027 = load i8*, i8** %_70071
  %_70072 = bitcast i8* %_70027 to { i8*, i32, i32, i8* }*
  %_70073 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_70072, i32 0, i32 2
  %_70028 = bitcast i32* %_70073 to i8*
  %_70074 = bitcast i8* %_70028 to i32*
  %_70029 = load i32, i32* %_70074
  %_70075 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_70076 = getelementptr i8*, i8** %_70075, i32 4219
  %_70030 = bitcast i8** %_70076 to i8*
  %_70077 = bitcast i8* %_70030 to i8**
  %_70078 = getelementptr i8*, i8** %_70077, i32 %_70029
  %_70031 = bitcast i8** %_70078 to i8*
  %_70079 = bitcast i8* %_70031 to i8**
  %_50002 = load i8*, i8** %_70079
  %_70080 = bitcast i8* %_50002 to i8* (i8*)*
  %_50003 = call dereferenceable_or_null(8) i8* %_70080(i8* dereferenceable_or_null(8) %_30004)
  %_70033 = icmp ne i8* %_2, null
  br i1 %_70033, label %_70032.0, label %_70011.0
_70032.0:
  %_70081 = bitcast i8* %_2 to i8**
  %_70034 = load i8*, i8** %_70081
  %_70082 = bitcast i8* %_70034 to { i8*, i32, i32, i8* }*
  %_70083 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_70082, i32 0, i32 2
  %_70035 = bitcast i32* %_70083 to i8*
  %_70084 = bitcast i8* %_70035 to i32*
  %_70036 = load i32, i32* %_70084
  %_70085 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_70086 = getelementptr i8*, i8** %_70085, i32 1010
  %_70037 = bitcast i8** %_70086 to i8*
  %_70087 = bitcast i8* %_70037 to i8**
  %_70088 = getelementptr i8*, i8** %_70087, i32 %_70036
  %_70038 = bitcast i8** %_70088 to i8*
  %_70089 = bitcast i8* %_70038 to i8**
  %_50005 = load i8*, i8** %_70089
  %_70090 = bitcast i8* %_50005 to i8* (i8*, i8*)*
  %_50006 = call dereferenceable_or_null(8) i8* %_70090(i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_50003)
  br label %_40000.0
_60000.0:
  br label %_70000.0
_70000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_70011.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_70003.0:
  %_70040 = phi i8* [%_1, %_70001.0]
  %_70041 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM29scala.collection.IterableOnceG4type" to i8*), %_70001.0]
  %_70091 = bitcast i8* %_70040 to i8**
  %_70042 = load i8*, i8** %_70091
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_70042, i8* %_70041)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM32scala.collection.IterableOnceOpsD7toArrayL22scala.reflect.ClassTagL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_60005 = icmp eq i8* %_1, null
  br i1 %_60005, label %_60003.0, label %_60002.0
_60002.0:
  %_60069 = bitcast i8* %_1 to i8**
  %_60006 = load i8*, i8** %_60069
  %_60070 = bitcast i8* %_60006 to { i8*, i32, i32, i8* }*
  %_60071 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60070, i32 0, i32 1
  %_60007 = bitcast i32* %_60071 to i8*
  %_60072 = bitcast i8* %_60007 to i32*
  %_60008 = load i32, i32* %_60072
  %_60073 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_60074 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_60073, i32 0, i32 %_60008, i32 57
  %_60009 = bitcast i1* %_60074 to i8*
  %_60075 = bitcast i8* %_60009 to i1*
  %_60010 = load i1, i1* %_60075
  br i1 %_60010, label %_60003.0, label %_60004.0
_60003.0:
  %_30001 = bitcast i8* %_1 to i8*
  %_60013 = icmp ne i8* %_30001, null
  br i1 %_60013, label %_60011.0, label %_60012.0
_60011.0:
  %_60076 = bitcast i8* %_30001 to i8**
  %_60014 = load i8*, i8** %_60076
  %_60077 = bitcast i8* %_60014 to { i8*, i32, i32, i8* }*
  %_60078 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60077, i32 0, i32 2
  %_60015 = bitcast i32* %_60078 to i8*
  %_60079 = bitcast i8* %_60015 to i32*
  %_60016 = load i32, i32* %_60079
  %_60080 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_60081 = getelementptr i8*, i8** %_60080, i32 669
  %_60017 = bitcast i8** %_60081 to i8*
  %_60082 = bitcast i8* %_60017 to i8**
  %_60083 = getelementptr i8*, i8** %_60082, i32 %_60016
  %_60018 = bitcast i8** %_60083 to i8*
  %_60084 = bitcast i8* %_60018 to i8**
  %_30003 = load i8*, i8** %_60084
  %_60085 = bitcast i8* %_30003 to i32 (i8*)*
  %_30004 = call i32 %_60085(i8* dereferenceable_or_null(8) %_30001)
  %_30006 = icmp sge i32 %_30004, 0
  br i1 %_30006, label %_40000.0, label %_50000.0
_40000.0:
  %_60021 = icmp eq i8* %_1, null
  br i1 %_60021, label %_60020.0, label %_60019.0
_60019.0:
  %_60086 = bitcast i8* %_1 to i8**
  %_60022 = load i8*, i8** %_60086
  %_60087 = bitcast i8* %_60022 to { i8*, i32, i32, i8* }*
  %_60088 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60087, i32 0, i32 1
  %_60023 = bitcast i32* %_60088 to i8*
  %_60089 = bitcast i8* %_60023 to i32*
  %_60024 = load i32, i32* %_60089
  %_60090 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_60091 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_60090, i32 0, i32 %_60024, i32 57
  %_60025 = bitcast i1* %_60091 to i8*
  %_60092 = bitcast i8* %_60025 to i1*
  %_60026 = load i1, i1* %_60092
  br i1 %_60026, label %_60020.0, label %_60004.0
_60020.0:
  %_40001 = bitcast i8* %_1 to i8*
  %_60028 = icmp ne i8* %_40001, null
  br i1 %_60028, label %_60027.0, label %_60012.0
_60027.0:
  %_60093 = bitcast i8* %_40001 to i8**
  %_60029 = load i8*, i8** %_60093
  %_60094 = bitcast i8* %_60029 to { i8*, i32, i32, i8* }*
  %_60095 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60094, i32 0, i32 2
  %_60030 = bitcast i32* %_60095 to i8*
  %_60096 = bitcast i8* %_60030 to i32*
  %_60031 = load i32, i32* %_60096
  %_60097 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_60098 = getelementptr i8*, i8** %_60097, i32 669
  %_60032 = bitcast i8** %_60098 to i8*
  %_60099 = bitcast i8* %_60032 to i8**
  %_60100 = getelementptr i8*, i8** %_60099, i32 %_60031
  %_60033 = bitcast i8** %_60100 to i8*
  %_60101 = bitcast i8* %_60033 to i8**
  %_40003 = load i8*, i8** %_60101
  %_60102 = bitcast i8* %_40003 to i32 (i8*)*
  %_40004 = call i32 %_60102(i8* dereferenceable_or_null(8) %_40001)
  %_60035 = icmp ne i8* %_2, null
  br i1 %_60035, label %_60034.0, label %_60012.0
_60034.0:
  %_60103 = bitcast i8* %_2 to i8**
  %_60036 = load i8*, i8** %_60103
  %_60104 = bitcast i8* %_60036 to { i8*, i32, i32, i8* }*
  %_60105 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60104, i32 0, i32 2
  %_60037 = bitcast i32* %_60105 to i8*
  %_60106 = bitcast i8* %_60037 to i32*
  %_60038 = load i32, i32* %_60106
  %_60107 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_60108 = getelementptr i8*, i8** %_60107, i32 1988
  %_60039 = bitcast i8** %_60108 to i8*
  %_60109 = bitcast i8* %_60039 to i8**
  %_60110 = getelementptr i8*, i8** %_60109, i32 %_60038
  %_60040 = bitcast i8** %_60110 to i8*
  %_60111 = bitcast i8* %_60040 to i8**
  %_40006 = load i8*, i8** %_60111
  %_60112 = bitcast i8* %_40006 to i8* (i8*, i32)*
  %_40007 = call dereferenceable_or_null(8) i8* %_60112(i8* dereferenceable_or_null(8) %_2, i32 %_40004)
  %_60042 = icmp ne i8* %_1, null
  br i1 %_60042, label %_60041.0, label %_60012.0
_60041.0:
  %_60113 = bitcast i8* %_1 to i8**
  %_60043 = load i8*, i8** %_60113
  %_60114 = bitcast i8* %_60043 to { i8*, i32, i32, i8* }*
  %_60115 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60114, i32 0, i32 2
  %_60044 = bitcast i32* %_60115 to i8*
  %_60116 = bitcast i8* %_60044 to i32*
  %_60045 = load i32, i32* %_60116
  %_60117 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_60118 = getelementptr i8*, i8** %_60117, i32 1814
  %_60046 = bitcast i8** %_60118 to i8*
  %_60119 = bitcast i8* %_60046 to i8**
  %_60120 = getelementptr i8*, i8** %_60119, i32 %_60045
  %_60047 = bitcast i8** %_60120 to i8*
  %_60121 = bitcast i8* %_60047 to i8**
  %_40009 = load i8*, i8** %_60121
  %_60122 = bitcast i8* %_40009 to i32 (i8*, i8*, i32)*
  %_40010 = call i32 %_60122(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_40007, i32 0)
  br label %_60000.0
_50000.0:
  %_60123 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM38scala.collection.mutable.ArrayBuilder$D4makeL22scala.reflect.ClassTagL37scala.collection.mutable.ArrayBuilderEO" to i8*) to i8* (i8*, i8*)*
  %_50002 = call dereferenceable_or_null(16) i8* %_60123(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM38scala.collection.mutable.ArrayBuilder$G8instance" to i8*), i8* dereferenceable_or_null(8) %_2)
  %_60050 = icmp eq i8* %_1, null
  br i1 %_60050, label %_60049.0, label %_60048.0
_60048.0:
  %_60124 = bitcast i8* %_1 to i8**
  %_60051 = load i8*, i8** %_60124
  %_60125 = bitcast i8* %_60051 to { i8*, i32, i32, i8* }*
  %_60126 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60125, i32 0, i32 1
  %_60052 = bitcast i32* %_60126 to i8*
  %_60127 = bitcast i8* %_60052 to i32*
  %_60053 = load i32, i32* %_60127
  %_60128 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_60129 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_60128, i32 0, i32 %_60053, i32 57
  %_60054 = bitcast i1* %_60129 to i8*
  %_60130 = bitcast i8* %_60054 to i1*
  %_60055 = load i1, i1* %_60130
  br i1 %_60055, label %_60049.0, label %_60004.0
_60049.0:
  %_50003 = bitcast i8* %_1 to i8*
  %_60057 = icmp ne i8* %_50002, null
  br i1 %_60057, label %_60056.0, label %_60012.0
_60056.0:
  %_60131 = bitcast i8* %_50002 to i8**
  %_60058 = load i8*, i8** %_60131
  %_60132 = bitcast i8* %_60058 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }*
  %_60133 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* %_60132, i32 0, i32 4, i32 7
  %_60059 = bitcast i8** %_60133 to i8*
  %_60134 = bitcast i8* %_60059 to i8**
  %_50005 = load i8*, i8** %_60134
  %_60135 = bitcast i8* %_50005 to i8* (i8*, i8*)*
  %_50006 = call dereferenceable_or_null(16) i8* %_60135(i8* dereferenceable_or_null(16) %_50002, i8* dereferenceable_or_null(8) %_50003)
  %_60061 = icmp ne i8* %_50006, null
  br i1 %_60061, label %_60060.0, label %_60012.0
_60060.0:
  %_60136 = bitcast i8* %_50006 to i8**
  %_60062 = load i8*, i8** %_60136
  %_60137 = bitcast i8* %_60062 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }*
  %_60138 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* %_60137, i32 0, i32 4, i32 6
  %_60063 = bitcast i8** %_60138 to i8*
  %_60139 = bitcast i8* %_60063 to i8**
  %_50008 = load i8*, i8** %_60139
  %_60140 = bitcast i8* %_50008 to i8* (i8*)*
  %_50009 = call dereferenceable_or_null(8) i8* %_60140(i8* dereferenceable_or_null(16) %_50006)
  br label %_60000.0
_60000.0:
  %_60001 = phi i8* [%_50009, %_60060.0], [%_40007, %_60041.0]
  ret i8* %_60001
_60012.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_60004.0:
  %_60065 = phi i8* [%_1, %_60002.0], [%_1, %_60048.0], [%_1, %_60019.0]
  %_60066 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM29scala.collection.IterableOnceG4type" to i8*), %_60002.0], [bitcast ({ i8*, i32, i32, i8* }* @"_SM29scala.collection.IterableOnceG4type" to i8*), %_60048.0], [bitcast ({ i8*, i32, i32, i8* }* @"_SM29scala.collection.IterableOnceG4type" to i8*), %_60019.0]
  %_60141 = bitcast i8* %_60065 to i8**
  %_60067 = load i8*, i8** %_60141
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_60067, i8* %_60066)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM32scala.collection.IterableOnceOpsD8mkStringL16java.lang.StringL16java.lang.StringL16java.lang.StringL16java.lang.StringEO"(i8* %_1, i8* %_2, i8* %_3, i8* %_4) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_240013 = icmp ne i8* %_1, null
  br i1 %_240013, label %_240011.0, label %_240012.0
_240011.0:
  %_240051 = bitcast i8* %_1 to i8**
  %_240014 = load i8*, i8** %_240051
  %_240052 = bitcast i8* %_240014 to { i8*, i32, i32, i8* }*
  %_240053 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_240052, i32 0, i32 2
  %_240015 = bitcast i32* %_240053 to i8*
  %_240054 = bitcast i8* %_240015 to i32*
  %_240016 = load i32, i32* %_240054
  %_240055 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_240056 = getelementptr i8*, i8** %_240055, i32 223
  %_240017 = bitcast i8** %_240056 to i8*
  %_240057 = bitcast i8* %_240017 to i8**
  %_240058 = getelementptr i8*, i8** %_240057, i32 %_240016
  %_240018 = bitcast i8** %_240058 to i8*
  %_240059 = bitcast i8* %_240018 to i8**
  %_50002 = load i8*, i8** %_240059
  %_240060 = bitcast i8* %_50002 to i1 (i8*)*
  %_50003 = call i1 %_240060(i8* dereferenceable_or_null(8) %_1)
  br i1 %_50003, label %_60000.0, label %_70000.0
_60000.0:
  %_60002 = icmp eq i8* %_2, null
  br i1 %_60002, label %_80000.0, label %_90000.0
_80000.0:
  br label %_100000.0
_90000.0:
  br label %_100000.0
_100000.0:
  %_100001 = phi i8* [%_2, %_90000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_80000.0]
  %_100003 = icmp eq i8* %_4, null
  br i1 %_100003, label %_110000.0, label %_120000.0
_110000.0:
  br label %_130000.0
_120000.0:
  br label %_130000.0
_130000.0:
  %_130001 = phi i8* [%_4, %_120000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_110000.0]
  %_240061 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_130002 = call dereferenceable_or_null(32) i8* %_240061(i8* dereferenceable_or_null(32) %_100001, i8* dereferenceable_or_null(32) %_130001)
  br label %_140000.0
_70000.0:
  %_240001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM38scala.collection.mutable.StringBuilderG4type" to i8*), i64 16)
  %_240002 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM23java.lang.StringBuilderG4type" to i8*), i64 24)
  %_240003 = call dereferenceable_or_null(8) i8* @"_SM36scala.scalanative.runtime.CharArray$D5allociL35scala.scalanative.runtime.CharArrayEO"(i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.CharArray$G8instance" to i8*), i32 16)
  %_240062 = bitcast i8* %_240002 to { i8*, i1, i32, i8* }*
  %_240063 = getelementptr { i8*, i1, i32, i8* }, { i8*, i1, i32, i8* }* %_240062, i32 0, i32 3
  %_240021 = bitcast i8** %_240063 to i8*
  %_240064 = bitcast i8* %_240021 to i8**
  store i8* %_240003, i8** %_240064
  %_240065 = bitcast i8* %_240001 to { i8*, i8* }*
  %_240066 = getelementptr { i8*, i8* }, { i8*, i8* }* %_240065, i32 0, i32 1
  %_240023 = bitcast i8** %_240066 to i8*
  %_240067 = bitcast i8* %_240023 to i8**
  store i8* %_240002, i8** %_240067
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM33scala.collection.mutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM34scala.collection.mutable.CloneableD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM31scala.collection.mutable.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.mutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM33scala.collection.mutable.GrowableD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.BuilderD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.mutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.mutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  %_240044 = icmp ne i8* %_1, null
  br i1 %_240044, label %_240043.0, label %_240012.0
_240043.0:
  %_240068 = bitcast i8* %_1 to i8**
  %_240045 = load i8*, i8** %_240068
  %_240069 = bitcast i8* %_240045 to { i8*, i32, i32, i8* }*
  %_240070 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_240069, i32 0, i32 2
  %_240046 = bitcast i32* %_240070 to i8*
  %_240071 = bitcast i8* %_240046 to i32*
  %_240047 = load i32, i32* %_240071
  %_240072 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_240073 = getelementptr i8*, i8** %_240072, i32 3034
  %_240048 = bitcast i8** %_240073 to i8*
  %_240074 = bitcast i8* %_240048 to i8**
  %_240075 = getelementptr i8*, i8** %_240074, i32 %_240047
  %_240049 = bitcast i8** %_240075 to i8*
  %_240076 = bitcast i8* %_240049 to i8**
  %_70003 = load i8*, i8** %_240076
  %_240077 = bitcast i8* %_70003 to i8* (i8*, i8*, i8*, i8*, i8*)*
  %_70004 = call dereferenceable_or_null(16) i8* %_240077(i8* dereferenceable_or_null(8) %_1, i8* nonnull dereferenceable(16) %_240001, i8* dereferenceable_or_null(32) %_2, i8* dereferenceable_or_null(32) %_3, i8* dereferenceable_or_null(32) %_4)
  %_70005 = call dereferenceable_or_null(32) i8* @"_SM38scala.collection.mutable.StringBuilderD6resultL16java.lang.StringEO"(i8* dereferenceable_or_null(16) %_70004)
  br label %_140000.0
_140000.0:
  %_140001 = phi i8* [%_70005, %_240043.0], [%_130002, %_130000.0]
  ret i8* %_140001
_240012.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM32scala.collection.IterableOnceOpsD8nonEmptyzEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20008 = icmp ne i8* %_1, null
  br i1 %_20008, label %_20006.0, label %_20007.0
_20006.0:
  %_20015 = bitcast i8* %_1 to i8**
  %_20009 = load i8*, i8** %_20015
  %_20016 = bitcast i8* %_20009 to { i8*, i32, i32, i8* }*
  %_20017 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_20016, i32 0, i32 2
  %_20010 = bitcast i32* %_20017 to i8*
  %_20018 = bitcast i8* %_20010 to i32*
  %_20011 = load i32, i32* %_20018
  %_20019 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_20020 = getelementptr i8*, i8** %_20019, i32 223
  %_20012 = bitcast i8** %_20020 to i8*
  %_20021 = bitcast i8* %_20012 to i8**
  %_20022 = getelementptr i8*, i8** %_20021, i32 %_20011
  %_20013 = bitcast i8** %_20022 to i8*
  %_20023 = bitcast i8* %_20013 to i8**
  %_20002 = load i8*, i8** %_20023
  %_20024 = bitcast i8* %_20002 to i1 (i8*)*
  %_20003 = call i1 %_20024(i8* dereferenceable_or_null(8) %_1)
  %_20005 = xor i1 %_20003, true
  ret i1 %_20005
_20007.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM32scala.collection.IterableOnceOpsD9addStringL38scala.collection.mutable.StringBuilderL16java.lang.StringL16java.lang.StringL16java.lang.StringL38scala.collection.mutable.StringBuilderEO"(i8* %_1, i8* %_2, i8* %_3, i8* %_4, i8* %_5) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  %_200004 = icmp ne i8* %_2, null
  br i1 %_200004, label %_200002.0, label %_200003.0
_200002.0:
  %_200041 = bitcast i8* %_2 to { i8*, i8* }*
  %_200042 = getelementptr { i8*, i8* }, { i8*, i8* }* %_200041, i32 0, i32 1
  %_200005 = bitcast i8** %_200042 to i8*
  %_200043 = bitcast i8* %_200005 to i8**
  %_70001 = load i8*, i8** %_200043, !dereferenceable_or_null !{i64 24}
  %_60001 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* dereferenceable_or_null(32) %_3)
  %_60003 = icmp ne i32 %_60001, 0
  br i1 %_60003, label %_80000.0, label %_90000.0
_80000.0:
  %_80001 = call dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.StringL23java.lang.StringBuilderEO"(i8* dereferenceable_or_null(24) %_70001, i8* dereferenceable_or_null(32) %_3)
  br label %_100000.0
_90000.0:
  br label %_100000.0
_100000.0:
  %_100001 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_90000.0], [%_80001, %_80000.0]
  %_200009 = icmp eq i8* %_1, null
  br i1 %_200009, label %_200007.0, label %_200006.0
_200006.0:
  %_200044 = bitcast i8* %_1 to i8**
  %_200010 = load i8*, i8** %_200044
  %_200045 = bitcast i8* %_200010 to { i8*, i32, i32, i8* }*
  %_200046 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_200045, i32 0, i32 1
  %_200011 = bitcast i32* %_200046 to i8*
  %_200047 = bitcast i8* %_200011 to i32*
  %_200012 = load i32, i32* %_200047
  %_200048 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_200049 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_200048, i32 0, i32 %_200012, i32 57
  %_200013 = bitcast i1* %_200049 to i8*
  %_200050 = bitcast i8* %_200013 to i1*
  %_200014 = load i1, i1* %_200050
  br i1 %_200014, label %_200007.0, label %_200008.0
_200007.0:
  %_100002 = bitcast i8* %_1 to i8*
  %_200016 = icmp ne i8* %_100002, null
  br i1 %_200016, label %_200015.0, label %_200003.0
_200015.0:
  %_200051 = bitcast i8* %_100002 to i8**
  %_200017 = load i8*, i8** %_200051
  %_200052 = bitcast i8* %_200017 to { i8*, i32, i32, i8* }*
  %_200053 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_200052, i32 0, i32 2
  %_200018 = bitcast i32* %_200053 to i8*
  %_200054 = bitcast i8* %_200018 to i32*
  %_200019 = load i32, i32* %_200054
  %_200055 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_200056 = getelementptr i8*, i8** %_200055, i32 446
  %_200020 = bitcast i8** %_200056 to i8*
  %_200057 = bitcast i8* %_200020 to i8**
  %_200058 = getelementptr i8*, i8** %_200057, i32 %_200019
  %_200021 = bitcast i8** %_200058 to i8*
  %_200059 = bitcast i8* %_200021 to i8**
  %_100004 = load i8*, i8** %_200059
  %_200060 = bitcast i8* %_100004 to i8* (i8*)*
  %_100005 = call dereferenceable_or_null(8) i8* %_200060(i8* dereferenceable_or_null(8) %_100002)
  %_200023 = icmp ne i8* %_100005, null
  br i1 %_200023, label %_200022.0, label %_200003.0
_200022.0:
  %_200061 = bitcast i8* %_100005 to i8**
  %_200024 = load i8*, i8** %_200061
  %_200062 = bitcast i8* %_200024 to { i8*, i32, i32, i8* }*
  %_200063 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_200062, i32 0, i32 2
  %_200025 = bitcast i32* %_200063 to i8*
  %_200064 = bitcast i8* %_200025 to i32*
  %_200026 = load i32, i32* %_200064
  %_200065 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_200066 = getelementptr i8*, i8** %_200065, i32 4359
  %_200027 = bitcast i8** %_200066 to i8*
  %_200067 = bitcast i8* %_200027 to i8**
  %_200068 = getelementptr i8*, i8** %_200067, i32 %_200026
  %_200028 = bitcast i8** %_200068 to i8*
  %_200069 = bitcast i8* %_200028 to i8**
  %_100007 = load i8*, i8** %_200069
  %_200070 = bitcast i8* %_100007 to i1 (i8*)*
  %_100008 = call i1 %_200070(i8* dereferenceable_or_null(8) %_100005)
  br i1 %_100008, label %_110000.0, label %_120000.0
_110000.0:
  %_200030 = icmp ne i8* %_100005, null
  br i1 %_200030, label %_200029.0, label %_200003.0
_200029.0:
  %_200071 = bitcast i8* %_100005 to i8**
  %_200031 = load i8*, i8** %_200071
  %_200072 = bitcast i8* %_200031 to { i8*, i32, i32, i8* }*
  %_200073 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_200072, i32 0, i32 2
  %_200032 = bitcast i32* %_200073 to i8*
  %_200074 = bitcast i8* %_200032 to i32*
  %_200033 = load i32, i32* %_200074
  %_200075 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_200076 = getelementptr i8*, i8** %_200075, i32 4219
  %_200034 = bitcast i8** %_200076 to i8*
  %_200077 = bitcast i8* %_200034 to i8**
  %_200078 = getelementptr i8*, i8** %_200077, i32 %_200033
  %_200035 = bitcast i8** %_200078 to i8*
  %_200079 = bitcast i8* %_200035 to i8**
  %_110002 = load i8*, i8** %_200079
  %_200080 = bitcast i8* %_110002 to i8* (i8*)*
  %_110003 = call dereferenceable_or_null(8) i8* %_200080(i8* dereferenceable_or_null(8) %_100005)
  %_110004 = call dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.ObjectL23java.lang.StringBuilderEO"(i8* dereferenceable_or_null(24) %_70001, i8* dereferenceable_or_null(8) %_110003)
  br label %_130000.0
_130000.0:
  %_200081 = bitcast i8* %_100007 to i1 (i8*)*
  %_130001 = call i1 %_200081(i8* dereferenceable_or_null(8) %_100005)
  br i1 %_130001, label %_140000.0, label %_150000.0
_140000.0:
  %_140001 = call dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.StringL23java.lang.StringBuilderEO"(i8* dereferenceable_or_null(24) %_70001, i8* dereferenceable_or_null(32) %_4)
  %_200082 = bitcast i8* %_110002 to i8* (i8*)*
  %_140002 = call dereferenceable_or_null(8) i8* %_200082(i8* dereferenceable_or_null(8) %_100005)
  %_140003 = call dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.ObjectL23java.lang.StringBuilderEO"(i8* dereferenceable_or_null(24) %_70001, i8* dereferenceable_or_null(8) %_140002)
  br label %_130000.0
_150000.0:
  br label %_160000.0
_160000.0:
  br label %_170000.0
_120000.0:
  br label %_170000.0
_170000.0:
  %_170001 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* dereferenceable_or_null(32) %_5)
  %_170003 = icmp ne i32 %_170001, 0
  br i1 %_170003, label %_180000.0, label %_190000.0
_180000.0:
  %_180001 = call dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.StringL23java.lang.StringBuilderEO"(i8* dereferenceable_or_null(24) %_70001, i8* dereferenceable_or_null(32) %_5)
  br label %_200000.0
_190000.0:
  br label %_200000.0
_200000.0:
  %_200001 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_190000.0], [%_180001, %_180000.0]
  ret i8* %_2
_200003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_200008.0:
  %_200037 = phi i8* [%_1, %_200006.0]
  %_200038 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM29scala.collection.IterableOnceG4type" to i8*), %_200006.0]
  %_200083 = bitcast i8* %_200037 to i8**
  %_200039 = load i8*, i8** %_200083
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_200039, i8* %_200038)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM33java.util.FormatterImpl$$Lambda$1D12apply$mcV$spuEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20006 = icmp ne i8* %_1, null
  br i1 %_20006, label %_20004.0, label %_20005.0
_20004.0:
  %_20013 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_20014 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_20013, i32 0, i32 1
  %_20007 = bitcast i8** %_20014 to i8*
  %_20015 = bitcast i8* %_20007 to i8**
  %_20001 = load i8*, i8** %_20015, !dereferenceable_or_null !{i64 40}
  %_20009 = icmp ne i8* %_1, null
  br i1 %_20009, label %_20008.0, label %_20005.0
_20008.0:
  %_20016 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_20017 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_20016, i32 0, i32 2
  %_20010 = bitcast i8** %_20017 to i8*
  %_20018 = bitcast i8* %_20010 to i8**
  %_20002 = load i8*, i8** %_20018, !dereferenceable_or_null !{i64 8}
  call nonnull dereferenceable(8) i8* @"_SM23java.util.FormatterImplD21sendToDest$$anonfun$1L30scala.collection.immutable.SequEPT23java.util.FormatterImpl"(i8* dereferenceable_or_null(40) %_20001, i8* dereferenceable_or_null(8) %_20002)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_20005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM33scala.concurrent.ExecutionContextD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(16) i8* @"_SM34scala.scalanative.runtime.IntArrayD5applyiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30007 = icmp ne i8* %_1, null
  br i1 %_30007, label %_30005.0, label %_30006.0
_30005.0:
  %_30018 = bitcast i8* %_1 to { i8*, i32 }*
  %_30019 = getelementptr { i8*, i32 }, { i8*, i32 }* %_30018, i32 0, i32 1
  %_30008 = bitcast i32* %_30019 to i8*
  %_30020 = bitcast i8* %_30008 to i32*
  %_30004 = load i32, i32* %_30020
  %_30011 = icmp sge i32 %_2, 0
  %_30012 = icmp slt i32 %_2, %_30004
  %_30013 = and i1 %_30011, %_30012
  br i1 %_30013, label %_30009.0, label %_30010.0
_30009.0:
  %_30021 = bitcast i8* %_1 to { i8*, i32, i32, [0 x i32] }*
  %_30022 = getelementptr { i8*, i32, i32, [0 x i32] }, { i8*, i32, i32, [0 x i32] }* %_30021, i32 0, i32 3, i32 %_2
  %_30014 = bitcast i32* %_30022 to i8*
  %_30023 = bitcast i8* %_30014 to i32*
  %_30001 = load i32, i32* %_30023
  %_30003 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_30001)
  ret i8* %_30003
_30006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_30010.0:
  %_30016 = phi i32 [%_2, %_30005.0]
  %_30024 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_30024(i8* null, i32 %_30016)
  unreachable
}

define i32 @"_SM34scala.scalanative.runtime.IntArrayD5applyiiEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp slt i32 %_2, 0
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  %_90003 = icmp ne i8* %_1, null
  br i1 %_90003, label %_90001.0, label %_90002.0
_90001.0:
  %_90011 = bitcast i8* %_1 to { i8*, i32 }*
  %_90012 = getelementptr { i8*, i32 }, { i8*, i32 }* %_90011, i32 0, i32 1
  %_90004 = bitcast i32* %_90012 to i8*
  %_90013 = bitcast i8* %_90004 to i32*
  %_50001 = load i32, i32* %_90013
  %_50003 = icmp sge i32 %_2, %_50001
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_50003, %_90001.0], [true, %_40000.0]
  br i1 %_60001, label %_70000.0, label %_80000.0
_80000.0:
  %_90005 = and i32 2, 31
  %_80005 = shl i32 %_2, %_90005
  %_80006 = add i32 %_80005, 16
  %_80007 = call i64 @"_SM10scala.Int$D8int2longijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM10scala.Int$G8instance" to i8*), i32 %_80006)
  %_80009 = bitcast i8* %_1 to i8*
  %_90014 = bitcast i8* %_80009 to i8*
  %_90015 = getelementptr i8, i8* %_90014, i64 %_80007
  %_80010 = bitcast i8* %_90015 to i8*
  %_90016 = bitcast i8* %_80010 to i32*
  %_80011 = load i32, i32* %_90016
  br label %_90000.0
_90000.0:
  ret i32 %_80011
_70000.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.runtime.package$G8instance" to i8*), i32 %_2)
  br label %_90007.0
_90002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_90007.0:
  %_90017 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_90017(i8* null)
  unreachable
}

define i8* @"_SM34scala.scalanative.runtime.IntArrayD5atRawiR_EO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp slt i32 %_2, 0
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  %_90005 = icmp ne i8* %_1, null
  br i1 %_90005, label %_90003.0, label %_90004.0
_90003.0:
  %_90013 = bitcast i8* %_1 to { i8*, i32 }*
  %_90014 = getelementptr { i8*, i32 }, { i8*, i32 }* %_90013, i32 0, i32 1
  %_90006 = bitcast i32* %_90014 to i8*
  %_90015 = bitcast i8* %_90006 to i32*
  %_50001 = load i32, i32* %_90015
  %_50003 = icmp sge i32 %_2, %_50001
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_50003, %_90003.0], [true, %_40000.0]
  br i1 %_60001, label %_70000.0, label %_80000.0
_80000.0:
  %_90007 = and i32 2, 31
  %_80005 = shl i32 %_2, %_90007
  %_80006 = add i32 %_80005, 16
  %_80007 = call i64 @"_SM10scala.Int$D8int2longijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM10scala.Int$G8instance" to i8*), i32 %_80006)
  br label %_90000.0
_90000.0:
  %_90001 = bitcast i8* %_1 to i8*
  %_90016 = bitcast i8* %_90001 to i8*
  %_90017 = getelementptr i8, i8* %_90016, i64 %_80007
  %_90002 = bitcast i8* %_90017 to i8*
  ret i8* %_90002
_70000.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.runtime.package$G8instance" to i8*), i32 %_2)
  br label %_90009.0
_90004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_90009.0:
  %_90018 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_90018(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM34scala.scalanative.runtime.IntArrayD6strideL32scala.scalanative.unsigned.ULongEO"(i8* %_1) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20003 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 4)
  %_20004 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_20003)
  ret i8* %_20004
}

define nonnull dereferenceable(8) i8* @"_SM34scala.scalanative.runtime.IntArrayD6updateiL16java.lang.ObjectuEO"(i8* %_1, i32 %_2, i8* %_3) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40018 = bitcast i8* bitcast (i32 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D10unboxToIntL16java.lang.ObjectiEO" to i8*) to i32 (i8*, i8*)*
  %_40001 = call i32 %_40018(i8* null, i8* dereferenceable_or_null(8) %_3)
  %_40007 = icmp ne i8* %_1, null
  br i1 %_40007, label %_40005.0, label %_40006.0
_40005.0:
  %_40019 = bitcast i8* %_1 to { i8*, i32 }*
  %_40020 = getelementptr { i8*, i32 }, { i8*, i32 }* %_40019, i32 0, i32 1
  %_40008 = bitcast i32* %_40020 to i8*
  %_40021 = bitcast i8* %_40008 to i32*
  %_40004 = load i32, i32* %_40021
  %_40011 = icmp sge i32 %_2, 0
  %_40012 = icmp slt i32 %_2, %_40004
  %_40013 = and i1 %_40011, %_40012
  br i1 %_40013, label %_40009.0, label %_40010.0
_40009.0:
  %_40022 = bitcast i8* %_1 to { i8*, i32, i32, [0 x i32] }*
  %_40023 = getelementptr { i8*, i32, i32, [0 x i32] }, { i8*, i32, i32, [0 x i32] }* %_40022, i32 0, i32 3, i32 %_2
  %_40014 = bitcast i32* %_40023 to i8*
  %_40024 = bitcast i8* %_40014 to i32*
  store i32 %_40001, i32* %_40024
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_40006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_40010.0:
  %_40016 = phi i32 [%_2, %_40005.0]
  %_40025 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_40025(i8* null, i32 %_40016)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM34scala.scalanative.runtime.IntArrayD6updateiiuEO"(i8* %_1, i32 %_2, i32 %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40002 = icmp slt i32 %_2, 0
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  br label %_70000.0
_60000.0:
  %_100003 = icmp ne i8* %_1, null
  br i1 %_100003, label %_100001.0, label %_100002.0
_100001.0:
  %_100012 = bitcast i8* %_1 to { i8*, i32 }*
  %_100013 = getelementptr { i8*, i32 }, { i8*, i32 }* %_100012, i32 0, i32 1
  %_100004 = bitcast i32* %_100013 to i8*
  %_100014 = bitcast i8* %_100004 to i32*
  %_60001 = load i32, i32* %_100014
  %_60003 = icmp sge i32 %_2, %_60001
  br label %_70000.0
_70000.0:
  %_70001 = phi i1 [%_60003, %_100001.0], [true, %_50000.0]
  br i1 %_70001, label %_80000.0, label %_90000.0
_90000.0:
  %_100005 = and i32 2, 31
  %_90005 = shl i32 %_2, %_100005
  %_90006 = add i32 %_90005, 16
  %_90007 = call i64 @"_SM10scala.Int$D8int2longijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM10scala.Int$G8instance" to i8*), i32 %_90006)
  %_90009 = bitcast i8* %_1 to i8*
  %_100015 = bitcast i8* %_90009 to i8*
  %_100016 = getelementptr i8, i8* %_100015, i64 %_90007
  %_90010 = bitcast i8* %_100016 to i8*
  %_100017 = bitcast i8* %_90010 to i32*
  store i32 %_3, i32* %_100017
  br label %_100000.0
_100000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_80000.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.runtime.package$G8instance" to i8*), i32 %_2)
  br label %_100008.0
_100002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_100008.0:
  %_100018 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_100018(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM34scala.scalanative.runtime.Monitor$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D6IGNOREL34java.nio.charset.CodingErrorActionEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"()
  %_20004 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8* }*
  %_20005 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_20004, i32 0, i32 1
  %_20003 = bitcast i8** %_20005 to i8*
  %_20006 = bitcast i8* %_20003 to i8**
  %_20002 = load i8*, i8** %_20006, !dereferenceable_or_null !{i64 16}
  ret i8* %_20002
}

define dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D6REPORTL34java.nio.charset.CodingErrorActionEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"()
  %_20004 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8* }*
  %_20005 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_20004, i32 0, i32 2
  %_20003 = bitcast i8** %_20005 to i8*
  %_20006 = bitcast i8* %_20003 to i8**
  %_20002 = load i8*, i8** %_20006, !dereferenceable_or_null !{i64 16}
  ret i8* %_20002
}

define dereferenceable_or_null(16) i8* @"_SM35java.nio.charset.CodingErrorAction$D7REPLACEL34java.nio.charset.CodingErrorActionEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"()
  %_20004 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8* }*
  %_20005 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_20004, i32 0, i32 3
  %_20003 = bitcast i8** %_20005 to i8*
  %_20006 = bitcast i8* %_20003 to i8**
  %_20002 = load i8*, i8** %_20006, !dereferenceable_or_null !{i64 16}
  ret i8* %_20002
}

define dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 121
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 32}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM35java.nio.charset.CodingErrorAction$G4type" to i8*), i64 32)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM35java.nio.charset.CodingErrorAction$RE"(i8* dereferenceable_or_null(32) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM35java.nio.charset.CodingErrorAction$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM35java.nio.charset.CodingErrorAction$G4load"()
  %_20005 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM34java.nio.charset.CodingErrorActionG4type" to i8*), i64 16)
  %_20032 = bitcast i8* %_20005 to { i8*, i8* }*
  %_20033 = getelementptr { i8*, i8* }, { i8*, i8* }* %_20032, i32 0, i32 1
  %_20021 = bitcast i8** %_20033 to i8*
  %_20034 = bitcast i8* %_20021 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-211" to i8*), i8** %_20034
  %_20035 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8* }*
  %_20036 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_20035, i32 0, i32 1
  %_20023 = bitcast i8** %_20036 to i8*
  %_20037 = bitcast i8* %_20023 to i8**
  store i8* %_20005, i8** %_20037
  %_20011 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM34java.nio.charset.CodingErrorActionG4type" to i8*), i64 16)
  %_20038 = bitcast i8* %_20011 to { i8*, i8* }*
  %_20039 = getelementptr { i8*, i8* }, { i8*, i8* }* %_20038, i32 0, i32 1
  %_20025 = bitcast i8** %_20039 to i8*
  %_20040 = bitcast i8* %_20025 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-213" to i8*), i8** %_20040
  %_20041 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8* }*
  %_20042 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_20041, i32 0, i32 3
  %_20027 = bitcast i8** %_20042 to i8*
  %_20043 = bitcast i8* %_20027 to i8**
  store i8* %_20011, i8** %_20043
  %_20017 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM34java.nio.charset.CodingErrorActionG4type" to i8*), i64 16)
  %_20044 = bitcast i8* %_20017 to { i8*, i8* }*
  %_20045 = getelementptr { i8*, i8* }, { i8*, i8* }* %_20044, i32 0, i32 1
  %_20029 = bitcast i8** %_20045 to i8*
  %_20046 = bitcast i8* %_20029 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-215" to i8*), i8** %_20046
  %_20047 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8* }*
  %_20048 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_20047, i32 0, i32 2
  %_20031 = bitcast i8** %_20048 to i8*
  %_20049 = bitcast i8* %_20031 to i8**
  store i8* %_20017, i8** %_20049
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM35java.util.FormatterImpl$FormatTokenD7setFlagczEO"(i8* %_1, i16 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_40002 = zext i16 %_2 to i32
  switch i32 %_40002, label %_50000.0 [
    i32 45, label %_60000.0
    i32 35, label %_70000.0
    i32 43, label %_80000.0
    i32 32, label %_90000.0
    i32 48, label %_100000.0
    i32 44, label %_110000.0
    i32 40, label %_120000.0
    i32 60, label %_130000.0
  ]
_50000.0:
  ret i1 false
_60000.0:
  br label %_140000.0
_70000.0:
  br label %_140000.0
_80000.0:
  br label %_140000.0
_90000.0:
  br label %_140000.0
_100000.0:
  br label %_140000.0
_110000.0:
  br label %_140000.0
_120000.0:
  br label %_140000.0
_130000.0:
  br label %_140000.0
_140000.0:
  %_140001 = phi i32 [128, %_130000.0], [64, %_120000.0], [32, %_110000.0], [16, %_100000.0], [8, %_90000.0], [4, %_80000.0], [2, %_70000.0], [1, %_60000.0]
  %_440007 = icmp ne i8* %_1, null
  br i1 %_440007, label %_440005.0, label %_440006.0
_440005.0:
  %_440031 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }*
  %_440032 = getelementptr { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }, { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }* %_440031, i32 0, i32 7
  %_440008 = bitcast i32* %_440032 to i8*
  %_440033 = bitcast i8* %_440008 to i32*
  %_140002 = load i32, i32* %_440033
  %_140005 = and i32 %_140002, %_140001
  %_140006 = icmp ne i32 %_140005, 0
  br i1 %_140006, label %_150000.0, label %_160000.0
_160000.0:
  br label %_440000.0
_440000.0:
  %_440010 = icmp ne i8* %_1, null
  br i1 %_440010, label %_440009.0, label %_440006.0
_440009.0:
  %_440034 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }*
  %_440035 = getelementptr { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }, { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }* %_440034, i32 0, i32 7
  %_440011 = bitcast i32* %_440035 to i8*
  %_440036 = bitcast i8* %_440011 to i32*
  %_440001 = load i32, i32* %_440036
  %_440003 = or i32 %_440001, %_140001
  %_440014 = icmp ne i8* %_1, null
  br i1 %_440014, label %_440013.0, label %_440006.0
_440013.0:
  %_440037 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }*
  %_440038 = getelementptr { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }, { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }* %_440037, i32 0, i32 7
  %_440015 = bitcast i32* %_440038 to i8*
  %_440039 = bitcast i8* %_440015 to i32*
  store i32 %_440003, i32* %_440039
  ret i1 true
_150000.0:
  %_150003 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8* null, i16 %_2)
  %_440040 = bitcast i8* bitcast (i8* (i8*)* @"_SM19java.lang.CharacterD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_150004 = call dereferenceable_or_null(32) i8* %_440040(i8* nonnull dereferenceable(16) %_150003)
  br label %_280000.0
_280000.0:
  %_280001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM39java.util.DuplicateFormatFlagsExceptionG4type" to i8*), i64 48)
  %_440041 = bitcast i8* %_280001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_440042 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_440041, i32 0, i32 5
  %_440017 = bitcast i1* %_440042 to i8*
  %_440043 = bitcast i8* %_440017 to i1*
  store i1 true, i1* %_440043
  %_440044 = bitcast i8* %_280001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_440045 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_440044, i32 0, i32 4
  %_440019 = bitcast i1* %_440045 to i8*
  %_440046 = bitcast i8* %_440019 to i1*
  store i1 true, i1* %_440046
  %_280004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(48) %_280001)
  br label %_290000.0
_290000.0:
  %_440047 = bitcast i8* %_280001 to { i8*, i8*, i8*, i8*, i1, i1, i8* }*
  %_440048 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8* }, { i8*, i8*, i8*, i8*, i1, i1, i8* }* %_440047, i32 0, i32 6
  %_440021 = bitcast i8** %_440048 to i8*
  %_440049 = bitcast i8* %_440021 to i8**
  store i8* null, i8** %_440049
  %_170002 = icmp eq i8* %_150004, null
  br i1 %_170002, label %_300000.0, label %_310000.0
_310000.0:
  br label %_430000.0
_300000.0:
  br label %_410000.0
_410000.0:
  %_410001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM30java.lang.NullPointerExceptionG4type" to i8*), i64 40)
  %_440050 = bitcast i8* %_410001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_440051 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_440050, i32 0, i32 5
  %_440023 = bitcast i1* %_440051 to i8*
  %_440052 = bitcast i8* %_440023 to i1*
  store i1 true, i1* %_440052
  %_440053 = bitcast i8* %_410001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_440054 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_440053, i32 0, i32 4
  %_440025 = bitcast i1* %_440054 to i8*
  %_440055 = bitcast i8* %_440025 to i1*
  store i1 true, i1* %_440055
  %_410004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_410001)
  br label %_420000.0
_420000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_410001)
  unreachable
_430000.0:
  %_440056 = bitcast i8* %_280001 to { i8*, i8*, i8*, i8*, i1, i1, i8* }*
  %_440057 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8* }, { i8*, i8*, i8*, i8*, i1, i1, i8* }* %_440056, i32 0, i32 6
  %_440028 = bitcast i8** %_440057 to i8*
  %_440058 = bitcast i8* %_440028 to i8**
  store i8* %_150004, i8** %_440058
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(48) %_280001)
  unreachable
_440006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM35java.util.FormatterImpl$FormatTokenD7setFlagiuEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30007 = icmp ne i8* %_1, null
  br i1 %_30007, label %_30005.0, label %_30006.0
_30005.0:
  %_30014 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }*
  %_30015 = getelementptr { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }, { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }* %_30014, i32 0, i32 7
  %_30008 = bitcast i32* %_30015 to i8*
  %_30016 = bitcast i8* %_30008 to i32*
  %_30001 = load i32, i32* %_30016
  %_30003 = or i32 %_30001, %_2
  %_30011 = icmp ne i8* %_1, null
  br i1 %_30011, label %_30010.0, label %_30006.0
_30010.0:
  %_30017 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }*
  %_30018 = getelementptr { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }, { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }* %_30017, i32 0, i32 7
  %_30012 = bitcast i32* %_30018 to i8*
  %_30019 = bitcast i8* %_30012 to i32*
  store i32 %_30003, i32* %_30019
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_30006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM35java.util.FormatterImpl$FormatTokenD8getFlagsiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }*
  %_20008 = getelementptr { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }, { i8*, i32, i32, i8*, i16, i32, i32, i32, i32 }* %_20007, i32 0, i32 7
  %_20005 = bitcast i32* %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i32*
  %_20001 = load i32, i32* %_20009
  ret i32 %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM35scala.collection.MapFactoryDefaultsD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(16) i8* @"_SM35scala.scalanative.unsafe.CFuncPtr6$D10fromRawPtrR_L34scala.scalanative.unsafe.CFuncPtr6EO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM34scala.scalanative.unsafe.CFuncPtr6G4type" to i8*), i64 16)
  %_30006 = bitcast i8* %_30002 to { i8*, i8* }*
  %_30007 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30006, i32 0, i32 1
  %_30005 = bitcast i8** %_30007 to i8*
  %_30008 = bitcast i8* %_30005 to i8**
  store i8* %_2, i8** %_30008
  ret i8* %_30002
}

define nonnull dereferenceable(8) i8* @"_SM35scala.scalanative.unsafe.CFuncPtr6$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(16) i8* @"_SM36scala.scalanative.unsafe.CFuncPtr14$D10fromRawPtrR_L35scala.scalanative.unsafe.CFuncPtr14EO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM35scala.scalanative.unsafe.CFuncPtr14G4type" to i8*), i64 16)
  %_30006 = bitcast i8* %_30002 to { i8*, i8* }*
  %_30007 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30006, i32 0, i32 1
  %_30005 = bitcast i8** %_30007 to i8*
  %_30008 = bitcast i8* %_30005 to i8**
  store i8* %_2, i8** %_30008
  ret i8* %_30002
}

define nonnull dereferenceable(8) i8* @"_SM36scala.scalanative.unsafe.CFuncPtr14$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(32) i8* @"_SM37java.util.IllegalFormatWidthExceptionD10getMessageL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i1, i1, i32 }*
  %_30008 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i32 }, { i8*, i8*, i8*, i8*, i1, i1, i32 }* %_30007, i32 0, i32 6
  %_30005 = bitcast i32* %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i32*
  %_30001 = load i32, i32* %_30009
  %_30010 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM18java.lang.Integer$D8toStringiL16java.lang.StringEO" to i8*) to i8* (i8*, i32)*
  %_20002 = call dereferenceable_or_null(32) i8* %_30010(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM18java.lang.Integer$G8instance" to i8*), i32 %_30001)
  ret i8* %_20002
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM37java.util.concurrent.TimeUnit$$anon$2D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM37java.util.concurrent.TimeUnit$$anon$2D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM37java.util.concurrent.TimeUnit$$anon$2D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define i1 @"_SM37scala.collection.immutable.IndexedSeqD12sameElementsL29scala.collection.IterableOncezEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_430007 = icmp eq i8* %_2, null
  br i1 %_430007, label %_430004.0, label %_430005.0
_430004.0:
  br label %_430006.0
_430005.0:
  %_430149 = bitcast i8* %_2 to i8**
  %_430008 = load i8*, i8** %_430149
  %_430150 = bitcast i8* %_430008 to { i8*, i32, i32, i8* }*
  %_430151 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430150, i32 0, i32 1
  %_430009 = bitcast i32* %_430151 to i8*
  %_430152 = bitcast i8* %_430009 to i32*
  %_430010 = load i32, i32* %_430152
  %_430153 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_430154 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_430153, i32 0, i32 %_430010, i32 92
  %_430011 = bitcast i1* %_430154 to i8*
  %_430155 = bitcast i8* %_430011 to i1*
  %_430012 = load i1, i1* %_430155
  br label %_430006.0
_430006.0:
  %_40002 = phi i1 [%_430012, %_430005.0], [false, %_430004.0]
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_430016 = icmp eq i8* %_2, null
  br i1 %_430016, label %_430014.0, label %_430013.0
_430013.0:
  %_430156 = bitcast i8* %_2 to i8**
  %_430017 = load i8*, i8** %_430156
  %_430157 = bitcast i8* %_430017 to { i8*, i32, i32, i8* }*
  %_430158 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430157, i32 0, i32 1
  %_430018 = bitcast i32* %_430158 to i8*
  %_430159 = bitcast i8* %_430018 to i32*
  %_430019 = load i32, i32* %_430159
  %_430160 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_430161 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_430160, i32 0, i32 %_430019, i32 92
  %_430020 = bitcast i1* %_430161 to i8*
  %_430162 = bitcast i8* %_430020 to i1*
  %_430021 = load i1, i1* %_430162
  br i1 %_430021, label %_430014.0, label %_430015.0
_430014.0:
  %_50001 = bitcast i8* %_2 to i8*
  %_50003 = icmp eq i8* %_1, %_50001
  br i1 %_50003, label %_70000.0, label %_80000.0
_70000.0:
  br label %_90000.0
_80000.0:
  %_430024 = icmp ne i8* %_1, null
  br i1 %_430024, label %_430022.0, label %_430023.0
_430022.0:
  %_430163 = bitcast i8* %_1 to i8**
  %_430025 = load i8*, i8** %_430163
  %_430164 = bitcast i8* %_430025 to { i8*, i32, i32, i8* }*
  %_430165 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430164, i32 0, i32 2
  %_430026 = bitcast i32* %_430165 to i8*
  %_430166 = bitcast i8* %_430026 to i32*
  %_430027 = load i32, i32* %_430166
  %_430167 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430168 = getelementptr i8*, i8** %_430167, i32 4009
  %_430028 = bitcast i8** %_430168 to i8*
  %_430169 = bitcast i8* %_430028 to i8**
  %_430170 = getelementptr i8*, i8** %_430169, i32 %_430027
  %_430029 = bitcast i8** %_430170 to i8*
  %_430171 = bitcast i8* %_430029 to i8**
  %_80002 = load i8*, i8** %_430171
  %_430172 = bitcast i8* %_80002 to i32 (i8*)*
  %_80003 = call i32 %_430172(i8* dereferenceable_or_null(8) %_1)
  %_430031 = icmp ne i8* %_50001, null
  br i1 %_430031, label %_430030.0, label %_430023.0
_430030.0:
  %_430173 = bitcast i8* %_50001 to i8**
  %_430032 = load i8*, i8** %_430173
  %_430174 = bitcast i8* %_430032 to { i8*, i32, i32, i8* }*
  %_430175 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430174, i32 0, i32 2
  %_430033 = bitcast i32* %_430175 to i8*
  %_430176 = bitcast i8* %_430033 to i32*
  %_430034 = load i32, i32* %_430176
  %_430177 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430178 = getelementptr i8*, i8** %_430177, i32 4009
  %_430035 = bitcast i8** %_430178 to i8*
  %_430179 = bitcast i8* %_430035 to i8**
  %_430180 = getelementptr i8*, i8** %_430179, i32 %_430034
  %_430036 = bitcast i8** %_430180 to i8*
  %_430181 = bitcast i8* %_430036 to i8**
  %_80005 = load i8*, i8** %_430181
  %_430182 = bitcast i8* %_80005 to i32 (i8*)*
  %_80006 = call i32 %_430182(i8* dereferenceable_or_null(8) %_50001)
  %_80008 = icmp eq i32 %_80003, %_80006
  br i1 %_80008, label %_100000.0, label %_110000.0
_100000.0:
  %_430038 = icmp ne i8* %_1, null
  br i1 %_430038, label %_430037.0, label %_430023.0
_430037.0:
  %_430183 = bitcast i8* %_1 to i8**
  %_430039 = load i8*, i8** %_430183
  %_430184 = bitcast i8* %_430039 to { i8*, i32, i32, i8* }*
  %_430185 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430184, i32 0, i32 2
  %_430040 = bitcast i32* %_430185 to i8*
  %_430186 = bitcast i8* %_430040 to i32*
  %_430041 = load i32, i32* %_430186
  %_430187 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430188 = getelementptr i8*, i8** %_430187, i32 1418
  %_430042 = bitcast i8** %_430188 to i8*
  %_430189 = bitcast i8* %_430042 to i8**
  %_430190 = getelementptr i8*, i8** %_430189, i32 %_430041
  %_430043 = bitcast i8** %_430190 to i8*
  %_430191 = bitcast i8* %_430043 to i8**
  %_100002 = load i8*, i8** %_430191
  %_430192 = bitcast i8* %_100002 to i32 (i8*)*
  %_100003 = call i32 %_430192(i8* dereferenceable_or_null(8) %_1)
  %_430045 = icmp ne i8* %_50001, null
  br i1 %_430045, label %_430044.0, label %_430023.0
_430044.0:
  %_430193 = bitcast i8* %_50001 to i8**
  %_430046 = load i8*, i8** %_430193
  %_430194 = bitcast i8* %_430046 to { i8*, i32, i32, i8* }*
  %_430195 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430194, i32 0, i32 2
  %_430047 = bitcast i32* %_430195 to i8*
  %_430196 = bitcast i8* %_430047 to i32*
  %_430048 = load i32, i32* %_430196
  %_430197 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430198 = getelementptr i8*, i8** %_430197, i32 1418
  %_430049 = bitcast i8** %_430198 to i8*
  %_430199 = bitcast i8* %_430049 to i8**
  %_430200 = getelementptr i8*, i8** %_430199, i32 %_430048
  %_430050 = bitcast i8** %_430200 to i8*
  %_430201 = bitcast i8* %_430050 to i8**
  %_100005 = load i8*, i8** %_430201
  %_430202 = bitcast i8* %_100005 to i32 (i8*)*
  %_100006 = call i32 %_430202(i8* dereferenceable_or_null(8) %_50001)
  %_100007 = call i32 @"_SM14java.lang.MathD3miniiiEo"(i32 %_100003, i32 %_100006)
  %_100012 = sext i32 %_80003 to i64
  %_100013 = sext i32 %_100007 to i64
  %_430051 = and i64 1, 63
  %_100014 = shl i64 %_100013, %_430051
  %_100015 = icmp sgt i64 %_100012, %_100014
  br i1 %_100015, label %_120000.0, label %_130000.0
_120000.0:
  br label %_140000.0
_130000.0:
  br label %_140000.0
_140000.0:
  %_140001 = phi i32 [%_80003, %_130000.0], [%_100007, %_120000.0]
  br label %_150000.0
_150000.0:
  %_150001 = phi i1 [%_80008, %_140000.0], [%_230001, %_230000.0]
  %_150002 = phi i32 [0, %_140000.0], [%_230003, %_230000.0]
  %_150004 = icmp slt i32 %_150002, %_140001
  br i1 %_150004, label %_160000.0, label %_170000.0
_160000.0:
  br label %_180000.0
_170000.0:
  br label %_180000.0
_180000.0:
  %_180001 = phi i1 [false, %_170000.0], [%_150001, %_160000.0]
  br i1 %_180001, label %_190000.0, label %_200000.0
_190000.0:
  %_430053 = icmp ne i8* %_1, null
  br i1 %_430053, label %_430052.0, label %_430023.0
_430052.0:
  %_430203 = bitcast i8* %_1 to i8**
  %_430054 = load i8*, i8** %_430203
  %_430204 = bitcast i8* %_430054 to { i8*, i32, i32, i8* }*
  %_430205 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430204, i32 0, i32 2
  %_430055 = bitcast i32* %_430205 to i8*
  %_430206 = bitcast i8* %_430055 to i32*
  %_430056 = load i32, i32* %_430206
  %_430207 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430208 = getelementptr i8*, i8** %_430207, i32 4209
  %_430057 = bitcast i8** %_430208 to i8*
  %_430209 = bitcast i8* %_430057 to i8**
  %_430210 = getelementptr i8*, i8** %_430209, i32 %_430056
  %_430058 = bitcast i8** %_430210 to i8*
  %_430211 = bitcast i8* %_430058 to i8**
  %_190002 = load i8*, i8** %_430211
  %_430212 = bitcast i8* %_190002 to i8* (i8*, i32)*
  %_190003 = call dereferenceable_or_null(8) i8* %_430212(i8* dereferenceable_or_null(8) %_1, i32 %_150002)
  %_190005 = icmp eq i8* %_190003, null
  br i1 %_190005, label %_210000.0, label %_220000.0
_210000.0:
  %_430060 = icmp ne i8* %_50001, null
  br i1 %_430060, label %_430059.0, label %_430023.0
_430059.0:
  %_430213 = bitcast i8* %_50001 to i8**
  %_430061 = load i8*, i8** %_430213
  %_430214 = bitcast i8* %_430061 to { i8*, i32, i32, i8* }*
  %_430215 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430214, i32 0, i32 2
  %_430062 = bitcast i32* %_430215 to i8*
  %_430216 = bitcast i8* %_430062 to i32*
  %_430063 = load i32, i32* %_430216
  %_430217 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430218 = getelementptr i8*, i8** %_430217, i32 4209
  %_430064 = bitcast i8** %_430218 to i8*
  %_430219 = bitcast i8* %_430064 to i8**
  %_430220 = getelementptr i8*, i8** %_430219, i32 %_430063
  %_430065 = bitcast i8** %_430220 to i8*
  %_430221 = bitcast i8* %_430065 to i8**
  %_210002 = load i8*, i8** %_430221
  %_430222 = bitcast i8* %_210002 to i8* (i8*, i32)*
  %_210003 = call dereferenceable_or_null(8) i8* %_430222(i8* dereferenceable_or_null(8) %_50001, i32 %_150002)
  %_210005 = icmp eq i8* %_210003, null
  br label %_230000.0
_220000.0:
  %_430067 = icmp ne i8* %_50001, null
  br i1 %_430067, label %_430066.0, label %_430023.0
_430066.0:
  %_430223 = bitcast i8* %_50001 to i8**
  %_430068 = load i8*, i8** %_430223
  %_430224 = bitcast i8* %_430068 to { i8*, i32, i32, i8* }*
  %_430225 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430224, i32 0, i32 2
  %_430069 = bitcast i32* %_430225 to i8*
  %_430226 = bitcast i8* %_430069 to i32*
  %_430070 = load i32, i32* %_430226
  %_430227 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430228 = getelementptr i8*, i8** %_430227, i32 4209
  %_430071 = bitcast i8** %_430228 to i8*
  %_430229 = bitcast i8* %_430071 to i8**
  %_430230 = getelementptr i8*, i8** %_430229, i32 %_430070
  %_430072 = bitcast i8** %_430230 to i8*
  %_430231 = bitcast i8* %_430072 to i8**
  %_220002 = load i8*, i8** %_430231
  %_430232 = bitcast i8* %_220002 to i8* (i8*, i32)*
  %_220003 = call dereferenceable_or_null(8) i8* %_430232(i8* dereferenceable_or_null(8) %_50001, i32 %_150002)
  %_430074 = icmp ne i8* %_190003, null
  br i1 %_430074, label %_430073.0, label %_430023.0
_430073.0:
  %_430233 = bitcast i8* %_190003 to i8**
  %_430075 = load i8*, i8** %_430233
  %_430234 = bitcast i8* %_430075 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_430235 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_430234, i32 0, i32 4, i32 0
  %_430076 = bitcast i8** %_430235 to i8*
  %_430236 = bitcast i8* %_430076 to i8**
  %_220005 = load i8*, i8** %_430236
  %_430237 = bitcast i8* %_220005 to i1 (i8*, i8*)*
  %_220006 = call i1 %_430237(i8* dereferenceable_or_null(8) %_190003, i8* dereferenceable_or_null(8) %_220003)
  br label %_230000.0
_230000.0:
  %_230001 = phi i1 [%_220006, %_430073.0], [%_210005, %_430059.0]
  %_230003 = add i32 %_150002, 1
  br label %_150000.0
_200000.0:
  br label %_240000.0
_240000.0:
  %_240002 = icmp slt i32 %_150002, %_80003
  br i1 %_240002, label %_250000.0, label %_260000.0
_250000.0:
  br label %_270000.0
_260000.0:
  br label %_270000.0
_270000.0:
  %_270001 = phi i1 [false, %_260000.0], [%_150001, %_250000.0]
  br i1 %_270001, label %_280000.0, label %_290000.0
_280000.0:
  %_430078 = icmp ne i8* %_1, null
  br i1 %_430078, label %_430077.0, label %_430023.0
_430077.0:
  %_430238 = bitcast i8* %_1 to i8**
  %_430079 = load i8*, i8** %_430238
  %_430239 = bitcast i8* %_430079 to { i8*, i32, i32, i8* }*
  %_430240 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430239, i32 0, i32 2
  %_430080 = bitcast i32* %_430240 to i8*
  %_430241 = bitcast i8* %_430080 to i32*
  %_430081 = load i32, i32* %_430241
  %_430242 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430243 = getelementptr i8*, i8** %_430242, i32 446
  %_430082 = bitcast i8** %_430243 to i8*
  %_430244 = bitcast i8* %_430082 to i8**
  %_430245 = getelementptr i8*, i8** %_430244, i32 %_430081
  %_430083 = bitcast i8** %_430245 to i8*
  %_430246 = bitcast i8* %_430083 to i8**
  %_280002 = load i8*, i8** %_430246
  %_430247 = bitcast i8* %_280002 to i8* (i8*)*
  %_280003 = call dereferenceable_or_null(8) i8* %_430247(i8* dereferenceable_or_null(8) %_1)
  %_430085 = icmp ne i8* %_280003, null
  br i1 %_430085, label %_430084.0, label %_430023.0
_430084.0:
  %_430248 = bitcast i8* %_280003 to i8**
  %_430086 = load i8*, i8** %_430248
  %_430249 = bitcast i8* %_430086 to { i8*, i32, i32, i8* }*
  %_430250 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430249, i32 0, i32 2
  %_430087 = bitcast i32* %_430250 to i8*
  %_430251 = bitcast i8* %_430087 to i32*
  %_430088 = load i32, i32* %_430251
  %_430252 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430253 = getelementptr i8*, i8** %_430252, i32 4149
  %_430089 = bitcast i8** %_430253 to i8*
  %_430254 = bitcast i8* %_430089 to i8**
  %_430255 = getelementptr i8*, i8** %_430254, i32 %_430088
  %_430090 = bitcast i8** %_430255 to i8*
  %_430256 = bitcast i8* %_430090 to i8**
  %_280005 = load i8*, i8** %_430256
  %_430257 = bitcast i8* %_280005 to i8* (i8*, i32)*
  %_280006 = call dereferenceable_or_null(8) i8* %_430257(i8* dereferenceable_or_null(8) %_280003, i32 %_150002)
  %_430092 = icmp ne i8* %_50001, null
  br i1 %_430092, label %_430091.0, label %_430023.0
_430091.0:
  %_430258 = bitcast i8* %_50001 to i8**
  %_430093 = load i8*, i8** %_430258
  %_430259 = bitcast i8* %_430093 to { i8*, i32, i32, i8* }*
  %_430260 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430259, i32 0, i32 2
  %_430094 = bitcast i32* %_430260 to i8*
  %_430261 = bitcast i8* %_430094 to i32*
  %_430095 = load i32, i32* %_430261
  %_430262 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430263 = getelementptr i8*, i8** %_430262, i32 446
  %_430096 = bitcast i8** %_430263 to i8*
  %_430264 = bitcast i8* %_430096 to i8**
  %_430265 = getelementptr i8*, i8** %_430264, i32 %_430095
  %_430097 = bitcast i8** %_430265 to i8*
  %_430266 = bitcast i8* %_430097 to i8**
  %_280008 = load i8*, i8** %_430266
  %_430267 = bitcast i8* %_280008 to i8* (i8*)*
  %_280009 = call dereferenceable_or_null(8) i8* %_430267(i8* dereferenceable_or_null(8) %_50001)
  %_430099 = icmp ne i8* %_280009, null
  br i1 %_430099, label %_430098.0, label %_430023.0
_430098.0:
  %_430268 = bitcast i8* %_280009 to i8**
  %_430100 = load i8*, i8** %_430268
  %_430269 = bitcast i8* %_430100 to { i8*, i32, i32, i8* }*
  %_430270 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430269, i32 0, i32 2
  %_430101 = bitcast i32* %_430270 to i8*
  %_430271 = bitcast i8* %_430101 to i32*
  %_430102 = load i32, i32* %_430271
  %_430272 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430273 = getelementptr i8*, i8** %_430272, i32 4149
  %_430103 = bitcast i8** %_430273 to i8*
  %_430274 = bitcast i8* %_430103 to i8**
  %_430275 = getelementptr i8*, i8** %_430274, i32 %_430102
  %_430104 = bitcast i8** %_430275 to i8*
  %_430276 = bitcast i8* %_430104 to i8**
  %_280011 = load i8*, i8** %_430276
  %_430277 = bitcast i8* %_280011 to i8* (i8*, i32)*
  %_280012 = call dereferenceable_or_null(8) i8* %_430277(i8* dereferenceable_or_null(8) %_280009, i32 %_150002)
  br label %_300000.0
_300000.0:
  %_300001 = phi i1 [%_150001, %_430098.0], [%_380001, %_380000.0]
  br i1 %_300001, label %_310000.0, label %_320000.0
_310000.0:
  %_430106 = icmp ne i8* %_280006, null
  br i1 %_430106, label %_430105.0, label %_430023.0
_430105.0:
  %_430278 = bitcast i8* %_280006 to i8**
  %_430107 = load i8*, i8** %_430278
  %_430279 = bitcast i8* %_430107 to { i8*, i32, i32, i8* }*
  %_430280 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430279, i32 0, i32 2
  %_430108 = bitcast i32* %_430280 to i8*
  %_430281 = bitcast i8* %_430108 to i32*
  %_430109 = load i32, i32* %_430281
  %_430282 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430283 = getelementptr i8*, i8** %_430282, i32 4359
  %_430110 = bitcast i8** %_430283 to i8*
  %_430284 = bitcast i8* %_430110 to i8**
  %_430285 = getelementptr i8*, i8** %_430284, i32 %_430109
  %_430111 = bitcast i8** %_430285 to i8*
  %_430286 = bitcast i8* %_430111 to i8**
  %_310002 = load i8*, i8** %_430286
  %_430287 = bitcast i8* %_310002 to i1 (i8*)*
  %_310003 = call i1 %_430287(i8* dereferenceable_or_null(8) %_280006)
  br label %_330000.0
_320000.0:
  br label %_330000.0
_330000.0:
  %_330001 = phi i1 [false, %_320000.0], [%_310003, %_430105.0]
  br i1 %_330001, label %_340000.0, label %_350000.0
_340000.0:
  %_430113 = icmp ne i8* %_280006, null
  br i1 %_430113, label %_430112.0, label %_430023.0
_430112.0:
  %_430288 = bitcast i8* %_280006 to i8**
  %_430114 = load i8*, i8** %_430288
  %_430289 = bitcast i8* %_430114 to { i8*, i32, i32, i8* }*
  %_430290 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430289, i32 0, i32 2
  %_430115 = bitcast i32* %_430290 to i8*
  %_430291 = bitcast i8* %_430115 to i32*
  %_430116 = load i32, i32* %_430291
  %_430292 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430293 = getelementptr i8*, i8** %_430292, i32 4219
  %_430117 = bitcast i8** %_430293 to i8*
  %_430294 = bitcast i8* %_430117 to i8**
  %_430295 = getelementptr i8*, i8** %_430294, i32 %_430116
  %_430118 = bitcast i8** %_430295 to i8*
  %_430296 = bitcast i8* %_430118 to i8**
  %_340002 = load i8*, i8** %_430296
  %_430297 = bitcast i8* %_340002 to i8* (i8*)*
  %_340003 = call dereferenceable_or_null(8) i8* %_430297(i8* dereferenceable_or_null(8) %_280006)
  %_340005 = icmp eq i8* %_340003, null
  br i1 %_340005, label %_360000.0, label %_370000.0
_360000.0:
  %_430120 = icmp ne i8* %_280012, null
  br i1 %_430120, label %_430119.0, label %_430023.0
_430119.0:
  %_430298 = bitcast i8* %_280012 to i8**
  %_430121 = load i8*, i8** %_430298
  %_430299 = bitcast i8* %_430121 to { i8*, i32, i32, i8* }*
  %_430300 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430299, i32 0, i32 2
  %_430122 = bitcast i32* %_430300 to i8*
  %_430301 = bitcast i8* %_430122 to i32*
  %_430123 = load i32, i32* %_430301
  %_430302 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430303 = getelementptr i8*, i8** %_430302, i32 4219
  %_430124 = bitcast i8** %_430303 to i8*
  %_430304 = bitcast i8* %_430124 to i8**
  %_430305 = getelementptr i8*, i8** %_430304, i32 %_430123
  %_430125 = bitcast i8** %_430305 to i8*
  %_430306 = bitcast i8* %_430125 to i8**
  %_360002 = load i8*, i8** %_430306
  %_430307 = bitcast i8* %_360002 to i8* (i8*)*
  %_360003 = call dereferenceable_or_null(8) i8* %_430307(i8* dereferenceable_or_null(8) %_280012)
  %_360005 = icmp eq i8* %_360003, null
  br label %_380000.0
_370000.0:
  %_430127 = icmp ne i8* %_280012, null
  br i1 %_430127, label %_430126.0, label %_430023.0
_430126.0:
  %_430308 = bitcast i8* %_280012 to i8**
  %_430128 = load i8*, i8** %_430308
  %_430309 = bitcast i8* %_430128 to { i8*, i32, i32, i8* }*
  %_430310 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430309, i32 0, i32 2
  %_430129 = bitcast i32* %_430310 to i8*
  %_430311 = bitcast i8* %_430129 to i32*
  %_430130 = load i32, i32* %_430311
  %_430312 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430313 = getelementptr i8*, i8** %_430312, i32 4219
  %_430131 = bitcast i8** %_430313 to i8*
  %_430314 = bitcast i8* %_430131 to i8**
  %_430315 = getelementptr i8*, i8** %_430314, i32 %_430130
  %_430132 = bitcast i8** %_430315 to i8*
  %_430316 = bitcast i8* %_430132 to i8**
  %_370002 = load i8*, i8** %_430316
  %_430317 = bitcast i8* %_370002 to i8* (i8*)*
  %_370003 = call dereferenceable_or_null(8) i8* %_430317(i8* dereferenceable_or_null(8) %_280012)
  %_430134 = icmp ne i8* %_340003, null
  br i1 %_430134, label %_430133.0, label %_430023.0
_430133.0:
  %_430318 = bitcast i8* %_340003 to i8**
  %_430135 = load i8*, i8** %_430318
  %_430319 = bitcast i8* %_430135 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_430320 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_430319, i32 0, i32 4, i32 0
  %_430136 = bitcast i8** %_430320 to i8*
  %_430321 = bitcast i8* %_430136 to i8**
  %_370005 = load i8*, i8** %_430321
  %_430322 = bitcast i8* %_370005 to i1 (i8*, i8*)*
  %_370006 = call i1 %_430322(i8* dereferenceable_or_null(8) %_340003, i8* dereferenceable_or_null(8) %_370003)
  br label %_380000.0
_380000.0:
  %_380001 = phi i1 [%_370006, %_430133.0], [%_360005, %_430119.0]
  br label %_300000.0
_350000.0:
  br label %_390000.0
_390000.0:
  br label %_400000.0
_290000.0:
  br label %_400000.0
_400000.0:
  %_400001 = phi i1 [%_150001, %_290000.0], [%_300001, %_390000.0]
  br label %_410000.0
_110000.0:
  br label %_410000.0
_410000.0:
  %_410001 = phi i1 [%_80008, %_110000.0], [%_400001, %_400000.0]
  %_410002 = phi i32 [0, %_110000.0], [%_150002, %_400000.0]
  br label %_90000.0
_90000.0:
  %_90001 = phi i1 [%_410001, %_410000.0], [false, %_70000.0]
  %_90002 = phi i32 [%_410002, %_410000.0], [0, %_70000.0]
  %_90003 = phi i1 [%_410001, %_410000.0], [true, %_70000.0]
  br label %_420000.0
_60000.0:
  br label %_430000.0
_430000.0:
  %_430138 = icmp ne i8* %_1, null
  br i1 %_430138, label %_430137.0, label %_430023.0
_430137.0:
  %_430323 = bitcast i8* %_1 to i8**
  %_430139 = load i8*, i8** %_430323
  %_430324 = bitcast i8* %_430139 to { i8*, i32, i32, i8* }*
  %_430325 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_430324, i32 0, i32 2
  %_430140 = bitcast i32* %_430325 to i8*
  %_430326 = bitcast i8* %_430140 to i32*
  %_430141 = load i32, i32* %_430326
  %_430327 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_430328 = getelementptr i8*, i8** %_430327, i32 1305
  %_430142 = bitcast i8** %_430328 to i8*
  %_430329 = bitcast i8* %_430142 to i8**
  %_430330 = getelementptr i8*, i8** %_430329, i32 %_430141
  %_430143 = bitcast i8** %_430330 to i8*
  %_430331 = bitcast i8* %_430143 to i8**
  %_430002 = load i8*, i8** %_430331
  %_430332 = bitcast i8* %_430002 to i1 (i8*, i8*)*
  %_430003 = call i1 %_430332(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  br label %_420000.0
_420000.0:
  %_420001 = phi i1 [false, %_430137.0], [%_90001, %_90000.0]
  %_420002 = phi i32 [0, %_430137.0], [%_90002, %_90000.0]
  %_420003 = phi i1 [%_430003, %_430137.0], [%_90003, %_90000.0]
  ret i1 %_420003
_430023.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_430015.0:
  %_430145 = phi i8* [%_2, %_430013.0]
  %_430146 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM37scala.collection.immutable.IndexedSeqG4type" to i8*), %_430013.0]
  %_430333 = bitcast i8* %_430145 to i8**
  %_430147 = load i8*, i8** %_430333
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_430147, i8* %_430146)
  unreachable
}

define nonnull dereferenceable(16) i8* @"_SM37scala.collection.immutable.IndexedSeqD15iterableFactoryL27scala.collection.SeqFactoryEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(16) i8* @"_SM38scala.collection.immutable.IndexedSeq$G4load"()
  ret i8* %_20001
}

define nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM37scala.collection.immutable.IndexedSeqD8canEqualL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_110007 = icmp eq i8* %_2, null
  br i1 %_110007, label %_110004.0, label %_110005.0
_110004.0:
  br label %_110006.0
_110005.0:
  %_110056 = bitcast i8* %_2 to i8**
  %_110008 = load i8*, i8** %_110056
  %_110057 = bitcast i8* %_110008 to { i8*, i32, i32, i8* }*
  %_110058 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110057, i32 0, i32 1
  %_110009 = bitcast i32* %_110058 to i8*
  %_110059 = bitcast i8* %_110009 to i32*
  %_110010 = load i32, i32* %_110059
  %_110060 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_110061 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_110060, i32 0, i32 %_110010, i32 92
  %_110011 = bitcast i1* %_110061 to i8*
  %_110062 = bitcast i8* %_110011 to i1*
  %_110012 = load i1, i1* %_110062
  br label %_110006.0
_110006.0:
  %_40002 = phi i1 [%_110012, %_110005.0], [false, %_110004.0]
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_110016 = icmp eq i8* %_2, null
  br i1 %_110016, label %_110014.0, label %_110013.0
_110013.0:
  %_110063 = bitcast i8* %_2 to i8**
  %_110017 = load i8*, i8** %_110063
  %_110064 = bitcast i8* %_110017 to { i8*, i32, i32, i8* }*
  %_110065 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110064, i32 0, i32 1
  %_110018 = bitcast i32* %_110065 to i8*
  %_110066 = bitcast i8* %_110018 to i32*
  %_110019 = load i32, i32* %_110066
  %_110067 = bitcast i8* bitcast ([657 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [657 x [122 x i1]]*
  %_110068 = getelementptr [657 x [122 x i1]], [657 x [122 x i1]]* %_110067, i32 0, i32 %_110019, i32 92
  %_110020 = bitcast i1* %_110068 to i8*
  %_110069 = bitcast i8* %_110020 to i1*
  %_110021 = load i1, i1* %_110069
  br i1 %_110021, label %_110014.0, label %_110015.0
_110014.0:
  %_50001 = bitcast i8* %_2 to i8*
  %_110024 = icmp ne i8* %_1, null
  br i1 %_110024, label %_110022.0, label %_110023.0
_110022.0:
  %_110070 = bitcast i8* %_1 to i8**
  %_110025 = load i8*, i8** %_110070
  %_110071 = bitcast i8* %_110025 to { i8*, i32, i32, i8* }*
  %_110072 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110071, i32 0, i32 2
  %_110026 = bitcast i32* %_110072 to i8*
  %_110073 = bitcast i8* %_110026 to i32*
  %_110027 = load i32, i32* %_110073
  %_110074 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_110075 = getelementptr i8*, i8** %_110074, i32 4009
  %_110028 = bitcast i8** %_110075 to i8*
  %_110076 = bitcast i8* %_110028 to i8**
  %_110077 = getelementptr i8*, i8** %_110076, i32 %_110027
  %_110029 = bitcast i8** %_110077 to i8*
  %_110078 = bitcast i8* %_110029 to i8**
  %_50003 = load i8*, i8** %_110078
  %_110079 = bitcast i8* %_50003 to i32 (i8*)*
  %_50004 = call i32 %_110079(i8* dereferenceable_or_null(8) %_1)
  %_110031 = icmp ne i8* %_50001, null
  br i1 %_110031, label %_110030.0, label %_110023.0
_110030.0:
  %_110080 = bitcast i8* %_50001 to i8**
  %_110032 = load i8*, i8** %_110080
  %_110081 = bitcast i8* %_110032 to { i8*, i32, i32, i8* }*
  %_110082 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110081, i32 0, i32 2
  %_110033 = bitcast i32* %_110082 to i8*
  %_110083 = bitcast i8* %_110033 to i32*
  %_110034 = load i32, i32* %_110083
  %_110084 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_110085 = getelementptr i8*, i8** %_110084, i32 4009
  %_110035 = bitcast i8** %_110085 to i8*
  %_110086 = bitcast i8* %_110035 to i8**
  %_110087 = getelementptr i8*, i8** %_110086, i32 %_110034
  %_110036 = bitcast i8** %_110087 to i8*
  %_110088 = bitcast i8* %_110036 to i8**
  %_50006 = load i8*, i8** %_110088
  %_110089 = bitcast i8* %_50006 to i32 (i8*)*
  %_50007 = call i32 %_110089(i8* dereferenceable_or_null(8) %_50001)
  %_50009 = icmp eq i32 %_50004, %_50007
  br i1 %_50009, label %_70000.0, label %_80000.0
_70000.0:
  %_110038 = icmp ne i8* %_1, null
  br i1 %_110038, label %_110037.0, label %_110023.0
_110037.0:
  %_110090 = bitcast i8* %_1 to i8**
  %_110039 = load i8*, i8** %_110090
  %_110091 = bitcast i8* %_110039 to { i8*, i32, i32, i8* }*
  %_110092 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110091, i32 0, i32 2
  %_110040 = bitcast i32* %_110092 to i8*
  %_110093 = bitcast i8* %_110040 to i32*
  %_110041 = load i32, i32* %_110093
  %_110094 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_110095 = getelementptr i8*, i8** %_110094, i32 1471
  %_110042 = bitcast i8** %_110095 to i8*
  %_110096 = bitcast i8* %_110042 to i8**
  %_110097 = getelementptr i8*, i8** %_110096, i32 %_110041
  %_110043 = bitcast i8** %_110097 to i8*
  %_110098 = bitcast i8* %_110043 to i8**
  %_70002 = load i8*, i8** %_110098
  %_110099 = bitcast i8* %_70002 to i1 (i8*, i8*)*
  %_70003 = call i1 %_110099(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  br label %_90000.0
_80000.0:
  br label %_90000.0
_90000.0:
  %_90001 = phi i1 [false, %_80000.0], [%_70003, %_110037.0]
  br label %_100000.0
_60000.0:
  br label %_110000.0
_110000.0:
  %_110045 = icmp ne i8* %_1, null
  br i1 %_110045, label %_110044.0, label %_110023.0
_110044.0:
  %_110100 = bitcast i8* %_1 to i8**
  %_110046 = load i8*, i8** %_110100
  %_110101 = bitcast i8* %_110046 to { i8*, i32, i32, i8* }*
  %_110102 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_110101, i32 0, i32 2
  %_110047 = bitcast i32* %_110102 to i8*
  %_110103 = bitcast i8* %_110047 to i32*
  %_110048 = load i32, i32* %_110103
  %_110104 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_110105 = getelementptr i8*, i8** %_110104, i32 1471
  %_110049 = bitcast i8** %_110105 to i8*
  %_110106 = bitcast i8* %_110049 to i8**
  %_110107 = getelementptr i8*, i8** %_110106, i32 %_110048
  %_110050 = bitcast i8** %_110107 to i8*
  %_110108 = bitcast i8* %_110050 to i8**
  %_110002 = load i8*, i8** %_110108
  %_110109 = bitcast i8* %_110002 to i1 (i8*, i8*)*
  %_110003 = call i1 %_110109(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  br label %_100000.0
_100000.0:
  %_100001 = phi i1 [%_110003, %_110044.0], [%_90001, %_90000.0]
  ret i1 %_100001
_110023.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_110015.0:
  %_110052 = phi i8* [%_2, %_110013.0]
  %_110053 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM37scala.collection.immutable.IndexedSeqG4type" to i8*), %_110013.0]
  %_110110 = bitcast i8* %_110052 to i8**
  %_110054 = load i8*, i8** %_110110
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_110054, i8* %_110053)
  unreachable
}

define i1 @"_SM38java.util.package$CompareNullablesOps$D19$eq$eq$eq$extensionL16java.lang.ObjectL16java.lang.ObjectzEO"(i8* %_1, i8* %_2, i8* %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40002 = call i1 @"_SM18java.util.Objects$D6equalsL16java.lang.ObjectL16java.lang.ObjectzEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM18java.util.Objects$G8instance" to i8*), i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_3)
  ret i1 %_40002
}

define nonnull dereferenceable(8) i8* @"_SM38java.util.package$CompareNullablesOps$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(8) i8* @"_SM38scala.collection.mutable.IndexedBufferD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i32 @"_SM40java.nio.file.StandardOpenOption$$anon$2D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM40java.nio.file.StandardOpenOption$$anon$2D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM40java.nio.file.StandardOpenOption$$anon$2D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(8) i8* @"_SM40scala.collection.mutable.GrowableBuilderD13$plus$plus$eqL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.GrowableD13$plus$plus$eqL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(16) i8* @"_SM40scala.collection.mutable.GrowableBuilderD6addAllL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(16) i8* @"_SM40scala.collection.mutable.GrowableBuilderD6addAllL29scala.collection.IterableOnceL40scala.collection.mutable.GrowableBuilderEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(16) i8* @"_SM40scala.collection.mutable.GrowableBuilderD6addAllL29scala.collection.IterableOnceL40scala.collection.mutable.GrowableBuilderEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_40004 = icmp ne i8* %_1, null
  br i1 %_40004, label %_40002.0, label %_40003.0
_40002.0:
  %_40014 = bitcast i8* %_1 to { i8*, i8* }*
  %_40015 = getelementptr { i8*, i8* }, { i8*, i8* }* %_40014, i32 0, i32 1
  %_40005 = bitcast i8** %_40015 to i8*
  %_40016 = bitcast i8* %_40005 to i8**
  %_40001 = load i8*, i8** %_40016, !dereferenceable_or_null !{i64 8}
  %_40007 = icmp ne i8* %_40001, null
  br i1 %_40007, label %_40006.0, label %_40003.0
_40006.0:
  %_40017 = bitcast i8* %_40001 to i8**
  %_40008 = load i8*, i8** %_40017
  %_40018 = bitcast i8* %_40008 to { i8*, i32, i32, i8* }*
  %_40019 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40018, i32 0, i32 2
  %_40009 = bitcast i32* %_40019 to i8*
  %_40020 = bitcast i8* %_40009 to i32*
  %_40010 = load i32, i32* %_40020
  %_40021 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_40022 = getelementptr i8*, i8** %_40021, i32 3448
  %_40011 = bitcast i8** %_40022 to i8*
  %_40023 = bitcast i8* %_40011 to i8**
  %_40024 = getelementptr i8*, i8** %_40023, i32 %_40010
  %_40012 = bitcast i8** %_40024 to i8*
  %_40025 = bitcast i8* %_40012 to i8**
  %_30002 = load i8*, i8** %_40025
  %_40026 = bitcast i8* %_30002 to i8* (i8*, i8*)*
  %_30003 = call dereferenceable_or_null(8) i8* %_40026(i8* dereferenceable_or_null(8) %_40001, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_1
_40003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM40scala.collection.mutable.GrowableBuilderD6addOneL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(16) i8* @"_SM40scala.collection.mutable.GrowableBuilderD6addOneL16java.lang.ObjectL40scala.collection.mutable.GrowableBuilderEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(16) i8* @"_SM40scala.collection.mutable.GrowableBuilderD6addOneL16java.lang.ObjectL40scala.collection.mutable.GrowableBuilderEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_40004 = icmp ne i8* %_1, null
  br i1 %_40004, label %_40002.0, label %_40003.0
_40002.0:
  %_40014 = bitcast i8* %_1 to { i8*, i8* }*
  %_40015 = getelementptr { i8*, i8* }, { i8*, i8* }* %_40014, i32 0, i32 1
  %_40005 = bitcast i8** %_40015 to i8*
  %_40016 = bitcast i8* %_40005 to i8**
  %_40001 = load i8*, i8** %_40016, !dereferenceable_or_null !{i64 8}
  %_40007 = icmp ne i8* %_40001, null
  br i1 %_40007, label %_40006.0, label %_40003.0
_40006.0:
  %_40017 = bitcast i8* %_40001 to i8**
  %_40008 = load i8*, i8** %_40017
  %_40018 = bitcast i8* %_40008 to { i8*, i32, i32, i8* }*
  %_40019 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40018, i32 0, i32 2
  %_40009 = bitcast i32* %_40019 to i8*
  %_40020 = bitcast i8* %_40009 to i32*
  %_40010 = load i32, i32* %_40020
  %_40021 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_40022 = getelementptr i8*, i8** %_40021, i32 3748
  %_40011 = bitcast i8** %_40022 to i8*
  %_40023 = bitcast i8* %_40011 to i8**
  %_40024 = getelementptr i8*, i8** %_40023, i32 %_40010
  %_40012 = bitcast i8** %_40024 to i8*
  %_40025 = bitcast i8* %_40012 to i8**
  %_30002 = load i8*, i8** %_40025
  %_40026 = bitcast i8* %_30002 to i8* (i8*, i8*)*
  %_30003 = call dereferenceable_or_null(8) i8* %_40026(i8* dereferenceable_or_null(8) %_40001, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_1
_40003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM40scala.collection.mutable.GrowableBuilderD6resultL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(8) i8* @"_SM40scala.collection.mutable.GrowableBuilderD6resultL33scala.collection.mutable.GrowableEO"(i8* dereferenceable_or_null(16) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM40scala.collection.mutable.GrowableBuilderD6resultL33scala.collection.mutable.GrowableEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i8* }*
  %_30008 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30007, i32 0, i32 1
  %_30005 = bitcast i8** %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i8**
  %_30001 = load i8*, i8** %_30009, !dereferenceable_or_null !{i64 8}
  ret i8* %_30001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM40scala.collection.mutable.GrowableBuilderD8$plus$eqL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.GrowableD8$plus$eqL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(8) i8* @"_SM40scala.collection.mutable.GrowableBuilderD9mapResultL15scala.Function1L32scala.collection.mutable.BuilderEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.collection.mutable.BuilderD9mapResultL15scala.Function1L32scala.collection.mutable.BuilderEO" to i8*) to i8* (i8*, i8*)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define i32 @"_SM41java.nio.file.StandardOpenOption$$anon$10D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM41java.nio.file.StandardOpenOption$$anon$10D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM41java.nio.file.StandardOpenOption$$anon$10D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(32) i8* @"_SM41java.util.Formatter$BigDecimalLayoutForm$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 171
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 32}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM41java.util.Formatter$BigDecimalLayoutForm$G4type" to i8*), i64 32)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM41java.util.Formatter$BigDecimalLayoutForm$RE"(i8* dereferenceable_or_null(32) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM41java.util.Formatter$BigDecimalLayoutForm$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM41java.util.Formatter$BigDecimalLayoutForm$G4load"()
  %_20003 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.util.Formatter$$anon$1G4type" to i8*), i64 24)
  %_20050 = bitcast i8* %_20003 to { i8*, i32, i8* }*
  %_20051 = getelementptr { i8*, i32, i8* }, { i8*, i32, i8* }* %_20050, i32 0, i32 2
  %_20019 = bitcast i8** %_20051 to i8*
  %_20052 = bitcast i8* %_20019 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-217" to i8*), i8** %_20052
  %_20053 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8* }*
  %_20054 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_20053, i32 0, i32 2
  %_20021 = bitcast i8** %_20054 to i8*
  %_20055 = bitcast i8* %_20021 to i8**
  store i8* %_20003, i8** %_20055
  %_20007 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.util.Formatter$$anon$2G4type" to i8*), i64 24)
  %_20056 = bitcast i8* %_20007 to { i8*, i32, i8* }*
  %_20057 = getelementptr { i8*, i32, i8* }, { i8*, i32, i8* }* %_20056, i32 0, i32 1
  %_20023 = bitcast i32* %_20057 to i8*
  %_20058 = bitcast i8* %_20023 to i32*
  store i32 1, i32* %_20058
  %_20059 = bitcast i8* %_20007 to { i8*, i32, i8* }*
  %_20060 = getelementptr { i8*, i32, i8* }, { i8*, i32, i8* }* %_20059, i32 0, i32 2
  %_20025 = bitcast i8** %_20060 to i8*
  %_20061 = bitcast i8* %_20025 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-219" to i8*), i8** %_20061
  %_20062 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8* }*
  %_20063 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_20062, i32 0, i32 1
  %_20027 = bitcast i8** %_20063 to i8*
  %_20064 = bitcast i8* %_20027 to i8**
  store i8* %_20007, i8** %_20064
  %_20011 = call dereferenceable_or_null(24) i8* @"_SM40java.util.Formatter$BigDecimalLayoutFormD10SCIENTIFICL40java.util.Formatter$BigDecimalLayoutFormEo"()
  %_20012 = call dereferenceable_or_null(24) i8* @"_SM40java.util.Formatter$BigDecimalLayoutFormD13DECIMAL_FLOATL40java.util.Formatter$BigDecimalLayoutFormEo"()
  %_20014 = call dereferenceable_or_null(8) i8* @"_SM38scala.scalanative.runtime.ObjectArray$D5allociL37scala.scalanative.runtime.ObjectArrayEO"(i8* bitcast ({ i8* }* @"_SM38scala.scalanative.runtime.ObjectArray$G8instance" to i8*), i32 2)
  %_20065 = bitcast i8* %_20014 to { i8*, i32 }*
  %_20066 = getelementptr { i8*, i32 }, { i8*, i32 }* %_20065, i32 0, i32 1
  %_20031 = bitcast i32* %_20066 to i8*
  %_20067 = bitcast i8* %_20031 to i32*
  %_20030 = load i32, i32* %_20067
  %_20034 = icmp sge i32 0, 0
  %_20035 = icmp slt i32 0, %_20030
  %_20036 = and i1 %_20034, %_20035
  br i1 %_20036, label %_20032.0, label %_20033.0
_20032.0:
  %_20068 = bitcast i8* %_20014 to { i8*, i32, i32, [0 x i8*] }*
  %_20069 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_20068, i32 0, i32 3, i32 0
  %_20037 = bitcast i8** %_20069 to i8*
  %_20070 = bitcast i8* %_20037 to i8**
  store i8* %_20011, i8** %_20070
  %_20071 = bitcast i8* %_20014 to { i8*, i32 }*
  %_20072 = getelementptr { i8*, i32 }, { i8*, i32 }* %_20071, i32 0, i32 1
  %_20040 = bitcast i32* %_20072 to i8*
  %_20073 = bitcast i8* %_20040 to i32*
  %_20039 = load i32, i32* %_20073
  %_20042 = icmp sge i32 1, 0
  %_20043 = icmp slt i32 1, %_20039
  %_20044 = and i1 %_20042, %_20043
  br i1 %_20044, label %_20041.0, label %_20033.0
_20041.0:
  %_20074 = bitcast i8* %_20014 to { i8*, i32, i32, [0 x i8*] }*
  %_20075 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_20074, i32 0, i32 3, i32 1
  %_20045 = bitcast i8** %_20075 to i8*
  %_20076 = bitcast i8* %_20045 to i8**
  store i8* %_20012, i8** %_20076
  %_20077 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8* }*
  %_20078 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_20077, i32 0, i32 3
  %_20047 = bitcast i8** %_20078 to i8*
  %_20079 = bitcast i8* %_20047 to i8**
  store i8* %_20014, i8** %_20079
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_20033.0:
  %_20048 = phi i32 [0, %_20000.0], [1, %_20032.0]
  %_20080 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_20080(i8* null, i32 %_20048)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM41scala.scalanative.posix.pwdOps$passwdOps$D16pw_dir$extensionL28scala.scalanative.unsafe.PtrL28scala.scalanative.unsafe.PtrEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_1290300 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_50003 = call dereferenceable_or_null(16) i8* %_1290300(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_1290301 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_90001 = call dereferenceable_or_null(16) i8* %_1290301(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_1290302 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_110001 = call dereferenceable_or_null(16) i8* %_1290302(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_120003 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_120004 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* dereferenceable_or_null(16) %_50003)
  %_1290010 = icmp eq i8* %_120004, null
  br i1 %_1290010, label %_1290008.0, label %_1290007.0
_1290007.0:
  %_1290303 = bitcast i8* %_120004 to i8**
  %_1290011 = load i8*, i8** %_1290303
  %_1290304 = bitcast i8* %_1290011 to { i8*, i32, i32, i8* }*
  %_1290305 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_1290304, i32 0, i32 1
  %_1290012 = bitcast i32* %_1290305 to i8*
  %_1290306 = bitcast i8* %_1290012 to i32*
  %_1290013 = load i32, i32* %_1290306
  %_1290014 = icmp sle i32 297, %_1290013
  %_1290015 = icmp sle i32 %_1290013, 301
  %_1290016 = and i1 %_1290014, %_1290015
  br i1 %_1290016, label %_1290008.0, label %_1290009.0
_1290008.0:
  %_120005 = bitcast i8* %_120004 to i8*
  %_120007 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$UInt$G8instance" to i8*))
  %_1290019 = icmp eq i8* %_120007, null
  br i1 %_1290019, label %_1290018.0, label %_1290017.0
_1290017.0:
  %_1290307 = bitcast i8* %_120007 to i8**
  %_1290020 = load i8*, i8** %_1290307
  %_1290308 = bitcast i8* %_1290020 to { i8*, i32, i32, i8* }*
  %_1290309 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_1290308, i32 0, i32 1
  %_1290021 = bitcast i32* %_1290309 to i8*
  %_1290310 = bitcast i8* %_1290021 to i32*
  %_1290022 = load i32, i32* %_1290310
  %_1290023 = icmp sle i32 297, %_1290022
  %_1290024 = icmp sle i32 %_1290022, 301
  %_1290025 = and i1 %_1290023, %_1290024
  br i1 %_1290025, label %_1290018.0, label %_1290009.0
_1290018.0:
  %_120008 = bitcast i8* %_120007 to i8*
  %_120009 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$UInt$G8instance" to i8*))
  %_1290028 = icmp eq i8* %_120009, null
  br i1 %_1290028, label %_1290027.0, label %_1290026.0
_1290026.0:
  %_1290311 = bitcast i8* %_120009 to i8**
  %_1290029 = load i8*, i8** %_1290311
  %_1290312 = bitcast i8* %_1290029 to { i8*, i32, i32, i8* }*
  %_1290313 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_1290312, i32 0, i32 1
  %_1290030 = bitcast i32* %_1290313 to i8*
  %_1290314 = bitcast i8* %_1290030 to i32*
  %_1290031 = load i32, i32* %_1290314
  %_1290032 = icmp sle i32 297, %_1290031
  %_1290033 = icmp sle i32 %_1290031, 301
  %_1290034 = and i1 %_1290032, %_1290033
  br i1 %_1290034, label %_1290027.0, label %_1290009.0
_1290027.0:
  %_120010 = bitcast i8* %_120009 to i8*
  %_120011 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* dereferenceable_or_null(16) %_90001)
  %_1290037 = icmp eq i8* %_120011, null
  br i1 %_1290037, label %_1290036.0, label %_1290035.0
_1290035.0:
  %_1290315 = bitcast i8* %_120011 to i8**
  %_1290038 = load i8*, i8** %_1290315
  %_1290316 = bitcast i8* %_1290038 to { i8*, i32, i32, i8* }*
  %_1290317 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_1290316, i32 0, i32 1
  %_1290039 = bitcast i32* %_1290317 to i8*
  %_1290318 = bitcast i8* %_1290039 to i32*
  %_1290040 = load i32, i32* %_1290318
  %_1290041 = icmp sle i32 297, %_1290040
  %_1290042 = icmp sle i32 %_1290040, 301
  %_1290043 = and i1 %_1290041, %_1290042
  br i1 %_1290043, label %_1290036.0, label %_1290009.0
_1290036.0:
  %_120012 = bitcast i8* %_120011 to i8*
  %_120013 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* dereferenceable_or_null(16) %_110001)
  %_1290046 = icmp eq i8* %_120013, null
  br i1 %_1290046, label %_1290045.0, label %_1290044.0
_1290044.0:
  %_1290319 = bitcast i8* %_120013 to i8**
  %_1290047 = load i8*, i8** %_1290319
  %_1290320 = bitcast i8* %_1290047 to { i8*, i32, i32, i8* }*
  %_1290321 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_1290320, i32 0, i32 1
  %_1290048 = bitcast i32* %_1290321 to i8*
  %_1290322 = bitcast i8* %_1290048 to i32*
  %_1290049 = load i32, i32* %_1290322
  %_1290050 = icmp sle i32 297, %_1290049
  %_1290051 = icmp sle i32 %_1290049, 301
  %_1290052 = and i1 %_1290050, %_1290051
  br i1 %_1290052, label %_1290045.0, label %_1290009.0
_1290045.0:
  %_120014 = bitcast i8* %_120013 to i8*
  %_1290323 = bitcast i8* bitcast (i8* (i8*, i8*, i8*, i8*, i8*, i8*)* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$D5applyL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL37scala.scalanative.unsafe.Tag$CStruct5EO" to i8*) to i8* (i8*, i8*, i8*, i8*, i8*, i8*)*
  %_120015 = call dereferenceable_or_null(48) i8* %_1290323(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$G8instance" to i8*), i8* dereferenceable_or_null(8) %_120005, i8* dereferenceable_or_null(8) %_120008, i8* dereferenceable_or_null(8) %_120010, i8* dereferenceable_or_null(8) %_120012, i8* dereferenceable_or_null(8) %_120014)
  %_1290055 = icmp ne i8* %_2, null
  br i1 %_1290055, label %_1290053.0, label %_1290054.0
_1290053.0:
  %_1290324 = bitcast i8* %_2 to { i8*, i8* }*
  %_1290325 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1290324, i32 0, i32 1
  %_1290056 = bitcast i8** %_1290325 to i8*
  %_1290326 = bitcast i8* %_1290056 to i8**
  %_170001 = load i8*, i8** %_1290326
  %_1290327 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_220001 = call dereferenceable_or_null(16) i8* %_1290327(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_1290328 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_260001 = call dereferenceable_or_null(16) i8* %_1290328(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_1290329 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_280001 = call dereferenceable_or_null(16) i8* %_1290329(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_290002 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* dereferenceable_or_null(16) %_220001)
  %_1290059 = icmp eq i8* %_290002, null
  br i1 %_1290059, label %_1290058.0, label %_1290057.0
_1290057.0:
  %_1290330 = bitcast i8* %_290002 to i8**
  %_1290060 = load i8*, i8** %_1290330
  %_1290331 = bitcast i8* %_1290060 to { i8*, i32, i32, i8* }*
  %_1290332 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_1290331, i32 0, i32 1
  %_1290061 = bitcast i32* %_1290332 to i8*
  %_1290333 = bitcast i8* %_1290061 to i32*
  %_1290062 = load i32, i32* %_1290333
  %_1290063 = icmp sle i32 297, %_1290062
  %_1290064 = icmp sle i32 %_1290062, 301
  %_1290065 = and i1 %_1290063, %_1290064
  br i1 %_1290065, label %_1290058.0, label %_1290009.0
_1290058.0:
  %_290003 = bitcast i8* %_290002 to i8*
  %_290004 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$UInt$G8instance" to i8*))
  %_1290068 = icmp eq i8* %_290004, null
  br i1 %_1290068, label %_1290067.0, label %_1290066.0
_1290066.0:
  %_1290334 = bitcast i8* %_290004 to i8**
  %_1290069 = load i8*, i8** %_1290334
  %_1290335 = bitcast i8* %_1290069 to { i8*, i32, i32, i8* }*
  %_1290336 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_1290335, i32 0, i32 1
  %_1290070 = bitcast i32* %_1290336 to i8*
  %_1290337 = bitcast i8* %_1290070 to i32*
  %_1290071 = load i32, i32* %_1290337
  %_1290072 = icmp sle i32 297, %_1290071
  %_1290073 = icmp sle i32 %_1290071, 301
  %_1290074 = and i1 %_1290072, %_1290073
  br i1 %_1290074, label %_1290067.0, label %_1290009.0
_1290067.0:
  %_290005 = bitcast i8* %_290004 to i8*
  %_290006 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$UInt$G8instance" to i8*))
  %_1290077 = icmp eq i8* %_290006, null
  br i1 %_1290077, label %_1290076.0, label %_1290075.0
_1290075.0:
  %_1290338 = bitcast i8* %_290006 to i8**
  %_1290078 = load i8*, i8** %_1290338
  %_1290339 = bitcast i8* %_1290078 to { i8*, i32, i32, i8* }*
  %_1290340 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_1290339, i32 0, i32 1
  %_1290079 = bitcast i32* %_1290340 to i8*
  %_1290341 = bitcast i8* %_1290079 to i32*
  %_1290080 = load i32, i32* %_1290341
  %_1290081 = icmp sle i32 297, %_1290080
  %_1290082 = icmp sle i32 %_1290080, 301
  %_1290083 = and i1 %_1290081, %_1290082
  br i1 %_1290083, label %_1290076.0, label %_1290009.0
_1290076.0:
  %_290007 = bitcast i8* %_290006 to i8*
  %_290008 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* dereferenceable_or_null(16) %_260001)
  %_1290086 = icmp eq i8* %_290008, null
  br i1 %_1290086, label %_1290085.0, label %_1290084.0
_1290084.0:
  %_1290342 = bitcast i8* %_290008 to i8**
  %_1290087 = load i8*, i8** %_1290342
  %_1290343 = bitcast i8* %_1290087 to { i8*, i32, i32, i8* }*
  %_1290344 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_1290343, i32 0, i32 1
  %_1290088 = bitcast i32* %_1290344 to i8*
  %_1290345 = bitcast i8* %_1290088 to i32*
  %_1290089 = load i32, i32* %_1290345
  %_1290090 = icmp sle i32 297, %_1290089
  %_1290091 = icmp sle i32 %_1290089, 301
  %_1290092 = and i1 %_1290090, %_1290091
  br i1 %_1290092, label %_1290085.0, label %_1290009.0
_1290085.0:
  %_290009 = bitcast i8* %_290008 to i8*
  %_290010 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* dereferenceable_or_null(16) %_280001)
  %_1290095 = icmp eq i8* %_290010, null
  br i1 %_1290095, label %_1290094.0, label %_1290093.0
_1290093.0:
  %_1290346 = bitcast i8* %_290010 to i8**
  %_1290096 = load i8*, i8** %_1290346
  %_1290347 = bitcast i8* %_1290096 to { i8*, i32, i32, i8* }*
  %_1290348 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_1290347, i32 0, i32 1
  %_1290097 = bitcast i32* %_1290348 to i8*
  %_1290349 = bitcast i8* %_1290097 to i32*
  %_1290098 = load i32, i32* %_1290349
  %_1290099 = icmp sle i32 297, %_1290098
  %_1290100 = icmp sle i32 %_1290098, 301
  %_1290101 = and i1 %_1290099, %_1290100
  br i1 %_1290101, label %_1290094.0, label %_1290009.0
_1290094.0:
  %_290011 = bitcast i8* %_290010 to i8*
  %_1290350 = bitcast i8* bitcast (i8* (i8*, i8*, i8*, i8*, i8*, i8*)* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$D5applyL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL37scala.scalanative.unsafe.Tag$CStruct5EO" to i8*) to i8* (i8*, i8*, i8*, i8*, i8*, i8*)*
  %_290012 = call dereferenceable_or_null(48) i8* %_1290350(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$G8instance" to i8*), i8* dereferenceable_or_null(8) %_290003, i8* dereferenceable_or_null(8) %_290005, i8* dereferenceable_or_null(8) %_290007, i8* dereferenceable_or_null(8) %_290009, i8* dereferenceable_or_null(8) %_290011)
  %_300004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 3)
  %_300005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_300004)
  br label %_330000.0
_330000.0:
  %_330001 = call i32 @"_SM32scala.scalanative.unsigned.ULongD5toIntiEO"(i8* dereferenceable_or_null(16) %_300005)
  switch i32 %_330001, label %_340000.0 [
    i32 0, label %_350000.0
    i32 1, label %_360000.0
    i32 2, label %_370000.0
    i32 3, label %_380000.0
    i32 4, label %_390000.0
  ]
_350000.0:
  %_350001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_350002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_350001)
  %_1290103 = icmp ne i8* %_290012, null
  br i1 %_1290103, label %_1290102.0, label %_1290054.0
_1290102.0:
  %_1290351 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290352 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290351, i32 0, i32 5
  %_1290104 = bitcast i8** %_1290352 to i8*
  %_1290353 = bitcast i8* %_1290104 to i8**
  %_400001 = load i8*, i8** %_1290353, !dereferenceable_or_null !{i64 8}
  %_1290106 = icmp ne i8* %_400001, null
  br i1 %_1290106, label %_1290105.0, label %_1290054.0
_1290105.0:
  %_1290354 = bitcast i8* %_400001 to i8**
  %_1290107 = load i8*, i8** %_1290354
  %_1290355 = bitcast i8* %_1290107 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290356 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290355, i32 0, i32 4, i32 4
  %_1290108 = bitcast i8** %_1290356 to i8*
  %_1290357 = bitcast i8* %_1290108 to i8**
  %_350005 = load i8*, i8** %_1290357
  %_1290358 = bitcast i8* %_350005 to i8* (i8*)*
  %_350006 = call dereferenceable_or_null(16) i8* %_1290358(i8* dereferenceable_or_null(8) %_400001)
  %_410001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_410002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_410001)
  %_1290359 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_410003 = call dereferenceable_or_null(16) i8* %_1290359(i8* dereferenceable_or_null(16) %_350006, i8* dereferenceable_or_null(16) %_410002)
  %_410004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_410005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_410004)
  %_1290360 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_410006 = call dereferenceable_or_null(16) i8* %_1290360(i8* dereferenceable_or_null(16) %_350002, i8* dereferenceable_or_null(16) %_410003)
  %_410007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_410006, i8* dereferenceable_or_null(16) %_410005)
  br i1 %_410007, label %_420000.0, label %_430000.0
_420000.0:
  br label %_440000.0
_430000.0:
  %_1290361 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_430001 = call dereferenceable_or_null(16) i8* %_1290361(i8* dereferenceable_or_null(16) %_350002, i8* dereferenceable_or_null(16) %_410003)
  %_1290362 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_430002 = call dereferenceable_or_null(16) i8* %_1290362(i8* dereferenceable_or_null(16) %_350006, i8* dereferenceable_or_null(16) %_430001)
  br label %_440000.0
_440000.0:
  %_440001 = phi i8* [%_430002, %_430000.0], [%_410005, %_420000.0]
  %_1290363 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_440002 = call dereferenceable_or_null(16) i8* %_1290363(i8* dereferenceable_or_null(16) %_350002, i8* dereferenceable_or_null(16) %_440001)
  br label %_450000.0
_360000.0:
  %_360001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_360002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_360001)
  %_1290110 = icmp ne i8* %_290012, null
  br i1 %_1290110, label %_1290109.0, label %_1290054.0
_1290109.0:
  %_1290364 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290365 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290364, i32 0, i32 5
  %_1290111 = bitcast i8** %_1290365 to i8*
  %_1290366 = bitcast i8* %_1290111 to i8**
  %_460001 = load i8*, i8** %_1290366, !dereferenceable_or_null !{i64 8}
  %_1290113 = icmp ne i8* %_460001, null
  br i1 %_1290113, label %_1290112.0, label %_1290054.0
_1290112.0:
  %_1290367 = bitcast i8* %_460001 to i8**
  %_1290114 = load i8*, i8** %_1290367
  %_1290368 = bitcast i8* %_1290114 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290369 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290368, i32 0, i32 4, i32 4
  %_1290115 = bitcast i8** %_1290369 to i8*
  %_1290370 = bitcast i8* %_1290115 to i8**
  %_360005 = load i8*, i8** %_1290370
  %_1290371 = bitcast i8* %_360005 to i8* (i8*)*
  %_360006 = call dereferenceable_or_null(16) i8* %_1290371(i8* dereferenceable_or_null(8) %_460001)
  %_470001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_470002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_470001)
  %_1290372 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_470003 = call dereferenceable_or_null(16) i8* %_1290372(i8* dereferenceable_or_null(16) %_360006, i8* dereferenceable_or_null(16) %_470002)
  %_470004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_470005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_470004)
  %_1290373 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_470006 = call dereferenceable_or_null(16) i8* %_1290373(i8* dereferenceable_or_null(16) %_360002, i8* dereferenceable_or_null(16) %_470003)
  %_470007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_470006, i8* dereferenceable_or_null(16) %_470005)
  br i1 %_470007, label %_480000.0, label %_490000.0
_480000.0:
  br label %_500000.0
_490000.0:
  %_1290374 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_490001 = call dereferenceable_or_null(16) i8* %_1290374(i8* dereferenceable_or_null(16) %_360002, i8* dereferenceable_or_null(16) %_470003)
  %_1290375 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_490002 = call dereferenceable_or_null(16) i8* %_1290375(i8* dereferenceable_or_null(16) %_360006, i8* dereferenceable_or_null(16) %_490001)
  br label %_500000.0
_500000.0:
  %_500001 = phi i8* [%_490002, %_490000.0], [%_470005, %_480000.0]
  %_1290376 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_500002 = call dereferenceable_or_null(16) i8* %_1290376(i8* dereferenceable_or_null(16) %_360002, i8* dereferenceable_or_null(16) %_500001)
  %_1290117 = icmp ne i8* %_290012, null
  br i1 %_1290117, label %_1290116.0, label %_1290054.0
_1290116.0:
  %_1290377 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290378 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290377, i32 0, i32 5
  %_1290118 = bitcast i8** %_1290378 to i8*
  %_1290379 = bitcast i8* %_1290118 to i8**
  %_510001 = load i8*, i8** %_1290379, !dereferenceable_or_null !{i64 8}
  %_1290120 = icmp ne i8* %_510001, null
  br i1 %_1290120, label %_1290119.0, label %_1290054.0
_1290119.0:
  %_1290380 = bitcast i8* %_510001 to i8**
  %_1290121 = load i8*, i8** %_1290380
  %_1290381 = bitcast i8* %_1290121 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290382 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290381, i32 0, i32 4, i32 6
  %_1290122 = bitcast i8** %_1290382 to i8*
  %_1290383 = bitcast i8* %_1290122 to i8**
  %_360008 = load i8*, i8** %_1290383
  %_1290384 = bitcast i8* %_360008 to i8* (i8*)*
  %_360009 = call dereferenceable_or_null(16) i8* %_1290384(i8* dereferenceable_or_null(8) %_510001)
  %_1290385 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_360010 = call dereferenceable_or_null(16) i8* %_1290385(i8* dereferenceable_or_null(16) %_500002, i8* dereferenceable_or_null(16) %_360009)
  %_1290124 = icmp ne i8* %_290012, null
  br i1 %_1290124, label %_1290123.0, label %_1290054.0
_1290123.0:
  %_1290386 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290387 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290386, i32 0, i32 4
  %_1290125 = bitcast i8** %_1290387 to i8*
  %_1290388 = bitcast i8* %_1290125 to i8**
  %_520001 = load i8*, i8** %_1290388, !dereferenceable_or_null !{i64 8}
  %_1290127 = icmp ne i8* %_520001, null
  br i1 %_1290127, label %_1290126.0, label %_1290054.0
_1290126.0:
  %_1290389 = bitcast i8* %_520001 to i8**
  %_1290128 = load i8*, i8** %_1290389
  %_1290390 = bitcast i8* %_1290128 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290391 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290390, i32 0, i32 4, i32 4
  %_1290129 = bitcast i8** %_1290391 to i8*
  %_1290392 = bitcast i8* %_1290129 to i8**
  %_360012 = load i8*, i8** %_1290392
  %_1290393 = bitcast i8* %_360012 to i8* (i8*)*
  %_360013 = call dereferenceable_or_null(16) i8* %_1290393(i8* dereferenceable_or_null(8) %_520001)
  %_530001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_530002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_530001)
  %_1290394 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_530003 = call dereferenceable_or_null(16) i8* %_1290394(i8* dereferenceable_or_null(16) %_360013, i8* dereferenceable_or_null(16) %_530002)
  %_530004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_530005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_530004)
  %_1290395 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_530006 = call dereferenceable_or_null(16) i8* %_1290395(i8* dereferenceable_or_null(16) %_360010, i8* dereferenceable_or_null(16) %_530003)
  %_530007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_530006, i8* dereferenceable_or_null(16) %_530005)
  br i1 %_530007, label %_540000.0, label %_550000.0
_540000.0:
  br label %_560000.0
_550000.0:
  %_1290396 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_550001 = call dereferenceable_or_null(16) i8* %_1290396(i8* dereferenceable_or_null(16) %_360010, i8* dereferenceable_or_null(16) %_530003)
  %_1290397 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_550002 = call dereferenceable_or_null(16) i8* %_1290397(i8* dereferenceable_or_null(16) %_360013, i8* dereferenceable_or_null(16) %_550001)
  br label %_560000.0
_560000.0:
  %_560001 = phi i8* [%_550002, %_550000.0], [%_530005, %_540000.0]
  %_1290398 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_560002 = call dereferenceable_or_null(16) i8* %_1290398(i8* dereferenceable_or_null(16) %_360010, i8* dereferenceable_or_null(16) %_560001)
  br label %_450000.0
_370000.0:
  %_370001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_370002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_370001)
  %_1290131 = icmp ne i8* %_290012, null
  br i1 %_1290131, label %_1290130.0, label %_1290054.0
_1290130.0:
  %_1290399 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290400 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290399, i32 0, i32 5
  %_1290132 = bitcast i8** %_1290400 to i8*
  %_1290401 = bitcast i8* %_1290132 to i8**
  %_570001 = load i8*, i8** %_1290401, !dereferenceable_or_null !{i64 8}
  %_1290134 = icmp ne i8* %_570001, null
  br i1 %_1290134, label %_1290133.0, label %_1290054.0
_1290133.0:
  %_1290402 = bitcast i8* %_570001 to i8**
  %_1290135 = load i8*, i8** %_1290402
  %_1290403 = bitcast i8* %_1290135 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290404 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290403, i32 0, i32 4, i32 4
  %_1290136 = bitcast i8** %_1290404 to i8*
  %_1290405 = bitcast i8* %_1290136 to i8**
  %_370005 = load i8*, i8** %_1290405
  %_1290406 = bitcast i8* %_370005 to i8* (i8*)*
  %_370006 = call dereferenceable_or_null(16) i8* %_1290406(i8* dereferenceable_or_null(8) %_570001)
  %_580001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_580002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_580001)
  %_1290407 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_580003 = call dereferenceable_or_null(16) i8* %_1290407(i8* dereferenceable_or_null(16) %_370006, i8* dereferenceable_or_null(16) %_580002)
  %_580004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_580005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_580004)
  %_1290408 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_580006 = call dereferenceable_or_null(16) i8* %_1290408(i8* dereferenceable_or_null(16) %_370002, i8* dereferenceable_or_null(16) %_580003)
  %_580007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_580006, i8* dereferenceable_or_null(16) %_580005)
  br i1 %_580007, label %_590000.0, label %_600000.0
_590000.0:
  br label %_610000.0
_600000.0:
  %_1290409 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_600001 = call dereferenceable_or_null(16) i8* %_1290409(i8* dereferenceable_or_null(16) %_370002, i8* dereferenceable_or_null(16) %_580003)
  %_1290410 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_600002 = call dereferenceable_or_null(16) i8* %_1290410(i8* dereferenceable_or_null(16) %_370006, i8* dereferenceable_or_null(16) %_600001)
  br label %_610000.0
_610000.0:
  %_610001 = phi i8* [%_600002, %_600000.0], [%_580005, %_590000.0]
  %_1290411 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_610002 = call dereferenceable_or_null(16) i8* %_1290411(i8* dereferenceable_or_null(16) %_370002, i8* dereferenceable_or_null(16) %_610001)
  %_1290138 = icmp ne i8* %_290012, null
  br i1 %_1290138, label %_1290137.0, label %_1290054.0
_1290137.0:
  %_1290412 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290413 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290412, i32 0, i32 5
  %_1290139 = bitcast i8** %_1290413 to i8*
  %_1290414 = bitcast i8* %_1290139 to i8**
  %_620001 = load i8*, i8** %_1290414, !dereferenceable_or_null !{i64 8}
  %_1290141 = icmp ne i8* %_620001, null
  br i1 %_1290141, label %_1290140.0, label %_1290054.0
_1290140.0:
  %_1290415 = bitcast i8* %_620001 to i8**
  %_1290142 = load i8*, i8** %_1290415
  %_1290416 = bitcast i8* %_1290142 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290417 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290416, i32 0, i32 4, i32 6
  %_1290143 = bitcast i8** %_1290417 to i8*
  %_1290418 = bitcast i8* %_1290143 to i8**
  %_370008 = load i8*, i8** %_1290418
  %_1290419 = bitcast i8* %_370008 to i8* (i8*)*
  %_370009 = call dereferenceable_or_null(16) i8* %_1290419(i8* dereferenceable_or_null(8) %_620001)
  %_1290420 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_370010 = call dereferenceable_or_null(16) i8* %_1290420(i8* dereferenceable_or_null(16) %_610002, i8* dereferenceable_or_null(16) %_370009)
  %_1290145 = icmp ne i8* %_290012, null
  br i1 %_1290145, label %_1290144.0, label %_1290054.0
_1290144.0:
  %_1290421 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290422 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290421, i32 0, i32 4
  %_1290146 = bitcast i8** %_1290422 to i8*
  %_1290423 = bitcast i8* %_1290146 to i8**
  %_630001 = load i8*, i8** %_1290423, !dereferenceable_or_null !{i64 8}
  %_1290148 = icmp ne i8* %_630001, null
  br i1 %_1290148, label %_1290147.0, label %_1290054.0
_1290147.0:
  %_1290424 = bitcast i8* %_630001 to i8**
  %_1290149 = load i8*, i8** %_1290424
  %_1290425 = bitcast i8* %_1290149 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290426 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290425, i32 0, i32 4, i32 4
  %_1290150 = bitcast i8** %_1290426 to i8*
  %_1290427 = bitcast i8* %_1290150 to i8**
  %_370012 = load i8*, i8** %_1290427
  %_1290428 = bitcast i8* %_370012 to i8* (i8*)*
  %_370013 = call dereferenceable_or_null(16) i8* %_1290428(i8* dereferenceable_or_null(8) %_630001)
  %_640001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_640002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_640001)
  %_1290429 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_640003 = call dereferenceable_or_null(16) i8* %_1290429(i8* dereferenceable_or_null(16) %_370013, i8* dereferenceable_or_null(16) %_640002)
  %_640004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_640005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_640004)
  %_1290430 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_640006 = call dereferenceable_or_null(16) i8* %_1290430(i8* dereferenceable_or_null(16) %_370010, i8* dereferenceable_or_null(16) %_640003)
  %_640007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_640006, i8* dereferenceable_or_null(16) %_640005)
  br i1 %_640007, label %_650000.0, label %_660000.0
_650000.0:
  br label %_670000.0
_660000.0:
  %_1290431 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_660001 = call dereferenceable_or_null(16) i8* %_1290431(i8* dereferenceable_or_null(16) %_370010, i8* dereferenceable_or_null(16) %_640003)
  %_1290432 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_660002 = call dereferenceable_or_null(16) i8* %_1290432(i8* dereferenceable_or_null(16) %_370013, i8* dereferenceable_or_null(16) %_660001)
  br label %_670000.0
_670000.0:
  %_670001 = phi i8* [%_660002, %_660000.0], [%_640005, %_650000.0]
  %_1290433 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_670002 = call dereferenceable_or_null(16) i8* %_1290433(i8* dereferenceable_or_null(16) %_370010, i8* dereferenceable_or_null(16) %_670001)
  %_1290152 = icmp ne i8* %_290012, null
  br i1 %_1290152, label %_1290151.0, label %_1290054.0
_1290151.0:
  %_1290434 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290435 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290434, i32 0, i32 4
  %_1290153 = bitcast i8** %_1290435 to i8*
  %_1290436 = bitcast i8* %_1290153 to i8**
  %_680001 = load i8*, i8** %_1290436, !dereferenceable_or_null !{i64 8}
  %_1290155 = icmp ne i8* %_680001, null
  br i1 %_1290155, label %_1290154.0, label %_1290054.0
_1290154.0:
  %_1290437 = bitcast i8* %_680001 to i8**
  %_1290156 = load i8*, i8** %_1290437
  %_1290438 = bitcast i8* %_1290156 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290439 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290438, i32 0, i32 4, i32 6
  %_1290157 = bitcast i8** %_1290439 to i8*
  %_1290440 = bitcast i8* %_1290157 to i8**
  %_370015 = load i8*, i8** %_1290440
  %_1290441 = bitcast i8* %_370015 to i8* (i8*)*
  %_370016 = call dereferenceable_or_null(16) i8* %_1290441(i8* dereferenceable_or_null(8) %_680001)
  %_1290442 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_370017 = call dereferenceable_or_null(16) i8* %_1290442(i8* dereferenceable_or_null(16) %_670002, i8* dereferenceable_or_null(16) %_370016)
  %_1290159 = icmp ne i8* %_290012, null
  br i1 %_1290159, label %_1290158.0, label %_1290054.0
_1290158.0:
  %_1290443 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290444 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290443, i32 0, i32 3
  %_1290160 = bitcast i8** %_1290444 to i8*
  %_1290445 = bitcast i8* %_1290160 to i8**
  %_690001 = load i8*, i8** %_1290445, !dereferenceable_or_null !{i64 8}
  %_1290162 = icmp ne i8* %_690001, null
  br i1 %_1290162, label %_1290161.0, label %_1290054.0
_1290161.0:
  %_1290446 = bitcast i8* %_690001 to i8**
  %_1290163 = load i8*, i8** %_1290446
  %_1290447 = bitcast i8* %_1290163 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290448 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290447, i32 0, i32 4, i32 4
  %_1290164 = bitcast i8** %_1290448 to i8*
  %_1290449 = bitcast i8* %_1290164 to i8**
  %_370019 = load i8*, i8** %_1290449
  %_1290450 = bitcast i8* %_370019 to i8* (i8*)*
  %_370020 = call dereferenceable_or_null(16) i8* %_1290450(i8* dereferenceable_or_null(8) %_690001)
  %_700001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_700002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_700001)
  %_1290451 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_700003 = call dereferenceable_or_null(16) i8* %_1290451(i8* dereferenceable_or_null(16) %_370020, i8* dereferenceable_or_null(16) %_700002)
  %_700004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_700005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_700004)
  %_1290452 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_700006 = call dereferenceable_or_null(16) i8* %_1290452(i8* dereferenceable_or_null(16) %_370017, i8* dereferenceable_or_null(16) %_700003)
  %_700007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_700006, i8* dereferenceable_or_null(16) %_700005)
  br i1 %_700007, label %_710000.0, label %_720000.0
_710000.0:
  br label %_730000.0
_720000.0:
  %_1290453 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_720001 = call dereferenceable_or_null(16) i8* %_1290453(i8* dereferenceable_or_null(16) %_370017, i8* dereferenceable_or_null(16) %_700003)
  %_1290454 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_720002 = call dereferenceable_or_null(16) i8* %_1290454(i8* dereferenceable_or_null(16) %_370020, i8* dereferenceable_or_null(16) %_720001)
  br label %_730000.0
_730000.0:
  %_730001 = phi i8* [%_720002, %_720000.0], [%_700005, %_710000.0]
  %_1290455 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_730002 = call dereferenceable_or_null(16) i8* %_1290455(i8* dereferenceable_or_null(16) %_370017, i8* dereferenceable_or_null(16) %_730001)
  br label %_450000.0
_380000.0:
  %_380001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_380002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_380001)
  %_1290166 = icmp ne i8* %_290012, null
  br i1 %_1290166, label %_1290165.0, label %_1290054.0
_1290165.0:
  %_1290456 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290457 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290456, i32 0, i32 5
  %_1290167 = bitcast i8** %_1290457 to i8*
  %_1290458 = bitcast i8* %_1290167 to i8**
  %_740001 = load i8*, i8** %_1290458, !dereferenceable_or_null !{i64 8}
  %_1290169 = icmp ne i8* %_740001, null
  br i1 %_1290169, label %_1290168.0, label %_1290054.0
_1290168.0:
  %_1290459 = bitcast i8* %_740001 to i8**
  %_1290170 = load i8*, i8** %_1290459
  %_1290460 = bitcast i8* %_1290170 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290461 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290460, i32 0, i32 4, i32 4
  %_1290171 = bitcast i8** %_1290461 to i8*
  %_1290462 = bitcast i8* %_1290171 to i8**
  %_380005 = load i8*, i8** %_1290462
  %_1290463 = bitcast i8* %_380005 to i8* (i8*)*
  %_380006 = call dereferenceable_or_null(16) i8* %_1290463(i8* dereferenceable_or_null(8) %_740001)
  %_750001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_750002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_750001)
  %_1290464 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_750003 = call dereferenceable_or_null(16) i8* %_1290464(i8* dereferenceable_or_null(16) %_380006, i8* dereferenceable_or_null(16) %_750002)
  %_750004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_750005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_750004)
  %_1290465 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_750006 = call dereferenceable_or_null(16) i8* %_1290465(i8* dereferenceable_or_null(16) %_380002, i8* dereferenceable_or_null(16) %_750003)
  %_750007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_750006, i8* dereferenceable_or_null(16) %_750005)
  br i1 %_750007, label %_760000.0, label %_770000.0
_760000.0:
  br label %_780000.0
_770000.0:
  %_1290466 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_770001 = call dereferenceable_or_null(16) i8* %_1290466(i8* dereferenceable_or_null(16) %_380002, i8* dereferenceable_or_null(16) %_750003)
  %_1290467 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_770002 = call dereferenceable_or_null(16) i8* %_1290467(i8* dereferenceable_or_null(16) %_380006, i8* dereferenceable_or_null(16) %_770001)
  br label %_780000.0
_780000.0:
  %_780001 = phi i8* [%_770002, %_770000.0], [%_750005, %_760000.0]
  %_1290468 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_780002 = call dereferenceable_or_null(16) i8* %_1290468(i8* dereferenceable_or_null(16) %_380002, i8* dereferenceable_or_null(16) %_780001)
  %_1290173 = icmp ne i8* %_290012, null
  br i1 %_1290173, label %_1290172.0, label %_1290054.0
_1290172.0:
  %_1290469 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290470 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290469, i32 0, i32 5
  %_1290174 = bitcast i8** %_1290470 to i8*
  %_1290471 = bitcast i8* %_1290174 to i8**
  %_790001 = load i8*, i8** %_1290471, !dereferenceable_or_null !{i64 8}
  %_1290176 = icmp ne i8* %_790001, null
  br i1 %_1290176, label %_1290175.0, label %_1290054.0
_1290175.0:
  %_1290472 = bitcast i8* %_790001 to i8**
  %_1290177 = load i8*, i8** %_1290472
  %_1290473 = bitcast i8* %_1290177 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290474 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290473, i32 0, i32 4, i32 6
  %_1290178 = bitcast i8** %_1290474 to i8*
  %_1290475 = bitcast i8* %_1290178 to i8**
  %_380008 = load i8*, i8** %_1290475
  %_1290476 = bitcast i8* %_380008 to i8* (i8*)*
  %_380009 = call dereferenceable_or_null(16) i8* %_1290476(i8* dereferenceable_or_null(8) %_790001)
  %_1290477 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_380010 = call dereferenceable_or_null(16) i8* %_1290477(i8* dereferenceable_or_null(16) %_780002, i8* dereferenceable_or_null(16) %_380009)
  %_1290180 = icmp ne i8* %_290012, null
  br i1 %_1290180, label %_1290179.0, label %_1290054.0
_1290179.0:
  %_1290478 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290479 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290478, i32 0, i32 4
  %_1290181 = bitcast i8** %_1290479 to i8*
  %_1290480 = bitcast i8* %_1290181 to i8**
  %_800001 = load i8*, i8** %_1290480, !dereferenceable_or_null !{i64 8}
  %_1290183 = icmp ne i8* %_800001, null
  br i1 %_1290183, label %_1290182.0, label %_1290054.0
_1290182.0:
  %_1290481 = bitcast i8* %_800001 to i8**
  %_1290184 = load i8*, i8** %_1290481
  %_1290482 = bitcast i8* %_1290184 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290483 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290482, i32 0, i32 4, i32 4
  %_1290185 = bitcast i8** %_1290483 to i8*
  %_1290484 = bitcast i8* %_1290185 to i8**
  %_380012 = load i8*, i8** %_1290484
  %_1290485 = bitcast i8* %_380012 to i8* (i8*)*
  %_380013 = call dereferenceable_or_null(16) i8* %_1290485(i8* dereferenceable_or_null(8) %_800001)
  %_810001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_810002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_810001)
  %_1290486 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_810003 = call dereferenceable_or_null(16) i8* %_1290486(i8* dereferenceable_or_null(16) %_380013, i8* dereferenceable_or_null(16) %_810002)
  %_810004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_810005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_810004)
  %_1290487 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_810006 = call dereferenceable_or_null(16) i8* %_1290487(i8* dereferenceable_or_null(16) %_380010, i8* dereferenceable_or_null(16) %_810003)
  %_810007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_810006, i8* dereferenceable_or_null(16) %_810005)
  br i1 %_810007, label %_820000.0, label %_830000.0
_820000.0:
  br label %_840000.0
_830000.0:
  %_1290488 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_830001 = call dereferenceable_or_null(16) i8* %_1290488(i8* dereferenceable_or_null(16) %_380010, i8* dereferenceable_or_null(16) %_810003)
  %_1290489 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_830002 = call dereferenceable_or_null(16) i8* %_1290489(i8* dereferenceable_or_null(16) %_380013, i8* dereferenceable_or_null(16) %_830001)
  br label %_840000.0
_840000.0:
  %_840001 = phi i8* [%_830002, %_830000.0], [%_810005, %_820000.0]
  %_1290490 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_840002 = call dereferenceable_or_null(16) i8* %_1290490(i8* dereferenceable_or_null(16) %_380010, i8* dereferenceable_or_null(16) %_840001)
  %_1290187 = icmp ne i8* %_290012, null
  br i1 %_1290187, label %_1290186.0, label %_1290054.0
_1290186.0:
  %_1290491 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290492 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290491, i32 0, i32 4
  %_1290188 = bitcast i8** %_1290492 to i8*
  %_1290493 = bitcast i8* %_1290188 to i8**
  %_850001 = load i8*, i8** %_1290493, !dereferenceable_or_null !{i64 8}
  %_1290190 = icmp ne i8* %_850001, null
  br i1 %_1290190, label %_1290189.0, label %_1290054.0
_1290189.0:
  %_1290494 = bitcast i8* %_850001 to i8**
  %_1290191 = load i8*, i8** %_1290494
  %_1290495 = bitcast i8* %_1290191 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290496 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290495, i32 0, i32 4, i32 6
  %_1290192 = bitcast i8** %_1290496 to i8*
  %_1290497 = bitcast i8* %_1290192 to i8**
  %_380015 = load i8*, i8** %_1290497
  %_1290498 = bitcast i8* %_380015 to i8* (i8*)*
  %_380016 = call dereferenceable_or_null(16) i8* %_1290498(i8* dereferenceable_or_null(8) %_850001)
  %_1290499 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_380017 = call dereferenceable_or_null(16) i8* %_1290499(i8* dereferenceable_or_null(16) %_840002, i8* dereferenceable_or_null(16) %_380016)
  %_1290194 = icmp ne i8* %_290012, null
  br i1 %_1290194, label %_1290193.0, label %_1290054.0
_1290193.0:
  %_1290500 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290501 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290500, i32 0, i32 3
  %_1290195 = bitcast i8** %_1290501 to i8*
  %_1290502 = bitcast i8* %_1290195 to i8**
  %_860001 = load i8*, i8** %_1290502, !dereferenceable_or_null !{i64 8}
  %_1290197 = icmp ne i8* %_860001, null
  br i1 %_1290197, label %_1290196.0, label %_1290054.0
_1290196.0:
  %_1290503 = bitcast i8* %_860001 to i8**
  %_1290198 = load i8*, i8** %_1290503
  %_1290504 = bitcast i8* %_1290198 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290505 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290504, i32 0, i32 4, i32 4
  %_1290199 = bitcast i8** %_1290505 to i8*
  %_1290506 = bitcast i8* %_1290199 to i8**
  %_380019 = load i8*, i8** %_1290506
  %_1290507 = bitcast i8* %_380019 to i8* (i8*)*
  %_380020 = call dereferenceable_or_null(16) i8* %_1290507(i8* dereferenceable_or_null(8) %_860001)
  %_870001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_870002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_870001)
  %_1290508 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_870003 = call dereferenceable_or_null(16) i8* %_1290508(i8* dereferenceable_or_null(16) %_380020, i8* dereferenceable_or_null(16) %_870002)
  %_870004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_870005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_870004)
  %_1290509 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_870006 = call dereferenceable_or_null(16) i8* %_1290509(i8* dereferenceable_or_null(16) %_380017, i8* dereferenceable_or_null(16) %_870003)
  %_870007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_870006, i8* dereferenceable_or_null(16) %_870005)
  br i1 %_870007, label %_880000.0, label %_890000.0
_880000.0:
  br label %_900000.0
_890000.0:
  %_1290510 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_890001 = call dereferenceable_or_null(16) i8* %_1290510(i8* dereferenceable_or_null(16) %_380017, i8* dereferenceable_or_null(16) %_870003)
  %_1290511 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_890002 = call dereferenceable_or_null(16) i8* %_1290511(i8* dereferenceable_or_null(16) %_380020, i8* dereferenceable_or_null(16) %_890001)
  br label %_900000.0
_900000.0:
  %_900001 = phi i8* [%_890002, %_890000.0], [%_870005, %_880000.0]
  %_1290512 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_900002 = call dereferenceable_or_null(16) i8* %_1290512(i8* dereferenceable_or_null(16) %_380017, i8* dereferenceable_or_null(16) %_900001)
  %_1290201 = icmp ne i8* %_290012, null
  br i1 %_1290201, label %_1290200.0, label %_1290054.0
_1290200.0:
  %_1290513 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290514 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290513, i32 0, i32 3
  %_1290202 = bitcast i8** %_1290514 to i8*
  %_1290515 = bitcast i8* %_1290202 to i8**
  %_910001 = load i8*, i8** %_1290515, !dereferenceable_or_null !{i64 8}
  %_1290204 = icmp ne i8* %_910001, null
  br i1 %_1290204, label %_1290203.0, label %_1290054.0
_1290203.0:
  %_1290516 = bitcast i8* %_910001 to i8**
  %_1290205 = load i8*, i8** %_1290516
  %_1290517 = bitcast i8* %_1290205 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290518 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290517, i32 0, i32 4, i32 6
  %_1290206 = bitcast i8** %_1290518 to i8*
  %_1290519 = bitcast i8* %_1290206 to i8**
  %_380022 = load i8*, i8** %_1290519
  %_1290520 = bitcast i8* %_380022 to i8* (i8*)*
  %_380023 = call dereferenceable_or_null(16) i8* %_1290520(i8* dereferenceable_or_null(8) %_910001)
  %_1290521 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_380024 = call dereferenceable_or_null(16) i8* %_1290521(i8* dereferenceable_or_null(16) %_900002, i8* dereferenceable_or_null(16) %_380023)
  %_1290208 = icmp ne i8* %_290012, null
  br i1 %_1290208, label %_1290207.0, label %_1290054.0
_1290207.0:
  %_1290522 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290523 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290522, i32 0, i32 2
  %_1290209 = bitcast i8** %_1290523 to i8*
  %_1290524 = bitcast i8* %_1290209 to i8**
  %_920001 = load i8*, i8** %_1290524, !dereferenceable_or_null !{i64 8}
  %_1290211 = icmp ne i8* %_920001, null
  br i1 %_1290211, label %_1290210.0, label %_1290054.0
_1290210.0:
  %_1290525 = bitcast i8* %_920001 to i8**
  %_1290212 = load i8*, i8** %_1290525
  %_1290526 = bitcast i8* %_1290212 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290527 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290526, i32 0, i32 4, i32 4
  %_1290213 = bitcast i8** %_1290527 to i8*
  %_1290528 = bitcast i8* %_1290213 to i8**
  %_380026 = load i8*, i8** %_1290528
  %_1290529 = bitcast i8* %_380026 to i8* (i8*)*
  %_380027 = call dereferenceable_or_null(16) i8* %_1290529(i8* dereferenceable_or_null(8) %_920001)
  %_930001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_930002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_930001)
  %_1290530 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_930003 = call dereferenceable_or_null(16) i8* %_1290530(i8* dereferenceable_or_null(16) %_380027, i8* dereferenceable_or_null(16) %_930002)
  %_930004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_930005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_930004)
  %_1290531 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_930006 = call dereferenceable_or_null(16) i8* %_1290531(i8* dereferenceable_or_null(16) %_380024, i8* dereferenceable_or_null(16) %_930003)
  %_930007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_930006, i8* dereferenceable_or_null(16) %_930005)
  br i1 %_930007, label %_940000.0, label %_950000.0
_940000.0:
  br label %_960000.0
_950000.0:
  %_1290532 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_950001 = call dereferenceable_or_null(16) i8* %_1290532(i8* dereferenceable_or_null(16) %_380024, i8* dereferenceable_or_null(16) %_930003)
  %_1290533 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_950002 = call dereferenceable_or_null(16) i8* %_1290533(i8* dereferenceable_or_null(16) %_380027, i8* dereferenceable_or_null(16) %_950001)
  br label %_960000.0
_960000.0:
  %_960001 = phi i8* [%_950002, %_950000.0], [%_930005, %_940000.0]
  %_1290534 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_960002 = call dereferenceable_or_null(16) i8* %_1290534(i8* dereferenceable_or_null(16) %_380024, i8* dereferenceable_or_null(16) %_960001)
  br label %_450000.0
_390000.0:
  %_390001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_390002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_390001)
  %_1290215 = icmp ne i8* %_290012, null
  br i1 %_1290215, label %_1290214.0, label %_1290054.0
_1290214.0:
  %_1290535 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290536 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290535, i32 0, i32 5
  %_1290216 = bitcast i8** %_1290536 to i8*
  %_1290537 = bitcast i8* %_1290216 to i8**
  %_970001 = load i8*, i8** %_1290537, !dereferenceable_or_null !{i64 8}
  %_1290218 = icmp ne i8* %_970001, null
  br i1 %_1290218, label %_1290217.0, label %_1290054.0
_1290217.0:
  %_1290538 = bitcast i8* %_970001 to i8**
  %_1290219 = load i8*, i8** %_1290538
  %_1290539 = bitcast i8* %_1290219 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290540 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290539, i32 0, i32 4, i32 4
  %_1290220 = bitcast i8** %_1290540 to i8*
  %_1290541 = bitcast i8* %_1290220 to i8**
  %_390005 = load i8*, i8** %_1290541
  %_1290542 = bitcast i8* %_390005 to i8* (i8*)*
  %_390006 = call dereferenceable_or_null(16) i8* %_1290542(i8* dereferenceable_or_null(8) %_970001)
  %_980001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_980002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_980001)
  %_1290543 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_980003 = call dereferenceable_or_null(16) i8* %_1290543(i8* dereferenceable_or_null(16) %_390006, i8* dereferenceable_or_null(16) %_980002)
  %_980004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_980005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_980004)
  %_1290544 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_980006 = call dereferenceable_or_null(16) i8* %_1290544(i8* dereferenceable_or_null(16) %_390002, i8* dereferenceable_or_null(16) %_980003)
  %_980007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_980006, i8* dereferenceable_or_null(16) %_980005)
  br i1 %_980007, label %_990000.0, label %_1000000.0
_990000.0:
  br label %_1010000.0
_1000000.0:
  %_1290545 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1000001 = call dereferenceable_or_null(16) i8* %_1290545(i8* dereferenceable_or_null(16) %_390002, i8* dereferenceable_or_null(16) %_980003)
  %_1290546 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1000002 = call dereferenceable_or_null(16) i8* %_1290546(i8* dereferenceable_or_null(16) %_390006, i8* dereferenceable_or_null(16) %_1000001)
  br label %_1010000.0
_1010000.0:
  %_1010001 = phi i8* [%_1000002, %_1000000.0], [%_980005, %_990000.0]
  %_1290547 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1010002 = call dereferenceable_or_null(16) i8* %_1290547(i8* dereferenceable_or_null(16) %_390002, i8* dereferenceable_or_null(16) %_1010001)
  %_1290222 = icmp ne i8* %_290012, null
  br i1 %_1290222, label %_1290221.0, label %_1290054.0
_1290221.0:
  %_1290548 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290549 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290548, i32 0, i32 5
  %_1290223 = bitcast i8** %_1290549 to i8*
  %_1290550 = bitcast i8* %_1290223 to i8**
  %_1020001 = load i8*, i8** %_1290550, !dereferenceable_or_null !{i64 8}
  %_1290225 = icmp ne i8* %_1020001, null
  br i1 %_1290225, label %_1290224.0, label %_1290054.0
_1290224.0:
  %_1290551 = bitcast i8* %_1020001 to i8**
  %_1290226 = load i8*, i8** %_1290551
  %_1290552 = bitcast i8* %_1290226 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290553 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290552, i32 0, i32 4, i32 6
  %_1290227 = bitcast i8** %_1290553 to i8*
  %_1290554 = bitcast i8* %_1290227 to i8**
  %_390008 = load i8*, i8** %_1290554
  %_1290555 = bitcast i8* %_390008 to i8* (i8*)*
  %_390009 = call dereferenceable_or_null(16) i8* %_1290555(i8* dereferenceable_or_null(8) %_1020001)
  %_1290556 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_390010 = call dereferenceable_or_null(16) i8* %_1290556(i8* dereferenceable_or_null(16) %_1010002, i8* dereferenceable_or_null(16) %_390009)
  %_1290229 = icmp ne i8* %_290012, null
  br i1 %_1290229, label %_1290228.0, label %_1290054.0
_1290228.0:
  %_1290557 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290558 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290557, i32 0, i32 4
  %_1290230 = bitcast i8** %_1290558 to i8*
  %_1290559 = bitcast i8* %_1290230 to i8**
  %_1030001 = load i8*, i8** %_1290559, !dereferenceable_or_null !{i64 8}
  %_1290232 = icmp ne i8* %_1030001, null
  br i1 %_1290232, label %_1290231.0, label %_1290054.0
_1290231.0:
  %_1290560 = bitcast i8* %_1030001 to i8**
  %_1290233 = load i8*, i8** %_1290560
  %_1290561 = bitcast i8* %_1290233 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290562 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290561, i32 0, i32 4, i32 4
  %_1290234 = bitcast i8** %_1290562 to i8*
  %_1290563 = bitcast i8* %_1290234 to i8**
  %_390012 = load i8*, i8** %_1290563
  %_1290564 = bitcast i8* %_390012 to i8* (i8*)*
  %_390013 = call dereferenceable_or_null(16) i8* %_1290564(i8* dereferenceable_or_null(8) %_1030001)
  %_1040001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_1040002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_1040001)
  %_1290565 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1040003 = call dereferenceable_or_null(16) i8* %_1290565(i8* dereferenceable_or_null(16) %_390013, i8* dereferenceable_or_null(16) %_1040002)
  %_1040004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_1040005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_1040004)
  %_1290566 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1040006 = call dereferenceable_or_null(16) i8* %_1290566(i8* dereferenceable_or_null(16) %_390010, i8* dereferenceable_or_null(16) %_1040003)
  %_1040007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_1040006, i8* dereferenceable_or_null(16) %_1040005)
  br i1 %_1040007, label %_1050000.0, label %_1060000.0
_1050000.0:
  br label %_1070000.0
_1060000.0:
  %_1290567 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1060001 = call dereferenceable_or_null(16) i8* %_1290567(i8* dereferenceable_or_null(16) %_390010, i8* dereferenceable_or_null(16) %_1040003)
  %_1290568 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1060002 = call dereferenceable_or_null(16) i8* %_1290568(i8* dereferenceable_or_null(16) %_390013, i8* dereferenceable_or_null(16) %_1060001)
  br label %_1070000.0
_1070000.0:
  %_1070001 = phi i8* [%_1060002, %_1060000.0], [%_1040005, %_1050000.0]
  %_1290569 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1070002 = call dereferenceable_or_null(16) i8* %_1290569(i8* dereferenceable_or_null(16) %_390010, i8* dereferenceable_or_null(16) %_1070001)
  %_1290236 = icmp ne i8* %_290012, null
  br i1 %_1290236, label %_1290235.0, label %_1290054.0
_1290235.0:
  %_1290570 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290571 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290570, i32 0, i32 4
  %_1290237 = bitcast i8** %_1290571 to i8*
  %_1290572 = bitcast i8* %_1290237 to i8**
  %_1080001 = load i8*, i8** %_1290572, !dereferenceable_or_null !{i64 8}
  %_1290239 = icmp ne i8* %_1080001, null
  br i1 %_1290239, label %_1290238.0, label %_1290054.0
_1290238.0:
  %_1290573 = bitcast i8* %_1080001 to i8**
  %_1290240 = load i8*, i8** %_1290573
  %_1290574 = bitcast i8* %_1290240 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290575 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290574, i32 0, i32 4, i32 6
  %_1290241 = bitcast i8** %_1290575 to i8*
  %_1290576 = bitcast i8* %_1290241 to i8**
  %_390015 = load i8*, i8** %_1290576
  %_1290577 = bitcast i8* %_390015 to i8* (i8*)*
  %_390016 = call dereferenceable_or_null(16) i8* %_1290577(i8* dereferenceable_or_null(8) %_1080001)
  %_1290578 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_390017 = call dereferenceable_or_null(16) i8* %_1290578(i8* dereferenceable_or_null(16) %_1070002, i8* dereferenceable_or_null(16) %_390016)
  %_1290243 = icmp ne i8* %_290012, null
  br i1 %_1290243, label %_1290242.0, label %_1290054.0
_1290242.0:
  %_1290579 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290580 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290579, i32 0, i32 3
  %_1290244 = bitcast i8** %_1290580 to i8*
  %_1290581 = bitcast i8* %_1290244 to i8**
  %_1090001 = load i8*, i8** %_1290581, !dereferenceable_or_null !{i64 8}
  %_1290246 = icmp ne i8* %_1090001, null
  br i1 %_1290246, label %_1290245.0, label %_1290054.0
_1290245.0:
  %_1290582 = bitcast i8* %_1090001 to i8**
  %_1290247 = load i8*, i8** %_1290582
  %_1290583 = bitcast i8* %_1290247 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290584 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290583, i32 0, i32 4, i32 4
  %_1290248 = bitcast i8** %_1290584 to i8*
  %_1290585 = bitcast i8* %_1290248 to i8**
  %_390019 = load i8*, i8** %_1290585
  %_1290586 = bitcast i8* %_390019 to i8* (i8*)*
  %_390020 = call dereferenceable_or_null(16) i8* %_1290586(i8* dereferenceable_or_null(8) %_1090001)
  %_1100001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_1100002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_1100001)
  %_1290587 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1100003 = call dereferenceable_or_null(16) i8* %_1290587(i8* dereferenceable_or_null(16) %_390020, i8* dereferenceable_or_null(16) %_1100002)
  %_1100004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_1100005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_1100004)
  %_1290588 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1100006 = call dereferenceable_or_null(16) i8* %_1290588(i8* dereferenceable_or_null(16) %_390017, i8* dereferenceable_or_null(16) %_1100003)
  %_1100007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_1100006, i8* dereferenceable_or_null(16) %_1100005)
  br i1 %_1100007, label %_1110000.0, label %_1120000.0
_1110000.0:
  br label %_1130000.0
_1120000.0:
  %_1290589 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1120001 = call dereferenceable_or_null(16) i8* %_1290589(i8* dereferenceable_or_null(16) %_390017, i8* dereferenceable_or_null(16) %_1100003)
  %_1290590 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1120002 = call dereferenceable_or_null(16) i8* %_1290590(i8* dereferenceable_or_null(16) %_390020, i8* dereferenceable_or_null(16) %_1120001)
  br label %_1130000.0
_1130000.0:
  %_1130001 = phi i8* [%_1120002, %_1120000.0], [%_1100005, %_1110000.0]
  %_1290591 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1130002 = call dereferenceable_or_null(16) i8* %_1290591(i8* dereferenceable_or_null(16) %_390017, i8* dereferenceable_or_null(16) %_1130001)
  %_1290250 = icmp ne i8* %_290012, null
  br i1 %_1290250, label %_1290249.0, label %_1290054.0
_1290249.0:
  %_1290592 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290593 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290592, i32 0, i32 3
  %_1290251 = bitcast i8** %_1290593 to i8*
  %_1290594 = bitcast i8* %_1290251 to i8**
  %_1140001 = load i8*, i8** %_1290594, !dereferenceable_or_null !{i64 8}
  %_1290253 = icmp ne i8* %_1140001, null
  br i1 %_1290253, label %_1290252.0, label %_1290054.0
_1290252.0:
  %_1290595 = bitcast i8* %_1140001 to i8**
  %_1290254 = load i8*, i8** %_1290595
  %_1290596 = bitcast i8* %_1290254 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290597 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290596, i32 0, i32 4, i32 6
  %_1290255 = bitcast i8** %_1290597 to i8*
  %_1290598 = bitcast i8* %_1290255 to i8**
  %_390022 = load i8*, i8** %_1290598
  %_1290599 = bitcast i8* %_390022 to i8* (i8*)*
  %_390023 = call dereferenceable_or_null(16) i8* %_1290599(i8* dereferenceable_or_null(8) %_1140001)
  %_1290600 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_390024 = call dereferenceable_or_null(16) i8* %_1290600(i8* dereferenceable_or_null(16) %_1130002, i8* dereferenceable_or_null(16) %_390023)
  %_1290257 = icmp ne i8* %_290012, null
  br i1 %_1290257, label %_1290256.0, label %_1290054.0
_1290256.0:
  %_1290601 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290602 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290601, i32 0, i32 2
  %_1290258 = bitcast i8** %_1290602 to i8*
  %_1290603 = bitcast i8* %_1290258 to i8**
  %_1150001 = load i8*, i8** %_1290603, !dereferenceable_or_null !{i64 8}
  %_1290260 = icmp ne i8* %_1150001, null
  br i1 %_1290260, label %_1290259.0, label %_1290054.0
_1290259.0:
  %_1290604 = bitcast i8* %_1150001 to i8**
  %_1290261 = load i8*, i8** %_1290604
  %_1290605 = bitcast i8* %_1290261 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290606 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290605, i32 0, i32 4, i32 4
  %_1290262 = bitcast i8** %_1290606 to i8*
  %_1290607 = bitcast i8* %_1290262 to i8**
  %_390026 = load i8*, i8** %_1290607
  %_1290608 = bitcast i8* %_390026 to i8* (i8*)*
  %_390027 = call dereferenceable_or_null(16) i8* %_1290608(i8* dereferenceable_or_null(8) %_1150001)
  %_1160001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_1160002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_1160001)
  %_1290609 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1160003 = call dereferenceable_or_null(16) i8* %_1290609(i8* dereferenceable_or_null(16) %_390027, i8* dereferenceable_or_null(16) %_1160002)
  %_1160004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_1160005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_1160004)
  %_1290610 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1160006 = call dereferenceable_or_null(16) i8* %_1290610(i8* dereferenceable_or_null(16) %_390024, i8* dereferenceable_or_null(16) %_1160003)
  %_1160007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_1160006, i8* dereferenceable_or_null(16) %_1160005)
  br i1 %_1160007, label %_1170000.0, label %_1180000.0
_1170000.0:
  br label %_1190000.0
_1180000.0:
  %_1290611 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1180001 = call dereferenceable_or_null(16) i8* %_1290611(i8* dereferenceable_or_null(16) %_390024, i8* dereferenceable_or_null(16) %_1160003)
  %_1290612 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1180002 = call dereferenceable_or_null(16) i8* %_1290612(i8* dereferenceable_or_null(16) %_390027, i8* dereferenceable_or_null(16) %_1180001)
  br label %_1190000.0
_1190000.0:
  %_1190001 = phi i8* [%_1180002, %_1180000.0], [%_1160005, %_1170000.0]
  %_1290613 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1190002 = call dereferenceable_or_null(16) i8* %_1290613(i8* dereferenceable_or_null(16) %_390024, i8* dereferenceable_or_null(16) %_1190001)
  %_1290264 = icmp ne i8* %_290012, null
  br i1 %_1290264, label %_1290263.0, label %_1290054.0
_1290263.0:
  %_1290614 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290615 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290614, i32 0, i32 2
  %_1290265 = bitcast i8** %_1290615 to i8*
  %_1290616 = bitcast i8* %_1290265 to i8**
  %_1200001 = load i8*, i8** %_1290616, !dereferenceable_or_null !{i64 8}
  %_1290267 = icmp ne i8* %_1200001, null
  br i1 %_1290267, label %_1290266.0, label %_1290054.0
_1290266.0:
  %_1290617 = bitcast i8* %_1200001 to i8**
  %_1290268 = load i8*, i8** %_1290617
  %_1290618 = bitcast i8* %_1290268 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290619 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290618, i32 0, i32 4, i32 6
  %_1290269 = bitcast i8** %_1290619 to i8*
  %_1290620 = bitcast i8* %_1290269 to i8**
  %_390029 = load i8*, i8** %_1290620
  %_1290621 = bitcast i8* %_390029 to i8* (i8*)*
  %_390030 = call dereferenceable_or_null(16) i8* %_1290621(i8* dereferenceable_or_null(8) %_1200001)
  %_1290622 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_390031 = call dereferenceable_or_null(16) i8* %_1290622(i8* dereferenceable_or_null(16) %_1190002, i8* dereferenceable_or_null(16) %_390030)
  %_1290271 = icmp ne i8* %_290012, null
  br i1 %_1290271, label %_1290270.0, label %_1290054.0
_1290270.0:
  %_1290623 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290624 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290623, i32 0, i32 1
  %_1290272 = bitcast i8** %_1290624 to i8*
  %_1290625 = bitcast i8* %_1290272 to i8**
  %_1210001 = load i8*, i8** %_1290625, !dereferenceable_or_null !{i64 8}
  %_1290274 = icmp ne i8* %_1210001, null
  br i1 %_1290274, label %_1290273.0, label %_1290054.0
_1290273.0:
  %_1290626 = bitcast i8* %_1210001 to i8**
  %_1290275 = load i8*, i8** %_1290626
  %_1290627 = bitcast i8* %_1290275 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290628 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290627, i32 0, i32 4, i32 4
  %_1290276 = bitcast i8** %_1290628 to i8*
  %_1290629 = bitcast i8* %_1290276 to i8**
  %_390033 = load i8*, i8** %_1290629
  %_1290630 = bitcast i8* %_390033 to i8* (i8*)*
  %_390034 = call dereferenceable_or_null(16) i8* %_1290630(i8* dereferenceable_or_null(8) %_1210001)
  %_1220001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_1220002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_1220001)
  %_1290631 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1220003 = call dereferenceable_or_null(16) i8* %_1290631(i8* dereferenceable_or_null(16) %_390034, i8* dereferenceable_or_null(16) %_1220002)
  %_1220004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_1220005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_1220004)
  %_1290632 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1220006 = call dereferenceable_or_null(16) i8* %_1290632(i8* dereferenceable_or_null(16) %_390031, i8* dereferenceable_or_null(16) %_1220003)
  %_1220007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_1220006, i8* dereferenceable_or_null(16) %_1220005)
  br i1 %_1220007, label %_1230000.0, label %_1240000.0
_1230000.0:
  br label %_1250000.0
_1240000.0:
  %_1290633 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1240001 = call dereferenceable_or_null(16) i8* %_1290633(i8* dereferenceable_or_null(16) %_390031, i8* dereferenceable_or_null(16) %_1220003)
  %_1290634 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1240002 = call dereferenceable_or_null(16) i8* %_1290634(i8* dereferenceable_or_null(16) %_390034, i8* dereferenceable_or_null(16) %_1240001)
  br label %_1250000.0
_1250000.0:
  %_1250001 = phi i8* [%_1240002, %_1240000.0], [%_1220005, %_1230000.0]
  %_1290635 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_1250002 = call dereferenceable_or_null(16) i8* %_1290635(i8* dereferenceable_or_null(16) %_390031, i8* dereferenceable_or_null(16) %_1250001)
  br label %_450000.0
_340000.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.runtime.package$G8instance" to i8*))
  br label %_1290278.0
_450000.0:
  %_450001 = phi i8* [null, %_1250000.0], [%_380024, %_960000.0], [null, %_730000.0], [null, %_560000.0], [null, %_440000.0]
  %_450002 = phi i8* [%_390031, %_1250000.0], [null, %_960000.0], [null, %_730000.0], [null, %_560000.0], [null, %_440000.0]
  %_450003 = phi i8* [null, %_1250000.0], [null, %_960000.0], [null, %_730000.0], [%_360010, %_560000.0], [null, %_440000.0]
  %_450004 = phi i8* [null, %_1250000.0], [null, %_960000.0], [%_370017, %_730000.0], [null, %_560000.0], [null, %_440000.0]
  %_450005 = phi i8* [%_1250002, %_1250000.0], [%_960002, %_960000.0], [%_730002, %_730000.0], [%_560002, %_560000.0], [%_440002, %_440000.0]
  %_300006 = call i64 @"_SM32scala.scalanative.unsigned.ULongD6toLongjEO"(i8* dereferenceable_or_null(16) %_450005)
  %_1290281 = icmp ne i8* %_290012, null
  br i1 %_1290281, label %_1290280.0, label %_1290054.0
_1290280.0:
  %_1290636 = bitcast i8* %_290012 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_1290637 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_1290636, i32 0, i32 2
  %_1290282 = bitcast i8** %_1290637 to i8*
  %_1290638 = bitcast i8* %_1290282 to i8**
  %_1280001 = load i8*, i8** %_1290638, !dereferenceable_or_null !{i64 8}
  %_1290284 = icmp ne i8* %_1280001, null
  br i1 %_1290284, label %_1290283.0, label %_1290054.0
_1290283.0:
  %_1290639 = bitcast i8* %_1280001 to i8**
  %_1290285 = load i8*, i8** %_1290639
  %_1290640 = bitcast i8* %_1290285 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_1290641 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_1290640, i32 0, i32 4, i32 5
  %_1290286 = bitcast i8** %_1290641 to i8*
  %_1290642 = bitcast i8* %_1290286 to i8**
  %_1290002 = load i8*, i8** %_1290642
  %_1290003 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28scala.scalanative.unsafe.PtrG4type" to i8*), i64 16)
  %_1290643 = bitcast i8* %_170001 to i8*
  %_1290644 = getelementptr i8, i8* %_1290643, i64 %_300006
  %_1290004 = bitcast i8* %_1290644 to i8*
  %_1290645 = bitcast i8* %_1290003 to { i8*, i8* }*
  %_1290646 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1290645, i32 0, i32 1
  %_1290288 = bitcast i8** %_1290646 to i8*
  %_1290647 = bitcast i8* %_1290288 to i8**
  store i8* %_1290004, i8** %_1290647
  %_1290648 = bitcast i8* %_1290002 to i8* (i8*, i8*)*
  %_1290006 = call dereferenceable_or_null(8) i8* %_1290648(i8* dereferenceable_or_null(8) %_1280001, i8* nonnull dereferenceable(16) %_1290003)
  %_1290291 = icmp eq i8* %_1290006, null
  br i1 %_1290291, label %_1290290.0, label %_1290289.0
_1290289.0:
  %_1290649 = bitcast i8* %_1290006 to i8**
  %_1290292 = load i8*, i8** %_1290649
  %_1290293 = icmp eq i8* %_1290292, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28scala.scalanative.unsafe.PtrG4type" to i8*)
  br i1 %_1290293, label %_1290290.0, label %_1290009.0
_1290290.0:
  %_30003 = bitcast i8* %_1290006 to i8*
  ret i8* %_30003
_1290054.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_1290009.0:
  %_1290295 = phi i8* [%_120004, %_1290007.0], [%_120007, %_1290017.0], [%_120009, %_1290026.0], [%_120011, %_1290035.0], [%_120013, %_1290044.0], [%_290002, %_1290057.0], [%_290004, %_1290066.0], [%_290006, %_1290075.0], [%_290008, %_1290084.0], [%_290010, %_1290093.0], [%_1290006, %_1290289.0]
  %_1290296 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_1290007.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_1290017.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_1290026.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_1290035.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_1290044.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_1290057.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_1290066.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_1290075.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_1290084.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_1290093.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28scala.scalanative.unsafe.PtrG4type" to i8*), %_1290289.0]
  %_1290650 = bitcast i8* %_1290295 to i8**
  %_1290297 = load i8*, i8** %_1290650
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_1290297, i8* %_1290296)
  unreachable
_1290278.0:
  %_1290651 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_1290651(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM41scala.scalanative.posix.pwdOps$passwdOps$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(32) i8* @"_SM42java.util.IllegalFormatConversionExceptionD10getMessageL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*), null
  br i1 %_20004, label %_30000.0, label %_40000.0
_30000.0:
  br label %_50000.0
_40000.0:
  br label %_50000.0
_50000.0:
  %_50001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*), %_40000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_30000.0]
  %_210005 = icmp ne i8* %_1, null
  br i1 %_210005, label %_210003.0, label %_210004.0
_210003.0:
  %_210011 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i1, i1, i8*, i16 }*
  %_210012 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8*, i16 }, { i8*, i8*, i8*, i8*, i1, i1, i8*, i16 }* %_210011, i32 0, i32 7
  %_210006 = bitcast i16* %_210012 to i8*
  %_210013 = bitcast i8* %_210006 to i16*
  %_60001 = load i16, i16* %_210013
  %_50004 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8* null, i16 %_60001)
  %_50005 = icmp eq i8* %_50004, null
  br i1 %_50005, label %_70000.0, label %_80000.0
_70000.0:
  br label %_90000.0
_80000.0:
  %_210014 = bitcast i8* bitcast (i8* (i8*)* @"_SM19java.lang.CharacterD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_80001 = call dereferenceable_or_null(32) i8* %_210014(i8* nonnull dereferenceable(16) %_50004)
  br label %_90000.0
_90000.0:
  %_90001 = phi i8* [%_80001, %_80000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_70000.0]
  %_210015 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_90002 = call dereferenceable_or_null(32) i8* %_210015(i8* nonnull dereferenceable(32) %_50001, i8* dereferenceable_or_null(32) %_90001)
  %_90004 = icmp eq i8* %_90002, null
  br i1 %_90004, label %_100000.0, label %_110000.0
_100000.0:
  br label %_120000.0
_110000.0:
  br label %_120000.0
_120000.0:
  %_120001 = phi i8* [%_90002, %_110000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_100000.0]
  %_120005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-221" to i8*), null
  br i1 %_120005, label %_130000.0, label %_140000.0
_130000.0:
  br label %_150000.0
_140000.0:
  br label %_150000.0
_150000.0:
  %_150001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-221" to i8*), %_140000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_130000.0]
  %_210016 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_150002 = call dereferenceable_or_null(32) i8* %_210016(i8* dereferenceable_or_null(32) %_120001, i8* nonnull dereferenceable(32) %_150001)
  %_150004 = icmp eq i8* %_150002, null
  br i1 %_150004, label %_160000.0, label %_170000.0
_160000.0:
  br label %_180000.0
_170000.0:
  br label %_180000.0
_180000.0:
  %_180001 = phi i8* [%_150002, %_170000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_160000.0]
  %_210008 = icmp ne i8* %_1, null
  br i1 %_210008, label %_210007.0, label %_210004.0
_210007.0:
  %_210017 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i1, i1, i8*, i16 }*
  %_210018 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8*, i16 }, { i8*, i8*, i8*, i8*, i1, i1, i8*, i16 }* %_210017, i32 0, i32 6
  %_210009 = bitcast i8** %_210018 to i8*
  %_210019 = bitcast i8* %_210009 to i8**
  %_180002 = load i8*, i8** %_210019, !dereferenceable_or_null !{i64 32}
  %_180003 = call dereferenceable_or_null(32) i8* @"_SM15java.lang.ClassD7getNameL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_180002)
  %_180005 = icmp eq i8* %_180003, null
  br i1 %_180005, label %_190000.0, label %_200000.0
_190000.0:
  br label %_210000.0
_200000.0:
  br label %_210000.0
_210000.0:
  %_210001 = phi i8* [%_180003, %_200000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_190000.0]
  %_210020 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_210002 = call dereferenceable_or_null(32) i8* %_210020(i8* dereferenceable_or_null(32) %_180001, i8* dereferenceable_or_null(32) %_210001)
  ret i8* %_210002
_210004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM42java.util.UnknownFormatConversionExceptionD10getMessageL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-223" to i8*), null
  br i1 %_20004, label %_30000.0, label %_40000.0
_30000.0:
  br label %_50000.0
_40000.0:
  br label %_50000.0
_50000.0:
  %_50001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-223" to i8*), %_40000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_30000.0]
  %_140005 = icmp ne i8* %_1, null
  br i1 %_140005, label %_140003.0, label %_140004.0
_140003.0:
  %_140008 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i1, i1, i8* }*
  %_140009 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8* }, { i8*, i8*, i8*, i8*, i1, i1, i8* }* %_140008, i32 0, i32 6
  %_140006 = bitcast i8** %_140009 to i8*
  %_140010 = bitcast i8* %_140006 to i8**
  %_50002 = load i8*, i8** %_140010, !dereferenceable_or_null !{i64 32}
  %_50004 = icmp eq i8* %_50002, null
  br i1 %_50004, label %_60000.0, label %_70000.0
_60000.0:
  br label %_80000.0
_70000.0:
  br label %_80000.0
_80000.0:
  %_80001 = phi i8* [%_50002, %_70000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_60000.0]
  %_140011 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_80002 = call dereferenceable_or_null(32) i8* %_140011(i8* nonnull dereferenceable(32) %_50001, i8* dereferenceable_or_null(32) %_80001)
  %_80004 = icmp eq i8* %_80002, null
  br i1 %_80004, label %_90000.0, label %_100000.0
_90000.0:
  br label %_110000.0
_100000.0:
  br label %_110000.0
_110000.0:
  %_110001 = phi i8* [%_80002, %_100000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_90000.0]
  %_110005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-225" to i8*), null
  br i1 %_110005, label %_120000.0, label %_130000.0
_120000.0:
  br label %_140000.0
_130000.0:
  br label %_140000.0
_140000.0:
  %_140001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-225" to i8*), %_130000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-177" to i8*), %_120000.0]
  %_140012 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_140002 = call dereferenceable_or_null(32) i8* %_140012(i8* dereferenceable_or_null(32) %_110001, i8* nonnull dereferenceable(32) %_140001)
  ret i8* %_140002
_140004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM43java.util.FormatterImpl$ParserStateMachine$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 179
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 16}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM43java.util.FormatterImpl$ParserStateMachine$G4type" to i8*), i64 16)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM43java.util.FormatterImpl$ParserStateMachine$RE"(i8* dereferenceable_or_null(16) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM43java.util.FormatterImpl$ParserStateMachine$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(16) i8* @"_SM43java.util.FormatterImpl$ParserStateMachine$G4load"()
  %_20005 = bitcast i8* %_20001 to { i8*, i16 }*
  %_20006 = getelementptr { i8*, i16 }, { i8*, i16 }* %_20005, i32 0, i32 1
  %_20004 = bitcast i16* %_20006 to i8*
  %_20007 = bitcast i8* %_20004 to i16*
  store i16 65535, i16* %_20007
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(32) i8* @"_SM43scala.reflect.ManifestFactory$FloatManifestD12runtimeClassL15java.lang.ClassEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM15java.lang.FloatD4TYPEL15java.lang.ClassEo"()
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM43scala.reflect.ManifestFactory$FloatManifestD8newArrayiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM43scala.reflect.ManifestFactory$FloatManifestD8newArrayiLAf_EO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define nonnull dereferenceable(8) i8* @"_SM43scala.reflect.ManifestFactory$FloatManifestD8newArrayiLAf_EO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM37scala.scalanative.runtime.FloatArray$D5allociL36scala.scalanative.runtime.FloatArrayEO"(i8* bitcast ({ i8* }* @"_SM37scala.scalanative.runtime.FloatArray$G8instance" to i8*), i32 %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(32) i8* @"_SM43scala.reflect.ManifestFactory$ShortManifestD12runtimeClassL15java.lang.ClassEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM15java.lang.ShortD4TYPEL15java.lang.ClassEo"()
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM43scala.reflect.ManifestFactory$ShortManifestD8newArrayiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM43scala.reflect.ManifestFactory$ShortManifestD8newArrayiLAs_EO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define nonnull dereferenceable(8) i8* @"_SM43scala.reflect.ManifestFactory$ShortManifestD8newArrayiLAs_EO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM37scala.scalanative.runtime.ShortArray$D5allociL36scala.scalanative.runtime.ShortArrayEO"(i8* bitcast ({ i8* }* @"_SM37scala.scalanative.runtime.ShortArray$G8instance" to i8*), i32 %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(8) i8* @"_SM44scala.collection.mutable.ArraySeq$$$Lambda$1D5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30005 = icmp ne i8* %_1, null
  br i1 %_30005, label %_30003.0, label %_30004.0
_30003.0:
  %_30008 = bitcast i8* %_1 to { i8*, i8* }*
  %_30009 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30008, i32 0, i32 1
  %_30006 = bitcast i8** %_30009 to i8*
  %_30010 = bitcast i8* %_30006 to i8**
  %_30001 = load i8*, i8** %_30010, !dereferenceable_or_null !{i64 24}
  %_30002 = call dereferenceable_or_null(8) i8* @"_SM34scala.collection.mutable.ArraySeq$D21$anonfun$newBuilder$1L16java.lang.ObjectL33scala.collection.mutable.ArraySeqEPT34scala.collection.mutable.ArraySeq$"(i8* dereferenceable_or_null(24) %_30001, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30002
_30004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM44scala.reflect.ManifestFactory$DoubleManifestD12runtimeClassL15java.lang.ClassEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.DoubleD4TYPEL15java.lang.ClassEo"()
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM44scala.reflect.ManifestFactory$DoubleManifestD8newArrayiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM44scala.reflect.ManifestFactory$DoubleManifestD8newArrayiLAd_EO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define nonnull dereferenceable(8) i8* @"_SM44scala.reflect.ManifestFactory$DoubleManifestD8newArrayiLAd_EO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM38scala.scalanative.runtime.DoubleArray$D5allociL37scala.scalanative.runtime.DoubleArrayEO"(i8* bitcast ({ i8* }* @"_SM38scala.scalanative.runtime.DoubleArray$G8instance" to i8*), i32 %_2)
  ret i8* %_30001
}

define nonnull dereferenceable(16) i8* @"_SM45java.util.ScalaOps$JavaIteratorOps$$$Lambda$2D5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30008 = icmp ne i8* %_1, null
  br i1 %_30008, label %_30006.0, label %_30007.0
_30006.0:
  %_30014 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_30015 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_30014, i32 0, i32 1
  %_30009 = bitcast i8** %_30015 to i8*
  %_30016 = bitcast i8* %_30009 to i8**
  %_30001 = load i8*, i8** %_30016, !dereferenceable_or_null !{i64 8}
  %_30011 = icmp ne i8* %_1, null
  br i1 %_30011, label %_30010.0, label %_30007.0
_30010.0:
  %_30017 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_30018 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_30017, i32 0, i32 2
  %_30012 = bitcast i8** %_30018 to i8*
  %_30019 = bitcast i8* %_30012 to i8**
  %_30002 = load i8*, i8** %_30019, !dereferenceable_or_null !{i64 8}
  %_30003 = call i1 @"_SM35java.util.ScalaOps$JavaIteratorOps$D27forall$extension$$anonfun$1L15scala.Function1L16java.lang.ObjectzEPT35java.util.ScalaOps$JavaIteratorOps$"(i8* dereferenceable_or_null(8) %_30001, i8* dereferenceable_or_null(8) %_30002, i8* dereferenceable_or_null(8) %_2)
  %_30005 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToBooleanzL17java.lang.BooleanEO"(i8* null, i1 %_30003)
  ret i8* %_30005
_30007.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM50scala.collection.StrictOptimizedClassTagSeqFactoryD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i32 @"_SM51java.nio.file.attribute.PosixFilePermission$$anon$6D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM51java.nio.file.attribute.PosixFilePermission$$anon$6D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM51java.nio.file.attribute.PosixFilePermission$$anon$6D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define nonnull dereferenceable(8) i8* @"_SM66scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$$$Lambda$3D13apply$mcVI$spiuEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30010 = icmp ne i8* %_1, null
  br i1 %_30010, label %_30008.0, label %_30009.0
_30008.0:
  %_30029 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30030 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30029, i32 0, i32 1
  %_30011 = bitcast i8** %_30030 to i8*
  %_30031 = bitcast i8* %_30011 to i8**
  %_30001 = load i8*, i8** %_30031, !dereferenceable_or_null !{i64 24}
  %_30013 = icmp ne i8* %_1, null
  br i1 %_30013, label %_30012.0, label %_30009.0
_30012.0:
  %_30032 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30033 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30032, i32 0, i32 2
  %_30014 = bitcast i8** %_30033 to i8*
  %_30034 = bitcast i8* %_30014 to i8**
  %_30002 = load i8*, i8** %_30034, !dereferenceable_or_null !{i64 16}
  %_30016 = icmp ne i8* %_1, null
  br i1 %_30016, label %_30015.0, label %_30009.0
_30015.0:
  %_30035 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30036 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30035, i32 0, i32 3
  %_30017 = bitcast i32* %_30036 to i8*
  %_30037 = bitcast i8* %_30017 to i32*
  %_30003 = load i32, i32* %_30037
  %_30019 = icmp ne i8* %_1, null
  br i1 %_30019, label %_30018.0, label %_30009.0
_30018.0:
  %_30038 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30039 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30038, i32 0, i32 4
  %_30020 = bitcast i8** %_30039 to i8*
  %_30040 = bitcast i8* %_30020 to i8**
  %_30004 = load i8*, i8** %_30040, !dereferenceable_or_null !{i64 8}
  %_30022 = icmp ne i8* %_1, null
  br i1 %_30022, label %_30021.0, label %_30009.0
_30021.0:
  %_30041 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30042 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30041, i32 0, i32 5
  %_30023 = bitcast i8** %_30042 to i8*
  %_30043 = bitcast i8* %_30023 to i8**
  %_30005 = load i8*, i8** %_30043, !dereferenceable_or_null !{i64 16}
  %_30025 = icmp ne i8* %_1, null
  br i1 %_30025, label %_30024.0, label %_30009.0
_30024.0:
  %_30044 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30045 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30044, i32 0, i32 6
  %_30026 = bitcast i32* %_30045 to i8*
  %_30046 = bitcast i8* %_30026 to i32*
  %_30006 = load i32, i32* %_30046
  call nonnull dereferenceable(8) i8* @"_SM56scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$D25doubleToString$$anonfun$3L21scala.runtime.LongRefiLAc_L20scala.runtime.IntRefiiuEPT56scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$"(i8* dereferenceable_or_null(24) %_30001, i8* dereferenceable_or_null(16) %_30002, i32 %_30003, i8* dereferenceable_or_null(8) %_30004, i8* dereferenceable_or_null(16) %_30005, i32 %_30006, i32 %_2)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_30009.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}