object Main {
  import scalanative.unsafe._
  import scala.scalanative.libc.{math=>MATH}
  import scala.scalanative.libc.{stdio=>STDIO}
  def main(args: Array[String]): Unit =
    val x1:Int= -6
    val x2:CInt=x1
    val y2:CInt=MATH.abs(x2)
    val y1:Int=y2
    println(y1)
}
