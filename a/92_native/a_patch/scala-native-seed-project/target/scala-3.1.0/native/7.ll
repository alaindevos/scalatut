declare i32 @llvm.eh.typeid.for(i8*)
declare i32 @__gxx_personality_v0(...)
declare i8* @__cxa_begin_catch(i8*)
declare void @__cxa_end_catch()
@_ZTIN11scalanative16ExceptionWrapperE = external constant { i8*, i8*, i8* }

@"_SM7__constG1-0" = private unnamed_addr constant { i8*, i32, i32, [10 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 10, i32 0, [10 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 83, i16 111, i16 109, i16 101 ] }
@"_SM7__constG1-1" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [10 x i16] }* @"_SM7__constG1-0" to i8*), i32 0, i32 10, i32 1763538108 }
@"_SM7__constG1-2" = private unnamed_addr constant [2 x i64] [ i64 0, i64 -1 ]
@"_SM7__constG1-3" = private unnamed_addr constant { i8*, i32, i32, [13 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 13, i32 0, [13 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 79, i16 112, i16 116, i16 105, i16 111, i16 110, i16 36 ] }
@"_SM7__constG1-4" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [13 x i16] }* @"_SM7__constG1-3" to i8*), i32 0, i32 13, i32 -1990880889 }
@"_SM7__constG1-5" = private unnamed_addr constant [1 x i64] [ i64 -1 ]
@"_SM7__constG1-6" = private unnamed_addr constant { i8*, i32, i32, [13 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 13, i32 0, [13 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 80, i16 114, i16 111, i16 100, i16 117, i16 99, i16 116 ] }
@"_SM7__constG1-7" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [13 x i16] }* @"_SM7__constG1-6" to i8*), i32 0, i32 13, i32 -1050879961 }
@"_SM7__constG1-8" = private unnamed_addr constant { i8*, i32, i32, [15 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 15, i32 0, [15 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 114, i16 116, i16 116, i16 105, i16 36 ] }
@"_SM7__constG1-9" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [15 x i16] }* @"_SM7__constG1-8" to i8*), i32 0, i32 15, i32 -487001575 }
@"_SM7__constG2-10" = private unnamed_addr constant { i8*, i32, i32, [15 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 15, i32 0, [15 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 70, i16 117, i16 110, i16 99, i16 116, i16 105, i16 111, i16 110, i16 49 ] }
@"_SM7__constG2-11" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [15 x i16] }* @"_SM7__constG2-10" to i8*), i32 0, i32 15, i32 332932241 }
@"_SM7__constG2-12" = private unnamed_addr constant { i8*, i32, i32, [17 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 17, i32 0, [17 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 83, i16 121, i16 115, i16 116, i16 101, i16 109, i16 36 ] }
@"_SM7__constG2-13" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [17 x i16] }* @"_SM7__constG2-12" to i8*), i32 0, i32 17, i32 -1457272831 }
@"_SM7__constG2-14" = private unnamed_addr constant [6 x i64] [ i64 0, i64 1, i64 2, i64 4, i64 5, i64 -1 ]
@"_SM7__constG2-15" = private unnamed_addr constant { i8*, i32, i32, [18 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 18, i32 0, [18 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 73, i16 116, i16 101, i16 114, i16 97, i16 98, i16 108, i16 101 ] }
@"_SM7__constG2-16" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [18 x i16] }* @"_SM7__constG2-15" to i8*), i32 0, i32 18, i32 1275614662 }
@"_SM7__constG2-17" = private unnamed_addr constant { i8*, i32, i32, [18 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 18, i32 0, [18 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 82, i16 101, i16 97, i16 100, i16 97, i16 98, i16 108, i16 101 ] }
@"_SM7__constG2-18" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [18 x i16] }* @"_SM7__constG2-17" to i8*), i32 0, i32 18, i32 -774060732 }
@"_SM7__constG2-19" = private unnamed_addr constant { i8*, i32, i32, [18 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 18, i32 0, [18 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 73, i16 116, i16 101, i16 114, i16 97, i16 116, i16 111, i16 114 ] }
@"_SM7__constG2-20" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [18 x i16] }* @"_SM7__constG2-19" to i8*), i32 0, i32 18, i32 499831342 }
@"_SM7__constG2-21" = private unnamed_addr constant { i8*, i32, i32, [19 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 19, i32 0, [19 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 105, i16 111, i16 46, i16 73, i16 79, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG2-22" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [19 x i16] }* @"_SM7__constG2-21" to i8*), i32 0, i32 19, i32 -2010664371 }
@"_SM7__constG2-23" = private unnamed_addr constant [4 x i64] [ i64 0, i64 1, i64 2, i64 -1 ]
@"_SM7__constG2-24" = private unnamed_addr constant { i8*, i32, i32, [19 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 19, i32 0, [19 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG2-25" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [19 x i16] }* @"_SM7__constG2-24" to i8*), i32 0, i32 19, i32 72706427 }
@"_SM7__constG2-26" = private unnamed_addr constant { i8*, i32, i32, [20 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 20, i32 0, [20 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 105, i16 111, i16 46, i16 79, i16 117, i16 116, i16 112, i16 117, i16 116, i16 83, i16 116, i16 114, i16 101, i16 97, i16 109 ] }
@"_SM7__constG2-27" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [20 x i16] }* @"_SM7__constG2-26" to i8*), i32 0, i32 20, i32 840782845 }
@"_SM7__constG2-28" = private unnamed_addr constant { i8*, i32, i32, [20 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 20, i32 0, [20 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 83, i16 101, i16 113 ] }
@"_SM7__constG2-29" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [20 x i16] }* @"_SM7__constG2-28" to i8*), i32 0, i32 20, i32 -911149417 }
@"_SM7__constG2-30" = private unnamed_addr constant { i8*, i32, i32, [21 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 21, i32 0, [21 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 65, i16 98, i16 115, i16 116, i16 114, i16 97, i16 99, i16 116, i16 77, i16 97, i16 112 ] }
@"_SM7__constG2-31" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [21 x i16] }* @"_SM7__constG2-30" to i8*), i32 0, i32 21, i32 1860735002 }
@"_SM7__constG2-32" = private unnamed_addr constant { i8*, i32, i32, [21 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 21, i32 0, [21 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 65, i16 114, i16 114, i16 97, i16 121, i16 115, i16 36 ] }
@"_SM7__constG2-33" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [21 x i16] }* @"_SM7__constG2-32" to i8*), i32 0, i32 21, i32 -590214836 }
@"_SM7__constG2-34" = private unnamed_addr constant { i8*, i32, i32, [21 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 21, i32 0, [21 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 73, i16 110, i16 116, i16 82, i16 101, i16 102, i16 36 ] }
@"_SM7__constG2-35" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [21 x i16] }* @"_SM7__constG2-34" to i8*), i32 0, i32 21, i32 2101711266 }
@"_SM7__constG2-36" = private unnamed_addr constant { i8*, i32, i32, [23 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 23, i32 0, [23 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 71, i16 101, i16 110, i16 72, i16 101, i16 97, i16 112, i16 66, i16 117, i16 102, i16 102, i16 101, i16 114, i16 36 ] }
@"_SM7__constG2-37" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [23 x i16] }* @"_SM7__constG2-36" to i8*), i32 0, i32 23, i32 398741870 }
@"_SM7__constG2-38" = private unnamed_addr constant { i8*, i32, i32, [23 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 23, i32 0, [23 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 77, i16 97, i16 112, i16 79, i16 112, i16 115 ] }
@"_SM7__constG2-39" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [23 x i16] }* @"_SM7__constG2-38" to i8*), i32 0, i32 23, i32 -34390402 }
@"_SM7__constG2-40" = private unnamed_addr constant { i8*, i32, i32, [24 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 24, i32 0, [24 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 105, i16 111, i16 46, i16 82, i16 101, i16 97, i16 100, i16 101, i16 114, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49 ] }
@"_SM7__constG2-41" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [24 x i16] }* @"_SM7__constG2-40" to i8*), i32 0, i32 24, i32 -1848376749 }
@"_SM7__constG2-42" = private unnamed_addr constant { i8*, i32, i32, [24 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 24, i32 0, [24 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 105, i16 111, i16 46, i16 87, i16 114, i16 105, i16 116, i16 101, i16 114, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49 ] }
@"_SM7__constG2-43" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [24 x i16] }* @"_SM7__constG2-42" to i8*), i32 0, i32 24, i32 -2131725821 }
@"_SM7__constG2-44" = private unnamed_addr constant { i8*, i32, i32, [24 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 24, i32 0, [24 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 76, i16 105, i16 110, i16 107, i16 79, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG2-45" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [24 x i16] }* @"_SM7__constG2-44" to i8*), i32 0, i32 24, i32 -638948549 }
@"_SM7__constG2-46" = private unnamed_addr constant [2 x i64] [ i64 1, i64 -1 ]
@"_SM7__constG2-47" = private unnamed_addr constant { i8*, i32, i32, [24 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 24, i32 0, [24 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 79, i16 112, i16 101, i16 110, i16 79, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG2-48" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [24 x i16] }* @"_SM7__constG2-47" to i8*), i32 0, i32 24, i32 -1133648501 }
@"_SM7__constG2-49" = private unnamed_addr constant { i8*, i32, i32, [25 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 25, i32 0, [25 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 73, i16 116, i16 101, i16 114, i16 97, i16 116, i16 111, i16 114 ] }
@"_SM7__constG2-50" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [25 x i16] }* @"_SM7__constG2-49" to i8*), i32 0, i32 25, i32 -1966051018 }
@"_SM7__constG2-51" = private unnamed_addr constant { i8*, i32, i32, [26 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 26, i32 0, [26 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 83, i16 116, i16 114, i16 105, i16 110, i16 103, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 55 ] }
@"_SM7__constG2-52" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [26 x i16] }* @"_SM7__constG2-51" to i8*), i32 0, i32 26, i32 -1555388577 }
@"_SM7__constG2-53" = private unnamed_addr constant { i8*, i32, i32, [27 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 27, i32 0, [27 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 83, i16 121, i16 115, i16 116, i16 101, i16 109, i16 36, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 54 ] }
@"_SM7__constG2-54" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [27 x i16] }* @"_SM7__constG2-53" to i8*), i32 0, i32 27, i32 -276044294 }
@"_SM7__constG2-55" = private unnamed_addr constant { i8*, i32, i32, [28 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 28, i32 0, [28 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 83, i16 121, i16 115, i16 116, i16 101, i16 109, i16 36, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49, i16 50 ] }
@"_SM7__constG2-56" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [28 x i16] }* @"_SM7__constG2-55" to i8*), i32 0, i32 28, i32 32561373 }
@"_SM7__constG2-57" = private unnamed_addr constant { i8*, i32, i32, [28 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 28, i32 0, [28 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 65, i16 98, i16 115, i16 116, i16 114, i16 97, i16 99, i16 116, i16 83, i16 101, i16 113 ] }
@"_SM7__constG2-58" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [28 x i16] }* @"_SM7__constG2-57" to i8*), i32 0, i32 28, i32 1879984917 }
@"_SM7__constG2-59" = private unnamed_addr constant { i8*, i32, i32, [28 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 28, i32 0, [28 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 83, i16 101, i16 113 ] }
@"_SM7__constG2-60" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [28 x i16] }* @"_SM7__constG2-59" to i8*), i32 0, i32 28, i32 -876839985 }
@"_SM7__constG2-61" = private unnamed_addr constant { i8*, i32, i32, [28 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 28, i32 0, [28 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 101, i16 102, i16 108, i16 101, i16 99, i16 116, i16 46, i16 65, i16 110, i16 121, i16 86, i16 97, i16 108, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116 ] }
@"_SM7__constG2-62" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [28 x i16] }* @"_SM7__constG2-61" to i8*), i32 0, i32 28, i32 1346320381 }
@"_SM7__constG2-63" = private unnamed_addr constant { i8*, i32, i32, [28 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 28, i32 0, [28 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 83, i16 99, i16 97, i16 108, i16 97, i16 51, i16 82, i16 117, i16 110, i16 84, i16 105, i16 109, i16 101, i16 36 ] }
@"_SM7__constG2-64" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [28 x i16] }* @"_SM7__constG2-63" to i8*), i32 0, i32 28, i32 -1618699113 }
@"_SM7__constG2-65" = private unnamed_addr constant { i8*, i32, i32, [28 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 28, i32 0, [28 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 80, i16 116, i16 114 ] }
@"_SM7__constG2-66" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [28 x i16] }* @"_SM7__constG2-65" to i8*), i32 0, i32 28, i32 -310984225 }
@"_SM7__constG2-67" = private unnamed_addr constant { i8*, i32, i32, [29 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 29, i32 0, [29 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 99, i16 104, i16 97, i16 114, i16 115, i16 101, i16 116, i16 46, i16 67, i16 111, i16 100, i16 101, i16 114, i16 82, i16 101, i16 115, i16 117, i16 108, i16 116, i16 36 ] }
@"_SM7__constG2-68" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [29 x i16] }* @"_SM7__constG2-67" to i8*), i32 0, i32 29, i32 1900320554 }
@"_SM7__constG2-69" = private unnamed_addr constant [13 x i64] [ i64 0, i64 1, i64 2, i64 3, i64 4, i64 5, i64 6, i64 7, i64 8, i64 9, i64 10, i64 11, i64 -1 ]
@"_SM7__constG2-70" = private unnamed_addr constant { i8*, i32, i32, [29 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 29, i32 0, [29 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 80, i16 111, i16 115, i16 105, i16 120, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110, i16 36 ] }
@"_SM7__constG2-71" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [29 x i16] }* @"_SM7__constG2-70" to i8*), i32 0, i32 29, i32 -1167107604 }
@"_SM7__constG2-72" = private unnamed_addr constant { i8*, i32, i32, [29 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 29, i32 0, [29 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 72, i16 97, i16 115, i16 104, i16 116, i16 97, i16 98, i16 108, i16 101, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49 ] }
@"_SM7__constG2-73" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [29 x i16] }* @"_SM7__constG2-72" to i8*), i32 0, i32 29, i32 -1048797964 }
@"_SM7__constG2-74" = private unnamed_addr constant { i8*, i32, i32, [29 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 29, i32 0, [29 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 99, i16 111, i16 110, i16 99, i16 117, i16 114, i16 114, i16 101, i16 110, i16 116, i16 46, i16 69, i16 120, i16 101, i16 99, i16 117, i16 116, i16 111, i16 114 ] }
@"_SM7__constG2-75" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [29 x i16] }* @"_SM7__constG2-74" to i8*), i32 0, i32 29, i32 2093989290 }
@"_SM7__constG2-76" = private unnamed_addr constant { i8*, i32, i32, [30 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 30, i32 0, [30 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 109, i16 97, i16 116, i16 104, i16 46, i16 82, i16 111, i16 117, i16 110, i16 100, i16 105, i16 110, i16 103, i16 77, i16 111, i16 100, i16 101, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 55 ] }
@"_SM7__constG2-77" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [30 x i16] }* @"_SM7__constG2-76" to i8*), i32 0, i32 30, i32 -1496511568 }
@"_SM7__constG2-78" = private unnamed_addr constant { i8*, i32, i32, [30 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 30, i32 0, [30 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 77, i16 97, i16 112, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49 ] }
@"_SM7__constG2-79" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [30 x i16] }* @"_SM7__constG2-78" to i8*), i32 0, i32 30, i32 -1891966520 }
@"_SM7__constG2-80" = private unnamed_addr constant [3 x i64] [ i64 0, i64 1, i64 -1 ]
@"_SM7__constG2-81" = private unnamed_addr constant { i8*, i32, i32, [30 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 30, i32 0, [30 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 77, i16 97, i16 112 ] }
@"_SM7__constG2-82" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [30 x i16] }* @"_SM7__constG2-81" to i8*), i32 0, i32 30, i32 -1668267224 }
@"_SM7__constG2-83" = private unnamed_addr constant { i8*, i32, i32, [31 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 31, i32 0, [31 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 78, i16 117, i16 109, i16 98, i16 101, i16 114, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG2-84" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [31 x i16] }* @"_SM7__constG2-83" to i8*), i32 0, i32 31, i32 1641150139 }
@"_SM7__constG2-85" = private unnamed_addr constant { i8*, i32, i32, [32 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 32, i32 0, [32 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 76, i16 105, i16 110, i16 107, i16 79, i16 112, i16 116, i16 105, i16 111, i16 110, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 49 ] }
@"_SM7__constG2-86" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [32 x i16] }* @"_SM7__constG2-85" to i8*), i32 0, i32 32, i32 -840439532 }
@"_SM7__constG2-87" = private unnamed_addr constant { i8*, i32, i32, [32 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 32, i32 0, [32 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 66, i16 117, i16 102, i16 102, i16 101, i16 114, i16 36 ] }
@"_SM7__constG2-88" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [32 x i16] }* @"_SM7__constG2-87" to i8*), i32 0, i32 32, i32 -167478444 }
@"_SM7__constG2-89" = private unnamed_addr constant { i8*, i32, i32, [32 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 32, i32 0, [32 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 72, i16 97, i16 115, i16 104, i16 77, i16 97, i16 112 ] }
@"_SM7__constG2-90" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [32 x i16] }* @"_SM7__constG2-89" to i8*), i32 0, i32 32, i32 302035166 }
@"_SM7__constG2-91" = private unnamed_addr constant [2 x i64] [ i64 2, i64 -1 ]
@"_SM7__constG2-92" = private unnamed_addr constant { i8*, i32, i32, [33 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 33, i32 0, [33 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 82, i16 97, i16 110, i16 103, i16 101, i16 36 ] }
@"_SM7__constG2-93" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [33 x i16] }* @"_SM7__constG2-92" to i8*), i32 0, i32 33, i32 -2139099845 }
@"_SM7__constG2-94" = private unnamed_addr constant { i8*, i32, i32, [33 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 33, i32 0, [33 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 112, i16 97, i16 99, i16 107, i16 97, i16 103, i16 101, i16 36 ] }
@"_SM7__constG2-95" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [33 x i16] }* @"_SM7__constG2-94" to i8*), i32 0, i32 33, i32 -609578963 }
@"_SM7__constG2-96" = private unnamed_addr constant { i8*, i32, i32, [34 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 34, i32 0, [34 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 73, i16 110, i16 100, i16 101, i16 120, i16 101, i16 100, i16 83, i16 101, i16 113, i16 86, i16 105, i16 101, i16 119, i16 36, i16 73, i16 100 ] }
@"_SM7__constG2-97" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [34 x i16] }* @"_SM7__constG2-96" to i8*), i32 0, i32 34, i32 1298562628 }
@"_SM7__constG2-98" = private unnamed_addr constant { i8*, i32, i32, [34 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 34, i32 0, [34 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 73, i16 116, i16 101, i16 114, i16 97, i16 116, i16 111, i16 114, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 49, i16 57 ] }
@"_SM7__constG2-99" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [34 x i16] }* @"_SM7__constG2-98" to i8*), i32 0, i32 34, i32 -735658486 }
@"_SM7__constG3-100" = private unnamed_addr constant { i8*, i32, i32, [34 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 34, i32 0, [34 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 86, i16 101, i16 99, i16 116, i16 111, i16 114, i16 51 ] }
@"_SM7__constG3-101" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [34 x i16] }* @"_SM7__constG3-100" to i8*), i32 0, i32 34, i32 1767186204 }
@"_SM7__constG3-102" = private unnamed_addr constant [6 x i64] [ i64 0, i64 2, i64 4, i64 6, i64 7, i64 -1 ]
@"_SM7__constG3-103" = private unnamed_addr constant { i8*, i32, i32, [34 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 34, i32 0, [34 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 67, i16 70, i16 117, i16 110, i16 99, i16 80, i16 116, i16 114, i16 50 ] }
@"_SM7__constG3-104" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [34 x i16] }* @"_SM7__constG3-103" to i8*), i32 0, i32 34, i32 1379985308 }
@"_SM7__constG3-105" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 67, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 115, i16 36, i16 87, i16 114, i16 97, i16 112, i16 112, i16 101, i16 100, i16 69, i16 113, i16 117, i16 97, i16 108, i16 115 ] }
@"_SM7__constG3-106" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-105" to i8*), i32 0, i32 35, i32 -2122945035 }
@"_SM7__constG3-107" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 67, i16 108, i16 97, i16 115, i16 115, i16 84, i16 97, i16 103, i16 83, i16 101, i16 113, i16 70, i16 97, i16 99, i16 116, i16 111, i16 114, i16 121 ] }
@"_SM7__constG3-108" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-107" to i8*), i32 0, i32 35, i32 1137921813 }
@"_SM7__constG3-109" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 67, i16 70, i16 117, i16 110, i16 99, i16 80, i16 116, i16 114, i16 49, i16 50 ] }
@"_SM7__constG3-110" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-109" to i8*), i32 0, i32 35, i32 -170128393 }
@"_SM7__constG3-111" = private unnamed_addr constant { i8*, i32, i32, [35 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 35, i32 0, [35 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 67, i16 70, i16 117, i16 110, i16 99, i16 80, i16 116, i16 114, i16 51, i16 36 ] }
@"_SM7__constG3-112" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [35 x i16] }* @"_SM7__constG3-111" to i8*), i32 0, i32 35, i32 -170128345 }
@"_SM7__constG3-113" = private unnamed_addr constant { i8*, i32, i32, [36 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 36, i32 0, [36 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 65, i16 114, i16 114, i16 97, i16 121, i16 83, i16 101, i16 113, i16 36 ] }
@"_SM7__constG3-114" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [36 x i16] }* @"_SM7__constG3-113" to i8*), i32 0, i32 36, i32 2129458794 }
@"_SM7__constG3-115" = private unnamed_addr constant [3 x i64] [ i64 0, i64 2, i64 -1 ]
@"_SM7__constG3-116" = private unnamed_addr constant { i8*, i32, i32, [36 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 36, i32 0, [36 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 65, i16 98, i16 115, i16 116, i16 114, i16 97, i16 99, i16 116, i16 77, i16 97, i16 112 ] }
@"_SM7__constG3-117" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [36 x i16] }* @"_SM7__constG3-116" to i8*), i32 0, i32 36, i32 868024650 }
@"_SM7__constG3-118" = private unnamed_addr constant { i8*, i32, i32, [36 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 36, i32 0, [36 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 67, i16 104, i16 97, i16 114, i16 65, i16 114, i16 114, i16 97, i16 121, i16 36 ] }
@"_SM7__constG3-119" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [36 x i16] }* @"_SM7__constG3-118" to i8*), i32 0, i32 36, i32 -819858896 }
@"_SM7__constG3-120" = private unnamed_addr constant { i8*, i32, i32, [36 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 36, i32 0, [36 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 67, i16 70, i16 117, i16 110, i16 99, i16 80, i16 116, i16 114, i16 49, i16 55, i16 36 ] }
@"_SM7__constG3-121" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [36 x i16] }* @"_SM7__constG3-120" to i8*), i32 0, i32 36, i32 -979012696 }
@"_SM7__constG3-122" = private unnamed_addr constant { i8*, i32, i32, [36 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 36, i32 0, [36 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 67, i16 70, i16 117, i16 110, i16 99, i16 80, i16 116, i16 114, i16 50, i16 48, i16 36 ] }
@"_SM7__constG3-123" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [36 x i16] }* @"_SM7__constG3-122" to i8*), i32 0, i32 36, i32 -979011952 }
@"_SM7__constG3-124" = private unnamed_addr constant { i8*, i32, i32, [37 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 37, i32 0, [37 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 70, i16 105, i16 108, i16 101, i16 86, i16 105, i16 115, i16 105, i16 116, i16 79, i16 112, i16 116, i16 105, i16 111, i16 110, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 49 ] }
@"_SM7__constG3-125" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [37 x i16] }* @"_SM7__constG3-124" to i8*), i32 0, i32 37, i32 649292977 }
@"_SM7__constG3-126" = private unnamed_addr constant { i8*, i32, i32, [37 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 37, i32 0, [37 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 70, i16 105, i16 108, i16 101, i16 86, i16 105, i16 115, i16 105, i16 116, i16 82, i16 101, i16 115, i16 117, i16 108, i16 116, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 49 ] }
@"_SM7__constG3-127" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [37 x i16] }* @"_SM7__constG3-126" to i8*), i32 0, i32 37, i32 477135961 }
@"_SM7__constG3-128" = private unnamed_addr constant { i8*, i32, i32, [37 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 37, i32 0, [37 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 99, i16 111, i16 110, i16 99, i16 117, i16 114, i16 114, i16 101, i16 110, i16 116, i16 46, i16 84, i16 105, i16 109, i16 101, i16 85, i16 110, i16 105, i16 116, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 55 ] }
@"_SM7__constG3-129" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [37 x i16] }* @"_SM7__constG3-128" to i8*), i32 0, i32 37, i32 -873736281 }
@"_SM7__constG3-130" = private unnamed_addr constant { i8*, i32, i32, [37 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 37, i32 0, [37 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 86, i16 101, i16 99, i16 116, i16 111, i16 114, i16 73, i16 109, i16 112, i16 108 ] }
@"_SM7__constG3-131" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [37 x i16] }* @"_SM7__constG3-130" to i8*), i32 0, i32 37, i32 -1464147273 }
@"_SM7__constG3-132" = private unnamed_addr constant { i8*, i32, i32, [37 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 37, i32 0, [37 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 68, i16 111, i16 117, i16 98, i16 108, i16 101, i16 65, i16 114, i16 114, i16 97, i16 121 ] }
@"_SM7__constG3-133" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [37 x i16] }* @"_SM7__constG3-132" to i8*), i32 0, i32 37, i32 958077081 }
@"_SM7__constG3-134" = private unnamed_addr constant { i8*, i32, i32, [38 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 38, i32 0, [38 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 117, i16 110, i16 115, i16 97, i16 102, i16 101, i16 46, i16 84, i16 97, i16 103, i16 36, i16 67, i16 83, i16 116, i16 114, i16 117, i16 99, i16 116, i16 53, i16 36 ] }
@"_SM7__constG3-135" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [38 x i16] }* @"_SM7__constG3-134" to i8*), i32 0, i32 38, i32 -1984714834 }
@"_SM7__constG3-136" = private unnamed_addr constant { i8*, i32, i32, [40 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 40, i32 0, [40 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 83, i16 116, i16 97, i16 110, i16 100, i16 97, i16 114, i16 100, i16 79, i16 112, i16 101, i16 110, i16 79, i16 112, i16 116, i16 105, i16 111, i16 110, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 55 ] }
@"_SM7__constG3-137" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [40 x i16] }* @"_SM7__constG3-136" to i8*), i32 0, i32 40, i32 1087941895 }
@"_SM7__constG3-138" = private unnamed_addr constant { i8*, i32, i32, [40 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 40, i32 0, [40 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 77, i16 97, i16 112, i16 36, i16 69, i16 109, i16 112, i16 116, i16 121, i16 77, i16 97, i16 112, i16 36 ] }
@"_SM7__constG3-139" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [40 x i16] }* @"_SM7__constG3-138" to i8*), i32 0, i32 40, i32 -1003222375 }
@"_SM7__constG3-140" = private unnamed_addr constant { i8*, i32, i32, [41 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 41, i32 0, [41 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 108, i16 97, i16 110, i16 103, i16 46, i16 83, i16 116, i16 114, i16 105, i16 110, i16 103, i16 73, i16 110, i16 100, i16 101, i16 120, i16 79, i16 117, i16 116, i16 79, i16 102, i16 66, i16 111, i16 117, i16 110, i16 100, i16 115, i16 69, i16 120, i16 99, i16 101, i16 112, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-141" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [41 x i16] }* @"_SM7__constG3-140" to i8*), i32 0, i32 41, i32 -1383341662 }
@"_SM7__constG3-142" = private unnamed_addr constant { i8*, i32, i32, [41 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 41, i32 0, [41 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 101, i16 102, i16 108, i16 101, i16 99, i16 116, i16 46, i16 67, i16 108, i16 97, i16 115, i16 115, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116, i16 68, i16 101, i16 112, i16 114, i16 101, i16 99, i16 97, i16 116, i16 101, i16 100, i16 65, i16 112, i16 105, i16 115 ] }
@"_SM7__constG3-143" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [41 x i16] }* @"_SM7__constG3-142" to i8*), i32 0, i32 41, i32 -1500824294 }
@"_SM7__constG3-144" = private unnamed_addr constant { i8*, i32, i32, [42 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 42, i32 0, [42 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 65, i16 114, i16 114, i16 97, i16 121, i16 83, i16 101, i16 113, i16 36, i16 111, i16 102, i16 67, i16 104, i16 97, i16 114 ] }
@"_SM7__constG3-145" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [42 x i16] }* @"_SM7__constG3-144" to i8*), i32 0, i32 42, i32 -2057809737 }
@"_SM7__constG3-146" = private unnamed_addr constant { i8*, i32, i32, [42 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 42, i32 0, [42 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 101, i16 102, i16 108, i16 101, i16 99, i16 116, i16 46, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116, i16 70, i16 97, i16 99, i16 116, i16 111, i16 114, i16 121, i16 36, i16 85, i16 110, i16 105, i16 116, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116 ] }
@"_SM7__constG3-147" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [42 x i16] }* @"_SM7__constG3-146" to i8*), i32 0, i32 42, i32 610522997 }
@"_SM7__constG3-148" = private unnamed_addr constant { i8*, i32, i32, [43 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 43, i32 0, [43 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 117, i16 116, i16 105, i16 108, i16 46, i16 70, i16 111, i16 114, i16 109, i16 97, i16 116, i16 116, i16 101, i16 114, i16 67, i16 111, i16 109, i16 112, i16 97, i16 110, i16 105, i16 111, i16 110, i16 73, i16 109, i16 112, i16 108, i16 36, i16 76, i16 111, i16 99, i16 97, i16 108, i16 101, i16 73, i16 110, i16 102, i16 111 ] }
@"_SM7__constG3-149" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [43 x i16] }* @"_SM7__constG3-148" to i8*), i32 0, i32 43, i32 778654666 }
@"_SM7__constG3-150" = private unnamed_addr constant { i8*, i32, i32, [43 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 43, i32 0, [43 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 83, i16 116, i16 114, i16 105, i16 99, i16 116, i16 79, i16 112, i16 116, i16 105, i16 109, i16 105, i16 122, i16 101, i16 100, i16 73, i16 116, i16 101, i16 114, i16 97, i16 98, i16 108, i16 101, i16 79, i16 112, i16 115 ] }
@"_SM7__constG3-151" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [43 x i16] }* @"_SM7__constG3-150" to i8*), i32 0, i32 43, i32 993726570 }
@"_SM7__constG3-152" = private unnamed_addr constant { i8*, i32, i32, [43 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 43, i32 0, [43 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 65, i16 114, i16 114, i16 97, i16 121, i16 66, i16 117, i16 105, i16 108, i16 100, i16 101, i16 114, i16 36, i16 111, i16 102, i16 82, i16 101, i16 102 ] }
@"_SM7__constG3-153" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [43 x i16] }* @"_SM7__constG3-152" to i8*), i32 0, i32 43, i32 281828778 }
@"_SM7__constG3-154" = private unnamed_addr constant [3 x i64] [ i64 1, i64 2, i64 -1 ]
@"_SM7__constG3-155" = private unnamed_addr constant { i8*, i32, i32, [44 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 44, i32 0, [44 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 73, i16 116, i16 101, i16 114, i16 97, i16 116, i16 111, i16 114, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 50, i16 49, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49 ] }
@"_SM7__constG3-156" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [44 x i16] }* @"_SM7__constG3-155" to i8*), i32 0, i32 44, i32 543798293 }
@"_SM7__constG3-157" = private unnamed_addr constant { i8*, i32, i32, [44 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 44, i32 0, [44 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 65, i16 114, i16 114, i16 97, i16 121, i16 66, i16 117, i16 105, i16 108, i16 100, i16 101, i16 114, i16 36, i16 111, i16 102, i16 76, i16 111, i16 110, i16 103 ] }
@"_SM7__constG3-158" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [44 x i16] }* @"_SM7__constG3-157" to i8*), i32 0, i32 44, i32 146588741 }
@"_SM7__constG3-159" = private unnamed_addr constant { i8*, i32, i32, [44 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 44, i32 0, [44 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 65, i16 114, i16 114, i16 97, i16 121, i16 66, i16 117, i16 105, i16 108, i16 100, i16 101, i16 114, i16 36, i16 111, i16 102, i16 85, i16 110, i16 105, i16 116 ] }
@"_SM7__constG3-160" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [44 x i16] }* @"_SM7__constG3-159" to i8*), i32 0, i32 44, i32 146855757 }
@"_SM7__constG3-161" = private unnamed_addr constant { i8*, i32, i32, [44 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 44, i32 0, [44 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 114, i16 101, i16 102, i16 108, i16 101, i16 99, i16 116, i16 46, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116, i16 70, i16 97, i16 99, i16 116, i16 111, i16 114, i16 121, i16 36, i16 83, i16 104, i16 111, i16 114, i16 116, i16 77, i16 97, i16 110, i16 105, i16 102, i16 101, i16 115, i16 116, i16 36 ] }
@"_SM7__constG3-162" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [44 x i16] }* @"_SM7__constG3-161" to i8*), i32 0, i32 44, i32 -1385411109 }
@"_SM7__constG3-163" = private unnamed_addr constant { i8*, i32, i32, [46 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 46, i32 0, [46 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 65, i16 114, i16 114, i16 97, i16 121, i16 79, i16 112, i16 115, i16 36, i16 65, i16 114, i16 114, i16 97, i16 121, i16 73, i16 116, i16 101, i16 114, i16 97, i16 116, i16 111, i16 114, i16 36, i16 109, i16 99, i16 68, i16 36, i16 115, i16 112 ] }
@"_SM7__constG3-164" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [46 x i16] }* @"_SM7__constG3-163" to i8*), i32 0, i32 46, i32 1590287667 }
@"_SM7__constG3-165" = private unnamed_addr constant { i8*, i32, i32, [4 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 4, i32 0, [4 x i16] [ i16 77, i16 97, i16 105, i16 110 ] }
@"_SM7__constG3-166" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [4 x i16] }* @"_SM7__constG3-165" to i8*), i32 0, i32 4, i32 2390489 }
@"_SM7__constG3-167" = private unnamed_addr constant { i8*, i32, i32, [50 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 50, i32 0, [50 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 99, i16 104, i16 97, i16 110, i16 110, i16 101, i16 108, i16 115, i16 46, i16 115, i16 112, i16 105, i16 46, i16 65, i16 98, i16 115, i16 116, i16 114, i16 97, i16 99, i16 116, i16 73, i16 110, i16 116, i16 101, i16 114, i16 114, i16 117, i16 112, i16 116, i16 105, i16 98, i16 108, i16 101, i16 67, i16 104, i16 97, i16 110, i16 110, i16 101, i16 108 ] }
@"_SM7__constG3-168" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [50 x i16] }* @"_SM7__constG3-167" to i8*), i32 0, i32 50, i32 1890665130 }
@"_SM7__constG3-169" = private unnamed_addr constant { i8*, i32, i32, [50 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 50, i32 0, [50 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 99, i16 111, i16 108, i16 108, i16 101, i16 99, i16 116, i16 105, i16 111, i16 110, i16 46, i16 105, i16 109, i16 109, i16 117, i16 116, i16 97, i16 98, i16 108, i16 101, i16 46, i16 86, i16 101, i16 99, i16 116, i16 111, i16 114, i16 66, i16 117, i16 105, i16 108, i16 100, i16 101, i16 114, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 49 ] }
@"_SM7__constG3-170" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [50 x i16] }* @"_SM7__constG3-169" to i8*), i32 0, i32 50, i32 -776781352 }
@"_SM7__constG3-171" = private unnamed_addr constant { i8*, i32, i32, [51 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 51, i32 0, [51 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 97, i16 116, i16 116, i16 114, i16 105, i16 98, i16 117, i16 116, i16 101, i16 46, i16 80, i16 111, i16 115, i16 105, i16 120, i16 70, i16 105, i16 108, i16 101, i16 80, i16 101, i16 114, i16 109, i16 105, i16 115, i16 115, i16 105, i16 111, i16 110, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 49 ] }
@"_SM7__constG3-172" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [51 x i16] }* @"_SM7__constG3-171" to i8*), i32 0, i32 51, i32 489634473 }
@"_SM7__constG3-173" = private unnamed_addr constant { i8*, i32, i32, [51 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 51, i32 0, [51 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 110, i16 105, i16 111, i16 46, i16 102, i16 105, i16 108, i16 101, i16 46, i16 97, i16 116, i16 116, i16 114, i16 105, i16 98, i16 117, i16 116, i16 101, i16 46, i16 80, i16 111, i16 115, i16 105, i16 120, i16 70, i16 105, i16 108, i16 101, i16 80, i16 101, i16 114, i16 109, i16 105, i16 115, i16 115, i16 105, i16 111, i16 110, i16 36, i16 36, i16 97, i16 110, i16 111, i16 110, i16 36, i16 57 ] }
@"_SM7__constG3-174" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [51 x i16] }* @"_SM7__constG3-173" to i8*), i32 0, i32 51, i32 489634481 }
@"_SM7__constG3-175" = private unnamed_addr constant { i8*, i32, i32, [65 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 65, i32 0, [65 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 105, i16 101, i16 101, i16 101, i16 55, i16 53, i16 52, i16 116, i16 111, i16 115, i16 116, i16 114, i16 105, i16 110, i16 103, i16 46, i16 114, i16 121, i16 117, i16 46, i16 82, i16 121, i16 117, i16 70, i16 108, i16 111, i16 97, i16 116, i16 36, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 50 ] }
@"_SM7__constG3-176" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [65 x i16] }* @"_SM7__constG3-175" to i8*), i32 0, i32 65, i32 516599316 }
@"_SM7__constG3-177" = private unnamed_addr constant [5 x i64] [ i64 0, i64 1, i64 3, i64 4, i64 -1 ]
@"_SM7__constG3-178" = private unnamed_addr constant { i8*, i32, i32, [66 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 66, i32 0, [66 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 105, i16 101, i16 101, i16 101, i16 55, i16 53, i16 52, i16 116, i16 111, i16 115, i16 116, i16 114, i16 105, i16 110, i16 103, i16 46, i16 114, i16 121, i16 117, i16 46, i16 82, i16 121, i16 117, i16 68, i16 111, i16 117, i16 98, i16 108, i16 101, i16 36, i16 36, i16 36, i16 76, i16 97, i16 109, i16 98, i16 100, i16 97, i16 36, i16 54 ] }
@"_SM7__constG3-179" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [66 x i16] }* @"_SM7__constG3-178" to i8*), i32 0, i32 66, i32 581473589 }
@"_SM7__constG3-180" = private unnamed_addr constant { i8*, i32, i32, [75 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 75, i32 0, [75 x i16] [ i16 115, i16 99, i16 97, i16 108, i16 97, i16 46, i16 115, i16 99, i16 97, i16 108, i16 97, i16 110, i16 97, i16 116, i16 105, i16 118, i16 101, i16 46, i16 114, i16 117, i16 110, i16 116, i16 105, i16 109, i16 101, i16 46, i16 105, i16 101, i16 101, i16 101, i16 55, i16 53, i16 52, i16 116, i16 111, i16 115, i16 116, i16 114, i16 105, i16 110, i16 103, i16 46, i16 114, i16 121, i16 117, i16 46, i16 82, i16 121, i16 117, i16 82, i16 111, i16 117, i16 110, i16 100, i16 105, i16 110, i16 103, i16 77, i16 111, i16 100, i16 101, i16 36, i16 67, i16 111, i16 110, i16 115, i16 101, i16 114, i16 118, i16 97, i16 116, i16 105, i16 118, i16 101, i16 36 ] }
@"_SM7__constG3-181" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [75 x i16] }* @"_SM7__constG3-180" to i8*), i32 0, i32 75, i32 1742138305 }
@"_SM7__constG3-182" = private unnamed_addr constant { i8*, i32, i32, [4 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 4, i32 0, [4 x i16] [ i16 83, i16 111, i16 109, i16 101 ] }
@"_SM7__constG3-183" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [4 x i16] }* @"_SM7__constG3-182" to i8*), i32 0, i32 4, i32 2582804 }
@"_SM7__constG3-184" = private unnamed_addr constant { i8*, i32, i32, [11 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 11, i32 0, [11 x i16] [ i16 60, i16 102, i16 117, i16 110, i16 99, i16 116, i16 105, i16 111, i16 110, i16 49, i16 62 ] }
@"_SM7__constG3-185" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [11 x i16] }* @"_SM7__constG3-184" to i8*), i32 0, i32 11, i32 -505689791 }
@"_SM7__constG3-186" = private unnamed_addr constant { i8*, i32, i32, [7 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 7, i32 0, [7 x i16] [ i16 84, i16 69, i16 77, i16 80, i16 68, i16 73, i16 82 ] }
@"_SM7__constG3-187" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [7 x i16] }* @"_SM7__constG3-186" to i8*), i32 0, i32 7, i32 -710128615 }
@"_SM7__constG3-188" = private unnamed_addr constant { i8*, i32, i32, [3 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 3, i32 0, [3 x i16] [ i16 84, i16 77, i16 80 ] }
@"_SM7__constG3-189" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [3 x i16] }* @"_SM7__constG3-188" to i8*), i32 0, i32 3, i32 83191 }
@"_SM7__constG3-190" = private unnamed_addr constant { i8*, i32, i32, [4 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 4, i32 0, [4 x i16] [ i16 84, i16 69, i16 77, i16 80 ] }
@"_SM7__constG3-191" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [4 x i16] }* @"_SM7__constG3-190" to i8*), i32 0, i32 4, i32 2571220 }
@"_SM7__constG3-192" = private unnamed_addr constant { i8*, i32, i32, [4 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 4, i32 0, [4 x i16] [ i16 47, i16 116, i16 109, i16 112 ] }
@"_SM7__constG3-193" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [4 x i16] }* @"_SM7__constG3-192" to i8*), i32 0, i32 4, i32 1515144 }
@"_SM7__constG3-194" = private unnamed_addr constant { i8*, i32, i32, [0 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 0, i32 0, [0 x i16] [  ] }
@"_SM7__constG3-195" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [0 x i16] }* @"_SM7__constG3-194" to i8*), i32 0, i32 0, i32 0 }
@"_SM7__constG3-196" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 10 ] }
@"_SM7__constG3-197" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-196" to i8*), i32 0, i32 1, i32 10 }
@"_SM7__constG3-198" = private unnamed_addr constant { i8*, i32, i32, [2 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 2, i32 0, [2 x i16] [ i16 13, i16 10 ] }
@"_SM7__constG3-199" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [2 x i16] }* @"_SM7__constG3-198" to i8*), i32 0, i32 2, i32 413 }
@"_SM7__constG3-200" = private unnamed_addr constant { i8*, i32, i32, [4 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 4, i32 0, [4 x i16] [ i16 76, i16 65, i16 78, i16 71 ] }
@"_SM7__constG3-201" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [4 x i16] }* @"_SM7__constG3-200" to i8*), i32 0, i32 4, i32 2329070 }
@"_SM7__constG3-202" = private unnamed_addr constant { i8*, i32, i32, [12 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 12, i32 0, [12 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 118, i16 101, i16 114, i16 115, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-203" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [12 x i16] }* @"_SM7__constG3-202" to i8*), i32 0, i32 12, i32 560567564 }
@"_SM7__constG3-204" = private unnamed_addr constant { i8*, i32, i32, [3 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 3, i32 0, [3 x i16] [ i16 49, i16 46, i16 56 ] }
@"_SM7__constG3-205" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [3 x i16] }* @"_SM7__constG3-204" to i8*), i32 0, i32 3, i32 48571 }
@"_SM7__constG3-206" = private unnamed_addr constant { i8*, i32, i32, [29 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 29, i32 0, [29 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 118, i16 109, i16 46, i16 115, i16 112, i16 101, i16 99, i16 105, i16 102, i16 105, i16 99, i16 97, i16 116, i16 105, i16 111, i16 110, i16 46, i16 118, i16 101, i16 114, i16 115, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-207" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [29 x i16] }* @"_SM7__constG3-206" to i8*), i32 0, i32 29, i32 -975448766 }
@"_SM7__constG3-208" = private unnamed_addr constant { i8*, i32, i32, [28 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 28, i32 0, [28 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 118, i16 109, i16 46, i16 115, i16 112, i16 101, i16 99, i16 105, i16 102, i16 105, i16 99, i16 97, i16 116, i16 105, i16 111, i16 110, i16 46, i16 118, i16 101, i16 110, i16 100, i16 111, i16 114 ] }
@"_SM7__constG3-209" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [28 x i16] }* @"_SM7__constG3-208" to i8*), i32 0, i32 28, i32 799684510 }
@"_SM7__constG3-210" = private unnamed_addr constant { i8*, i32, i32, [18 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 18, i32 0, [18 x i16] [ i16 79, i16 114, i16 97, i16 99, i16 108, i16 101, i16 32, i16 67, i16 111, i16 114, i16 112, i16 111, i16 114, i16 97, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-211" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [18 x i16] }* @"_SM7__constG3-210" to i8*), i32 0, i32 18, i32 987324518 }
@"_SM7__constG3-212" = private unnamed_addr constant { i8*, i32, i32, [26 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 26, i32 0, [26 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 118, i16 109, i16 46, i16 115, i16 112, i16 101, i16 99, i16 105, i16 102, i16 105, i16 99, i16 97, i16 116, i16 105, i16 111, i16 110, i16 46, i16 110, i16 97, i16 109, i16 101 ] }
@"_SM7__constG3-213" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [26 x i16] }* @"_SM7__constG3-212" to i8*), i32 0, i32 26, i32 1980476001 }
@"_SM7__constG3-214" = private unnamed_addr constant { i8*, i32, i32, [34 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 34, i32 0, [34 x i16] [ i16 74, i16 97, i16 118, i16 97, i16 32, i16 86, i16 105, i16 114, i16 116, i16 117, i16 97, i16 108, i16 32, i16 77, i16 97, i16 99, i16 104, i16 105, i16 110, i16 101, i16 32, i16 83, i16 112, i16 101, i16 99, i16 105, i16 102, i16 105, i16 99, i16 97, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-215" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [34 x i16] }* @"_SM7__constG3-214" to i8*), i32 0, i32 34, i32 1259379671 }
@"_SM7__constG3-216" = private unnamed_addr constant { i8*, i32, i32, [12 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 12, i32 0, [12 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 118, i16 109, i16 46, i16 110, i16 97, i16 109, i16 101 ] }
@"_SM7__constG3-217" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [12 x i16] }* @"_SM7__constG3-216" to i8*), i32 0, i32 12, i32 726644630 }
@"_SM7__constG3-218" = private unnamed_addr constant { i8*, i32, i32, [12 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 12, i32 0, [12 x i16] [ i16 83, i16 99, i16 97, i16 108, i16 97, i16 32, i16 78, i16 97, i16 116, i16 105, i16 118, i16 101 ] }
@"_SM7__constG3-219" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [12 x i16] }* @"_SM7__constG3-218" to i8*), i32 0, i32 12, i32 -1283648399 }
@"_SM7__constG3-220" = private unnamed_addr constant { i8*, i32, i32, [26 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 26, i32 0, [26 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 115, i16 112, i16 101, i16 99, i16 105, i16 102, i16 105, i16 99, i16 97, i16 116, i16 105, i16 111, i16 110, i16 46, i16 118, i16 101, i16 114, i16 115, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-221" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [26 x i16] }* @"_SM7__constG3-220" to i8*), i32 0, i32 26, i32 -333068255 }
@"_SM7__constG3-222" = private unnamed_addr constant { i8*, i32, i32, [25 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 25, i32 0, [25 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 115, i16 112, i16 101, i16 99, i16 105, i16 102, i16 105, i16 99, i16 97, i16 116, i16 105, i16 111, i16 110, i16 46, i16 118, i16 101, i16 110, i16 100, i16 111, i16 114 ] }
@"_SM7__constG3-223" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [25 x i16] }* @"_SM7__constG3-222" to i8*), i32 0, i32 25, i32 1928785119 }
@"_SM7__constG3-224" = private unnamed_addr constant { i8*, i32, i32, [23 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 23, i32 0, [23 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 115, i16 112, i16 101, i16 99, i16 105, i16 102, i16 105, i16 99, i16 97, i16 116, i16 105, i16 111, i16 110, i16 46, i16 110, i16 97, i16 109, i16 101 ] }
@"_SM7__constG3-225" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [23 x i16] }* @"_SM7__constG3-224" to i8*), i32 0, i32 23, i32 -2704414 }
@"_SM7__constG3-226" = private unnamed_addr constant { i8*, i32, i32, [31 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 31, i32 0, [31 x i16] [ i16 74, i16 97, i16 118, i16 97, i16 32, i16 80, i16 108, i16 97, i16 116, i16 102, i16 111, i16 114, i16 109, i16 32, i16 65, i16 80, i16 73, i16 32, i16 83, i16 112, i16 101, i16 99, i16 105, i16 102, i16 105, i16 99, i16 97, i16 116, i16 105, i16 111, i16 110 ] }
@"_SM7__constG3-227" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [31 x i16] }* @"_SM7__constG3-226" to i8*), i32 0, i32 31, i32 -549849202 }
@"_SM7__constG3-228" = private unnamed_addr constant { i8*, i32, i32, [14 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 14, i32 0, [14 x i16] [ i16 108, i16 105, i16 110, i16 101, i16 46, i16 115, i16 101, i16 112, i16 97, i16 114, i16 97, i16 116, i16 111, i16 114 ] }
@"_SM7__constG3-229" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [14 x i16] }* @"_SM7__constG3-228" to i8*), i32 0, i32 14, i32 1985578347 }
@"_SM7__constG3-230" = private unnamed_addr constant { i8*, i32, i32, [14 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 14, i32 0, [14 x i16] [ i16 102, i16 105, i16 108, i16 101, i16 46, i16 115, i16 101, i16 112, i16 97, i16 114, i16 97, i16 116, i16 111, i16 114 ] }
@"_SM7__constG3-231" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [14 x i16] }* @"_SM7__constG3-230" to i8*), i32 0, i32 14, i32 465797363 }
@"_SM7__constG3-232" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 47 ] }
@"_SM7__constG3-233" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-232" to i8*), i32 0, i32 1, i32 47 }
@"_SM7__constG3-234" = private unnamed_addr constant { i8*, i32, i32, [14 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 14, i32 0, [14 x i16] [ i16 112, i16 97, i16 116, i16 104, i16 46, i16 115, i16 101, i16 112, i16 97, i16 114, i16 97, i16 116, i16 111, i16 114 ] }
@"_SM7__constG3-235" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [14 x i16] }* @"_SM7__constG3-234" to i8*), i32 0, i32 14, i32 1989177436 }
@"_SM7__constG3-236" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 58 ] }
@"_SM7__constG3-237" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-236" to i8*), i32 0, i32 1, i32 58 }
@"_SM7__constG3-238" = private unnamed_addr constant { i8*, i32, i32, [6 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 6, i32 0, [6 x i16] [ i16 84, i16 77, i16 80, i16 68, i16 73, i16 82 ] }
@"_SM7__constG3-239" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [6 x i16] }* @"_SM7__constG3-238" to i8*), i32 0, i32 6, i32 -1816556522 }
@"_SM7__constG3-240" = private unnamed_addr constant { i8*, i32, i32, [14 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 14, i32 0, [14 x i16] [ i16 106, i16 97, i16 118, i16 97, i16 46, i16 105, i16 111, i16 46, i16 116, i16 109, i16 112, i16 100, i16 105, i16 114 ] }
@"_SM7__constG3-241" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [14 x i16] }* @"_SM7__constG3-240" to i8*), i32 0, i32 14, i32 -378505966 }
@"_SM7__constG3-242" = private unnamed_addr constant { i8*, i32, i32, [8 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 8, i32 0, [8 x i16] [ i16 117, i16 115, i16 101, i16 114, i16 46, i16 100, i16 105, i16 114 ] }
@"_SM7__constG3-243" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [8 x i16] }* @"_SM7__constG3-242" to i8*), i32 0, i32 8, i32 -267617302 }
@"_SM7__constG3-244" = private unnamed_addr constant { i8*, i32, i32, [9 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 9, i32 0, [9 x i16] [ i16 117, i16 115, i16 101, i16 114, i16 46, i16 104, i16 111, i16 109, i16 101 ] }
@"_SM7__constG3-245" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [9 x i16] }* @"_SM7__constG3-244" to i8*), i32 0, i32 9, i32 293923106 }
@"_SM7__constG3-246" = private unnamed_addr constant { i8*, i32, i32, [12 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 12, i32 0, [12 x i16] [ i16 117, i16 115, i16 101, i16 114, i16 46, i16 99, i16 111, i16 117, i16 110, i16 116, i16 114, i16 121 ] }
@"_SM7__constG3-247" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [12 x i16] }* @"_SM7__constG3-246" to i8*), i32 0, i32 12, i32 -1309845389 }
@"_SM7__constG3-248" = private unnamed_addr constant { i8*, i32, i32, [13 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 13, i32 0, [13 x i16] [ i16 117, i16 115, i16 101, i16 114, i16 46, i16 108, i16 97, i16 110, i16 103, i16 117, i16 97, i16 103, i16 101 ] }
@"_SM7__constG3-249" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [13 x i16] }* @"_SM7__constG3-248" to i8*), i32 0, i32 13, i32 1102884379 }
@"_SM7__constG3-250" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 123 ] }
@"_SM7__constG3-251" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-250" to i8*), i32 0, i32 1, i32 123 }
@"_SM7__constG3-252" = private unnamed_addr constant { i8*, i32, i32, [2 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 2, i32 0, [2 x i16] [ i16 44, i16 32 ] }
@"_SM7__constG3-253" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [2 x i16] }* @"_SM7__constG3-252" to i8*), i32 0, i32 2, i32 1396 }
@"_SM7__constG3-254" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 61 ] }
@"_SM7__constG3-255" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-254" to i8*), i32 0, i32 1, i32 61 }
@"_SM7__constG3-256" = private unnamed_addr constant { i8*, i32, i32, [1 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 1, i32 0, [1 x i16] [ i16 125 ] }
@"_SM7__constG3-257" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [1 x i16] }* @"_SM7__constG3-256" to i8*), i32 0, i32 1, i32 125 }
@"_SM7__constG3-258" = private unnamed_addr constant { i8*, i32, i32, [4 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 4, i32 0, [4 x i16] [ i16 110, i16 117, i16 108, i16 108 ] }
@"_SM7__constG3-259" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [4 x i16] }* @"_SM7__constG3-258" to i8*), i32 0, i32 4, i32 3392903 }
@"_SM7__constG3-260" = private unnamed_addr constant { i8*, i32, i32, [4 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 4, i32 0, [4 x i16] [ i16 32, i16 45, i16 62, i16 32 ] }
@"_SM7__constG3-261" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [4 x i16] }* @"_SM7__constG3-260" to i8*), i32 0, i32 4, i32 998511 }
@"_SM7__constG3-262" = private unnamed_addr constant { i8*, i32, i32, [15 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 15, i32 0, [15 x i16] [ i16 107, i16 101, i16 121, i16 32, i16 110, i16 111, i16 116, i16 32, i16 102, i16 111, i16 117, i16 110, i16 100, i16 58, i16 32 ] }
@"_SM7__constG3-263" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [15 x i16] }* @"_SM7__constG3-262" to i8*), i32 0, i32 15, i32 1181171098 }
@"_SM7__constG3-264" = private unnamed_addr constant { i8*, i32, i32, [10 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 10, i32 0, [10 x i16] [ i16 60, i16 105, i16 116, i16 101, i16 114, i16 97, i16 116, i16 111, i16 114, i16 62 ] }
@"_SM7__constG3-265" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [10 x i16] }* @"_SM7__constG3-264" to i8*), i32 0, i32 10, i32 -902068012 }
@"_SM7__constG3-266" = private unnamed_addr constant { i8*, i32, i32, [16 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 16, i32 0, [16 x i16] [ i16 97, i16 115, i16 115, i16 101, i16 114, i16 116, i16 105, i16 111, i16 110, i16 32, i16 102, i16 97, i16 105, i16 108, i16 101, i16 100 ] }
@"_SM7__constG3-267" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [16 x i16] }* @"_SM7__constG3-266" to i8*), i32 0, i32 16, i32 -1177526501 }
@"_SM7__constG3-268" = private unnamed_addr constant { i8*, i32, i32, [4 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 4, i32 0, [4 x i16] [ i16 80, i16 116, i16 114, i16 64 ] }
@"_SM7__constG3-269" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [4 x i16] }* @"_SM7__constG3-268" to i8*), i32 0, i32 4, i32 2498354 }
@"_SM7__constG3-270" = private unnamed_addr constant { i8*, i32, i32, [7 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 7, i32 0, [7 x i16] [ i16 72, i16 97, i16 115, i16 104, i16 77, i16 97, i16 112 ] }
@"_SM7__constG3-271" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [7 x i16] }* @"_SM7__constG3-270" to i8*), i32 0, i32 7, i32 -1932803762 }
@"_SM7__constG3-272" = private unnamed_addr constant { i8*, i32, i32, [23 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 23, i32 0, [23 x i16] [ i16 110, i16 101, i16 119, i16 32, i16 72, i16 97, i16 115, i16 104, i16 77, i16 97, i16 112, i16 32, i16 116, i16 97, i16 98, i16 108, i16 101, i16 32, i16 115, i16 105, i16 122, i16 101, i16 32 ] }
@"_SM7__constG3-273" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [23 x i16] }* @"_SM7__constG3-272" to i8*), i32 0, i32 23, i32 -1705563365 }
@"_SM7__constG3-274" = private unnamed_addr constant { i8*, i32, i32, [16 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 16, i32 0, [16 x i16] [ i16 32, i16 101, i16 120, i16 99, i16 101, i16 101, i16 100, i16 115, i16 32, i16 109, i16 97, i16 120, i16 105, i16 109, i16 117, i16 109 ] }
@"_SM7__constG3-275" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [16 x i16] }* @"_SM7__constG3-274" to i8*), i32 0, i32 16, i32 -1079193057 }
@"_SM7__constG3-276" = private unnamed_addr constant { i8*, i32, i32, [15 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 15, i32 0, [15 x i16] [ i16 32, i16 111, i16 110, i16 32, i16 101, i16 109, i16 112, i16 116, i16 121, i16 32, i16 82, i16 97, i16 110, i16 103, i16 101 ] }
@"_SM7__constG3-277" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [15 x i16] }* @"_SM7__constG3-276" to i8*), i32 0, i32 15, i32 -1968168983 }
@"_SM7__constG3-278" = private unnamed_addr constant { i8*, i32, i32, [22 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 22, i32 0, [22 x i16] [ i16 110, i16 101, i16 120, i16 116, i16 32, i16 111, i16 110, i16 32, i16 101, i16 109, i16 112, i16 116, i16 121, i16 32, i16 105, i16 116, i16 101, i16 114, i16 97, i16 116, i16 111, i16 114 ] }
@"_SM7__constG3-279" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [22 x i16] }* @"_SM7__constG3-278" to i8*), i32 0, i32 22, i32 71434837 }
@"_SM7__constG3-280" = private unnamed_addr constant { i8*, i32, i32, [8 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 8, i32 0, [8 x i16] [ i16 67, i16 83, i16 116, i16 114, i16 117, i16 99, i16 116, i16 53 ] }
@"_SM7__constG3-281" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [8 x i16] }* @"_SM7__constG3-280" to i8*), i32 0, i32 8, i32 587416893 }
@"_SM7__constG3-282" = private unnamed_addr constant { i8*, i32, i32, [18 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 18, i32 0, [18 x i16] [ i16 65, i16 114, i16 114, i16 97, i16 121, i16 66, i16 117, i16 105, i16 108, i16 100, i16 101, i16 114, i16 46, i16 111, i16 102, i16 82, i16 101, i16 102 ] }
@"_SM7__constG3-283" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [18 x i16] }* @"_SM7__constG3-282" to i8*), i32 0, i32 18, i32 37756272 }
@"_SM7__constG3-284" = private unnamed_addr constant { i8*, i32, i32, [19 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 19, i32 0, [19 x i16] [ i16 65, i16 114, i16 114, i16 97, i16 121, i16 66, i16 117, i16 105, i16 108, i16 100, i16 101, i16 114, i16 46, i16 111, i16 102, i16 76, i16 111, i16 110, i16 103 ] }
@"_SM7__constG3-285" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [19 x i16] }* @"_SM7__constG3-284" to i8*), i32 0, i32 19, i32 1170275647 }
@"_SM7__constG3-286" = private unnamed_addr constant { i8*, i32, i32, [19 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 19, i32 0, [19 x i16] [ i16 65, i16 114, i16 114, i16 97, i16 121, i16 66, i16 117, i16 105, i16 108, i16 100, i16 101, i16 114, i16 46, i16 111, i16 102, i16 85, i16 110, i16 105, i16 116 ] }
@"_SM7__constG3-287" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [19 x i16] }* @"_SM7__constG3-286" to i8*), i32 0, i32 19, i32 1170542663 }
@"_SM7__constG3-288" = private unnamed_addr constant { i8*, i32, i32, [5 x i16] } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i32 5, i32 0, [5 x i16] [ i16 83, i16 104, i16 111, i16 114, i16 116 ] }
@"_SM7__constG3-289" = private unnamed_addr constant { i8*, i8*, i32, i32, i32 } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i8* bitcast ({ i8*, i32, i32, [5 x i16] }* @"_SM7__constG3-288" to i8*), i32 0, i32 5, i32 79860828 }

declare i32 @"scalanative_eexist"()
@"_SM39scala.collection.Iterator$SliceIteratorG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM18java.util.Objects$G8instance" = external constant { i8* }

declare nonnull dereferenceable(8) i8* @"_SM33scala.collection.immutable.MapOpsD6$init$uEO"(i8*)
@"_SM5Main$G8instance" = external constant { i8* }

declare dereferenceable_or_null(8) i8* @"_SM37java.util.ScalaOps$ToJavaIterableOps$D18scalaOps$extensionL18java.lang.IterableL18java.lang.IterableEO"(i8*, i8*)

declare i64 @"scalanative_wide_char_size"()
@"_SM28java.lang.System$$$Lambda$13G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM22scala.reflect.ManifestD6$init$uEO"(i8*)

declare dereferenceable_or_null(8) i8* @"_SM16java.util.ArraysD6copyOfLAL16java.lang.Object_iLAL16java.lang.Object_Eo"(i8*, i32) inlinehint
@"_SM21scala.Product$$anon$1G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM41scala.collection.immutable.ArraySeq$ofRefG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }
@"_SM34java.lang.IllegalArgumentExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }
@"_SM27java.lang.System$$$Lambda$8G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.GrowableD6addAllL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8*, i8*)
@"_SM34scala.scalanative.unsafe.Tag$UInt$G8instance" = external constant { i8* }
@"_SM33scala.collection.MapOps$$Lambda$3G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8*)

declare i1 @"_SM23scala.collection.SeqOpsD7isEmptyzEO"(i8*)
@"_SM36scala.scalanative.runtime.ByteArray$G8instance" = external constant { i8* }
@"_SM28java.lang.System$$$Lambda$14G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM50scala.collection.StrictOptimizedClassTagSeqFactoryD6$init$uEO"(i8*)

declare dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.ULongD3maxL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8*, i8*) inlinehint

declare nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8*)

declare i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8*) noinline
@"_SM20java.util.PropertiesG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM38scala.collection.mutable.IndexedSeqOpsD6$init$uEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM16java.lang.StringD8getCharsiiLAc_iuEO"(i8*, i32, i32, i8*, i32)
@"_SM26java.lang.RuntimeExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }
@"_SM23scala.runtime.LazyVals$G8instance" = external constant { i8* }

declare dereferenceable_or_null(16) i8* @"_SM40scala.collection.mutable.ArraySeq$ofCharD9addStringL38scala.collection.mutable.StringBuilderL16java.lang.StringL16java.lang.StringL16java.lang.StringL38scala.collection.mutable.StringBuilderEO"(i8*, i8*, i8*, i8*, i8*)

declare nonnull dereferenceable(8) i8* @"_SM31scala.collection.mutable.SeqOpsD6$init$uEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8*)

declare i32 @"_SM28scala.collection.AbstractMapD8hashCodeiEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM12scala.OptionD7foreachL15scala.Function1uEO"(i8*, i8*) inlinehint

declare nonnull dereferenceable(8) i8* @"_SM35scala.scalanative.runtime.LazyVals$D7setFlagR_iiuEO"(i8*, i8*, i32, i32) inlinehint
@"_SM35java.util.ScalaOps$JavaIterableOps$G8instance" = external constant { i8* }

declare nonnull dereferenceable(8) i8* @"_SM21scala.runtime.StaticsD12releaseFenceuEo"() inlinehint

declare i8* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO"(i8*, i32) noinline
@"_SM16scala.MatchErrorG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM27java.lang.System$$$Lambda$1G17$extern$forwarder"(i8*, i8*)
@"_SM36scala.scalanative.runtime.ShortArrayG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }

declare dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8*)

declare nonnull dereferenceable(40) i8* @"_SM33scala.collection.immutable.VectorD4ioobiL35java.lang.IndexOutOfBoundsExceptionEO"(i8*, i32)

declare dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.UInt$D10uint2ulongL31scala.scalanative.unsigned.UIntL32scala.scalanative.unsigned.ULongEO"(i8*, i8*)

declare i32 @"_SM19java.nio.CharBufferD6lengthiEO"(i8*)
@"_SM40scala.collection.mutable.ArraySeq$ofCharG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM36scala.scalanative.runtime.LongArray$D5allociL35scala.scalanative.runtime.LongArrayEO"(i8*, i32) inlinehint

declare nonnull dereferenceable(16) i8* @"_SM11scala.Some$D5applyL16java.lang.ObjectL10scala.SomeEO"(i8*, i8*)
@"_SM19java.util.Map$EntryG4type" = external global { i8*, i32, i32, i8* }

declare i32 @"_SM23scala.collection.SeqOpsD13lengthCompareiiEO"(i8*, i32)

declare nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8next_$eqL37scala.collection.mutable.HashMap$NodeuEO"(i8*, i8*)
@"_SM28java.lang.System$$$Lambda$10G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"_SM16java.lang.SystemD16identityHashCodeL16java.lang.ObjectiEo"(i8*) inlinehint
@"_ST17__trait_has_trait" = external constant [122 x [122 x i1]]

declare i8* @"_SM35scala.scalanative.runtime.CharArrayD5atRawiR_EO"(i8*, i32) inlinehint

declare i64 @"strlen"(i8*)

declare i32 @"_SM16java.lang.ObjectD8hashCodeiEO"(i8*) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM14java.lang.VoidD4TYPEL15java.lang.ClassEo"() inlinehint

declare nonnull dereferenceable(8) i8* @"_SM12scala.Array$D4copyL16java.lang.ObjectiL16java.lang.ObjectiiuEO"(i8*, i8*, i32, i8*, i32, i32)

declare dereferenceable_or_null(8) i8* @"_SM36scala.collection.MapFactory$DelegateD5emptyL16java.lang.ObjectEO"(i8*)
@"_SM28java.lang.System$$$Lambda$15G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD4nextL37scala.collection.mutable.HashMap$NodeEO"(i8*)

declare i8* @"scalanative_throw"(i8*)

declare i32 @"_SM21scala.runtime.StaticsD7anyHashL16java.lang.ObjectiEo"(i8*) inlinehint
@"_SM27java.lang.System$$$Lambda$4G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8*, i32)

declare dereferenceable_or_null(8) i8* @"_SM19java.util.ScalaOps$D17ToJavaIterableOpsL18java.lang.IterableL18java.lang.IterableEO"(i8*, i8*)
@"_SM37scala.collection.mutable.HashMap$NodeG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"_SM15java.lang.Long$D8hashCodejiEO"(i8*, i64) inlinehint

declare i32 @"_SM16java.lang.StringD6lengthiEO"(i8*)

declare double @"_SM27scala.runtime.BoxesRunTime$D13unboxToDoubleL16java.lang.ObjectdEO"(i8*, i8*)
@"_SM20java.nio.ByteBuffer$G8instance" = external constant { i8* }
@"_SM22scala.reflect.ClassTagG4type" = external global { i8*, i32, i32, i8* }

declare i32 @"_SM27scala.runtime.ScalaRunTime$D12array_lengthL16java.lang.ObjectiEO"(i8*, i8*)

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD11toUpperCaseL16java.lang.StringEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D19dropWhile$extensionL16java.lang.StringL15scala.Function1L16java.lang.StringEO"(i8*, i8*, i8*)

declare dereferenceable_or_null(16) i8* @"_SM14java.io.WriterD17$init$$$anonfun$1L14java.io.WriterEPT14java.io.Writer"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM27scala.collection.SeqFactoryD6$init$uEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO"(i8*) inlinehint

declare nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableFactoryD6$init$uEO"(i8*)

declare i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8*, i8*) inlinehint
@"_SM11scala.Some$G8instance" = external constant { i8* }

declare nonnull dereferenceable(24) i8* @"_SM31scala.collection.IndexedSeqViewD8iteratorL25scala.collection.IteratorEO"(i8*)

declare i32 @"scalanative_enotdir"()

declare i32 @"_SM22java.util.HashMap$NodeD8hashCodeiEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM17java.lang.IntegerD8toStringL16java.lang.StringEO"(i8*) inlinehint

declare dereferenceable_or_null(8) i8* @"_SM28scala.collection.AbstractMapD7defaultL16java.lang.ObjectL16java.lang.ObjectEO"(i8*, i8*)

declare i32 @"_SM17java.util.HashMapD4sizeiEO"(i8*)

declare i32 @"_SM22scala.runtime.RichInt$D13min$extensioniiiEO"(i8*, i32, i32)

declare nonnull dereferenceable(16) i8* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8*, i8*) inlinehint

declare i8* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO"(i8*) noinline
@"_SM27scala.collection.StringOps$G8instance" = external constant { i8* }

declare i1 @"_SM18java.util.Objects$D6equalsL16java.lang.ObjectL16java.lang.ObjectzEO"(i8*, i8*, i8*) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D14drop$extensionL16java.lang.StringiL16java.lang.StringEO"(i8*, i8*, i32)

declare dereferenceable_or_null(8) i8* @"_SM26java.util.HashMap$EntrySetD8iteratorL18java.util.IteratorEO"(i8*)
@"_SM31java.util.AbstractMap$$Lambda$8G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"_SM17java.util.HashMapD12tableSizeForiiEPT17java.util.HashMap"(i8*, i32) inlinehint

declare nonnull dereferenceable(24) i8* @"_SM32scala.collection.mutable.BuilderD9mapResultL15scala.Function1L32scala.collection.mutable.BuilderEO"(i8*, i8*)
@"_SM38scala.scalanative.runtime.BooleanArrayG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }

declare nonnull dereferenceable(16) i8* @"_SM33scala.collection.mutable.IterableD15iterableFactoryL32scala.collection.IterableFactoryEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM32scala.scalanative.runtime.Array$D4copyL16java.lang.ObjectiL16java.lang.ObjectiiuEO"(i8*, i8*, i32, i8*, i32, i32)
@"_SM27java.lang.System$$$Lambda$5G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM44scala.collection.immutable.ArraySeq$ofDoubleG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8*)

declare dereferenceable_or_null(40) i8* @"_SM24java.nio.charset.CharsetD6decodeL19java.nio.ByteBufferL19java.nio.CharBufferEO"(i8*, i8*)
@"_SM19java.lang.ThrowableG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM36scala.scalanative.runtime.ByteArray$D5allociL35scala.scalanative.runtime.ByteArrayEO"(i8*, i32) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.ObjectD8getClassL15java.lang.ClassEO"(i8*) inlinehint
@"_SM42scala.collection.immutable.ArraySeq$ofLongG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM35scala.collection.MapFactoryDefaultsD6$init$uEO"(i8*)

declare nonnull dereferenceable(16) i8* @"_SM32scala.scalanative.runtime.Boxes$D10boxToULongjL32scala.scalanative.unsigned.ULongEO"(i8*, i64) inlinehint
@"_SM28java.lang.System$$$Lambda$17G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"() noinline

declare nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD9value_$eqL16java.lang.ObjectuEO"(i8*, i8*)

declare dereferenceable_or_null(8) i8* @"_SM41scala.collection.immutable.VectorStatics$D9mapElems1LAL16java.lang.Object_L15scala.Function1LAL16java.lang.Object_EO"(i8*, i8*, i8*)

declare dereferenceable_or_null(16) i8* @"_SM26scala.collection.Iterator$G4load"() noinline

declare dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD5valueL16java.lang.ObjectEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD22toUpperCase$$anonfun$1iL16java.lang.StringEPT16java.lang.String"(i8*, i32)
@"_SM35scala.scalanative.runtime.CharArrayG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }

declare i64 @"_SM27scala.runtime.BoxesRunTime$D11unboxToLongL16java.lang.ObjectjEO"(i8*, i8*)

declare i64 @"_SM23scala.runtime.LazyVals$D5STATEjijEO"(i8*, i64, i32)

declare dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8findNodeL16java.lang.ObjectiL37scala.collection.mutable.HashMap$NodeEO"(i8*, i8*, i32)
@"_SM31java.util.AbstractMap$$Lambda$7G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"_SM37scala.collection.mutable.HashMap$NodeD4hashiEO"(i8*)

declare i32 @"_SM30scala.collection.IndexedSeqOpsD9knownSizeiEO"(i8*)

declare i32 @"_SM23scala.collection.SeqOpsD4sizeiEO"(i8*)

declare dereferenceable_or_null(48) i8* @"_SM41java.util.HashMap$AbstractHashMapIteratorD4nextL16java.lang.ObjectEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO"(i8*, i8*)

declare i64 @"_SM32scala.scalanative.unsigned.ULongD6toLongjEO"(i8*) inlinehint

declare nonnull dereferenceable(8) i8* @"_SM22scala.reflect.ClassTagD6$init$uEO"(i8*)

declare nonnull dereferenceable(32) i8* @"_SM31scala.collection.IndexedSeqViewD12stringPrefixL16java.lang.StringEO"(i8*)

declare dereferenceable_or_null(8) i8* @"_SM26scala.collection.Iterator$D5emptyL25scala.collection.IteratorEO"(i8*) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM27scala.runtime.ScalaRunTime$D9_toStringL13scala.ProductL16java.lang.StringEO"(i8*, i8*)

declare dereferenceable_or_null(16) i8* @"_SM29scala.collection.mutable.Map$G4load"() noinline

declare i8* @"memcpy"(i8*, i8*, i64)
@"_SM35scala.scalanative.unsigned.package$G8instance" = external constant { i8* }

declare nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8*)

declare i8* @"scalanative_alloc_small"(i8*, i64)

declare nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM23java.io.FileDescriptor$G4load"() noinline
@"_SM40scala.collection.mutable.HashMap$$anon$5G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }
@"_ST17__class_has_trait" = external constant [656 x [122 x i1]]

declare nonnull dereferenceable(8) i8* @"_SM40scala.collection.ClassTagIterableFactoryD6$init$uEO"(i8*)
@"_SM36scala.scalanative.runtime.LongArray$G8instance" = external constant { i8* }

declare nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8*)
@"_ST10__dispatch" = external constant [4582 x i8*]

declare dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.ULongD7toULongL32scala.scalanative.unsigned.ULongEO"(i8*) inlinehint

declare nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8*)

declare i8* @"_SM32scala.scalanative.runtime.Boxes$D10unboxToPtrL16java.lang.ObjectR_EO"(i8*, i8*) inlinehint
@"_SM22java.util.Collections$G8instance" = external constant { i8* }

declare i32 @"scalanative_enoent"()

declare nonnull dereferenceable(32) i8* @"_SM33scala.collection.mutable.HashMap$D5emptyL32scala.collection.mutable.HashMapEO"(i8*)
@"_SM43scala.collection.immutable.ArraySeq$ofFloatG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM25scala.collection.IterableD8toStringL16java.lang.StringEO"(i8*)

declare nonnull dereferenceable(16) i8* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO"(i8*, i8*)

declare i32 @"_SM27scala.runtime.BoxesRunTime$D10unboxToIntL16java.lang.ObjectiEO"(i8*, i8*)

declare i8* @"getcwd"(i8*, i64)

declare nonnull dereferenceable(8) i8* @"_SM33scala.collection.mutable.IterableD6$init$uEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.ArrayBuilderD10ensureSizeiuEO"(i8*, i32)

declare nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.VectorBuilderD20$anonfun$addVector$1LAL16java.lang.Object_uEPT40scala.collection.immutable.VectorBuilder"(i8*, i8*)
@"_SM15java.lang.Long$G8instance" = external constant { i8* }

declare i8* @"scalanative_alloc_atomic"(i8*, i64)

declare nonnull dereferenceable(8) i8* @"_SM20scala.collection.MapD6$init$uEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM55scala.scalanative.runtime.ieee754tostring.ryu.RyuFloat$D24floatToString$$anonfun$2L20scala.runtime.IntRefiLAc_L20scala.runtime.IntRefiiuEPT55scala.scalanative.runtime.ieee754tostring.ryu.RyuFloat$"(i8*, i8*, i32, i8*, i8*, i32, i32)

declare dereferenceable_or_null(8) i8* @"_SM31scala.collection.mutable.MapOpsD6resultL31scala.collection.mutable.MapOpsEO"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM28scala.collection.AbstractMapD8toStringL16java.lang.StringEO"(i8*)
@"_SM28scala.scalanative.unsafe.TagG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }

declare i32 @"_SM32scala.scalanative.unsigned.ULongD5toIntiEO"(i8*) inlinehint
@"_SM38scala.scalanative.runtime.ObjectArray$G8instance" = external constant { i8* }

declare dereferenceable_or_null(8) i8* @"_SM17java.util.HashMapD3putL16java.lang.ObjectL16java.lang.ObjectL16java.lang.ObjectEO"(i8*, i8*, i8*)

declare dereferenceable_or_null(16) i8* @"_SM37scala.collection.mutable.ArrayBuilderD6addAllL29scala.collection.IterableOnceL37scala.collection.mutable.ArrayBuilderEO"(i8*, i8*)

declare i1 @"_SM23scala.collection.SeqOpsD12sameElementsL29scala.collection.IterableOncezEO"(i8*, i8*)
@"_SM27java.lang.System$$$Lambda$9G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM19java.util.HashtableD14get$$anonfun$1L16java.lang.ObjectEpT19java.util.Hashtable"()
@"_SM42scala.collection.immutable.Range$ExclusiveG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM35scala.scalanative.runtime.ByteArrayG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM11scala.None$G4load"() noinline

declare i32 @"_SM17java.util.HashMapD12newThresholdiiEPT17java.util.HashMap"(i8*, i32) inlinehint
@"_SM35scala.collection.immutable.ArraySeqG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }
@"_SM36scala.scalanative.runtime.FloatArrayG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM41scala.collection.immutable.VectorStatics$D8mapElemsiLAL16java.lang.Object_L15scala.Function1LAL16java.lang.Object_EO"(i8*, i32, i8*, i8*)

declare dereferenceable_or_null(40) i8* @"_SM40scala.collection.Iterator$ConcatIteratorD6concatL15scala.Function0L25scala.collection.IteratorEO"(i8*, i8*)

declare i8* @"strerror"(i32)

declare dereferenceable_or_null(8) i8* @"_SM33scala.collection.AbstractIterableD7toArrayL22scala.reflect.ClassTagL16java.lang.ObjectEO"(i8*, i8*)

declare dereferenceable_or_null(8) i8* @"_SM22java.util.HashMap$NodeD6getKeyL16java.lang.ObjectEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM35scala.collection.mutable.IndexedSeqD6$init$uEO"(i8*)
@"_SM39java.nio.charset.CoderResult$$$Lambda$1G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i64 @"_SM32scala.scalanative.runtime.Boxes$D12unboxToULongL16java.lang.ObjectjEO"(i8*, i8*) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM23java.lang.StringBuilderD8toStringL16java.lang.StringEO"(i8*)

declare i32 @"_SM31scala.util.hashing.MurmurHash3$D7seqHashL20scala.collection.SeqiEO"(i8*, i8*)

declare dereferenceable_or_null(32) i8* @"_SM14java.lang.LongD11toHexStringjL16java.lang.StringEo"(i64) inlinehint
@"_SM28java.nio.charset.CoderResultG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM42scala.collection.immutable.ArraySeq$ofUnitG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8*)
@"_SM27java.lang.System$$$Lambda$3G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8*, i16)
@"_SM35java.nio.file.NotDirectoryExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM36scala.collection.SeqFactory$DelegateD4fromL29scala.collection.IterableOnceL23scala.collection.SeqOpsEO"(i8*, i8*)
@"_SM35scala.scalanative.runtime.LongArrayG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }
@"_SM35scala.scalanative.unsafe.CFuncPtr17G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"_SM31scala.util.hashing.MurmurHash3$D19arraySeqHash$mCc$spLAc_iEO"(i8*, i8*)

declare i32 @"_SM28scala.collection.IterableOpsD11sizeCompareiiEO"(i8*, i32)

declare dereferenceable_or_null(8) i8* @"_SM22java.util.HashMap$NodeD8getValueL16java.lang.ObjectEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD12foreachEntryL15scala.Function2uEO"(i8*, i8*)

declare nonnull dereferenceable(8) i8* @"_SM27scala.runtime.ScalaRunTime$D12array_updateL16java.lang.ObjectiL16java.lang.ObjectuEO"(i8*, i8*, i32, i8*) inlinehint

declare dereferenceable_or_null(24) i8* @"_SM32scala.scalanative.unsigned.UInt$G4load"() noinline

declare dereferenceable_or_null(8) i8* @"_SM35java.util.ScalaOps$JavaIterableOps$D18foldLeft$extensionL18java.lang.IterableL16java.lang.ObjectL15scala.Function2L16java.lang.ObjectEO"(i8*, i8*, i8*, i8*) inlinehint

declare dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToBooleanzL17java.lang.BooleanEO"(i8*, i1)

declare nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8*)
@"_SM35scala.scalanative.runtime.LazyVals$G8instance" = external constant { i8* }
@"_SM23java.lang.StringBuilderG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i16 @"_SM48java.util.FormatterCompanionImpl$RootLocaleInfo$D9zeroDigitcEO"(i8*)

declare dereferenceable_or_null(24) i8* @"_SM25java.nio.file.LinkOption$G4load"() noinline

declare dereferenceable_or_null(8) i8* @"_SM31scala.collection.mutable.MapOpsD15getOrElseUpdateL16java.lang.ObjectL15scala.Function0L16java.lang.ObjectEO"(i8*, i8*, i8*)
@"_SM35java.nio.file.AccessDeniedExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.GrowableD13$plus$plus$eqL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8*, i8*) inlinehint

declare nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8*)

declare i32 @"_SM30scala.util.hashing.MurmurHash3D13unorderedHashL29scala.collection.IterableOnceiiEO"(i8*, i8*, i32)
@"_SM33java.nio.file.NoSuchFileExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8*)

declare nonnull dereferenceable(16) i8* @"_SM32scala.scalanative.unsigned.ULongD6$timesL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8*, i8*) inlinehint
@"_SM16java.lang.StringG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i1 @"_SM16java.util.ArraysD6equalsLAc_LAc_zEo"(i8*, i8*) inlinehint
@"_SM35java.lang.IndexOutOfBoundsExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM12scala.OptionD6orElseL15scala.Function0L12scala.OptionEO"(i8*, i8*) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD9substringiiL16java.lang.StringEO"(i8*, i32, i32)
@"_SM19java.io.PrintStreamG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] }

declare i64 @"_SM10scala.Int$D8int2longijEO"(i8*, i32)
@"_SM50scala.collection.ClassTagSeqFactory$AnySeqDelegateG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD3keyL16java.lang.ObjectEO"(i8*)

declare dereferenceable_or_null(8) i8* @"_SM31scala.collection.IndexedSeqViewD4viewL31scala.collection.IndexedSeqViewEO"(i8*)

declare dereferenceable_or_null(8) i8* @"_SM12scala.Array$D4fromL29scala.collection.IterableOnceL22scala.reflect.ClassTagL16java.lang.ObjectEO"(i8*, i8*, i8*)
@"_SM40scala.collection.mutable.HashMap$$anon$4G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8*, i8*, i8*) noinline

declare i1 @"_SM41java.util.HashMap$AbstractHashMapIteratorD7hasNextzEO"(i8*)

declare i32 @"getuid"()
@"_SM42scala.collection.immutable.ArraySeq$ofByteG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }
@"__modules" = external global [194 x i8*]
@"_SM27java.lang.System$$$Lambda$7G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM34scala.scalanative.unsafe.CFuncPtr3G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8*)
@"_SM34scala.collection.immutable.HashMapG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM19java.util.ScalaOps$G8instance" = external constant { i8* }

declare nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8*)

declare i1 @"_SM35scala.scalanative.runtime.LazyVals$D3CASR_jiizEO"(i8*, i8*, i64, i32, i32) inlinehint

declare i1 @"scalanative_platform_is_windows"()
@"_SM37java.util.ScalaOps$ToJavaIterableOps$G8instance" = external constant { i8* }
@"_SM42scala.collection.mutable.HashMap$$Lambda$2G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(32) i8* @"_SM19java.lang.ThrowableD10getMessageL16java.lang.StringEO"(i8*)
@"_SM46scala.collection.immutable.ArraySeq$$$Lambda$1G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM5Main$D4mainLAL16java.lang.String_uEO"(i8*, i8*)

declare i8* @"_SM35scala.scalanative.runtime.LazyVals$D17wait4NotificationR_jiuEO"(i8*, i8*, i64, i32)

declare nonnull dereferenceable(16) i8* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8*, i8*) inlinehint
@"_SM33scala.collection.Iterator$$anon$9G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM40java.lang.ArrayIndexOutOfBoundsExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }
@"_SM12scala.Tuple2G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }
@"_SM10scala.Int$G8instance" = external constant { i8* }
@"_SM37scala.scalanative.runtime.ObjectArrayG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM34scala.collection.Iterator$$anon$21D17$anonfun$addOne$1L16java.lang.ObjectL25scala.collection.IteratorEPT34scala.collection.Iterator$$anon$21"(i8*, i8*)
@"_SM40scala.collection.mutable.HashMap$$anon$1G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD8toStringL16java.lang.StringEO"(i8*)

declare nonnull dereferenceable(8) i8* @"_SM56scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$D25doubleToString$$anonfun$6L20scala.runtime.IntRefL21scala.runtime.LongRefiLAc_L20scala.runtime.IntRefiuEPT56scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$"(i8*, i8*, i8*, i32, i8*, i8*, i32)

declare dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8*)

declare nonnull dereferenceable(24) i8* @"_SM27scala.runtime.ScalaRunTime$D20typedProductIteratorL13scala.ProductL25scala.collection.IteratorEO"(i8*, i8*)

declare dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.ArrayBuffer$G4load"() noinline

declare dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8*, i8*, i8*)
@"_SM40scala.collection.Iterator$ConcatIteratorG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"scalanative_getpwuid"(i32, i8*)

declare nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8*)
@"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" = external constant { i8* }

declare nonnull dereferenceable(8) i8* @"_SM12scala.OptionD3mapL15scala.Function1L12scala.OptionEO"(i8*, i8*) inlinehint

declare dereferenceable_or_null(8) i8* @"_SM21scala.runtime.StaticsD5ioobeiL16java.lang.ObjectEo"(i32) inlinehint

declare i32 @"_SM14java.lang.LongD8hashCodejiEo"(i64) inlinehint

declare dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.runtime.Boxes$D8boxToPtrR_L28scala.scalanative.unsafe.PtrEO"(i8*, i8*) inlinehint
@"_SM34scala.scalanative.runtime.package$G8instance" = external constant { i8* }
@"_SM20scala.runtime.IntRefG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i1 @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO"(i8*, i8*) inlinehint

declare dereferenceable_or_null(32) i8* @"_SM29scala.collection.AbstractViewD8toStringL16java.lang.StringEO"(i8*)
@"_SM45scala.collection.immutable.ArraySeq$ofBooleanG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }
@"_SM22scala.runtime.RichInt$G8instance" = external constant { i8* }

declare dereferenceable_or_null(16) i8* @"_SM41scala.scalanative.posix.pwdOps$passwdOps$D16pw_dir$extensionL28scala.scalanative.unsafe.PtrL28scala.scalanative.unsafe.PtrEO"(i8*, i8*)
@"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" = external constant { i8* }

declare nonnull dereferenceable(24) i8* @"_SM22java.util.Collections$D15unmodifiableMapL13java.util.MapL13java.util.MapEO"(i8*, i8*)
@"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" = external constant { i8* }

declare i1 @"_SM35java.util.ScalaOps$JavaIterableOps$D16forall$extensionL18java.lang.IterableL15scala.Function1zEO"(i8*, i8*, i8*) inlinehint

declare nonnull dereferenceable(16) i8* @"_SM37scala.collection.mutable.ArrayBuffer$D10newBuilderL32scala.collection.mutable.BuilderEO"(i8*)

declare i1 @"_SM20scala.collection.MapD17$anonfun$equals$1L20scala.collection.MapL12scala.Tuple2zEPT20scala.collection.Map"(i8*, i8*, i8*)
@"_SM39java.lang.UnsupportedOperationExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(32) i8* @"_SM33scala.collection.AbstractIteratorD8toStringL16java.lang.StringEO"(i8*)

declare dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D11boxToDoubledL16java.lang.DoubleEO"(i8*, double)

declare i64 @"_SM32scala.collection.immutable.RangeD10longLengthjEPT32scala.collection.immutable.Range"(i8*)

declare dereferenceable_or_null(32) i8* @"_SM19java.lang.CharacterD8toStringL16java.lang.StringEO"(i8*) inlinehint
@"_SM23scala.runtime.BoxedUnitG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(32) i8* @"_SM20java.util.PropertiesD11getPropertyL16java.lang.StringL16java.lang.StringL16java.lang.StringEO"(i8*, i8*, i8*)

declare nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8*)
@"_SM28scala.collection.mutable.MapG4type" = external global { i8*, i32, i32, i8* }
@"_SM46scala.collection.ArrayOps$ArrayIterator$mcC$spG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(16) i8* @"_SM17java.util.HashMapD8entrySetL13java.util.SetEO"(i8*)
@"_SM31scala.scalanative.posix.pwdOps$G8instance" = external constant { i8* }

declare dereferenceable_or_null(8) i8* @"_SM12scala.OptionD9getOrElseL15scala.Function0L16java.lang.ObjectEO"(i8*, i8*) inlinehint

declare i8* @"memset"(i8*, i32, i64)

declare dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D19takeWhile$extensionL16java.lang.StringL15scala.Function1L16java.lang.StringEO"(i8*, i8*, i8*)
@"_SM28java.lang.System$$$Lambda$16G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(16) i8* @"_SM14java.io.ReaderD17$init$$$anonfun$1L14java.io.ReaderEPT14java.io.Reader"(i8*)

declare dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.HashMap$G4load"() noinline
@"_SM34scala.scalanative.runtime.IntArrayG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }
@"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" = external constant { i8* }

declare i32 @"_SM22scala.runtime.RichInt$D13max$extensioniiiEO"(i8*, i32, i32)
@"_SM17java.util.HashMapG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.GrowableD8$plus$eqL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8*, i8*) inlinehint
@"_SM33java.nio.channels.FileChannelImplG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM44scala.scalanative.unsafe.UnsafePackageCompatD6$init$uEO"(i8*)
@"_SM32java.util.NoSuchElementExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }
@"_SM41scala.scalanative.posix.pwdOps$passwdOps$G8instance" = external constant { i8* }

declare i1 @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO"(i8*, i8*) inlinehint

declare i1 @"_SM28scala.collection.AbstractMapD6equalsL16java.lang.ObjectzEO"(i8*, i8*)

declare dereferenceable_or_null(8) i8* @"_SM27scala.runtime.ScalaRunTime$D11array_applyL16java.lang.ObjectiL16java.lang.ObjectEO"(i8*, i8*, i32) inlinehint
@"_SM13java.util.MapG4type" = external global { i8*, i32, i32, i8* }

declare i32 @"_SM17java.lang.IntegerD13highestOneBitiiEo"(i32) inlinehint
@"_SM23java.io.FileInputStreamG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.StringL23java.lang.StringBuilderEO"(i8*, i8*)

declare nonnull dereferenceable(8) i8* @"_SM34scala.collection.mutable.CloneableD6$init$uEO"(i8*)

declare i8* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO"(i8*, i32)

declare nonnull dereferenceable(16) i8* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8*, i8*) inlinehint

declare dereferenceable_or_null(48) i8* @"_SM20java.nio.ByteBuffer$D4wrapLAb_iiL19java.nio.ByteBufferEO"(i8*, i8*, i32, i32)

declare nonnull dereferenceable(8) i8* @"_SM40scala.collection.EvidenceIterableFactoryD6$init$uEO"(i8*)

declare nonnull dereferenceable(8) i8* @"scalanative_set_os_props"(i8*)
@"_SM35scala.scalanative.unsafe.CFuncPtr20G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare nonnull dereferenceable(8) i8* @"_SM44scala.collection.generic.DefaultSerializableD6$init$uEO"(i8*)

declare dereferenceable_or_null(8) i8* @"_SM38scala.scalanative.runtime.ObjectArray$D5allociL37scala.scalanative.runtime.ObjectArrayEO"(i8*, i32) inlinehint
@"_SM24java.io.FileOutputStreamG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] }

declare dereferenceable_or_null(32) i8* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO"(i8*)

declare i1 @"_SM15java.lang.ClassD6equalsL16java.lang.ObjectzEO"(i8*, i8*) inlinehint

declare dereferenceable_or_null(80) i8* @"_SM12scala.Array$G4load"() noinline

declare dereferenceable_or_null(16) i8* @"_SM31scala.scalanative.posix.pwdOps$D9passwdOpsL28scala.scalanative.unsafe.PtrL28scala.scalanative.unsafe.PtrEO"(i8*, i8*)
@"_SM43scala.collection.immutable.ArraySeq$ofShortG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }

declare i32 @"_SM30scala.collection.IndexedSeqOpsD13lengthCompareiiEO"(i8*, i32)

declare dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8*, i8*) inlinehint

declare dereferenceable_or_null(8) i8* @"_SM19java.nio.CharBufferD5arrayLAc_EO"(i8*) inlinehint

declare i1 @"_SM42scala.collection.immutable.Range$ExclusiveD11isInclusivezEO"(i8*)

declare dereferenceable_or_null(56) i8* @"_SM41scala.collection.immutable.VectorStatics$G4load"() noinline

declare dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8*, i32) inlinehint
@"_SM37scala.scalanative.unsafe.Tag$CStruct5G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }

declare dereferenceable_or_null(24) i8* @"_SM31scala.util.hashing.MurmurHash3$G4load"() noinline

declare dereferenceable_or_null(32) i8* @"_SM24java.nio.charset.CharsetD14defaultCharsetL24java.nio.charset.CharsetEo"() inlinehint
@"_SM15java.lang.ClassG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"scalanative_eacces"()
@"_SM27scala.runtime.ScalaRunTime$G8instance" = external constant { i8* }

declare dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8*, i32)
@"_SM27java.lang.System$$$Lambda$2G4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }

declare i32 @"_SM27scala.runtime.ScalaRunTime$D9_hashCodeL13scala.ProductiEO"(i8*, i8*)
@"_SM40java.nio.file.FileAlreadyExistsExceptionG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare nonnull dereferenceable(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D16toUInt$extensioniL31scala.scalanative.unsigned.UIntEO"(i8*, i32) inlinehint
@"_SM41scala.collection.immutable.ArraySeq$ofIntG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }

declare i32 @"_SM26scala.LowPriorityImplicitsD10intWrapperiiEO"(i8*, i32) inlinehint

declare i64 @"_SM35scala.scalanative.runtime.LazyVals$D3getR_jEO"(i8*, i8*) alwaysinline
@"_SM32scala.scalanative.runtime.Array$G8instance" = external constant { i8* }
@"_SM24java.lang.AssertionErrorG4type" = external global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }

declare dereferenceable_or_null(32) i8* @"_SM13scala.Predef$D13augmentStringL16java.lang.StringL16java.lang.StringEO"(i8*, i8*) inlinehint

declare i8* @"scalanative_environ"()

declare dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO"(i8*)
@"_SM13scala.Option$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM13scala.Option$G4type" to i8*) }
@"_SM21scala.runtime.Arrays$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM21scala.runtime.Arrays$G4type" to i8*) }
@"_SM21scala.runtime.IntRef$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM21scala.runtime.IntRef$G4type" to i8*) }
@"_SM23java.nio.GenHeapBuffer$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM23java.nio.GenHeapBuffer$G4type" to i8*) }
@"_SM28scala.runtime.Scala3RunTime$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28scala.runtime.Scala3RunTime$G4type" to i8*) }
@"_SM29java.nio.file.PosixException$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM29java.nio.file.PosixException$G4type" to i8*) }
@"_SM33scala.collection.immutable.Range$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM33scala.collection.immutable.Range$G4type" to i8*) }
@"_SM35scala.scalanative.unsafe.CFuncPtr3$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM35scala.scalanative.unsafe.CFuncPtr3$G4type" to i8*) }
@"_SM36scala.scalanative.runtime.CharArray$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM36scala.scalanative.runtime.CharArray$G4type" to i8*) }
@"_SM36scala.scalanative.unsafe.CFuncPtr17$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM36scala.scalanative.unsafe.CFuncPtr17$G4type" to i8*) }
@"_SM36scala.scalanative.unsafe.CFuncPtr20$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM36scala.scalanative.unsafe.CFuncPtr20$G4type" to i8*) }
@"_SM38scala.scalanative.unsafe.Tag$CStruct5$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$G4type" to i8*) }
@"_SM75scala.scalanative.runtime.ieee754tostring.ryu.RyuRoundingMode$Conservative$G8instance" = hidden constant { i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM75scala.scalanative.runtime.ieee754tostring.ryu.RyuRoundingMode$Conservative$G4type" to i8*) }
@"_SM10scala.SomeG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 8, i32 0, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG1-1" to i8*) }, i32 16, i32 8, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM10scala.SomeD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM10scala.SomeD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM10scala.SomeD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM10scala.SomeD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM10scala.SomeD3getL16java.lang.ObjectEO" to i8*) ] }
@"_SM13scala.Option$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 13, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG1-4" to i8*) }, i32 8, i32 13, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM13scala.ProductG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -4, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG1-7" to i8*) }
@"_SM15java.lang.rtti$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 90, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG1-9" to i8*) }, i32 8, i32 90, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM15scala.Function1G4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -8, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-11" to i8*) }
@"_SM17java.lang.System$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 116, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-13" to i8*) }, i32 56, i32 116, { i8* } { i8* bitcast ([6 x i64]* @"_SM7__constG2-14" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM18java.lang.IterableG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -12, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-16" to i8*) }
@"_SM18java.lang.ReadableG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -13, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-18" to i8*) }
@"_SM18java.util.IteratorG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -15, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-20" to i8*) }
@"_SM19java.io.IOExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 132, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-22" to i8*) }, i32 40, i32 140, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-23" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM19java.lang.ExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 131, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-25" to i8*) }, i32 40, i32 174, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-23" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM20java.io.OutputStreamG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 180, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-27" to i8*) }, i32 8, i32 183, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [6 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM20java.io.OutputStreamD5flushuEO" to i8*), i8* null ] }
@"_SM20scala.collection.SeqG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -27, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-29" to i8*) }
@"_SM21java.util.AbstractMapG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 197, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-31" to i8*) }, i32 8, i32 198, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM21java.util.AbstractMapD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM21java.util.AbstractMapD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM21java.util.AbstractMapD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM21java.util.AbstractMapD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM21scala.runtime.Arrays$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 200, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-33" to i8*) }, i32 8, i32 200, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM21scala.runtime.IntRef$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 202, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-35" to i8*) }, i32 8, i32 202, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM23java.nio.GenHeapBuffer$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 217, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-37" to i8*) }, i32 8, i32 217, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM23scala.collection.MapOpsG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -37, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-39" to i8*) }
@"_SM24java.io.Reader$$Lambda$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 226, i32 62, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-41" to i8*) }, i32 16, i32 226, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM24java.io.Writer$$Lambda$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 227, i32 63, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-43" to i8*) }, i32 16, i32 227, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM24java.nio.file.LinkOptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 31, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-45" to i8*) }, i32 24, i32 32, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM24java.nio.file.OpenOptionG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -41, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-48" to i8*) }
@"_SM25scala.collection.IteratorG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -45, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-50" to i8*) }
@"_SM26java.lang.String$$Lambda$7G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 237, i32 64, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-52" to i8*) }, i32 16, i32 237, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM27java.lang.System$$$Lambda$6G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 250, i32 71, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-54" to i8*) }, i32 16, i32 250, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM28java.lang.System$$$Lambda$12G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 263, i32 77, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-56" to i8*) }, i32 16, i32 263, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM28scala.collection.AbstractSeqG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 384, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-58" to i8*) }, i32 8, i32 427, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractSeqD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.collection.AbstractSeqD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.collection.AbstractSeqD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractSeqD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM28scala.collection.mutable.SeqG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -55, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-60" to i8*) }
@"_SM28scala.reflect.AnyValManifestG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 274, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-62" to i8*) }, i32 24, i32 292, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.reflect.AnyValManifestD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.reflect.AnyValManifestD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM28scala.runtime.Scala3RunTime$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 293, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-64" to i8*) }, i32 8, i32 293, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM28scala.scalanative.unsafe.PtrG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 295, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-66" to i8*) }, i32 16, i32 295, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.scalanative.unsafe.PtrD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.scalanative.unsafe.PtrD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.scalanative.unsafe.PtrD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.scalanative.unsafe.PtrD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM29java.nio.charset.CoderResult$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 310, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-68" to i8*) }, i32 104, i32 310, { i8* } { i8* bitcast ([13 x i64]* @"_SM7__constG2-69" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM29java.nio.file.PosixException$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 311, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-71" to i8*) }, i32 8, i32 311, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM29java.util.Hashtable$$Lambda$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 312, i32 101, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-73" to i8*) }, i32 8, i32 312, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM29java.util.concurrent.ExecutorG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -57, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-75" to i8*) }
@"_SM30java.math.RoundingMode$$anon$7G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 29, i32 11, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-77" to i8*) }, i32 24, i32 29, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM30scala.collection.Map$$Lambda$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 325, i32 104, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-79" to i8*) }, i32 24, i32 325, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG2-80" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM30scala.collection.immutable.MapG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -62, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-82" to i8*) }
@"_SM31java.lang.NumberFormatExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 154, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-84" to i8*) }, i32 40, i32 154, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-23" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM32java.nio.file.LinkOption$$anon$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 32, i32 13, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-86" to i8*) }, i32 24, i32 32, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM32scala.collection.mutable.Buffer$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 521, i32 184, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-88" to i8*) }, i32 16, i32 521, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*, i8*)* @"_SM36scala.collection.SeqFactory$DelegateD4fromL29scala.collection.IterableOnceL23scala.collection.SeqOpsEO" to i8*) ] }
@"_SM32scala.collection.mutable.HashMapG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 380, i32 116, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-90" to i8*) }, i32 32, i32 380, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-91" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractMapD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.collection.AbstractMapD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM32scala.collection.mutable.HashMapD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractMapD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM33scala.collection.immutable.Range$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 462, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-93" to i8*) }, i32 8, i32 462, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM33scala.scalanative.unsafe.package$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 489, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-95" to i8*) }, i32 16, i32 489, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM34scala.collection.IndexedSeqView$IdG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 431, i32 151, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-97" to i8*) }, i32 16, i32 431, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM29scala.collection.AbstractViewD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM34scala.collection.Iterator$$anon$19G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 437, i32 155, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG2-99" to i8*) }, i32 8, i32 437, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM33scala.collection.AbstractIteratorD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM34scala.collection.immutable.Vector3G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 412, i32 136, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-101" to i8*) }, i32 72, i32 412, { i8* } { i8* bitcast ([6 x i64]* @"_SM7__constG3-102" to i8*) }, [6 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractSeqD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.collection.AbstractSeqD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.collection.AbstractSeqD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractSeqD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM34scala.collection.immutable.Vector3D11vectorSliceiLAL16java.lang.Object_EO" to i8*), i8* bitcast (i32 (i8*)* @"_SM34scala.collection.immutable.Vector3D16vectorSliceCountiEO" to i8*) ] }
@"_SM34scala.scalanative.unsafe.CFuncPtr2G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 468, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-104" to i8*) }, i32 16, i32 468, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM35java.util.Collections$WrappedEqualsG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -82, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-106" to i8*) }
@"_SM35scala.collection.ClassTagSeqFactoryG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -83, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-108" to i8*) }
@"_SM35scala.scalanative.unsafe.CFuncPtr12G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 478, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-110" to i8*) }, i32 16, i32 478, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM35scala.scalanative.unsafe.CFuncPtr3$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 509, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-112" to i8*) }, i32 8, i32 509, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM36scala.collection.immutable.ArraySeq$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 523, i32 186, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-114" to i8*) }, i32 32, i32 523, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG3-115" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM36scala.collection.mutable.AbstractMapG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 379, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-117" to i8*) }, i32 8, i32 380, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractMapD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.collection.AbstractMapD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.collection.AbstractMapD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractMapD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM36scala.scalanative.runtime.CharArray$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 526, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-119" to i8*) }, i32 8, i32 526, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM36scala.scalanative.unsafe.CFuncPtr17$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 535, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-121" to i8*) }, i32 8, i32 535, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM36scala.scalanative.unsafe.CFuncPtr20$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 538, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-123" to i8*) }, i32 8, i32 538, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM37java.nio.file.FileVisitOption$$anon$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 34, i32 14, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-125" to i8*) }, i32 24, i32 34, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM37java.nio.file.FileVisitResult$$anon$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 36, i32 15, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-127" to i8*) }, i32 24, i32 36, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM37java.util.concurrent.TimeUnit$$anon$7G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 47, i32 25, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-129" to i8*) }, i32 24, i32 47, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM37scala.collection.immutable.VectorImplG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 408, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-131" to i8*) }, i32 16, i32 416, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [6 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractSeqD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.collection.AbstractSeqD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.collection.AbstractSeqD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractSeqD6equalsL16java.lang.ObjectzEO" to i8*), i8* null, i8* null ] }
@"_SM37scala.scalanative.runtime.DoubleArrayG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 353, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-133" to i8*) }, i32 8, i32 353, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [8 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM37scala.scalanative.runtime.DoubleArrayD6strideL32scala.scalanative.unsigned.ULongEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM37scala.scalanative.runtime.DoubleArrayD5applyiL16java.lang.ObjectEO" to i8*), i8* bitcast (i8* (i8*, i32, i8*)* @"_SM37scala.scalanative.runtime.DoubleArrayD6updateiL16java.lang.ObjectuEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM37scala.scalanative.runtime.DoubleArrayD5atRawiR_EO" to i8*) ] }
@"_SM38scala.scalanative.unsafe.Tag$CStruct5$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 569, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-135" to i8*) }, i32 8, i32 569, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$D8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM40java.nio.file.StandardOpenOption$$anon$7G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 59, i32 35, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-137" to i8*) }, i32 24, i32 59, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM40scala.collection.immutable.Map$EmptyMap$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 383, i32 117, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-139" to i8*) }, i32 8, i32 383, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractMapD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.collection.AbstractMapD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.collection.AbstractMapD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.collection.AbstractMapD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM41java.lang.StringIndexOutOfBoundsExceptionG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 168, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-141" to i8*) }, i32 40, i32 168, { i8* } { i8* bitcast ([4 x i64]* @"_SM7__constG2-23" to i8*) }, [5 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM19java.lang.ThrowableD10getMessageL16java.lang.StringEO" to i8*) ] }
@"_SM41scala.reflect.ClassManifestDeprecatedApisG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -113, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-143" to i8*) }
@"_SM42scala.collection.immutable.ArraySeq$ofCharG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 421, i32 144, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-145" to i8*) }, i32 16, i32 421, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [7 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM42scala.collection.immutable.ArraySeq$ofCharD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.collection.AbstractSeqD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM42scala.collection.immutable.ArraySeq$ofCharD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM42scala.collection.immutable.ArraySeq$ofCharD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM42scala.collection.immutable.ArraySeq$ofCharD11unsafeArrayL16java.lang.ObjectEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM42scala.collection.immutable.ArraySeq$ofCharD5applyiL16java.lang.ObjectEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM42scala.collection.immutable.ArraySeq$ofCharD6lengthiEO" to i8*) ] }
@"_SM42scala.reflect.ManifestFactory$UnitManifestG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 283, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-147" to i8*) }, i32 24, i32 284, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.reflect.AnyValManifestD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.reflect.AnyValManifestD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM43java.util.FormatterCompanionImpl$LocaleInfoG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 609, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-149" to i8*) }, i32 16, i32 610, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM43scala.collection.StrictOptimizedIterableOpsG4type" = hidden global { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 -115, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-151" to i8*) }
@"_SM43scala.collection.mutable.ArrayBuilder$ofRefG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 548, i32 192, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-153" to i8*) }, i32 32, i32 548, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG3-154" to i8*) }, [8 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6resizeiuEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD5elemsL16java.lang.ObjectEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6resultL16java.lang.ObjectEO" to i8*), i8* bitcast (i8* (i8*, i8*)* @"_SM37scala.collection.mutable.ArrayBuilderD6addAllL29scala.collection.IterableOnceL37scala.collection.mutable.ArrayBuilderEO" to i8*) ] }
@"_SM44scala.collection.Iterator$$anon$21$$Lambda$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 614, i32 221, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-156" to i8*) }, i32 24, i32 614, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG2-80" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM44scala.collection.mutable.ArrayBuilder$ofLongG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 551, i32 195, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-158" to i8*) }, i32 24, i32 551, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [8 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6resizeiuEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD5elemsL16java.lang.ObjectEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6resultL16java.lang.ObjectEO" to i8*), i8* bitcast (i8* (i8*, i8*)* @"_SM37scala.collection.mutable.ArrayBuilderD6addAllL29scala.collection.IterableOnceL37scala.collection.mutable.ArrayBuilderEO" to i8*) ] }
@"_SM44scala.collection.mutable.ArrayBuilder$ofUnitG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 552, i32 196, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-160" to i8*) }, i32 16, i32 552, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [8 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*, i32)* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6resizeiuEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD5elemsL16java.lang.ObjectEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6resultL16java.lang.ObjectEO" to i8*), i8* bitcast (i8* (i8*, i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6addAllL29scala.collection.IterableOnceL37scala.collection.mutable.ArrayBuilderEO" to i8*) ] }
@"_SM44scala.reflect.ManifestFactory$ShortManifest$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 288, i32 89, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-162" to i8*) }, i32 24, i32 288, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM28scala.reflect.AnyValManifestD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM28scala.reflect.AnyValManifestD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM46scala.collection.ArrayOps$ArrayIterator$mcD$spG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 443, i32 161, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-164" to i8*) }, i32 32, i32 443, { i8* } { i8* bitcast ([3 x i64]* @"_SM7__constG3-154" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM33scala.collection.AbstractIteratorD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM4MainG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 630, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-166" to i8*) }, i32 8, i32 630, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM50java.nio.channels.spi.AbstractInterruptibleChannelG4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 631, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-168" to i8*) }, i32 8, i32 633, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM50scala.collection.immutable.VectorBuilder$$Lambda$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 634, i32 233, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-170" to i8*) }, i32 16, i32 634, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG1-2" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM51java.nio.file.attribute.PosixFilePermission$$anon$1G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 73, i32 46, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-172" to i8*) }, i32 24, i32 73, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM51java.nio.file.attribute.PosixFilePermission$$anon$9G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 81, i32 54, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-174" to i8*) }, i32 24, i32 81, { i8* } { i8* bitcast ([2 x i64]* @"_SM7__constG2-46" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM14java.lang.EnumD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM65scala.scalanative.runtime.ieee754tostring.ryu.RyuFloat$$$Lambda$2G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 646, i32 236, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-176" to i8*) }, i32 56, i32 646, { i8* } { i8* bitcast ([5 x i64]* @"_SM7__constG3-177" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM66scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$$$Lambda$6G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 655, i32 245, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-179" to i8*) }, i32 56, i32 655, { i8* } { i8* bitcast ([6 x i64]* @"_SM7__constG2-14" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }
@"_SM75scala.scalanative.runtime.ieee754tostring.ryu.RyuRoundingMode$Conservative$G4type" = hidden global { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] } { { i8*, i32, i32, i8* } { i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM15java.lang.ClassG4type" to i8*), i32 643, i32 -1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-181" to i8*) }, i32 8, i32 643, { i8* } { i8* bitcast ([1 x i64]* @"_SM7__constG1-5" to i8*) }, [4 x i8*] [ i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO" to i8*), i8* bitcast (i8* (i8*)* @"_SM16java.lang.ObjectD8toStringL16java.lang.StringEO" to i8*), i8* bitcast (i32 (i8*)* @"_SM16java.lang.ObjectD8hashCodeiEO" to i8*), i8* bitcast (i1 (i8*, i8*)* @"_SM16java.lang.ObjectD6equalsL16java.lang.ObjectzEO" to i8*) ] }

define i32 @"_SM10scala.SomeD12productArityiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i32 1
}

define nonnull dereferenceable(32) i8* @"_SM10scala.SomeD13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-183" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM10scala.SomeD14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  switch i32 %_2, label %_40000.0 [
    i32 0, label %_50000.0
  ]
_40000.0:
  %_40001 = call dereferenceable_or_null(8) i8* @"_SM21scala.runtime.StaticsD5ioobeiL16java.lang.ObjectEo"(i32 %_2)
  br label %_60000.0
_50000.0:
  %_70004 = icmp ne i8* %_1, null
  br i1 %_70004, label %_70002.0, label %_70003.0
_70002.0:
  %_70007 = bitcast i8* %_1 to { i8*, i8* }*
  %_70008 = getelementptr { i8*, i8* }, { i8*, i8* }* %_70007, i32 0, i32 1
  %_70005 = bitcast i8** %_70008 to i8*
  %_70009 = bitcast i8* %_70005 to i8**
  %_70001 = load i8*, i8** %_70009, !dereferenceable_or_null !{i64 8}
  br label %_60000.0
_60000.0:
  %_60001 = phi i8* [%_70001, %_70002.0], [%_40001, %_40000.0]
  ret i8* %_60001
_70003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM10scala.SomeD15productIteratorL25scala.collection.IteratorEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20003 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM27scala.runtime.ScalaRunTime$D20typedProductIteratorL13scala.ProductL25scala.collection.IteratorEO" to i8*) to i8* (i8*, i8*)*
  %_20002 = call dereferenceable_or_null(8) i8* %_20003(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(16) %_1)
  ret i8* %_20002
}

define dereferenceable_or_null(8) i8* @"_SM10scala.SomeD3getL16java.lang.ObjectEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i8* }*
  %_30008 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30007, i32 0, i32 1
  %_30005 = bitcast i8** %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i8**
  %_30001 = load i8*, i8** %_30009, !dereferenceable_or_null !{i64 8}
  ret i8* %_30001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM10scala.SomeD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_1, %_2
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  br label %_70000.0
_70000.0:
  %_200005 = icmp eq i8* %_2, null
  br i1 %_200005, label %_200002.0, label %_200003.0
_200002.0:
  br label %_200004.0
_200003.0:
  %_200033 = bitcast i8* %_2 to i8**
  %_200006 = load i8*, i8** %_200033
  %_200007 = icmp eq i8* %_200006, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM10scala.SomeG4type" to i8*)
  br label %_200004.0
_200004.0:
  %_70002 = phi i1 [%_200007, %_200003.0], [false, %_200002.0]
  br i1 %_70002, label %_80000.0, label %_90000.0
_80000.0:
  br label %_100000.0
_90000.0:
  br label %_110000.0
_110000.0:
  br label %_100000.0
_100000.0:
  %_100001 = phi i1 [false, %_110000.0], [true, %_80000.0]
  br i1 %_100001, label %_120000.0, label %_130000.0
_120000.0:
  %_200011 = icmp eq i8* %_2, null
  br i1 %_200011, label %_200009.0, label %_200008.0
_200008.0:
  %_200034 = bitcast i8* %_2 to i8**
  %_200012 = load i8*, i8** %_200034
  %_200013 = icmp eq i8* %_200012, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM10scala.SomeG4type" to i8*)
  br i1 %_200013, label %_200009.0, label %_200010.0
_200009.0:
  %_120001 = bitcast i8* %_2 to i8*
  %_200016 = icmp ne i8* %_1, null
  br i1 %_200016, label %_200014.0, label %_200015.0
_200014.0:
  %_200035 = bitcast i8* %_1 to { i8*, i8* }*
  %_200036 = getelementptr { i8*, i8* }, { i8*, i8* }* %_200035, i32 0, i32 1
  %_200017 = bitcast i8** %_200036 to i8*
  %_200037 = bitcast i8* %_200017 to i8**
  %_140001 = load i8*, i8** %_200037, !dereferenceable_or_null !{i64 8}
  %_120003 = icmp eq i8* %_140001, null
  br i1 %_120003, label %_150000.0, label %_160000.0
_150000.0:
  %_200019 = icmp ne i8* %_120001, null
  br i1 %_200019, label %_200018.0, label %_200015.0
_200018.0:
  %_200038 = bitcast i8* %_120001 to { i8*, i8* }*
  %_200039 = getelementptr { i8*, i8* }, { i8*, i8* }* %_200038, i32 0, i32 1
  %_200020 = bitcast i8** %_200039 to i8*
  %_200040 = bitcast i8* %_200020 to i8**
  %_170001 = load i8*, i8** %_200040, !dereferenceable_or_null !{i64 8}
  %_150002 = icmp eq i8* %_170001, null
  br label %_180000.0
_160000.0:
  %_200022 = icmp ne i8* %_120001, null
  br i1 %_200022, label %_200021.0, label %_200015.0
_200021.0:
  %_200041 = bitcast i8* %_120001 to { i8*, i8* }*
  %_200042 = getelementptr { i8*, i8* }, { i8*, i8* }* %_200041, i32 0, i32 1
  %_200023 = bitcast i8** %_200042 to i8*
  %_200043 = bitcast i8* %_200023 to i8**
  %_190001 = load i8*, i8** %_200043, !dereferenceable_or_null !{i64 8}
  %_200025 = icmp ne i8* %_140001, null
  br i1 %_200025, label %_200024.0, label %_200015.0
_200024.0:
  %_200044 = bitcast i8* %_140001 to i8**
  %_200026 = load i8*, i8** %_200044
  %_200045 = bitcast i8* %_200026 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_200046 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_200045, i32 0, i32 4, i32 0
  %_200027 = bitcast i8** %_200046 to i8*
  %_200047 = bitcast i8* %_200027 to i8**
  %_160002 = load i8*, i8** %_200047
  %_200048 = bitcast i8* %_160002 to i1 (i8*, i8*)*
  %_160003 = call i1 %_200048(i8* dereferenceable_or_null(8) %_140001, i8* dereferenceable_or_null(8) %_190001)
  br label %_180000.0
_180000.0:
  %_180001 = phi i1 [%_160003, %_200024.0], [%_150002, %_200018.0]
  br label %_200000.0
_130000.0:
  br label %_200000.0
_200000.0:
  %_200001 = phi i1 [false, %_130000.0], [%_180001, %_180000.0]
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_200001, %_200000.0], [true, %_40000.0]
  ret i1 %_60001
_200015.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_200010.0:
  %_200029 = phi i8* [%_2, %_200008.0]
  %_200030 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM10scala.SomeG4type" to i8*), %_200008.0]
  %_200049 = bitcast i8* %_200029 to i8**
  %_200031 = load i8*, i8** %_200049
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_200031, i8* %_200030)
  unreachable
}

define i32 @"_SM10scala.SomeD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = call i32 @"_SM27scala.runtime.ScalaRunTime$D9_hashCodeL13scala.ProductiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(16) %_1)
  ret i32 %_20002
}

define dereferenceable_or_null(32) i8* @"_SM10scala.SomeD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = call dereferenceable_or_null(32) i8* @"_SM27scala.runtime.ScalaRunTime$D9_toStringL13scala.ProductL16java.lang.StringEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(16) %_1)
  ret i8* %_20002
}

define nonnull dereferenceable(8) i8* @"_SM13scala.Option$D5applyL16java.lang.ObjectL12scala.OptionEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_2, null
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  %_40001 = call dereferenceable_or_null(8) i8* @"_SM11scala.None$G4load"()
  br label %_60000.0
_50000.0:
  %_80001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM10scala.SomeG4type" to i8*), i64 16)
  %_80009 = bitcast i8* %_80001 to { i8*, i8* }*
  %_80010 = getelementptr { i8*, i8* }, { i8*, i8* }* %_80009, i32 0, i32 1
  %_80006 = bitcast i8** %_80010 to i8*
  %_80011 = bitcast i8* %_80006 to i8**
  store i8* %_2, i8** %_80011
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_80001)
  call nonnull dereferenceable(8) i8* @"_SM13scala.ProductD6$init$uEO"(i8* nonnull dereferenceable(16) %_80001)
  br label %_60000.0
_60000.0:
  %_60001 = phi i8* [%_80001, %_50000.0], [%_40001, %_40000.0]
  ret i8* %_60001
}

define nonnull dereferenceable(8) i8* @"_SM13scala.Option$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(24) i8* @"_SM13scala.ProductD15productIteratorL25scala.collection.IteratorEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30002 = icmp eq i8* %_1, null
  br i1 %_30002, label %_40000.0, label %_50000.0
_50000.0:
  br label %_60000.0
_40000.0:
  %_70008 = icmp ne i8* null, null
  br i1 %_70008, label %_70006.0, label %_70007.0
_70006.0:
  call i8* @"scalanative_throw"(i8* null)
  unreachable
_60000.0:
  %_70001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM21scala.Product$$anon$1G4type" to i8*), i64 24)
  %_70027 = bitcast i8* %_70001 to { i8*, i32, i32, i8* }*
  %_70028 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_70027, i32 0, i32 3
  %_70011 = bitcast i8** %_70028 to i8*
  %_70029 = bitcast i8* %_70011 to i8**
  store i8* %_1, i8** %_70029
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(24) %_70001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(24) %_70001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IteratorD6$init$uEO"(i8* nonnull dereferenceable(24) %_70001)
  %_70030 = bitcast i8* %_70001 to { i8*, i32, i32, i8* }*
  %_70031 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_70030, i32 0, i32 2
  %_70016 = bitcast i32* %_70031 to i8*
  %_70032 = bitcast i8* %_70016 to i32*
  store i32 0, i32* %_70032
  %_70018 = icmp ne i8* %_1, null
  br i1 %_70018, label %_70017.0, label %_70007.0
_70017.0:
  %_70033 = bitcast i8* %_1 to i8**
  %_70019 = load i8*, i8** %_70033
  %_70034 = bitcast i8* %_70019 to { i8*, i32, i32, i8* }*
  %_70035 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_70034, i32 0, i32 2
  %_70020 = bitcast i32* %_70035 to i8*
  %_70036 = bitcast i8* %_70020 to i32*
  %_70021 = load i32, i32* %_70036
  %_70037 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_70038 = getelementptr i8*, i8** %_70037, i32 2365
  %_70022 = bitcast i8** %_70038 to i8*
  %_70039 = bitcast i8* %_70022 to i8**
  %_70040 = getelementptr i8*, i8** %_70039, i32 %_70021
  %_70023 = bitcast i8** %_70040 to i8*
  %_70041 = bitcast i8* %_70023 to i8**
  %_60003 = load i8*, i8** %_70041
  %_70042 = bitcast i8* %_60003 to i32 (i8*)*
  %_60004 = call i32 %_70042(i8* dereferenceable_or_null(8) %_1)
  %_70043 = bitcast i8* %_70001 to { i8*, i32, i32, i8* }*
  %_70044 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_70043, i32 0, i32 1
  %_70025 = bitcast i32* %_70044 to i8*
  %_70045 = bitcast i8* %_70025 to i32*
  store i32 %_60004, i32* %_70045
  ret i8* %_70001
_70007.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM13scala.ProductD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"__check_class_has_trait"(i32 %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_5.0:
  %_6 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_7 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_6, i32 0, i32 %_1, i32 %_2
  %_3 = bitcast i1* %_7 to i8*
  %_8 = bitcast i8* %_3 to i1*
  %_4 = load i1, i1* %_8
  ret i1 %_4
}

define i1 @"__check_trait_has_trait"(i32 %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_5.0:
  %_6 = bitcast i8* bitcast ([122 x [122 x i1]]* @"_ST17__trait_has_trait" to i8*) to [122 x [122 x i1]]*
  %_7 = getelementptr [122 x [122 x i1]], [122 x [122 x i1]]* %_6, i32 0, i32 %_1, i32 %_2
  %_3 = bitcast i1* %_7 to i8*
  %_8 = bitcast i8* %_3 to i1*
  %_4 = load i1, i1* %_8
  ret i1 %_4
}

define i32 @"_SM15scala.Function1D13apply$mcII$spiiEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30009 = icmp ne i8* %_1, null
  br i1 %_30009, label %_30007.0, label %_30008.0
_30007.0:
  %_30016 = bitcast i8* %_1 to i8**
  %_30010 = load i8*, i8** %_30016
  %_30017 = bitcast i8* %_30010 to { i8*, i32, i32, i8* }*
  %_30018 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30017, i32 0, i32 2
  %_30011 = bitcast i32* %_30018 to i8*
  %_30019 = bitcast i8* %_30011 to i32*
  %_30012 = load i32, i32* %_30019
  %_30020 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30021 = getelementptr i8*, i8** %_30020, i32 1010
  %_30013 = bitcast i8** %_30021 to i8*
  %_30022 = bitcast i8* %_30013 to i8**
  %_30023 = getelementptr i8*, i8** %_30022, i32 %_30012
  %_30014 = bitcast i8** %_30023 to i8*
  %_30024 = bitcast i8* %_30014 to i8**
  %_30003 = load i8*, i8** %_30024
  %_30004 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_2)
  %_30025 = bitcast i8* %_30003 to i8* (i8*, i8*)*
  %_30005 = call dereferenceable_or_null(8) i8* %_30025(i8* dereferenceable_or_null(8) %_1, i8* nonnull dereferenceable(16) %_30004)
  %_30026 = bitcast i8* bitcast (i32 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D10unboxToIntL16java.lang.ObjectiEO" to i8*) to i32 (i8*, i8*)*
  %_30006 = call i32 %_30026(i8* null, i8* dereferenceable_or_null(8) %_30005)
  ret i32 %_30006
_30008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(32) i8* @"_SM15scala.Function1D8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-185" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D10$anonfun$1L12scala.OptionEPT17java.lang.System$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20003 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D5env$1L16java.lang.StringL12scala.OptionEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-187" to i8*))
  ret i8* %_20003
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D10$anonfun$2L12scala.OptionEPT17java.lang.System$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20003 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D5env$1L16java.lang.StringL12scala.OptionEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-189" to i8*))
  ret i8* %_20003
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D10$anonfun$3L12scala.OptionEPT17java.lang.System$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20003 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D5env$1L16java.lang.StringL12scala.OptionEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-191" to i8*))
  ret i8* %_20003
}

define nonnull dereferenceable(32) i8* @"_SM17java.lang.System$D10$anonfun$4L16java.lang.StringEPT17java.lang.System$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-193" to i8*)
}

define i1 @"_SM17java.lang.System$D10$anonfun$5czEPT17java.lang.System$"(i8* %_1, i16 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30003 = zext i16 %_2 to i32
  %_30004 = icmp ne i32 %_30003, 61
  ret i1 %_30004
}

define dereferenceable_or_null(32) i8* @"_SM17java.lang.System$D11getPropertyL16java.lang.StringL16java.lang.StringL16java.lang.StringEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call dereferenceable_or_null(56) i8* @"_SM17java.lang.System$G4load"()
  %_40005 = bitcast i8* %_40001 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_40006 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_40005, i32 0, i32 2
  %_40004 = bitcast i8** %_40006 to i8*
  %_40007 = bitcast i8* %_40004 to i8**
  %_40002 = load i8*, i8** %_40007, !dereferenceable_or_null !{i64 24}
  %_40003 = call dereferenceable_or_null(32) i8* @"_SM20java.util.PropertiesD11getPropertyL16java.lang.StringL16java.lang.StringL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_40002, i8* dereferenceable_or_null(32) %_2, i8* dereferenceable_or_null(32) %_3)
  ret i8* %_40003
}

define nonnull dereferenceable(32) i8* @"_SM17java.lang.System$D13getEnvsUnix$1L17java.util.HashMapEPT17java.lang.System$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  br label %_70000.0
_70000.0:
  br label %_80000.0
_80000.0:
  br label %_90000.0
_90000.0:
  br label %_100000.0
_100000.0:
  %_100001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM17java.util.HashMapG4type" to i8*), i64 32)
  %_660022 = bitcast i8* %_100001 to { i8*, i32, i32, i8*, float }*
  %_660023 = getelementptr { i8*, i32, i32, i8*, float }, { i8*, i32, i32, i8*, float }* %_660022, i32 0, i32 4
  %_660006 = bitcast float* %_660023 to i8*
  %_660024 = bitcast i8* %_660006 to float*
  store float 0x3fe8000000000000, float* %_660024
  %_100003 = call i32 @"_SM17java.util.HashMapD12tableSizeForiiEPT17java.util.HashMap"(i8* nonnull dereferenceable(32) %_100001, i32 16)
  %_100004 = call dereferenceable_or_null(8) i8* @"_SM38scala.scalanative.runtime.ObjectArray$D5allociL37scala.scalanative.runtime.ObjectArrayEO"(i8* bitcast ({ i8* }* @"_SM38scala.scalanative.runtime.ObjectArray$G8instance" to i8*), i32 %_100003)
  %_660025 = bitcast i8* %_100001 to { i8*, i32, i32, i8*, float }*
  %_660026 = getelementptr { i8*, i32, i32, i8*, float }, { i8*, i32, i32, i8*, float }* %_660025, i32 0, i32 3
  %_660009 = bitcast i8** %_660026 to i8*
  %_660027 = bitcast i8* %_660009 to i8**
  store i8* %_100004, i8** %_660027
  %_660028 = bitcast i8* %_100001 to { i8*, i32, i32, i8*, float }*
  %_660029 = getelementptr { i8*, i32, i32, i8*, float }, { i8*, i32, i32, i8*, float }* %_660028, i32 0, i32 3
  %_660010 = bitcast i8** %_660029 to i8*
  %_660030 = bitcast i8* %_660010 to i8**
  %_100006 = load i8*, i8** %_660030, !dereferenceable_or_null !{i64 8}
  %_660013 = icmp ne i8* %_100006, null
  br i1 %_660013, label %_660011.0, label %_660012.0
_660011.0:
  %_660031 = bitcast i8* %_100006 to { i8*, i32 }*
  %_660032 = getelementptr { i8*, i32 }, { i8*, i32 }* %_660031, i32 0, i32 1
  %_660014 = bitcast i32* %_660032 to i8*
  %_660033 = bitcast i8* %_660014 to i32*
  %_100007 = load i32, i32* %_660033
  %_100008 = call i32 @"_SM17java.util.HashMapD12newThresholdiiEPT17java.util.HashMap"(i8* nonnull dereferenceable(32) %_100001, i32 %_100007)
  %_660034 = bitcast i8* %_100001 to { i8*, i32, i32, i8*, float }*
  %_660035 = getelementptr { i8*, i32, i32, i8*, float }, { i8*, i32, i32, i8*, float }* %_660034, i32 0, i32 2
  %_660016 = bitcast i32* %_660035 to i8*
  %_660036 = bitcast i8* %_660016 to i32*
  store i32 %_100008, i32* %_660036
  %_660037 = bitcast i8* %_100001 to { i8*, i32, i32, i8*, float }*
  %_660038 = getelementptr { i8*, i32, i32, i8*, float }, { i8*, i32, i32, i8*, float }* %_660037, i32 0, i32 1
  %_660018 = bitcast i32* %_660038 to i8*
  %_660039 = bitcast i8* %_660018 to i32*
  store i32 0, i32* %_660039
  %_20004 = call i8* @"scalanative_environ"()
  br label %_110000.0
_110000.0:
  %_110001 = phi i32 [0, %_660011.0], [%_420004, %_420000.0]
  %_110003 = call i64 @"_SM10scala.Int$D8int2longijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM10scala.Int$G8instance" to i8*), i32 %_110001)
  %_660040 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_440003 = call dereferenceable_or_null(16) i8* %_660040(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_490003 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 8)
  %_490004 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_490003)
  %_460003 = call i64 @"_SM32scala.scalanative.unsigned.ULongD6toLongjEO"(i8* dereferenceable_or_null(16) %_490004)
  %_540002 = mul i64 %_110003, %_460003
  %_660041 = bitcast i8* %_20004 to i8*
  %_660042 = getelementptr i8, i8* %_660041, i64 %_540002
  %_540003 = bitcast i8* %_660042 to i8*
  %_660043 = bitcast i8* %_540003 to i8**
  %_540004 = load i8*, i8** %_660043
  %_110007 = icmp eq i8* null, %_540004
  %_110008 = xor i1 %_110007, true
  br i1 %_110008, label %_240000.0, label %_250000.0
_240000.0:
  %_240002 = call i64 @"_SM10scala.Int$D8int2longijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM10scala.Int$G8instance" to i8*), i32 %_110001)
  %_660044 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_560001 = call dereferenceable_or_null(16) i8* %_660044(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_610001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 8)
  %_610002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_610001)
  %_580003 = call i64 @"_SM32scala.scalanative.unsigned.ULongD6toLongjEO"(i8* dereferenceable_or_null(16) %_610002)
  %_660002 = mul i64 %_240002, %_580003
  %_660045 = bitcast i8* %_20004 to i8*
  %_660046 = getelementptr i8, i8* %_660045, i64 %_660002
  %_660003 = bitcast i8* %_660046 to i8*
  %_660047 = bitcast i8* %_660003 to i8**
  %_660004 = load i8*, i8** %_660047
  %_240003 = call dereferenceable_or_null(16) i8* @"_SM33scala.scalanative.unsafe.package$G4load"()
  %_240004 = call dereferenceable_or_null(32) i8* @"_SM33scala.scalanative.unsafe.package$D21fromCString$default$2L24java.nio.charset.CharsetEO"(i8* nonnull dereferenceable(16) %_240003)
  %_660048 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D8boxToPtrR_L28scala.scalanative.unsafe.PtrEO" to i8*) to i8* (i8*, i8*)*
  %_240005 = call dereferenceable_or_null(16) i8* %_660048(i8* null, i8* %_660004)
  %_660049 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM33scala.scalanative.unsafe.package$D11fromCStringL28scala.scalanative.unsafe.PtrL24java.nio.charset.CharsetL16java.lang.StringEO" to i8*) to i8* (i8*, i8*, i8*)*
  %_240006 = call dereferenceable_or_null(32) i8* %_660049(i8* nonnull dereferenceable(16) %_240003, i8* dereferenceable_or_null(16) %_240005, i8* dereferenceable_or_null(32) %_240004)
  %_240009 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_240010 = call dereferenceable_or_null(32) i8* @"_SM13scala.Predef$D13augmentStringL16java.lang.StringL16java.lang.StringEO"(i8* nonnull dereferenceable(16) %_240009, i8* dereferenceable_or_null(32) %_240006)
  %_240012 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.lang.System$$$Lambda$17G4type" to i8*), i64 16)
  %_660050 = bitcast i8* %_240012 to { i8*, i8* }*
  %_660051 = getelementptr { i8*, i8* }, { i8*, i8* }* %_660050, i32 0, i32 1
  %_660020 = bitcast i8** %_660051 to i8*
  %_660052 = bitcast i8* %_660020 to i8**
  store i8* %_1, i8** %_660052
  %_240014 = call dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D19takeWhile$extensionL16java.lang.StringL15scala.Function1L16java.lang.StringEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_240010, i8* nonnull dereferenceable(16) %_240012)
  %_240015 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* dereferenceable_or_null(32) %_240014)
  %_240016 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* dereferenceable_or_null(32) %_240006)
  %_240018 = icmp slt i32 %_240015, %_240016
  br i1 %_240018, label %_400000.0, label %_410000.0
_400000.0:
  %_400001 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* dereferenceable_or_null(32) %_240014)
  %_400003 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* dereferenceable_or_null(32) %_240006)
  %_400004 = add i32 %_400001, 1
  %_400005 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD9substringiiL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_240006, i32 %_400004, i32 %_400003)
  br label %_420000.0
_410000.0:
  br label %_420000.0
_420000.0:
  %_420001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-195" to i8*), %_410000.0], [%_400005, %_400000.0]
  %_420002 = call dereferenceable_or_null(8) i8* @"_SM17java.util.HashMapD3putL16java.lang.ObjectL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(32) %_100001, i8* dereferenceable_or_null(32) %_240014, i8* dereferenceable_or_null(32) %_420001)
  %_420004 = add i32 %_110001, 1
  br label %_110000.0
_250000.0:
  ret i8* %_100001
_660012.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(32) i8* @"_SM17java.lang.System$D13lineSeparatorL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = call i1 @"scalanative_platform_is_windows"()
  br i1 %_20002, label %_30000.0, label %_40000.0
_30000.0:
  br label %_50000.0
_40000.0:
  br label %_50000.0
_50000.0:
  %_50001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-197" to i8*), %_40000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-199" to i8*), %_30000.0]
  ret i8* %_50001
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D14getUserCountryL12scala.OptionEPT17java.lang.System$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  br label %_30000.0
_30000.0:
  %_30004 = call dereferenceable_or_null(32) i8* @"_SM17java.lang.System$D6getenvL16java.lang.StringL16java.lang.StringEO"(i8* dereferenceable_or_null(56) %_1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-201" to i8*))
  %_60003 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM13scala.Option$D5applyL16java.lang.ObjectL12scala.OptionEO" to i8*) to i8* (i8*, i8*)*
  %_30005 = call dereferenceable_or_null(8) i8* %_60003(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM13scala.Option$G8instance" to i8*), i8* dereferenceable_or_null(32) %_30004)
  %_30007 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.lang.System$$$Lambda$13G4type" to i8*), i64 16)
  %_60004 = bitcast i8* %_30007 to { i8*, i8* }*
  %_60005 = getelementptr { i8*, i8* }, { i8*, i8* }* %_60004, i32 0, i32 1
  %_60002 = bitcast i8** %_60005 to i8*
  %_60006 = bitcast i8* %_60002 to i8**
  store i8* %_1, i8** %_60006
  %_60007 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM12scala.OptionD3mapL15scala.Function1L12scala.OptionEO" to i8*) to i8* (i8*, i8*)*
  %_30009 = call dereferenceable_or_null(8) i8* %_60007(i8* dereferenceable_or_null(8) %_30005, i8* nonnull dereferenceable(16) %_30007)
  br label %_60000.0
_60000.0:
  ret i8* %_30009
}

define nonnull dereferenceable(24) i8* @"_SM17java.lang.System$D14loadPropertiesL20java.util.PropertiesEPT17java.lang.System$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_50001 = call dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.HashMap$G4load"()
  %_260041 = bitcast i8* bitcast (i8* (i8*)* @"_SM33scala.collection.mutable.HashMap$D5emptyL32scala.collection.mutable.HashMapEO" to i8*) to i8* (i8*)*
  %_50002 = call dereferenceable_or_null(32) i8* %_260041(i8* nonnull dereferenceable(8) %_50001)
  %_20006 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM20java.util.PropertiesG4type" to i8*), i64 24)
  %_260042 = bitcast i8* %_20006 to { i8*, i8* }*
  %_260043 = getelementptr { i8*, i8* }, { i8*, i8* }* %_260042, i32 0, i32 1
  %_260002 = bitcast i8** %_260043 to i8*
  %_260044 = bitcast i8* %_260002 to i8**
  store i8* %_50002, i8** %_260044
  %_20008 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-203" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-205" to i8*))
  %_20013 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-207" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-205" to i8*))
  %_20018 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-209" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-211" to i8*))
  %_20023 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-213" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-215" to i8*))
  %_20028 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-217" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-219" to i8*))
  %_20033 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-221" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-205" to i8*))
  %_20038 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-223" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-211" to i8*))
  %_20043 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-225" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-227" to i8*))
  %_260045 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.lang.System$D13lineSeparatorL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_20044 = call dereferenceable_or_null(32) i8* %_260045(i8* dereferenceable_or_null(56) %_1)
  %_20047 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-229" to i8*), i8* dereferenceable_or_null(32) %_20044)
  %_20048 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D19getCurrentDirectoryL12scala.OptionEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_1)
  %_20050 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.System$$$Lambda$2G4type" to i8*), i64 24)
  %_260046 = bitcast i8* %_20050 to { i8*, i8*, i8* }*
  %_260047 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_260046, i32 0, i32 2
  %_260004 = bitcast i8** %_260047 to i8*
  %_260048 = bitcast i8* %_260004 to i8**
  store i8* %_20006, i8** %_260048
  %_260049 = bitcast i8* %_20050 to { i8*, i8*, i8* }*
  %_260050 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_260049, i32 0, i32 1
  %_260006 = bitcast i8** %_260050 to i8*
  %_260051 = bitcast i8* %_260006 to i8**
  store i8* %_1, i8** %_260051
  call nonnull dereferenceable(8) i8* @"_SM12scala.OptionD7foreachL15scala.Function1uEO"(i8* dereferenceable_or_null(8) %_20048, i8* nonnull dereferenceable(24) %_20050)
  %_20054 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D20getUserHomeDirectoryL12scala.OptionEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_1)
  %_20056 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.System$$$Lambda$3G4type" to i8*), i64 24)
  %_260052 = bitcast i8* %_20056 to { i8*, i8*, i8* }*
  %_260053 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_260052, i32 0, i32 2
  %_260009 = bitcast i8** %_260053 to i8*
  %_260054 = bitcast i8* %_260009 to i8**
  store i8* %_20006, i8** %_260054
  %_260055 = bitcast i8* %_20056 to { i8*, i8*, i8* }*
  %_260056 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_260055, i32 0, i32 1
  %_260011 = bitcast i8** %_260056 to i8*
  %_260057 = bitcast i8* %_260011 to i8**
  store i8* %_1, i8** %_260057
  call nonnull dereferenceable(8) i8* @"_SM12scala.OptionD7foreachL15scala.Function1uEO"(i8* dereferenceable_or_null(8) %_20054, i8* nonnull dereferenceable(24) %_20056)
  %_20060 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D14getUserCountryL12scala.OptionEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_1)
  %_20062 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.System$$$Lambda$4G4type" to i8*), i64 24)
  %_260058 = bitcast i8* %_20062 to { i8*, i8*, i8* }*
  %_260059 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_260058, i32 0, i32 2
  %_260014 = bitcast i8** %_260059 to i8*
  %_260060 = bitcast i8* %_260014 to i8**
  store i8* %_20006, i8** %_260060
  %_260061 = bitcast i8* %_20062 to { i8*, i8*, i8* }*
  %_260062 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_260061, i32 0, i32 1
  %_260016 = bitcast i8** %_260062 to i8*
  %_260063 = bitcast i8* %_260016 to i8**
  store i8* %_1, i8** %_260063
  call nonnull dereferenceable(8) i8* @"_SM12scala.OptionD7foreachL15scala.Function1uEO"(i8* dereferenceable_or_null(8) %_20060, i8* nonnull dereferenceable(24) %_20062)
  %_20066 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D15getUserLanguageL12scala.OptionEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_1)
  %_20068 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.System$$$Lambda$5G4type" to i8*), i64 24)
  %_260064 = bitcast i8* %_20068 to { i8*, i8*, i8* }*
  %_260065 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_260064, i32 0, i32 2
  %_260019 = bitcast i8** %_260065 to i8*
  %_260066 = bitcast i8* %_260019 to i8**
  store i8* %_20006, i8** %_260066
  %_260067 = bitcast i8* %_20068 to { i8*, i8*, i8* }*
  %_260068 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_260067, i32 0, i32 1
  %_260021 = bitcast i8** %_260068 to i8*
  %_260069 = bitcast i8* %_260021 to i8**
  store i8* %_1, i8** %_260069
  call nonnull dereferenceable(8) i8* @"_SM12scala.OptionD7foreachL15scala.Function1uEO"(i8* dereferenceable_or_null(8) %_20066, i8* nonnull dereferenceable(24) %_20068)
  br label %_170000.0
_170000.0:
  %_170005 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-231" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-233" to i8*))
  %_170010 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-235" to i8*), i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-237" to i8*))
  %_170013 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D5env$1L16java.lang.StringL12scala.OptionEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-239" to i8*))
  %_170015 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.System$$$Lambda$6G4type" to i8*), i64 16)
  %_260070 = bitcast i8* %_170015 to { i8*, i8* }*
  %_260071 = getelementptr { i8*, i8* }, { i8*, i8* }* %_260070, i32 0, i32 1
  %_260024 = bitcast i8** %_260071 to i8*
  %_260072 = bitcast i8* %_260024 to i8**
  store i8* %_1, i8** %_260072
  %_170017 = call dereferenceable_or_null(8) i8* @"_SM12scala.OptionD6orElseL15scala.Function0L12scala.OptionEO"(i8* dereferenceable_or_null(8) %_170013, i8* nonnull dereferenceable(16) %_170015)
  %_170019 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.System$$$Lambda$7G4type" to i8*), i64 16)
  %_260073 = bitcast i8* %_170019 to { i8*, i8* }*
  %_260074 = getelementptr { i8*, i8* }, { i8*, i8* }* %_260073, i32 0, i32 1
  %_260026 = bitcast i8** %_260074 to i8*
  %_260075 = bitcast i8* %_260026 to i8**
  store i8* %_1, i8** %_260075
  %_170021 = call dereferenceable_or_null(8) i8* @"_SM12scala.OptionD6orElseL15scala.Function0L12scala.OptionEO"(i8* dereferenceable_or_null(8) %_170017, i8* nonnull dereferenceable(16) %_170019)
  %_170023 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.System$$$Lambda$8G4type" to i8*), i64 16)
  %_260076 = bitcast i8* %_170023 to { i8*, i8* }*
  %_260077 = getelementptr { i8*, i8* }, { i8*, i8* }* %_260076, i32 0, i32 1
  %_260028 = bitcast i8** %_260077 to i8*
  %_260078 = bitcast i8* %_260028 to i8**
  store i8* %_1, i8** %_260078
  %_170025 = call dereferenceable_or_null(8) i8* @"_SM12scala.OptionD6orElseL15scala.Function0L12scala.OptionEO"(i8* dereferenceable_or_null(8) %_170021, i8* nonnull dereferenceable(16) %_170023)
  %_170027 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM27java.lang.System$$$Lambda$9G4type" to i8*), i64 16)
  %_260079 = bitcast i8* %_170027 to { i8*, i8* }*
  %_260080 = getelementptr { i8*, i8* }, { i8*, i8* }* %_260079, i32 0, i32 1
  %_260030 = bitcast i8** %_260080 to i8*
  %_260081 = bitcast i8* %_260030 to i8**
  store i8* %_1, i8** %_260081
  %_170029 = call dereferenceable_or_null(8) i8* @"_SM12scala.OptionD9getOrElseL15scala.Function0L16java.lang.ObjectEO"(i8* dereferenceable_or_null(8) %_170025, i8* nonnull dereferenceable(16) %_170027)
  %_260034 = icmp eq i8* %_170029, null
  br i1 %_260034, label %_260032.0, label %_260031.0
_260031.0:
  %_260082 = bitcast i8* %_170029 to i8**
  %_260035 = load i8*, i8** %_260082
  %_260036 = icmp eq i8* %_260035, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*)
  br i1 %_260036, label %_260032.0, label %_260033.0
_260032.0:
  %_170030 = bitcast i8* %_170029 to i8*
  %_170033 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* nonnull dereferenceable(24) %_20006, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-241" to i8*), i8* dereferenceable_or_null(32) %_170030)
  br label %_260000.0
_260000.0:
  ret i8* %_20006
_260033.0:
  %_260037 = phi i8* [%_170029, %_260031.0]
  %_260038 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), %_260031.0]
  %_260083 = bitcast i8* %_260037 to i8**
  %_260039 = load i8*, i8** %_260083
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_260039, i8* %_260038)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D15getUserLanguageL12scala.OptionEPT17java.lang.System$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  br label %_30000.0
_30000.0:
  %_30004 = call dereferenceable_or_null(32) i8* @"_SM17java.lang.System$D6getenvL16java.lang.StringL16java.lang.StringEO"(i8* dereferenceable_or_null(56) %_1, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-201" to i8*))
  %_60003 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM13scala.Option$D5applyL16java.lang.ObjectL12scala.OptionEO" to i8*) to i8* (i8*, i8*)*
  %_30005 = call dereferenceable_or_null(8) i8* %_60003(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM13scala.Option$G8instance" to i8*), i8* dereferenceable_or_null(32) %_30004)
  %_30007 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.lang.System$$$Lambda$12G4type" to i8*), i64 16)
  %_60004 = bitcast i8* %_30007 to { i8*, i8* }*
  %_60005 = getelementptr { i8*, i8* }, { i8*, i8* }* %_60004, i32 0, i32 1
  %_60002 = bitcast i8** %_60005 to i8*
  %_60006 = bitcast i8* %_60002 to i8**
  store i8* %_1, i8** %_60006
  %_60007 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM12scala.OptionD3mapL15scala.Function1L12scala.OptionEO" to i8*) to i8* (i8*, i8*)*
  %_30009 = call dereferenceable_or_null(8) i8* %_60007(i8* dereferenceable_or_null(8) %_30005, i8* nonnull dereferenceable(16) %_30007)
  br label %_60000.0
_60000.0:
  ret i8* %_30009
}

define i32 @"_SM17java.lang.System$D16identityHashCodeL16java.lang.ObjectiEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30004 = bitcast i8* %_2 to i8*
  %_30005 = ptrtoint i8* %_30004 to i64
  %_30006 = call i32 @"_SM15java.lang.Long$D8hashCodejiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM15java.lang.Long$G8instance" to i8*), i64 %_30005)
  ret i32 %_30006
}

define nonnull dereferenceable(8) i8* @"_SM17java.lang.System$D17$init$$$anonfun$1L28scala.scalanative.unsafe.PtrL28scala.scalanative.unsafe.PtruEPT17java.lang.System$"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  br label %_50000.0
_50000.0:
  %_50001 = call dereferenceable_or_null(56) i8* @"_SM17java.lang.System$G4load"()
  %_60002 = bitcast i8* %_50001 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_60003 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_60002, i32 0, i32 2
  %_60001 = bitcast i8** %_60003 to i8*
  %_60004 = bitcast i8* %_60001 to i8**
  %_50002 = load i8*, i8** %_60004, !dereferenceable_or_null !{i64 24}
  %_50004 = call dereferenceable_or_null(16) i8* @"_SM33scala.scalanative.unsafe.package$G4load"()
  %_50005 = call dereferenceable_or_null(32) i8* @"_SM33scala.scalanative.unsafe.package$D21fromCString$default$2L24java.nio.charset.CharsetEO"(i8* nonnull dereferenceable(16) %_50004)
  %_60005 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM33scala.scalanative.unsafe.package$D11fromCStringL28scala.scalanative.unsafe.PtrL24java.nio.charset.CharsetL16java.lang.StringEO" to i8*) to i8* (i8*, i8*, i8*)*
  %_50006 = call dereferenceable_or_null(32) i8* %_60005(i8* nonnull dereferenceable(16) %_50004, i8* dereferenceable_or_null(16) %_2, i8* dereferenceable_or_null(32) %_50005)
  %_50007 = call dereferenceable_or_null(32) i8* @"_SM33scala.scalanative.unsafe.package$D21fromCString$default$2L24java.nio.charset.CharsetEO"(i8* nonnull dereferenceable(16) %_50004)
  %_60006 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM33scala.scalanative.unsafe.package$D11fromCStringL28scala.scalanative.unsafe.PtrL24java.nio.charset.CharsetL16java.lang.StringEO" to i8*) to i8* (i8*, i8*, i8*)*
  %_50008 = call dereferenceable_or_null(32) i8* %_60006(i8* nonnull dereferenceable(16) %_50004, i8* dereferenceable_or_null(16) %_3, i8* dereferenceable_or_null(32) %_50007)
  %_50009 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* dereferenceable_or_null(24) %_50002, i8* dereferenceable_or_null(32) %_50006, i8* dereferenceable_or_null(32) %_50008)
  br label %_60000.0
_60000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D19getCurrentDirectoryL12scala.OptionEPT17java.lang.System$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20003 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1024)
  %_90003 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D16toUInt$extensioniL31scala.scalanative.unsigned.UIntEO" to i8*) to i8* (i8*, i32)*
  %_20004 = call dereferenceable_or_null(16) i8* %_90003(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_20003)
  br label %_30000.0
_30000.0:
  %_60001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_60002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_60001)
  %_30003 = call dereferenceable_or_null(24) i8* @"_SM32scala.scalanative.unsigned.UInt$G4load"()
  %_30004 = call dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.UInt$D10uint2ulongL31scala.scalanative.unsigned.UIntL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(24) %_30003, i8* dereferenceable_or_null(16) %_20004)
  %_30005 = call dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.ULongD7toULongL32scala.scalanative.unsigned.ULongEO"(i8* dereferenceable_or_null(16) %_30004)
  %_90004 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$timesL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_30006 = call dereferenceable_or_null(16) i8* %_90004(i8* dereferenceable_or_null(16) %_60002, i8* dereferenceable_or_null(16) %_30005)
  %_90005 = bitcast i8* bitcast (i64 (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D12unboxToULongL16java.lang.ObjectjEO" to i8*) to i64 (i8*, i8*)*
  %_30007 = call i64 %_90005(i8* null, i8* dereferenceable_or_null(16) %_30006)
  %_90006 = alloca i8, i64 %_30007, align 8
  %_30008 = bitcast i8* %_90006 to i8*
  %_30010 = call i8* @"memset"(i8* %_30008, i32 0, i64 %_30007)
  %_30014 = call dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.UInt$D10uint2ulongL31scala.scalanative.unsigned.UIntL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(24) %_30003, i8* dereferenceable_or_null(16) %_20004)
  %_90007 = bitcast i8* bitcast (i64 (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D12unboxToULongL16java.lang.ObjectjEO" to i8*) to i64 (i8*, i8*)*
  %_30015 = call i64 %_90007(i8* null, i8* dereferenceable_or_null(16) %_30014)
  %_30016 = call i8* @"getcwd"(i8* %_30008, i64 %_30015)
  %_90008 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D8boxToPtrR_L28scala.scalanative.unsafe.PtrEO" to i8*) to i8* (i8*, i8*)*
  %_30019 = call dereferenceable_or_null(16) i8* %_90008(i8* null, i8* %_30016)
  %_90009 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM13scala.Option$D5applyL16java.lang.ObjectL12scala.OptionEO" to i8*) to i8* (i8*, i8*)*
  %_30020 = call dereferenceable_or_null(8) i8* %_90009(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM13scala.Option$G8instance" to i8*), i8* dereferenceable_or_null(16) %_30019)
  %_30022 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.lang.System$$$Lambda$10G4type" to i8*), i64 16)
  %_90010 = bitcast i8* %_30022 to { i8*, i8* }*
  %_90011 = getelementptr { i8*, i8* }, { i8*, i8* }* %_90010, i32 0, i32 1
  %_90002 = bitcast i8** %_90011 to i8*
  %_90012 = bitcast i8* %_90002 to i8**
  store i8* %_1, i8** %_90012
  %_90013 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM12scala.OptionD3mapL15scala.Function1L12scala.OptionEO" to i8*) to i8* (i8*, i8*)*
  %_30024 = call dereferenceable_or_null(8) i8* %_90013(i8* dereferenceable_or_null(8) %_30020, i8* nonnull dereferenceable(16) %_30022)
  br label %_90000.0
_90000.0:
  ret i8* %_30024
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D20getUserHomeDirectoryL12scala.OptionEPT17java.lang.System$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  br label %_30000.0
_30000.0:
  %_610158 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_50003 = call dereferenceable_or_null(16) i8* %_610158(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_610159 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_90001 = call dereferenceable_or_null(16) i8* %_610159(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_610160 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.scalanative.unsafe.Tag$Ptr$D5applyL28scala.scalanative.unsafe.TagL32scala.scalanative.unsafe.Tag$PtrEO" to i8*) to i8* (i8*, i8*)*
  %_110001 = call dereferenceable_or_null(16) i8* %_610160(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM33scala.scalanative.unsafe.Tag$Ptr$G8instance" to i8*), i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$Byte$G8instance" to i8*))
  %_120003 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_120004 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* dereferenceable_or_null(16) %_50003)
  %_610004 = icmp eq i8* %_120004, null
  br i1 %_610004, label %_610002.0, label %_610001.0
_610001.0:
  %_610161 = bitcast i8* %_120004 to i8**
  %_610005 = load i8*, i8** %_610161
  %_610162 = bitcast i8* %_610005 to { i8*, i32, i32, i8* }*
  %_610163 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_610162, i32 0, i32 1
  %_610006 = bitcast i32* %_610163 to i8*
  %_610164 = bitcast i8* %_610006 to i32*
  %_610007 = load i32, i32* %_610164
  %_610008 = icmp sle i32 296, %_610007
  %_610009 = icmp sle i32 %_610007, 300
  %_610010 = and i1 %_610008, %_610009
  br i1 %_610010, label %_610002.0, label %_610003.0
_610002.0:
  %_120005 = bitcast i8* %_120004 to i8*
  %_120007 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$UInt$G8instance" to i8*))
  %_610013 = icmp eq i8* %_120007, null
  br i1 %_610013, label %_610012.0, label %_610011.0
_610011.0:
  %_610165 = bitcast i8* %_120007 to i8**
  %_610014 = load i8*, i8** %_610165
  %_610166 = bitcast i8* %_610014 to { i8*, i32, i32, i8* }*
  %_610167 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_610166, i32 0, i32 1
  %_610015 = bitcast i32* %_610167 to i8*
  %_610168 = bitcast i8* %_610015 to i32*
  %_610016 = load i32, i32* %_610168
  %_610017 = icmp sle i32 296, %_610016
  %_610018 = icmp sle i32 %_610016, 300
  %_610019 = and i1 %_610017, %_610018
  br i1 %_610019, label %_610012.0, label %_610003.0
_610012.0:
  %_120008 = bitcast i8* %_120007 to i8*
  %_120009 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.unsafe.Tag$UInt$G8instance" to i8*))
  %_610022 = icmp eq i8* %_120009, null
  br i1 %_610022, label %_610021.0, label %_610020.0
_610020.0:
  %_610169 = bitcast i8* %_120009 to i8**
  %_610023 = load i8*, i8** %_610169
  %_610170 = bitcast i8* %_610023 to { i8*, i32, i32, i8* }*
  %_610171 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_610170, i32 0, i32 1
  %_610024 = bitcast i32* %_610171 to i8*
  %_610172 = bitcast i8* %_610024 to i32*
  %_610025 = load i32, i32* %_610172
  %_610026 = icmp sle i32 296, %_610025
  %_610027 = icmp sle i32 %_610025, 300
  %_610028 = and i1 %_610026, %_610027
  br i1 %_610028, label %_610021.0, label %_610003.0
_610021.0:
  %_120010 = bitcast i8* %_120009 to i8*
  %_120011 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* dereferenceable_or_null(16) %_90001)
  %_610031 = icmp eq i8* %_120011, null
  br i1 %_610031, label %_610030.0, label %_610029.0
_610029.0:
  %_610173 = bitcast i8* %_120011 to i8**
  %_610032 = load i8*, i8** %_610173
  %_610174 = bitcast i8* %_610032 to { i8*, i32, i32, i8* }*
  %_610175 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_610174, i32 0, i32 1
  %_610033 = bitcast i32* %_610175 to i8*
  %_610176 = bitcast i8* %_610033 to i32*
  %_610034 = load i32, i32* %_610176
  %_610035 = icmp sle i32 296, %_610034
  %_610036 = icmp sle i32 %_610034, 300
  %_610037 = and i1 %_610035, %_610036
  br i1 %_610037, label %_610030.0, label %_610003.0
_610030.0:
  %_120012 = bitcast i8* %_120011 to i8*
  %_120013 = call dereferenceable_or_null(8) i8* @"_SM13scala.Predef$D10implicitlyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_120003, i8* dereferenceable_or_null(16) %_110001)
  %_610040 = icmp eq i8* %_120013, null
  br i1 %_610040, label %_610039.0, label %_610038.0
_610038.0:
  %_610177 = bitcast i8* %_120013 to i8**
  %_610041 = load i8*, i8** %_610177
  %_610178 = bitcast i8* %_610041 to { i8*, i32, i32, i8* }*
  %_610179 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_610178, i32 0, i32 1
  %_610042 = bitcast i32* %_610179 to i8*
  %_610180 = bitcast i8* %_610042 to i32*
  %_610043 = load i32, i32* %_610180
  %_610044 = icmp sle i32 296, %_610043
  %_610045 = icmp sle i32 %_610043, 300
  %_610046 = and i1 %_610044, %_610045
  br i1 %_610046, label %_610039.0, label %_610003.0
_610039.0:
  %_120014 = bitcast i8* %_120013 to i8*
  %_610181 = bitcast i8* bitcast (i8* (i8*, i8*, i8*, i8*, i8*, i8*)* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$D5applyL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL37scala.scalanative.unsafe.Tag$CStruct5EO" to i8*) to i8* (i8*, i8*, i8*, i8*, i8*, i8*)*
  %_120015 = call dereferenceable_or_null(48) i8* %_610181(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$G8instance" to i8*), i8* dereferenceable_or_null(8) %_120005, i8* dereferenceable_or_null(8) %_120008, i8* dereferenceable_or_null(8) %_120010, i8* dereferenceable_or_null(8) %_120012, i8* dereferenceable_or_null(8) %_120014)
  %_140004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_140005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_140004)
  %_610049 = icmp ne i8* %_120015, null
  br i1 %_610049, label %_610047.0, label %_610048.0
_610047.0:
  %_610182 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610183 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610182, i32 0, i32 5
  %_610050 = bitcast i8** %_610183 to i8*
  %_610184 = bitcast i8* %_610050 to i8**
  %_150001 = load i8*, i8** %_610184, !dereferenceable_or_null !{i64 8}
  %_610052 = icmp ne i8* %_150001, null
  br i1 %_610052, label %_610051.0, label %_610048.0
_610051.0:
  %_610185 = bitcast i8* %_150001 to i8**
  %_610053 = load i8*, i8** %_610185
  %_610186 = bitcast i8* %_610053 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610187 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610186, i32 0, i32 4, i32 4
  %_610054 = bitcast i8** %_610187 to i8*
  %_610188 = bitcast i8* %_610054 to i8**
  %_140008 = load i8*, i8** %_610188
  %_610189 = bitcast i8* %_140008 to i8* (i8*)*
  %_140009 = call dereferenceable_or_null(16) i8* %_610189(i8* dereferenceable_or_null(8) %_150001)
  %_160001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_160002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_160001)
  %_610190 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_160003 = call dereferenceable_or_null(16) i8* %_610190(i8* dereferenceable_or_null(16) %_140009, i8* dereferenceable_or_null(16) %_160002)
  %_160004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_160005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_160004)
  %_610191 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_160006 = call dereferenceable_or_null(16) i8* %_610191(i8* dereferenceable_or_null(16) %_140005, i8* dereferenceable_or_null(16) %_160003)
  %_160007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_160006, i8* dereferenceable_or_null(16) %_160005)
  br i1 %_160007, label %_170000.0, label %_180000.0
_170000.0:
  br label %_190000.0
_180000.0:
  %_610192 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_180001 = call dereferenceable_or_null(16) i8* %_610192(i8* dereferenceable_or_null(16) %_140005, i8* dereferenceable_or_null(16) %_160003)
  %_610193 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_180002 = call dereferenceable_or_null(16) i8* %_610193(i8* dereferenceable_or_null(16) %_140009, i8* dereferenceable_or_null(16) %_180001)
  br label %_190000.0
_190000.0:
  %_190001 = phi i8* [%_180002, %_180000.0], [%_160005, %_170000.0]
  %_610194 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_190002 = call dereferenceable_or_null(16) i8* %_610194(i8* dereferenceable_or_null(16) %_140005, i8* dereferenceable_or_null(16) %_190001)
  %_610056 = icmp ne i8* %_120015, null
  br i1 %_610056, label %_610055.0, label %_610048.0
_610055.0:
  %_610195 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610196 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610195, i32 0, i32 5
  %_610057 = bitcast i8** %_610196 to i8*
  %_610197 = bitcast i8* %_610057 to i8**
  %_200001 = load i8*, i8** %_610197, !dereferenceable_or_null !{i64 8}
  %_610059 = icmp ne i8* %_200001, null
  br i1 %_610059, label %_610058.0, label %_610048.0
_610058.0:
  %_610198 = bitcast i8* %_200001 to i8**
  %_610060 = load i8*, i8** %_610198
  %_610199 = bitcast i8* %_610060 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610200 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610199, i32 0, i32 4, i32 6
  %_610061 = bitcast i8** %_610200 to i8*
  %_610201 = bitcast i8* %_610061 to i8**
  %_140011 = load i8*, i8** %_610201
  %_610202 = bitcast i8* %_140011 to i8* (i8*)*
  %_140012 = call dereferenceable_or_null(16) i8* %_610202(i8* dereferenceable_or_null(8) %_200001)
  %_610203 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_140013 = call dereferenceable_or_null(16) i8* %_610203(i8* dereferenceable_or_null(16) %_190002, i8* dereferenceable_or_null(16) %_140012)
  %_610063 = icmp ne i8* %_120015, null
  br i1 %_610063, label %_610062.0, label %_610048.0
_610062.0:
  %_610204 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610205 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610204, i32 0, i32 4
  %_610064 = bitcast i8** %_610205 to i8*
  %_610206 = bitcast i8* %_610064 to i8**
  %_210001 = load i8*, i8** %_610206, !dereferenceable_or_null !{i64 8}
  %_610066 = icmp ne i8* %_210001, null
  br i1 %_610066, label %_610065.0, label %_610048.0
_610065.0:
  %_610207 = bitcast i8* %_210001 to i8**
  %_610067 = load i8*, i8** %_610207
  %_610208 = bitcast i8* %_610067 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610209 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610208, i32 0, i32 4, i32 4
  %_610068 = bitcast i8** %_610209 to i8*
  %_610210 = bitcast i8* %_610068 to i8**
  %_140015 = load i8*, i8** %_610210
  %_610211 = bitcast i8* %_140015 to i8* (i8*)*
  %_140016 = call dereferenceable_or_null(16) i8* %_610211(i8* dereferenceable_or_null(8) %_210001)
  %_220001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_220002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_220001)
  %_610212 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_220003 = call dereferenceable_or_null(16) i8* %_610212(i8* dereferenceable_or_null(16) %_140016, i8* dereferenceable_or_null(16) %_220002)
  %_220004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_220005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_220004)
  %_610213 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_220006 = call dereferenceable_or_null(16) i8* %_610213(i8* dereferenceable_or_null(16) %_140013, i8* dereferenceable_or_null(16) %_220003)
  %_220007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_220006, i8* dereferenceable_or_null(16) %_220005)
  br i1 %_220007, label %_230000.0, label %_240000.0
_230000.0:
  br label %_250000.0
_240000.0:
  %_610214 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_240001 = call dereferenceable_or_null(16) i8* %_610214(i8* dereferenceable_or_null(16) %_140013, i8* dereferenceable_or_null(16) %_220003)
  %_610215 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_240002 = call dereferenceable_or_null(16) i8* %_610215(i8* dereferenceable_or_null(16) %_140016, i8* dereferenceable_or_null(16) %_240001)
  br label %_250000.0
_250000.0:
  %_250001 = phi i8* [%_240002, %_240000.0], [%_220005, %_230000.0]
  %_610216 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_250002 = call dereferenceable_or_null(16) i8* %_610216(i8* dereferenceable_or_null(16) %_140013, i8* dereferenceable_or_null(16) %_250001)
  %_610070 = icmp ne i8* %_120015, null
  br i1 %_610070, label %_610069.0, label %_610048.0
_610069.0:
  %_610217 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610218 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610217, i32 0, i32 4
  %_610071 = bitcast i8** %_610218 to i8*
  %_610219 = bitcast i8* %_610071 to i8**
  %_260001 = load i8*, i8** %_610219, !dereferenceable_or_null !{i64 8}
  %_610073 = icmp ne i8* %_260001, null
  br i1 %_610073, label %_610072.0, label %_610048.0
_610072.0:
  %_610220 = bitcast i8* %_260001 to i8**
  %_610074 = load i8*, i8** %_610220
  %_610221 = bitcast i8* %_610074 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610222 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610221, i32 0, i32 4, i32 6
  %_610075 = bitcast i8** %_610222 to i8*
  %_610223 = bitcast i8* %_610075 to i8**
  %_140018 = load i8*, i8** %_610223
  %_610224 = bitcast i8* %_140018 to i8* (i8*)*
  %_140019 = call dereferenceable_or_null(16) i8* %_610224(i8* dereferenceable_or_null(8) %_260001)
  %_610225 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_140020 = call dereferenceable_or_null(16) i8* %_610225(i8* dereferenceable_or_null(16) %_250002, i8* dereferenceable_or_null(16) %_140019)
  %_610077 = icmp ne i8* %_120015, null
  br i1 %_610077, label %_610076.0, label %_610048.0
_610076.0:
  %_610226 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610227 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610226, i32 0, i32 3
  %_610078 = bitcast i8** %_610227 to i8*
  %_610228 = bitcast i8* %_610078 to i8**
  %_270001 = load i8*, i8** %_610228, !dereferenceable_or_null !{i64 8}
  %_610080 = icmp ne i8* %_270001, null
  br i1 %_610080, label %_610079.0, label %_610048.0
_610079.0:
  %_610229 = bitcast i8* %_270001 to i8**
  %_610081 = load i8*, i8** %_610229
  %_610230 = bitcast i8* %_610081 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610231 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610230, i32 0, i32 4, i32 4
  %_610082 = bitcast i8** %_610231 to i8*
  %_610232 = bitcast i8* %_610082 to i8**
  %_140022 = load i8*, i8** %_610232
  %_610233 = bitcast i8* %_140022 to i8* (i8*)*
  %_140023 = call dereferenceable_or_null(16) i8* %_610233(i8* dereferenceable_or_null(8) %_270001)
  %_280001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_280002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_280001)
  %_610234 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_280003 = call dereferenceable_or_null(16) i8* %_610234(i8* dereferenceable_or_null(16) %_140023, i8* dereferenceable_or_null(16) %_280002)
  %_280004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_280005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_280004)
  %_610235 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_280006 = call dereferenceable_or_null(16) i8* %_610235(i8* dereferenceable_or_null(16) %_140020, i8* dereferenceable_or_null(16) %_280003)
  %_280007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_280006, i8* dereferenceable_or_null(16) %_280005)
  br i1 %_280007, label %_290000.0, label %_300000.0
_290000.0:
  br label %_310000.0
_300000.0:
  %_610236 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_300001 = call dereferenceable_or_null(16) i8* %_610236(i8* dereferenceable_or_null(16) %_140020, i8* dereferenceable_or_null(16) %_280003)
  %_610237 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_300002 = call dereferenceable_or_null(16) i8* %_610237(i8* dereferenceable_or_null(16) %_140023, i8* dereferenceable_or_null(16) %_300001)
  br label %_310000.0
_310000.0:
  %_310001 = phi i8* [%_300002, %_300000.0], [%_280005, %_290000.0]
  %_610238 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_310002 = call dereferenceable_or_null(16) i8* %_610238(i8* dereferenceable_or_null(16) %_140020, i8* dereferenceable_or_null(16) %_310001)
  %_610084 = icmp ne i8* %_120015, null
  br i1 %_610084, label %_610083.0, label %_610048.0
_610083.0:
  %_610239 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610240 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610239, i32 0, i32 3
  %_610085 = bitcast i8** %_610240 to i8*
  %_610241 = bitcast i8* %_610085 to i8**
  %_320001 = load i8*, i8** %_610241, !dereferenceable_or_null !{i64 8}
  %_610087 = icmp ne i8* %_320001, null
  br i1 %_610087, label %_610086.0, label %_610048.0
_610086.0:
  %_610242 = bitcast i8* %_320001 to i8**
  %_610088 = load i8*, i8** %_610242
  %_610243 = bitcast i8* %_610088 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610244 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610243, i32 0, i32 4, i32 6
  %_610089 = bitcast i8** %_610244 to i8*
  %_610245 = bitcast i8* %_610089 to i8**
  %_140025 = load i8*, i8** %_610245
  %_610246 = bitcast i8* %_140025 to i8* (i8*)*
  %_140026 = call dereferenceable_or_null(16) i8* %_610246(i8* dereferenceable_or_null(8) %_320001)
  %_610247 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_140027 = call dereferenceable_or_null(16) i8* %_610247(i8* dereferenceable_or_null(16) %_310002, i8* dereferenceable_or_null(16) %_140026)
  %_610091 = icmp ne i8* %_120015, null
  br i1 %_610091, label %_610090.0, label %_610048.0
_610090.0:
  %_610248 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610249 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610248, i32 0, i32 2
  %_610092 = bitcast i8** %_610249 to i8*
  %_610250 = bitcast i8* %_610092 to i8**
  %_330001 = load i8*, i8** %_610250, !dereferenceable_or_null !{i64 8}
  %_610094 = icmp ne i8* %_330001, null
  br i1 %_610094, label %_610093.0, label %_610048.0
_610093.0:
  %_610251 = bitcast i8* %_330001 to i8**
  %_610095 = load i8*, i8** %_610251
  %_610252 = bitcast i8* %_610095 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610253 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610252, i32 0, i32 4, i32 4
  %_610096 = bitcast i8** %_610253 to i8*
  %_610254 = bitcast i8* %_610096 to i8**
  %_140029 = load i8*, i8** %_610254
  %_610255 = bitcast i8* %_140029 to i8* (i8*)*
  %_140030 = call dereferenceable_or_null(16) i8* %_610255(i8* dereferenceable_or_null(8) %_330001)
  %_340001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_340002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_340001)
  %_610256 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_340003 = call dereferenceable_or_null(16) i8* %_610256(i8* dereferenceable_or_null(16) %_140030, i8* dereferenceable_or_null(16) %_340002)
  %_340004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_340005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_340004)
  %_610257 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_340006 = call dereferenceable_or_null(16) i8* %_610257(i8* dereferenceable_or_null(16) %_140027, i8* dereferenceable_or_null(16) %_340003)
  %_340007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_340006, i8* dereferenceable_or_null(16) %_340005)
  br i1 %_340007, label %_350000.0, label %_360000.0
_350000.0:
  br label %_370000.0
_360000.0:
  %_610258 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_360001 = call dereferenceable_or_null(16) i8* %_610258(i8* dereferenceable_or_null(16) %_140027, i8* dereferenceable_or_null(16) %_340003)
  %_610259 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_360002 = call dereferenceable_or_null(16) i8* %_610259(i8* dereferenceable_or_null(16) %_140030, i8* dereferenceable_or_null(16) %_360001)
  br label %_370000.0
_370000.0:
  %_370001 = phi i8* [%_360002, %_360000.0], [%_340005, %_350000.0]
  %_610260 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_370002 = call dereferenceable_or_null(16) i8* %_610260(i8* dereferenceable_or_null(16) %_140027, i8* dereferenceable_or_null(16) %_370001)
  %_610098 = icmp ne i8* %_120015, null
  br i1 %_610098, label %_610097.0, label %_610048.0
_610097.0:
  %_610261 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610262 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610261, i32 0, i32 2
  %_610099 = bitcast i8** %_610262 to i8*
  %_610263 = bitcast i8* %_610099 to i8**
  %_380001 = load i8*, i8** %_610263, !dereferenceable_or_null !{i64 8}
  %_610101 = icmp ne i8* %_380001, null
  br i1 %_610101, label %_610100.0, label %_610048.0
_610100.0:
  %_610264 = bitcast i8* %_380001 to i8**
  %_610102 = load i8*, i8** %_610264
  %_610265 = bitcast i8* %_610102 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610266 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610265, i32 0, i32 4, i32 6
  %_610103 = bitcast i8** %_610266 to i8*
  %_610267 = bitcast i8* %_610103 to i8**
  %_140032 = load i8*, i8** %_610267
  %_610268 = bitcast i8* %_140032 to i8* (i8*)*
  %_140033 = call dereferenceable_or_null(16) i8* %_610268(i8* dereferenceable_or_null(8) %_380001)
  %_610269 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_140034 = call dereferenceable_or_null(16) i8* %_610269(i8* dereferenceable_or_null(16) %_370002, i8* dereferenceable_or_null(16) %_140033)
  %_610105 = icmp ne i8* %_120015, null
  br i1 %_610105, label %_610104.0, label %_610048.0
_610104.0:
  %_610270 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610271 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610270, i32 0, i32 1
  %_610106 = bitcast i8** %_610271 to i8*
  %_610272 = bitcast i8* %_610106 to i8**
  %_390001 = load i8*, i8** %_610272, !dereferenceable_or_null !{i64 8}
  %_610108 = icmp ne i8* %_390001, null
  br i1 %_610108, label %_610107.0, label %_610048.0
_610107.0:
  %_610273 = bitcast i8* %_390001 to i8**
  %_610109 = load i8*, i8** %_610273
  %_610274 = bitcast i8* %_610109 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610275 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610274, i32 0, i32 4, i32 4
  %_610110 = bitcast i8** %_610275 to i8*
  %_610276 = bitcast i8* %_610110 to i8**
  %_140036 = load i8*, i8** %_610276
  %_610277 = bitcast i8* %_140036 to i8* (i8*)*
  %_140037 = call dereferenceable_or_null(16) i8* %_610277(i8* dereferenceable_or_null(8) %_390001)
  %_400001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_400002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_400001)
  %_610278 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_400003 = call dereferenceable_or_null(16) i8* %_610278(i8* dereferenceable_or_null(16) %_140037, i8* dereferenceable_or_null(16) %_400002)
  %_400004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_400005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_400004)
  %_610279 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_400006 = call dereferenceable_or_null(16) i8* %_610279(i8* dereferenceable_or_null(16) %_140034, i8* dereferenceable_or_null(16) %_400003)
  %_400007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_400006, i8* dereferenceable_or_null(16) %_400005)
  br i1 %_400007, label %_410000.0, label %_420000.0
_410000.0:
  br label %_430000.0
_420000.0:
  %_610280 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_420001 = call dereferenceable_or_null(16) i8* %_610280(i8* dereferenceable_or_null(16) %_140034, i8* dereferenceable_or_null(16) %_400003)
  %_610281 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_420002 = call dereferenceable_or_null(16) i8* %_610281(i8* dereferenceable_or_null(16) %_140037, i8* dereferenceable_or_null(16) %_420001)
  br label %_430000.0
_430000.0:
  %_430001 = phi i8* [%_420002, %_420000.0], [%_400005, %_410000.0]
  %_610282 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_430002 = call dereferenceable_or_null(16) i8* %_610282(i8* dereferenceable_or_null(16) %_140034, i8* dereferenceable_or_null(16) %_430001)
  %_610112 = icmp ne i8* %_120015, null
  br i1 %_610112, label %_610111.0, label %_610048.0
_610111.0:
  %_610283 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610284 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610283, i32 0, i32 1
  %_610113 = bitcast i8** %_610284 to i8*
  %_610285 = bitcast i8* %_610113 to i8**
  %_440001 = load i8*, i8** %_610285, !dereferenceable_or_null !{i64 8}
  %_610115 = icmp ne i8* %_440001, null
  br i1 %_610115, label %_610114.0, label %_610048.0
_610114.0:
  %_610286 = bitcast i8* %_440001 to i8**
  %_610116 = load i8*, i8** %_610286
  %_610287 = bitcast i8* %_610116 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610288 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610287, i32 0, i32 4, i32 6
  %_610117 = bitcast i8** %_610288 to i8*
  %_610289 = bitcast i8* %_610117 to i8**
  %_140039 = load i8*, i8** %_610289
  %_610290 = bitcast i8* %_140039 to i8* (i8*)*
  %_140040 = call dereferenceable_or_null(16) i8* %_610290(i8* dereferenceable_or_null(8) %_440001)
  %_610291 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_140041 = call dereferenceable_or_null(16) i8* %_610291(i8* dereferenceable_or_null(16) %_430002, i8* dereferenceable_or_null(16) %_140040)
  %_450002 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_450003 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_450002)
  %_610119 = icmp ne i8* %_120015, null
  br i1 %_610119, label %_610118.0, label %_610048.0
_610118.0:
  %_610292 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610293 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610292, i32 0, i32 5
  %_610120 = bitcast i8** %_610293 to i8*
  %_610294 = bitcast i8* %_610120 to i8**
  %_460001 = load i8*, i8** %_610294, !dereferenceable_or_null !{i64 8}
  %_610122 = icmp ne i8* %_460001, null
  br i1 %_610122, label %_610121.0, label %_610048.0
_610121.0:
  %_610295 = bitcast i8* %_460001 to i8**
  %_610123 = load i8*, i8** %_610295
  %_610296 = bitcast i8* %_610123 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610297 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610296, i32 0, i32 4, i32 4
  %_610124 = bitcast i8** %_610297 to i8*
  %_610298 = bitcast i8* %_610124 to i8**
  %_450005 = load i8*, i8** %_610298
  %_610299 = bitcast i8* %_450005 to i8* (i8*)*
  %_450006 = call dereferenceable_or_null(16) i8* %_610299(i8* dereferenceable_or_null(8) %_460001)
  %_450007 = call dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.ULongD3maxL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8* dereferenceable_or_null(16) %_450003, i8* dereferenceable_or_null(16) %_450006)
  %_610126 = icmp ne i8* %_120015, null
  br i1 %_610126, label %_610125.0, label %_610048.0
_610125.0:
  %_610300 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610301 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610300, i32 0, i32 4
  %_610127 = bitcast i8** %_610301 to i8*
  %_610302 = bitcast i8* %_610127 to i8**
  %_470001 = load i8*, i8** %_610302, !dereferenceable_or_null !{i64 8}
  %_610129 = icmp ne i8* %_470001, null
  br i1 %_610129, label %_610128.0, label %_610048.0
_610128.0:
  %_610303 = bitcast i8* %_470001 to i8**
  %_610130 = load i8*, i8** %_610303
  %_610304 = bitcast i8* %_610130 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610305 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610304, i32 0, i32 4, i32 4
  %_610131 = bitcast i8** %_610305 to i8*
  %_610306 = bitcast i8* %_610131 to i8**
  %_450009 = load i8*, i8** %_610306
  %_610307 = bitcast i8* %_450009 to i8* (i8*)*
  %_450010 = call dereferenceable_or_null(16) i8* %_610307(i8* dereferenceable_or_null(8) %_470001)
  %_450011 = call dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.ULongD3maxL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8* dereferenceable_or_null(16) %_450007, i8* dereferenceable_or_null(16) %_450010)
  %_610133 = icmp ne i8* %_120015, null
  br i1 %_610133, label %_610132.0, label %_610048.0
_610132.0:
  %_610308 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610309 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610308, i32 0, i32 3
  %_610134 = bitcast i8** %_610309 to i8*
  %_610310 = bitcast i8* %_610134 to i8**
  %_480001 = load i8*, i8** %_610310, !dereferenceable_or_null !{i64 8}
  %_610136 = icmp ne i8* %_480001, null
  br i1 %_610136, label %_610135.0, label %_610048.0
_610135.0:
  %_610311 = bitcast i8* %_480001 to i8**
  %_610137 = load i8*, i8** %_610311
  %_610312 = bitcast i8* %_610137 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610313 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610312, i32 0, i32 4, i32 4
  %_610138 = bitcast i8** %_610313 to i8*
  %_610314 = bitcast i8* %_610138 to i8**
  %_450013 = load i8*, i8** %_610314
  %_610315 = bitcast i8* %_450013 to i8* (i8*)*
  %_450014 = call dereferenceable_or_null(16) i8* %_610315(i8* dereferenceable_or_null(8) %_480001)
  %_450015 = call dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.ULongD3maxL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8* dereferenceable_or_null(16) %_450011, i8* dereferenceable_or_null(16) %_450014)
  %_610140 = icmp ne i8* %_120015, null
  br i1 %_610140, label %_610139.0, label %_610048.0
_610139.0:
  %_610316 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610317 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610316, i32 0, i32 2
  %_610141 = bitcast i8** %_610317 to i8*
  %_610318 = bitcast i8* %_610141 to i8**
  %_490001 = load i8*, i8** %_610318, !dereferenceable_or_null !{i64 8}
  %_610143 = icmp ne i8* %_490001, null
  br i1 %_610143, label %_610142.0, label %_610048.0
_610142.0:
  %_610319 = bitcast i8* %_490001 to i8**
  %_610144 = load i8*, i8** %_610319
  %_610320 = bitcast i8* %_610144 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610321 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610320, i32 0, i32 4, i32 4
  %_610145 = bitcast i8** %_610321 to i8*
  %_610322 = bitcast i8* %_610145 to i8**
  %_450017 = load i8*, i8** %_610322
  %_610323 = bitcast i8* %_450017 to i8* (i8*)*
  %_450018 = call dereferenceable_or_null(16) i8* %_610323(i8* dereferenceable_or_null(8) %_490001)
  %_450019 = call dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.ULongD3maxL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8* dereferenceable_or_null(16) %_450015, i8* dereferenceable_or_null(16) %_450018)
  %_610147 = icmp ne i8* %_120015, null
  br i1 %_610147, label %_610146.0, label %_610048.0
_610146.0:
  %_610324 = bitcast i8* %_120015 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_610325 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_610324, i32 0, i32 1
  %_610148 = bitcast i8** %_610325 to i8*
  %_610326 = bitcast i8* %_610148 to i8**
  %_500001 = load i8*, i8** %_610326, !dereferenceable_or_null !{i64 8}
  %_610150 = icmp ne i8* %_500001, null
  br i1 %_610150, label %_610149.0, label %_610048.0
_610149.0:
  %_610327 = bitcast i8* %_500001 to i8**
  %_610151 = load i8*, i8** %_610327
  %_610328 = bitcast i8* %_610151 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }*
  %_610329 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* %_610328, i32 0, i32 4, i32 4
  %_610152 = bitcast i8** %_610329 to i8*
  %_610330 = bitcast i8* %_610152 to i8**
  %_450021 = load i8*, i8** %_610330
  %_610331 = bitcast i8* %_450021 to i8* (i8*)*
  %_450022 = call dereferenceable_or_null(16) i8* %_610331(i8* dereferenceable_or_null(8) %_500001)
  %_450023 = call dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.ULongD3maxL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO"(i8* dereferenceable_or_null(16) %_450019, i8* dereferenceable_or_null(16) %_450022)
  %_510001 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_510002 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_510001)
  %_610332 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_510003 = call dereferenceable_or_null(16) i8* %_610332(i8* dereferenceable_or_null(16) %_450023, i8* dereferenceable_or_null(16) %_510002)
  %_510004 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 0)
  %_510005 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_510004)
  %_610333 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_510006 = call dereferenceable_or_null(16) i8* %_610333(i8* dereferenceable_or_null(16) %_140041, i8* dereferenceable_or_null(16) %_510003)
  %_510007 = call i1 @"_SM32scala.scalanative.unsigned.ULongD6$eq$eqL32scala.scalanative.unsigned.ULongzEO"(i8* dereferenceable_or_null(16) %_510006, i8* dereferenceable_or_null(16) %_510005)
  br i1 %_510007, label %_520000.0, label %_530000.0
_520000.0:
  br label %_540000.0
_530000.0:
  %_610334 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD4$ampL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_530001 = call dereferenceable_or_null(16) i8* %_610334(i8* dereferenceable_or_null(16) %_140041, i8* dereferenceable_or_null(16) %_510003)
  %_610335 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$minusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_530002 = call dereferenceable_or_null(16) i8* %_610335(i8* dereferenceable_or_null(16) %_450023, i8* dereferenceable_or_null(16) %_530001)
  br label %_540000.0
_540000.0:
  %_540001 = phi i8* [%_530002, %_530000.0], [%_510005, %_520000.0]
  %_610336 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD5$plusL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_540002 = call dereferenceable_or_null(16) i8* %_610336(i8* dereferenceable_or_null(16) %_140041, i8* dereferenceable_or_null(16) %_540001)
  %_30003 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_30004 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_30003)
  %_30005 = call dereferenceable_or_null(16) i8* @"_SM32scala.scalanative.unsigned.ULongD7toULongL32scala.scalanative.unsigned.ULongEO"(i8* dereferenceable_or_null(16) %_30004)
  %_610337 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.unsigned.ULongD6$timesL32scala.scalanative.unsigned.ULongL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i8*)*
  %_30006 = call dereferenceable_or_null(16) i8* %_610337(i8* dereferenceable_or_null(16) %_540002, i8* dereferenceable_or_null(16) %_30005)
  %_610338 = bitcast i8* bitcast (i64 (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D12unboxToULongL16java.lang.ObjectjEO" to i8*) to i64 (i8*, i8*)*
  %_30007 = call i64 %_610338(i8* null, i8* dereferenceable_or_null(16) %_30006)
  %_610339 = alloca i8, i64 %_30007, align 8
  %_30008 = bitcast i8* %_610339 to i8*
  %_30010 = call i8* @"memset"(i8* %_30008, i32 0, i64 %_30007)
  %_30014 = call i32 @"getuid"()
  %_30017 = call i32 @"scalanative_getpwuid"(i32 %_30014, i8* %_30008)
  %_30019 = icmp eq i32 %_30017, 0
  br i1 %_30019, label %_550000.0, label %_560000.0
_550000.0:
  %_610340 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D8boxToPtrR_L28scala.scalanative.unsafe.PtrEO" to i8*) to i8* (i8*, i8*)*
  %_550003 = call dereferenceable_or_null(16) i8* %_610340(i8* null, i8* %_30008)
  %_550004 = call dereferenceable_or_null(16) i8* @"_SM31scala.scalanative.posix.pwdOps$D9passwdOpsL28scala.scalanative.unsafe.PtrL28scala.scalanative.unsafe.PtrEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM31scala.scalanative.posix.pwdOps$G8instance" to i8*), i8* dereferenceable_or_null(16) %_550003)
  %_550005 = call dereferenceable_or_null(16) i8* @"_SM41scala.scalanative.posix.pwdOps$passwdOps$D16pw_dir$extensionL28scala.scalanative.unsafe.PtrL28scala.scalanative.unsafe.PtrEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM41scala.scalanative.posix.pwdOps$passwdOps$G8instance" to i8*), i8* dereferenceable_or_null(16) %_550004)
  %_550008 = icmp eq i8* %_550005, null
  %_550009 = xor i1 %_550008, true
  br label %_570000.0
_560000.0:
  %_610341 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D8boxToPtrR_L28scala.scalanative.unsafe.PtrEO" to i8*) to i8* (i8*, i8*)*
  %_560001 = call dereferenceable_or_null(16) i8* %_610341(i8* null, i8* %_30008)
  br label %_570000.0
_570000.0:
  %_570001 = phi i8* [%_560001, %_560000.0], [%_550003, %_550000.0]
  %_570002 = phi i1 [false, %_560000.0], [%_550009, %_550000.0]
  br i1 %_570002, label %_580000.0, label %_590000.0
_580000.0:
  %_580004 = call dereferenceable_or_null(16) i8* @"_SM31scala.scalanative.posix.pwdOps$D9passwdOpsL28scala.scalanative.unsafe.PtrL28scala.scalanative.unsafe.PtrEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM31scala.scalanative.posix.pwdOps$G8instance" to i8*), i8* dereferenceable_or_null(16) %_570001)
  %_580005 = call dereferenceable_or_null(16) i8* @"_SM41scala.scalanative.posix.pwdOps$passwdOps$D16pw_dir$extensionL28scala.scalanative.unsafe.PtrL28scala.scalanative.unsafe.PtrEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM41scala.scalanative.posix.pwdOps$passwdOps$G8instance" to i8*), i8* dereferenceable_or_null(16) %_580004)
  %_580006 = call dereferenceable_or_null(16) i8* @"_SM33scala.scalanative.unsafe.package$G4load"()
  %_580007 = call dereferenceable_or_null(32) i8* @"_SM33scala.scalanative.unsafe.package$D21fromCString$default$2L24java.nio.charset.CharsetEO"(i8* nonnull dereferenceable(16) %_580006)
  %_610342 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM33scala.scalanative.unsafe.package$D11fromCStringL28scala.scalanative.unsafe.PtrL24java.nio.charset.CharsetL16java.lang.StringEO" to i8*) to i8* (i8*, i8*, i8*)*
  %_580008 = call dereferenceable_or_null(32) i8* %_610342(i8* nonnull dereferenceable(16) %_580006, i8* dereferenceable_or_null(16) %_580005, i8* dereferenceable_or_null(32) %_580007)
  %_610343 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM11scala.Some$D5applyL16java.lang.ObjectL10scala.SomeEO" to i8*) to i8* (i8*, i8*)*
  %_580009 = call dereferenceable_or_null(16) i8* %_610343(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM11scala.Some$G8instance" to i8*), i8* dereferenceable_or_null(32) %_580008)
  br label %_600000.0
_590000.0:
  %_590001 = call dereferenceable_or_null(8) i8* @"_SM11scala.None$G4load"()
  %_590002 = call dereferenceable_or_null(16) i8* @"_SM33scala.scalanative.unsafe.package$G4load"()
  br label %_600000.0
_600000.0:
  %_600001 = phi i8* [%_590002, %_590000.0], [%_580006, %_580000.0]
  %_600002 = phi i8* [%_590001, %_590000.0], [%_580009, %_580000.0]
  br label %_610000.0
_610000.0:
  ret i8* %_600002
_610048.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_610003.0:
  %_610154 = phi i8* [%_120004, %_610001.0], [%_120007, %_610011.0], [%_120009, %_610020.0], [%_120011, %_610029.0], [%_120013, %_610038.0]
  %_610155 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_610001.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_610011.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_610020.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_610029.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM28scala.scalanative.unsafe.TagG4type" to i8*), %_610038.0]
  %_610344 = bitcast i8* %_610154 to i8**
  %_610156 = load i8*, i8** %_610344
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_610156, i8* %_610155)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM17java.lang.System$D25getUserCountry$$anonfun$1L16java.lang.StringL16java.lang.StringEPT17java.lang.System$"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30003 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_30004 = call dereferenceable_or_null(32) i8* @"_SM13scala.Predef$D13augmentStringL16java.lang.StringL16java.lang.StringEO"(i8* nonnull dereferenceable(16) %_30003, i8* dereferenceable_or_null(32) %_2)
  %_30006 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.lang.System$$$Lambda$15G4type" to i8*), i64 16)
  %_30020 = bitcast i8* %_30006 to { i8*, i8* }*
  %_30021 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30020, i32 0, i32 1
  %_30017 = bitcast i8** %_30021 to i8*
  %_30022 = bitcast i8* %_30017 to i8**
  store i8* %_1, i8** %_30022
  %_30008 = call dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D19dropWhile$extensionL16java.lang.StringL15scala.Function1L16java.lang.StringEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_30004, i8* nonnull dereferenceable(16) %_30006)
  %_30009 = call dereferenceable_or_null(32) i8* @"_SM13scala.Predef$D13augmentStringL16java.lang.StringL16java.lang.StringEO"(i8* nonnull dereferenceable(16) %_30003, i8* dereferenceable_or_null(32) %_30008)
  %_30011 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.lang.System$$$Lambda$16G4type" to i8*), i64 16)
  %_30023 = bitcast i8* %_30011 to { i8*, i8* }*
  %_30024 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30023, i32 0, i32 1
  %_30019 = bitcast i8** %_30024 to i8*
  %_30025 = bitcast i8* %_30019 to i8**
  store i8* %_1, i8** %_30025
  %_30013 = call dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D19takeWhile$extensionL16java.lang.StringL15scala.Function1L16java.lang.StringEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_30009, i8* nonnull dereferenceable(16) %_30011)
  %_30014 = call dereferenceable_or_null(32) i8* @"_SM13scala.Predef$D13augmentStringL16java.lang.StringL16java.lang.StringEO"(i8* nonnull dereferenceable(16) %_30003, i8* dereferenceable_or_null(32) %_30013)
  %_30015 = call dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D14drop$extensionL16java.lang.StringiL16java.lang.StringEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_30014, i32 1)
  ret i8* %_30015
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D25loadProperties$$anonfun$1L20java.util.PropertiesL16java.lang.StringL16java.lang.ObjectEPT17java.lang.System$"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40003 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* dereferenceable_or_null(24) %_2, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-243" to i8*), i8* dereferenceable_or_null(32) %_3)
  ret i8* %_40003
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D25loadProperties$$anonfun$2L20java.util.PropertiesL16java.lang.StringL16java.lang.ObjectEPT17java.lang.System$"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40003 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* dereferenceable_or_null(24) %_2, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-245" to i8*), i8* dereferenceable_or_null(32) %_3)
  ret i8* %_40003
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D25loadProperties$$anonfun$3L20java.util.PropertiesL16java.lang.StringL16java.lang.ObjectEPT17java.lang.System$"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40003 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* dereferenceable_or_null(24) %_2, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-247" to i8*), i8* dereferenceable_or_null(32) %_3)
  ret i8* %_40003
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D25loadProperties$$anonfun$4L20java.util.PropertiesL16java.lang.StringL16java.lang.ObjectEPT17java.lang.System$"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40003 = call dereferenceable_or_null(8) i8* @"_SM20java.util.PropertiesD11setPropertyL16java.lang.StringL16java.lang.StringL16java.lang.ObjectEO"(i8* dereferenceable_or_null(24) %_2, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-249" to i8*), i8* dereferenceable_or_null(32) %_3)
  ret i8* %_40003
}

define dereferenceable_or_null(32) i8* @"_SM17java.lang.System$D26getUserLanguage$$anonfun$1L16java.lang.StringL16java.lang.StringEPT17java.lang.System$"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30003 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_30004 = call dereferenceable_or_null(32) i8* @"_SM13scala.Predef$D13augmentStringL16java.lang.StringL16java.lang.StringEO"(i8* nonnull dereferenceable(16) %_30003, i8* dereferenceable_or_null(32) %_2)
  %_30006 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.lang.System$$$Lambda$14G4type" to i8*), i64 16)
  %_30011 = bitcast i8* %_30006 to { i8*, i8* }*
  %_30012 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30011, i32 0, i32 1
  %_30010 = bitcast i8** %_30012 to i8*
  %_30013 = bitcast i8* %_30010 to i8**
  store i8* %_1, i8** %_30013
  %_30008 = call dereferenceable_or_null(32) i8* @"_SM27scala.collection.StringOps$D19takeWhile$extensionL16java.lang.StringL15scala.Function1L16java.lang.StringEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.collection.StringOps$G8instance" to i8*), i8* dereferenceable_or_null(32) %_30004, i8* nonnull dereferenceable(16) %_30006)
  ret i8* %_30008
}

define dereferenceable_or_null(32) i8* @"_SM17java.lang.System$D30getCurrentDirectory$$anonfun$1L28scala.scalanative.unsafe.PtrL16java.lang.StringEPT17java.lang.System$"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = call dereferenceable_or_null(16) i8* @"_SM33scala.scalanative.unsafe.package$G4load"()
  %_30003 = call dereferenceable_or_null(32) i8* @"_SM33scala.scalanative.unsafe.package$D21fromCString$default$2L24java.nio.charset.CharsetEO"(i8* nonnull dereferenceable(16) %_30002)
  %_30005 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM33scala.scalanative.unsafe.package$D11fromCStringL28scala.scalanative.unsafe.PtrL24java.nio.charset.CharsetL16java.lang.StringEO" to i8*) to i8* (i8*, i8*, i8*)*
  %_30004 = call dereferenceable_or_null(32) i8* %_30005(i8* nonnull dereferenceable(16) %_30002, i8* dereferenceable_or_null(16) %_2, i8* dereferenceable_or_null(32) %_30003)
  ret i8* %_30004
}

define i1 @"_SM17java.lang.System$D36getUserCountry$$anonfun$1$$anonfun$1czEPT17java.lang.System$"(i8* %_1, i16 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30003 = zext i16 %_2 to i32
  %_30004 = icmp ne i32 %_30003, 95
  ret i1 %_30004
}

define i1 @"_SM17java.lang.System$D36getUserCountry$$anonfun$1$$anonfun$2czEPT17java.lang.System$"(i8* %_1, i16 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30003 = zext i16 %_2 to i32
  %_30004 = icmp ne i32 %_30003, 46
  br i1 %_30004, label %_40000.0, label %_50000.0
_40000.0:
  %_40002 = icmp ne i32 %_30003, 64
  br label %_60000.0
_50000.0:
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [false, %_50000.0], [%_40002, %_40000.0]
  ret i1 %_60001
}

define i1 @"_SM17java.lang.System$D37getUserLanguage$$anonfun$1$$anonfun$1czEPT17java.lang.System$"(i8* %_1, i16 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30003 = zext i16 %_2 to i32
  %_30004 = icmp ne i32 %_30003, 95
  ret i1 %_30004
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D5env$1L16java.lang.StringL12scala.OptionEPT17java.lang.System$"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D7envVarsL13java.util.MapEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_1)
  %_30009 = icmp ne i8* %_30002, null
  br i1 %_30009, label %_30007.0, label %_30008.0
_30007.0:
  %_30016 = bitcast i8* %_30002 to i8**
  %_30010 = load i8*, i8** %_30016
  %_30017 = bitcast i8* %_30010 to { i8*, i32, i32, i8* }*
  %_30018 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30017, i32 0, i32 2
  %_30011 = bitcast i32* %_30018 to i8*
  %_30019 = bitcast i8* %_30011 to i32*
  %_30012 = load i32, i32* %_30019
  %_30020 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30021 = getelementptr i8*, i8** %_30020, i32 2838
  %_30013 = bitcast i8** %_30021 to i8*
  %_30022 = bitcast i8* %_30013 to i8**
  %_30023 = getelementptr i8*, i8** %_30022, i32 %_30012
  %_30014 = bitcast i8** %_30023 to i8*
  %_30024 = bitcast i8* %_30014 to i8**
  %_30004 = load i8*, i8** %_30024
  %_30025 = bitcast i8* %_30004 to i8* (i8*, i8*)*
  %_30005 = call dereferenceable_or_null(8) i8* %_30025(i8* dereferenceable_or_null(8) %_30002, i8* dereferenceable_or_null(32) %_2)
  %_30026 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM13scala.Option$D5applyL16java.lang.ObjectL12scala.OptionEO" to i8*) to i8* (i8*, i8*)*
  %_30006 = call dereferenceable_or_null(8) i8* %_30026(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM13scala.Option$G8instance" to i8*), i8* dereferenceable_or_null(8) %_30005)
  ret i8* %_30006
_30008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM17java.lang.System$D6getenvL16java.lang.StringL16java.lang.StringEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D7envVarsL13java.util.MapEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_1)
  %_30002 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD11toUpperCaseL16java.lang.StringEO"(i8* dereferenceable_or_null(32) %_2)
  %_30009 = icmp ne i8* %_30001, null
  br i1 %_30009, label %_30007.0, label %_30008.0
_30007.0:
  %_30026 = bitcast i8* %_30001 to i8**
  %_30010 = load i8*, i8** %_30026
  %_30027 = bitcast i8* %_30010 to { i8*, i32, i32, i8* }*
  %_30028 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30027, i32 0, i32 2
  %_30011 = bitcast i32* %_30028 to i8*
  %_30029 = bitcast i8* %_30011 to i32*
  %_30012 = load i32, i32* %_30029
  %_30030 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30031 = getelementptr i8*, i8** %_30030, i32 2838
  %_30013 = bitcast i8** %_30031 to i8*
  %_30032 = bitcast i8* %_30013 to i8**
  %_30033 = getelementptr i8*, i8** %_30032, i32 %_30012
  %_30014 = bitcast i8** %_30033 to i8*
  %_30034 = bitcast i8* %_30014 to i8**
  %_30004 = load i8*, i8** %_30034
  %_30035 = bitcast i8* %_30004 to i8* (i8*, i8*)*
  %_30005 = call dereferenceable_or_null(8) i8* %_30035(i8* dereferenceable_or_null(8) %_30001, i8* dereferenceable_or_null(32) %_30002)
  %_30018 = icmp eq i8* %_30005, null
  br i1 %_30018, label %_30016.0, label %_30015.0
_30015.0:
  %_30036 = bitcast i8* %_30005 to i8**
  %_30019 = load i8*, i8** %_30036
  %_30020 = icmp eq i8* %_30019, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*)
  br i1 %_30020, label %_30016.0, label %_30017.0
_30016.0:
  %_30006 = bitcast i8* %_30005 to i8*
  ret i8* %_30006
_30008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_30017.0:
  %_30022 = phi i8* [%_30005, %_30015.0]
  %_30023 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), %_30015.0]
  %_30037 = bitcast i8* %_30022 to i8**
  %_30024 = load i8*, i8** %_30037
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_30024, i8* %_30023)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D7envVarsL13java.util.MapEPT17java.lang.System$"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_2.0:
  br label %_3.0
_3.0:
  br i1 true, label %_4.0, label %_5.0
_4.0:
  %_79 = icmp ne i8* %_1, null
  br i1 %_79, label %_77.0, label %_78.0
_77.0:
  %_154 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_155 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_154, i32 0, i32 4
  %_80 = bitcast i64* %_155 to i8*
  %_8 = call i64 @"_SM35scala.scalanative.runtime.LazyVals$D3getR_jEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.runtime.LazyVals$G8instance" to i8*), i8* %_80)
  %_10 = call i64 @"_SM23scala.runtime.LazyVals$D5STATEjijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM23scala.runtime.LazyVals$G8instance" to i8*), i64 %_8, i32 0)
  %_15 = sext i32 3 to i64
  %_16 = icmp eq i64 %_10, %_15
  br i1 %_16, label %_11.0, label %_12.0
_11.0:
  %_17 = call dereferenceable_or_null(56) i8* @"_SM17java.lang.System$G4load"()
  %_156 = bitcast i8* %_17 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_157 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_156, i32 0, i32 3
  %_81 = bitcast i8** %_157 to i8*
  %_158 = bitcast i8* %_81 to i8**
  %_18 = load i8*, i8** %_158, !dereferenceable_or_null !{i64 8}
  ret i8* %_18
_12.0:
  %_24 = sext i32 0 to i64
  %_25 = icmp eq i64 %_10, %_24
  br i1 %_25, label %_20.0, label %_21.0
_20.0:
  %_83 = icmp ne i8* %_1, null
  br i1 %_83, label %_82.0, label %_78.0
_82.0:
  %_159 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_160 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_159, i32 0, i32 4
  %_84 = bitcast i64* %_160 to i8*
  %_32 = call i1 @"_SM35scala.scalanative.runtime.LazyVals$D3CASR_jiizEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.runtime.LazyVals$G8instance" to i8*), i8* %_84, i64 %_8, i32 1, i32 0)
  br i1 %_32, label %_26.0, label %_27.0
_26.0:
  br label %_35.0
_35.0:
  br label %_42.0
_42.0:
  %_161 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.lang.System$D13getEnvsUnix$1L17java.util.HashMapEPT17java.lang.System$" to i8*) to i8* (i8*)*
  %_48 = invoke dereferenceable_or_null(32) i8* %_161(i8* dereferenceable_or_null(56) %_1) to label %_42.1 unwind label %_88.landingpad
_42.1:
  br label %_43.0
_43.0:
  %_44 = phi i8* [%_48, %_42.1]
  %_162 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM22java.util.Collections$D15unmodifiableMapL13java.util.MapL13java.util.MapEO" to i8*) to i8* (i8*, i8*)*
  %_50 = invoke dereferenceable_or_null(8) i8* %_162(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM22java.util.Collections$G8instance" to i8*), i8* dereferenceable_or_null(8) %_44) to label %_43.1 unwind label %_90.landingpad
_43.1:
  %_52 = invoke dereferenceable_or_null(56) i8* @"_SM17java.lang.System$G4load"() to label %_43.2 unwind label %_92.landingpad
_43.2:
  %_163 = bitcast i8* %_52 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_164 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_163, i32 0, i32 3
  %_96 = bitcast i8** %_164 to i8*
  %_165 = bitcast i8* %_96 to i8**
  store i8* %_50, i8** %_165
  %_105 = icmp ne i8* %_1, null
  br i1 %_105, label %_102.0, label %_103.0
_102.0:
  %_166 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_167 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_166, i32 0, i32 4
  %_107 = bitcast i64* %_167 to i8*
  invoke nonnull dereferenceable(8) i8* @"_SM35scala.scalanative.runtime.LazyVals$D7setFlagR_iiuEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.runtime.LazyVals$G8instance" to i8*), i8* %_107, i32 3, i32 0) to label %_102.1 unwind label %_111.landingpad
_102.1:
  ret i8* %_50
_33.0:
  %_37 = phi i8* [%_59, %_109.0], [%_57, %_101.0], [%_55, %_99.0], [%_53, %_93.0], [%_51, %_91.0], [%_49, %_89.0], [%_47, %_87.0], [%_39, %_85.0]
  %_116 = icmp eq i8* %_37, null
  br i1 %_116, label %_113.0, label %_114.0
_113.0:
  br label %_115.0
_114.0:
  %_168 = bitcast i8* %_37 to i8**
  %_117 = load i8*, i8** %_168
  %_169 = bitcast i8* %_117 to { i8*, i32, i32, i8* }*
  %_170 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_169, i32 0, i32 1
  %_118 = bitcast i32* %_170 to i8*
  %_171 = bitcast i8* %_118 to i32*
  %_119 = load i32, i32* %_171
  %_120 = icmp sle i32 126, %_119
  %_121 = icmp sle i32 %_119, 175
  %_122 = and i1 %_120, %_121
  br label %_115.0
_115.0:
  %_62 = phi i1 [%_122, %_114.0], [false, %_113.0]
  br i1 %_62, label %_63.0, label %_64.0
_63.0:
  %_126 = icmp eq i8* %_37, null
  br i1 %_126, label %_124.0, label %_123.0
_123.0:
  %_172 = bitcast i8* %_37 to i8**
  %_127 = load i8*, i8** %_172
  %_173 = bitcast i8* %_127 to { i8*, i32, i32, i8* }*
  %_174 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_173, i32 0, i32 1
  %_128 = bitcast i32* %_174 to i8*
  %_175 = bitcast i8* %_128 to i32*
  %_129 = load i32, i32* %_175
  %_130 = icmp sle i32 126, %_129
  %_131 = icmp sle i32 %_129, 175
  %_132 = and i1 %_130, %_131
  br i1 %_132, label %_124.0, label %_125.0
_124.0:
  %_67 = bitcast i8* %_37 to i8*
  %_134 = icmp ne i8* %_1, null
  br i1 %_134, label %_133.0, label %_78.0
_133.0:
  %_176 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_177 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_176, i32 0, i32 4
  %_135 = bitcast i64* %_177 to i8*
  call nonnull dereferenceable(8) i8* @"_SM35scala.scalanative.runtime.LazyVals$D7setFlagR_iiuEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.runtime.LazyVals$G8instance" to i8*), i8* %_135, i32 0, i32 0)
  %_138 = icmp ne i8* %_67, null
  br i1 %_138, label %_137.0, label %_78.0
_137.0:
  call i8* @"scalanative_throw"(i8* dereferenceable_or_null(40) %_67)
  unreachable
_64.0:
  %_141 = icmp ne i8* %_37, null
  br i1 %_141, label %_140.0, label %_78.0
_140.0:
  call i8* @"scalanative_throw"(i8* dereferenceable_or_null(8) %_37)
  unreachable
_27.0:
  br label %_28.0
_28.0:
  %_29 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_27.0]
  br label %_22.0
_21.0:
  %_144 = icmp ne i8* %_1, null
  br i1 %_144, label %_143.0, label %_78.0
_143.0:
  %_178 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_179 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_178, i32 0, i32 4
  %_145 = bitcast i64* %_179 to i8*
  %_180 = bitcast i8* bitcast (i8* (i8*, i8*, i64, i32)* @"_SM35scala.scalanative.runtime.LazyVals$D17wait4NotificationR_jiuEO" to i8*) to i8* (i8*, i8*, i64, i32)*
  call nonnull dereferenceable(8) i8* %_180(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.runtime.LazyVals$G8instance" to i8*), i8* %_145, i64 %_8, i32 0)
  br label %_22.0
_22.0:
  %_23 = phi i8* [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_143.0], [%_29, %_28.0]
  br label %_13.0
_13.0:
  %_14 = phi i8* [%_23, %_22.0]
  br label %_3.0
_5.0:
  ret i8* zeroinitializer
_78.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_103.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_103.1 unwind label %_148.landingpad
_103.1:
  unreachable
_125.0:
  %_150 = phi i8* [%_37, %_123.0]
  %_151 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM19java.lang.ThrowableG4type" to i8*), %_123.0]
  %_181 = bitcast i8* %_150 to i8**
  %_152 = load i8*, i8** %_181
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_152, i8* %_151)
  unreachable
_85.0:
  %_39 = phi i8* [%_86, %_86.landingpad.succ]
  br label %_33.0
_87.0:
  %_47 = phi i8* [%_88, %_88.landingpad.succ]
  br label %_33.0
_89.0:
  %_49 = phi i8* [%_90, %_90.landingpad.succ]
  br label %_33.0
_91.0:
  %_51 = phi i8* [%_92, %_92.landingpad.succ]
  br label %_33.0
_93.0:
  %_53 = phi i8* [%_95, %_95.landingpad.succ], [%_97, %_97.landingpad.succ]
  br label %_33.0
_99.0:
  %_55 = phi i8* [%_100, %_100.landingpad.succ]
  br label %_33.0
_101.0:
  %_57 = phi i8* [%_104, %_104.landingpad.succ], [%_148, %_148.landingpad.succ], [%_106, %_106.landingpad.succ], [%_108, %_108.landingpad.succ]
  br label %_33.0
_109.0:
  %_59 = phi i8* [%_111, %_111.landingpad.succ]
  br label %_33.0
_86.landingpad:
  %_182 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_183 = extractvalue { i8*, i32 } %_182, 0
  %_184 = extractvalue { i8*, i32 } %_182, 1
  %_185 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_186 = icmp eq i32 %_184, %_185
  br i1 %_186, label %_86.landingpad.succ, label %_86.landingpad.fail
_86.landingpad.succ:
  %_187 = call i8* @__cxa_begin_catch(i8* %_183)
  %_188 = bitcast i8* %_187 to i8**
  %_189 = getelementptr i8*, i8** %_188, i32 1
  %_86 = load i8*, i8** %_189
  call void @__cxa_end_catch()
  br label %_85.0
_86.landingpad.fail:
  resume { i8*, i32 } %_182
_88.landingpad:
  %_190 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_191 = extractvalue { i8*, i32 } %_190, 0
  %_192 = extractvalue { i8*, i32 } %_190, 1
  %_193 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_194 = icmp eq i32 %_192, %_193
  br i1 %_194, label %_88.landingpad.succ, label %_88.landingpad.fail
_88.landingpad.succ:
  %_195 = call i8* @__cxa_begin_catch(i8* %_191)
  %_196 = bitcast i8* %_195 to i8**
  %_197 = getelementptr i8*, i8** %_196, i32 1
  %_88 = load i8*, i8** %_197
  call void @__cxa_end_catch()
  br label %_87.0
_88.landingpad.fail:
  resume { i8*, i32 } %_190
_90.landingpad:
  %_198 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_199 = extractvalue { i8*, i32 } %_198, 0
  %_200 = extractvalue { i8*, i32 } %_198, 1
  %_201 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_202 = icmp eq i32 %_200, %_201
  br i1 %_202, label %_90.landingpad.succ, label %_90.landingpad.fail
_90.landingpad.succ:
  %_203 = call i8* @__cxa_begin_catch(i8* %_199)
  %_204 = bitcast i8* %_203 to i8**
  %_205 = getelementptr i8*, i8** %_204, i32 1
  %_90 = load i8*, i8** %_205
  call void @__cxa_end_catch()
  br label %_89.0
_90.landingpad.fail:
  resume { i8*, i32 } %_198
_92.landingpad:
  %_206 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_207 = extractvalue { i8*, i32 } %_206, 0
  %_208 = extractvalue { i8*, i32 } %_206, 1
  %_209 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_210 = icmp eq i32 %_208, %_209
  br i1 %_210, label %_92.landingpad.succ, label %_92.landingpad.fail
_92.landingpad.succ:
  %_211 = call i8* @__cxa_begin_catch(i8* %_207)
  %_212 = bitcast i8* %_211 to i8**
  %_213 = getelementptr i8*, i8** %_212, i32 1
  %_92 = load i8*, i8** %_213
  call void @__cxa_end_catch()
  br label %_91.0
_92.landingpad.fail:
  resume { i8*, i32 } %_206
_95.landingpad:
  %_214 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_215 = extractvalue { i8*, i32 } %_214, 0
  %_216 = extractvalue { i8*, i32 } %_214, 1
  %_217 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_218 = icmp eq i32 %_216, %_217
  br i1 %_218, label %_95.landingpad.succ, label %_95.landingpad.fail
_95.landingpad.succ:
  %_219 = call i8* @__cxa_begin_catch(i8* %_215)
  %_220 = bitcast i8* %_219 to i8**
  %_221 = getelementptr i8*, i8** %_220, i32 1
  %_95 = load i8*, i8** %_221
  call void @__cxa_end_catch()
  br label %_93.0
_95.landingpad.fail:
  resume { i8*, i32 } %_214
_97.landingpad:
  %_222 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_223 = extractvalue { i8*, i32 } %_222, 0
  %_224 = extractvalue { i8*, i32 } %_222, 1
  %_225 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_226 = icmp eq i32 %_224, %_225
  br i1 %_226, label %_97.landingpad.succ, label %_97.landingpad.fail
_97.landingpad.succ:
  %_227 = call i8* @__cxa_begin_catch(i8* %_223)
  %_228 = bitcast i8* %_227 to i8**
  %_229 = getelementptr i8*, i8** %_228, i32 1
  %_97 = load i8*, i8** %_229
  call void @__cxa_end_catch()
  br label %_93.0
_97.landingpad.fail:
  resume { i8*, i32 } %_222
_100.landingpad:
  %_230 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_231 = extractvalue { i8*, i32 } %_230, 0
  %_232 = extractvalue { i8*, i32 } %_230, 1
  %_233 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_234 = icmp eq i32 %_232, %_233
  br i1 %_234, label %_100.landingpad.succ, label %_100.landingpad.fail
_100.landingpad.succ:
  %_235 = call i8* @__cxa_begin_catch(i8* %_231)
  %_236 = bitcast i8* %_235 to i8**
  %_237 = getelementptr i8*, i8** %_236, i32 1
  %_100 = load i8*, i8** %_237
  call void @__cxa_end_catch()
  br label %_99.0
_100.landingpad.fail:
  resume { i8*, i32 } %_230
_104.landingpad:
  %_238 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_239 = extractvalue { i8*, i32 } %_238, 0
  %_240 = extractvalue { i8*, i32 } %_238, 1
  %_241 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_242 = icmp eq i32 %_240, %_241
  br i1 %_242, label %_104.landingpad.succ, label %_104.landingpad.fail
_104.landingpad.succ:
  %_243 = call i8* @__cxa_begin_catch(i8* %_239)
  %_244 = bitcast i8* %_243 to i8**
  %_245 = getelementptr i8*, i8** %_244, i32 1
  %_104 = load i8*, i8** %_245
  call void @__cxa_end_catch()
  br label %_101.0
_104.landingpad.fail:
  resume { i8*, i32 } %_238
_106.landingpad:
  %_246 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_247 = extractvalue { i8*, i32 } %_246, 0
  %_248 = extractvalue { i8*, i32 } %_246, 1
  %_249 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_250 = icmp eq i32 %_248, %_249
  br i1 %_250, label %_106.landingpad.succ, label %_106.landingpad.fail
_106.landingpad.succ:
  %_251 = call i8* @__cxa_begin_catch(i8* %_247)
  %_252 = bitcast i8* %_251 to i8**
  %_253 = getelementptr i8*, i8** %_252, i32 1
  %_106 = load i8*, i8** %_253
  call void @__cxa_end_catch()
  br label %_101.0
_106.landingpad.fail:
  resume { i8*, i32 } %_246
_108.landingpad:
  %_254 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_255 = extractvalue { i8*, i32 } %_254, 0
  %_256 = extractvalue { i8*, i32 } %_254, 1
  %_257 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_258 = icmp eq i32 %_256, %_257
  br i1 %_258, label %_108.landingpad.succ, label %_108.landingpad.fail
_108.landingpad.succ:
  %_259 = call i8* @__cxa_begin_catch(i8* %_255)
  %_260 = bitcast i8* %_259 to i8**
  %_261 = getelementptr i8*, i8** %_260, i32 1
  %_108 = load i8*, i8** %_261
  call void @__cxa_end_catch()
  br label %_101.0
_108.landingpad.fail:
  resume { i8*, i32 } %_254
_111.landingpad:
  %_262 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_263 = extractvalue { i8*, i32 } %_262, 0
  %_264 = extractvalue { i8*, i32 } %_262, 1
  %_265 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_266 = icmp eq i32 %_264, %_265
  br i1 %_266, label %_111.landingpad.succ, label %_111.landingpad.fail
_111.landingpad.succ:
  %_267 = call i8* @__cxa_begin_catch(i8* %_263)
  %_268 = bitcast i8* %_267 to i8**
  %_269 = getelementptr i8*, i8** %_268, i32 1
  %_111 = load i8*, i8** %_269
  call void @__cxa_end_catch()
  br label %_109.0
_111.landingpad.fail:
  resume { i8*, i32 } %_262
_148.landingpad:
  %_270 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_271 = extractvalue { i8*, i32 } %_270, 0
  %_272 = extractvalue { i8*, i32 } %_270, 1
  %_273 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_274 = icmp eq i32 %_272, %_273
  br i1 %_274, label %_148.landingpad.succ, label %_148.landingpad.fail
_148.landingpad.succ:
  %_275 = call i8* @__cxa_begin_catch(i8* %_271)
  %_276 = bitcast i8* %_275 to i8**
  %_277 = getelementptr i8*, i8** %_276, i32 1
  %_148 = load i8*, i8** %_277
  call void @__cxa_end_catch()
  br label %_101.0
_148.landingpad.fail:
  resume { i8*, i32 } %_270
}

define nonnull dereferenceable(8) i8* @"_SM17java.lang.System$D9arraycopyL16java.lang.ObjectiL16java.lang.ObjectiiuEO"(i8* %_1, i8* %_2, i32 %_3, i8* %_4, i32 %_5, i32 %_6) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_70000.0:
  call nonnull dereferenceable(8) i8* @"_SM32scala.scalanative.runtime.Array$D4copyL16java.lang.ObjectiL16java.lang.ObjectiiuEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM32scala.scalanative.runtime.Array$G8instance" to i8*), i8* dereferenceable_or_null(8) %_2, i32 %_3, i8* dereferenceable_or_null(8) %_4, i32 %_5, i32 %_6)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(56) i8* @"_SM17java.lang.System$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 20
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 56}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM17java.lang.System$G4type" to i8*), i64 56)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM17java.lang.System$RE"(i8* dereferenceable_or_null(56) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM17java.lang.System$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(56) i8* @"_SM17java.lang.System$G4load"()
  %_20003 = call dereferenceable_or_null(32) i8* @"_SM23java.io.FileDescriptor$G4load"()
  %_270048 = bitcast i8* %_20003 to { i8*, i8*, i8*, i8* }*
  %_270049 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_270048, i32 0, i32 3
  %_270002 = bitcast i8** %_270049 to i8*
  %_270050 = bitcast i8* %_270002 to i8**
  %_40001 = load i8*, i8** %_270050, !dereferenceable_or_null !{i64 24}
  %_50001 = call dereferenceable_or_null(8) i8* @"_SM11scala.None$G4load"()
  %_20004 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM23java.io.FileInputStreamG4type" to i8*), i64 24)
  %_20005 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM33java.nio.channels.FileChannelImplG4type" to i8*), i64 40)
  %_270051 = bitcast i8* %_20005 to { i8*, i1, i8*, i8*, i1 }*
  %_270052 = getelementptr { i8*, i1, i8*, i8*, i1 }, { i8*, i1, i8*, i8*, i1 }* %_270051, i32 0, i32 4
  %_270004 = bitcast i1* %_270052 to i8*
  %_270053 = bitcast i8* %_270004 to i1*
  store i1 true, i1* %_270053
  %_270054 = bitcast i8* %_20005 to { i8*, i1, i8*, i8*, i1 }*
  %_270055 = getelementptr { i8*, i1, i8*, i8*, i1 }, { i8*, i1, i8*, i8*, i1 }* %_270054, i32 0, i32 3
  %_270006 = bitcast i8** %_270055 to i8*
  %_270056 = bitcast i8* %_270006 to i8**
  store i8* %_50001, i8** %_270056
  %_270057 = bitcast i8* %_20005 to { i8*, i1, i8*, i8*, i1 }*
  %_270058 = getelementptr { i8*, i1, i8*, i8*, i1 }, { i8*, i1, i8*, i8*, i1 }* %_270057, i32 0, i32 2
  %_270008 = bitcast i8** %_270058 to i8*
  %_270059 = bitcast i8* %_270008 to i8**
  store i8* %_40001, i8** %_270059
  %_270060 = bitcast i8* %_20004 to { i8*, i8*, i8* }*
  %_270061 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_270060, i32 0, i32 2
  %_270010 = bitcast i8** %_270061 to i8*
  %_270062 = bitcast i8* %_270010 to i8**
  store i8* %_20005, i8** %_270062
  %_270063 = bitcast i8* %_20004 to { i8*, i8*, i8* }*
  %_270064 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_270063, i32 0, i32 1
  %_270012 = bitcast i8** %_270064 to i8*
  %_270065 = bitcast i8* %_270012 to i8**
  store i8* %_40001, i8** %_270065
  %_270066 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_270067 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_270066, i32 0, i32 6
  %_270014 = bitcast i8** %_270067 to i8*
  %_270068 = bitcast i8* %_270014 to i8**
  store i8* %_20004, i8** %_270068
  %_270069 = bitcast i8* %_20003 to { i8*, i8*, i8*, i8* }*
  %_270070 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_270069, i32 0, i32 2
  %_270015 = bitcast i8** %_270070 to i8*
  %_270071 = bitcast i8* %_270015 to i8**
  %_130001 = load i8*, i8** %_270071, !dereferenceable_or_null !{i64 24}
  %_20014 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] }* @"_SM19java.io.PrintStreamG4type" to i8*), i64 56)
  %_20015 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] }* @"_SM24java.io.FileOutputStreamG4type" to i8*), i64 24)
  %_20016 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM33java.nio.channels.FileChannelImplG4type" to i8*), i64 40)
  %_270072 = bitcast i8* %_20016 to { i8*, i1, i8*, i8*, i1 }*
  %_270073 = getelementptr { i8*, i1, i8*, i8*, i1 }, { i8*, i1, i8*, i8*, i1 }* %_270072, i32 0, i32 1
  %_270017 = bitcast i1* %_270073 to i8*
  %_270074 = bitcast i8* %_270017 to i1*
  store i1 true, i1* %_270074
  %_270075 = bitcast i8* %_20016 to { i8*, i1, i8*, i8*, i1 }*
  %_270076 = getelementptr { i8*, i1, i8*, i8*, i1 }, { i8*, i1, i8*, i8*, i1 }* %_270075, i32 0, i32 3
  %_270019 = bitcast i8** %_270076 to i8*
  %_270077 = bitcast i8* %_270019 to i8**
  store i8* %_50001, i8** %_270077
  %_270078 = bitcast i8* %_20016 to { i8*, i1, i8*, i8*, i1 }*
  %_270079 = getelementptr { i8*, i1, i8*, i8*, i1 }, { i8*, i1, i8*, i8*, i1 }* %_270078, i32 0, i32 2
  %_270021 = bitcast i8** %_270079 to i8*
  %_270080 = bitcast i8* %_270021 to i8**
  store i8* %_130001, i8** %_270080
  %_270081 = bitcast i8* %_20015 to { i8*, i8*, i8* }*
  %_270082 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_270081, i32 0, i32 2
  %_270023 = bitcast i8** %_270082 to i8*
  %_270083 = bitcast i8* %_270023 to i8**
  store i8* %_20016, i8** %_270083
  %_270084 = bitcast i8* %_20015 to { i8*, i8*, i8* }*
  %_270085 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_270084, i32 0, i32 1
  %_270025 = bitcast i8** %_270085 to i8*
  %_270086 = bitcast i8* %_270025 to i8**
  store i8* %_130001, i8** %_270086
  %_270087 = bitcast i8* %_20014 to { i8*, i8* }*
  %_270088 = getelementptr { i8*, i8* }, { i8*, i8* }* %_270087, i32 0, i32 1
  %_270027 = bitcast i8** %_270088 to i8*
  %_270089 = bitcast i8* %_270027 to i8**
  store i8* %_20015, i8** %_270089
  %_270090 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_270091 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_270090, i32 0, i32 5
  %_270029 = bitcast i8** %_270091 to i8*
  %_270092 = bitcast i8* %_270029 to i8**
  store i8* %_20014, i8** %_270092
  %_270093 = bitcast i8* %_20003 to { i8*, i8*, i8*, i8* }*
  %_270094 = getelementptr { i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8* }* %_270093, i32 0, i32 1
  %_270030 = bitcast i8** %_270094 to i8*
  %_270095 = bitcast i8* %_270030 to i8**
  %_270001 = load i8*, i8** %_270095, !dereferenceable_or_null !{i64 24}
  %_20026 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] }* @"_SM19java.io.PrintStreamG4type" to i8*), i64 56)
  %_20027 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] }* @"_SM24java.io.FileOutputStreamG4type" to i8*), i64 24)
  %_20028 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM33java.nio.channels.FileChannelImplG4type" to i8*), i64 40)
  %_270096 = bitcast i8* %_20028 to { i8*, i1, i8*, i8*, i1 }*
  %_270097 = getelementptr { i8*, i1, i8*, i8*, i1 }, { i8*, i1, i8*, i8*, i1 }* %_270096, i32 0, i32 1
  %_270032 = bitcast i1* %_270097 to i8*
  %_270098 = bitcast i8* %_270032 to i1*
  store i1 true, i1* %_270098
  %_270099 = bitcast i8* %_20028 to { i8*, i1, i8*, i8*, i1 }*
  %_270100 = getelementptr { i8*, i1, i8*, i8*, i1 }, { i8*, i1, i8*, i8*, i1 }* %_270099, i32 0, i32 3
  %_270034 = bitcast i8** %_270100 to i8*
  %_270101 = bitcast i8* %_270034 to i8**
  store i8* %_50001, i8** %_270101
  %_270102 = bitcast i8* %_20028 to { i8*, i1, i8*, i8*, i1 }*
  %_270103 = getelementptr { i8*, i1, i8*, i8*, i1 }, { i8*, i1, i8*, i8*, i1 }* %_270102, i32 0, i32 2
  %_270036 = bitcast i8** %_270103 to i8*
  %_270104 = bitcast i8* %_270036 to i8**
  store i8* %_270001, i8** %_270104
  %_270105 = bitcast i8* %_20027 to { i8*, i8*, i8* }*
  %_270106 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_270105, i32 0, i32 2
  %_270038 = bitcast i8** %_270106 to i8*
  %_270107 = bitcast i8* %_270038 to i8**
  store i8* %_20028, i8** %_270107
  %_270108 = bitcast i8* %_20027 to { i8*, i8*, i8* }*
  %_270109 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_270108, i32 0, i32 1
  %_270040 = bitcast i8** %_270109 to i8*
  %_270110 = bitcast i8* %_270040 to i8**
  store i8* %_270001, i8** %_270110
  %_270111 = bitcast i8* %_20026 to { i8*, i8* }*
  %_270112 = getelementptr { i8*, i8* }, { i8*, i8* }* %_270111, i32 0, i32 1
  %_270042 = bitcast i8** %_270112 to i8*
  %_270113 = bitcast i8* %_270042 to i8**
  store i8* %_20027, i8** %_270113
  %_270114 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_270115 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_270114, i32 0, i32 1
  %_270044 = bitcast i8** %_270115 to i8*
  %_270116 = bitcast i8* %_270044 to i8**
  store i8* %_20026, i8** %_270116
  %_270117 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.lang.System$D14loadPropertiesL20java.util.PropertiesEPT17java.lang.System$" to i8*) to i8* (i8*)*
  %_20036 = call dereferenceable_or_null(24) i8* %_270117(i8* dereferenceable_or_null(56) %_1)
  %_270118 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i64, i8*, i8* }*
  %_270119 = getelementptr { i8*, i8*, i8*, i8*, i64, i8*, i8* }, { i8*, i8*, i8*, i8*, i64, i8*, i8* }* %_270118, i32 0, i32 2
  %_270046 = bitcast i8** %_270119 to i8*
  %_270120 = bitcast i8* %_270046 to i8**
  store i8* %_20036, i8** %_270120
  call nonnull dereferenceable(8) i8* @"scalanative_set_os_props"(i8* bitcast (i8* (i8*, i8*)* @"_SM27java.lang.System$$$Lambda$1G17$extern$forwarder" to i8*))
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(8) i8* @"_SM20java.io.OutputStreamD5flushuEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM20scala.collection.SeqD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_1, %_2
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  br label %_70000.0
_70000.0:
  %_130004 = icmp eq i8* %_2, null
  br i1 %_130004, label %_130001.0, label %_130002.0
_130001.0:
  br label %_130003.0
_130002.0:
  %_130039 = bitcast i8* %_2 to i8**
  %_130005 = load i8*, i8** %_130039
  %_130040 = bitcast i8* %_130005 to { i8*, i32, i32, i8* }*
  %_130041 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_130040, i32 0, i32 1
  %_130006 = bitcast i32* %_130041 to i8*
  %_130042 = bitcast i8* %_130006 to i32*
  %_130007 = load i32, i32* %_130042
  %_130043 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_130044 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_130043, i32 0, i32 %_130007, i32 26
  %_130008 = bitcast i1* %_130044 to i8*
  %_130045 = bitcast i8* %_130008 to i1*
  %_130009 = load i1, i1* %_130045
  br label %_130003.0
_130003.0:
  %_70002 = phi i1 [%_130009, %_130002.0], [false, %_130001.0]
  br i1 %_70002, label %_80000.0, label %_90000.0
_80000.0:
  %_130013 = icmp eq i8* %_2, null
  br i1 %_130013, label %_130011.0, label %_130010.0
_130010.0:
  %_130046 = bitcast i8* %_2 to i8**
  %_130014 = load i8*, i8** %_130046
  %_130047 = bitcast i8* %_130014 to { i8*, i32, i32, i8* }*
  %_130048 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_130047, i32 0, i32 1
  %_130015 = bitcast i32* %_130048 to i8*
  %_130049 = bitcast i8* %_130015 to i32*
  %_130016 = load i32, i32* %_130049
  %_130050 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_130051 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_130050, i32 0, i32 %_130016, i32 26
  %_130017 = bitcast i1* %_130051 to i8*
  %_130052 = bitcast i8* %_130017 to i1*
  %_130018 = load i1, i1* %_130052
  br i1 %_130018, label %_130011.0, label %_130012.0
_130011.0:
  %_80001 = bitcast i8* %_2 to i8*
  %_130021 = icmp ne i8* %_80001, null
  br i1 %_130021, label %_130019.0, label %_130020.0
_130019.0:
  %_130053 = bitcast i8* %_80001 to i8**
  %_130022 = load i8*, i8** %_130053
  %_130054 = bitcast i8* %_130022 to { i8*, i32, i32, i8* }*
  %_130055 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_130054, i32 0, i32 2
  %_130023 = bitcast i32* %_130055 to i8*
  %_130056 = bitcast i8* %_130023 to i32*
  %_130024 = load i32, i32* %_130056
  %_130057 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_130058 = getelementptr i8*, i8** %_130057, i32 4141
  %_130025 = bitcast i8** %_130058 to i8*
  %_130059 = bitcast i8* %_130025 to i8**
  %_130060 = getelementptr i8*, i8** %_130059, i32 %_130024
  %_130026 = bitcast i8** %_130060 to i8*
  %_130061 = bitcast i8* %_130026 to i8**
  %_80003 = load i8*, i8** %_130061
  %_130062 = bitcast i8* %_80003 to i1 (i8*, i8*)*
  %_80004 = call i1 %_130062(i8* dereferenceable_or_null(8) %_80001, i8* dereferenceable_or_null(8) %_1)
  br i1 %_80004, label %_100000.0, label %_110000.0
_100000.0:
  %_130028 = icmp ne i8* %_1, null
  br i1 %_130028, label %_130027.0, label %_130020.0
_130027.0:
  %_130063 = bitcast i8* %_1 to i8**
  %_130029 = load i8*, i8** %_130063
  %_130064 = bitcast i8* %_130029 to { i8*, i32, i32, i8* }*
  %_130065 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_130064, i32 0, i32 2
  %_130030 = bitcast i32* %_130065 to i8*
  %_130066 = bitcast i8* %_130030 to i32*
  %_130031 = load i32, i32* %_130066
  %_130067 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_130068 = getelementptr i8*, i8** %_130067, i32 3139
  %_130032 = bitcast i8** %_130068 to i8*
  %_130069 = bitcast i8* %_130032 to i8**
  %_130070 = getelementptr i8*, i8** %_130069, i32 %_130031
  %_130033 = bitcast i8** %_130070 to i8*
  %_130071 = bitcast i8* %_130033 to i8**
  %_100002 = load i8*, i8** %_130071
  %_130072 = bitcast i8* %_100002 to i1 (i8*, i8*)*
  %_100003 = call i1 %_130072(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_80001)
  br label %_120000.0
_110000.0:
  br label %_130000.0
_90000.0:
  br label %_130000.0
_130000.0:
  br label %_120000.0
_120000.0:
  %_120001 = phi i1 [false, %_130000.0], [%_100003, %_130027.0]
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_120001, %_120000.0], [true, %_40000.0]
  ret i1 %_60001
_130020.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_130012.0:
  %_130035 = phi i8* [%_2, %_130010.0]
  %_130036 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM20scala.collection.SeqG4type" to i8*), %_130010.0]
  %_130073 = bitcast i8* %_130035 to i8**
  %_130037 = load i8*, i8** %_130073
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_130037, i8* %_130036)
  unreachable
}

define i1 @"_SM20scala.collection.SeqD8canEqualL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i1 true
}

define i32 @"_SM20scala.collection.SeqD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(24) i8* @"_SM31scala.util.hashing.MurmurHash3$G4load"()
  %_20002 = call i32 @"_SM31scala.util.hashing.MurmurHash3$D7seqHashL20scala.collection.SeqiEO"(i8* nonnull dereferenceable(24) %_20001, i8* dereferenceable_or_null(8) %_1)
  ret i32 %_20002
}

define dereferenceable_or_null(32) i8* @"_SM20scala.collection.SeqD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM25scala.collection.IterableD8toStringL16java.lang.StringEO"(i8* dereferenceable_or_null(8) %_1)
  ret i8* %_20001
}

define i1 @"_SM21java.util.AbstractMapD17equals$$anonfun$1L13java.util.MapL19java.util.Map$EntryzEpT21java.util.AbstractMap"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30017 = bitcast i8* bitcast (i8* (i8*)* @"_SM22java.util.HashMap$NodeD6getKeyL16java.lang.ObjectEO" to i8*) to i8* (i8*)*
  %_30002 = call dereferenceable_or_null(8) i8* %_30017(i8* dereferenceable_or_null(8) %_2)
  %_30010 = icmp ne i8* %_1, null
  br i1 %_30010, label %_30008.0, label %_30009.0
_30008.0:
  %_30018 = bitcast i8* %_1 to i8**
  %_30011 = load i8*, i8** %_30018
  %_30019 = bitcast i8* %_30011 to { i8*, i32, i32, i8* }*
  %_30020 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30019, i32 0, i32 2
  %_30012 = bitcast i32* %_30020 to i8*
  %_30021 = bitcast i8* %_30012 to i32*
  %_30013 = load i32, i32* %_30021
  %_30022 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30023 = getelementptr i8*, i8** %_30022, i32 2838
  %_30014 = bitcast i8** %_30023 to i8*
  %_30024 = bitcast i8* %_30014 to i8**
  %_30025 = getelementptr i8*, i8** %_30024, i32 %_30013
  %_30015 = bitcast i8** %_30025 to i8*
  %_30026 = bitcast i8* %_30015 to i8**
  %_30004 = load i8*, i8** %_30026
  %_30027 = bitcast i8* %_30004 to i8* (i8*, i8*)*
  %_30005 = call dereferenceable_or_null(8) i8* %_30027(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_30002)
  %_30028 = bitcast i8* bitcast (i8* (i8*)* @"_SM22java.util.HashMap$NodeD8getValueL16java.lang.ObjectEO" to i8*) to i8* (i8*)*
  %_30006 = call dereferenceable_or_null(8) i8* %_30028(i8* dereferenceable_or_null(8) %_2)
  %_30007 = call i1 @"_SM18java.util.Objects$D6equalsL16java.lang.ObjectL16java.lang.ObjectzEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM18java.util.Objects$G8instance" to i8*), i8* dereferenceable_or_null(8) %_30005, i8* dereferenceable_or_null(8) %_30006)
  ret i1 %_30007
_30009.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM21java.util.AbstractMapD19hashCode$$anonfun$1iL19java.util.Map$EntryiEpT21java.util.AbstractMap"(i32 %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30004 = bitcast i8* bitcast (i32 (i8*)* @"_SM22java.util.HashMap$NodeD8hashCodeiEO" to i8*) to i32 (i8*)*
  %_30001 = call i32 %_30004(i8* dereferenceable_or_null(8) %_2)
  %_30003 = add i32 %_30001, %_1
  ret i32 %_30003
}

define i1 @"_SM21java.util.AbstractMapD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_2, %_1
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  br label %_70000.0
_70000.0:
  %_190004 = icmp eq i8* %_2, null
  br i1 %_190004, label %_190001.0, label %_190002.0
_190001.0:
  br label %_190003.0
_190002.0:
  %_190034 = bitcast i8* %_2 to i8**
  %_190005 = load i8*, i8** %_190034
  %_190035 = bitcast i8* %_190005 to { i8*, i32, i32, i8* }*
  %_190036 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_190035, i32 0, i32 1
  %_190006 = bitcast i32* %_190036 to i8*
  %_190037 = bitcast i8* %_190006 to i32*
  %_190007 = load i32, i32* %_190037
  %_190038 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_190039 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_190038, i32 0, i32 %_190007, i32 1
  %_190008 = bitcast i1* %_190039 to i8*
  %_190040 = bitcast i8* %_190008 to i1*
  %_190009 = load i1, i1* %_190040
  br label %_190003.0
_190003.0:
  %_70002 = phi i1 [%_190009, %_190002.0], [false, %_190001.0]
  br i1 %_70002, label %_80000.0, label %_90000.0
_80000.0:
  %_190013 = icmp eq i8* %_2, null
  br i1 %_190013, label %_190011.0, label %_190010.0
_190010.0:
  %_190041 = bitcast i8* %_2 to i8**
  %_190014 = load i8*, i8** %_190041
  %_190042 = bitcast i8* %_190014 to { i8*, i32, i32, i8* }*
  %_190043 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_190042, i32 0, i32 1
  %_190015 = bitcast i32* %_190043 to i8*
  %_190044 = bitcast i8* %_190015 to i32*
  %_190016 = load i32, i32* %_190044
  %_190045 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_190046 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_190045, i32 0, i32 %_190016, i32 1
  %_190017 = bitcast i1* %_190046 to i8*
  %_190047 = bitcast i8* %_190017 to i1*
  %_190018 = load i1, i1* %_190047
  br i1 %_190018, label %_190011.0, label %_190012.0
_190011.0:
  %_80001 = bitcast i8* %_2 to i8*
  %_190048 = bitcast i8* bitcast (i32 (i8*)* @"_SM17java.util.HashMapD4sizeiEO" to i8*) to i32 (i8*)*
  %_80002 = call i32 %_190048(i8* dereferenceable_or_null(8) %_1)
  %_190021 = icmp ne i8* %_80001, null
  br i1 %_190021, label %_190019.0, label %_190020.0
_190019.0:
  %_190049 = bitcast i8* %_80001 to i8**
  %_190022 = load i8*, i8** %_190049
  %_190050 = bitcast i8* %_190022 to { i8*, i32, i32, i8* }*
  %_190051 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_190050, i32 0, i32 2
  %_190023 = bitcast i32* %_190051 to i8*
  %_190052 = bitcast i8* %_190023 to i32*
  %_190024 = load i32, i32* %_190052
  %_190053 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_190054 = getelementptr i8*, i8** %_190053, i32 1698
  %_190025 = bitcast i8** %_190054 to i8*
  %_190055 = bitcast i8* %_190025 to i8**
  %_190056 = getelementptr i8*, i8** %_190055, i32 %_190024
  %_190026 = bitcast i8** %_190056 to i8*
  %_190057 = bitcast i8* %_190026 to i8**
  %_80004 = load i8*, i8** %_190057
  %_190058 = bitcast i8* %_80004 to i32 (i8*)*
  %_80005 = call i32 %_190058(i8* dereferenceable_or_null(8) %_80001)
  %_80007 = icmp eq i32 %_80002, %_80005
  br i1 %_80007, label %_100000.0, label %_110000.0
_100000.0:
  %_190059 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.util.HashMapD8entrySetL13java.util.SetEO" to i8*) to i8* (i8*)*
  %_100002 = call dereferenceable_or_null(8) i8* %_190059(i8* dereferenceable_or_null(8) %_1)
  %_100003 = call dereferenceable_or_null(8) i8* @"_SM19java.util.ScalaOps$D17ToJavaIterableOpsL18java.lang.IterableL18java.lang.IterableEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19java.util.ScalaOps$G8instance" to i8*), i8* dereferenceable_or_null(8) %_100002)
  %_100006 = call dereferenceable_or_null(8) i8* @"_SM37java.util.ScalaOps$ToJavaIterableOps$D18scalaOps$extensionL18java.lang.IterableL18java.lang.IterableEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM37java.util.ScalaOps$ToJavaIterableOps$G8instance" to i8*), i8* dereferenceable_or_null(8) %_100003)
  %_100009 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM31java.util.AbstractMap$$Lambda$7G4type" to i8*), i64 16)
  %_190060 = bitcast i8* %_100009 to { i8*, i8* }*
  %_190061 = getelementptr { i8*, i8* }, { i8*, i8* }* %_190060, i32 0, i32 1
  %_190028 = bitcast i8** %_190061 to i8*
  %_190062 = bitcast i8* %_190028 to i8**
  store i8* %_80001, i8** %_190062
  %_100011 = call i1 @"_SM35java.util.ScalaOps$JavaIterableOps$D16forall$extensionL18java.lang.IterableL15scala.Function1zEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35java.util.ScalaOps$JavaIterableOps$G8instance" to i8*), i8* dereferenceable_or_null(8) %_100006, i8* nonnull dereferenceable(16) %_100009)
  br label %_170000.0
_110000.0:
  br label %_170000.0
_170000.0:
  %_170001 = phi i1 [false, %_110000.0], [%_100011, %_100000.0]
  br label %_180000.0
_90000.0:
  br label %_190000.0
_190000.0:
  br label %_180000.0
_180000.0:
  %_180001 = phi i1 [false, %_190000.0], [%_170001, %_170000.0]
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_180001, %_180000.0], [true, %_40000.0]
  ret i1 %_60001
_190020.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_190012.0:
  %_190030 = phi i8* [%_2, %_190010.0]
  %_190031 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM13java.util.MapG4type" to i8*), %_190010.0]
  %_190063 = bitcast i8* %_190030 to i8**
  %_190032 = load i8*, i8** %_190063
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_190032, i8* %_190031)
  unreachable
}

define i32 @"_SM21java.util.AbstractMapD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20014 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.util.HashMapD8entrySetL13java.util.SetEO" to i8*) to i8* (i8*)*
  %_20002 = call dereferenceable_or_null(8) i8* %_20014(i8* dereferenceable_or_null(8) %_1)
  %_20003 = call dereferenceable_or_null(8) i8* @"_SM19java.util.ScalaOps$D17ToJavaIterableOpsL18java.lang.IterableL18java.lang.IterableEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM19java.util.ScalaOps$G8instance" to i8*), i8* dereferenceable_or_null(8) %_20002)
  %_20006 = call dereferenceable_or_null(8) i8* @"_SM37java.util.ScalaOps$ToJavaIterableOps$D18scalaOps$extensionL18java.lang.IterableL18java.lang.IterableEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM37java.util.ScalaOps$ToJavaIterableOps$G8instance" to i8*), i8* dereferenceable_or_null(8) %_20003)
  %_20010 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 0)
  %_20011 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM31java.util.AbstractMap$$Lambda$8G4type" to i8*), i64 8)
  %_20012 = call dereferenceable_or_null(8) i8* @"_SM35java.util.ScalaOps$JavaIterableOps$D18foldLeft$extensionL18java.lang.IterableL16java.lang.ObjectL15scala.Function2L16java.lang.ObjectEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35java.util.ScalaOps$JavaIterableOps$G8instance" to i8*), i8* dereferenceable_or_null(8) %_20006, i8* nonnull dereferenceable(16) %_20010, i8* nonnull dereferenceable(8) %_20011)
  %_20015 = bitcast i8* bitcast (i32 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D10unboxToIntL16java.lang.ObjectiEO" to i8*) to i32 (i8*, i8*)*
  %_20013 = call i32 %_20015(i8* null, i8* dereferenceable_or_null(8) %_20012)
  ret i32 %_20013
}

define dereferenceable_or_null(32) i8* @"_SM21java.util.AbstractMapD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30001 = call i32 @"_SM16java.lang.StringD6lengthiEO"(i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-251" to i8*))
  %_30003 = add i32 %_30001, 16
  %_30004 = call dereferenceable_or_null(8) i8* @"_SM36scala.scalanative.runtime.CharArray$D5allociL35scala.scalanative.runtime.CharArrayEO"(i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.CharArray$G8instance" to i8*), i32 %_30003)
  call nonnull dereferenceable(8) i8* @"_SM16java.lang.StringD8getCharsiiLAc_iuEO"(i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-251" to i8*), i32 0, i32 %_30001, i8* nonnull dereferenceable(8) %_30004, i32 0)
  %_200033 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.util.HashMapD8entrySetL13java.util.SetEO" to i8*) to i8* (i8*)*
  %_20005 = call dereferenceable_or_null(8) i8* %_200033(i8* dereferenceable_or_null(8) %_1)
  %_200034 = bitcast i8* bitcast (i8* (i8*)* @"_SM26java.util.HashMap$EntrySetD8iteratorL18java.util.IteratorEO" to i8*) to i8* (i8*)*
  %_20006 = call dereferenceable_or_null(8) i8* %_200034(i8* dereferenceable_or_null(8) %_20005)
  %_20007 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM23java.lang.StringBuilderG4type" to i8*), i64 24)
  %_200035 = bitcast i8* %_20007 to { i8*, i1, i32, i8* }*
  %_200036 = getelementptr { i8*, i1, i32, i8* }, { i8*, i1, i32, i8* }* %_200035, i32 0, i32 3
  %_200007 = bitcast i8** %_200036 to i8*
  %_200037 = bitcast i8* %_200007 to i8**
  store i8* %_30004, i8** %_200037
  %_200038 = bitcast i8* %_20007 to { i8*, i1, i32, i8* }*
  %_200039 = getelementptr { i8*, i1, i32, i8* }, { i8*, i1, i32, i8* }* %_200038, i32 0, i32 2
  %_200009 = bitcast i32* %_200039 to i8*
  %_200040 = bitcast i8* %_200009 to i32*
  store i32 %_30001, i32* %_200040
  br label %_150000.0
_150000.0:
  %_150001 = phi i1 [true, %_20000.0], [%_190001, %_200024.0]
  %_150002 = phi i8* [%_20007, %_20000.0], [%_150002, %_200024.0]
  %_200041 = bitcast i8* bitcast (i1 (i8*)* @"_SM41java.util.HashMap$AbstractHashMapIteratorD7hasNextzEO" to i8*) to i1 (i8*)*
  %_150003 = call i1 %_200041(i8* dereferenceable_or_null(8) %_20006)
  br i1 %_150003, label %_160000.0, label %_170000.0
_160000.0:
  %_200042 = bitcast i8* bitcast (i8* (i8*)* @"_SM41java.util.HashMap$AbstractHashMapIteratorD4nextL16java.lang.ObjectEO" to i8*) to i8* (i8*)*
  %_160001 = call dereferenceable_or_null(8) i8* %_200042(i8* dereferenceable_or_null(8) %_20006)
  %_200013 = icmp eq i8* %_160001, null
  br i1 %_200013, label %_200011.0, label %_200010.0
_200010.0:
  %_200043 = bitcast i8* %_160001 to i8**
  %_200014 = load i8*, i8** %_200043
  %_200044 = bitcast i8* %_200014 to { i8*, i32, i32, i8* }*
  %_200045 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_200044, i32 0, i32 1
  %_200015 = bitcast i32* %_200045 to i8*
  %_200046 = bitcast i8* %_200015 to i32*
  %_200016 = load i32, i32* %_200046
  %_200047 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_200048 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_200047, i32 0, i32 %_200016, i32 18
  %_200017 = bitcast i1* %_200048 to i8*
  %_200049 = bitcast i8* %_200017 to i1*
  %_200018 = load i1, i1* %_200049
  br i1 %_200018, label %_200011.0, label %_200012.0
_200011.0:
  %_160002 = bitcast i8* %_160001 to i8*
  br i1 %_150001, label %_180000.0, label %_200000.0
_180000.0:
  br label %_190000.0
_200000.0:
  %_200003 = call dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.StringL23java.lang.StringBuilderEO"(i8* nonnull dereferenceable(24) %_150002, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-253" to i8*))
  br label %_190000.0
_190000.0:
  %_190001 = phi i1 [%_150001, %_200000.0], [false, %_180000.0]
  %_190002 = phi i8* [%_200003, %_200000.0], [bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), %_180000.0]
  %_200050 = bitcast i8* bitcast (i8* (i8*)* @"_SM22java.util.HashMap$NodeD6getKeyL16java.lang.ObjectEO" to i8*) to i8* (i8*)*
  %_190003 = call dereferenceable_or_null(8) i8* %_200050(i8* dereferenceable_or_null(8) %_160002)
  %_200021 = icmp ne i8* %_190003, null
  br i1 %_200021, label %_200019.0, label %_200020.0
_200019.0:
  %_200051 = bitcast i8* %_190003 to i8**
  %_200022 = load i8*, i8** %_200051
  %_200052 = bitcast i8* %_200022 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_200053 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_200052, i32 0, i32 4, i32 1
  %_200023 = bitcast i8** %_200053 to i8*
  %_200054 = bitcast i8* %_200023 to i8**
  %_190005 = load i8*, i8** %_200054
  %_200055 = bitcast i8* %_190005 to i8* (i8*)*
  %_190006 = call dereferenceable_or_null(32) i8* %_200055(i8* dereferenceable_or_null(8) %_190003)
  %_190007 = call dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.StringL23java.lang.StringBuilderEO"(i8* nonnull dereferenceable(24) %_150002, i8* dereferenceable_or_null(32) %_190006)
  %_190010 = call dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.StringL23java.lang.StringBuilderEO"(i8* dereferenceable_or_null(24) %_190007, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-255" to i8*))
  %_200056 = bitcast i8* bitcast (i8* (i8*)* @"_SM22java.util.HashMap$NodeD8getValueL16java.lang.ObjectEO" to i8*) to i8* (i8*)*
  %_190011 = call dereferenceable_or_null(8) i8* %_200056(i8* dereferenceable_or_null(8) %_160002)
  %_200025 = icmp ne i8* %_190011, null
  br i1 %_200025, label %_200024.0, label %_200020.0
_200024.0:
  %_200057 = bitcast i8* %_190011 to i8**
  %_200026 = load i8*, i8** %_200057
  %_200058 = bitcast i8* %_200026 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_200059 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_200058, i32 0, i32 4, i32 1
  %_200027 = bitcast i8** %_200059 to i8*
  %_200060 = bitcast i8* %_200027 to i8**
  %_190013 = load i8*, i8** %_200060
  %_200061 = bitcast i8* %_190013 to i8* (i8*)*
  %_190014 = call dereferenceable_or_null(32) i8* %_200061(i8* dereferenceable_or_null(8) %_190011)
  %_190015 = call dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.StringL23java.lang.StringBuilderEO"(i8* dereferenceable_or_null(24) %_190010, i8* dereferenceable_or_null(32) %_190014)
  br label %_150000.0
_170000.0:
  %_170003 = call dereferenceable_or_null(24) i8* @"_SM23java.lang.StringBuilderD6appendL16java.lang.StringL23java.lang.StringBuilderEO"(i8* nonnull dereferenceable(24) %_150002, i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-257" to i8*))
  %_170004 = call dereferenceable_or_null(32) i8* @"_SM23java.lang.StringBuilderD8toStringL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_170003)
  ret i8* %_170004
_200020.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_200012.0:
  %_200029 = phi i8* [%_160001, %_200010.0]
  %_200030 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM19java.util.Map$EntryG4type" to i8*), %_200010.0]
  %_200062 = bitcast i8* %_200029 to i8**
  %_200031 = load i8*, i8** %_200062
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_200031, i8* %_200030)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM21scala.runtime.Arrays$D15newGenericArrayiL22scala.reflect.ClassTagL16java.lang.ObjectEO"(i8* %_1, i32 %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40006 = icmp ne i8* %_3, null
  br i1 %_40006, label %_40004.0, label %_40005.0
_40004.0:
  %_40013 = bitcast i8* %_3 to i8**
  %_40007 = load i8*, i8** %_40013
  %_40014 = bitcast i8* %_40007 to { i8*, i32, i32, i8* }*
  %_40015 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40014, i32 0, i32 2
  %_40008 = bitcast i32* %_40015 to i8*
  %_40016 = bitcast i8* %_40008 to i32*
  %_40009 = load i32, i32* %_40016
  %_40017 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_40018 = getelementptr i8*, i8** %_40017, i32 1988
  %_40010 = bitcast i8** %_40018 to i8*
  %_40019 = bitcast i8* %_40010 to i8**
  %_40020 = getelementptr i8*, i8** %_40019, i32 %_40009
  %_40011 = bitcast i8** %_40020 to i8*
  %_40021 = bitcast i8* %_40011 to i8**
  %_40002 = load i8*, i8** %_40021
  %_40022 = bitcast i8* %_40002 to i8* (i8*, i32)*
  %_40003 = call dereferenceable_or_null(8) i8* %_40022(i8* dereferenceable_or_null(8) %_3, i32 %_2)
  ret i8* %_40003
_40005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM21scala.runtime.Arrays$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(16) i8* @"_SM21scala.runtime.IntRef$D6createiL20scala.runtime.IntRefEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM20scala.runtime.IntRefG4type" to i8*), i64 16)
  %_30006 = bitcast i8* %_30002 to { i8*, i32 }*
  %_30007 = getelementptr { i8*, i32 }, { i8*, i32 }* %_30006, i32 0, i32 1
  %_30005 = bitcast i32* %_30007 to i8*
  %_30008 = bitcast i8* %_30005 to i32*
  store i32 %_2, i32* %_30008
  ret i8* %_30002
}

define nonnull dereferenceable(8) i8* @"_SM21scala.runtime.IntRef$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(24) i8* @"_SM23java.nio.GenHeapBuffer$D12generic_wrapL16java.lang.ObjectiiiizL36java.nio.GenHeapBuffer$NewHeapBufferL15java.nio.BufferEO"(i8* %_1, i8* %_2, i32 %_3, i32 %_4, i32 %_5, i32 %_6, i1 %_7, i8* %_8) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_90000.0:
  %_90002 = icmp slt i32 %_4, 0
  br i1 %_90002, label %_100000.0, label %_110000.0
_110000.0:
  br label %_220000.0
_220000.0:
  %_220002 = icmp slt i32 %_3, 0
  br i1 %_220002, label %_230000.0, label %_240000.0
_230000.0:
  br label %_250000.0
_240000.0:
  %_240003 = call i32 @"_SM27scala.runtime.ScalaRunTime$D12array_lengthL16java.lang.ObjectiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(8) %_2)
  %_240005 = add i32 %_3, %_4
  %_240006 = icmp sgt i32 %_240005, %_240003
  br label %_250000.0
_250000.0:
  %_250001 = phi i1 [%_240006, %_240000.0], [true, %_230000.0]
  br i1 %_250001, label %_260000.0, label %_270000.0
_270000.0:
  br label %_390000.0
_390000.0:
  %_390003 = icmp slt i32 %_5, 0
  br i1 %_390003, label %_400000.0, label %_410000.0
_400000.0:
  br label %_420000.0
_410000.0:
  %_410002 = icmp slt i32 %_6, 0
  br label %_420000.0
_420000.0:
  %_420001 = phi i1 [%_410002, %_410000.0], [true, %_400000.0]
  br i1 %_420001, label %_430000.0, label %_440000.0
_430000.0:
  %_430001 = add i32 %_5, %_6
  br label %_450000.0
_440000.0:
  %_440002 = add i32 %_5, %_6
  %_440003 = icmp sgt i32 %_440002, %_4
  br label %_450000.0
_450000.0:
  %_450001 = phi i32 [%_440002, %_440000.0], [%_430001, %_430000.0]
  %_450002 = phi i1 [%_440003, %_440000.0], [true, %_430000.0]
  br i1 %_450002, label %_460000.0, label %_470000.0
_470000.0:
  br label %_590000.0
_590000.0:
  %_590006 = icmp ne i8* %_8, null
  br i1 %_590006, label %_590004.0, label %_590005.0
_590004.0:
  %_590028 = bitcast i8* %_8 to i8**
  %_590007 = load i8*, i8** %_590028
  %_590029 = bitcast i8* %_590007 to { i8*, i32, i32, i8* }*
  %_590030 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_590029, i32 0, i32 2
  %_590008 = bitcast i32* %_590030 to i8*
  %_590031 = bitcast i8* %_590008 to i32*
  %_590009 = load i32, i32* %_590031
  %_590032 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_590033 = getelementptr i8*, i8** %_590032, i32 2887
  %_590010 = bitcast i8** %_590033 to i8*
  %_590034 = bitcast i8* %_590010 to i8**
  %_590035 = getelementptr i8*, i8** %_590034, i32 %_590009
  %_590011 = bitcast i8** %_590035 to i8*
  %_590036 = bitcast i8* %_590011 to i8**
  %_590002 = load i8*, i8** %_590036
  %_590037 = bitcast i8* %_590002 to i8* (i8*, i32, i8*, i32, i32, i32, i1)*
  %_590003 = call dereferenceable_or_null(24) i8* %_590037(i8* dereferenceable_or_null(8) %_8, i32 %_4, i8* dereferenceable_or_null(8) %_2, i32 %_3, i32 %_5, i32 %_450001, i1 %_7)
  ret i8* %_590003
_100000.0:
  br label %_200000.0
_200000.0:
  %_200001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM34java.lang.IllegalArgumentExceptionG4type" to i8*), i64 40)
  %_590038 = bitcast i8* %_200001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_590039 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_590038, i32 0, i32 5
  %_590013 = bitcast i1* %_590039 to i8*
  %_590040 = bitcast i8* %_590013 to i1*
  store i1 true, i1* %_590040
  %_590041 = bitcast i8* %_200001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_590042 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_590041, i32 0, i32 4
  %_590015 = bitcast i1* %_590042 to i8*
  %_590043 = bitcast i8* %_590015 to i1*
  store i1 true, i1* %_590043
  %_200004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_200001)
  br label %_210000.0
_210000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_200001)
  unreachable
_260000.0:
  br label %_370000.0
_370000.0:
  %_370001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM35java.lang.IndexOutOfBoundsExceptionG4type" to i8*), i64 40)
  %_590044 = bitcast i8* %_370001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_590045 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_590044, i32 0, i32 5
  %_590018 = bitcast i1* %_590045 to i8*
  %_590046 = bitcast i8* %_590018 to i1*
  store i1 true, i1* %_590046
  %_590047 = bitcast i8* %_370001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_590048 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_590047, i32 0, i32 4
  %_590020 = bitcast i1* %_590048 to i8*
  %_590049 = bitcast i8* %_590020 to i1*
  store i1 true, i1* %_590049
  %_370004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_370001)
  br label %_380000.0
_380000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_370001)
  unreachable
_460000.0:
  br label %_570000.0
_570000.0:
  %_570001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM35java.lang.IndexOutOfBoundsExceptionG4type" to i8*), i64 40)
  %_590050 = bitcast i8* %_570001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_590051 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_590050, i32 0, i32 5
  %_590023 = bitcast i1* %_590051 to i8*
  %_590052 = bitcast i8* %_590023 to i1*
  store i1 true, i1* %_590052
  %_590053 = bitcast i8* %_570001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_590054 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_590053, i32 0, i32 4
  %_590025 = bitcast i1* %_590054 to i8*
  %_590055 = bitcast i8* %_590025 to i1*
  store i1 true, i1* %_590055
  %_570004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_570001)
  br label %_580000.0
_580000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_570001)
  unreachable
_590005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM23java.nio.GenHeapBuffer$D22generic_load$extensionL15java.nio.BufferiL16java.lang.ObjectEO"(i8* %_1, i8* %_2, i32 %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40013 = icmp ne i8* %_2, null
  br i1 %_40013, label %_40011.0, label %_40012.0
_40011.0:
  %_40021 = bitcast i8* %_2 to i8**
  %_40014 = load i8*, i8** %_40021
  %_40022 = bitcast i8* %_40014 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_40023 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_40022, i32 0, i32 4, i32 5
  %_40015 = bitcast i8** %_40023 to i8*
  %_40024 = bitcast i8* %_40015 to i8**
  %_40003 = load i8*, i8** %_40024
  %_40025 = bitcast i8* %_40003 to i8* (i8*)*
  %_40004 = call dereferenceable_or_null(8) i8* %_40025(i8* dereferenceable_or_null(24) %_2)
  %_40017 = icmp ne i8* %_2, null
  br i1 %_40017, label %_40016.0, label %_40012.0
_40016.0:
  %_40026 = bitcast i8* %_2 to i8**
  %_40018 = load i8*, i8** %_40026
  %_40027 = bitcast i8* %_40018 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_40028 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_40027, i32 0, i32 4, i32 6
  %_40019 = bitcast i8** %_40028 to i8*
  %_40029 = bitcast i8* %_40019 to i8**
  %_40006 = load i8*, i8** %_40029
  %_40030 = bitcast i8* %_40006 to i32 (i8*)*
  %_40007 = call i32 %_40030(i8* dereferenceable_or_null(24) %_2)
  %_40009 = add i32 %_40007, %_3
  %_40010 = call dereferenceable_or_null(8) i8* @"_SM27scala.runtime.ScalaRunTime$D11array_applyL16java.lang.ObjectiL16java.lang.ObjectEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(8) %_40004, i32 %_40009)
  ret i8* %_40010
_40012.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM23java.nio.GenHeapBuffer$D22generic_load$extensionL15java.nio.BufferiL16java.lang.ObjectiiuEO"(i8* %_1, i8* %_2, i32 %_3, i8* %_4, i32 %_5, i32 %_6) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_70000.0:
  %_70001 = call dereferenceable_or_null(56) i8* @"_SM17java.lang.System$G4load"()
  %_70013 = icmp ne i8* %_2, null
  br i1 %_70013, label %_70011.0, label %_70012.0
_70011.0:
  %_70022 = bitcast i8* %_2 to i8**
  %_70014 = load i8*, i8** %_70022
  %_70023 = bitcast i8* %_70014 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_70024 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_70023, i32 0, i32 4, i32 5
  %_70015 = bitcast i8** %_70024 to i8*
  %_70025 = bitcast i8* %_70015 to i8**
  %_70003 = load i8*, i8** %_70025
  %_70026 = bitcast i8* %_70003 to i8* (i8*)*
  %_70004 = call dereferenceable_or_null(8) i8* %_70026(i8* dereferenceable_or_null(24) %_2)
  %_70017 = icmp ne i8* %_2, null
  br i1 %_70017, label %_70016.0, label %_70012.0
_70016.0:
  %_70027 = bitcast i8* %_2 to i8**
  %_70018 = load i8*, i8** %_70027
  %_70028 = bitcast i8* %_70018 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_70029 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_70028, i32 0, i32 4, i32 6
  %_70019 = bitcast i8** %_70029 to i8*
  %_70030 = bitcast i8* %_70019 to i8**
  %_70006 = load i8*, i8** %_70030
  %_70031 = bitcast i8* %_70006 to i32 (i8*)*
  %_70007 = call i32 %_70031(i8* dereferenceable_or_null(24) %_2)
  %_70009 = add i32 %_70007, %_3
  call nonnull dereferenceable(8) i8* @"_SM17java.lang.System$D9arraycopyL16java.lang.ObjectiL16java.lang.ObjectiiuEO"(i8* nonnull dereferenceable(56) %_70001, i8* dereferenceable_or_null(8) %_70004, i32 %_70009, i8* dereferenceable_or_null(8) %_4, i32 %_5, i32 %_6)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_70012.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM23java.nio.GenHeapBuffer$D23generic_store$extensionL15java.nio.BufferiL16java.lang.ObjectiiuEO"(i8* %_1, i8* %_2, i32 %_3, i8* %_4, i32 %_5, i32 %_6) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_70000.0:
  %_70001 = call dereferenceable_or_null(56) i8* @"_SM17java.lang.System$G4load"()
  %_70013 = icmp ne i8* %_2, null
  br i1 %_70013, label %_70011.0, label %_70012.0
_70011.0:
  %_70022 = bitcast i8* %_2 to i8**
  %_70014 = load i8*, i8** %_70022
  %_70023 = bitcast i8* %_70014 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_70024 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_70023, i32 0, i32 4, i32 5
  %_70015 = bitcast i8** %_70024 to i8*
  %_70025 = bitcast i8* %_70015 to i8**
  %_70003 = load i8*, i8** %_70025
  %_70026 = bitcast i8* %_70003 to i8* (i8*)*
  %_70004 = call dereferenceable_or_null(8) i8* %_70026(i8* dereferenceable_or_null(24) %_2)
  %_70017 = icmp ne i8* %_2, null
  br i1 %_70017, label %_70016.0, label %_70012.0
_70016.0:
  %_70027 = bitcast i8* %_2 to i8**
  %_70018 = load i8*, i8** %_70027
  %_70028 = bitcast i8* %_70018 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_70029 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_70028, i32 0, i32 4, i32 6
  %_70019 = bitcast i8** %_70029 to i8*
  %_70030 = bitcast i8* %_70019 to i8**
  %_70006 = load i8*, i8** %_70030
  %_70031 = bitcast i8* %_70006 to i32 (i8*)*
  %_70007 = call i32 %_70031(i8* dereferenceable_or_null(24) %_2)
  %_70009 = add i32 %_70007, %_3
  call nonnull dereferenceable(8) i8* @"_SM17java.lang.System$D9arraycopyL16java.lang.ObjectiL16java.lang.ObjectiiuEO"(i8* nonnull dereferenceable(56) %_70001, i8* dereferenceable_or_null(8) %_4, i32 %_5, i8* dereferenceable_or_null(8) %_70004, i32 %_70009, i32 %_6)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_70012.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM23java.nio.GenHeapBuffer$D23generic_store$extensionL15java.nio.BufferiL16java.lang.ObjectuEO"(i8* %_1, i8* %_2, i32 %_3, i8* %_4) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_50013 = icmp ne i8* %_2, null
  br i1 %_50013, label %_50011.0, label %_50012.0
_50011.0:
  %_50022 = bitcast i8* %_2 to i8**
  %_50014 = load i8*, i8** %_50022
  %_50023 = bitcast i8* %_50014 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_50024 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_50023, i32 0, i32 4, i32 5
  %_50015 = bitcast i8** %_50024 to i8*
  %_50025 = bitcast i8* %_50015 to i8**
  %_50003 = load i8*, i8** %_50025
  %_50026 = bitcast i8* %_50003 to i8* (i8*)*
  %_50004 = call dereferenceable_or_null(8) i8* %_50026(i8* dereferenceable_or_null(24) %_2)
  %_50017 = icmp ne i8* %_2, null
  br i1 %_50017, label %_50016.0, label %_50012.0
_50016.0:
  %_50027 = bitcast i8* %_2 to i8**
  %_50018 = load i8*, i8** %_50027
  %_50028 = bitcast i8* %_50018 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }*
  %_50029 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [12 x i8*] }* %_50028, i32 0, i32 4, i32 6
  %_50019 = bitcast i8** %_50029 to i8*
  %_50030 = bitcast i8* %_50019 to i8**
  %_50006 = load i8*, i8** %_50030
  %_50031 = bitcast i8* %_50006 to i32 (i8*)*
  %_50007 = call i32 %_50031(i8* dereferenceable_or_null(24) %_2)
  %_50009 = add i32 %_50007, %_3
  call nonnull dereferenceable(8) i8* @"_SM27scala.runtime.ScalaRunTime$D12array_updateL16java.lang.ObjectiL16java.lang.ObjectuEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(8) %_50004, i32 %_50009, i8* dereferenceable_or_null(8) %_4)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_50012.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM23java.nio.GenHeapBuffer$D5applyL15java.nio.BufferL15java.nio.BufferEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i8* %_2
}

define nonnull dereferenceable(8) i8* @"_SM23java.nio.GenHeapBuffer$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(8) i8* @"_SM23scala.collection.MapOpsD12foreachEntryL15scala.Function2uEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_90003 = icmp ne i8* %_1, null
  br i1 %_90003, label %_90001.0, label %_90002.0
_90001.0:
  %_90047 = bitcast i8* %_1 to i8**
  %_90004 = load i8*, i8** %_90047
  %_90048 = bitcast i8* %_90004 to { i8*, i32, i32, i8* }*
  %_90049 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90048, i32 0, i32 2
  %_90005 = bitcast i32* %_90049 to i8*
  %_90050 = bitcast i8* %_90005 to i32*
  %_90006 = load i32, i32* %_90050
  %_90051 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_90052 = getelementptr i8*, i8** %_90051, i32 446
  %_90007 = bitcast i8** %_90052 to i8*
  %_90053 = bitcast i8* %_90007 to i8**
  %_90054 = getelementptr i8*, i8** %_90053, i32 %_90006
  %_90008 = bitcast i8** %_90054 to i8*
  %_90055 = bitcast i8* %_90008 to i8**
  %_30002 = load i8*, i8** %_90055
  %_90056 = bitcast i8* %_30002 to i8* (i8*)*
  %_30003 = call dereferenceable_or_null(8) i8* %_90056(i8* dereferenceable_or_null(8) %_1)
  br label %_40000.0
_40000.0:
  %_90010 = icmp ne i8* %_30003, null
  br i1 %_90010, label %_90009.0, label %_90002.0
_90009.0:
  %_90057 = bitcast i8* %_30003 to i8**
  %_90011 = load i8*, i8** %_90057
  %_90058 = bitcast i8* %_90011 to { i8*, i32, i32, i8* }*
  %_90059 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90058, i32 0, i32 2
  %_90012 = bitcast i32* %_90059 to i8*
  %_90060 = bitcast i8* %_90012 to i32*
  %_90013 = load i32, i32* %_90060
  %_90061 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_90062 = getelementptr i8*, i8** %_90061, i32 4359
  %_90014 = bitcast i8** %_90062 to i8*
  %_90063 = bitcast i8* %_90014 to i8**
  %_90064 = getelementptr i8*, i8** %_90063, i32 %_90013
  %_90015 = bitcast i8** %_90064 to i8*
  %_90065 = bitcast i8* %_90015 to i8**
  %_40002 = load i8*, i8** %_90065
  %_90066 = bitcast i8* %_40002 to i1 (i8*)*
  %_40003 = call i1 %_90066(i8* dereferenceable_or_null(8) %_30003)
  br i1 %_40003, label %_50000.0, label %_60000.0
_50000.0:
  %_90017 = icmp ne i8* %_30003, null
  br i1 %_90017, label %_90016.0, label %_90002.0
_90016.0:
  %_90067 = bitcast i8* %_30003 to i8**
  %_90018 = load i8*, i8** %_90067
  %_90068 = bitcast i8* %_90018 to { i8*, i32, i32, i8* }*
  %_90069 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90068, i32 0, i32 2
  %_90019 = bitcast i32* %_90069 to i8*
  %_90070 = bitcast i8* %_90019 to i32*
  %_90020 = load i32, i32* %_90070
  %_90071 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_90072 = getelementptr i8*, i8** %_90071, i32 4219
  %_90021 = bitcast i8** %_90072 to i8*
  %_90073 = bitcast i8* %_90021 to i8**
  %_90074 = getelementptr i8*, i8** %_90073, i32 %_90020
  %_90022 = bitcast i8** %_90074 to i8*
  %_90075 = bitcast i8* %_90022 to i8**
  %_50002 = load i8*, i8** %_90075
  %_90076 = bitcast i8* %_50002 to i8* (i8*)*
  %_50003 = call dereferenceable_or_null(8) i8* %_90076(i8* dereferenceable_or_null(8) %_30003)
  %_90026 = icmp eq i8* %_50003, null
  br i1 %_90026, label %_90024.0, label %_90023.0
_90023.0:
  %_90077 = bitcast i8* %_50003 to i8**
  %_90027 = load i8*, i8** %_90077
  %_90028 = icmp eq i8* %_90027, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM12scala.Tuple2G4type" to i8*)
  br i1 %_90028, label %_90024.0, label %_90025.0
_90024.0:
  %_50004 = bitcast i8* %_50003 to i8*
  %_90030 = icmp ne i8* %_50004, null
  br i1 %_90030, label %_90029.0, label %_90002.0
_90029.0:
  %_90078 = bitcast i8* %_50004 to { i8*, i8*, i8* }*
  %_90079 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_90078, i32 0, i32 2
  %_90031 = bitcast i8** %_90079 to i8*
  %_90080 = bitcast i8* %_90031 to i8**
  %_70001 = load i8*, i8** %_90080, !dereferenceable_or_null !{i64 8}
  %_90033 = icmp ne i8* %_50004, null
  br i1 %_90033, label %_90032.0, label %_90002.0
_90032.0:
  %_90081 = bitcast i8* %_50004 to { i8*, i8*, i8* }*
  %_90082 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_90081, i32 0, i32 1
  %_90034 = bitcast i8** %_90082 to i8*
  %_90083 = bitcast i8* %_90034 to i8**
  %_80001 = load i8*, i8** %_90083, !dereferenceable_or_null !{i64 8}
  %_90036 = icmp ne i8* %_2, null
  br i1 %_90036, label %_90035.0, label %_90002.0
_90035.0:
  %_90084 = bitcast i8* %_2 to i8**
  %_90037 = load i8*, i8** %_90084
  %_90085 = bitcast i8* %_90037 to { i8*, i32, i32, i8* }*
  %_90086 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90085, i32 0, i32 2
  %_90038 = bitcast i32* %_90086 to i8*
  %_90087 = bitcast i8* %_90038 to i32*
  %_90039 = load i32, i32* %_90087
  %_90088 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_90089 = getelementptr i8*, i8** %_90088, i32 2146
  %_90040 = bitcast i8** %_90089 to i8*
  %_90090 = bitcast i8* %_90040 to i8**
  %_90091 = getelementptr i8*, i8** %_90090, i32 %_90039
  %_90041 = bitcast i8** %_90091 to i8*
  %_90092 = bitcast i8* %_90041 to i8**
  %_50006 = load i8*, i8** %_90092
  %_90093 = bitcast i8* %_50006 to i8* (i8*, i8*, i8*)*
  %_50007 = call dereferenceable_or_null(8) i8* %_90093(i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_70001, i8* dereferenceable_or_null(8) %_80001)
  br label %_40000.0
_60000.0:
  br label %_90000.0
_90000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_90002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_90025.0:
  %_90043 = phi i8* [%_50003, %_90023.0]
  %_90044 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM12scala.Tuple2G4type" to i8*), %_90023.0]
  %_90094 = bitcast i8* %_90043 to i8**
  %_90045 = load i8*, i8** %_90094
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_90045, i8* %_90044)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM23scala.collection.MapOpsD20$anonfun$addString$1L12scala.Tuple2L16java.lang.StringEPT23scala.collection.MapOps"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_40002 = icmp ne i8* %_2, null
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_380003 = icmp ne i8* %_2, null
  br i1 %_380003, label %_380001.0, label %_380002.0
_380001.0:
  %_380024 = bitcast i8* %_2 to { i8*, i8*, i8* }*
  %_380025 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_380024, i32 0, i32 2
  %_380004 = bitcast i8** %_380025 to i8*
  %_380026 = bitcast i8* %_380004 to i8**
  %_70001 = load i8*, i8** %_380026, !dereferenceable_or_null !{i64 8}
  %_380006 = icmp ne i8* %_2, null
  br i1 %_380006, label %_380005.0, label %_380002.0
_380005.0:
  %_380027 = bitcast i8* %_2 to { i8*, i8*, i8* }*
  %_380028 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_380027, i32 0, i32 1
  %_380007 = bitcast i8** %_380028 to i8*
  %_380029 = bitcast i8* %_380007 to i8**
  %_80001 = load i8*, i8** %_380029, !dereferenceable_or_null !{i64 8}
  %_50004 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-195" to i8*), null
  br i1 %_50004, label %_90000.0, label %_100000.0
_90000.0:
  br label %_110000.0
_100000.0:
  br label %_110000.0
_110000.0:
  %_110001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-195" to i8*), %_100000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_90000.0]
  %_110003 = icmp eq i8* %_70001, null
  br i1 %_110003, label %_120000.0, label %_130000.0
_120000.0:
  br label %_140000.0
_130000.0:
  %_380009 = icmp ne i8* %_70001, null
  br i1 %_380009, label %_380008.0, label %_380002.0
_380008.0:
  %_380030 = bitcast i8* %_70001 to i8**
  %_380010 = load i8*, i8** %_380030
  %_380031 = bitcast i8* %_380010 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_380032 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_380031, i32 0, i32 4, i32 1
  %_380011 = bitcast i8** %_380032 to i8*
  %_380033 = bitcast i8* %_380011 to i8**
  %_130002 = load i8*, i8** %_380033
  %_380034 = bitcast i8* %_130002 to i8* (i8*)*
  %_130003 = call dereferenceable_or_null(32) i8* %_380034(i8* dereferenceable_or_null(8) %_70001)
  br label %_140000.0
_140000.0:
  %_140001 = phi i8* [%_130003, %_380008.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_120000.0]
  %_380035 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_140002 = call dereferenceable_or_null(32) i8* %_380035(i8* nonnull dereferenceable(32) %_110001, i8* dereferenceable_or_null(32) %_140001)
  %_140004 = icmp eq i8* %_140002, null
  br i1 %_140004, label %_150000.0, label %_160000.0
_150000.0:
  br label %_170000.0
_160000.0:
  br label %_170000.0
_170000.0:
  %_170001 = phi i8* [%_140002, %_160000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_150000.0]
  %_170005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-261" to i8*), null
  br i1 %_170005, label %_180000.0, label %_190000.0
_180000.0:
  br label %_200000.0
_190000.0:
  br label %_200000.0
_200000.0:
  %_200001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-261" to i8*), %_190000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_180000.0]
  %_380036 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_200002 = call dereferenceable_or_null(32) i8* %_380036(i8* dereferenceable_or_null(32) %_170001, i8* nonnull dereferenceable(32) %_200001)
  %_200004 = icmp eq i8* %_200002, null
  br i1 %_200004, label %_210000.0, label %_220000.0
_210000.0:
  br label %_230000.0
_220000.0:
  br label %_230000.0
_230000.0:
  %_230001 = phi i8* [%_200002, %_220000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_210000.0]
  %_230003 = icmp eq i8* %_80001, null
  br i1 %_230003, label %_240000.0, label %_250000.0
_240000.0:
  br label %_260000.0
_250000.0:
  %_380013 = icmp ne i8* %_80001, null
  br i1 %_380013, label %_380012.0, label %_380002.0
_380012.0:
  %_380037 = bitcast i8* %_80001 to i8**
  %_380014 = load i8*, i8** %_380037
  %_380038 = bitcast i8* %_380014 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_380039 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_380038, i32 0, i32 4, i32 1
  %_380015 = bitcast i8** %_380039 to i8*
  %_380040 = bitcast i8* %_380015 to i8**
  %_250002 = load i8*, i8** %_380040
  %_380041 = bitcast i8* %_250002 to i8* (i8*)*
  %_250003 = call dereferenceable_or_null(32) i8* %_380041(i8* dereferenceable_or_null(8) %_80001)
  br label %_260000.0
_260000.0:
  %_260001 = phi i8* [%_250003, %_380012.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_240000.0]
  %_380042 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_260002 = call dereferenceable_or_null(32) i8* %_380042(i8* dereferenceable_or_null(32) %_230001, i8* dereferenceable_or_null(32) %_260001)
  br label %_270000.0
_60000.0:
  br label %_280000.0
_270000.0:
  ret i8* %_260002
_280000.0:
  br label %_370000.0
_370000.0:
  %_370001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM16scala.MatchErrorG4type" to i8*), i64 64)
  %_380043 = bitcast i8* %_370001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_380044 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_380043, i32 0, i32 5
  %_380017 = bitcast i1* %_380044 to i8*
  %_380045 = bitcast i8* %_380017 to i1*
  store i1 true, i1* %_380045
  %_380046 = bitcast i8* %_370001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_380047 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_380046, i32 0, i32 4
  %_380019 = bitcast i1* %_380047 to i8*
  %_380048 = bitcast i8* %_380019 to i1*
  store i1 true, i1* %_380048
  %_380049 = bitcast i8* %_370001 to { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }*
  %_380050 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }, { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }* %_380049, i32 0, i32 6
  %_380021 = bitcast i8** %_380050 to i8*
  %_380051 = bitcast i8* %_380021 to i8**
  store i8* %_2, i8** %_380051
  %_370005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(64) %_370001)
  br label %_380000.0
_380000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(64) %_370001)
  unreachable
_380002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM23scala.collection.MapOpsD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i8* @"_SM23scala.collection.MapOpsD7defaultL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-263" to i8*), null
  br i1 %_30005, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  br label %_60000.0
_60000.0:
  %_60001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-263" to i8*), %_50000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_40000.0]
  %_60003 = icmp eq i8* %_2, null
  br i1 %_60003, label %_70000.0, label %_80000.0
_70000.0:
  br label %_90000.0
_80000.0:
  %_190003 = icmp ne i8* %_2, null
  br i1 %_190003, label %_190001.0, label %_190002.0
_190001.0:
  %_190014 = bitcast i8* %_2 to i8**
  %_190004 = load i8*, i8** %_190014
  %_190015 = bitcast i8* %_190004 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_190016 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_190015, i32 0, i32 4, i32 1
  %_190005 = bitcast i8** %_190016 to i8*
  %_190017 = bitcast i8* %_190005 to i8**
  %_80002 = load i8*, i8** %_190017
  %_190018 = bitcast i8* %_80002 to i8* (i8*)*
  %_80003 = call dereferenceable_or_null(32) i8* %_190018(i8* dereferenceable_or_null(8) %_2)
  br label %_90000.0
_90000.0:
  %_90001 = phi i8* [%_80003, %_190001.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_70000.0]
  %_190019 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_90002 = call dereferenceable_or_null(32) i8* %_190019(i8* nonnull dereferenceable(32) %_60001, i8* dereferenceable_or_null(32) %_90001)
  br label %_180000.0
_180000.0:
  %_180001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.util.NoSuchElementExceptionG4type" to i8*), i64 40)
  %_190020 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_190021 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_190020, i32 0, i32 5
  %_190007 = bitcast i1* %_190021 to i8*
  %_190022 = bitcast i8* %_190007 to i1*
  store i1 true, i1* %_190022
  %_190023 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_190024 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_190023, i32 0, i32 4
  %_190009 = bitcast i1* %_190024 to i8*
  %_190025 = bitcast i8* %_190009 to i1*
  store i1 true, i1* %_190025
  %_190026 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_190027 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_190026, i32 0, i32 3
  %_190011 = bitcast i8** %_190027 to i8*
  %_190028 = bitcast i8* %_190011 to i8**
  store i8* %_90002, i8** %_190028
  %_180005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_180001)
  br label %_190000.0
_190000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_180001)
  unreachable
_190002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM23scala.collection.MapOpsD9addStringL38scala.collection.mutable.StringBuilderL16java.lang.StringL16java.lang.StringL16java.lang.StringL38scala.collection.mutable.StringBuilderEO"(i8* %_1, i8* %_2, i8* %_3, i8* %_4, i8* %_5) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  %_60015 = icmp ne i8* %_1, null
  br i1 %_60015, label %_60013.0, label %_60014.0
_60013.0:
  %_60038 = bitcast i8* %_1 to i8**
  %_60016 = load i8*, i8** %_60038
  %_60039 = bitcast i8* %_60016 to { i8*, i32, i32, i8* }*
  %_60040 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60039, i32 0, i32 2
  %_60017 = bitcast i32* %_60040 to i8*
  %_60041 = bitcast i8* %_60017 to i32*
  %_60018 = load i32, i32* %_60041
  %_60042 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_60043 = getelementptr i8*, i8** %_60042, i32 446
  %_60019 = bitcast i8** %_60043 to i8*
  %_60044 = bitcast i8* %_60019 to i8**
  %_60045 = getelementptr i8*, i8** %_60044, i32 %_60018
  %_60020 = bitcast i8** %_60045 to i8*
  %_60046 = bitcast i8* %_60020 to i8**
  %_60002 = load i8*, i8** %_60046
  %_60047 = bitcast i8* %_60002 to i8* (i8*)*
  %_60003 = call dereferenceable_or_null(8) i8* %_60047(i8* dereferenceable_or_null(8) %_1)
  %_60022 = icmp ne i8* %_60003, null
  br i1 %_60022, label %_60021.0, label %_60014.0
_60021.0:
  %_60048 = bitcast i8* %_60003 to i8**
  %_60023 = load i8*, i8** %_60048
  %_60049 = bitcast i8* %_60023 to { i8*, i32, i32, i8* }*
  %_60050 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60049, i32 0, i32 2
  %_60024 = bitcast i32* %_60050 to i8*
  %_60051 = bitcast i8* %_60024 to i32*
  %_60025 = load i32, i32* %_60051
  %_60052 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_60053 = getelementptr i8*, i8** %_60052, i32 4079
  %_60026 = bitcast i8** %_60053 to i8*
  %_60054 = bitcast i8* %_60026 to i8**
  %_60055 = getelementptr i8*, i8** %_60054, i32 %_60025
  %_60027 = bitcast i8** %_60055 to i8*
  %_60056 = bitcast i8* %_60027 to i8**
  %_60006 = load i8*, i8** %_60056
  %_60007 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM33scala.collection.MapOps$$Lambda$3G4type" to i8*), i64 16)
  %_60057 = bitcast i8* %_60007 to { i8*, i8* }*
  %_60058 = getelementptr { i8*, i8* }, { i8*, i8* }* %_60057, i32 0, i32 1
  %_60029 = bitcast i8** %_60058 to i8*
  %_60059 = bitcast i8* %_60029 to i8**
  store i8* %_1, i8** %_60059
  %_60060 = bitcast i8* %_60006 to i8* (i8*, i8*)*
  %_60009 = call dereferenceable_or_null(8) i8* %_60060(i8* dereferenceable_or_null(8) %_60003, i8* nonnull dereferenceable(16) %_60007)
  %_60031 = icmp ne i8* %_60009, null
  br i1 %_60031, label %_60030.0, label %_60014.0
_60030.0:
  %_60061 = bitcast i8* %_60009 to i8**
  %_60032 = load i8*, i8** %_60061
  %_60062 = bitcast i8* %_60032 to { i8*, i32, i32, i8* }*
  %_60063 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60062, i32 0, i32 2
  %_60033 = bitcast i32* %_60063 to i8*
  %_60064 = bitcast i8* %_60033 to i32*
  %_60034 = load i32, i32* %_60064
  %_60065 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_60066 = getelementptr i8*, i8** %_60065, i32 3034
  %_60035 = bitcast i8** %_60066 to i8*
  %_60067 = bitcast i8* %_60035 to i8**
  %_60068 = getelementptr i8*, i8** %_60067, i32 %_60034
  %_60036 = bitcast i8** %_60068 to i8*
  %_60069 = bitcast i8* %_60036 to i8**
  %_60011 = load i8*, i8** %_60069
  %_60070 = bitcast i8* %_60011 to i8* (i8*, i8*, i8*, i8*, i8*)*
  %_60012 = call dereferenceable_or_null(16) i8* %_60070(i8* dereferenceable_or_null(8) %_60009, i8* dereferenceable_or_null(16) %_2, i8* dereferenceable_or_null(32) %_3, i8* dereferenceable_or_null(32) %_4, i8* dereferenceable_or_null(32) %_5)
  ret i8* %_60012
_60014.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM23scala.collection.MapOpsD9getOrElseL16java.lang.ObjectL15scala.Function0L16java.lang.ObjectEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_250003 = icmp ne i8* %_1, null
  br i1 %_250003, label %_250001.0, label %_250002.0
_250001.0:
  %_250043 = bitcast i8* %_1 to i8**
  %_250004 = load i8*, i8** %_250043
  %_250044 = bitcast i8* %_250004 to { i8*, i32, i32, i8* }*
  %_250045 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_250044, i32 0, i32 2
  %_250005 = bitcast i32* %_250045 to i8*
  %_250046 = bitcast i8* %_250005 to i32*
  %_250006 = load i32, i32* %_250046
  %_250047 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_250048 = getelementptr i8*, i8** %_250047, i32 2378
  %_250007 = bitcast i8** %_250048 to i8*
  %_250049 = bitcast i8* %_250007 to i8**
  %_250050 = getelementptr i8*, i8** %_250049, i32 %_250006
  %_250008 = bitcast i8** %_250050 to i8*
  %_250051 = bitcast i8* %_250008 to i8**
  %_40002 = load i8*, i8** %_250051
  %_250052 = bitcast i8* %_40002 to i8* (i8*, i8*)*
  %_40003 = call dereferenceable_or_null(8) i8* %_250052(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  br label %_50000.0
_50000.0:
  %_250012 = icmp eq i8* %_40003, null
  br i1 %_250012, label %_250009.0, label %_250010.0
_250009.0:
  br label %_250011.0
_250010.0:
  %_250053 = bitcast i8* %_40003 to i8**
  %_250013 = load i8*, i8** %_250053
  %_250014 = icmp eq i8* %_250013, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM10scala.SomeG4type" to i8*)
  br label %_250011.0
_250011.0:
  %_50002 = phi i1 [%_250014, %_250010.0], [false, %_250009.0]
  br i1 %_50002, label %_60000.0, label %_70000.0
_60000.0:
  %_250018 = icmp eq i8* %_40003, null
  br i1 %_250018, label %_250016.0, label %_250015.0
_250015.0:
  %_250054 = bitcast i8* %_40003 to i8**
  %_250019 = load i8*, i8** %_250054
  %_250020 = icmp eq i8* %_250019, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM10scala.SomeG4type" to i8*)
  br i1 %_250020, label %_250016.0, label %_250017.0
_250016.0:
  %_60001 = bitcast i8* %_40003 to i8*
  %_250022 = icmp ne i8* %_60001, null
  br i1 %_250022, label %_250021.0, label %_250002.0
_250021.0:
  %_250055 = bitcast i8* %_60001 to { i8*, i8* }*
  %_250056 = getelementptr { i8*, i8* }, { i8*, i8* }* %_250055, i32 0, i32 1
  %_250023 = bitcast i8** %_250056 to i8*
  %_250057 = bitcast i8* %_250023 to i8**
  %_80001 = load i8*, i8** %_250057, !dereferenceable_or_null !{i64 8}
  br label %_90000.0
_70000.0:
  br label %_100000.0
_100000.0:
  %_100001 = call dereferenceable_or_null(8) i8* @"_SM11scala.None$G4load"()
  br label %_110000.0
_110000.0:
  %_110001 = call i1 @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO"(i8* nonnull dereferenceable(8) %_100001, i8* dereferenceable_or_null(8) %_40003)
  br label %_120000.0
_120000.0:
  br i1 %_110001, label %_130000.0, label %_140000.0
_130000.0:
  %_250025 = icmp ne i8* %_3, null
  br i1 %_250025, label %_250024.0, label %_250002.0
_250024.0:
  %_250058 = bitcast i8* %_3 to i8**
  %_250026 = load i8*, i8** %_250058
  %_250059 = bitcast i8* %_250026 to { i8*, i32, i32, i8* }*
  %_250060 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_250059, i32 0, i32 2
  %_250027 = bitcast i32* %_250060 to i8*
  %_250061 = bitcast i8* %_250027 to i32*
  %_250028 = load i32, i32* %_250061
  %_250062 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_250063 = getelementptr i8*, i8** %_250062, i32 1529
  %_250029 = bitcast i8** %_250063 to i8*
  %_250064 = bitcast i8* %_250029 to i8**
  %_250065 = getelementptr i8*, i8** %_250064, i32 %_250028
  %_250030 = bitcast i8** %_250065 to i8*
  %_250066 = bitcast i8* %_250030 to i8**
  %_130002 = load i8*, i8** %_250066
  %_250067 = bitcast i8* %_130002 to i8* (i8*)*
  %_130003 = call dereferenceable_or_null(8) i8* %_250067(i8* dereferenceable_or_null(8) %_3)
  br label %_90000.0
_140000.0:
  br label %_150000.0
_90000.0:
  %_90001 = phi i8* [%_130003, %_250024.0], [%_80001, %_250021.0]
  ret i8* %_90001
_150000.0:
  br label %_240000.0
_240000.0:
  %_240001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM16scala.MatchErrorG4type" to i8*), i64 64)
  %_250068 = bitcast i8* %_240001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_250069 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_250068, i32 0, i32 5
  %_250032 = bitcast i1* %_250069 to i8*
  %_250070 = bitcast i8* %_250032 to i1*
  store i1 true, i1* %_250070
  %_250071 = bitcast i8* %_240001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_250072 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_250071, i32 0, i32 4
  %_250034 = bitcast i1* %_250072 to i8*
  %_250073 = bitcast i8* %_250034 to i1*
  store i1 true, i1* %_250073
  %_250074 = bitcast i8* %_240001 to { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }*
  %_250075 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }, { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }* %_250074, i32 0, i32 6
  %_250036 = bitcast i8** %_250075 to i8*
  %_250076 = bitcast i8* %_250036 to i8**
  store i8* %_40003, i8** %_250076
  %_240005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(64) %_240001)
  br label %_250000.0
_250000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(64) %_240001)
  unreachable
_250002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_250017.0:
  %_250039 = phi i8* [%_40003, %_250015.0]
  %_250040 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM10scala.SomeG4type" to i8*), %_250015.0]
  %_250077 = bitcast i8* %_250039 to i8**
  %_250041 = load i8*, i8** %_250077
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_250041, i8* %_250040)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM24java.io.Reader$$Lambda$1D5applyL16java.lang.ObjectEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20005 = icmp ne i8* %_1, null
  br i1 %_20005, label %_20003.0, label %_20004.0
_20003.0:
  %_20008 = bitcast i8* %_1 to { i8*, i8* }*
  %_20009 = getelementptr { i8*, i8* }, { i8*, i8* }* %_20008, i32 0, i32 1
  %_20006 = bitcast i8** %_20009 to i8*
  %_20010 = bitcast i8* %_20006 to i8**
  %_20001 = load i8*, i8** %_20010, !dereferenceable_or_null !{i64 16}
  %_20002 = call dereferenceable_or_null(16) i8* @"_SM14java.io.ReaderD17$init$$$anonfun$1L14java.io.ReaderEPT14java.io.Reader"(i8* dereferenceable_or_null(16) %_20001)
  ret i8* %_20002
_20004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM24java.io.Writer$$Lambda$1D5applyL16java.lang.ObjectEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20005 = icmp ne i8* %_1, null
  br i1 %_20005, label %_20003.0, label %_20004.0
_20003.0:
  %_20008 = bitcast i8* %_1 to { i8*, i8* }*
  %_20009 = getelementptr { i8*, i8* }, { i8*, i8* }* %_20008, i32 0, i32 1
  %_20006 = bitcast i8** %_20009 to i8*
  %_20010 = bitcast i8* %_20006 to i8**
  %_20001 = load i8*, i8** %_20010, !dereferenceable_or_null !{i64 16}
  %_20002 = call dereferenceable_or_null(16) i8* @"_SM14java.io.WriterD17$init$$$anonfun$1L14java.io.WriterEPT14java.io.Writer"(i8* dereferenceable_or_null(16) %_20001)
  ret i8* %_20002
_20004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM24java.nio.file.LinkOptionD14NOFOLLOW_LINKSL24java.nio.file.LinkOptionEo"() inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_10000.0:
  %_10001 = call dereferenceable_or_null(24) i8* @"_SM25java.nio.file.LinkOption$G4load"()
  %_10004 = bitcast i8* %_10001 to { i8*, i8*, i8* }*
  %_10005 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_10004, i32 0, i32 1
  %_10003 = bitcast i8** %_10005 to i8*
  %_10006 = bitcast i8* %_10003 to i8**
  %_10002 = load i8*, i8** %_10006, !dereferenceable_or_null !{i64 24}
  ret i8* %_10002
}

define dereferenceable_or_null(8) i8* @"_SM24java.nio.file.LinkOptionD15productIteratorL25scala.collection.IteratorEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = bitcast i8* bitcast (i8* (i8*)* @"_SM13scala.ProductD15productIteratorL25scala.collection.IteratorEO" to i8*) to i8* (i8*)*
  %_20001 = call dereferenceable_or_null(8) i8* %_20002(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define nonnull dereferenceable(8) i8* @"_SM24java.nio.file.LinkOptionIE"() personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_10000.0:
  %_10001 = call dereferenceable_or_null(24) i8* @"_SM25java.nio.file.LinkOption$G4load"()
  %_10002 = call dereferenceable_or_null(24) i8* @"_SM24java.nio.file.LinkOptionD14NOFOLLOW_LINKSL24java.nio.file.LinkOptionEo"()
  %_10006 = bitcast i8* %_10001 to { i8*, i8*, i8* }*
  %_10007 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_10006, i32 0, i32 1
  %_10005 = bitcast i8** %_10007 to i8*
  %_10008 = bitcast i8* %_10005 to i8**
  store i8* %_10002, i8** %_10008
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM25scala.collection.IteratorD10$plus$plusL15scala.Function0L25scala.collection.IteratorEO"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30006 = icmp ne i8* %_1, null
  br i1 %_30006, label %_30004.0, label %_30005.0
_30004.0:
  %_30013 = bitcast i8* %_1 to i8**
  %_30007 = load i8*, i8** %_30013
  %_30014 = bitcast i8* %_30007 to { i8*, i32, i32, i8* }*
  %_30015 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30014, i32 0, i32 2
  %_30008 = bitcast i32* %_30015 to i8*
  %_30016 = bitcast i8* %_30008 to i32*
  %_30009 = load i32, i32* %_30016
  %_30017 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30018 = getelementptr i8*, i8** %_30017, i32 4289
  %_30010 = bitcast i8** %_30018 to i8*
  %_30019 = bitcast i8* %_30010 to i8**
  %_30020 = getelementptr i8*, i8** %_30019, i32 %_30009
  %_30011 = bitcast i8** %_30020 to i8*
  %_30021 = bitcast i8* %_30011 to i8**
  %_30002 = load i8*, i8** %_30021
  %_30022 = bitcast i8* %_30002 to i8* (i8*, i8*)*
  %_30003 = call dereferenceable_or_null(8) i8* %_30022(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30003
_30005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM25scala.collection.IteratorD12sameElementsL29scala.collection.IterableOncezEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_160009 = icmp ne i8* %_2, null
  br i1 %_160009, label %_160007.0, label %_160008.0
_160007.0:
  %_160062 = bitcast i8* %_2 to i8**
  %_160010 = load i8*, i8** %_160062
  %_160063 = bitcast i8* %_160010 to { i8*, i32, i32, i8* }*
  %_160064 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_160063, i32 0, i32 2
  %_160011 = bitcast i32* %_160064 to i8*
  %_160065 = bitcast i8* %_160011 to i32*
  %_160012 = load i32, i32* %_160065
  %_160066 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_160067 = getelementptr i8*, i8** %_160066, i32 446
  %_160013 = bitcast i8** %_160067 to i8*
  %_160068 = bitcast i8* %_160013 to i8**
  %_160069 = getelementptr i8*, i8** %_160068, i32 %_160012
  %_160014 = bitcast i8** %_160069 to i8*
  %_160070 = bitcast i8* %_160014 to i8**
  %_30002 = load i8*, i8** %_160070
  %_160071 = bitcast i8* %_30002 to i8* (i8*)*
  %_30003 = call dereferenceable_or_null(8) i8* %_160071(i8* dereferenceable_or_null(8) %_2)
  br label %_40000.0
_40000.0:
  %_160016 = icmp ne i8* %_1, null
  br i1 %_160016, label %_160015.0, label %_160008.0
_160015.0:
  %_160072 = bitcast i8* %_1 to i8**
  %_160017 = load i8*, i8** %_160072
  %_160073 = bitcast i8* %_160017 to { i8*, i32, i32, i8* }*
  %_160074 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_160073, i32 0, i32 2
  %_160018 = bitcast i32* %_160074 to i8*
  %_160075 = bitcast i8* %_160018 to i32*
  %_160019 = load i32, i32* %_160075
  %_160076 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_160077 = getelementptr i8*, i8** %_160076, i32 4359
  %_160020 = bitcast i8** %_160077 to i8*
  %_160078 = bitcast i8* %_160020 to i8**
  %_160079 = getelementptr i8*, i8** %_160078, i32 %_160019
  %_160021 = bitcast i8** %_160079 to i8*
  %_160080 = bitcast i8* %_160021 to i8**
  %_40002 = load i8*, i8** %_160080
  %_160081 = bitcast i8* %_40002 to i1 (i8*)*
  %_40003 = call i1 %_160081(i8* dereferenceable_or_null(8) %_1)
  br i1 %_40003, label %_50000.0, label %_60000.0
_50000.0:
  %_160023 = icmp ne i8* %_30003, null
  br i1 %_160023, label %_160022.0, label %_160008.0
_160022.0:
  %_160082 = bitcast i8* %_30003 to i8**
  %_160024 = load i8*, i8** %_160082
  %_160083 = bitcast i8* %_160024 to { i8*, i32, i32, i8* }*
  %_160084 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_160083, i32 0, i32 2
  %_160025 = bitcast i32* %_160084 to i8*
  %_160085 = bitcast i8* %_160025 to i32*
  %_160026 = load i32, i32* %_160085
  %_160086 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_160087 = getelementptr i8*, i8** %_160086, i32 4359
  %_160027 = bitcast i8** %_160087 to i8*
  %_160088 = bitcast i8* %_160027 to i8**
  %_160089 = getelementptr i8*, i8** %_160088, i32 %_160026
  %_160028 = bitcast i8** %_160089 to i8*
  %_160090 = bitcast i8* %_160028 to i8**
  %_50002 = load i8*, i8** %_160090
  %_160091 = bitcast i8* %_50002 to i1 (i8*)*
  %_50003 = call i1 %_160091(i8* dereferenceable_or_null(8) %_30003)
  br label %_70000.0
_60000.0:
  br label %_70000.0
_70000.0:
  %_70001 = phi i1 [false, %_60000.0], [%_50003, %_160022.0]
  br i1 %_70001, label %_80000.0, label %_90000.0
_80000.0:
  %_160030 = icmp ne i8* %_1, null
  br i1 %_160030, label %_160029.0, label %_160008.0
_160029.0:
  %_160092 = bitcast i8* %_1 to i8**
  %_160031 = load i8*, i8** %_160092
  %_160093 = bitcast i8* %_160031 to { i8*, i32, i32, i8* }*
  %_160094 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_160093, i32 0, i32 2
  %_160032 = bitcast i32* %_160094 to i8*
  %_160095 = bitcast i8* %_160032 to i32*
  %_160033 = load i32, i32* %_160095
  %_160096 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_160097 = getelementptr i8*, i8** %_160096, i32 4219
  %_160034 = bitcast i8** %_160097 to i8*
  %_160098 = bitcast i8* %_160034 to i8**
  %_160099 = getelementptr i8*, i8** %_160098, i32 %_160033
  %_160035 = bitcast i8** %_160099 to i8*
  %_160100 = bitcast i8* %_160035 to i8**
  %_80002 = load i8*, i8** %_160100
  %_160101 = bitcast i8* %_80002 to i8* (i8*)*
  %_80003 = call dereferenceable_or_null(8) i8* %_160101(i8* dereferenceable_or_null(8) %_1)
  %_80005 = icmp eq i8* %_80003, null
  br i1 %_80005, label %_100000.0, label %_110000.0
_100000.0:
  %_160037 = icmp ne i8* %_30003, null
  br i1 %_160037, label %_160036.0, label %_160008.0
_160036.0:
  %_160102 = bitcast i8* %_30003 to i8**
  %_160038 = load i8*, i8** %_160102
  %_160103 = bitcast i8* %_160038 to { i8*, i32, i32, i8* }*
  %_160104 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_160103, i32 0, i32 2
  %_160039 = bitcast i32* %_160104 to i8*
  %_160105 = bitcast i8* %_160039 to i32*
  %_160040 = load i32, i32* %_160105
  %_160106 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_160107 = getelementptr i8*, i8** %_160106, i32 4219
  %_160041 = bitcast i8** %_160107 to i8*
  %_160108 = bitcast i8* %_160041 to i8**
  %_160109 = getelementptr i8*, i8** %_160108, i32 %_160040
  %_160042 = bitcast i8** %_160109 to i8*
  %_160110 = bitcast i8* %_160042 to i8**
  %_100002 = load i8*, i8** %_160110
  %_160111 = bitcast i8* %_100002 to i8* (i8*)*
  %_100003 = call dereferenceable_or_null(8) i8* %_160111(i8* dereferenceable_or_null(8) %_30003)
  %_100005 = icmp eq i8* %_100003, null
  br label %_120000.0
_110000.0:
  %_160044 = icmp ne i8* %_30003, null
  br i1 %_160044, label %_160043.0, label %_160008.0
_160043.0:
  %_160112 = bitcast i8* %_30003 to i8**
  %_160045 = load i8*, i8** %_160112
  %_160113 = bitcast i8* %_160045 to { i8*, i32, i32, i8* }*
  %_160114 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_160113, i32 0, i32 2
  %_160046 = bitcast i32* %_160114 to i8*
  %_160115 = bitcast i8* %_160046 to i32*
  %_160047 = load i32, i32* %_160115
  %_160116 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_160117 = getelementptr i8*, i8** %_160116, i32 4219
  %_160048 = bitcast i8** %_160117 to i8*
  %_160118 = bitcast i8* %_160048 to i8**
  %_160119 = getelementptr i8*, i8** %_160118, i32 %_160047
  %_160049 = bitcast i8** %_160119 to i8*
  %_160120 = bitcast i8* %_160049 to i8**
  %_110002 = load i8*, i8** %_160120
  %_160121 = bitcast i8* %_110002 to i8* (i8*)*
  %_110003 = call dereferenceable_or_null(8) i8* %_160121(i8* dereferenceable_or_null(8) %_30003)
  %_160051 = icmp ne i8* %_80003, null
  br i1 %_160051, label %_160050.0, label %_160008.0
_160050.0:
  %_160122 = bitcast i8* %_80003 to i8**
  %_160052 = load i8*, i8** %_160122
  %_160123 = bitcast i8* %_160052 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_160124 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_160123, i32 0, i32 4, i32 0
  %_160053 = bitcast i8** %_160124 to i8*
  %_160125 = bitcast i8* %_160053 to i8**
  %_110005 = load i8*, i8** %_160125
  %_160126 = bitcast i8* %_110005 to i1 (i8*, i8*)*
  %_110006 = call i1 %_160126(i8* dereferenceable_or_null(8) %_80003, i8* dereferenceable_or_null(8) %_110003)
  br label %_120000.0
_120000.0:
  %_120001 = phi i1 [%_110006, %_160050.0], [%_100005, %_160036.0]
  %_120003 = xor i1 %_120001, true
  br i1 %_120003, label %_130000.0, label %_140000.0
_130000.0:
  ret i1 false
_140000.0:
  br label %_150000.0
_150000.0:
  br label %_40000.0
_90000.0:
  br label %_160000.0
_160000.0:
  %_160127 = bitcast i8* %_40002 to i1 (i8*)*
  %_160001 = call i1 %_160127(i8* dereferenceable_or_null(8) %_1)
  %_160055 = icmp ne i8* %_30003, null
  br i1 %_160055, label %_160054.0, label %_160008.0
_160054.0:
  %_160128 = bitcast i8* %_30003 to i8**
  %_160056 = load i8*, i8** %_160128
  %_160129 = bitcast i8* %_160056 to { i8*, i32, i32, i8* }*
  %_160130 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_160129, i32 0, i32 2
  %_160057 = bitcast i32* %_160130 to i8*
  %_160131 = bitcast i8* %_160057 to i32*
  %_160058 = load i32, i32* %_160131
  %_160132 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_160133 = getelementptr i8*, i8** %_160132, i32 4359
  %_160059 = bitcast i8** %_160133 to i8*
  %_160134 = bitcast i8* %_160059 to i8**
  %_160135 = getelementptr i8*, i8** %_160134, i32 %_160058
  %_160060 = bitcast i8** %_160135 to i8*
  %_160136 = bitcast i8* %_160060 to i8**
  %_160003 = load i8*, i8** %_160136
  %_160137 = bitcast i8* %_160003 to i1 (i8*)*
  %_160004 = call i1 %_160137(i8* dereferenceable_or_null(8) %_30003)
  %_160006 = icmp eq i1 %_160001, %_160004
  ret i1 %_160006
_160008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM25scala.collection.IteratorD13sliceIteratoriiL25scala.collection.IteratorEO"(i8* %_1, i32 %_2, i32 %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40003 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_40004 = call i32 @"_SM26scala.LowPriorityImplicitsD10intWrapperiiEO"(i8* nonnull dereferenceable(16) %_40003, i32 %_2)
  %_40005 = call i32 @"_SM22scala.runtime.RichInt$D13max$extensioniiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM22scala.runtime.RichInt$G8instance" to i8*), i32 %_40004, i32 0)
  %_40007 = icmp slt i32 %_3, 0
  br i1 %_40007, label %_50000.0, label %_60000.0
_50000.0:
  br label %_70000.0
_60000.0:
  %_60002 = icmp sle i32 %_3, %_40005
  br i1 %_60002, label %_80000.0, label %_90000.0
_80000.0:
  br label %_100000.0
_90000.0:
  %_90002 = sub i32 %_3, %_40005
  br label %_100000.0
_100000.0:
  %_100001 = phi i32 [%_90002, %_90000.0], [0, %_80000.0]
  br label %_70000.0
_70000.0:
  %_70001 = phi i32 [%_100001, %_100000.0], [-1, %_50000.0]
  %_70003 = icmp eq i32 %_70001, 0
  br i1 %_70003, label %_110000.0, label %_120000.0
_110000.0:
  %_110001 = call dereferenceable_or_null(16) i8* @"_SM26scala.collection.Iterator$G4load"()
  %_110002 = call dereferenceable_or_null(8) i8* @"_SM26scala.collection.Iterator$D5emptyL25scala.collection.IteratorEO"(i8* nonnull dereferenceable(16) %_110001)
  br label %_130000.0
_120000.0:
  %_150001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM39scala.collection.Iterator$SliceIteratorG4type" to i8*), i64 24)
  %_150015 = bitcast i8* %_150001 to { i8*, i32, i32, i8* }*
  %_150016 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_150015, i32 0, i32 3
  %_150007 = bitcast i8** %_150016 to i8*
  %_150017 = bitcast i8* %_150007 to i8**
  store i8* %_1, i8** %_150017
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(24) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(24) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IteratorD6$init$uEO"(i8* nonnull dereferenceable(24) %_150001)
  %_150018 = bitcast i8* %_150001 to { i8*, i32, i32, i8* }*
  %_150019 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_150018, i32 0, i32 2
  %_150012 = bitcast i32* %_150019 to i8*
  %_150020 = bitcast i8* %_150012 to i32*
  store i32 %_70001, i32* %_150020
  %_150021 = bitcast i8* %_150001 to { i8*, i32, i32, i8* }*
  %_150022 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_150021, i32 0, i32 1
  %_150014 = bitcast i32* %_150022 to i8*
  %_150023 = bitcast i8* %_150014 to i32*
  store i32 %_40005, i32* %_150023
  br label %_130000.0
_130000.0:
  %_130001 = phi i8* [%_150001, %_120000.0], [%_110002, %_110000.0]
  ret i8* %_130001
}

define nonnull dereferenceable(24) i8* @"_SM25scala.collection.IteratorD3mapL15scala.Function1L25scala.collection.IteratorEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_40002 = icmp eq i8* %_1, null
  br i1 %_40002, label %_50000.0, label %_60000.0
_60000.0:
  br label %_70000.0
_50000.0:
  %_80009 = icmp ne i8* null, null
  br i1 %_80009, label %_80007.0, label %_80008.0
_80007.0:
  call i8* @"scalanative_throw"(i8* null)
  unreachable
_70000.0:
  %_80001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM33scala.collection.Iterator$$anon$9G4type" to i8*), i64 24)
  %_80019 = bitcast i8* %_80001 to { i8*, i8*, i8* }*
  %_80020 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_80019, i32 0, i32 2
  %_80012 = bitcast i8** %_80020 to i8*
  %_80021 = bitcast i8* %_80012 to i8**
  store i8* %_2, i8** %_80021
  %_80022 = bitcast i8* %_80001 to { i8*, i8*, i8* }*
  %_80023 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_80022, i32 0, i32 1
  %_80014 = bitcast i8** %_80023 to i8*
  %_80024 = bitcast i8* %_80014 to i8**
  store i8* %_1, i8** %_80024
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(24) %_80001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(24) %_80001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IteratorD6$init$uEO"(i8* nonnull dereferenceable(24) %_80001)
  ret i8* %_80001
_80008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM25scala.collection.IteratorD4dropiL25scala.collection.IteratorEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30006 = icmp ne i8* %_1, null
  br i1 %_30006, label %_30004.0, label %_30005.0
_30004.0:
  %_30013 = bitcast i8* %_1 to i8**
  %_30007 = load i8*, i8** %_30013
  %_30014 = bitcast i8* %_30007 to { i8*, i32, i32, i8* }*
  %_30015 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30014, i32 0, i32 2
  %_30008 = bitcast i32* %_30015 to i8*
  %_30016 = bitcast i8* %_30008 to i32*
  %_30009 = load i32, i32* %_30016
  %_30017 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30018 = getelementptr i8*, i8** %_30017, i32 4009
  %_30010 = bitcast i8** %_30018 to i8*
  %_30019 = bitcast i8* %_30010 to i8**
  %_30020 = getelementptr i8*, i8** %_30019, i32 %_30009
  %_30011 = bitcast i8** %_30020 to i8*
  %_30021 = bitcast i8* %_30011 to i8**
  %_30002 = load i8*, i8** %_30021
  %_30022 = bitcast i8* %_30002 to i8* (i8*, i32, i32)*
  %_30003 = call dereferenceable_or_null(8) i8* %_30022(i8* dereferenceable_or_null(8) %_1, i32 %_2, i32 -1)
  ret i8* %_30003
_30005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM25scala.collection.IteratorD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM25scala.collection.IteratorD6concatL15scala.Function0L25scala.collection.IteratorEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_50001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM40scala.collection.Iterator$ConcatIteratorG4type" to i8*), i64 40)
  %_50017 = bitcast i8* %_50001 to { i8*, i1, i8*, i8*, i8* }*
  %_50018 = getelementptr { i8*, i1, i8*, i8*, i8* }, { i8*, i1, i8*, i8*, i8* }* %_50017, i32 0, i32 4
  %_50007 = bitcast i8** %_50018 to i8*
  %_50019 = bitcast i8* %_50007 to i8**
  store i8* %_1, i8** %_50019
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(40) %_50001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(40) %_50001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IteratorD6$init$uEO"(i8* nonnull dereferenceable(40) %_50001)
  %_50020 = bitcast i8* %_50001 to { i8*, i1, i8*, i8*, i8* }*
  %_50021 = getelementptr { i8*, i1, i8*, i8*, i8* }, { i8*, i1, i8*, i8*, i8* }* %_50020, i32 0, i32 3
  %_50012 = bitcast i8** %_50021 to i8*
  %_50022 = bitcast i8* %_50012 to i8**
  store i8* null, i8** %_50022
  %_50023 = bitcast i8* %_50001 to { i8*, i1, i8*, i8*, i8* }*
  %_50024 = getelementptr { i8*, i1, i8*, i8*, i8* }, { i8*, i1, i8*, i8*, i8* }* %_50023, i32 0, i32 2
  %_50014 = bitcast i8** %_50024 to i8*
  %_50025 = bitcast i8* %_50014 to i8**
  store i8* null, i8** %_50025
  %_50026 = bitcast i8* %_50001 to { i8*, i1, i8*, i8*, i8* }*
  %_50027 = getelementptr { i8*, i1, i8*, i8*, i8* }, { i8*, i1, i8*, i8*, i8* }* %_50026, i32 0, i32 1
  %_50016 = bitcast i1* %_50027 to i8*
  %_50028 = bitcast i8* %_50016 to i1*
  store i1 false, i1* %_50028
  %_50029 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM40scala.collection.Iterator$ConcatIteratorD6concatL15scala.Function0L25scala.collection.IteratorEO" to i8*) to i8* (i8*, i8*)*
  %_30002 = call dereferenceable_or_null(8) i8* %_50029(i8* nonnull dereferenceable(40) %_50001, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30002
}

define i1 @"_SM25scala.collection.IteratorD7isEmptyzEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20008 = icmp ne i8* %_1, null
  br i1 %_20008, label %_20006.0, label %_20007.0
_20006.0:
  %_20015 = bitcast i8* %_1 to i8**
  %_20009 = load i8*, i8** %_20015
  %_20016 = bitcast i8* %_20009 to { i8*, i32, i32, i8* }*
  %_20017 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_20016, i32 0, i32 2
  %_20010 = bitcast i32* %_20017 to i8*
  %_20018 = bitcast i8* %_20010 to i32*
  %_20011 = load i32, i32* %_20018
  %_20019 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_20020 = getelementptr i8*, i8** %_20019, i32 4359
  %_20012 = bitcast i8** %_20020 to i8*
  %_20021 = bitcast i8* %_20012 to i8**
  %_20022 = getelementptr i8*, i8** %_20021, i32 %_20011
  %_20013 = bitcast i8** %_20022 to i8*
  %_20023 = bitcast i8* %_20013 to i8**
  %_20002 = load i8*, i8** %_20023
  %_20024 = bitcast i8* %_20002 to i1 (i8*)*
  %_20003 = call i1 %_20024(i8* dereferenceable_or_null(8) %_1)
  %_20005 = xor i1 %_20003, true
  ret i1 %_20005
_20007.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM25scala.collection.IteratorD8iteratorL25scala.collection.IteratorEO"(i8* %_1) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* %_1
}

define nonnull dereferenceable(32) i8* @"_SM25scala.collection.IteratorD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-265" to i8*)
}

define dereferenceable_or_null(32) i8* @"_SM26java.lang.String$$Lambda$7D5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_40005 = icmp ne i8* %_1, null
  br i1 %_40005, label %_40003.0, label %_40004.0
_40003.0:
  %_40008 = bitcast i8* %_1 to { i8*, i8* }*
  %_40009 = getelementptr { i8*, i8* }, { i8*, i8* }* %_40008, i32 0, i32 1
  %_40006 = bitcast i8** %_40009 to i8*
  %_40010 = bitcast i8* %_40006 to i8**
  %_30001 = load i8*, i8** %_40010, !dereferenceable_or_null !{i64 32}
  %_40011 = bitcast i8* bitcast (i32 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D10unboxToIntL16java.lang.ObjectiEO" to i8*) to i32 (i8*, i8*)*
  %_40001 = call i32 %_40011(i8* null, i8* dereferenceable_or_null(8) %_2)
  %_40002 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.StringD22toUpperCase$$anonfun$1iL16java.lang.StringEPT16java.lang.String"(i8* dereferenceable_or_null(32) %_30001, i32 %_40001)
  ret i8* %_40002
_40004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM27java.lang.System$$$Lambda$6D5applyL16java.lang.ObjectEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20005 = icmp ne i8* %_1, null
  br i1 %_20005, label %_20003.0, label %_20004.0
_20003.0:
  %_20008 = bitcast i8* %_1 to { i8*, i8* }*
  %_20009 = getelementptr { i8*, i8* }, { i8*, i8* }* %_20008, i32 0, i32 1
  %_20006 = bitcast i8** %_20009 to i8*
  %_20010 = bitcast i8* %_20006 to i8**
  %_20001 = load i8*, i8** %_20010, !dereferenceable_or_null !{i64 56}
  %_20002 = call dereferenceable_or_null(8) i8* @"_SM17java.lang.System$D10$anonfun$1L12scala.OptionEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_20001)
  ret i8* %_20002
_20004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM28java.lang.System$$$Lambda$12D5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30007 = icmp ne i8* %_1, null
  br i1 %_30007, label %_30005.0, label %_30006.0
_30005.0:
  %_30010 = bitcast i8* %_1 to { i8*, i8* }*
  %_30011 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30010, i32 0, i32 1
  %_30008 = bitcast i8** %_30011 to i8*
  %_30012 = bitcast i8* %_30008 to i8**
  %_30002 = load i8*, i8** %_30012, !dereferenceable_or_null !{i64 56}
  %_30003 = bitcast i8* %_2 to i8*
  %_30004 = call dereferenceable_or_null(32) i8* @"_SM17java.lang.System$D26getUserLanguage$$anonfun$1L16java.lang.StringL16java.lang.StringEPT17java.lang.System$"(i8* dereferenceable_or_null(56) %_30002, i8* dereferenceable_or_null(32) %_30003)
  ret i8* %_30004
_30006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM28scala.collection.AbstractSeqD12sameElementsL29scala.collection.IterableOncezEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i1 @"_SM23scala.collection.SeqOpsD12sameElementsL29scala.collection.IterableOncezEO"(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i1 %_30001
}

define i32 @"_SM28scala.collection.AbstractSeqD13apply$mcII$spiiEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i32 @"_SM15scala.Function1D13apply$mcII$spiiEO"(i8* dereferenceable_or_null(8) %_1, i32 %_2)
  ret i32 %_30001
}

define i32 @"_SM28scala.collection.AbstractSeqD13lengthCompareiiEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i32 @"_SM23scala.collection.SeqOpsD13lengthCompareiiEO"(i8* dereferenceable_or_null(8) %_1, i32 %_2)
  ret i32 %_30001
}

define i32 @"_SM28scala.collection.AbstractSeqD42scala$collection$SeqOps$$super$sizeCompareiiEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i32 @"_SM28scala.collection.IterableOpsD11sizeCompareiiEO"(i8* dereferenceable_or_null(8) %_1, i32 %_2)
  ret i32 %_30001
}

define i32 @"_SM28scala.collection.AbstractSeqD4sizeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.collection.SeqOpsD4sizeiEO"(i8* dereferenceable_or_null(8) %_1)
  ret i32 %_20001
}

define i1 @"_SM28scala.collection.AbstractSeqD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i1 @"_SM20scala.collection.SeqD6equalsL16java.lang.ObjectzEO"(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i1 %_30001
}

define i1 @"_SM28scala.collection.AbstractSeqD7isEmptyzEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i1 @"_SM23scala.collection.SeqOpsD7isEmptyzEO"(i8* dereferenceable_or_null(8) %_1)
  ret i1 %_20001
}

define i1 @"_SM28scala.collection.AbstractSeqD8canEqualL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i1 @"_SM20scala.collection.SeqD8canEqualL16java.lang.ObjectzEO"(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i1 %_30001
}

define i32 @"_SM28scala.collection.AbstractSeqD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM20scala.collection.SeqD8hashCodeiEO"(i8* dereferenceable_or_null(8) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM28scala.collection.AbstractSeqD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM20scala.collection.SeqD8toStringL16java.lang.StringEO"(i8* dereferenceable_or_null(8) %_1)
  ret i8* %_20001
}

define nonnull dereferenceable(8) i8* @"_SM28scala.collection.mutable.SeqD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM28scala.reflect.AnyValManifestD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_1, %_2
  ret i1 %_30002
}

define i32 @"_SM28scala.reflect.AnyValManifestD8hashCodeiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, i8* }*
  %_20008 = getelementptr { i8*, i32, i8* }, { i8*, i32, i8* }* %_20007, i32 0, i32 1
  %_20005 = bitcast i32* %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i32*
  %_20001 = load i32, i32* %_20009
  ret i32 %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM28scala.reflect.AnyValManifestD8toStringL16java.lang.StringEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, i32, i8* }*
  %_20008 = getelementptr { i8*, i32, i8* }, { i8*, i32, i8* }* %_20007, i32 0, i32 2
  %_20005 = bitcast i8** %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i8**
  %_20001 = load i8*, i8** %_20009, !dereferenceable_or_null !{i64 32}
  ret i8* %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i8* @"_SM28scala.runtime.Scala3RunTime$D12assertFailednEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_100008 = bitcast i8* bitcast (i8* (i8*)* @"_SM16java.lang.StringD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_30001 = call dereferenceable_or_null(32) i8* %_100008(i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-267" to i8*))
  br label %_90000.0
_90000.0:
  %_90001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM24java.lang.AssertionErrorG4type" to i8*), i64 40)
  %_100009 = bitcast i8* %_90001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_100010 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_100009, i32 0, i32 5
  %_100002 = bitcast i1* %_100010 to i8*
  %_100011 = bitcast i8* %_100002 to i1*
  store i1 true, i1* %_100011
  %_100012 = bitcast i8* %_90001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_100013 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_100012, i32 0, i32 4
  %_100004 = bitcast i1* %_100013 to i8*
  %_100014 = bitcast i8* %_100004 to i1*
  store i1 true, i1* %_100014
  %_100015 = bitcast i8* %_90001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_100016 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_100015, i32 0, i32 3
  %_100006 = bitcast i8** %_100016 to i8*
  %_100017 = bitcast i8* %_100006 to i8**
  store i8* %_30001, i8** %_100017
  %_90005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_90001)
  br label %_100000.0
_100000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_90001)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM28scala.runtime.Scala3RunTime$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM28scala.scalanative.unsafe.PtrD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp eq i8* %_1, %_2
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  br label %_70000.0
_70000.0:
  %_130004 = icmp eq i8* %_2, null
  br i1 %_130004, label %_130001.0, label %_130002.0
_130001.0:
  br label %_130003.0
_130002.0:
  %_130025 = bitcast i8* %_2 to i8**
  %_130005 = load i8*, i8** %_130025
  %_130006 = icmp eq i8* %_130005, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28scala.scalanative.unsafe.PtrG4type" to i8*)
  br label %_130003.0
_130003.0:
  %_70002 = phi i1 [%_130006, %_130002.0], [false, %_130001.0]
  br i1 %_70002, label %_80000.0, label %_90000.0
_80000.0:
  %_130010 = icmp eq i8* %_2, null
  br i1 %_130010, label %_130008.0, label %_130007.0
_130007.0:
  %_130026 = bitcast i8* %_2 to i8**
  %_130011 = load i8*, i8** %_130026
  %_130012 = icmp eq i8* %_130011, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28scala.scalanative.unsafe.PtrG4type" to i8*)
  br i1 %_130012, label %_130008.0, label %_130009.0
_130008.0:
  %_80001 = bitcast i8* %_2 to i8*
  %_130015 = icmp ne i8* %_80001, null
  br i1 %_130015, label %_130013.0, label %_130014.0
_130013.0:
  %_130027 = bitcast i8* %_80001 to { i8*, i8* }*
  %_130028 = getelementptr { i8*, i8* }, { i8*, i8* }* %_130027, i32 0, i32 1
  %_130016 = bitcast i8** %_130028 to i8*
  %_130029 = bitcast i8* %_130016 to i8**
  %_100001 = load i8*, i8** %_130029
  %_130018 = icmp ne i8* %_1, null
  br i1 %_130018, label %_130017.0, label %_130014.0
_130017.0:
  %_130030 = bitcast i8* %_1 to { i8*, i8* }*
  %_130031 = getelementptr { i8*, i8* }, { i8*, i8* }* %_130030, i32 0, i32 1
  %_130019 = bitcast i8** %_130031 to i8*
  %_130032 = bitcast i8* %_130019 to i8**
  %_110001 = load i8*, i8** %_130032
  %_80003 = icmp eq i8* %_100001, %_110001
  br label %_120000.0
_90000.0:
  br label %_130000.0
_130000.0:
  br label %_120000.0
_120000.0:
  %_120001 = phi i1 [false, %_130000.0], [%_80003, %_130017.0]
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_120001, %_120000.0], [true, %_40000.0]
  ret i1 %_60001
_130014.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_130009.0:
  %_130021 = phi i8* [%_2, %_130007.0]
  %_130022 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28scala.scalanative.unsafe.PtrG4type" to i8*), %_130007.0]
  %_130033 = bitcast i8* %_130021 to i8**
  %_130023 = load i8*, i8** %_130033
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_130023, i8* %_130022)
  unreachable
}

define i32 @"_SM28scala.scalanative.unsafe.PtrD8hashCodeiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i8* }*
  %_30008 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30007, i32 0, i32 1
  %_30005 = bitcast i8** %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i8**
  %_30001 = load i8*, i8** %_30009
  %_20002 = ptrtoint i8* %_30001 to i64
  %_20003 = call i32 @"_SM14java.lang.LongD8hashCodejiEo"(i64 %_20002)
  ret i32 %_20003
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM28scala.scalanative.unsafe.PtrD8toStringL16java.lang.StringEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-269" to i8*), null
  br i1 %_20004, label %_30000.0, label %_40000.0
_30000.0:
  br label %_50000.0
_40000.0:
  br label %_50000.0
_50000.0:
  %_50001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-269" to i8*), %_40000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_30000.0]
  %_90005 = icmp ne i8* %_1, null
  br i1 %_90005, label %_90003.0, label %_90004.0
_90003.0:
  %_90008 = bitcast i8* %_1 to { i8*, i8* }*
  %_90009 = getelementptr { i8*, i8* }, { i8*, i8* }* %_90008, i32 0, i32 1
  %_90006 = bitcast i8** %_90009 to i8*
  %_90010 = bitcast i8* %_90006 to i8**
  %_60001 = load i8*, i8** %_90010
  %_50003 = ptrtoint i8* %_60001 to i64
  %_50004 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.LongD11toHexStringjL16java.lang.StringEo"(i64 %_50003)
  %_50006 = icmp eq i8* %_50004, null
  br i1 %_50006, label %_70000.0, label %_80000.0
_70000.0:
  br label %_90000.0
_80000.0:
  br label %_90000.0
_90000.0:
  %_90001 = phi i8* [%_50004, %_80000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_70000.0]
  %_90011 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_90002 = call dereferenceable_or_null(32) i8* %_90011(i8* nonnull dereferenceable(32) %_50001, i8* dereferenceable_or_null(32) %_90001)
  ret i8* %_90002
_90004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM29java.nio.charset.CoderResult$D18malformedForLengthiL28java.nio.charset.CoderResultEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  switch i32 %_2, label %_50000.0 [
    i32 1, label %_60000.0
    i32 2, label %_70000.0
    i32 3, label %_80000.0
    i32 4, label %_90000.0
  ]
_50000.0:
  %_50001 = call dereferenceable_or_null(16) i8* @"_SM29java.nio.charset.CoderResult$D22malformedForLengthImpliL28java.nio.charset.CoderResultEPT29java.nio.charset.CoderResult$"(i8* dereferenceable_or_null(104) %_1, i32 %_2)
  br label %_100000.0
_60000.0:
  %_60001 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_100006 = bitcast i8* %_60001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_100007 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_100006, i32 0, i32 5
  %_100002 = bitcast i8** %_100007 to i8*
  %_100008 = bitcast i8* %_100002 to i8**
  %_60002 = load i8*, i8** %_100008, !dereferenceable_or_null !{i64 16}
  br label %_100000.0
_70000.0:
  %_70001 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_100009 = bitcast i8* %_70001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_100010 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_100009, i32 0, i32 4
  %_100003 = bitcast i8** %_100010 to i8*
  %_100011 = bitcast i8* %_100003 to i8**
  %_70002 = load i8*, i8** %_100011, !dereferenceable_or_null !{i64 16}
  br label %_100000.0
_80000.0:
  %_80001 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_100012 = bitcast i8* %_80001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_100013 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_100012, i32 0, i32 3
  %_100004 = bitcast i8** %_100013 to i8*
  %_100014 = bitcast i8* %_100004 to i8**
  %_80002 = load i8*, i8** %_100014, !dereferenceable_or_null !{i64 16}
  br label %_100000.0
_90000.0:
  %_90001 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_100015 = bitcast i8* %_90001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_100016 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_100015, i32 0, i32 2
  %_100005 = bitcast i8** %_100016 to i8*
  %_100017 = bitcast i8* %_100005 to i8**
  %_90002 = load i8*, i8** %_100017, !dereferenceable_or_null !{i64 16}
  br label %_100000.0
_100000.0:
  %_100001 = phi i8* [%_90002, %_90000.0], [%_80002, %_80000.0], [%_70002, %_70000.0], [%_60002, %_60000.0], [%_50001, %_50000.0]
  ret i8* %_100001
}

define dereferenceable_or_null(16) i8* @"_SM29java.nio.charset.CoderResult$D22malformedForLengthImpliL28java.nio.charset.CoderResultEPT29java.nio.charset.CoderResult$"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_30026 = bitcast i8* %_30001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_30027 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_30026, i32 0, i32 6
  %_30011 = bitcast i8** %_30027 to i8*
  %_30028 = bitcast i8* %_30011 to i8**
  %_30002 = load i8*, i8** %_30028, !dereferenceable_or_null !{i64 8}
  %_30005 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_2)
  %_30006 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM39java.nio.charset.CoderResult$$$Lambda$1G4type" to i8*), i64 24)
  %_30029 = bitcast i8* %_30006 to { i8*, i8*, i32 }*
  %_30030 = getelementptr { i8*, i8*, i32 }, { i8*, i8*, i32 }* %_30029, i32 0, i32 2
  %_30013 = bitcast i32* %_30030 to i8*
  %_30031 = bitcast i8* %_30013 to i32*
  store i32 %_2, i32* %_30031
  %_30032 = bitcast i8* %_30006 to { i8*, i8*, i32 }*
  %_30033 = getelementptr { i8*, i8*, i32 }, { i8*, i8*, i32 }* %_30032, i32 0, i32 1
  %_30015 = bitcast i8** %_30033 to i8*
  %_30034 = bitcast i8* %_30015 to i8**
  store i8* %_1, i8** %_30034
  %_30035 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM32scala.collection.mutable.HashMapD15getOrElseUpdateL16java.lang.ObjectL15scala.Function0L16java.lang.ObjectEO" to i8*) to i8* (i8*, i8*, i8*)*
  %_30009 = call dereferenceable_or_null(8) i8* %_30035(i8* dereferenceable_or_null(8) %_30002, i8* nonnull dereferenceable(16) %_30005, i8* nonnull dereferenceable(24) %_30006)
  %_30019 = icmp eq i8* %_30009, null
  br i1 %_30019, label %_30017.0, label %_30016.0
_30016.0:
  %_30036 = bitcast i8* %_30009 to i8**
  %_30020 = load i8*, i8** %_30036
  %_30021 = icmp eq i8* %_30020, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*)
  br i1 %_30021, label %_30017.0, label %_30018.0
_30017.0:
  %_30010 = bitcast i8* %_30009 to i8*
  ret i8* %_30010
_30018.0:
  %_30022 = phi i8* [%_30009, %_30016.0]
  %_30023 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), %_30016.0]
  %_30037 = bitcast i8* %_30022 to i8**
  %_30024 = load i8*, i8** %_30037
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_30024, i8* %_30023)
  unreachable
}

define nonnull dereferenceable(16) i8* @"_SM29java.nio.charset.CoderResult$D33malformedForLengthImpl$$anonfun$1iL28java.nio.charset.CoderResultEPT29java.nio.charset.CoderResult$"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_30009 = bitcast i8* %_30002 to { i8*, i32, i32 }*
  %_30010 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_30009, i32 0, i32 1
  %_30006 = bitcast i32* %_30010 to i8*
  %_30011 = bitcast i8* %_30006 to i32*
  store i32 %_2, i32* %_30011
  %_30012 = bitcast i8* %_30002 to { i8*, i32, i32 }*
  %_30013 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_30012, i32 0, i32 2
  %_30008 = bitcast i32* %_30013 to i8*
  %_30014 = bitcast i8* %_30008 to i32*
  store i32 2, i32* %_30014
  ret i8* %_30002
}

define dereferenceable_or_null(16) i8* @"_SM29java.nio.charset.CoderResult$D8OVERFLOWL28java.nio.charset.CoderResultEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_20004 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20005 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20004, i32 0, i32 1
  %_20003 = bitcast i8** %_20005 to i8*
  %_20006 = bitcast i8* %_20003 to i8**
  %_20002 = load i8*, i8** %_20006, !dereferenceable_or_null !{i64 16}
  ret i8* %_20002
}

define dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 84
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 104}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM29java.nio.charset.CoderResult$G4type" to i8*), i64 104)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM29java.nio.charset.CoderResult$RE"(i8* dereferenceable_or_null(104) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM29java.nio.charset.CoderResult$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(104) i8* @"_SM29java.nio.charset.CoderResult$G4load"()
  %_20003 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_20141 = bitcast i8* %_20003 to { i8*, i32, i32 }*
  %_20142 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20141, i32 0, i32 1
  %_20059 = bitcast i32* %_20142 to i8*
  %_20143 = bitcast i8* %_20059 to i32*
  store i32 -1, i32* %_20143
  %_20144 = bitcast i8* %_20003 to { i8*, i32, i32 }*
  %_20145 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20144, i32 0, i32 2
  %_20061 = bitcast i32* %_20145 to i8*
  %_20146 = bitcast i8* %_20061 to i32*
  store i32 1, i32* %_20146
  %_20147 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20148 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20147, i32 0, i32 1
  %_20063 = bitcast i8** %_20148 to i8*
  %_20149 = bitcast i8* %_20063 to i8**
  store i8* %_20003, i8** %_20149
  %_20008 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_20150 = bitcast i8* %_20008 to { i8*, i32, i32 }*
  %_20151 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20150, i32 0, i32 1
  %_20065 = bitcast i32* %_20151 to i8*
  %_20152 = bitcast i8* %_20065 to i32*
  store i32 -1, i32* %_20152
  %_20153 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20154 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20153, i32 0, i32 12
  %_20067 = bitcast i8** %_20154 to i8*
  %_20155 = bitcast i8* %_20067 to i8**
  store i8* %_20008, i8** %_20155
  %_20012 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_20156 = bitcast i8* %_20012 to { i8*, i32, i32 }*
  %_20157 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20156, i32 0, i32 1
  %_20069 = bitcast i32* %_20157 to i8*
  %_20158 = bitcast i8* %_20069 to i32*
  store i32 1, i32* %_20158
  %_20159 = bitcast i8* %_20012 to { i8*, i32, i32 }*
  %_20160 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20159, i32 0, i32 2
  %_20071 = bitcast i32* %_20160 to i8*
  %_20161 = bitcast i8* %_20071 to i32*
  store i32 2, i32* %_20161
  %_20162 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20163 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20162, i32 0, i32 5
  %_20073 = bitcast i8** %_20163 to i8*
  %_20164 = bitcast i8* %_20073 to i8**
  store i8* %_20012, i8** %_20164
  %_20017 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_20165 = bitcast i8* %_20017 to { i8*, i32, i32 }*
  %_20166 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20165, i32 0, i32 1
  %_20075 = bitcast i32* %_20166 to i8*
  %_20167 = bitcast i8* %_20075 to i32*
  store i32 2, i32* %_20167
  %_20168 = bitcast i8* %_20017 to { i8*, i32, i32 }*
  %_20169 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20168, i32 0, i32 2
  %_20077 = bitcast i32* %_20169 to i8*
  %_20170 = bitcast i8* %_20077 to i32*
  store i32 2, i32* %_20170
  %_20171 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20172 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20171, i32 0, i32 4
  %_20079 = bitcast i8** %_20172 to i8*
  %_20173 = bitcast i8* %_20079 to i8**
  store i8* %_20017, i8** %_20173
  %_20022 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_20174 = bitcast i8* %_20022 to { i8*, i32, i32 }*
  %_20175 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20174, i32 0, i32 1
  %_20081 = bitcast i32* %_20175 to i8*
  %_20176 = bitcast i8* %_20081 to i32*
  store i32 3, i32* %_20176
  %_20177 = bitcast i8* %_20022 to { i8*, i32, i32 }*
  %_20178 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20177, i32 0, i32 2
  %_20083 = bitcast i32* %_20178 to i8*
  %_20179 = bitcast i8* %_20083 to i32*
  store i32 2, i32* %_20179
  %_20180 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20181 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20180, i32 0, i32 3
  %_20085 = bitcast i8** %_20181 to i8*
  %_20182 = bitcast i8* %_20085 to i8**
  store i8* %_20022, i8** %_20182
  %_20027 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_20183 = bitcast i8* %_20027 to { i8*, i32, i32 }*
  %_20184 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20183, i32 0, i32 1
  %_20087 = bitcast i32* %_20184 to i8*
  %_20185 = bitcast i8* %_20087 to i32*
  store i32 4, i32* %_20185
  %_20186 = bitcast i8* %_20027 to { i8*, i32, i32 }*
  %_20187 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20186, i32 0, i32 2
  %_20089 = bitcast i32* %_20187 to i8*
  %_20188 = bitcast i8* %_20089 to i32*
  store i32 2, i32* %_20188
  %_20189 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20190 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20189, i32 0, i32 2
  %_20091 = bitcast i8** %_20190 to i8*
  %_20191 = bitcast i8* %_20091 to i8**
  store i8* %_20027, i8** %_20191
  %_20031 = call dereferenceable_or_null(16) i8* @"_SM29scala.collection.mutable.Map$G4load"()
  %_20032 = call dereferenceable_or_null(8) i8* @"_SM36scala.collection.MapFactory$DelegateD5emptyL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_20031)
  %_20095 = icmp eq i8* %_20032, null
  br i1 %_20095, label %_20093.0, label %_20092.0
_20092.0:
  %_20192 = bitcast i8* %_20032 to i8**
  %_20096 = load i8*, i8** %_20192
  %_20193 = bitcast i8* %_20096 to { i8*, i32, i32, i8* }*
  %_20194 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_20193, i32 0, i32 1
  %_20097 = bitcast i32* %_20194 to i8*
  %_20195 = bitcast i8* %_20097 to i32*
  %_20098 = load i32, i32* %_20195
  %_20196 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_20197 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_20196, i32 0, i32 %_20098, i32 53
  %_20099 = bitcast i1* %_20197 to i8*
  %_20198 = bitcast i8* %_20099 to i1*
  %_20100 = load i1, i1* %_20198
  br i1 %_20100, label %_20093.0, label %_20094.0
_20093.0:
  %_20033 = bitcast i8* %_20032 to i8*
  %_20199 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20200 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20199, i32 0, i32 6
  %_20102 = bitcast i8** %_20200 to i8*
  %_20201 = bitcast i8* %_20102 to i8**
  store i8* %_20033, i8** %_20201
  %_20036 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_20202 = bitcast i8* %_20036 to { i8*, i32, i32 }*
  %_20203 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20202, i32 0, i32 1
  %_20104 = bitcast i32* %_20203 to i8*
  %_20204 = bitcast i8* %_20104 to i32*
  store i32 1, i32* %_20204
  %_20205 = bitcast i8* %_20036 to { i8*, i32, i32 }*
  %_20206 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20205, i32 0, i32 2
  %_20106 = bitcast i32* %_20206 to i8*
  %_20207 = bitcast i8* %_20106 to i32*
  store i32 3, i32* %_20207
  %_20208 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20209 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20208, i32 0, i32 11
  %_20108 = bitcast i8** %_20209 to i8*
  %_20210 = bitcast i8* %_20108 to i8**
  store i8* %_20036, i8** %_20210
  %_20041 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_20211 = bitcast i8* %_20041 to { i8*, i32, i32 }*
  %_20212 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20211, i32 0, i32 1
  %_20110 = bitcast i32* %_20212 to i8*
  %_20213 = bitcast i8* %_20110 to i32*
  store i32 2, i32* %_20213
  %_20214 = bitcast i8* %_20041 to { i8*, i32, i32 }*
  %_20215 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20214, i32 0, i32 2
  %_20112 = bitcast i32* %_20215 to i8*
  %_20216 = bitcast i8* %_20112 to i32*
  store i32 3, i32* %_20216
  %_20217 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20218 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20217, i32 0, i32 10
  %_20114 = bitcast i8** %_20218 to i8*
  %_20219 = bitcast i8* %_20114 to i8**
  store i8* %_20041, i8** %_20219
  %_20046 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_20220 = bitcast i8* %_20046 to { i8*, i32, i32 }*
  %_20221 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20220, i32 0, i32 1
  %_20116 = bitcast i32* %_20221 to i8*
  %_20222 = bitcast i8* %_20116 to i32*
  store i32 3, i32* %_20222
  %_20223 = bitcast i8* %_20046 to { i8*, i32, i32 }*
  %_20224 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20223, i32 0, i32 2
  %_20118 = bitcast i32* %_20224 to i8*
  %_20225 = bitcast i8* %_20118 to i32*
  store i32 3, i32* %_20225
  %_20226 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20227 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20226, i32 0, i32 9
  %_20120 = bitcast i8** %_20227 to i8*
  %_20228 = bitcast i8* %_20120 to i8**
  store i8* %_20046, i8** %_20228
  %_20051 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM28java.nio.charset.CoderResultG4type" to i8*), i64 16)
  %_20229 = bitcast i8* %_20051 to { i8*, i32, i32 }*
  %_20230 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20229, i32 0, i32 1
  %_20122 = bitcast i32* %_20230 to i8*
  %_20231 = bitcast i8* %_20122 to i32*
  store i32 4, i32* %_20231
  %_20232 = bitcast i8* %_20051 to { i8*, i32, i32 }*
  %_20233 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_20232, i32 0, i32 2
  %_20124 = bitcast i32* %_20233 to i8*
  %_20234 = bitcast i8* %_20124 to i32*
  store i32 3, i32* %_20234
  %_20235 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20236 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20235, i32 0, i32 8
  %_20126 = bitcast i8** %_20236 to i8*
  %_20237 = bitcast i8* %_20126 to i8**
  store i8* %_20051, i8** %_20237
  %_20055 = call dereferenceable_or_null(8) i8* @"_SM36scala.collection.MapFactory$DelegateD5emptyL16java.lang.ObjectEO"(i8* nonnull dereferenceable(16) %_20031)
  %_20129 = icmp eq i8* %_20055, null
  br i1 %_20129, label %_20128.0, label %_20127.0
_20127.0:
  %_20238 = bitcast i8* %_20055 to i8**
  %_20130 = load i8*, i8** %_20238
  %_20239 = bitcast i8* %_20130 to { i8*, i32, i32, i8* }*
  %_20240 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_20239, i32 0, i32 1
  %_20131 = bitcast i32* %_20240 to i8*
  %_20241 = bitcast i8* %_20131 to i32*
  %_20132 = load i32, i32* %_20241
  %_20242 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_20243 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_20242, i32 0, i32 %_20132, i32 53
  %_20133 = bitcast i1* %_20243 to i8*
  %_20244 = bitcast i8* %_20133 to i1*
  %_20134 = load i1, i1* %_20244
  br i1 %_20134, label %_20128.0, label %_20094.0
_20128.0:
  %_20056 = bitcast i8* %_20055 to i8*
  %_20245 = bitcast i8* %_20001 to { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }*
  %_20246 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8* }* %_20245, i32 0, i32 7
  %_20136 = bitcast i8** %_20246 to i8*
  %_20247 = bitcast i8* %_20136 to i8**
  store i8* %_20056, i8** %_20247
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_20094.0:
  %_20137 = phi i8* [%_20032, %_20092.0], [%_20055, %_20127.0]
  %_20138 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM28scala.collection.mutable.MapG4type" to i8*), %_20092.0], [bitcast ({ i8*, i32, i32, i8* }* @"_SM28scala.collection.mutable.MapG4type" to i8*), %_20127.0]
  %_20248 = bitcast i8* %_20137 to i8**
  %_20139 = load i8*, i8** %_20248
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_20139, i8* %_20138)
  unreachable
}

define nonnull dereferenceable(40) i8* @"_SM29java.nio.file.PosixException$D5applyL16java.lang.StringiL19java.io.IOExceptionEO"(i8* %_1, i8* %_2, i32 %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  br label %_50000.0
_50000.0:
  %_50002 = call i32 @"scalanative_enotdir"()
  %_50004 = icmp eq i32 %_3, %_50002
  br i1 %_50004, label %_60000.0, label %_70000.0
_60000.0:
  br label %_170000.0
_170000.0:
  %_170001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM35java.nio.file.NotDirectoryExceptionG4type" to i8*), i64 64)
  %_700031 = bitcast i8* %_170001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700032 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700031, i32 0, i32 5
  %_700002 = bitcast i1* %_700032 to i8*
  %_700033 = bitcast i8* %_700002 to i1*
  store i1 true, i1* %_700033
  %_700034 = bitcast i8* %_170001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700035 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700034, i32 0, i32 4
  %_700004 = bitcast i1* %_700035 to i8*
  %_700036 = bitcast i8* %_700004 to i1*
  store i1 true, i1* %_700036
  %_700037 = bitcast i8* %_170001 to { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }*
  %_700038 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }* %_700037, i32 0, i32 6
  %_700006 = bitcast i8** %_700038 to i8*
  %_700039 = bitcast i8* %_700006 to i8**
  store i8* %_2, i8** %_700039
  %_170005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(64) %_170001)
  br label %_180000.0
_180000.0:
  br label %_190000.0
_70000.0:
  br label %_200000.0
_200000.0:
  %_200001 = call i32 @"scalanative_eacces"()
  %_200003 = icmp eq i32 %_3, %_200001
  br i1 %_200003, label %_210000.0, label %_220000.0
_210000.0:
  br label %_320000.0
_320000.0:
  %_320001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM35java.nio.file.AccessDeniedExceptionG4type" to i8*), i64 64)
  %_700040 = bitcast i8* %_320001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700041 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700040, i32 0, i32 5
  %_700008 = bitcast i1* %_700041 to i8*
  %_700042 = bitcast i8* %_700008 to i1*
  store i1 true, i1* %_700042
  %_700043 = bitcast i8* %_320001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700044 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700043, i32 0, i32 4
  %_700010 = bitcast i1* %_700044 to i8*
  %_700045 = bitcast i8* %_700010 to i1*
  store i1 true, i1* %_700045
  %_700046 = bitcast i8* %_320001 to { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }*
  %_700047 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }* %_700046, i32 0, i32 6
  %_700012 = bitcast i8** %_700047 to i8*
  %_700048 = bitcast i8* %_700012 to i8**
  store i8* %_2, i8** %_700048
  %_320005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(64) %_320001)
  br label %_330000.0
_330000.0:
  br label %_190000.0
_220000.0:
  br label %_340000.0
_340000.0:
  %_340001 = call i32 @"scalanative_enoent"()
  %_340003 = icmp eq i32 %_3, %_340001
  br i1 %_340003, label %_350000.0, label %_360000.0
_350000.0:
  br label %_460000.0
_460000.0:
  %_460001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM33java.nio.file.NoSuchFileExceptionG4type" to i8*), i64 64)
  %_700049 = bitcast i8* %_460001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700050 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700049, i32 0, i32 5
  %_700014 = bitcast i1* %_700050 to i8*
  %_700051 = bitcast i8* %_700014 to i1*
  store i1 true, i1* %_700051
  %_700052 = bitcast i8* %_460001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700053 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700052, i32 0, i32 4
  %_700016 = bitcast i1* %_700053 to i8*
  %_700054 = bitcast i8* %_700016 to i1*
  store i1 true, i1* %_700054
  %_700055 = bitcast i8* %_460001 to { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }*
  %_700056 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }* %_700055, i32 0, i32 6
  %_700018 = bitcast i8** %_700056 to i8*
  %_700057 = bitcast i8* %_700018 to i8**
  store i8* %_2, i8** %_700057
  %_460005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(64) %_460001)
  br label %_470000.0
_470000.0:
  br label %_190000.0
_360000.0:
  br label %_480000.0
_480000.0:
  %_480001 = call i32 @"scalanative_eexist"()
  %_480003 = icmp eq i32 %_3, %_480001
  br i1 %_480003, label %_490000.0, label %_500000.0
_490000.0:
  br label %_600000.0
_600000.0:
  %_600001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM40java.nio.file.FileAlreadyExistsExceptionG4type" to i8*), i64 64)
  %_700058 = bitcast i8* %_600001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700059 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700058, i32 0, i32 5
  %_700020 = bitcast i1* %_700059 to i8*
  %_700060 = bitcast i8* %_700020 to i1*
  store i1 true, i1* %_700060
  %_700061 = bitcast i8* %_600001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700062 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700061, i32 0, i32 4
  %_700022 = bitcast i1* %_700062 to i8*
  %_700063 = bitcast i8* %_700022 to i1*
  store i1 true, i1* %_700063
  %_700064 = bitcast i8* %_600001 to { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }*
  %_700065 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i8* }* %_700064, i32 0, i32 6
  %_700024 = bitcast i8** %_700065 to i8*
  %_700066 = bitcast i8* %_700024 to i8**
  store i8* %_2, i8** %_700066
  %_600005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(64) %_600001)
  br label %_610000.0
_610000.0:
  br label %_190000.0
_500000.0:
  br label %_620000.0
_620000.0:
  %_620004 = call i8* @"strerror"(i32 %_3)
  %_620006 = call dereferenceable_or_null(16) i8* @"_SM33scala.scalanative.unsafe.package$G4load"()
  %_620007 = call dereferenceable_or_null(32) i8* @"_SM33scala.scalanative.unsafe.package$D21fromCString$default$2L24java.nio.charset.CharsetEO"(i8* nonnull dereferenceable(16) %_620006)
  %_700067 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D8boxToPtrR_L28scala.scalanative.unsafe.PtrEO" to i8*) to i8* (i8*, i8*)*
  %_620008 = call dereferenceable_or_null(16) i8* %_700067(i8* null, i8* %_620004)
  %_700068 = bitcast i8* bitcast (i8* (i8*, i8*, i8*)* @"_SM33scala.scalanative.unsafe.package$D11fromCStringL28scala.scalanative.unsafe.PtrL24java.nio.charset.CharsetL16java.lang.StringEO" to i8*) to i8* (i8*, i8*, i8*)*
  %_620009 = call dereferenceable_or_null(32) i8* %_700068(i8* nonnull dereferenceable(16) %_620006, i8* dereferenceable_or_null(16) %_620008, i8* dereferenceable_or_null(32) %_620007)
  br label %_690000.0
_690000.0:
  %_690001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM19java.io.IOExceptionG4type" to i8*), i64 40)
  %_700069 = bitcast i8* %_690001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700070 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700069, i32 0, i32 5
  %_700026 = bitcast i1* %_700070 to i8*
  %_700071 = bitcast i8* %_700026 to i1*
  store i1 true, i1* %_700071
  %_700072 = bitcast i8* %_690001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700073 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700072, i32 0, i32 4
  %_700028 = bitcast i1* %_700073 to i8*
  %_700074 = bitcast i8* %_700028 to i1*
  store i1 true, i1* %_700074
  %_700075 = bitcast i8* %_690001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_700076 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_700075, i32 0, i32 3
  %_700030 = bitcast i8** %_700076 to i8*
  %_700077 = bitcast i8* %_700030 to i8**
  store i8* %_620009, i8** %_700077
  %_690005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_690001)
  br label %_700000.0
_700000.0:
  br label %_190000.0
_190000.0:
  %_190001 = phi i8* [%_690001, %_700000.0], [%_600001, %_610000.0], [%_460001, %_470000.0], [%_320001, %_330000.0], [%_170001, %_180000.0]
  ret i8* %_190001
}

define nonnull dereferenceable(8) i8* @"_SM29java.nio.file.PosixException$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM29java.util.Hashtable$$Lambda$1D5applyL16java.lang.ObjectEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = bitcast i8* bitcast (i8* ()* @"_SM19java.util.HashtableD14get$$anonfun$1L16java.lang.ObjectEpT19java.util.Hashtable" to i8*) to i8* ()*
  %_20001 = call dereferenceable_or_null(8) i8* %_20002()
  ret i8* %_20001
}

define i32 @"_SM30java.math.RoundingMode$$anon$7D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM30java.math.RoundingMode$$anon$7D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM30java.math.RoundingMode$$anon$7D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define nonnull dereferenceable(16) i8* @"_SM30scala.collection.Map$$Lambda$1D5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30010 = icmp ne i8* %_1, null
  br i1 %_30010, label %_30008.0, label %_30009.0
_30008.0:
  %_30016 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_30017 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_30016, i32 0, i32 2
  %_30011 = bitcast i8** %_30017 to i8*
  %_30018 = bitcast i8* %_30011 to i8**
  %_30002 = load i8*, i8** %_30018, !dereferenceable_or_null !{i64 8}
  %_30013 = icmp ne i8* %_1, null
  br i1 %_30013, label %_30012.0, label %_30009.0
_30012.0:
  %_30019 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_30020 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_30019, i32 0, i32 1
  %_30014 = bitcast i8** %_30020 to i8*
  %_30021 = bitcast i8* %_30014 to i8**
  %_30003 = load i8*, i8** %_30021, !dereferenceable_or_null !{i64 8}
  %_30004 = bitcast i8* %_2 to i8*
  %_30005 = call i1 @"_SM20scala.collection.MapD17$anonfun$equals$1L20scala.collection.MapL12scala.Tuple2zEPT20scala.collection.Map"(i8* dereferenceable_or_null(8) %_30002, i8* dereferenceable_or_null(8) %_30003, i8* dereferenceable_or_null(24) %_30004)
  %_30007 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToBooleanzL17java.lang.BooleanEO"(i8* null, i1 %_30005)
  ret i8* %_30007
_30009.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM30scala.collection.Map$$Lambda$1RL20scala.collection.MapL20scala.collection.MapE"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40006 = icmp ne i8* %_1, null
  br i1 %_40006, label %_40004.0, label %_40005.0
_40004.0:
  %_40013 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_40014 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_40013, i32 0, i32 2
  %_40007 = bitcast i8** %_40014 to i8*
  %_40015 = bitcast i8* %_40007 to i8**
  store i8* %_2, i8** %_40015
  %_40010 = icmp ne i8* %_1, null
  br i1 %_40010, label %_40009.0, label %_40005.0
_40009.0:
  %_40016 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_40017 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_40016, i32 0, i32 1
  %_40011 = bitcast i8** %_40017 to i8*
  %_40018 = bitcast i8* %_40011 to i8**
  store i8* %_3, i8** %_40018
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_40005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.MapD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i32 @"_SM32java.nio.file.LinkOption$$anon$1D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM32java.nio.file.LinkOption$$anon$1D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM32java.nio.file.LinkOption$$anon$1D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(16) i8* @"_SM32scala.collection.mutable.Buffer$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 139
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 16}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32scala.collection.mutable.Buffer$G4type" to i8*), i64 16)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.Buffer$RE"(i8* dereferenceable_or_null(16) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.Buffer$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.ArrayBuffer$G4load"()
  %_30007 = icmp ne i8* %_1, null
  br i1 %_30007, label %_30005.0, label %_30006.0
_30005.0:
  %_30012 = bitcast i8* %_1 to { i8*, i8* }*
  %_30013 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30012, i32 0, i32 1
  %_30008 = bitcast i8** %_30013 to i8*
  %_30014 = bitcast i8* %_30008 to i8**
  store i8* %_20001, i8** %_30014
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableFactoryD6$init$uEO"(i8* dereferenceable_or_null(16) %_1)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.SeqFactoryD6$init$uEO"(i8* dereferenceable_or_null(16) %_1)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_30006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.HashMapD12foreachEntryL15scala.Function2uEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_100003 = icmp ne i8* %_1, null
  br i1 %_100003, label %_100001.0, label %_100002.0
_100001.0:
  %_100025 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_100026 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_100025, i32 0, i32 4
  %_100004 = bitcast i8** %_100026 to i8*
  %_100027 = bitcast i8* %_100004 to i8**
  %_30002 = load i8*, i8** %_100027, !dereferenceable_or_null !{i64 8}
  %_100006 = icmp ne i8* %_30002, null
  br i1 %_100006, label %_100005.0, label %_100002.0
_100005.0:
  %_100028 = bitcast i8* %_30002 to { i8*, i32 }*
  %_100029 = getelementptr { i8*, i32 }, { i8*, i32 }* %_100028, i32 0, i32 1
  %_100007 = bitcast i32* %_100029 to i8*
  %_100030 = bitcast i8* %_100007 to i32*
  %_30003 = load i32, i32* %_100030
  br label %_40000.0
_40000.0:
  %_40001 = phi i32 [0, %_100005.0], [%_90002, %_90000.0]
  %_40003 = icmp slt i32 %_40001, %_30003
  br i1 %_40003, label %_50000.0, label %_60000.0
_50000.0:
  %_100009 = icmp ne i8* %_1, null
  br i1 %_100009, label %_100008.0, label %_100002.0
_100008.0:
  %_100031 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_100032 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_100031, i32 0, i32 4
  %_100010 = bitcast i8** %_100032 to i8*
  %_100033 = bitcast i8* %_100010 to i8**
  %_50001 = load i8*, i8** %_100033, !dereferenceable_or_null !{i64 8}
  %_100013 = icmp ne i8* %_50001, null
  br i1 %_100013, label %_100012.0, label %_100002.0
_100012.0:
  %_100034 = bitcast i8* %_50001 to { i8*, i32 }*
  %_100035 = getelementptr { i8*, i32 }, { i8*, i32 }* %_100034, i32 0, i32 1
  %_100014 = bitcast i32* %_100035 to i8*
  %_100036 = bitcast i8* %_100014 to i32*
  %_100011 = load i32, i32* %_100036
  %_100017 = icmp sge i32 %_40001, 0
  %_100018 = icmp slt i32 %_40001, %_100011
  %_100019 = and i1 %_100017, %_100018
  br i1 %_100019, label %_100015.0, label %_100016.0
_100015.0:
  %_100037 = bitcast i8* %_50001 to { i8*, i32, i32, [0 x i8*] }*
  %_100038 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_100037, i32 0, i32 3, i32 %_40001
  %_100020 = bitcast i8** %_100038 to i8*
  %_100039 = bitcast i8* %_100020 to i8**
  %_50002 = load i8*, i8** %_100039, !dereferenceable_or_null !{i64 40}
  %_50004 = icmp ne i8* %_50002, null
  br i1 %_50004, label %_70000.0, label %_80000.0
_70000.0:
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD12foreachEntryL15scala.Function2uEO"(i8* dereferenceable_or_null(40) %_50002, i8* dereferenceable_or_null(8) %_2)
  br label %_90000.0
_80000.0:
  br label %_90000.0
_90000.0:
  %_90002 = add i32 %_40001, 1
  br label %_40000.0
_60000.0:
  br label %_100000.0
_100000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_100002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_100016.0:
  %_100023 = phi i32 [%_40001, %_100012.0]
  %_100040 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_100040(i8* null, i32 %_100023)
  unreachable
}

define i32 @"_SM32scala.collection.mutable.HashMapD12newThresholdiiEPT32scala.collection.mutable.HashMap"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30010 = icmp ne i8* %_1, null
  br i1 %_30010, label %_30008.0, label %_30009.0
_30008.0:
  %_30024 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_30025 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_30024, i32 0, i32 1
  %_30011 = bitcast double* %_30025 to i8*
  %_30026 = bitcast i8* %_30011 to double*
  %_30002 = load double, double* %_30026
  %_30005 = sitofp i32 %_2 to double
  %_30006 = fmul double %_30005, %_30002
  %_30019 = fcmp une double %_30006, %_30006
  br i1 %_30019, label %_30012.0, label %_30013.0
_30012.0:
  br label %_30018.0
_30013.0:
  %_30020 = fcmp ole double %_30006, 0xc1e0000000000000
  br i1 %_30020, label %_30014.0, label %_30015.0
_30014.0:
  br label %_30018.0
_30015.0:
  %_30021 = fcmp oge double %_30006, 0x41dfffffffc00000
  br i1 %_30021, label %_30016.0, label %_30017.0
_30016.0:
  br label %_30018.0
_30017.0:
  %_30022 = fptosi double %_30006 to i32
  br label %_30018.0
_30018.0:
  %_30007 = phi i32 [%_30022, %_30017.0], [2147483647, %_30016.0], [-2147483648, %_30014.0], [zeroinitializer, %_30012.0]
  ret i32 %_30007
_30009.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM32scala.collection.mutable.HashMapD12nodeIteratorL25scala.collection.IteratorEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM32scala.collection.mutable.HashMapD4sizeiEO"(i8* dereferenceable_or_null(32) %_1)
  %_20003 = icmp eq i32 %_20001, 0
  br i1 %_20003, label %_30000.0, label %_40000.0
_30000.0:
  %_30001 = call dereferenceable_or_null(16) i8* @"_SM26scala.collection.Iterator$G4load"()
  %_30002 = call dereferenceable_or_null(8) i8* @"_SM26scala.collection.Iterator$D5emptyL25scala.collection.IteratorEO"(i8* nonnull dereferenceable(16) %_30001)
  br label %_50000.0
_40000.0:
  %_70002 = icmp eq i8* %_1, null
  br i1 %_70002, label %_80000.0, label %_90000.0
_90000.0:
  br label %_100000.0
_80000.0:
  %_110008 = icmp ne i8* null, null
  br i1 %_110008, label %_110006.0, label %_110007.0
_110006.0:
  call i8* @"scalanative_throw"(i8* null)
  unreachable
_100000.0:
  %_110001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM40scala.collection.mutable.HashMap$$anon$4G4type" to i8*), i64 40)
  %_110028 = bitcast i8* %_110001 to { i8*, i32, i8*, i32, i8* }*
  %_110029 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_110028, i32 0, i32 4
  %_110011 = bitcast i8** %_110029 to i8*
  %_110030 = bitcast i8* %_110011 to i8**
  store i8* %_1, i8** %_110030
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(40) %_110001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(40) %_110001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IteratorD6$init$uEO"(i8* nonnull dereferenceable(40) %_110001)
  %_110031 = bitcast i8* %_110001 to { i8*, i32, i8*, i32, i8* }*
  %_110032 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_110031, i32 0, i32 3
  %_110016 = bitcast i32* %_110032 to i8*
  %_110033 = bitcast i8* %_110016 to i32*
  store i32 0, i32* %_110033
  %_110034 = bitcast i8* %_110001 to { i8*, i32, i8*, i32, i8* }*
  %_110035 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_110034, i32 0, i32 2
  %_110018 = bitcast i8** %_110035 to i8*
  %_110036 = bitcast i8* %_110018 to i8**
  store i8* null, i8** %_110036
  %_110020 = icmp ne i8* %_1, null
  br i1 %_110020, label %_110019.0, label %_110007.0
_110019.0:
  %_110037 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_110038 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_110037, i32 0, i32 4
  %_110021 = bitcast i8** %_110038 to i8*
  %_110039 = bitcast i8* %_110021 to i8**
  %_100003 = load i8*, i8** %_110039, !dereferenceable_or_null !{i64 8}
  %_110023 = icmp ne i8* %_100003, null
  br i1 %_110023, label %_110022.0, label %_110007.0
_110022.0:
  %_110040 = bitcast i8* %_100003 to { i8*, i32 }*
  %_110041 = getelementptr { i8*, i32 }, { i8*, i32 }* %_110040, i32 0, i32 1
  %_110024 = bitcast i32* %_110041 to i8*
  %_110042 = bitcast i8* %_110024 to i32*
  %_100004 = load i32, i32* %_110042
  %_110043 = bitcast i8* %_110001 to { i8*, i32, i8*, i32, i8* }*
  %_110044 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_110043, i32 0, i32 1
  %_110026 = bitcast i32* %_110044 to i8*
  %_110045 = bitcast i8* %_110026 to i32*
  store i32 %_100004, i32* %_110045
  br label %_50000.0
_50000.0:
  %_50001 = phi i8* [%_110001, %_110022.0], [%_30002, %_30000.0]
  ret i8* %_50001
_110007.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(32) i8* @"_SM32scala.collection.mutable.HashMapD12stringPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-271" to i8*)
}

define i32 @"_SM32scala.collection.mutable.HashMapD12tableSizeForiiEPT32scala.collection.mutable.HashMap"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30004 = call dereferenceable_or_null(16) i8* @"_SM13scala.Predef$G4load"()
  %_30005 = sub i32 %_2, 1
  %_30006 = call i32 @"_SM26scala.LowPriorityImplicitsD10intWrapperiiEO"(i8* nonnull dereferenceable(16) %_30004, i32 %_30005)
  %_30007 = call i32 @"_SM22scala.runtime.RichInt$D13max$extensioniiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM22scala.runtime.RichInt$G8instance" to i8*), i32 %_30006, i32 4)
  %_30008 = call i32 @"_SM17java.lang.IntegerD13highestOneBitiiEo"(i32 %_30007)
  %_30013 = and i32 1, 31
  %_30010 = shl i32 %_30008, %_30013
  %_30011 = call i32 @"_SM26scala.LowPriorityImplicitsD10intWrapperiiEO"(i8* nonnull dereferenceable(16) %_30004, i32 %_30010)
  %_30012 = call i32 @"_SM22scala.runtime.RichInt$D13min$extensioniiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM22scala.runtime.RichInt$G8instance" to i8*), i32 %_30011, i32 1073741824)
  ret i32 %_30012
}

define i32 @"_SM32scala.collection.mutable.HashMapD13unimproveHashiiEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i32 @"_SM32scala.collection.mutable.HashMapD45scala$collection$mutable$HashMap$$improveHashiiEO"(i8* dereferenceable_or_null(32) %_1, i32 %_2)
  ret i32 %_30001
}

define dereferenceable_or_null(8) i8* @"_SM32scala.collection.mutable.HashMapD15getOrElseUpdateL16java.lang.ObjectL15scala.Function0L16java.lang.ObjectEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.ObjectD8getClassL15java.lang.ClassEO"(i8* dereferenceable_or_null(32) %_1)
  %_40003 = icmp eq i8* %_40001, null
  br i1 %_40003, label %_50000.0, label %_60000.0
_50000.0:
  %_50002 = icmp eq i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM32scala.collection.mutable.HashMapG4type" to i8*), null
  br label %_70000.0
_60000.0:
  %_240047 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM15java.lang.ClassD6equalsL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_60001 = call i1 %_240047(i8* dereferenceable_or_null(32) %_40001, i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM32scala.collection.mutable.HashMapG4type" to i8*))
  br label %_70000.0
_70000.0:
  %_70001 = phi i1 [%_60001, %_60000.0], [%_50002, %_50000.0]
  %_70003 = xor i1 %_70001, true
  br i1 %_70003, label %_80000.0, label %_90000.0
_80000.0:
  %_80001 = call dereferenceable_or_null(8) i8* @"_SM31scala.collection.mutable.MapOpsD15getOrElseUpdateL16java.lang.ObjectL15scala.Function0L16java.lang.ObjectEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_3)
  br label %_100000.0
_90000.0:
  %_90001 = call i32 @"_SM32scala.collection.mutable.HashMapD45scala$collection$mutable$HashMap$$computeHashL16java.lang.ObjectiEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2)
  %_90002 = call i32 @"_SM32scala.collection.mutable.HashMapD39scala$collection$mutable$HashMap$$indexiiEO"(i8* dereferenceable_or_null(32) %_1, i32 %_90001)
  %_240005 = icmp ne i8* %_1, null
  br i1 %_240005, label %_240003.0, label %_240004.0
_240003.0:
  %_240048 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_240049 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_240048, i32 0, i32 4
  %_240006 = bitcast i8** %_240049 to i8*
  %_240050 = bitcast i8* %_240006 to i8**
  %_90003 = load i8*, i8** %_240050, !dereferenceable_or_null !{i64 8}
  %_240009 = icmp ne i8* %_90003, null
  br i1 %_240009, label %_240008.0, label %_240004.0
_240008.0:
  %_240051 = bitcast i8* %_90003 to { i8*, i32 }*
  %_240052 = getelementptr { i8*, i32 }, { i8*, i32 }* %_240051, i32 0, i32 1
  %_240010 = bitcast i32* %_240052 to i8*
  %_240053 = bitcast i8* %_240010 to i32*
  %_240007 = load i32, i32* %_240053
  %_240013 = icmp sge i32 %_90002, 0
  %_240014 = icmp slt i32 %_90002, %_240007
  %_240015 = and i1 %_240013, %_240014
  br i1 %_240015, label %_240011.0, label %_240012.0
_240011.0:
  %_240054 = bitcast i8* %_90003 to { i8*, i32, i32, [0 x i8*] }*
  %_240055 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_240054, i32 0, i32 3, i32 %_90002
  %_240016 = bitcast i8** %_240055 to i8*
  %_240056 = bitcast i8* %_240016 to i8**
  %_90004 = load i8*, i8** %_240056, !dereferenceable_or_null !{i64 40}
  br label %_110000.0
_110000.0:
  %_110002 = icmp eq i8* %_90004, null
  br i1 %_110002, label %_120000.0, label %_130000.0
_120000.0:
  br label %_140000.0
_130000.0:
  br label %_150000.0
_150000.0:
  %_150001 = call dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8findNodeL16java.lang.ObjectiL37scala.collection.mutable.HashMap$NodeEO"(i8* dereferenceable_or_null(40) %_90004, i8* dereferenceable_or_null(8) %_2, i32 %_90001)
  br label %_140000.0
_140000.0:
  %_140001 = phi i8* [%_150001, %_150000.0], [null, %_120000.0]
  %_140003 = icmp ne i8* %_140001, null
  br i1 %_140003, label %_160000.0, label %_170000.0
_160000.0:
  %_160001 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD5valueL16java.lang.ObjectEO"(i8* dereferenceable_or_null(40) %_140001)
  br label %_180000.0
_170000.0:
  %_240018 = icmp ne i8* %_1, null
  br i1 %_240018, label %_240017.0, label %_240004.0
_240017.0:
  %_240057 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_240058 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_240057, i32 0, i32 4
  %_240019 = bitcast i8** %_240058 to i8*
  %_240059 = bitcast i8* %_240019 to i8**
  %_170001 = load i8*, i8** %_240059, !dereferenceable_or_null !{i64 8}
  %_240021 = icmp ne i8* %_3, null
  br i1 %_240021, label %_240020.0, label %_240004.0
_240020.0:
  %_240060 = bitcast i8* %_3 to i8**
  %_240022 = load i8*, i8** %_240060
  %_240061 = bitcast i8* %_240022 to { i8*, i32, i32, i8* }*
  %_240062 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_240061, i32 0, i32 2
  %_240023 = bitcast i32* %_240062 to i8*
  %_240063 = bitcast i8* %_240023 to i32*
  %_240024 = load i32, i32* %_240063
  %_240064 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_240065 = getelementptr i8*, i8** %_240064, i32 1529
  %_240025 = bitcast i8** %_240065 to i8*
  %_240066 = bitcast i8* %_240025 to i8**
  %_240067 = getelementptr i8*, i8** %_240066, i32 %_240024
  %_240026 = bitcast i8** %_240067 to i8*
  %_240068 = bitcast i8* %_240026 to i8**
  %_170003 = load i8*, i8** %_240068
  %_240069 = bitcast i8* %_170003 to i8* (i8*)*
  %_170004 = call dereferenceable_or_null(8) i8* %_240069(i8* dereferenceable_or_null(8) %_3)
  %_240028 = icmp ne i8* %_1, null
  br i1 %_240028, label %_240027.0, label %_240004.0
_240027.0:
  %_240070 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_240071 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_240070, i32 0, i32 3
  %_240029 = bitcast i32* %_240071 to i8*
  %_240072 = bitcast i8* %_240029 to i32*
  %_170005 = load i32, i32* %_240072
  %_240031 = icmp ne i8* %_1, null
  br i1 %_240031, label %_240030.0, label %_240004.0
_240030.0:
  %_240073 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_240074 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_240073, i32 0, i32 2
  %_240032 = bitcast i32* %_240074 to i8*
  %_240075 = bitcast i8* %_240032 to i32*
  %_170007 = load i32, i32* %_240075
  %_170009 = add i32 %_170005, 1
  %_170010 = icmp sge i32 %_170009, %_170007
  br i1 %_170010, label %_190000.0, label %_200000.0
_190000.0:
  %_240034 = icmp ne i8* %_1, null
  br i1 %_240034, label %_240033.0, label %_240004.0
_240033.0:
  %_240076 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_240077 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_240076, i32 0, i32 4
  %_240035 = bitcast i8** %_240077 to i8*
  %_240078 = bitcast i8* %_240035 to i8**
  %_190001 = load i8*, i8** %_240078, !dereferenceable_or_null !{i64 8}
  %_240037 = icmp ne i8* %_190001, null
  br i1 %_240037, label %_240036.0, label %_240004.0
_240036.0:
  %_240079 = bitcast i8* %_190001 to { i8*, i32 }*
  %_240080 = getelementptr { i8*, i32 }, { i8*, i32 }* %_240079, i32 0, i32 1
  %_240038 = bitcast i32* %_240080 to i8*
  %_240081 = bitcast i8* %_240038 to i32*
  %_190002 = load i32, i32* %_240081
  %_240039 = and i32 1, 31
  %_190004 = shl i32 %_190002, %_240039
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.HashMapD9growTableiuEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i32 %_190004)
  br label %_210000.0
_200000.0:
  br label %_210000.0
_210000.0:
  %_240042 = icmp ne i8* %_1, null
  br i1 %_240042, label %_240041.0, label %_240004.0
_240041.0:
  %_240082 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_240083 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_240082, i32 0, i32 4
  %_240043 = bitcast i8** %_240083 to i8*
  %_240084 = bitcast i8* %_240043 to i8**
  %_210001 = load i8*, i8** %_240084, !dereferenceable_or_null !{i64 8}
  %_210003 = icmp eq i8* %_170001, %_210001
  br i1 %_210003, label %_220000.0, label %_230000.0
_220000.0:
  br label %_240000.0
_230000.0:
  %_230001 = call i32 @"_SM32scala.collection.mutable.HashMapD39scala$collection$mutable$HashMap$$indexiiEO"(i8* dereferenceable_or_null(32) %_1, i32 %_90001)
  br label %_240000.0
_240000.0:
  %_240001 = phi i32 [%_230001, %_230000.0], [%_90002, %_220000.0]
  %_240085 = bitcast i8* bitcast (i8* (i8*, i8*, i8*, i1, i32, i32)* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectziiL10scala.SomeEPT32scala.collection.mutable.HashMap" to i8*) to i8* (i8*, i8*, i8*, i1, i32, i32)*
  %_240002 = call dereferenceable_or_null(16) i8* %_240085(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_170004, i1 false, i32 %_90001, i32 %_240001)
  br label %_180000.0
_180000.0:
  %_180001 = phi i8* [%_170004, %_240000.0], [%_160001, %_160000.0]
  br label %_100000.0
_100000.0:
  %_100001 = phi i8* [%_180001, %_180000.0], [%_80001, %_80000.0]
  ret i8* %_100001
_240004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_240012.0:
  %_240045 = phi i32 [%_90002, %_240008.0]
  %_240086 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_240086(i8* null, i32 %_240045)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM32scala.collection.mutable.HashMapD17$anonfun$addAll$2L16java.lang.ObjectL16java.lang.ObjectL10scala.SomeEPT32scala.collection.mutable.HashMap"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call i32 @"_SM21scala.runtime.StaticsD7anyHashL16java.lang.ObjectiEo"(i8* dereferenceable_or_null(8) %_2)
  %_40002 = call i32 @"_SM32scala.collection.mutable.HashMapD45scala$collection$mutable$HashMap$$improveHashiiEO"(i8* dereferenceable_or_null(32) %_1, i32 %_40001)
  %_40003 = call dereferenceable_or_null(16) i8* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectizL10scala.SomeEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_3, i32 %_40002, i1 false)
  ret i8* %_40003
}

define dereferenceable_or_null(8) i8* @"_SM32scala.collection.mutable.HashMapD18strictOptimizedMapL32scala.collection.mutable.BuilderL15scala.Function1L16java.lang.ObjectEO"(i8* %_1, i8* %_2, i8* %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call dereferenceable_or_null(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD18strictOptimizedMapL32scala.collection.mutable.BuilderL15scala.Function1L16java.lang.ObjectEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_3)
  ret i8* %_40001
}

define i32 @"_SM32scala.collection.mutable.HashMapD39scala$collection$mutable$HashMap$$indexiiEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30009 = icmp ne i8* %_1, null
  br i1 %_30009, label %_30007.0, label %_30008.0
_30007.0:
  %_30015 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_30016 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_30015, i32 0, i32 4
  %_30010 = bitcast i8** %_30016 to i8*
  %_30017 = bitcast i8* %_30010 to i8**
  %_30001 = load i8*, i8** %_30017, !dereferenceable_or_null !{i64 8}
  %_30012 = icmp ne i8* %_30001, null
  br i1 %_30012, label %_30011.0, label %_30008.0
_30011.0:
  %_30018 = bitcast i8* %_30001 to { i8*, i32 }*
  %_30019 = getelementptr { i8*, i32 }, { i8*, i32 }* %_30018, i32 0, i32 1
  %_30013 = bitcast i32* %_30019 to i8*
  %_30020 = bitcast i8* %_30013 to i32*
  %_30002 = load i32, i32* %_30020
  %_30005 = sub i32 %_30002, 1
  %_30006 = and i32 %_2, %_30005
  ret i32 %_30006
_30008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.HashMapD3getL16java.lang.ObjectL12scala.OptionEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(40) i8* @"_SM32scala.collection.mutable.HashMapD8findNodeL16java.lang.ObjectL37scala.collection.mutable.HashMap$NodeEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2)
  br label %_40000.0
_40000.0:
  %_40002 = icmp eq i8* %_30001, null
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_50001 = call dereferenceable_or_null(8) i8* @"_SM11scala.None$G4load"()
  br label %_70000.0
_60000.0:
  br label %_80000.0
_80000.0:
  %_80002 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD5valueL16java.lang.ObjectEO"(i8* dereferenceable_or_null(40) %_30001)
  %_100001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM10scala.SomeG4type" to i8*), i64 16)
  %_100009 = bitcast i8* %_100001 to { i8*, i8* }*
  %_100010 = getelementptr { i8*, i8* }, { i8*, i8* }* %_100009, i32 0, i32 1
  %_100006 = bitcast i8** %_100010 to i8*
  %_100011 = bitcast i8* %_100006 to i8**
  store i8* %_80002, i8** %_100011
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_100001)
  call nonnull dereferenceable(8) i8* @"_SM13scala.ProductD6$init$uEO"(i8* nonnull dereferenceable(16) %_100001)
  br label %_70000.0
_70000.0:
  %_70001 = phi i8* [%_100001, %_80000.0], [%_50001, %_50000.0]
  ret i8* %_70001
}

define dereferenceable_or_null(8) i8* @"_SM32scala.collection.mutable.HashMapD3putL16java.lang.ObjectL16java.lang.ObjectL12scala.OptionEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call dereferenceable_or_null(16) i8* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectzL10scala.SomeEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_3, i1 true)
  br label %_50000.0
_50000.0:
  %_50002 = icmp eq i8* %_40001, null
  br i1 %_50002, label %_60000.0, label %_70000.0
_60000.0:
  %_60001 = call dereferenceable_or_null(8) i8* @"_SM11scala.None$G4load"()
  br label %_80000.0
_70000.0:
  br label %_90000.0
_90000.0:
  br label %_80000.0
_80000.0:
  %_80001 = phi i8* [%_40001, %_90000.0], [%_60001, %_60000.0]
  ret i8* %_80001
}

define i32 @"_SM32scala.collection.mutable.HashMapD45scala$collection$mutable$HashMap$$computeHashL16java.lang.ObjectiEO"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i32 @"_SM21scala.runtime.StaticsD7anyHashL16java.lang.ObjectiEo"(i8* dereferenceable_or_null(8) %_2)
  %_30002 = call i32 @"_SM32scala.collection.mutable.HashMapD45scala$collection$mutable$HashMap$$improveHashiiEO"(i8* dereferenceable_or_null(32) %_1, i32 %_30001)
  ret i32 %_30002
}

define i32 @"_SM32scala.collection.mutable.HashMapD45scala$collection$mutable$HashMap$$improveHashiiEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30005 = and i32 16, 31
  %_30003 = lshr i32 %_2, %_30005
  %_30004 = xor i32 %_2, %_30003
  ret i32 %_30004
}

define dereferenceable_or_null(16) i8* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectizL10scala.SomeEPT32scala.collection.mutable.HashMap"(i8* %_1, i8* %_2, i8* %_3, i32 %_4, i1 %_5) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  %_90005 = icmp ne i8* %_1, null
  br i1 %_90005, label %_90003.0, label %_90004.0
_90003.0:
  %_90019 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_90020 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_90019, i32 0, i32 3
  %_90006 = bitcast i32* %_90020 to i8*
  %_90021 = bitcast i8* %_90006 to i32*
  %_60001 = load i32, i32* %_90021
  %_90008 = icmp ne i8* %_1, null
  br i1 %_90008, label %_90007.0, label %_90004.0
_90007.0:
  %_90022 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_90023 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_90022, i32 0, i32 2
  %_90009 = bitcast i32* %_90023 to i8*
  %_90024 = bitcast i8* %_90009 to i32*
  %_60003 = load i32, i32* %_90024
  %_60005 = add i32 %_60001, 1
  %_60006 = icmp sge i32 %_60005, %_60003
  br i1 %_60006, label %_70000.0, label %_80000.0
_70000.0:
  %_90011 = icmp ne i8* %_1, null
  br i1 %_90011, label %_90010.0, label %_90004.0
_90010.0:
  %_90025 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_90026 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_90025, i32 0, i32 4
  %_90012 = bitcast i8** %_90026 to i8*
  %_90027 = bitcast i8* %_90012 to i8**
  %_70001 = load i8*, i8** %_90027, !dereferenceable_or_null !{i64 8}
  %_90014 = icmp ne i8* %_70001, null
  br i1 %_90014, label %_90013.0, label %_90004.0
_90013.0:
  %_90028 = bitcast i8* %_70001 to { i8*, i32 }*
  %_90029 = getelementptr { i8*, i32 }, { i8*, i32 }* %_90028, i32 0, i32 1
  %_90015 = bitcast i32* %_90029 to i8*
  %_90030 = bitcast i8* %_90015 to i32*
  %_70002 = load i32, i32* %_90030
  %_90016 = and i32 1, 31
  %_70004 = shl i32 %_70002, %_90016
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.HashMapD9growTableiuEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i32 %_70004)
  br label %_90000.0
_80000.0:
  br label %_90000.0
_90000.0:
  %_90001 = call i32 @"_SM32scala.collection.mutable.HashMapD39scala$collection$mutable$HashMap$$indexiiEO"(i8* dereferenceable_or_null(32) %_1, i32 %_4)
  %_90031 = bitcast i8* bitcast (i8* (i8*, i8*, i8*, i1, i32, i32)* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectziiL10scala.SomeEPT32scala.collection.mutable.HashMap" to i8*) to i8* (i8*, i8*, i8*, i1, i32, i32)*
  %_90002 = call dereferenceable_or_null(16) i8* %_90031(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_3, i1 %_5, i32 %_4, i32 %_90001)
  ret i8* %_90002
_90004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectzL10scala.SomeEPT32scala.collection.mutable.HashMap"(i8* %_1, i8* %_2, i8* %_3, i1 %_4) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_50000.0:
  %_80006 = icmp ne i8* %_1, null
  br i1 %_80006, label %_80004.0, label %_80005.0
_80004.0:
  %_80020 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_80021 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_80020, i32 0, i32 3
  %_80007 = bitcast i32* %_80021 to i8*
  %_80022 = bitcast i8* %_80007 to i32*
  %_50001 = load i32, i32* %_80022
  %_80009 = icmp ne i8* %_1, null
  br i1 %_80009, label %_80008.0, label %_80005.0
_80008.0:
  %_80023 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_80024 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_80023, i32 0, i32 2
  %_80010 = bitcast i32* %_80024 to i8*
  %_80025 = bitcast i8* %_80010 to i32*
  %_50003 = load i32, i32* %_80025
  %_50005 = add i32 %_50001, 1
  %_50006 = icmp sge i32 %_50005, %_50003
  br i1 %_50006, label %_60000.0, label %_70000.0
_60000.0:
  %_80012 = icmp ne i8* %_1, null
  br i1 %_80012, label %_80011.0, label %_80005.0
_80011.0:
  %_80026 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_80027 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_80026, i32 0, i32 4
  %_80013 = bitcast i8** %_80027 to i8*
  %_80028 = bitcast i8* %_80013 to i8**
  %_60001 = load i8*, i8** %_80028, !dereferenceable_or_null !{i64 8}
  %_80015 = icmp ne i8* %_60001, null
  br i1 %_80015, label %_80014.0, label %_80005.0
_80014.0:
  %_80029 = bitcast i8* %_60001 to { i8*, i32 }*
  %_80030 = getelementptr { i8*, i32 }, { i8*, i32 }* %_80029, i32 0, i32 1
  %_80016 = bitcast i32* %_80030 to i8*
  %_80031 = bitcast i8* %_80016 to i32*
  %_60002 = load i32, i32* %_80031
  %_80017 = and i32 1, 31
  %_60004 = shl i32 %_60002, %_80017
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.HashMapD9growTableiuEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i32 %_60004)
  br label %_80000.0
_70000.0:
  br label %_80000.0
_80000.0:
  %_80001 = call i32 @"_SM32scala.collection.mutable.HashMapD45scala$collection$mutable$HashMap$$computeHashL16java.lang.ObjectiEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2)
  %_80002 = call i32 @"_SM32scala.collection.mutable.HashMapD39scala$collection$mutable$HashMap$$indexiiEO"(i8* dereferenceable_or_null(32) %_1, i32 %_80001)
  %_80032 = bitcast i8* bitcast (i8* (i8*, i8*, i8*, i1, i32, i32)* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectziiL10scala.SomeEPT32scala.collection.mutable.HashMap" to i8*) to i8* (i8*, i8*, i8*, i1, i32, i32)*
  %_80003 = call dereferenceable_or_null(16) i8* %_80032(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_3, i1 %_4, i32 %_80001, i32 %_80002)
  ret i8* %_80003
_80005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectziiL10scala.SomeEPT32scala.collection.mutable.HashMap"(i8* %_1, i8* %_2, i8* %_3, i1 %_4, i32 %_5, i32 %_6) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_70000.0:
  %_440003 = icmp ne i8* %_1, null
  br i1 %_440003, label %_440001.0, label %_440002.0
_440001.0:
  %_440083 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_440084 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_440083, i32 0, i32 4
  %_440004 = bitcast i8** %_440084 to i8*
  %_440085 = bitcast i8* %_440004 to i8**
  %_70003 = load i8*, i8** %_440085, !dereferenceable_or_null !{i64 8}
  %_440007 = icmp ne i8* %_70003, null
  br i1 %_440007, label %_440006.0, label %_440002.0
_440006.0:
  %_440086 = bitcast i8* %_70003 to { i8*, i32 }*
  %_440087 = getelementptr { i8*, i32 }, { i8*, i32 }* %_440086, i32 0, i32 1
  %_440008 = bitcast i32* %_440087 to i8*
  %_440088 = bitcast i8* %_440008 to i32*
  %_440005 = load i32, i32* %_440088
  %_440011 = icmp sge i32 %_6, 0
  %_440012 = icmp slt i32 %_6, %_440005
  %_440013 = and i1 %_440011, %_440012
  br i1 %_440013, label %_440009.0, label %_440010.0
_440009.0:
  %_440089 = bitcast i8* %_70003 to { i8*, i32, i32, [0 x i8*] }*
  %_440090 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_440089, i32 0, i32 3, i32 %_6
  %_440014 = bitcast i8** %_440090 to i8*
  %_440091 = bitcast i8* %_440014 to i8**
  %_70004 = load i8*, i8** %_440091, !dereferenceable_or_null !{i64 40}
  br label %_80000.0
_80000.0:
  %_80002 = icmp eq i8* %_70004, null
  br i1 %_80002, label %_90000.0, label %_100000.0
_90000.0:
  %_440016 = icmp ne i8* %_1, null
  br i1 %_440016, label %_440015.0, label %_440002.0
_440015.0:
  %_440092 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_440093 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_440092, i32 0, i32 4
  %_440017 = bitcast i8** %_440093 to i8*
  %_440094 = bitcast i8* %_440017 to i8**
  %_90001 = load i8*, i8** %_440094, !dereferenceable_or_null !{i64 8}
  %_90003 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM37scala.collection.mutable.HashMap$NodeG4type" to i8*), i64 40)
  %_440095 = bitcast i8* %_90003 to { i8*, i8*, i32, i8*, i8* }*
  %_440096 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440095, i32 0, i32 4
  %_440019 = bitcast i8** %_440096 to i8*
  %_440097 = bitcast i8* %_440019 to i8**
  store i8* %_3, i8** %_440097
  %_440098 = bitcast i8* %_90003 to { i8*, i8*, i32, i8*, i8* }*
  %_440099 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440098, i32 0, i32 3
  %_440021 = bitcast i8** %_440099 to i8*
  %_440100 = bitcast i8* %_440021 to i8**
  store i8* %_2, i8** %_440100
  %_440101 = bitcast i8* %_90003 to { i8*, i8*, i32, i8*, i8* }*
  %_440102 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440101, i32 0, i32 2
  %_440023 = bitcast i32* %_440102 to i8*
  %_440103 = bitcast i8* %_440023 to i32*
  store i32 %_5, i32* %_440103
  %_440027 = icmp ne i8* %_90001, null
  br i1 %_440027, label %_440026.0, label %_440002.0
_440026.0:
  %_440104 = bitcast i8* %_90001 to { i8*, i32 }*
  %_440105 = getelementptr { i8*, i32 }, { i8*, i32 }* %_440104, i32 0, i32 1
  %_440028 = bitcast i32* %_440105 to i8*
  %_440106 = bitcast i8* %_440028 to i32*
  %_440025 = load i32, i32* %_440106
  %_440030 = icmp sge i32 %_6, 0
  %_440031 = icmp slt i32 %_6, %_440025
  %_440032 = and i1 %_440030, %_440031
  br i1 %_440032, label %_440029.0, label %_440010.0
_440029.0:
  %_440107 = bitcast i8* %_90001 to { i8*, i32, i32, [0 x i8*] }*
  %_440108 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_440107, i32 0, i32 3, i32 %_6
  %_440033 = bitcast i8** %_440108 to i8*
  %_440109 = bitcast i8* %_440033 to i8**
  store i8* %_90003, i8** %_440109
  br label %_130000.0
_100000.0:
  br label %_140000.0
_140000.0:
  br label %_150000.0
_150000.0:
  %_150001 = phi i8* [%_70004, %_140000.0], [%_350001, %_350000.0]
  %_150002 = phi i8* [null, %_140000.0], [%_150001, %_350000.0]
  %_150004 = icmp ne i8* %_150001, null
  br i1 %_150004, label %_160000.0, label %_170000.0
_160000.0:
  %_160001 = call i32 @"_SM37scala.collection.mutable.HashMap$NodeD4hashiEO"(i8* dereferenceable_or_null(40) %_150001)
  %_160003 = icmp sle i32 %_160001, %_5
  br label %_180000.0
_170000.0:
  br label %_180000.0
_180000.0:
  %_180001 = phi i1 [false, %_170000.0], [%_160003, %_160000.0]
  br i1 %_180001, label %_190000.0, label %_200000.0
_190000.0:
  %_190001 = call i32 @"_SM37scala.collection.mutable.HashMap$NodeD4hashiEO"(i8* dereferenceable_or_null(40) %_150001)
  %_190003 = icmp eq i32 %_190001, %_5
  br i1 %_190003, label %_210000.0, label %_220000.0
_210000.0:
  %_210002 = icmp eq i8* %_2, null
  br i1 %_210002, label %_230000.0, label %_240000.0
_230000.0:
  %_230001 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD3keyL16java.lang.ObjectEO"(i8* dereferenceable_or_null(40) %_150001)
  %_230003 = icmp eq i8* %_230001, null
  br label %_250000.0
_240000.0:
  %_240001 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD3keyL16java.lang.ObjectEO"(i8* dereferenceable_or_null(40) %_150001)
  %_440035 = icmp ne i8* %_2, null
  br i1 %_440035, label %_440034.0, label %_440002.0
_440034.0:
  %_440110 = bitcast i8* %_2 to i8**
  %_440036 = load i8*, i8** %_440110
  %_440111 = bitcast i8* %_440036 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_440112 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_440111, i32 0, i32 4, i32 0
  %_440037 = bitcast i8** %_440112 to i8*
  %_440113 = bitcast i8* %_440037 to i8**
  %_240003 = load i8*, i8** %_440113
  %_440114 = bitcast i8* %_240003 to i1 (i8*, i8*)*
  %_240004 = call i1 %_440114(i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_240001)
  br label %_250000.0
_250000.0:
  %_250001 = phi i1 [%_240004, %_440034.0], [%_230003, %_230000.0]
  br label %_260000.0
_220000.0:
  br label %_260000.0
_260000.0:
  %_260001 = phi i1 [false, %_220000.0], [%_250001, %_250000.0]
  br i1 %_260001, label %_270000.0, label %_280000.0
_270000.0:
  %_270001 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD5valueL16java.lang.ObjectEO"(i8* dereferenceable_or_null(40) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD9value_$eqL16java.lang.ObjectuEO"(i8* dereferenceable_or_null(40) %_150001, i8* dereferenceable_or_null(8) %_3)
  br i1 %_4, label %_290000.0, label %_300000.0
_290000.0:
  %_370001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM10scala.SomeG4type" to i8*), i64 16)
  %_440115 = bitcast i8* %_370001 to { i8*, i8* }*
  %_440116 = getelementptr { i8*, i8* }, { i8*, i8* }* %_440115, i32 0, i32 1
  %_440040 = bitcast i8** %_440116 to i8*
  %_440117 = bitcast i8* %_440040 to i8**
  store i8* %_270001, i8** %_440117
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_370001)
  call nonnull dereferenceable(8) i8* @"_SM13scala.ProductD6$init$uEO"(i8* nonnull dereferenceable(16) %_370001)
  br label %_340000.0
_300000.0:
  br label %_340000.0
_340000.0:
  %_340001 = phi i8* [null, %_300000.0], [%_370001, %_290000.0]
  ret i8* %_340001
_280000.0:
  br label %_350000.0
_350000.0:
  %_350001 = call dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD4nextL37scala.collection.mutable.HashMap$NodeEO"(i8* dereferenceable_or_null(40) %_150001)
  br label %_150000.0
_200000.0:
  br label %_390000.0
_390000.0:
  %_390002 = icmp eq i8* %_150002, null
  br i1 %_390002, label %_400000.0, label %_410000.0
_400000.0:
  %_440044 = icmp ne i8* %_1, null
  br i1 %_440044, label %_440043.0, label %_440002.0
_440043.0:
  %_440118 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_440119 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_440118, i32 0, i32 4
  %_440045 = bitcast i8** %_440119 to i8*
  %_440120 = bitcast i8* %_440045 to i8**
  %_400001 = load i8*, i8** %_440120, !dereferenceable_or_null !{i64 8}
  %_400003 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM37scala.collection.mutable.HashMap$NodeG4type" to i8*), i64 40)
  %_440121 = bitcast i8* %_400003 to { i8*, i8*, i32, i8*, i8* }*
  %_440122 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440121, i32 0, i32 1
  %_440047 = bitcast i8** %_440122 to i8*
  %_440123 = bitcast i8* %_440047 to i8**
  store i8* %_70004, i8** %_440123
  %_440124 = bitcast i8* %_400003 to { i8*, i8*, i32, i8*, i8* }*
  %_440125 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440124, i32 0, i32 4
  %_440049 = bitcast i8** %_440125 to i8*
  %_440126 = bitcast i8* %_440049 to i8**
  store i8* %_3, i8** %_440126
  %_440127 = bitcast i8* %_400003 to { i8*, i8*, i32, i8*, i8* }*
  %_440128 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440127, i32 0, i32 3
  %_440051 = bitcast i8** %_440128 to i8*
  %_440129 = bitcast i8* %_440051 to i8**
  store i8* %_2, i8** %_440129
  %_440130 = bitcast i8* %_400003 to { i8*, i8*, i32, i8*, i8* }*
  %_440131 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440130, i32 0, i32 2
  %_440053 = bitcast i32* %_440131 to i8*
  %_440132 = bitcast i8* %_440053 to i32*
  store i32 %_5, i32* %_440132
  %_440057 = icmp ne i8* %_400001, null
  br i1 %_440057, label %_440056.0, label %_440002.0
_440056.0:
  %_440133 = bitcast i8* %_400001 to { i8*, i32 }*
  %_440134 = getelementptr { i8*, i32 }, { i8*, i32 }* %_440133, i32 0, i32 1
  %_440058 = bitcast i32* %_440134 to i8*
  %_440135 = bitcast i8* %_440058 to i32*
  %_440055 = load i32, i32* %_440135
  %_440060 = icmp sge i32 %_6, 0
  %_440061 = icmp slt i32 %_6, %_440055
  %_440062 = and i1 %_440060, %_440061
  br i1 %_440062, label %_440059.0, label %_440010.0
_440059.0:
  %_440136 = bitcast i8* %_400001 to { i8*, i32, i32, [0 x i8*] }*
  %_440137 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_440136, i32 0, i32 3, i32 %_6
  %_440063 = bitcast i8** %_440137 to i8*
  %_440138 = bitcast i8* %_440063 to i8**
  store i8* %_400003, i8** %_440138
  br label %_440000.0
_410000.0:
  %_410002 = call dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD4nextL37scala.collection.mutable.HashMap$NodeEO"(i8* dereferenceable_or_null(40) %_150002)
  %_410003 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM37scala.collection.mutable.HashMap$NodeG4type" to i8*), i64 40)
  %_440139 = bitcast i8* %_410003 to { i8*, i8*, i32, i8*, i8* }*
  %_440140 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440139, i32 0, i32 1
  %_440065 = bitcast i8** %_440140 to i8*
  %_440141 = bitcast i8* %_440065 to i8**
  store i8* %_410002, i8** %_440141
  %_440142 = bitcast i8* %_410003 to { i8*, i8*, i32, i8*, i8* }*
  %_440143 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440142, i32 0, i32 4
  %_440067 = bitcast i8** %_440143 to i8*
  %_440144 = bitcast i8* %_440067 to i8**
  store i8* %_3, i8** %_440144
  %_440145 = bitcast i8* %_410003 to { i8*, i8*, i32, i8*, i8* }*
  %_440146 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440145, i32 0, i32 3
  %_440069 = bitcast i8** %_440146 to i8*
  %_440147 = bitcast i8* %_440069 to i8**
  store i8* %_2, i8** %_440147
  %_440148 = bitcast i8* %_410003 to { i8*, i8*, i32, i8*, i8* }*
  %_440149 = getelementptr { i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i8* }* %_440148, i32 0, i32 2
  %_440071 = bitcast i32* %_440149 to i8*
  %_440150 = bitcast i8* %_440071 to i32*
  store i32 %_5, i32* %_440150
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8next_$eqL37scala.collection.mutable.HashMap$NodeuEO"(i8* dereferenceable_or_null(40) %_150002, i8* nonnull dereferenceable(40) %_410003)
  br label %_440000.0
_440000.0:
  br label %_130000.0
_130000.0:
  %_130001 = phi i8* [%_150001, %_440000.0], [null, %_440029.0]
  %_130002 = phi i8* [%_150002, %_440000.0], [null, %_440029.0]
  %_440074 = icmp ne i8* %_1, null
  br i1 %_440074, label %_440073.0, label %_440002.0
_440073.0:
  %_440151 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_440152 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_440151, i32 0, i32 3
  %_440075 = bitcast i32* %_440152 to i8*
  %_440153 = bitcast i8* %_440075 to i32*
  %_130003 = load i32, i32* %_440153
  %_130005 = add i32 %_130003, 1
  %_440078 = icmp ne i8* %_1, null
  br i1 %_440078, label %_440077.0, label %_440002.0
_440077.0:
  %_440154 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_440155 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_440154, i32 0, i32 3
  %_440079 = bitcast i32* %_440155 to i8*
  %_440156 = bitcast i8* %_440079 to i32*
  store i32 %_130005, i32* %_440156
  ret i8* null
_440002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_440010.0:
  %_440081 = phi i32 [%_6, %_440006.0], [%_6, %_440056.0], [%_6, %_440026.0]
  %_440157 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_440157(i8* null, i32 %_440081)
  unreachable
}

define i32 @"_SM32scala.collection.mutable.HashMapD4sizeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20004 = icmp ne i8* %_1, null
  br i1 %_20004, label %_20002.0, label %_20003.0
_20002.0:
  %_20007 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_20008 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_20007, i32 0, i32 3
  %_20005 = bitcast i32* %_20008 to i8*
  %_20009 = bitcast i8* %_20005 to i32*
  %_20001 = load i32, i32* %_20009
  ret i32 %_20001
_20003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM32scala.collection.mutable.HashMapD5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(40) i8* @"_SM32scala.collection.mutable.HashMapD8findNodeL16java.lang.ObjectL37scala.collection.mutable.HashMap$NodeEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2)
  br label %_40000.0
_40000.0:
  %_40002 = icmp eq i8* %_30001, null
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_80002 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM28scala.collection.AbstractMapD7defaultL16java.lang.ObjectL16java.lang.ObjectEO" to i8*) to i8* (i8*, i8*)*
  %_50001 = call dereferenceable_or_null(8) i8* %_80002(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2)
  br label %_70000.0
_60000.0:
  br label %_80000.0
_80000.0:
  %_80001 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD5valueL16java.lang.ObjectEO"(i8* dereferenceable_or_null(40) %_30001)
  br label %_70000.0
_70000.0:
  %_70001 = phi i8* [%_80001, %_80000.0], [%_50001, %_50000.0]
  ret i8* %_70001
}

define dereferenceable_or_null(32) i8* @"_SM32scala.collection.mutable.HashMapD6addAllL29scala.collection.IterableOnceL32scala.collection.mutable.HashMapEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_220005 = icmp ne i8* %_2, null
  br i1 %_220005, label %_220003.0, label %_220004.0
_220003.0:
  %_220088 = bitcast i8* %_2 to i8**
  %_220006 = load i8*, i8** %_220088
  %_220089 = bitcast i8* %_220006 to { i8*, i32, i32, i8* }*
  %_220090 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_220089, i32 0, i32 2
  %_220007 = bitcast i32* %_220090 to i8*
  %_220091 = bitcast i8* %_220007 to i32*
  %_220008 = load i32, i32* %_220091
  %_220092 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_220093 = getelementptr i8*, i8** %_220092, i32 669
  %_220009 = bitcast i8** %_220093 to i8*
  %_220094 = bitcast i8* %_220009 to i8**
  %_220095 = getelementptr i8*, i8** %_220094, i32 %_220008
  %_220010 = bitcast i8** %_220095 to i8*
  %_220096 = bitcast i8* %_220010 to i8**
  %_30002 = load i8*, i8** %_220096
  %_220097 = bitcast i8* %_30002 to i32 (i8*)*
  %_30003 = call i32 %_220097(i8* dereferenceable_or_null(8) %_2)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.HashMapD8sizeHintiuEO"(i8* dereferenceable_or_null(32) %_1, i32 %_30003)
  br label %_40000.0
_40000.0:
  %_220015 = icmp eq i8* %_2, null
  br i1 %_220015, label %_220012.0, label %_220013.0
_220012.0:
  br label %_220014.0
_220013.0:
  %_220098 = bitcast i8* %_2 to i8**
  %_220016 = load i8*, i8** %_220098
  %_220017 = icmp eq i8* %_220016, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM34scala.collection.immutable.HashMapG4type" to i8*)
  br label %_220014.0
_220014.0:
  %_40002 = phi i1 [%_220017, %_220013.0], [false, %_220012.0]
  br i1 %_40002, label %_50000.0, label %_60000.0
_60000.0:
  br label %_90000.0
_90000.0:
  %_220021 = icmp eq i8* %_2, null
  br i1 %_220021, label %_220018.0, label %_220019.0
_220018.0:
  br label %_220020.0
_220019.0:
  %_220099 = bitcast i8* %_2 to i8**
  %_220022 = load i8*, i8** %_220099
  %_220023 = icmp eq i8* %_220022, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM32scala.collection.mutable.HashMapG4type" to i8*)
  br label %_220020.0
_220020.0:
  %_90002 = phi i1 [%_220023, %_220019.0], [false, %_220018.0]
  br i1 %_90002, label %_100000.0, label %_110000.0
_100000.0:
  %_220027 = icmp eq i8* %_2, null
  br i1 %_220027, label %_220025.0, label %_220024.0
_220024.0:
  %_220100 = bitcast i8* %_2 to i8**
  %_220028 = load i8*, i8** %_220100
  %_220029 = icmp eq i8* %_220028, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM32scala.collection.mutable.HashMapG4type" to i8*)
  br i1 %_220029, label %_220025.0, label %_220026.0
_220025.0:
  %_100001 = bitcast i8* %_2 to i8*
  %_100002 = call dereferenceable_or_null(8) i8* @"_SM32scala.collection.mutable.HashMapD12nodeIteratorL25scala.collection.IteratorEO"(i8* dereferenceable_or_null(32) %_100001)
  br label %_120000.0
_120000.0:
  %_220031 = icmp ne i8* %_100002, null
  br i1 %_220031, label %_220030.0, label %_220004.0
_220030.0:
  %_220101 = bitcast i8* %_100002 to i8**
  %_220032 = load i8*, i8** %_220101
  %_220102 = bitcast i8* %_220032 to { i8*, i32, i32, i8* }*
  %_220103 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_220102, i32 0, i32 2
  %_220033 = bitcast i32* %_220103 to i8*
  %_220104 = bitcast i8* %_220033 to i32*
  %_220034 = load i32, i32* %_220104
  %_220105 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_220106 = getelementptr i8*, i8** %_220105, i32 4359
  %_220035 = bitcast i8** %_220106 to i8*
  %_220107 = bitcast i8* %_220035 to i8**
  %_220108 = getelementptr i8*, i8** %_220107, i32 %_220034
  %_220036 = bitcast i8** %_220108 to i8*
  %_220109 = bitcast i8* %_220036 to i8**
  %_120002 = load i8*, i8** %_220109
  %_220110 = bitcast i8* %_120002 to i1 (i8*)*
  %_120003 = call i1 %_220110(i8* dereferenceable_or_null(8) %_100002)
  br i1 %_120003, label %_130000.0, label %_140000.0
_130000.0:
  %_220038 = icmp ne i8* %_100002, null
  br i1 %_220038, label %_220037.0, label %_220004.0
_220037.0:
  %_220111 = bitcast i8* %_100002 to i8**
  %_220039 = load i8*, i8** %_220111
  %_220112 = bitcast i8* %_220039 to { i8*, i32, i32, i8* }*
  %_220113 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_220112, i32 0, i32 2
  %_220040 = bitcast i32* %_220113 to i8*
  %_220114 = bitcast i8* %_220040 to i32*
  %_220041 = load i32, i32* %_220114
  %_220115 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_220116 = getelementptr i8*, i8** %_220115, i32 4219
  %_220042 = bitcast i8** %_220116 to i8*
  %_220117 = bitcast i8* %_220042 to i8**
  %_220118 = getelementptr i8*, i8** %_220117, i32 %_220041
  %_220043 = bitcast i8** %_220118 to i8*
  %_220119 = bitcast i8* %_220043 to i8**
  %_130002 = load i8*, i8** %_220119
  %_220120 = bitcast i8* %_130002 to i8* (i8*)*
  %_130003 = call dereferenceable_or_null(8) i8* %_220120(i8* dereferenceable_or_null(8) %_100002)
  %_220046 = icmp eq i8* %_130003, null
  br i1 %_220046, label %_220045.0, label %_220044.0
_220044.0:
  %_220121 = bitcast i8* %_130003 to i8**
  %_220047 = load i8*, i8** %_220121
  %_220048 = icmp eq i8* %_220047, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM37scala.collection.mutable.HashMap$NodeG4type" to i8*)
  br i1 %_220048, label %_220045.0, label %_220026.0
_220045.0:
  %_130004 = bitcast i8* %_130003 to i8*
  %_130005 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD3keyL16java.lang.ObjectEO"(i8* dereferenceable_or_null(40) %_130004)
  %_130006 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD5valueL16java.lang.ObjectEO"(i8* dereferenceable_or_null(40) %_130004)
  %_130007 = call i32 @"_SM37scala.collection.mutable.HashMap$NodeD4hashiEO"(i8* dereferenceable_or_null(40) %_130004)
  %_130008 = call dereferenceable_or_null(16) i8* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectizL10scala.SomeEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_130005, i8* dereferenceable_or_null(8) %_130006, i32 %_130007, i1 false)
  br label %_120000.0
_140000.0:
  br label %_150000.0
_150000.0:
  br label %_160000.0
_110000.0:
  br label %_170000.0
_170000.0:
  %_220052 = icmp eq i8* %_2, null
  br i1 %_220052, label %_220049.0, label %_220050.0
_220049.0:
  br label %_220051.0
_220050.0:
  %_220122 = bitcast i8* %_2 to i8**
  %_220053 = load i8*, i8** %_220122
  %_220123 = bitcast i8* %_220053 to { i8*, i32, i32, i8* }*
  %_220124 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_220123, i32 0, i32 1
  %_220054 = bitcast i32* %_220124 to i8*
  %_220125 = bitcast i8* %_220054 to i32*
  %_220055 = load i32, i32* %_220125
  %_220126 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_220127 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_220126, i32 0, i32 %_220055, i32 53
  %_220056 = bitcast i1* %_220127 to i8*
  %_220128 = bitcast i8* %_220056 to i1*
  %_220057 = load i1, i1* %_220128
  br label %_220051.0
_220051.0:
  %_170002 = phi i1 [%_220057, %_220050.0], [false, %_220049.0]
  br i1 %_170002, label %_180000.0, label %_190000.0
_180000.0:
  %_220060 = icmp eq i8* %_2, null
  br i1 %_220060, label %_220059.0, label %_220058.0
_220058.0:
  %_220129 = bitcast i8* %_2 to i8**
  %_220061 = load i8*, i8** %_220129
  %_220130 = bitcast i8* %_220061 to { i8*, i32, i32, i8* }*
  %_220131 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_220130, i32 0, i32 1
  %_220062 = bitcast i32* %_220131 to i8*
  %_220132 = bitcast i8* %_220062 to i32*
  %_220063 = load i32, i32* %_220132
  %_220133 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_220134 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_220133, i32 0, i32 %_220063, i32 53
  %_220064 = bitcast i1* %_220134 to i8*
  %_220135 = bitcast i8* %_220064 to i1*
  %_220065 = load i1, i1* %_220135
  br i1 %_220065, label %_220059.0, label %_220026.0
_220059.0:
  %_180001 = bitcast i8* %_2 to i8*
  %_180003 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM42scala.collection.mutable.HashMap$$Lambda$2G4type" to i8*), i64 16)
  %_220136 = bitcast i8* %_180003 to { i8*, i8* }*
  %_220137 = getelementptr { i8*, i8* }, { i8*, i8* }* %_220136, i32 0, i32 1
  %_220067 = bitcast i8** %_220137 to i8*
  %_220138 = bitcast i8* %_220067 to i8**
  store i8* %_1, i8** %_220138
  %_220139 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.collection.mutable.HashMapD12foreachEntryL15scala.Function2uEO" to i8*) to i8* (i8*, i8*)*
  call nonnull dereferenceable(8) i8* %_220139(i8* dereferenceable_or_null(8) %_180001, i8* nonnull dereferenceable(16) %_180003)
  br label %_160000.0
_190000.0:
  br label %_220000.0
_220000.0:
  %_220001 = call dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.GrowableD6addAllL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2)
  %_220071 = icmp eq i8* %_220001, null
  br i1 %_220071, label %_220070.0, label %_220069.0
_220069.0:
  %_220140 = bitcast i8* %_220001 to i8**
  %_220072 = load i8*, i8** %_220140
  %_220073 = icmp eq i8* %_220072, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM32scala.collection.mutable.HashMapG4type" to i8*)
  br i1 %_220073, label %_220070.0, label %_220026.0
_220070.0:
  %_220002 = bitcast i8* %_220001 to i8*
  br label %_160000.0
_160000.0:
  %_160001 = phi i8* [%_220002, %_220070.0], [%_1, %_220059.0], [%_1, %_150000.0]
  ret i8* %_160001
_50000.0:
  %_220076 = icmp eq i8* %_2, null
  br i1 %_220076, label %_220075.0, label %_220074.0
_220074.0:
  %_220141 = bitcast i8* %_2 to i8**
  %_220077 = load i8*, i8** %_220141
  %_220078 = icmp eq i8* %_220077, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM34scala.collection.immutable.HashMapG4type" to i8*)
  br i1 %_220078, label %_220075.0, label %_220026.0
_220075.0:
  %_50001 = bitcast i8* %_2 to i8*
  %_220080 = icmp ne i8* %_50001, null
  br i1 %_220080, label %_220079.0, label %_220004.0
_220079.0:
  br label %_220081.0
_220004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_220026.0:
  %_220083 = phi i8* [%_220001, %_220069.0], [%_2, %_220058.0], [%_2, %_220024.0], [%_130003, %_220044.0], [%_2, %_220074.0]
  %_220084 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM32scala.collection.mutable.HashMapG4type" to i8*), %_220069.0], [bitcast ({ i8*, i32, i32, i8* }* @"_SM28scala.collection.mutable.MapG4type" to i8*), %_220058.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM32scala.collection.mutable.HashMapG4type" to i8*), %_220024.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM37scala.collection.mutable.HashMap$NodeG4type" to i8*), %_220044.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM34scala.collection.immutable.HashMapG4type" to i8*), %_220074.0]
  %_220142 = bitcast i8* %_220083 to i8**
  %_220085 = load i8*, i8** %_220142
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_220085, i8* %_220084)
  unreachable
_220081.0:
  %_220143 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_220143(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM32scala.collection.mutable.HashMapD6addAllL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(32) i8* @"_SM32scala.collection.mutable.HashMapD6addAllL29scala.collection.IterableOnceL32scala.collection.mutable.HashMapEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(32) i8* @"_SM32scala.collection.mutable.HashMapD6addOneL12scala.Tuple2L32scala.collection.mutable.HashMapEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_50004 = icmp ne i8* %_2, null
  br i1 %_50004, label %_50002.0, label %_50003.0
_50002.0:
  %_50010 = bitcast i8* %_2 to { i8*, i8*, i8* }*
  %_50011 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_50010, i32 0, i32 2
  %_50005 = bitcast i8** %_50011 to i8*
  %_50012 = bitcast i8* %_50005 to i8**
  %_40001 = load i8*, i8** %_50012, !dereferenceable_or_null !{i64 8}
  %_50007 = icmp ne i8* %_2, null
  br i1 %_50007, label %_50006.0, label %_50003.0
_50006.0:
  %_50013 = bitcast i8* %_2 to { i8*, i8*, i8* }*
  %_50014 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_50013, i32 0, i32 1
  %_50008 = bitcast i8** %_50014 to i8*
  %_50015 = bitcast i8* %_50008 to i8**
  %_50001 = load i8*, i8** %_50015, !dereferenceable_or_null !{i64 8}
  %_30001 = call dereferenceable_or_null(16) i8* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectzL10scala.SomeEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_40001, i8* dereferenceable_or_null(8) %_50001, i1 false)
  ret i8* %_1
_50003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM32scala.collection.mutable.HashMapD6addOneL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30006 = icmp eq i8* %_2, null
  br i1 %_30006, label %_30004.0, label %_30003.0
_30003.0:
  %_30013 = bitcast i8* %_2 to i8**
  %_30007 = load i8*, i8** %_30013
  %_30008 = icmp eq i8* %_30007, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM12scala.Tuple2G4type" to i8*)
  br i1 %_30008, label %_30004.0, label %_30005.0
_30004.0:
  %_30001 = bitcast i8* %_2 to i8*
  %_30002 = call dereferenceable_or_null(32) i8* @"_SM32scala.collection.mutable.HashMapD6addOneL12scala.Tuple2L32scala.collection.mutable.HashMapEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(24) %_30001)
  ret i8* %_30002
_30005.0:
  %_30009 = phi i8* [%_2, %_30003.0]
  %_30010 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM12scala.Tuple2G4type" to i8*), %_30003.0]
  %_30014 = bitcast i8* %_30009 to i8**
  %_30011 = load i8*, i8** %_30014
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_30011, i8* %_30010)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.HashMapD6updateL16java.lang.ObjectL16java.lang.ObjectuEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call dereferenceable_or_null(16) i8* @"_SM32scala.collection.mutable.HashMapD4put0L16java.lang.ObjectL16java.lang.ObjectzL10scala.SomeEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_3, i1 false)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i1 @"_SM32scala.collection.mutable.HashMapD7isEmptyzEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM32scala.collection.mutable.HashMapD4sizeiEO"(i8* dereferenceable_or_null(32) %_1)
  %_20003 = icmp eq i32 %_20001, 0
  ret i1 %_20003
}

define dereferenceable_or_null(40) i8* @"_SM32scala.collection.mutable.HashMapD8findNodeL16java.lang.ObjectL37scala.collection.mutable.HashMap$NodeEPT32scala.collection.mutable.HashMap"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i32 @"_SM32scala.collection.mutable.HashMapD45scala$collection$mutable$HashMap$$computeHashL16java.lang.ObjectiEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2)
  %_80004 = icmp ne i8* %_1, null
  br i1 %_80004, label %_80002.0, label %_80003.0
_80002.0:
  %_80019 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_80020 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_80019, i32 0, i32 4
  %_80005 = bitcast i8** %_80020 to i8*
  %_80021 = bitcast i8* %_80005 to i8**
  %_30002 = load i8*, i8** %_80021, !dereferenceable_or_null !{i64 8}
  %_30003 = call i32 @"_SM32scala.collection.mutable.HashMapD39scala$collection$mutable$HashMap$$indexiiEO"(i8* dereferenceable_or_null(32) %_1, i32 %_30001)
  %_80008 = icmp ne i8* %_30002, null
  br i1 %_80008, label %_80007.0, label %_80003.0
_80007.0:
  %_80022 = bitcast i8* %_30002 to { i8*, i32 }*
  %_80023 = getelementptr { i8*, i32 }, { i8*, i32 }* %_80022, i32 0, i32 1
  %_80009 = bitcast i32* %_80023 to i8*
  %_80024 = bitcast i8* %_80009 to i32*
  %_80006 = load i32, i32* %_80024
  %_80012 = icmp sge i32 %_30003, 0
  %_80013 = icmp slt i32 %_30003, %_80006
  %_80014 = and i1 %_80012, %_80013
  br i1 %_80014, label %_80010.0, label %_80011.0
_80010.0:
  %_80025 = bitcast i8* %_30002 to { i8*, i32, i32, [0 x i8*] }*
  %_80026 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_80025, i32 0, i32 3, i32 %_30003
  %_80015 = bitcast i8** %_80026 to i8*
  %_80027 = bitcast i8* %_80015 to i8**
  %_30004 = load i8*, i8** %_80027, !dereferenceable_or_null !{i64 40}
  br label %_40000.0
_40000.0:
  %_40002 = icmp eq i8* %_30004, null
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  br label %_70000.0
_60000.0:
  br label %_80000.0
_80000.0:
  %_80001 = call dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8findNodeL16java.lang.ObjectiL37scala.collection.mutable.HashMap$NodeEO"(i8* dereferenceable_or_null(40) %_30004, i8* dereferenceable_or_null(8) %_2, i32 %_30001)
  br label %_70000.0
_70000.0:
  %_70001 = phi i8* [%_80001, %_80000.0], [null, %_50000.0]
  ret i8* %_70001
_80003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_80011.0:
  %_80017 = phi i32 [%_30003, %_80007.0]
  %_80028 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_80028(i8* null, i32 %_80017)
  unreachable
}

define i32 @"_SM32scala.collection.mutable.HashMapD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i1 @"_SM32scala.collection.mutable.HashMapD7isEmptyzEO"(i8* dereferenceable_or_null(32) %_1)
  br i1 %_20001, label %_30000.0, label %_40000.0
_30000.0:
  %_30001 = call dereferenceable_or_null(24) i8* @"_SM31scala.util.hashing.MurmurHash3$G4load"()
  %_170033 = bitcast i8* %_30001 to { i8*, i32, i32, i32, i32 }*
  %_170034 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_170033, i32 0, i32 1
  %_170002 = bitcast i32* %_170034 to i8*
  %_170035 = bitcast i8* %_170002 to i32*
  %_50001 = load i32, i32* %_170035
  br label %_60000.0
_40000.0:
  %_70002 = icmp eq i8* %_1, null
  br i1 %_70002, label %_80000.0, label %_90000.0
_90000.0:
  br label %_100000.0
_80000.0:
  %_170005 = icmp ne i8* null, null
  br i1 %_170005, label %_170003.0, label %_170004.0
_170003.0:
  call i8* @"scalanative_throw"(i8* null)
  unreachable
_100000.0:
  br i1 %_70002, label %_120000.0, label %_130000.0
_130000.0:
  br label %_140000.0
_120000.0:
  %_170008 = icmp ne i8* null, null
  br i1 %_170008, label %_170007.0, label %_170004.0
_170007.0:
  call i8* @"scalanative_throw"(i8* null)
  unreachable
_140000.0:
  %_150001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM40scala.collection.mutable.HashMap$$anon$5G4type" to i8*), i64 56)
  %_170036 = bitcast i8* %_150001 to { i8*, i32, i8*, i32, i8* }*
  %_170037 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_170036, i32 0, i32 4
  %_170011 = bitcast i8** %_170037 to i8*
  %_170038 = bitcast i8* %_170011 to i8**
  store i8* %_1, i8** %_170038
  %_170039 = bitcast i8* %_150001 to { i8*, i32, i8*, i32, i8*, i32, i8* }*
  %_170040 = getelementptr { i8*, i32, i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8*, i32, i8* }* %_170039, i32 0, i32 6
  %_170013 = bitcast i8** %_170040 to i8*
  %_170041 = bitcast i8* %_170013 to i8**
  store i8* %_1, i8** %_170041
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(56) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(56) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IteratorD6$init$uEO"(i8* nonnull dereferenceable(56) %_150001)
  %_170042 = bitcast i8* %_150001 to { i8*, i32, i8*, i32, i8* }*
  %_170043 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_170042, i32 0, i32 3
  %_170018 = bitcast i32* %_170043 to i8*
  %_170044 = bitcast i8* %_170018 to i32*
  store i32 0, i32* %_170044
  %_170045 = bitcast i8* %_150001 to { i8*, i32, i8*, i32, i8* }*
  %_170046 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_170045, i32 0, i32 2
  %_170020 = bitcast i8** %_170046 to i8*
  %_170047 = bitcast i8* %_170020 to i8**
  store i8* null, i8** %_170047
  %_170022 = icmp ne i8* %_1, null
  br i1 %_170022, label %_170021.0, label %_170004.0
_170021.0:
  %_170048 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_170049 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_170048, i32 0, i32 4
  %_170023 = bitcast i8** %_170049 to i8*
  %_170050 = bitcast i8* %_170023 to i8**
  %_140003 = load i8*, i8** %_170050, !dereferenceable_or_null !{i64 8}
  %_170025 = icmp ne i8* %_140003, null
  br i1 %_170025, label %_170024.0, label %_170004.0
_170024.0:
  %_170051 = bitcast i8* %_140003 to { i8*, i32 }*
  %_170052 = getelementptr { i8*, i32 }, { i8*, i32 }* %_170051, i32 0, i32 1
  %_170026 = bitcast i32* %_170052 to i8*
  %_170053 = bitcast i8* %_170026 to i32*
  %_140004 = load i32, i32* %_170053
  %_170054 = bitcast i8* %_150001 to { i8*, i32, i8*, i32, i8* }*
  %_170055 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_170054, i32 0, i32 1
  %_170028 = bitcast i32* %_170055 to i8*
  %_170056 = bitcast i8* %_170028 to i32*
  store i32 %_140004, i32* %_170056
  %_170057 = bitcast i8* %_150001 to { i8*, i32, i8*, i32, i8*, i32, i8* }*
  %_170058 = getelementptr { i8*, i32, i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8*, i32, i8* }* %_170057, i32 0, i32 5
  %_170030 = bitcast i32* %_170058 to i8*
  %_170059 = bitcast i8* %_170030 to i32*
  store i32 0, i32* %_170059
  %_40002 = call dereferenceable_or_null(24) i8* @"_SM31scala.util.hashing.MurmurHash3$G4load"()
  %_170060 = bitcast i8* %_40002 to { i8*, i32, i32, i32, i32 }*
  %_170061 = getelementptr { i8*, i32, i32, i32, i32 }, { i8*, i32, i32, i32, i32 }* %_170060, i32 0, i32 3
  %_170031 = bitcast i32* %_170061 to i8*
  %_170062 = bitcast i8* %_170031 to i32*
  %_170001 = load i32, i32* %_170062
  %_40003 = call i32 @"_SM30scala.util.hashing.MurmurHash3D13unorderedHashL29scala.collection.IterableOnceiiEO"(i8* nonnull dereferenceable(24) %_40002, i8* nonnull dereferenceable(56) %_150001, i32 %_170001)
  br label %_60000.0
_60000.0:
  %_60001 = phi i32 [%_40003, %_170024.0], [%_50001, %_30000.0]
  ret i32 %_60001
_170004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM32scala.collection.mutable.HashMapD8iteratorL25scala.collection.IteratorEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM32scala.collection.mutable.HashMapD4sizeiEO"(i8* dereferenceable_or_null(32) %_1)
  %_20003 = icmp eq i32 %_20001, 0
  br i1 %_20003, label %_30000.0, label %_40000.0
_30000.0:
  %_30001 = call dereferenceable_or_null(16) i8* @"_SM26scala.collection.Iterator$G4load"()
  %_30002 = call dereferenceable_or_null(8) i8* @"_SM26scala.collection.Iterator$D5emptyL25scala.collection.IteratorEO"(i8* nonnull dereferenceable(16) %_30001)
  br label %_50000.0
_40000.0:
  %_70002 = icmp eq i8* %_1, null
  br i1 %_70002, label %_80000.0, label %_90000.0
_90000.0:
  br label %_100000.0
_80000.0:
  %_110008 = icmp ne i8* null, null
  br i1 %_110008, label %_110006.0, label %_110007.0
_110006.0:
  call i8* @"scalanative_throw"(i8* null)
  unreachable
_100000.0:
  %_110001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM40scala.collection.mutable.HashMap$$anon$1G4type" to i8*), i64 40)
  %_110028 = bitcast i8* %_110001 to { i8*, i32, i8*, i32, i8* }*
  %_110029 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_110028, i32 0, i32 4
  %_110011 = bitcast i8** %_110029 to i8*
  %_110030 = bitcast i8* %_110011 to i8**
  store i8* %_1, i8** %_110030
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(40) %_110001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(40) %_110001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IteratorD6$init$uEO"(i8* nonnull dereferenceable(40) %_110001)
  %_110031 = bitcast i8* %_110001 to { i8*, i32, i8*, i32, i8* }*
  %_110032 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_110031, i32 0, i32 3
  %_110016 = bitcast i32* %_110032 to i8*
  %_110033 = bitcast i8* %_110016 to i32*
  store i32 0, i32* %_110033
  %_110034 = bitcast i8* %_110001 to { i8*, i32, i8*, i32, i8* }*
  %_110035 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_110034, i32 0, i32 2
  %_110018 = bitcast i8** %_110035 to i8*
  %_110036 = bitcast i8* %_110018 to i8**
  store i8* null, i8** %_110036
  %_110020 = icmp ne i8* %_1, null
  br i1 %_110020, label %_110019.0, label %_110007.0
_110019.0:
  %_110037 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_110038 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_110037, i32 0, i32 4
  %_110021 = bitcast i8** %_110038 to i8*
  %_110039 = bitcast i8* %_110021 to i8**
  %_100003 = load i8*, i8** %_110039, !dereferenceable_or_null !{i64 8}
  %_110023 = icmp ne i8* %_100003, null
  br i1 %_110023, label %_110022.0, label %_110007.0
_110022.0:
  %_110040 = bitcast i8* %_100003 to { i8*, i32 }*
  %_110041 = getelementptr { i8*, i32 }, { i8*, i32 }* %_110040, i32 0, i32 1
  %_110024 = bitcast i32* %_110041 to i8*
  %_110042 = bitcast i8* %_110024 to i32*
  %_100004 = load i32, i32* %_110042
  %_110043 = bitcast i8* %_110001 to { i8*, i32, i8*, i32, i8* }*
  %_110044 = getelementptr { i8*, i32, i8*, i32, i8* }, { i8*, i32, i8*, i32, i8* }* %_110043, i32 0, i32 1
  %_110026 = bitcast i32* %_110044 to i8*
  %_110045 = bitcast i8* %_110026 to i32*
  store i32 %_100004, i32* %_110045
  br label %_50000.0
_50000.0:
  %_50001 = phi i8* [%_110001, %_110022.0], [%_30002, %_30000.0]
  ret i8* %_50001
_110007.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.HashMapD8sizeHintiuEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_60003 = icmp ne i8* %_1, null
  br i1 %_60003, label %_60001.0, label %_60002.0
_60001.0:
  %_60024 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_60025 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_60024, i32 0, i32 1
  %_60004 = bitcast double* %_60025 to i8*
  %_60026 = bitcast i8* %_60004 to double*
  %_30003 = load double, double* %_60026
  %_30006 = add i32 %_2, 1
  %_30007 = sitofp i32 %_30006 to double
  %_30008 = fdiv double %_30007, %_30003
  %_60012 = fcmp une double %_30008, %_30008
  br i1 %_60012, label %_60005.0, label %_60006.0
_60005.0:
  br label %_60011.0
_60006.0:
  %_60013 = fcmp ole double %_30008, 0xc1e0000000000000
  br i1 %_60013, label %_60007.0, label %_60008.0
_60007.0:
  br label %_60011.0
_60008.0:
  %_60014 = fcmp oge double %_30008, 0x41dfffffffc00000
  br i1 %_60014, label %_60009.0, label %_60010.0
_60009.0:
  br label %_60011.0
_60010.0:
  %_60015 = fptosi double %_30008 to i32
  br label %_60011.0
_60011.0:
  %_30009 = phi i32 [%_60015, %_60010.0], [2147483647, %_60009.0], [-2147483648, %_60007.0], [zeroinitializer, %_60005.0]
  %_30010 = call i32 @"_SM32scala.collection.mutable.HashMapD12tableSizeForiiEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i32 %_30009)
  %_60017 = icmp ne i8* %_1, null
  br i1 %_60017, label %_60016.0, label %_60002.0
_60016.0:
  %_60027 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_60028 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_60027, i32 0, i32 4
  %_60018 = bitcast i8** %_60028 to i8*
  %_60029 = bitcast i8* %_60018 to i8**
  %_30011 = load i8*, i8** %_60029, !dereferenceable_or_null !{i64 8}
  %_60020 = icmp ne i8* %_30011, null
  br i1 %_60020, label %_60019.0, label %_60002.0
_60019.0:
  %_60030 = bitcast i8* %_30011 to { i8*, i32 }*
  %_60031 = getelementptr { i8*, i32 }, { i8*, i32 }* %_60030, i32 0, i32 1
  %_60021 = bitcast i32* %_60031 to i8*
  %_60032 = bitcast i8* %_60021 to i32*
  %_30012 = load i32, i32* %_60032
  %_30014 = icmp sgt i32 %_30010, %_30012
  br i1 %_30014, label %_40000.0, label %_50000.0
_40000.0:
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.HashMapD9growTableiuEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i32 %_30010)
  br label %_60000.0
_50000.0:
  br label %_60000.0
_60000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_60002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM32scala.collection.mutable.HashMapD9getOrElseL16java.lang.ObjectL15scala.Function0L16java.lang.ObjectEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call dereferenceable_or_null(32) i8* @"_SM16java.lang.ObjectD8getClassL15java.lang.ClassEO"(i8* dereferenceable_or_null(32) %_1)
  %_40003 = icmp eq i8* %_40001, null
  br i1 %_40003, label %_50000.0, label %_60000.0
_50000.0:
  %_50002 = icmp eq i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM32scala.collection.mutable.HashMapG4type" to i8*), null
  br label %_70000.0
_60000.0:
  %_130011 = bitcast i8* bitcast (i1 (i8*, i8*)* @"_SM15java.lang.ClassD6equalsL16java.lang.ObjectzEO" to i8*) to i1 (i8*, i8*)*
  %_60001 = call i1 %_130011(i8* dereferenceable_or_null(32) %_40001, i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM32scala.collection.mutable.HashMapG4type" to i8*))
  br label %_70000.0
_70000.0:
  %_70001 = phi i1 [%_60001, %_60000.0], [%_50002, %_50000.0]
  %_70003 = xor i1 %_70001, true
  br i1 %_70003, label %_80000.0, label %_90000.0
_80000.0:
  %_80001 = call dereferenceable_or_null(8) i8* @"_SM23scala.collection.MapOpsD9getOrElseL16java.lang.ObjectL15scala.Function0L16java.lang.ObjectEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_3)
  br label %_100000.0
_90000.0:
  %_90001 = call dereferenceable_or_null(40) i8* @"_SM32scala.collection.mutable.HashMapD8findNodeL16java.lang.ObjectL37scala.collection.mutable.HashMap$NodeEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2)
  %_90003 = icmp eq i8* %_90001, null
  br i1 %_90003, label %_110000.0, label %_120000.0
_110000.0:
  %_130004 = icmp ne i8* %_3, null
  br i1 %_130004, label %_130002.0, label %_130003.0
_130002.0:
  %_130012 = bitcast i8* %_3 to i8**
  %_130005 = load i8*, i8** %_130012
  %_130013 = bitcast i8* %_130005 to { i8*, i32, i32, i8* }*
  %_130014 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_130013, i32 0, i32 2
  %_130006 = bitcast i32* %_130014 to i8*
  %_130015 = bitcast i8* %_130006 to i32*
  %_130007 = load i32, i32* %_130015
  %_130016 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_130017 = getelementptr i8*, i8** %_130016, i32 1529
  %_130008 = bitcast i8** %_130017 to i8*
  %_130018 = bitcast i8* %_130008 to i8**
  %_130019 = getelementptr i8*, i8** %_130018, i32 %_130007
  %_130009 = bitcast i8** %_130019 to i8*
  %_130020 = bitcast i8* %_130009 to i8**
  %_110002 = load i8*, i8** %_130020
  %_130021 = bitcast i8* %_110002 to i8* (i8*)*
  %_110003 = call dereferenceable_or_null(8) i8* %_130021(i8* dereferenceable_or_null(8) %_3)
  br label %_130000.0
_120000.0:
  %_120001 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD5valueL16java.lang.ObjectEO"(i8* dereferenceable_or_null(40) %_90001)
  br label %_130000.0
_130000.0:
  %_130001 = phi i8* [%_120001, %_120000.0], [%_110003, %_130002.0]
  br label %_100000.0
_100000.0:
  %_100001 = phi i8* [%_130001, %_130000.0], [%_80001, %_80000.0]
  ret i8* %_100001
_130003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM32scala.collection.mutable.HashMapD9growTableiuEPT32scala.collection.mutable.HashMap"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30007 = icmp slt i32 %_2, 0
  br i1 %_30007, label %_40000.0, label %_50000.0
_40000.0:
  %_40005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-273" to i8*), null
  br i1 %_40005, label %_60000.0, label %_70000.0
_60000.0:
  br label %_80000.0
_70000.0:
  br label %_80000.0
_80000.0:
  %_80001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-273" to i8*), %_70000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_60000.0]
  %_80004 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_2)
  %_80005 = icmp eq i8* %_80004, null
  br i1 %_80005, label %_90000.0, label %_100000.0
_90000.0:
  br label %_110000.0
_100000.0:
  %_580080 = bitcast i8* bitcast (i8* (i8*)* @"_SM17java.lang.IntegerD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_100001 = call dereferenceable_or_null(32) i8* %_580080(i8* nonnull dereferenceable(16) %_80004)
  br label %_110000.0
_110000.0:
  %_110001 = phi i8* [%_100001, %_100000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_90000.0]
  %_580081 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_110002 = call dereferenceable_or_null(32) i8* %_580081(i8* nonnull dereferenceable(32) %_80001, i8* dereferenceable_or_null(32) %_110001)
  %_110004 = icmp eq i8* %_110002, null
  br i1 %_110004, label %_120000.0, label %_130000.0
_120000.0:
  br label %_140000.0
_130000.0:
  br label %_140000.0
_140000.0:
  %_140001 = phi i8* [%_110002, %_130000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_120000.0]
  %_140005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-275" to i8*), null
  br i1 %_140005, label %_150000.0, label %_160000.0
_150000.0:
  br label %_170000.0
_160000.0:
  br label %_170000.0
_50000.0:
  br label %_270000.0
_270000.0:
  %_580003 = icmp ne i8* %_1, null
  br i1 %_580003, label %_580001.0, label %_580002.0
_580001.0:
  %_580082 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_580083 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_580082, i32 0, i32 4
  %_580004 = bitcast i8** %_580083 to i8*
  %_580084 = bitcast i8* %_580004 to i8**
  %_270001 = load i8*, i8** %_580084, !dereferenceable_or_null !{i64 8}
  %_580006 = icmp ne i8* %_270001, null
  br i1 %_580006, label %_580005.0, label %_580002.0
_580005.0:
  %_580085 = bitcast i8* %_270001 to { i8*, i32 }*
  %_580086 = getelementptr { i8*, i32 }, { i8*, i32 }* %_580085, i32 0, i32 1
  %_580007 = bitcast i32* %_580086 to i8*
  %_580087 = bitcast i8* %_580007 to i32*
  %_270002 = load i32, i32* %_580087
  %_270003 = call i32 @"_SM32scala.collection.mutable.HashMapD12newThresholdiiEPT32scala.collection.mutable.HashMap"(i8* dereferenceable_or_null(32) %_1, i32 %_2)
  %_580010 = icmp ne i8* %_1, null
  br i1 %_580010, label %_580009.0, label %_580002.0
_580009.0:
  %_580088 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_580089 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_580088, i32 0, i32 2
  %_580011 = bitcast i32* %_580089 to i8*
  %_580090 = bitcast i8* %_580011 to i32*
  store i32 %_270003, i32* %_580090
  %_270005 = call i32 @"_SM32scala.collection.mutable.HashMapD4sizeiEO"(i8* dereferenceable_or_null(32) %_1)
  %_270007 = icmp eq i32 %_270005, 0
  br i1 %_270007, label %_280000.0, label %_290000.0
_280000.0:
  %_280001 = call dereferenceable_or_null(8) i8* @"_SM38scala.scalanative.runtime.ObjectArray$D5allociL37scala.scalanative.runtime.ObjectArrayEO"(i8* bitcast ({ i8* }* @"_SM38scala.scalanative.runtime.ObjectArray$G8instance" to i8*), i32 %_2)
  %_580015 = icmp ne i8* %_1, null
  br i1 %_580015, label %_580014.0, label %_580002.0
_580014.0:
  %_580091 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_580092 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_580091, i32 0, i32 4
  %_580016 = bitcast i8** %_580092 to i8*
  %_580093 = bitcast i8* %_580016 to i8**
  store i8* %_280001, i8** %_580093
  br label %_300000.0
_290000.0:
  %_580018 = icmp ne i8* %_1, null
  br i1 %_580018, label %_580017.0, label %_580002.0
_580017.0:
  %_580094 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_580095 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_580094, i32 0, i32 4
  %_580019 = bitcast i8** %_580095 to i8*
  %_580096 = bitcast i8* %_580019 to i8**
  %_290001 = load i8*, i8** %_580096, !dereferenceable_or_null !{i64 8}
  %_290002 = call dereferenceable_or_null(8) i8* @"_SM16java.util.ArraysD6copyOfLAL16java.lang.Object_iLAL16java.lang.Object_Eo"(i8* dereferenceable_or_null(8) %_290001, i32 %_2)
  %_580022 = icmp ne i8* %_1, null
  br i1 %_580022, label %_580021.0, label %_580002.0
_580021.0:
  %_580097 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_580098 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_580097, i32 0, i32 4
  %_580023 = bitcast i8** %_580098 to i8*
  %_580099 = bitcast i8* %_580023 to i8**
  store i8* %_290002, i8** %_580099
  %_290006 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM37scala.collection.mutable.HashMap$NodeG4type" to i8*), i64 40)
  %_290007 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM37scala.collection.mutable.HashMap$NodeG4type" to i8*), i64 40)
  br label %_350000.0
_350000.0:
  %_350001 = phi i32 [0, %_580021.0], [%_380001, %_570000.0]
  %_350002 = phi i8* [null, %_580021.0], [%_380002, %_570000.0]
  %_350003 = phi i8* [null, %_580021.0], [%_380003, %_570000.0]
  %_350004 = phi i8* [null, %_580021.0], [%_380004, %_570000.0]
  %_350005 = phi i32 [%_270002, %_580021.0], [%_570002, %_570000.0]
  %_350006 = phi i8* [%_290006, %_580021.0], [%_350006, %_570000.0]
  %_350007 = phi i8* [%_290007, %_580021.0], [%_350007, %_570000.0]
  %_350009 = icmp slt i32 %_350005, %_2
  br i1 %_350009, label %_360000.0, label %_370000.0
_360000.0:
  br label %_380000.0
_380000.0:
  %_380001 = phi i32 [0, %_360000.0], [%_560005, %_560000.0]
  %_380002 = phi i8* [%_350002, %_360000.0], [%_560001, %_560000.0]
  %_380003 = phi i8* [%_350003, %_360000.0], [%_560002, %_560000.0]
  %_380004 = phi i8* [%_350004, %_360000.0], [%_560003, %_560000.0]
  %_380006 = icmp slt i32 %_380001, %_350005
  br i1 %_380006, label %_390000.0, label %_400000.0
_390000.0:
  %_580025 = icmp ne i8* %_1, null
  br i1 %_580025, label %_580024.0, label %_580002.0
_580024.0:
  %_580100 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_580101 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_580100, i32 0, i32 4
  %_580026 = bitcast i8** %_580101 to i8*
  %_580102 = bitcast i8* %_580026 to i8**
  %_390001 = load i8*, i8** %_580102, !dereferenceable_or_null !{i64 8}
  %_580029 = icmp ne i8* %_390001, null
  br i1 %_580029, label %_580028.0, label %_580002.0
_580028.0:
  %_580103 = bitcast i8* %_390001 to { i8*, i32 }*
  %_580104 = getelementptr { i8*, i32 }, { i8*, i32 }* %_580103, i32 0, i32 1
  %_580030 = bitcast i32* %_580104 to i8*
  %_580105 = bitcast i8* %_580030 to i32*
  %_580027 = load i32, i32* %_580105
  %_580033 = icmp sge i32 %_380001, 0
  %_580034 = icmp slt i32 %_380001, %_580027
  %_580035 = and i1 %_580033, %_580034
  br i1 %_580035, label %_580031.0, label %_580032.0
_580031.0:
  %_580106 = bitcast i8* %_390001 to { i8*, i32, i32, [0 x i8*] }*
  %_580107 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_580106, i32 0, i32 3, i32 %_380001
  %_580036 = bitcast i8** %_580107 to i8*
  %_580108 = bitcast i8* %_580036 to i8**
  %_390002 = load i8*, i8** %_580108, !dereferenceable_or_null !{i64 40}
  %_390004 = icmp ne i8* %_390002, null
  br i1 %_390004, label %_410000.0, label %_420000.0
_410000.0:
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8next_$eqL37scala.collection.mutable.HashMap$NodeuEO"(i8* nonnull dereferenceable(40) %_350006, i8* null)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8next_$eqL37scala.collection.mutable.HashMap$NodeuEO"(i8* nonnull dereferenceable(40) %_350007, i8* null)
  br label %_430000.0
_430000.0:
  %_430001 = phi i8* [%_350007, %_410000.0], [%_480001, %_480000.0]
  %_430002 = phi i8* [%_350006, %_410000.0], [%_480002, %_480000.0]
  %_430003 = phi i8* [%_390002, %_410000.0], [%_440001, %_480000.0]
  %_430005 = icmp ne i8* %_430003, null
  br i1 %_430005, label %_440000.0, label %_450000.0
_440000.0:
  %_440001 = call dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD4nextL37scala.collection.mutable.HashMap$NodeEO"(i8* dereferenceable_or_null(40) %_430003)
  %_440002 = call i32 @"_SM37scala.collection.mutable.HashMap$NodeD4hashiEO"(i8* dereferenceable_or_null(40) %_430003)
  %_440005 = and i32 %_440002, %_350005
  %_440006 = icmp eq i32 %_440005, 0
  br i1 %_440006, label %_460000.0, label %_470000.0
_460000.0:
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8next_$eqL37scala.collection.mutable.HashMap$NodeuEO"(i8* dereferenceable_or_null(40) %_430002, i8* dereferenceable_or_null(40) %_430003)
  br label %_480000.0
_470000.0:
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8next_$eqL37scala.collection.mutable.HashMap$NodeuEO"(i8* dereferenceable_or_null(40) %_430001, i8* dereferenceable_or_null(40) %_430003)
  br label %_480000.0
_480000.0:
  %_480001 = phi i8* [%_430003, %_470000.0], [%_430001, %_460000.0]
  %_480002 = phi i8* [%_430002, %_470000.0], [%_430003, %_460000.0]
  br label %_430000.0
_450000.0:
  br label %_490000.0
_490000.0:
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8next_$eqL37scala.collection.mutable.HashMap$NodeuEO"(i8* dereferenceable_or_null(40) %_430002, i8* null)
  %_490002 = call dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD4nextL37scala.collection.mutable.HashMap$NodeEO"(i8* nonnull dereferenceable(40) %_350006)
  %_490004 = icmp ne i8* %_390002, %_490002
  br i1 %_490004, label %_500000.0, label %_510000.0
_500000.0:
  %_580043 = icmp ne i8* %_1, null
  br i1 %_580043, label %_580042.0, label %_580002.0
_580042.0:
  %_580109 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_580110 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_580109, i32 0, i32 4
  %_580044 = bitcast i8** %_580110 to i8*
  %_580111 = bitcast i8* %_580044 to i8**
  %_500001 = load i8*, i8** %_580111, !dereferenceable_or_null !{i64 8}
  %_500002 = call dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD4nextL37scala.collection.mutable.HashMap$NodeEO"(i8* nonnull dereferenceable(40) %_350006)
  %_580048 = icmp ne i8* %_500001, null
  br i1 %_580048, label %_580047.0, label %_580002.0
_580047.0:
  %_580112 = bitcast i8* %_500001 to { i8*, i32 }*
  %_580113 = getelementptr { i8*, i32 }, { i8*, i32 }* %_580112, i32 0, i32 1
  %_580049 = bitcast i32* %_580113 to i8*
  %_580114 = bitcast i8* %_580049 to i32*
  %_580046 = load i32, i32* %_580114
  %_580051 = icmp sge i32 %_380001, 0
  %_580052 = icmp slt i32 %_380001, %_580046
  %_580053 = and i1 %_580051, %_580052
  br i1 %_580053, label %_580050.0, label %_580032.0
_580050.0:
  %_580115 = bitcast i8* %_500001 to { i8*, i32, i32, [0 x i8*] }*
  %_580116 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_580115, i32 0, i32 3, i32 %_380001
  %_580054 = bitcast i8** %_580116 to i8*
  %_580117 = bitcast i8* %_580054 to i8**
  store i8* %_500002, i8** %_580117
  br label %_520000.0
_510000.0:
  br label %_520000.0
_520000.0:
  %_520001 = call dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD4nextL37scala.collection.mutable.HashMap$NodeEO"(i8* nonnull dereferenceable(40) %_350007)
  %_520003 = icmp ne i8* %_520001, null
  br i1 %_520003, label %_530000.0, label %_540000.0
_530000.0:
  %_580056 = icmp ne i8* %_1, null
  br i1 %_580056, label %_580055.0, label %_580002.0
_580055.0:
  %_580118 = bitcast i8* %_1 to { i8*, double, i32, i32, i8* }*
  %_580119 = getelementptr { i8*, double, i32, i32, i8* }, { i8*, double, i32, i32, i8* }* %_580118, i32 0, i32 4
  %_580057 = bitcast i8** %_580119 to i8*
  %_580120 = bitcast i8* %_580057 to i8**
  %_530001 = load i8*, i8** %_580120, !dereferenceable_or_null !{i64 8}
  %_530003 = call dereferenceable_or_null(40) i8* @"_SM37scala.collection.mutable.HashMap$NodeD4nextL37scala.collection.mutable.HashMap$NodeEO"(i8* nonnull dereferenceable(40) %_350007)
  %_530004 = add i32 %_380001, %_350005
  %_580061 = icmp ne i8* %_530001, null
  br i1 %_580061, label %_580060.0, label %_580002.0
_580060.0:
  %_580121 = bitcast i8* %_530001 to { i8*, i32 }*
  %_580122 = getelementptr { i8*, i32 }, { i8*, i32 }* %_580121, i32 0, i32 1
  %_580062 = bitcast i32* %_580122 to i8*
  %_580123 = bitcast i8* %_580062 to i32*
  %_580059 = load i32, i32* %_580123
  %_580064 = icmp sge i32 %_530004, 0
  %_580065 = icmp slt i32 %_530004, %_580059
  %_580066 = and i1 %_580064, %_580065
  br i1 %_580066, label %_580063.0, label %_580032.0
_580063.0:
  %_580124 = bitcast i8* %_530001 to { i8*, i32, i32, [0 x i8*] }*
  %_580125 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_580124, i32 0, i32 3, i32 %_530004
  %_580067 = bitcast i8** %_580125 to i8*
  %_580126 = bitcast i8* %_580067 to i8**
  store i8* %_530003, i8** %_580126
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.HashMap$NodeD8next_$eqL37scala.collection.mutable.HashMap$NodeuEO"(i8* dereferenceable_or_null(40) %_430001, i8* null)
  br label %_550000.0
_540000.0:
  br label %_550000.0
_550000.0:
  br label %_560000.0
_420000.0:
  br label %_560000.0
_560000.0:
  %_560001 = phi i8* [%_380002, %_420000.0], [%_430001, %_550000.0]
  %_560002 = phi i8* [%_380003, %_420000.0], [%_430002, %_550000.0]
  %_560003 = phi i8* [%_380004, %_420000.0], [%_430003, %_550000.0]
  %_560005 = add i32 %_380001, 1
  br label %_380000.0
_400000.0:
  br label %_570000.0
_570000.0:
  %_580069 = and i32 1, 31
  %_570002 = shl i32 %_350005, %_580069
  br label %_350000.0
_370000.0:
  br label %_580000.0
_580000.0:
  br label %_300000.0
_300000.0:
  %_300001 = phi i32 [%_350001, %_580000.0], [0, %_580014.0]
  %_300002 = phi i8* [%_350002, %_580000.0], [null, %_580014.0]
  %_300003 = phi i8* [%_350003, %_580000.0], [null, %_580014.0]
  %_300004 = phi i8* [%_350004, %_580000.0], [null, %_580014.0]
  %_300005 = phi i32 [%_350005, %_580000.0], [%_270002, %_580014.0]
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_170000.0:
  %_170001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-275" to i8*), %_160000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_150000.0]
  %_580127 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_170002 = call dereferenceable_or_null(32) i8* %_580127(i8* dereferenceable_or_null(32) %_140001, i8* nonnull dereferenceable(32) %_170001)
  br label %_250000.0
_250000.0:
  %_250001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM26java.lang.RuntimeExceptionG4type" to i8*), i64 40)
  %_580128 = bitcast i8* %_250001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_580129 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_580128, i32 0, i32 5
  %_580071 = bitcast i1* %_580129 to i8*
  %_580130 = bitcast i8* %_580071 to i1*
  store i1 true, i1* %_580130
  %_580131 = bitcast i8* %_250001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_580132 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_580131, i32 0, i32 4
  %_580073 = bitcast i1* %_580132 to i8*
  %_580133 = bitcast i8* %_580073 to i1*
  store i1 true, i1* %_580133
  %_580134 = bitcast i8* %_250001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_580135 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_580134, i32 0, i32 3
  %_580075 = bitcast i8** %_580135 to i8*
  %_580136 = bitcast i8* %_580075 to i8**
  store i8* %_170002, i8** %_580136
  %_250005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_250001)
  br label %_260000.0
_260000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_250001)
  unreachable
_580002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_580032.0:
  %_580078 = phi i32 [%_380001, %_580028.0], [%_530004, %_580060.0], [%_380001, %_580047.0]
  %_580137 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_580137(i8* null, i32 %_580078)
  unreachable
}

define i32 @"_SM32scala.collection.mutable.HashMapD9knownSizeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM32scala.collection.mutable.HashMapD4sizeiEO"(i8* dereferenceable_or_null(32) %_1)
  ret i32 %_20001
}

define nonnull dereferenceable(40) i8* @"_SM33scala.collection.immutable.Range$D49scala$collection$immutable$Range$$emptyRangeErrorL16java.lang.StringL19java.lang.ThrowableEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30003 = icmp eq i8* %_2, null
  br i1 %_30003, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  br label %_60000.0
_60000.0:
  %_60001 = phi i8* [%_2, %_50000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_40000.0]
  %_60005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-277" to i8*), null
  br i1 %_60005, label %_70000.0, label %_80000.0
_70000.0:
  br label %_90000.0
_80000.0:
  br label %_90000.0
_90000.0:
  %_90001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-277" to i8*), %_80000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_70000.0]
  %_190007 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_90002 = call dereferenceable_or_null(32) i8* %_190007(i8* dereferenceable_or_null(32) %_60001, i8* nonnull dereferenceable(32) %_90001)
  br label %_180000.0
_180000.0:
  %_180001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.util.NoSuchElementExceptionG4type" to i8*), i64 40)
  %_190008 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_190009 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_190008, i32 0, i32 5
  %_190002 = bitcast i1* %_190009 to i8*
  %_190010 = bitcast i8* %_190002 to i1*
  store i1 true, i1* %_190010
  %_190011 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_190012 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_190011, i32 0, i32 4
  %_190004 = bitcast i1* %_190012 to i8*
  %_190013 = bitcast i8* %_190004 to i1*
  store i1 true, i1* %_190013
  %_190014 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_190015 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_190014, i32 0, i32 3
  %_190006 = bitcast i8** %_190015 to i8*
  %_190016 = bitcast i8* %_190006 to i8**
  store i8* %_90002, i8** %_190016
  %_180005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_180001)
  br label %_190000.0
_190000.0:
  ret i8* %_180001
}

define nonnull dereferenceable(32) i8* @"_SM33scala.collection.immutable.Range$D5applyiiL42scala.collection.immutable.Range$ExclusiveEO"(i8* %_1, i32 %_2, i32 %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_90001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM42scala.collection.immutable.Range$ExclusiveG4type" to i8*), i64 32)
  %_390033 = bitcast i8* %_90001 to { i8*, i32, i32, i32, i1, i32, i32 }*
  %_390034 = getelementptr { i8*, i32, i32, i32, i1, i32, i32 }, { i8*, i32, i32, i32, i1, i32, i32 }* %_390033, i32 0, i32 1
  %_390003 = bitcast i32* %_390034 to i8*
  %_390035 = bitcast i8* %_390003 to i32*
  store i32 1, i32* %_390035
  %_390036 = bitcast i8* %_90001 to { i8*, i32, i32, i32, i1, i32, i32 }*
  %_390037 = getelementptr { i8*, i32, i32, i32, i1, i32, i32 }, { i8*, i32, i32, i32, i1, i32, i32 }* %_390036, i32 0, i32 6
  %_390005 = bitcast i32* %_390037 to i8*
  %_390038 = bitcast i8* %_390005 to i32*
  store i32 %_3, i32* %_390038
  %_390039 = bitcast i8* %_90001 to { i8*, i32, i32, i32, i1, i32, i32 }*
  %_390040 = getelementptr { i8*, i32, i32, i32, i1, i32, i32 }, { i8*, i32, i32, i32, i1, i32, i32 }* %_390039, i32 0, i32 3
  %_390007 = bitcast i32* %_390040 to i8*
  %_390041 = bitcast i8* %_390007 to i32*
  store i32 %_2, i32* %_390041
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(32) %_90001)
  %_60009 = icmp sgt i32 %_2, %_3
  br i1 %_60009, label %_110000.0, label %_120000.0
_110000.0:
  br label %_130000.0
_120000.0:
  br label %_130000.0
_130000.0:
  %_130001 = phi i1 [false, %_120000.0], [true, %_110000.0]
  br i1 %_130001, label %_140000.0, label %_150000.0
_140000.0:
  br label %_160000.0
_150000.0:
  %_150002 = icmp slt i32 %_2, %_3
  br i1 %_150002, label %_170000.0, label %_180000.0
_170000.0:
  br label %_190000.0
_180000.0:
  br label %_190000.0
_190000.0:
  br label %_160000.0
_160000.0:
  %_160001 = phi i1 [false, %_190000.0], [true, %_140000.0]
  br i1 %_160001, label %_200000.0, label %_210000.0
_200000.0:
  br label %_220000.0
_210000.0:
  %_210002 = icmp eq i32 %_2, %_3
  br i1 %_210002, label %_230000.0, label %_240000.0
_230000.0:
  %_390042 = bitcast i8* bitcast (i1 (i8*)* @"_SM42scala.collection.immutable.Range$ExclusiveD11isInclusivezEO" to i8*) to i1 (i8*)*
  %_230001 = call i1 %_390042(i8* nonnull dereferenceable(32) %_90001)
  %_230003 = xor i1 %_230001, true
  br label %_250000.0
_240000.0:
  br label %_250000.0
_250000.0:
  %_250001 = phi i1 [false, %_240000.0], [%_230003, %_230000.0]
  br label %_220000.0
_220000.0:
  %_220001 = phi i1 [%_250001, %_250000.0], [true, %_200000.0]
  %_390043 = bitcast i8* %_90001 to { i8*, i32, i32, i32, i1, i32, i32 }*
  %_390044 = getelementptr { i8*, i32, i32, i32, i1, i32, i32 }, { i8*, i32, i32, i32, i1, i32, i32 }* %_390043, i32 0, i32 4
  %_390027 = bitcast i1* %_390044 to i8*
  %_390045 = bitcast i8* %_390027 to i1*
  store i1 %_220001, i1* %_390045
  br label %_260000.0
_260000.0:
  %_390046 = bitcast i8* %_90001 to { i8*, i32, i32, i32, i1, i32, i32 }*
  %_390047 = getelementptr { i8*, i32, i32, i32, i1, i32, i32 }, { i8*, i32, i32, i32, i1, i32, i32 }* %_390046, i32 0, i32 4
  %_390028 = bitcast i1* %_390047 to i8*
  %_390048 = bitcast i8* %_390028 to i1*
  %_270001 = load i1, i1* %_390048
  br i1 %_270001, label %_280000.0, label %_290000.0
_280000.0:
  br label %_300000.0
_290000.0:
  %_290001 = call i64 @"_SM32scala.collection.immutable.RangeD10longLengthjEPT32scala.collection.immutable.Range"(i8* nonnull dereferenceable(32) %_90001)
  %_290003 = icmp sgt i64 %_290001, 2147483647
  br i1 %_290003, label %_310000.0, label %_320000.0
_310000.0:
  br label %_330000.0
_320000.0:
  %_320002 = trunc i64 %_290001 to i32
  br label %_330000.0
_330000.0:
  %_330001 = phi i32 [%_320002, %_320000.0], [-1, %_310000.0]
  br label %_300000.0
_300000.0:
  %_300001 = phi i32 [%_330001, %_330000.0], [0, %_280000.0]
  br label %_340000.0
_340000.0:
  %_390049 = bitcast i8* %_90001 to { i8*, i32, i32, i32, i1, i32, i32 }*
  %_390050 = getelementptr { i8*, i32, i32, i32, i1, i32, i32 }, { i8*, i32, i32, i32, i1, i32, i32 }* %_390049, i32 0, i32 5
  %_390030 = bitcast i32* %_390050 to i8*
  %_390051 = bitcast i8* %_390030 to i32*
  store i32 %_300001, i32* %_390051
  br label %_350000.0
_350000.0:
  %_390052 = bitcast i8* bitcast (i1 (i8*)* @"_SM42scala.collection.immutable.Range$ExclusiveD11isInclusivezEO" to i8*) to i1 (i8*)*
  %_350001 = call i1 %_390052(i8* nonnull dereferenceable(32) %_90001)
  br i1 %_350001, label %_360000.0, label %_370000.0
_360000.0:
  br label %_380000.0
_370000.0:
  %_370002 = sub i32 %_3, 1
  br label %_380000.0
_380000.0:
  %_380001 = phi i32 [%_370002, %_370000.0], [%_3, %_360000.0]
  br label %_390000.0
_390000.0:
  %_390053 = bitcast i8* %_90001 to { i8*, i32, i32, i32, i1, i32, i32 }*
  %_390054 = getelementptr { i8*, i32, i32, i32, i1, i32, i32 }, { i8*, i32, i32, i32, i1, i32, i32 }* %_390053, i32 0, i32 2
  %_390032 = bitcast i32* %_390054 to i8*
  %_390055 = bitcast i8* %_390032 to i32*
  store i32 %_380001, i32* %_390055
  ret i8* %_90001
}

define nonnull dereferenceable(8) i8* @"_SM33scala.collection.immutable.Range$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(32) i8* @"_SM33scala.scalanative.unsafe.package$D11fromCStringL28scala.scalanative.unsafe.PtrL24java.nio.charset.CharsetL16java.lang.StringEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40003 = icmp eq i8* %_2, null
  br i1 %_40003, label %_50000.0, label %_60000.0
_50000.0:
  br label %_70000.0
_60000.0:
  %_340028 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D10unboxToPtrL16java.lang.ObjectR_EO" to i8*) to i8* (i8*, i8*)*
  %_60002 = call i8* %_340028(i8* null, i8* dereferenceable_or_null(16) %_2)
  %_60003 = call i64 @"strlen"(i8* %_60002)
  %_340029 = bitcast i8* bitcast (i8* (i8*, i64)* @"_SM32scala.scalanative.runtime.Boxes$D10boxToULongjL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i64)*
  %_60005 = call dereferenceable_or_null(16) i8* %_340029(i8* null, i64 %_60003)
  %_60006 = call i32 @"_SM32scala.scalanative.unsigned.ULongD5toIntiEO"(i8* nonnull dereferenceable(16) %_60005)
  %_60007 = call dereferenceable_or_null(8) i8* @"_SM36scala.scalanative.runtime.ByteArray$D5allociL35scala.scalanative.runtime.ByteArrayEO"(i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.ByteArray$G8instance" to i8*), i32 %_60006)
  br label %_80000.0
_80000.0:
  %_80001 = phi i32 [0, %_60000.0], [%_90006, %_340014.0]
  %_80003 = icmp slt i32 %_80001, %_60006
  br i1 %_80003, label %_90000.0, label %_100000.0
_90000.0:
  %_90002 = call i64 @"_SM10scala.Int$D8int2longijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM10scala.Int$G8instance" to i8*), i32 %_80001)
  %_340009 = icmp ne i8* %_2, null
  br i1 %_340009, label %_340007.0, label %_340008.0
_340007.0:
  %_340030 = bitcast i8* %_2 to { i8*, i8* }*
  %_340031 = getelementptr { i8*, i8* }, { i8*, i8* }* %_340030, i32 0, i32 1
  %_340010 = bitcast i8** %_340031 to i8*
  %_340032 = bitcast i8* %_340010 to i8**
  %_240001 = load i8*, i8** %_340032
  %_260003 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 1)
  %_260004 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_260003)
  %_230003 = call i64 @"_SM32scala.scalanative.unsigned.ULongD6toLongjEO"(i8* dereferenceable_or_null(16) %_260004)
  %_320002 = mul i64 %_90002, %_230003
  %_340033 = bitcast i8* %_240001 to i8*
  %_340034 = getelementptr i8, i8* %_340033, i64 %_320002
  %_320003 = bitcast i8* %_340034 to i8*
  %_340035 = bitcast i8* %_320003 to i8*
  %_320004 = load i8, i8* %_340035
  %_340036 = bitcast i8* %_60007 to { i8*, i32 }*
  %_340037 = getelementptr { i8*, i32 }, { i8*, i32 }* %_340036, i32 0, i32 1
  %_340013 = bitcast i32* %_340037 to i8*
  %_340038 = bitcast i8* %_340013 to i32*
  %_340012 = load i32, i32* %_340038
  %_340016 = icmp sge i32 %_80001, 0
  %_340017 = icmp slt i32 %_80001, %_340012
  %_340018 = and i1 %_340016, %_340017
  br i1 %_340018, label %_340014.0, label %_340015.0
_340014.0:
  %_340039 = bitcast i8* %_60007 to { i8*, i32, i32, [0 x i8] }*
  %_340040 = getelementptr { i8*, i32, i32, [0 x i8] }, { i8*, i32, i32, [0 x i8] }* %_340039, i32 0, i32 3, i32 %_80001
  %_340019 = bitcast i8* %_340040 to i8*
  %_340041 = bitcast i8* %_340019 to i8*
  store i8 %_320004, i8* %_340041
  %_90006 = add i32 %_80001, 1
  br label %_80000.0
_100000.0:
  %_340042 = bitcast i8* %_60007 to { i8*, i32 }*
  %_340043 = getelementptr { i8*, i32 }, { i8*, i32 }* %_340042, i32 0, i32 1
  %_340020 = bitcast i32* %_340043 to i8*
  %_340044 = bitcast i8* %_340020 to i32*
  %_330001 = load i32, i32* %_340044
  %_340002 = call dereferenceable_or_null(48) i8* @"_SM20java.nio.ByteBuffer$D4wrapLAb_iiL19java.nio.ByteBufferEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM20java.nio.ByteBuffer$G8instance" to i8*), i8* nonnull dereferenceable(8) %_60007, i32 0, i32 %_330001)
  %_340003 = call dereferenceable_or_null(40) i8* @"_SM24java.nio.charset.CharsetD6decodeL19java.nio.ByteBufferL19java.nio.CharBufferEO"(i8* dereferenceable_or_null(32) %_3, i8* dereferenceable_or_null(48) %_340002)
  %_340004 = call dereferenceable_or_null(8) i8* @"_SM19java.nio.CharBufferD5arrayLAc_EO"(i8* dereferenceable_or_null(40) %_340003)
  %_340005 = call i32 @"_SM19java.nio.CharBufferD6lengthiEO"(i8* dereferenceable_or_null(40) %_340003)
  %_100002 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM16java.lang.StringG4type" to i8*), i64 32)
  %_340045 = bitcast i8* %_100002 to { i8*, i8*, i32, i32, i32 }*
  %_340046 = getelementptr { i8*, i8*, i32, i32, i32 }, { i8*, i8*, i32, i32, i32 }* %_340045, i32 0, i32 1
  %_340022 = bitcast i8** %_340046 to i8*
  %_340047 = bitcast i8* %_340022 to i8**
  store i8* %_340004, i8** %_340047
  %_340048 = bitcast i8* %_100002 to { i8*, i8*, i32, i32, i32 }*
  %_340049 = getelementptr { i8*, i8*, i32, i32, i32 }, { i8*, i8*, i32, i32, i32 }* %_340048, i32 0, i32 3
  %_340024 = bitcast i32* %_340049 to i8*
  %_340050 = bitcast i8* %_340024 to i32*
  store i32 %_340005, i32* %_340050
  br label %_70000.0
_70000.0:
  %_70001 = phi i32 [%_80001, %_100000.0], [0, %_50000.0]
  %_70002 = phi i8* [%_100002, %_100000.0], [null, %_50000.0]
  ret i8* %_70002
_340008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_340015.0:
  %_340026 = phi i32 [%_80001, %_340007.0]
  %_340051 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_340051(i8* null, i32 %_340026)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM33scala.scalanative.unsafe.package$D21fromCString$default$2L24java.nio.charset.CharsetEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM24java.nio.charset.CharsetD14defaultCharsetL24java.nio.charset.CharsetEo"()
  ret i8* %_20001
}

define dereferenceable_or_null(16) i8* @"_SM33scala.scalanative.unsafe.package$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 113
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 16}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM33scala.scalanative.unsafe.package$G4type" to i8*), i64 16)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM33scala.scalanative.unsafe.package$RE"(i8* dereferenceable_or_null(16) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM33scala.scalanative.unsafe.package$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  call nonnull dereferenceable(8) i8* @"_SM44scala.scalanative.unsafe.UnsafePackageCompatD6$init$uEO"(i8* dereferenceable_or_null(16) %_1)
  %_20004 = call i64 @"scalanative_wide_char_size"()
  %_20013 = bitcast i8* bitcast (i8* (i8*, i64)* @"_SM32scala.scalanative.runtime.Boxes$D10boxToULongjL32scala.scalanative.unsigned.ULongEO" to i8*) to i8* (i8*, i64)*
  %_20006 = call dereferenceable_or_null(16) i8* %_20013(i8* null, i64 %_20004)
  %_20007 = call i32 @"_SM32scala.scalanative.unsigned.ULongD5toIntiEO"(i8* nonnull dereferenceable(16) %_20006)
  %_20008 = call dereferenceable_or_null(16) i8* @"_SM33scala.scalanative.unsafe.package$G4load"()
  %_20014 = bitcast i8* %_20008 to { i8*, i32 }*
  %_20015 = getelementptr { i8*, i32 }, { i8*, i32 }* %_20014, i32 0, i32 1
  %_20012 = bitcast i32* %_20015 to i8*
  %_20016 = bitcast i8* %_20012 to i32*
  store i32 %_20007, i32* %_20016
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(32) i8* @"_SM34scala.collection.IndexedSeqView$IdD12stringPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = bitcast i8* bitcast (i8* (i8*)* @"_SM31scala.collection.IndexedSeqViewD12stringPrefixL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_20001 = call dereferenceable_or_null(32) i8* %_20002(i8* dereferenceable_or_null(16) %_1)
  ret i8* %_20001
}

define i32 @"_SM34scala.collection.IndexedSeqView$IdD13lengthCompareiiEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i32 @"_SM30scala.collection.IndexedSeqOpsD13lengthCompareiiEO"(i8* dereferenceable_or_null(16) %_1, i32 %_2)
  ret i32 %_30001
}

define dereferenceable_or_null(8) i8* @"_SM34scala.collection.IndexedSeqView$IdD4viewL31scala.collection.IndexedSeqViewEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(8) i8* @"_SM31scala.collection.IndexedSeqViewD4viewL31scala.collection.IndexedSeqViewEO"(i8* dereferenceable_or_null(16) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM34scala.collection.IndexedSeqView$IdD8iteratorL25scala.collection.IteratorEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = bitcast i8* bitcast (i8* (i8*)* @"_SM31scala.collection.IndexedSeqViewD8iteratorL25scala.collection.IteratorEO" to i8*) to i8* (i8*)*
  %_20001 = call dereferenceable_or_null(8) i8* %_20002(i8* dereferenceable_or_null(16) %_1)
  ret i8* %_20001
}

define i32 @"_SM34scala.collection.IndexedSeqView$IdD9knownSizeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM30scala.collection.IndexedSeqOpsD9knownSizeiEO"(i8* dereferenceable_or_null(16) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(8) i8* @"_SM34scala.collection.Iterator$$anon$19D13sliceIteratoriiL25scala.collection.IteratorEO"(i8* %_1, i32 %_2, i32 %_3) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call dereferenceable_or_null(8) i8* @"_SM34scala.collection.Iterator$$anon$19D13sliceIteratoriiL34scala.collection.Iterator$$anon$19EO"(i8* dereferenceable_or_null(8) %_1, i32 %_2, i32 %_3)
  ret i8* %_40001
}

define dereferenceable_or_null(8) i8* @"_SM34scala.collection.Iterator$$anon$19D13sliceIteratoriiL34scala.collection.Iterator$$anon$19EO"(i8* %_1, i32 %_2, i32 %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  ret i8* %_1
}

define i8* @"_SM34scala.collection.Iterator$$anon$19D4nextL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  call i8* @"_SM34scala.collection.Iterator$$anon$19D4nextnEO"(i8* dereferenceable_or_null(8) %_1)
  br label %_20003.0
_20003.0:
  %_20006 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_20006(i8* null)
  unreachable
}

define i8* @"_SM34scala.collection.Iterator$$anon$19D4nextnEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  br label %_110000.0
_110000.0:
  %_110001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.util.NoSuchElementExceptionG4type" to i8*), i64 40)
  %_120008 = bitcast i8* %_110001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_120009 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_120008, i32 0, i32 5
  %_120002 = bitcast i1* %_120009 to i8*
  %_120010 = bitcast i8* %_120002 to i1*
  store i1 true, i1* %_120010
  %_120011 = bitcast i8* %_110001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_120012 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_120011, i32 0, i32 4
  %_120004 = bitcast i1* %_120012 to i8*
  %_120013 = bitcast i8* %_120004 to i1*
  store i1 true, i1* %_120013
  %_120014 = bitcast i8* %_110001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_120015 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_120014, i32 0, i32 3
  %_120006 = bitcast i8** %_120015 to i8*
  %_120016 = bitcast i8* %_120006 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-279" to i8*), i8** %_120016
  %_110005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_110001)
  br label %_120000.0
_120000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_110001)
  unreachable
}

define i1 @"_SM34scala.collection.Iterator$$anon$19D7hasNextzEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i1 false
}

define i32 @"_SM34scala.collection.Iterator$$anon$19D9knownSizeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i32 0
}

define dereferenceable_or_null(8) i8* @"_SM34scala.collection.immutable.Vector3D11vectorSliceiLAL16java.lang.Object_EO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  switch i32 %_2, label %_40000.0 [
    i32 0, label %_50000.0
    i32 1, label %_60000.0
    i32 2, label %_70000.0
    i32 3, label %_80000.0
    i32 4, label %_90000.0
  ]
_50000.0:
  %_250004 = icmp ne i8* %_1, null
  br i1 %_250004, label %_250002.0, label %_250003.0
_250002.0:
  %_250026 = bitcast i8* %_1 to { i8*, i8* }*
  %_250027 = getelementptr { i8*, i8* }, { i8*, i8* }* %_250026, i32 0, i32 1
  %_250005 = bitcast i8** %_250027 to i8*
  %_250028 = bitcast i8* %_250005 to i8**
  %_200001 = load i8*, i8** %_250028, !dereferenceable_or_null !{i64 8}
  br label %_210000.0
_60000.0:
  %_250007 = icmp ne i8* %_1, null
  br i1 %_250007, label %_250006.0, label %_250003.0
_250006.0:
  %_250029 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_250030 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_250029, i32 0, i32 5
  %_250008 = bitcast i8** %_250030 to i8*
  %_250031 = bitcast i8* %_250008 to i8**
  %_220001 = load i8*, i8** %_250031, !dereferenceable_or_null !{i64 8}
  br label %_210000.0
_70000.0:
  %_250010 = icmp ne i8* %_1, null
  br i1 %_250010, label %_250009.0, label %_250003.0
_250009.0:
  %_250032 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_250033 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_250032, i32 0, i32 7
  %_250011 = bitcast i8** %_250033 to i8*
  %_250034 = bitcast i8* %_250011 to i8**
  %_230001 = load i8*, i8** %_250034, !dereferenceable_or_null !{i64 8}
  br label %_210000.0
_80000.0:
  %_250013 = icmp ne i8* %_1, null
  br i1 %_250013, label %_250012.0, label %_250003.0
_250012.0:
  %_250035 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_250036 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_250035, i32 0, i32 8
  %_250014 = bitcast i8** %_250036 to i8*
  %_250037 = bitcast i8* %_250014 to i8**
  %_240001 = load i8*, i8** %_250037, !dereferenceable_or_null !{i64 8}
  br label %_210000.0
_90000.0:
  %_250016 = icmp ne i8* %_1, null
  br i1 %_250016, label %_250015.0, label %_250003.0
_250015.0:
  %_250038 = bitcast i8* %_1 to { i8*, i8*, i32, i8* }*
  %_250039 = getelementptr { i8*, i8*, i32, i8* }, { i8*, i8*, i32, i8* }* %_250038, i32 0, i32 3
  %_250017 = bitcast i8** %_250039 to i8*
  %_250040 = bitcast i8* %_250017 to i8**
  %_250001 = load i8*, i8** %_250040, !dereferenceable_or_null !{i64 8}
  br label %_210000.0
_210000.0:
  %_210001 = phi i8* [%_250001, %_250015.0], [%_240001, %_250012.0], [%_230001, %_250009.0], [%_220001, %_250006.0], [%_200001, %_250002.0]
  ret i8* %_210001
_40000.0:
  br label %_180000.0
_180000.0:
  %_180001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM16scala.MatchErrorG4type" to i8*), i64 64)
  %_250041 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_250042 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_250041, i32 0, i32 5
  %_250019 = bitcast i1* %_250042 to i8*
  %_250043 = bitcast i8* %_250019 to i1*
  store i1 true, i1* %_250043
  %_250044 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_250045 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_250044, i32 0, i32 4
  %_250021 = bitcast i1* %_250045 to i8*
  %_250046 = bitcast i8* %_250021 to i1*
  store i1 true, i1* %_250046
  %_180004 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D12boxToIntegeriL17java.lang.IntegerEO"(i8* null, i32 %_2)
  %_250047 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }*
  %_250048 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }, { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }* %_250047, i32 0, i32 6
  %_250023 = bitcast i8** %_250048 to i8*
  %_250049 = bitcast i8* %_250023 to i8**
  store i8* %_180004, i8** %_250049
  %_180006 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(64) %_180001)
  br label %_190000.0
_190000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(64) %_180001)
  unreachable
_250003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM34scala.collection.immutable.Vector3D14copy$default$2iEPT34scala.collection.immutable.Vector3"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_30008 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_30007, i32 0, i32 4
  %_30005 = bitcast i32* %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i32*
  %_30001 = load i32, i32* %_30009
  ret i32 %_30001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM34scala.collection.immutable.Vector3D14copy$default$4iEPT34scala.collection.immutable.Vector3"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_30008 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_30007, i32 0, i32 6
  %_30005 = bitcast i32* %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i32*
  %_30001 = load i32, i32* %_30009
  ret i32 %_30001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM34scala.collection.immutable.Vector3D14copy$default$8iEPT34scala.collection.immutable.Vector3"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i8*, i32, i8* }*
  %_30008 = getelementptr { i8*, i8*, i32, i8* }, { i8*, i8*, i32, i8* }* %_30007, i32 0, i32 2
  %_30005 = bitcast i32* %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i32*
  %_30001 = load i32, i32* %_30009
  ret i32 %_30001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM34scala.collection.immutable.Vector3D16vectorSliceCountiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i32 5
}

define dereferenceable_or_null(16) i8* @"_SM34scala.collection.immutable.Vector3D3mapL15scala.Function1L16java.lang.ObjectEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM34scala.collection.immutable.Vector3D3mapL15scala.Function1L33scala.collection.immutable.VectorEO" to i8*) to i8* (i8*, i8*)*
  %_30001 = call dereferenceable_or_null(16) i8* %_30002(i8* dereferenceable_or_null(72) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(72) i8* @"_SM34scala.collection.immutable.Vector3D3mapL15scala.Function1L33scala.collection.immutable.VectorEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(56) i8* @"_SM41scala.collection.immutable.VectorStatics$G4load"()
  %_80004 = icmp ne i8* %_1, null
  br i1 %_80004, label %_80002.0, label %_80003.0
_80002.0:
  %_80019 = bitcast i8* %_1 to { i8*, i8* }*
  %_80020 = getelementptr { i8*, i8* }, { i8*, i8* }* %_80019, i32 0, i32 1
  %_80005 = bitcast i8** %_80020 to i8*
  %_80021 = bitcast i8* %_80005 to i8**
  %_40001 = load i8*, i8** %_80021, !dereferenceable_or_null !{i64 8}
  %_30002 = call dereferenceable_or_null(8) i8* @"_SM41scala.collection.immutable.VectorStatics$D9mapElems1LAL16java.lang.Object_L15scala.Function1LAL16java.lang.Object_EO"(i8* nonnull dereferenceable(56) %_30001, i8* dereferenceable_or_null(8) %_40001, i8* dereferenceable_or_null(8) %_2)
  %_80007 = icmp ne i8* %_1, null
  br i1 %_80007, label %_80006.0, label %_80003.0
_80006.0:
  %_80022 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_80023 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_80022, i32 0, i32 5
  %_80008 = bitcast i8** %_80023 to i8*
  %_80024 = bitcast i8* %_80008 to i8**
  %_50001 = load i8*, i8** %_80024, !dereferenceable_or_null !{i64 8}
  %_30003 = call dereferenceable_or_null(8) i8* @"_SM41scala.collection.immutable.VectorStatics$D8mapElemsiLAL16java.lang.Object_L15scala.Function1LAL16java.lang.Object_EO"(i8* nonnull dereferenceable(56) %_30001, i32 2, i8* dereferenceable_or_null(8) %_50001, i8* dereferenceable_or_null(8) %_2)
  %_80010 = icmp ne i8* %_1, null
  br i1 %_80010, label %_80009.0, label %_80003.0
_80009.0:
  %_80025 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_80026 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_80025, i32 0, i32 7
  %_80011 = bitcast i8** %_80026 to i8*
  %_80027 = bitcast i8* %_80011 to i8**
  %_60001 = load i8*, i8** %_80027, !dereferenceable_or_null !{i64 8}
  %_30004 = call dereferenceable_or_null(8) i8* @"_SM41scala.collection.immutable.VectorStatics$D8mapElemsiLAL16java.lang.Object_L15scala.Function1LAL16java.lang.Object_EO"(i8* nonnull dereferenceable(56) %_30001, i32 3, i8* dereferenceable_or_null(8) %_60001, i8* dereferenceable_or_null(8) %_2)
  %_80013 = icmp ne i8* %_1, null
  br i1 %_80013, label %_80012.0, label %_80003.0
_80012.0:
  %_80028 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_80029 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_80028, i32 0, i32 8
  %_80014 = bitcast i8** %_80029 to i8*
  %_80030 = bitcast i8* %_80014 to i8**
  %_70001 = load i8*, i8** %_80030, !dereferenceable_or_null !{i64 8}
  %_30005 = call dereferenceable_or_null(8) i8* @"_SM41scala.collection.immutable.VectorStatics$D8mapElemsiLAL16java.lang.Object_L15scala.Function1LAL16java.lang.Object_EO"(i8* nonnull dereferenceable(56) %_30001, i32 2, i8* dereferenceable_or_null(8) %_70001, i8* dereferenceable_or_null(8) %_2)
  %_80016 = icmp ne i8* %_1, null
  br i1 %_80016, label %_80015.0, label %_80003.0
_80015.0:
  %_80031 = bitcast i8* %_1 to { i8*, i8*, i32, i8* }*
  %_80032 = getelementptr { i8*, i8*, i32, i8* }, { i8*, i8*, i32, i8* }* %_80031, i32 0, i32 3
  %_80017 = bitcast i8** %_80032 to i8*
  %_80033 = bitcast i8* %_80017 to i8**
  %_80001 = load i8*, i8** %_80033, !dereferenceable_or_null !{i64 8}
  %_30006 = call dereferenceable_or_null(8) i8* @"_SM41scala.collection.immutable.VectorStatics$D9mapElems1LAL16java.lang.Object_L15scala.Function1LAL16java.lang.Object_EO"(i8* nonnull dereferenceable(56) %_30001, i8* dereferenceable_or_null(8) %_80001, i8* dereferenceable_or_null(8) %_2)
  %_30007 = call i32 @"_SM34scala.collection.immutable.Vector3D14copy$default$2iEPT34scala.collection.immutable.Vector3"(i8* dereferenceable_or_null(72) %_1)
  %_30008 = call i32 @"_SM34scala.collection.immutable.Vector3D14copy$default$4iEPT34scala.collection.immutable.Vector3"(i8* dereferenceable_or_null(72) %_1)
  %_30009 = call i32 @"_SM34scala.collection.immutable.Vector3D14copy$default$8iEPT34scala.collection.immutable.Vector3"(i8* dereferenceable_or_null(72) %_1)
  %_80034 = bitcast i8* bitcast (i8* (i8*, i8*, i32, i8*, i32, i8*, i8*, i8*, i32)* @"_SM34scala.collection.immutable.Vector3D4copyLAL16java.lang.Object_iLALAL16java.lang.Object__iLALALAL16java.lang.Object___LALAL16java.lang.Object__LAL16java.lang.Object_iL34scala.collection.immutable.Vector3EPT34scala.collection.immutable.Vector3" to i8*) to i8* (i8*, i8*, i32, i8*, i32, i8*, i8*, i8*, i32)*
  %_30010 = call dereferenceable_or_null(72) i8* %_80034(i8* dereferenceable_or_null(72) %_1, i8* dereferenceable_or_null(8) %_30002, i32 %_30007, i8* dereferenceable_or_null(8) %_30003, i32 %_30008, i8* dereferenceable_or_null(8) %_30004, i8* dereferenceable_or_null(8) %_30005, i8* dereferenceable_or_null(8) %_30006, i32 %_30009)
  ret i8* %_30010
_80003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(72) i8* @"_SM34scala.collection.immutable.Vector3D4copyLAL16java.lang.Object_iLALAL16java.lang.Object__iLALALAL16java.lang.Object___LALAL16java.lang.Object__LAL16java.lang.Object_iL34scala.collection.immutable.Vector3EPT34scala.collection.immutable.Vector3"(i8* %_1, i8* %_2, i32 %_3, i8* %_4, i32 %_5, i8* %_6, i8* %_7, i8* %_8, i32 %_9) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_100000.0:
  %_170001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] }* @"_SM34scala.collection.immutable.Vector3G4type" to i8*), i64 72)
  %_170050 = bitcast i8* %_170001 to { i8*, i8* }*
  %_170051 = getelementptr { i8*, i8* }, { i8*, i8* }* %_170050, i32 0, i32 1
  %_170016 = bitcast i8** %_170051 to i8*
  %_170052 = bitcast i8* %_170016 to i8**
  store i8* %_2, i8** %_170052
  %_170053 = bitcast i8* %_170001 to { i8*, i8*, i32, i8* }*
  %_170054 = getelementptr { i8*, i8*, i32, i8* }, { i8*, i8*, i32, i8* }* %_170053, i32 0, i32 2
  %_170018 = bitcast i32* %_170054 to i8*
  %_170055 = bitcast i8* %_170018 to i32*
  store i32 %_9, i32* %_170055
  %_170056 = bitcast i8* %_170001 to { i8*, i8*, i32, i8* }*
  %_170057 = getelementptr { i8*, i8*, i32, i8* }, { i8*, i8*, i32, i8* }* %_170056, i32 0, i32 3
  %_170020 = bitcast i8** %_170057 to i8*
  %_170058 = bitcast i8* %_170020 to i8**
  store i8* %_8, i8** %_170058
  %_170059 = bitcast i8* %_170001 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_170060 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_170059, i32 0, i32 8
  %_170022 = bitcast i8** %_170060 to i8*
  %_170061 = bitcast i8* %_170022 to i8**
  store i8* %_7, i8** %_170061
  %_170062 = bitcast i8* %_170001 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_170063 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_170062, i32 0, i32 7
  %_170024 = bitcast i8** %_170063 to i8*
  %_170064 = bitcast i8* %_170024 to i8**
  store i8* %_6, i8** %_170064
  %_170065 = bitcast i8* %_170001 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_170066 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_170065, i32 0, i32 6
  %_170026 = bitcast i32* %_170066 to i8*
  %_170067 = bitcast i8* %_170026 to i32*
  store i32 %_5, i32* %_170067
  %_170068 = bitcast i8* %_170001 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_170069 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_170068, i32 0, i32 5
  %_170028 = bitcast i8** %_170069 to i8*
  %_170070 = bitcast i8* %_170028 to i8**
  store i8* %_4, i8** %_170070
  %_170071 = bitcast i8* %_170001 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_170072 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_170071, i32 0, i32 4
  %_170030 = bitcast i32* %_170072 to i8*
  %_170073 = bitcast i8* %_170030 to i32*
  store i32 %_3, i32* %_170073
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  call nonnull dereferenceable(8) i8* @"_SM44scala.collection.generic.DefaultSerializableD6$init$uEO"(i8* nonnull dereferenceable(72) %_170001)
  ret i8* %_170001
}

define dereferenceable_or_null(8) i8* @"_SM34scala.collection.immutable.Vector3D5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30003 = bitcast i8* bitcast (i32 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D10unboxToIntL16java.lang.ObjectiEO" to i8*) to i32 (i8*, i8*)*
  %_30001 = call i32 %_30003(i8* null, i8* dereferenceable_or_null(8) %_2)
  %_30002 = call dereferenceable_or_null(8) i8* @"_SM34scala.collection.immutable.Vector3D5applyiL16java.lang.ObjectEO"(i8* dereferenceable_or_null(72) %_1, i32 %_30001)
  ret i8* %_30002
}

define dereferenceable_or_null(8) i8* @"_SM34scala.collection.immutable.Vector3D5applyiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp sge i32 %_2, 0
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  %_320003 = icmp ne i8* %_1, null
  br i1 %_320003, label %_320001.0, label %_320002.0
_320001.0:
  %_320133 = bitcast i8* %_1 to { i8*, i8*, i32, i8* }*
  %_320134 = getelementptr { i8*, i8*, i32, i8* }, { i8*, i8*, i32, i8* }* %_320133, i32 0, i32 2
  %_320004 = bitcast i32* %_320134 to i8*
  %_320135 = bitcast i8* %_320004 to i32*
  %_60001 = load i32, i32* %_320135
  %_40002 = icmp slt i32 %_2, %_60001
  br label %_70000.0
_50000.0:
  br label %_70000.0
_70000.0:
  %_70001 = phi i1 [false, %_50000.0], [%_40002, %_320001.0]
  br i1 %_70001, label %_80000.0, label %_90000.0
_80000.0:
  %_320006 = icmp ne i8* %_1, null
  br i1 %_320006, label %_320005.0, label %_320002.0
_320005.0:
  %_320136 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_320137 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_320136, i32 0, i32 6
  %_320007 = bitcast i32* %_320137 to i8*
  %_320138 = bitcast i8* %_320007 to i32*
  %_100001 = load i32, i32* %_320138
  %_80003 = sub i32 %_2, %_100001
  %_80004 = icmp sge i32 %_80003, 0
  br i1 %_80004, label %_110000.0, label %_120000.0
_110000.0:
  %_320009 = icmp ne i8* %_1, null
  br i1 %_320009, label %_320008.0, label %_320002.0
_320008.0:
  %_320139 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_320140 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_320139, i32 0, i32 7
  %_320010 = bitcast i8** %_320140 to i8*
  %_320141 = bitcast i8* %_320010 to i8**
  %_130001 = load i8*, i8** %_320141, !dereferenceable_or_null !{i64 8}
  %_320012 = icmp ne i8* %_130001, null
  br i1 %_320012, label %_320011.0, label %_320002.0
_320011.0:
  %_320142 = bitcast i8* %_130001 to { i8*, i32 }*
  %_320143 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320142, i32 0, i32 1
  %_320013 = bitcast i32* %_320143 to i8*
  %_320144 = bitcast i8* %_320013 to i32*
  %_110005 = load i32, i32* %_320144
  %_320014 = and i32 10, 31
  %_110007 = lshr i32 %_80003, %_320014
  %_110008 = icmp slt i32 %_110007, %_110005
  br i1 %_110008, label %_140000.0, label %_150000.0
_140000.0:
  %_320016 = icmp ne i8* %_1, null
  br i1 %_320016, label %_320015.0, label %_320002.0
_320015.0:
  %_320145 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_320146 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_320145, i32 0, i32 7
  %_320017 = bitcast i8** %_320146 to i8*
  %_320147 = bitcast i8* %_320017 to i8**
  %_160001 = load i8*, i8** %_320147, !dereferenceable_or_null !{i64 8}
  %_320020 = icmp ne i8* %_160001, null
  br i1 %_320020, label %_320019.0, label %_320002.0
_320019.0:
  %_320148 = bitcast i8* %_160001 to { i8*, i32 }*
  %_320149 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320148, i32 0, i32 1
  %_320021 = bitcast i32* %_320149 to i8*
  %_320150 = bitcast i8* %_320021 to i32*
  %_320018 = load i32, i32* %_320150
  %_320024 = icmp sge i32 %_110007, 0
  %_320025 = icmp slt i32 %_110007, %_320018
  %_320026 = and i1 %_320024, %_320025
  br i1 %_320026, label %_320022.0, label %_320023.0
_320022.0:
  %_320151 = bitcast i8* %_160001 to { i8*, i32, i32, [0 x i8*] }*
  %_320152 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_320151, i32 0, i32 3, i32 %_110007
  %_320027 = bitcast i8** %_320152 to i8*
  %_320153 = bitcast i8* %_320027 to i8**
  %_140001 = load i8*, i8** %_320153, !dereferenceable_or_null !{i64 8}
  %_320028 = and i32 5, 31
  %_140002 = lshr i32 %_80003, %_320028
  %_140003 = and i32 %_140002, 31
  %_320031 = icmp ne i8* %_140001, null
  br i1 %_320031, label %_320030.0, label %_320002.0
_320030.0:
  %_320154 = bitcast i8* %_140001 to { i8*, i32 }*
  %_320155 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320154, i32 0, i32 1
  %_320032 = bitcast i32* %_320155 to i8*
  %_320156 = bitcast i8* %_320032 to i32*
  %_320029 = load i32, i32* %_320156
  %_320034 = icmp sge i32 %_140003, 0
  %_320035 = icmp slt i32 %_140003, %_320029
  %_320036 = and i1 %_320034, %_320035
  br i1 %_320036, label %_320033.0, label %_320023.0
_320033.0:
  %_320157 = bitcast i8* %_140001 to { i8*, i32, i32, [0 x i8*] }*
  %_320158 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_320157, i32 0, i32 3, i32 %_140003
  %_320037 = bitcast i8** %_320158 to i8*
  %_320159 = bitcast i8* %_320037 to i8**
  %_140004 = load i8*, i8** %_320159, !dereferenceable_or_null !{i64 8}
  %_140005 = and i32 %_80003, 31
  %_320040 = icmp ne i8* %_140004, null
  br i1 %_320040, label %_320039.0, label %_320002.0
_320039.0:
  %_320160 = bitcast i8* %_140004 to { i8*, i32 }*
  %_320161 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320160, i32 0, i32 1
  %_320041 = bitcast i32* %_320161 to i8*
  %_320162 = bitcast i8* %_320041 to i32*
  %_320038 = load i32, i32* %_320162
  %_320043 = icmp sge i32 %_140005, 0
  %_320044 = icmp slt i32 %_140005, %_320038
  %_320045 = and i1 %_320043, %_320044
  br i1 %_320045, label %_320042.0, label %_320023.0
_320042.0:
  %_320163 = bitcast i8* %_140004 to { i8*, i32, i32, [0 x i8*] }*
  %_320164 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_320163, i32 0, i32 3, i32 %_140005
  %_320046 = bitcast i8** %_320164 to i8*
  %_320165 = bitcast i8* %_320046 to i8**
  %_140006 = load i8*, i8** %_320165, !dereferenceable_or_null !{i64 8}
  br label %_170000.0
_150000.0:
  %_320048 = icmp ne i8* %_1, null
  br i1 %_320048, label %_320047.0, label %_320002.0
_320047.0:
  %_320166 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_320167 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_320166, i32 0, i32 8
  %_320049 = bitcast i8** %_320167 to i8*
  %_320168 = bitcast i8* %_320049 to i8**
  %_180001 = load i8*, i8** %_320168, !dereferenceable_or_null !{i64 8}
  %_320051 = icmp ne i8* %_180001, null
  br i1 %_320051, label %_320050.0, label %_320002.0
_320050.0:
  %_320169 = bitcast i8* %_180001 to { i8*, i32 }*
  %_320170 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320169, i32 0, i32 1
  %_320052 = bitcast i32* %_320170 to i8*
  %_320171 = bitcast i8* %_320052 to i32*
  %_150001 = load i32, i32* %_320171
  %_320053 = and i32 5, 31
  %_150003 = lshr i32 %_80003, %_320053
  %_150004 = and i32 %_150003, 31
  %_150005 = icmp slt i32 %_150004, %_150001
  br i1 %_150005, label %_190000.0, label %_200000.0
_190000.0:
  %_320055 = icmp ne i8* %_1, null
  br i1 %_320055, label %_320054.0, label %_320002.0
_320054.0:
  %_320172 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_320173 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_320172, i32 0, i32 8
  %_320056 = bitcast i8** %_320173 to i8*
  %_320174 = bitcast i8* %_320056 to i8**
  %_210001 = load i8*, i8** %_320174, !dereferenceable_or_null !{i64 8}
  %_320059 = icmp ne i8* %_210001, null
  br i1 %_320059, label %_320058.0, label %_320002.0
_320058.0:
  %_320175 = bitcast i8* %_210001 to { i8*, i32 }*
  %_320176 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320175, i32 0, i32 1
  %_320060 = bitcast i32* %_320176 to i8*
  %_320177 = bitcast i8* %_320060 to i32*
  %_320057 = load i32, i32* %_320177
  %_320062 = icmp sge i32 %_150004, 0
  %_320063 = icmp slt i32 %_150004, %_320057
  %_320064 = and i1 %_320062, %_320063
  br i1 %_320064, label %_320061.0, label %_320023.0
_320061.0:
  %_320178 = bitcast i8* %_210001 to { i8*, i32, i32, [0 x i8*] }*
  %_320179 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_320178, i32 0, i32 3, i32 %_150004
  %_320065 = bitcast i8** %_320179 to i8*
  %_320180 = bitcast i8* %_320065 to i8**
  %_190001 = load i8*, i8** %_320180, !dereferenceable_or_null !{i64 8}
  %_190002 = and i32 %_80003, 31
  %_320068 = icmp ne i8* %_190001, null
  br i1 %_320068, label %_320067.0, label %_320002.0
_320067.0:
  %_320181 = bitcast i8* %_190001 to { i8*, i32 }*
  %_320182 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320181, i32 0, i32 1
  %_320069 = bitcast i32* %_320182 to i8*
  %_320183 = bitcast i8* %_320069 to i32*
  %_320066 = load i32, i32* %_320183
  %_320071 = icmp sge i32 %_190002, 0
  %_320072 = icmp slt i32 %_190002, %_320066
  %_320073 = and i1 %_320071, %_320072
  br i1 %_320073, label %_320070.0, label %_320023.0
_320070.0:
  %_320184 = bitcast i8* %_190001 to { i8*, i32, i32, [0 x i8*] }*
  %_320185 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_320184, i32 0, i32 3, i32 %_190002
  %_320074 = bitcast i8** %_320185 to i8*
  %_320186 = bitcast i8* %_320074 to i8**
  %_190003 = load i8*, i8** %_320186, !dereferenceable_or_null !{i64 8}
  br label %_220000.0
_200000.0:
  %_320076 = icmp ne i8* %_1, null
  br i1 %_320076, label %_320075.0, label %_320002.0
_320075.0:
  %_320187 = bitcast i8* %_1 to { i8*, i8*, i32, i8* }*
  %_320188 = getelementptr { i8*, i8*, i32, i8* }, { i8*, i8*, i32, i8* }* %_320187, i32 0, i32 3
  %_320077 = bitcast i8** %_320188 to i8*
  %_320189 = bitcast i8* %_320077 to i8**
  %_230001 = load i8*, i8** %_320189, !dereferenceable_or_null !{i64 8}
  %_200001 = and i32 %_80003, 31
  %_320080 = icmp ne i8* %_230001, null
  br i1 %_320080, label %_320079.0, label %_320002.0
_320079.0:
  %_320190 = bitcast i8* %_230001 to { i8*, i32 }*
  %_320191 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320190, i32 0, i32 1
  %_320081 = bitcast i32* %_320191 to i8*
  %_320192 = bitcast i8* %_320081 to i32*
  %_320078 = load i32, i32* %_320192
  %_320083 = icmp sge i32 %_200001, 0
  %_320084 = icmp slt i32 %_200001, %_320078
  %_320085 = and i1 %_320083, %_320084
  br i1 %_320085, label %_320082.0, label %_320023.0
_320082.0:
  %_320193 = bitcast i8* %_230001 to { i8*, i32, i32, [0 x i8*] }*
  %_320194 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_320193, i32 0, i32 3, i32 %_200001
  %_320086 = bitcast i8** %_320194 to i8*
  %_320195 = bitcast i8* %_320086 to i8**
  %_200002 = load i8*, i8** %_320195, !dereferenceable_or_null !{i64 8}
  br label %_220000.0
_220000.0:
  %_220001 = phi i32 [%_200001, %_320082.0], [%_190002, %_320070.0]
  %_220002 = phi i8* [%_200002, %_320082.0], [%_190003, %_320070.0]
  br label %_170000.0
_170000.0:
  %_170001 = phi i32 [%_150003, %_220000.0], [%_140002, %_320042.0]
  %_170002 = phi i32 [%_150004, %_220000.0], [%_140003, %_320042.0]
  %_170003 = phi i32 [%_220001, %_220000.0], [%_140005, %_320042.0]
  %_170004 = phi i8* [%_220002, %_220000.0], [%_140006, %_320042.0]
  br label %_240000.0
_120000.0:
  %_320088 = icmp ne i8* %_1, null
  br i1 %_320088, label %_320087.0, label %_320002.0
_320087.0:
  %_320196 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_320197 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_320196, i32 0, i32 4
  %_320089 = bitcast i32* %_320197 to i8*
  %_320198 = bitcast i8* %_320089 to i32*
  %_250001 = load i32, i32* %_320198
  %_120002 = icmp sge i32 %_2, %_250001
  br i1 %_120002, label %_260000.0, label %_270000.0
_260000.0:
  %_320091 = icmp ne i8* %_1, null
  br i1 %_320091, label %_320090.0, label %_320002.0
_320090.0:
  %_320199 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_320200 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_320199, i32 0, i32 4
  %_320092 = bitcast i32* %_320200 to i8*
  %_320201 = bitcast i8* %_320092 to i32*
  %_280001 = load i32, i32* %_320201
  %_320094 = icmp ne i8* %_1, null
  br i1 %_320094, label %_320093.0, label %_320002.0
_320093.0:
  %_320202 = bitcast i8* %_1 to { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }*
  %_320203 = getelementptr { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }, { i8*, i8*, i32, i8*, i32, i8*, i32, i8*, i8* }* %_320202, i32 0, i32 5
  %_320095 = bitcast i8** %_320203 to i8*
  %_320204 = bitcast i8* %_320095 to i8**
  %_290001 = load i8*, i8** %_320204, !dereferenceable_or_null !{i64 8}
  %_260003 = sub i32 %_2, %_280001
  %_320096 = and i32 5, 31
  %_260004 = lshr i32 %_260003, %_320096
  %_320099 = icmp ne i8* %_290001, null
  br i1 %_320099, label %_320098.0, label %_320002.0
_320098.0:
  %_320205 = bitcast i8* %_290001 to { i8*, i32 }*
  %_320206 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320205, i32 0, i32 1
  %_320100 = bitcast i32* %_320206 to i8*
  %_320207 = bitcast i8* %_320100 to i32*
  %_320097 = load i32, i32* %_320207
  %_320102 = icmp sge i32 %_260004, 0
  %_320103 = icmp slt i32 %_260004, %_320097
  %_320104 = and i1 %_320102, %_320103
  br i1 %_320104, label %_320101.0, label %_320023.0
_320101.0:
  %_320208 = bitcast i8* %_290001 to { i8*, i32, i32, [0 x i8*] }*
  %_320209 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_320208, i32 0, i32 3, i32 %_260004
  %_320105 = bitcast i8** %_320209 to i8*
  %_320210 = bitcast i8* %_320105 to i8**
  %_260005 = load i8*, i8** %_320210, !dereferenceable_or_null !{i64 8}
  %_260007 = and i32 %_260003, 31
  %_320108 = icmp ne i8* %_260005, null
  br i1 %_320108, label %_320107.0, label %_320002.0
_320107.0:
  %_320211 = bitcast i8* %_260005 to { i8*, i32 }*
  %_320212 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320211, i32 0, i32 1
  %_320109 = bitcast i32* %_320212 to i8*
  %_320213 = bitcast i8* %_320109 to i32*
  %_320106 = load i32, i32* %_320213
  %_320111 = icmp sge i32 %_260007, 0
  %_320112 = icmp slt i32 %_260007, %_320106
  %_320113 = and i1 %_320111, %_320112
  br i1 %_320113, label %_320110.0, label %_320023.0
_320110.0:
  %_320214 = bitcast i8* %_260005 to { i8*, i32, i32, [0 x i8*] }*
  %_320215 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_320214, i32 0, i32 3, i32 %_260007
  %_320114 = bitcast i8** %_320215 to i8*
  %_320216 = bitcast i8* %_320114 to i8**
  %_260008 = load i8*, i8** %_320216, !dereferenceable_or_null !{i64 8}
  br label %_300000.0
_270000.0:
  %_320116 = icmp ne i8* %_1, null
  br i1 %_320116, label %_320115.0, label %_320002.0
_320115.0:
  %_320217 = bitcast i8* %_1 to { i8*, i8* }*
  %_320218 = getelementptr { i8*, i8* }, { i8*, i8* }* %_320217, i32 0, i32 1
  %_320117 = bitcast i8** %_320218 to i8*
  %_320219 = bitcast i8* %_320117 to i8**
  %_310001 = load i8*, i8** %_320219, !dereferenceable_or_null !{i64 8}
  %_320120 = icmp ne i8* %_310001, null
  br i1 %_320120, label %_320119.0, label %_320002.0
_320119.0:
  %_320220 = bitcast i8* %_310001 to { i8*, i32 }*
  %_320221 = getelementptr { i8*, i32 }, { i8*, i32 }* %_320220, i32 0, i32 1
  %_320121 = bitcast i32* %_320221 to i8*
  %_320222 = bitcast i8* %_320121 to i32*
  %_320118 = load i32, i32* %_320222
  %_320123 = icmp sge i32 %_2, 0
  %_320124 = icmp slt i32 %_2, %_320118
  %_320125 = and i1 %_320123, %_320124
  br i1 %_320125, label %_320122.0, label %_320023.0
_320122.0:
  %_320223 = bitcast i8* %_310001 to { i8*, i32, i32, [0 x i8*] }*
  %_320224 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_320223, i32 0, i32 3, i32 %_2
  %_320126 = bitcast i8** %_320224 to i8*
  %_320225 = bitcast i8* %_320126 to i8**
  %_270001 = load i8*, i8** %_320225, !dereferenceable_or_null !{i64 8}
  br label %_300000.0
_300000.0:
  %_300001 = phi i8* [%_270001, %_320122.0], [%_260008, %_320110.0]
  br label %_240000.0
_240000.0:
  %_240001 = phi i8* [%_300001, %_300000.0], [%_170004, %_170000.0]
  br label %_320000.0
_320000.0:
  ret i8* %_240001
_90000.0:
  %_320226 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM33scala.collection.immutable.VectorD4ioobiL35java.lang.IndexOutOfBoundsExceptionEO" to i8*) to i8* (i8*, i32)*
  %_90001 = call dereferenceable_or_null(40) i8* %_320226(i8* dereferenceable_or_null(72) %_1, i32 %_2)
  %_320128 = icmp ne i8* %_90001, null
  br i1 %_320128, label %_320127.0, label %_320002.0
_320127.0:
  call i8* @"scalanative_throw"(i8* dereferenceable_or_null(40) %_90001)
  unreachable
_320002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_320023.0:
  %_320131 = phi i32 [%_2, %_320119.0], [%_260004, %_320098.0], [%_260007, %_320107.0], [%_200001, %_320079.0], [%_150004, %_320058.0], [%_190002, %_320067.0], [%_110007, %_320019.0], [%_140003, %_320030.0], [%_140005, %_320039.0]
  %_320227 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_320227(i8* null, i32 %_320131)
  unreachable
}

define i1 @"_SM35java.util.Collections$WrappedEqualsD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_50001 = bitcast i8* %_1 to i8*
  %_50005 = icmp ne i8* %_50001, null
  br i1 %_50005, label %_50003.0, label %_50004.0
_50003.0:
  %_50012 = bitcast i8* %_50001 to { i8*, i8*, i1 }*
  %_50013 = getelementptr { i8*, i8*, i1 }, { i8*, i8*, i1 }* %_50012, i32 0, i32 1
  %_50006 = bitcast i8** %_50013 to i8*
  %_50014 = bitcast i8* %_50006 to i8**
  %_50002 = load i8*, i8** %_50014, !dereferenceable_or_null !{i64 8}
  %_50008 = icmp ne i8* %_50002, null
  br i1 %_50008, label %_50007.0, label %_50004.0
_50007.0:
  %_50015 = bitcast i8* %_50002 to i8**
  %_50009 = load i8*, i8** %_50015
  %_50016 = bitcast i8* %_50009 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_50017 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_50016, i32 0, i32 4, i32 3
  %_50010 = bitcast i8** %_50017 to i8*
  %_50018 = bitcast i8* %_50010 to i8**
  %_30003 = load i8*, i8** %_50018
  %_50019 = bitcast i8* %_30003 to i1 (i8*, i8*)*
  %_30004 = call i1 %_50019(i8* dereferenceable_or_null(8) %_50002, i8* dereferenceable_or_null(8) %_2)
  ret i1 %_30004
_50004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM35java.util.Collections$WrappedEqualsD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_40001 = bitcast i8* %_1 to i8*
  %_40005 = icmp ne i8* %_40001, null
  br i1 %_40005, label %_40003.0, label %_40004.0
_40003.0:
  %_40012 = bitcast i8* %_40001 to { i8*, i8*, i1 }*
  %_40013 = getelementptr { i8*, i8*, i1 }, { i8*, i8*, i1 }* %_40012, i32 0, i32 1
  %_40006 = bitcast i8** %_40013 to i8*
  %_40014 = bitcast i8* %_40006 to i8**
  %_40002 = load i8*, i8** %_40014, !dereferenceable_or_null !{i64 8}
  %_40008 = icmp ne i8* %_40002, null
  br i1 %_40008, label %_40007.0, label %_40004.0
_40007.0:
  %_40015 = bitcast i8* %_40002 to i8**
  %_40009 = load i8*, i8** %_40015
  %_40016 = bitcast i8* %_40009 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_40017 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_40016, i32 0, i32 4, i32 2
  %_40010 = bitcast i8** %_40017 to i8*
  %_40018 = bitcast i8* %_40010 to i8**
  %_20003 = load i8*, i8** %_40018
  %_40019 = bitcast i8* %_20003 to i32 (i8*)*
  %_20004 = call i32 %_40019(i8* dereferenceable_or_null(8) %_40002)
  ret i32 %_20004
_40004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM35scala.collection.ClassTagSeqFactoryD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(16) i8* @"_SM35scala.scalanative.unsafe.CFuncPtr3$D10fromRawPtrR_L34scala.scalanative.unsafe.CFuncPtr3EO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM34scala.scalanative.unsafe.CFuncPtr3G4type" to i8*), i64 16)
  %_30006 = bitcast i8* %_30002 to { i8*, i8* }*
  %_30007 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30006, i32 0, i32 1
  %_30005 = bitcast i8** %_30007 to i8*
  %_30008 = bitcast i8* %_30005 to i8**
  store i8* %_2, i8** %_30008
  ret i8* %_30002
}

define nonnull dereferenceable(8) i8* @"_SM35scala.scalanative.unsafe.CFuncPtr3$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.immutable.ArraySeq$D10newBuilderL16java.lang.ObjectL32scala.collection.mutable.BuilderEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30006 = icmp eq i8* %_2, null
  br i1 %_30006, label %_30004.0, label %_30003.0
_30003.0:
  %_30016 = bitcast i8* %_2 to i8**
  %_30007 = load i8*, i8** %_30016
  %_30017 = bitcast i8* %_30007 to { i8*, i32, i32, i8* }*
  %_30018 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30017, i32 0, i32 1
  %_30008 = bitcast i32* %_30018 to i8*
  %_30019 = bitcast i8* %_30008 to i32*
  %_30009 = load i32, i32* %_30019
  %_30020 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_30021 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_30020, i32 0, i32 %_30009, i32 33
  %_30010 = bitcast i1* %_30021 to i8*
  %_30022 = bitcast i8* %_30010 to i1*
  %_30011 = load i1, i1* %_30022
  br i1 %_30011, label %_30004.0, label %_30005.0
_30004.0:
  %_30001 = bitcast i8* %_2 to i8*
  %_30002 = call dereferenceable_or_null(8) i8* @"_SM36scala.collection.immutable.ArraySeq$D10newBuilderL22scala.reflect.ClassTagL32scala.collection.mutable.BuilderEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_30001)
  ret i8* %_30002
_30005.0:
  %_30012 = phi i8* [%_2, %_30003.0]
  %_30013 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM22scala.reflect.ClassTagG4type" to i8*), %_30003.0]
  %_30023 = bitcast i8* %_30012 to i8**
  %_30014 = load i8*, i8** %_30023
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_30014, i8* %_30013)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.immutable.ArraySeq$D10newBuilderL22scala.reflect.ClassTagL32scala.collection.mutable.BuilderEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM37scala.collection.mutable.ArrayBuffer$G4load"()
  %_30023 = bitcast i8* bitcast (i8* (i8*)* @"_SM37scala.collection.mutable.ArrayBuffer$D10newBuilderL32scala.collection.mutable.BuilderEO" to i8*) to i8* (i8*)*
  %_30002 = call dereferenceable_or_null(8) i8* %_30023(i8* nonnull dereferenceable(8) %_30001)
  %_30012 = icmp ne i8* %_30002, null
  br i1 %_30012, label %_30010.0, label %_30011.0
_30010.0:
  %_30024 = bitcast i8* %_30002 to i8**
  %_30013 = load i8*, i8** %_30024
  %_30025 = bitcast i8* %_30013 to { i8*, i32, i32, i8* }*
  %_30026 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30025, i32 0, i32 2
  %_30014 = bitcast i32* %_30026 to i8*
  %_30027 = bitcast i8* %_30014 to i32*
  %_30015 = load i32, i32* %_30027
  %_30028 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30029 = getelementptr i8*, i8** %_30028, i32 3848
  %_30016 = bitcast i8** %_30029 to i8*
  %_30030 = bitcast i8* %_30016 to i8**
  %_30031 = getelementptr i8*, i8** %_30030, i32 %_30015
  %_30017 = bitcast i8** %_30031 to i8*
  %_30032 = bitcast i8* %_30017 to i8**
  %_30005 = load i8*, i8** %_30032
  %_30006 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM46scala.collection.immutable.ArraySeq$$$Lambda$1G4type" to i8*), i64 24)
  %_30033 = bitcast i8* %_30006 to { i8*, i8*, i8* }*
  %_30034 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_30033, i32 0, i32 2
  %_30019 = bitcast i8** %_30034 to i8*
  %_30035 = bitcast i8* %_30019 to i8**
  store i8* %_2, i8** %_30035
  %_30036 = bitcast i8* %_30006 to { i8*, i8*, i8* }*
  %_30037 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_30036, i32 0, i32 1
  %_30021 = bitcast i8** %_30037 to i8*
  %_30038 = bitcast i8* %_30021 to i8**
  store i8* %_1, i8** %_30038
  %_30039 = bitcast i8* %_30005 to i8* (i8*, i8*)*
  %_30009 = call dereferenceable_or_null(8) i8* %_30039(i8* dereferenceable_or_null(8) %_30002, i8* nonnull dereferenceable(24) %_30006)
  ret i8* %_30009
_30011.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.immutable.ArraySeq$D15unsafeWrapArrayL16java.lang.ObjectL35scala.collection.immutable.ArraySeqEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_40002 = icmp eq i8* %_2, null
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  br label %_70000.0
_60000.0:
  br label %_80000.0
_80000.0:
  %_1080004 = icmp eq i8* %_2, null
  br i1 %_1080004, label %_1080001.0, label %_1080002.0
_1080001.0:
  br label %_1080003.0
_1080002.0:
  %_1080333 = bitcast i8* %_2 to i8**
  %_1080005 = load i8*, i8** %_1080333
  %_1080006 = icmp eq i8* %_1080005, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.ObjectArrayG4type" to i8*)
  br label %_1080003.0
_1080003.0:
  %_80002 = phi i1 [%_1080006, %_1080002.0], [false, %_1080001.0]
  br i1 %_80002, label %_90000.0, label %_100000.0
_90000.0:
  %_1080010 = icmp eq i8* %_2, null
  br i1 %_1080010, label %_1080008.0, label %_1080007.0
_1080007.0:
  %_1080334 = bitcast i8* %_2 to i8**
  %_1080011 = load i8*, i8** %_1080334
  %_1080012 = icmp eq i8* %_1080011, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.ObjectArrayG4type" to i8*)
  br i1 %_1080012, label %_1080008.0, label %_1080009.0
_1080008.0:
  %_90001 = bitcast i8* %_2 to i8*
  %_150001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM41scala.collection.immutable.ArraySeq$ofRefG4type" to i8*), i64 16)
  %_1080335 = bitcast i8* %_150001 to { i8*, i8* }*
  %_1080336 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1080335, i32 0, i32 1
  %_1080014 = bitcast i8** %_1080336 to i8*
  %_1080337 = bitcast i8* %_1080014 to i8**
  store i8* %_90001, i8** %_1080337
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_150001)
  br label %_70000.0
_100000.0:
  br label %_170000.0
_170000.0:
  %_1080037 = icmp eq i8* %_2, null
  br i1 %_1080037, label %_1080034.0, label %_1080035.0
_1080034.0:
  br label %_1080036.0
_1080035.0:
  %_1080338 = bitcast i8* %_2 to i8**
  %_1080038 = load i8*, i8** %_1080338
  %_1080039 = icmp eq i8* %_1080038, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM34scala.scalanative.runtime.IntArrayG4type" to i8*)
  br label %_1080036.0
_1080036.0:
  %_170002 = phi i1 [%_1080039, %_1080035.0], [false, %_1080034.0]
  br i1 %_170002, label %_180000.0, label %_190000.0
_180000.0:
  %_1080042 = icmp eq i8* %_2, null
  br i1 %_1080042, label %_1080041.0, label %_1080040.0
_1080040.0:
  %_1080339 = bitcast i8* %_2 to i8**
  %_1080043 = load i8*, i8** %_1080339
  %_1080044 = icmp eq i8* %_1080043, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM34scala.scalanative.runtime.IntArrayG4type" to i8*)
  br i1 %_1080044, label %_1080041.0, label %_1080009.0
_1080041.0:
  %_180001 = bitcast i8* %_2 to i8*
  %_240001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM41scala.collection.immutable.ArraySeq$ofIntG4type" to i8*), i64 16)
  %_1080340 = bitcast i8* %_240001 to { i8*, i8* }*
  %_1080341 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1080340, i32 0, i32 1
  %_1080046 = bitcast i8** %_1080341 to i8*
  %_1080342 = bitcast i8* %_1080046 to i8**
  store i8* %_180001, i8** %_1080342
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_240001)
  br label %_70000.0
_190000.0:
  br label %_260000.0
_260000.0:
  %_1080069 = icmp eq i8* %_2, null
  br i1 %_1080069, label %_1080066.0, label %_1080067.0
_1080066.0:
  br label %_1080068.0
_1080067.0:
  %_1080343 = bitcast i8* %_2 to i8**
  %_1080070 = load i8*, i8** %_1080343
  %_1080071 = icmp eq i8* %_1080070, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.DoubleArrayG4type" to i8*)
  br label %_1080068.0
_1080068.0:
  %_260002 = phi i1 [%_1080071, %_1080067.0], [false, %_1080066.0]
  br i1 %_260002, label %_270000.0, label %_280000.0
_270000.0:
  %_1080074 = icmp eq i8* %_2, null
  br i1 %_1080074, label %_1080073.0, label %_1080072.0
_1080072.0:
  %_1080344 = bitcast i8* %_2 to i8**
  %_1080075 = load i8*, i8** %_1080344
  %_1080076 = icmp eq i8* %_1080075, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.DoubleArrayG4type" to i8*)
  br i1 %_1080076, label %_1080073.0, label %_1080009.0
_1080073.0:
  %_270001 = bitcast i8* %_2 to i8*
  %_330001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM44scala.collection.immutable.ArraySeq$ofDoubleG4type" to i8*), i64 16)
  %_1080345 = bitcast i8* %_330001 to { i8*, i8* }*
  %_1080346 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1080345, i32 0, i32 1
  %_1080078 = bitcast i8** %_1080346 to i8*
  %_1080347 = bitcast i8* %_1080078 to i8**
  store i8* %_270001, i8** %_1080347
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_330001)
  br label %_70000.0
_280000.0:
  br label %_350000.0
_350000.0:
  %_1080101 = icmp eq i8* %_2, null
  br i1 %_1080101, label %_1080098.0, label %_1080099.0
_1080098.0:
  br label %_1080100.0
_1080099.0:
  %_1080348 = bitcast i8* %_2 to i8**
  %_1080102 = load i8*, i8** %_1080348
  %_1080103 = icmp eq i8* %_1080102, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.LongArrayG4type" to i8*)
  br label %_1080100.0
_1080100.0:
  %_350002 = phi i1 [%_1080103, %_1080099.0], [false, %_1080098.0]
  br i1 %_350002, label %_360000.0, label %_370000.0
_360000.0:
  %_1080106 = icmp eq i8* %_2, null
  br i1 %_1080106, label %_1080105.0, label %_1080104.0
_1080104.0:
  %_1080349 = bitcast i8* %_2 to i8**
  %_1080107 = load i8*, i8** %_1080349
  %_1080108 = icmp eq i8* %_1080107, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.LongArrayG4type" to i8*)
  br i1 %_1080108, label %_1080105.0, label %_1080009.0
_1080105.0:
  %_360001 = bitcast i8* %_2 to i8*
  %_420001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM42scala.collection.immutable.ArraySeq$ofLongG4type" to i8*), i64 16)
  %_1080350 = bitcast i8* %_420001 to { i8*, i8* }*
  %_1080351 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1080350, i32 0, i32 1
  %_1080110 = bitcast i8** %_1080351 to i8*
  %_1080352 = bitcast i8* %_1080110 to i8**
  store i8* %_360001, i8** %_1080352
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_420001)
  br label %_70000.0
_370000.0:
  br label %_440000.0
_440000.0:
  %_1080133 = icmp eq i8* %_2, null
  br i1 %_1080133, label %_1080130.0, label %_1080131.0
_1080130.0:
  br label %_1080132.0
_1080131.0:
  %_1080353 = bitcast i8* %_2 to i8**
  %_1080134 = load i8*, i8** %_1080353
  %_1080135 = icmp eq i8* %_1080134, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM36scala.scalanative.runtime.FloatArrayG4type" to i8*)
  br label %_1080132.0
_1080132.0:
  %_440002 = phi i1 [%_1080135, %_1080131.0], [false, %_1080130.0]
  br i1 %_440002, label %_450000.0, label %_460000.0
_450000.0:
  %_1080138 = icmp eq i8* %_2, null
  br i1 %_1080138, label %_1080137.0, label %_1080136.0
_1080136.0:
  %_1080354 = bitcast i8* %_2 to i8**
  %_1080139 = load i8*, i8** %_1080354
  %_1080140 = icmp eq i8* %_1080139, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM36scala.scalanative.runtime.FloatArrayG4type" to i8*)
  br i1 %_1080140, label %_1080137.0, label %_1080009.0
_1080137.0:
  %_450001 = bitcast i8* %_2 to i8*
  %_510001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM43scala.collection.immutable.ArraySeq$ofFloatG4type" to i8*), i64 16)
  %_1080355 = bitcast i8* %_510001 to { i8*, i8* }*
  %_1080356 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1080355, i32 0, i32 1
  %_1080142 = bitcast i8** %_1080356 to i8*
  %_1080357 = bitcast i8* %_1080142 to i8**
  store i8* %_450001, i8** %_1080357
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_510001)
  br label %_70000.0
_460000.0:
  br label %_530000.0
_530000.0:
  %_1080165 = icmp eq i8* %_2, null
  br i1 %_1080165, label %_1080162.0, label %_1080163.0
_1080162.0:
  br label %_1080164.0
_1080163.0:
  %_1080358 = bitcast i8* %_2 to i8**
  %_1080166 = load i8*, i8** %_1080358
  %_1080167 = icmp eq i8* %_1080166, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*)
  br label %_1080164.0
_1080164.0:
  %_530002 = phi i1 [%_1080167, %_1080163.0], [false, %_1080162.0]
  br i1 %_530002, label %_540000.0, label %_550000.0
_540000.0:
  %_1080170 = icmp eq i8* %_2, null
  br i1 %_1080170, label %_1080169.0, label %_1080168.0
_1080168.0:
  %_1080359 = bitcast i8* %_2 to i8**
  %_1080171 = load i8*, i8** %_1080359
  %_1080172 = icmp eq i8* %_1080171, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*)
  br i1 %_1080172, label %_1080169.0, label %_1080009.0
_1080169.0:
  %_540001 = bitcast i8* %_2 to i8*
  %_600001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM42scala.collection.immutable.ArraySeq$ofCharG4type" to i8*), i64 16)
  %_1080360 = bitcast i8* %_600001 to { i8*, i8* }*
  %_1080361 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1080360, i32 0, i32 1
  %_1080174 = bitcast i8** %_1080361 to i8*
  %_1080362 = bitcast i8* %_1080174 to i8**
  store i8* %_540001, i8** %_1080362
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_600001)
  br label %_70000.0
_550000.0:
  br label %_620000.0
_620000.0:
  %_1080197 = icmp eq i8* %_2, null
  br i1 %_1080197, label %_1080194.0, label %_1080195.0
_1080194.0:
  br label %_1080196.0
_1080195.0:
  %_1080363 = bitcast i8* %_2 to i8**
  %_1080198 = load i8*, i8** %_1080363
  %_1080199 = icmp eq i8* %_1080198, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.ByteArrayG4type" to i8*)
  br label %_1080196.0
_1080196.0:
  %_620002 = phi i1 [%_1080199, %_1080195.0], [false, %_1080194.0]
  br i1 %_620002, label %_630000.0, label %_640000.0
_630000.0:
  %_1080202 = icmp eq i8* %_2, null
  br i1 %_1080202, label %_1080201.0, label %_1080200.0
_1080200.0:
  %_1080364 = bitcast i8* %_2 to i8**
  %_1080203 = load i8*, i8** %_1080364
  %_1080204 = icmp eq i8* %_1080203, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.ByteArrayG4type" to i8*)
  br i1 %_1080204, label %_1080201.0, label %_1080009.0
_1080201.0:
  %_630001 = bitcast i8* %_2 to i8*
  %_690001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM42scala.collection.immutable.ArraySeq$ofByteG4type" to i8*), i64 16)
  %_1080365 = bitcast i8* %_690001 to { i8*, i8* }*
  %_1080366 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1080365, i32 0, i32 1
  %_1080206 = bitcast i8** %_1080366 to i8*
  %_1080367 = bitcast i8* %_1080206 to i8**
  store i8* %_630001, i8** %_1080367
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_690001)
  br label %_70000.0
_640000.0:
  br label %_710000.0
_710000.0:
  %_1080229 = icmp eq i8* %_2, null
  br i1 %_1080229, label %_1080226.0, label %_1080227.0
_1080226.0:
  br label %_1080228.0
_1080227.0:
  %_1080368 = bitcast i8* %_2 to i8**
  %_1080230 = load i8*, i8** %_1080368
  %_1080231 = icmp eq i8* %_1080230, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM36scala.scalanative.runtime.ShortArrayG4type" to i8*)
  br label %_1080228.0
_1080228.0:
  %_710002 = phi i1 [%_1080231, %_1080227.0], [false, %_1080226.0]
  br i1 %_710002, label %_720000.0, label %_730000.0
_720000.0:
  %_1080234 = icmp eq i8* %_2, null
  br i1 %_1080234, label %_1080233.0, label %_1080232.0
_1080232.0:
  %_1080369 = bitcast i8* %_2 to i8**
  %_1080235 = load i8*, i8** %_1080369
  %_1080236 = icmp eq i8* %_1080235, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM36scala.scalanative.runtime.ShortArrayG4type" to i8*)
  br i1 %_1080236, label %_1080233.0, label %_1080009.0
_1080233.0:
  %_720001 = bitcast i8* %_2 to i8*
  %_780001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM43scala.collection.immutable.ArraySeq$ofShortG4type" to i8*), i64 16)
  %_1080370 = bitcast i8* %_780001 to { i8*, i8* }*
  %_1080371 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1080370, i32 0, i32 1
  %_1080238 = bitcast i8** %_1080371 to i8*
  %_1080372 = bitcast i8* %_1080238 to i8**
  store i8* %_720001, i8** %_1080372
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_780001)
  br label %_70000.0
_730000.0:
  br label %_800000.0
_800000.0:
  %_1080261 = icmp eq i8* %_2, null
  br i1 %_1080261, label %_1080258.0, label %_1080259.0
_1080258.0:
  br label %_1080260.0
_1080259.0:
  %_1080373 = bitcast i8* %_2 to i8**
  %_1080262 = load i8*, i8** %_1080373
  %_1080263 = icmp eq i8* %_1080262, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM38scala.scalanative.runtime.BooleanArrayG4type" to i8*)
  br label %_1080260.0
_1080260.0:
  %_800002 = phi i1 [%_1080263, %_1080259.0], [false, %_1080258.0]
  br i1 %_800002, label %_810000.0, label %_820000.0
_810000.0:
  %_1080266 = icmp eq i8* %_2, null
  br i1 %_1080266, label %_1080265.0, label %_1080264.0
_1080264.0:
  %_1080374 = bitcast i8* %_2 to i8**
  %_1080267 = load i8*, i8** %_1080374
  %_1080268 = icmp eq i8* %_1080267, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM38scala.scalanative.runtime.BooleanArrayG4type" to i8*)
  br i1 %_1080268, label %_1080265.0, label %_1080009.0
_1080265.0:
  %_810001 = bitcast i8* %_2 to i8*
  %_870001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM45scala.collection.immutable.ArraySeq$ofBooleanG4type" to i8*), i64 16)
  %_1080375 = bitcast i8* %_870001 to { i8*, i8* }*
  %_1080376 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1080375, i32 0, i32 1
  %_1080270 = bitcast i8** %_1080376 to i8*
  %_1080377 = bitcast i8* %_1080270 to i8**
  store i8* %_810001, i8** %_1080377
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_870001)
  br label %_70000.0
_820000.0:
  br label %_890000.0
_890000.0:
  %_1080293 = icmp eq i8* %_2, null
  br i1 %_1080293, label %_1080290.0, label %_1080291.0
_1080290.0:
  br label %_1080292.0
_1080291.0:
  %_1080378 = bitcast i8* %_2 to i8**
  %_1080294 = load i8*, i8** %_1080378
  %_1080295 = icmp eq i8* %_1080294, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.ObjectArrayG4type" to i8*)
  br label %_1080292.0
_1080292.0:
  %_890002 = phi i1 [%_1080295, %_1080291.0], [false, %_1080290.0]
  br i1 %_890002, label %_900000.0, label %_910000.0
_900000.0:
  %_1080298 = icmp eq i8* %_2, null
  br i1 %_1080298, label %_1080297.0, label %_1080296.0
_1080296.0:
  %_1080379 = bitcast i8* %_2 to i8**
  %_1080299 = load i8*, i8** %_1080379
  %_1080300 = icmp eq i8* %_1080299, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.ObjectArrayG4type" to i8*)
  br i1 %_1080300, label %_1080297.0, label %_1080009.0
_1080297.0:
  %_900001 = bitcast i8* %_2 to i8*
  %_960001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM42scala.collection.immutable.ArraySeq$ofUnitG4type" to i8*), i64 16)
  %_1080380 = bitcast i8* %_960001 to { i8*, i8* }*
  %_1080381 = getelementptr { i8*, i8* }, { i8*, i8* }* %_1080380, i32 0, i32 1
  %_1080302 = bitcast i8** %_1080381 to i8*
  %_1080382 = bitcast i8* %_1080302 to i8**
  store i8* %_900001, i8** %_1080382
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_960001)
  br label %_70000.0
_910000.0:
  br label %_980000.0
_70000.0:
  %_70001 = phi i8* [%_960001, %_1080297.0], [%_870001, %_1080265.0], [%_780001, %_1080233.0], [%_690001, %_1080201.0], [%_600001, %_1080169.0], [%_510001, %_1080137.0], [%_420001, %_1080105.0], [%_330001, %_1080073.0], [%_240001, %_1080041.0], [%_150001, %_1080008.0], [null, %_50000.0]
  ret i8* %_70001
_980000.0:
  br label %_1070000.0
_1070000.0:
  %_1070001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM16scala.MatchErrorG4type" to i8*), i64 64)
  %_1080383 = bitcast i8* %_1070001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_1080384 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_1080383, i32 0, i32 5
  %_1080323 = bitcast i1* %_1080384 to i8*
  %_1080385 = bitcast i8* %_1080323 to i1*
  store i1 true, i1* %_1080385
  %_1080386 = bitcast i8* %_1070001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_1080387 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_1080386, i32 0, i32 4
  %_1080325 = bitcast i1* %_1080387 to i8*
  %_1080388 = bitcast i8* %_1080325 to i1*
  store i1 true, i1* %_1080388
  %_1080389 = bitcast i8* %_1070001 to { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }*
  %_1080390 = getelementptr { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }, { i8*, i8*, i8*, i8*, i1, i1, i8*, i8*, i1 }* %_1080389, i32 0, i32 6
  %_1080327 = bitcast i8** %_1080390 to i8*
  %_1080391 = bitcast i8* %_1080327 to i8**
  store i8* %_2, i8** %_1080391
  %_1070005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(64) %_1070001)
  br label %_1080000.0
_1080000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(64) %_1070001)
  unreachable
_1080009.0:
  %_1080329 = phi i8* [%_2, %_1080296.0], [%_2, %_1080264.0], [%_2, %_1080232.0], [%_2, %_1080200.0], [%_2, %_1080168.0], [%_2, %_1080136.0], [%_2, %_1080104.0], [%_2, %_1080072.0], [%_2, %_1080040.0], [%_2, %_1080007.0]
  %_1080330 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.ObjectArrayG4type" to i8*), %_1080296.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM38scala.scalanative.runtime.BooleanArrayG4type" to i8*), %_1080264.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM36scala.scalanative.runtime.ShortArrayG4type" to i8*), %_1080232.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.ByteArrayG4type" to i8*), %_1080200.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), %_1080168.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM36scala.scalanative.runtime.FloatArrayG4type" to i8*), %_1080136.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.LongArrayG4type" to i8*), %_1080104.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.DoubleArrayG4type" to i8*), %_1080072.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM34scala.scalanative.runtime.IntArrayG4type" to i8*), %_1080040.0], [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.ObjectArrayG4type" to i8*), %_1080007.0]
  %_1080392 = bitcast i8* %_1080329 to i8**
  %_1080331 = load i8*, i8** %_1080392
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_1080331, i8* %_1080330)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM36scala.collection.immutable.ArraySeq$D20emptyImpl$lzycomputeL41scala.collection.immutable.ArraySeq$ofRefEPT36scala.collection.immutable.ArraySeq$"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_160004 = icmp ne i8* %_1, null
  br i1 %_160004, label %_160002.0, label %_160003.0
_160002.0:
  %_160040 = bitcast i8* %_1 to { i8*, i8*, i1, i8* }*
  %_160041 = getelementptr { i8*, i8*, i1, i8* }, { i8*, i8*, i1, i8* }* %_160040, i32 0, i32 2
  %_160005 = bitcast i1* %_160041 to i8*
  %_160042 = bitcast i8* %_160005 to i1*
  %_20002 = load i1, i1* %_160042
  %_20004 = xor i1 %_20002, true
  br i1 %_20004, label %_80000.0, label %_90000.0
_80000.0:
  %_140001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM41scala.collection.immutable.ArraySeq$ofRefG4type" to i8*), i64 16)
  %_140002 = call dereferenceable_or_null(8) i8* @"_SM38scala.scalanative.runtime.ObjectArray$D5allociL37scala.scalanative.runtime.ObjectArrayEO"(i8* bitcast ({ i8* }* @"_SM38scala.scalanative.runtime.ObjectArray$G8instance" to i8*), i32 0)
  %_160043 = bitcast i8* %_140001 to { i8*, i8* }*
  %_160044 = getelementptr { i8*, i8* }, { i8*, i8* }* %_160043, i32 0, i32 1
  %_160008 = bitcast i8** %_160044 to i8*
  %_160045 = bitcast i8* %_160008 to i8**
  store i8* %_140002, i8** %_160045
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.immutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.immutable.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  call nonnull dereferenceable(8) i8* @"_SM48scala.collection.EvidenceIterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_140001)
  %_160030 = icmp ne i8* %_1, null
  br i1 %_160030, label %_160029.0, label %_160003.0
_160029.0:
  %_160046 = bitcast i8* %_1 to { i8*, i8*, i1, i8* }*
  %_160047 = getelementptr { i8*, i8*, i1, i8* }, { i8*, i8*, i1, i8* }* %_160046, i32 0, i32 1
  %_160031 = bitcast i8** %_160047 to i8*
  %_160048 = bitcast i8* %_160031 to i8**
  store i8* %_140001, i8** %_160048
  %_160034 = icmp ne i8* %_1, null
  br i1 %_160034, label %_160033.0, label %_160003.0
_160033.0:
  %_160049 = bitcast i8* %_1 to { i8*, i8*, i1, i8* }*
  %_160050 = getelementptr { i8*, i8*, i1, i8* }, { i8*, i8*, i1, i8* }* %_160049, i32 0, i32 2
  %_160035 = bitcast i1* %_160050 to i8*
  %_160051 = bitcast i8* %_160035 to i1*
  store i1 true, i1* %_160051
  br label %_160000.0
_90000.0:
  br label %_160000.0
_160000.0:
  %_160037 = icmp ne i8* %_1, null
  br i1 %_160037, label %_160036.0, label %_160003.0
_160036.0:
  %_160052 = bitcast i8* %_1 to { i8*, i8*, i1, i8* }*
  %_160053 = getelementptr { i8*, i8*, i1, i8* }, { i8*, i8*, i1, i8* }* %_160052, i32 0, i32 1
  %_160038 = bitcast i8** %_160053 to i8*
  %_160054 = bitcast i8* %_160038 to i8**
  %_160001 = load i8*, i8** %_160054, !dereferenceable_or_null !{i64 16}
  ret i8* %_160001
_160003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.immutable.ArraySeq$D21$anonfun$newBuilder$1L22scala.reflect.ClassTagL36scala.collection.mutable.ArrayBufferL35scala.collection.immutable.ArraySeqEPT36scala.collection.immutable.ArraySeq$"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call dereferenceable_or_null(32) i8* @"_SM36scala.collection.immutable.ArraySeq$G4load"()
  %_40004 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM33scala.collection.AbstractIterableD7toArrayL22scala.reflect.ClassTagL16java.lang.ObjectEO" to i8*) to i8* (i8*, i8*)*
  %_40002 = call dereferenceable_or_null(8) i8* %_40004(i8* dereferenceable_or_null(32) %_3, i8* dereferenceable_or_null(8) %_2)
  %_40003 = call dereferenceable_or_null(8) i8* @"_SM36scala.collection.immutable.ArraySeq$D15unsafeWrapArrayL16java.lang.ObjectL35scala.collection.immutable.ArraySeqEO"(i8* nonnull dereferenceable(32) %_40001, i8* dereferenceable_or_null(8) %_40002)
  ret i8* %_40003
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.immutable.ArraySeq$D4fromL29scala.collection.IterableOnceL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2, i8* %_3) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40006 = icmp eq i8* %_3, null
  br i1 %_40006, label %_40004.0, label %_40003.0
_40003.0:
  %_40016 = bitcast i8* %_3 to i8**
  %_40007 = load i8*, i8** %_40016
  %_40017 = bitcast i8* %_40007 to { i8*, i32, i32, i8* }*
  %_40018 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40017, i32 0, i32 1
  %_40008 = bitcast i32* %_40018 to i8*
  %_40019 = bitcast i8* %_40008 to i32*
  %_40009 = load i32, i32* %_40019
  %_40020 = bitcast i8* bitcast ([656 x [122 x i1]]* @"_ST17__class_has_trait" to i8*) to [656 x [122 x i1]]*
  %_40021 = getelementptr [656 x [122 x i1]], [656 x [122 x i1]]* %_40020, i32 0, i32 %_40009, i32 33
  %_40010 = bitcast i1* %_40021 to i8*
  %_40022 = bitcast i8* %_40010 to i1*
  %_40011 = load i1, i1* %_40022
  br i1 %_40011, label %_40004.0, label %_40005.0
_40004.0:
  %_40001 = bitcast i8* %_3 to i8*
  %_40002 = call dereferenceable_or_null(8) i8* @"_SM36scala.collection.immutable.ArraySeq$D4fromL29scala.collection.IterableOnceL22scala.reflect.ClassTagL35scala.collection.immutable.ArraySeqEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_40001)
  ret i8* %_40002
_40005.0:
  %_40012 = phi i8* [%_3, %_40003.0]
  %_40013 = phi i8* [bitcast ({ i8*, i32, i32, i8* }* @"_SM22scala.reflect.ClassTagG4type" to i8*), %_40003.0]
  %_40023 = bitcast i8* %_40012 to i8**
  %_40014 = load i8*, i8** %_40023
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_40014, i8* %_40013)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.immutable.ArraySeq$D4fromL29scala.collection.IterableOnceL22scala.reflect.ClassTagL35scala.collection.immutable.ArraySeqEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  br label %_50000.0
_50000.0:
  %_90007 = icmp eq i8* %_2, null
  br i1 %_90007, label %_90004.0, label %_90005.0
_90004.0:
  br label %_90006.0
_90005.0:
  %_90028 = bitcast i8* %_2 to i8**
  %_90008 = load i8*, i8** %_90028
  %_90029 = bitcast i8* %_90008 to { i8*, i32, i32, i8* }*
  %_90030 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90029, i32 0, i32 1
  %_90009 = bitcast i32* %_90030 to i8*
  %_90031 = bitcast i8* %_90009 to i32*
  %_90010 = load i32, i32* %_90031
  %_90011 = icmp sle i32 417, %_90010
  %_90012 = icmp sle i32 %_90010, 427
  %_90013 = and i1 %_90011, %_90012
  br label %_90006.0
_90006.0:
  %_50002 = phi i1 [%_90013, %_90005.0], [false, %_90004.0]
  br i1 %_50002, label %_60000.0, label %_70000.0
_60000.0:
  %_90017 = icmp eq i8* %_2, null
  br i1 %_90017, label %_90015.0, label %_90014.0
_90014.0:
  %_90032 = bitcast i8* %_2 to i8**
  %_90018 = load i8*, i8** %_90032
  %_90033 = bitcast i8* %_90018 to { i8*, i32, i32, i8* }*
  %_90034 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90033, i32 0, i32 1
  %_90019 = bitcast i32* %_90034 to i8*
  %_90035 = bitcast i8* %_90019 to i32*
  %_90020 = load i32, i32* %_90035
  %_90021 = icmp sle i32 417, %_90020
  %_90022 = icmp sle i32 %_90020, 427
  %_90023 = and i1 %_90021, %_90022
  br i1 %_90023, label %_90015.0, label %_90016.0
_90015.0:
  %_60001 = bitcast i8* %_2 to i8*
  br label %_80000.0
_70000.0:
  br label %_90000.0
_90000.0:
  %_90001 = call dereferenceable_or_null(80) i8* @"_SM12scala.Array$G4load"()
  %_90002 = call dereferenceable_or_null(8) i8* @"_SM12scala.Array$D4fromL29scala.collection.IterableOnceL22scala.reflect.ClassTagL16java.lang.ObjectEO"(i8* nonnull dereferenceable(80) %_90001, i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_3)
  %_90003 = call dereferenceable_or_null(8) i8* @"_SM36scala.collection.immutable.ArraySeq$D15unsafeWrapArrayL16java.lang.ObjectL35scala.collection.immutable.ArraySeqEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_90002)
  br label %_80000.0
_80000.0:
  %_80001 = phi i8* [%_90003, %_90000.0], [%_60001, %_90015.0]
  ret i8* %_80001
_90016.0:
  %_90024 = phi i8* [%_2, %_90014.0]
  %_90025 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM35scala.collection.immutable.ArraySeqG4type" to i8*), %_90014.0]
  %_90036 = bitcast i8* %_90024 to i8**
  %_90026 = load i8*, i8** %_90036
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_90026, i8* %_90025)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM36scala.collection.immutable.ArraySeq$D5emptyL22scala.reflect.ClassTagL35scala.collection.immutable.ArraySeqEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_70004 = icmp ne i8* %_1, null
  br i1 %_70004, label %_70002.0, label %_70003.0
_70002.0:
  %_70010 = bitcast i8* %_1 to { i8*, i8*, i1, i8* }*
  %_70011 = getelementptr { i8*, i8*, i1, i8* }, { i8*, i8*, i1, i8* }* %_70010, i32 0, i32 2
  %_70005 = bitcast i1* %_70011 to i8*
  %_70012 = bitcast i8* %_70005 to i1*
  %_40001 = load i1, i1* %_70012
  %_40003 = xor i1 %_40001, true
  br i1 %_40003, label %_50000.0, label %_60000.0
_50000.0:
  %_50001 = call dereferenceable_or_null(16) i8* @"_SM36scala.collection.immutable.ArraySeq$D20emptyImpl$lzycomputeL41scala.collection.immutable.ArraySeq$ofRefEPT36scala.collection.immutable.ArraySeq$"(i8* dereferenceable_or_null(32) %_1)
  br label %_70000.0
_60000.0:
  %_70007 = icmp ne i8* %_1, null
  br i1 %_70007, label %_70006.0, label %_70003.0
_70006.0:
  %_70013 = bitcast i8* %_1 to { i8*, i8*, i1, i8* }*
  %_70014 = getelementptr { i8*, i8*, i1, i8* }, { i8*, i8*, i1, i8* }* %_70013, i32 0, i32 1
  %_70008 = bitcast i8** %_70014 to i8*
  %_70015 = bitcast i8* %_70008 to i8**
  %_60001 = load i8*, i8** %_70015, !dereferenceable_or_null !{i64 16}
  br label %_70000.0
_70000.0:
  %_70001 = phi i8* [%_60001, %_70006.0], [%_50001, %_50000.0]
  ret i8* %_70001
_70003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM36scala.collection.immutable.ArraySeq$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 141
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 32}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM36scala.collection.immutable.ArraySeq$G4type" to i8*), i64 32)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM36scala.collection.immutable.ArraySeq$RE"(i8* dereferenceable_or_null(32) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM36scala.collection.immutable.ArraySeq$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.EvidenceIterableFactoryD6$init$uEO"(i8* dereferenceable_or_null(32) %_1)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.ClassTagIterableFactoryD6$init$uEO"(i8* dereferenceable_or_null(32) %_1)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.ClassTagSeqFactoryD6$init$uEO"(i8* dereferenceable_or_null(32) %_1)
  call nonnull dereferenceable(8) i8* @"_SM50scala.collection.StrictOptimizedClassTagSeqFactoryD6$init$uEO"(i8* dereferenceable_or_null(32) %_1)
  %_50001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM50scala.collection.ClassTagSeqFactory$AnySeqDelegateG4type" to i8*), i64 16)
  %_50018 = bitcast i8* %_50001 to { i8*, i8* }*
  %_50019 = getelementptr { i8*, i8* }, { i8*, i8* }* %_50018, i32 0, i32 1
  %_50009 = bitcast i8** %_50019 to i8*
  %_50020 = bitcast i8* %_50009 to i8**
  store i8* %_1, i8** %_50020
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableFactoryD6$init$uEO"(i8* nonnull dereferenceable(16) %_50001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.SeqFactoryD6$init$uEO"(i8* nonnull dereferenceable(16) %_50001)
  %_50015 = icmp ne i8* %_1, null
  br i1 %_50015, label %_50013.0, label %_50014.0
_50013.0:
  %_50021 = bitcast i8* %_1 to { i8*, i8*, i1, i8* }*
  %_50022 = getelementptr { i8*, i8*, i1, i8* }, { i8*, i8*, i1, i8* }* %_50021, i32 0, i32 3
  %_50016 = bitcast i8** %_50022 to i8*
  %_50023 = bitcast i8* %_50016 to i8**
  store i8* %_50001, i8** %_50023
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_50014.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.mutable.AbstractMapD13$plus$plus$eqL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.GrowableD13$plus$plus$eqL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.mutable.AbstractMapD15iterableFactoryL32scala.collection.IterableFactoryEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = bitcast i8* bitcast (i8* (i8*)* @"_SM33scala.collection.mutable.IterableD15iterableFactoryL32scala.collection.IterableFactoryEO" to i8*) to i8* (i8*)*
  %_20001 = call dereferenceable_or_null(8) i8* %_20002(i8* dereferenceable_or_null(8) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.mutable.AbstractMapD6resultL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = bitcast i8* bitcast (i8* (i8*)* @"_SM36scala.collection.mutable.AbstractMapD6resultL31scala.collection.mutable.MapOpsEO" to i8*) to i8* (i8*)*
  %_20001 = call dereferenceable_or_null(8) i8* %_20002(i8* dereferenceable_or_null(8) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.mutable.AbstractMapD6resultL31scala.collection.mutable.MapOpsEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(8) i8* @"_SM31scala.collection.mutable.MapOpsD6resultL31scala.collection.mutable.MapOpsEO"(i8* dereferenceable_or_null(8) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.mutable.AbstractMapD8$plus$eqL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM33scala.collection.mutable.GrowableD8$plus$eqL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(8) i8* @"_SM36scala.collection.mutable.AbstractMapD9mapResultL15scala.Function1L32scala.collection.mutable.BuilderEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM32scala.collection.mutable.BuilderD9mapResultL15scala.Function1L32scala.collection.mutable.BuilderEO" to i8*) to i8* (i8*, i8*)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(8) i8* @"_SM36scala.scalanative.runtime.CharArray$D5allociL35scala.scalanative.runtime.CharArrayEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30021 = and i32 1, 31
  %_30005 = shl i32 %_2, %_30021
  %_30006 = add i32 %_30005, 16
  %_30007 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 %_30006)
  %_30008 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_30007)
  %_30034 = bitcast i8* bitcast (i64 (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D12unboxToULongL16java.lang.ObjectjEO" to i8*) to i64 (i8*, i8*)*
  %_30010 = call i64 %_30034(i8* null, i8* dereferenceable_or_null(16) %_30008)
  %_30011 = call i8* @"scalanative_alloc_atomic"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), i64 %_30010)
  %_30035 = bitcast i8* %_30011 to i8*
  %_30036 = getelementptr i8, i8* %_30035, i64 8
  %_30013 = bitcast i8* %_30036 to i8*
  %_30037 = bitcast i8* %_30013 to i32*
  store i32 %_2, i32* %_30037
  %_30038 = bitcast i8* %_30011 to i8*
  %_30039 = getelementptr i8, i8* %_30038, i64 12
  %_30016 = bitcast i8* %_30039 to i8*
  %_30040 = bitcast i8* %_30016 to i32*
  store i32 2, i32* %_30040
  %_30019 = bitcast i8* %_30011 to i8*
  %_30027 = icmp eq i8* %_30019, null
  br i1 %_30027, label %_30025.0, label %_30024.0
_30024.0:
  %_30041 = bitcast i8* %_30019 to i8**
  %_30028 = load i8*, i8** %_30041
  %_30029 = icmp eq i8* %_30028, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*)
  br i1 %_30029, label %_30025.0, label %_30026.0
_30025.0:
  %_30020 = bitcast i8* %_30019 to i8*
  ret i8* %_30020
_30026.0:
  %_30030 = phi i8* [%_30019, %_30024.0]
  %_30031 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM35scala.scalanative.runtime.CharArrayG4type" to i8*), %_30024.0]
  %_30042 = bitcast i8* %_30030 to i8**
  %_30032 = load i8*, i8** %_30042
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_30032, i8* %_30031)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM36scala.scalanative.runtime.CharArray$D8snapshotiR_L35scala.scalanative.runtime.CharArrayEO"(i8* %_1, i32 %_2, i8* %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40001 = call dereferenceable_or_null(8) i8* @"_SM36scala.scalanative.runtime.CharArray$D5allociL35scala.scalanative.runtime.CharArrayEO"(i8* dereferenceable_or_null(8) %_1, i32 %_2)
  %_40002 = call i8* @"_SM35scala.scalanative.runtime.CharArrayD5atRawiR_EO"(i8* dereferenceable_or_null(8) %_40001, i32 0)
  %_40012 = and i32 1, 31
  %_40006 = shl i32 %_2, %_40012
  %_40007 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 %_40006)
  %_40008 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_40007)
  %_40013 = bitcast i8* bitcast (i64 (i8*, i8*)* @"_SM32scala.scalanative.runtime.Boxes$D12unboxToULongL16java.lang.ObjectjEO" to i8*) to i64 (i8*, i8*)*
  %_40010 = call i64 %_40013(i8* null, i8* dereferenceable_or_null(16) %_40008)
  %_40011 = call i8* @"memcpy"(i8* %_40002, i8* %_3, i64 %_40010)
  ret i8* %_40001
}

define nonnull dereferenceable(8) i8* @"_SM36scala.scalanative.runtime.CharArray$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(16) i8* @"_SM36scala.scalanative.unsafe.CFuncPtr17$D10fromRawPtrR_L35scala.scalanative.unsafe.CFuncPtr17EO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM35scala.scalanative.unsafe.CFuncPtr17G4type" to i8*), i64 16)
  %_30006 = bitcast i8* %_30002 to { i8*, i8* }*
  %_30007 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30006, i32 0, i32 1
  %_30005 = bitcast i8** %_30007 to i8*
  %_30008 = bitcast i8* %_30005 to i8**
  store i8* %_2, i8** %_30008
  ret i8* %_30002
}

define nonnull dereferenceable(8) i8* @"_SM36scala.scalanative.unsafe.CFuncPtr17$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(16) i8* @"_SM36scala.scalanative.unsafe.CFuncPtr20$D10fromRawPtrR_L35scala.scalanative.unsafe.CFuncPtr20EO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM35scala.scalanative.unsafe.CFuncPtr20G4type" to i8*), i64 16)
  %_30006 = bitcast i8* %_30002 to { i8*, i8* }*
  %_30007 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30006, i32 0, i32 1
  %_30005 = bitcast i8** %_30007 to i8*
  %_30008 = bitcast i8* %_30005 to i8**
  store i8* %_2, i8** %_30008
  ret i8* %_30002
}

define nonnull dereferenceable(8) i8* @"_SM36scala.scalanative.unsafe.CFuncPtr20$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i32 @"_SM37java.nio.file.FileVisitOption$$anon$1D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM37java.nio.file.FileVisitOption$$anon$1D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM37java.nio.file.FileVisitOption$$anon$1D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define i32 @"_SM37java.nio.file.FileVisitResult$$anon$1D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM37java.nio.file.FileVisitResult$$anon$1D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM37java.nio.file.FileVisitResult$$anon$1D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define i32 @"_SM37java.util.concurrent.TimeUnit$$anon$7D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM37java.util.concurrent.TimeUnit$$anon$7D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM37java.util.concurrent.TimeUnit$$anon$7D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define nonnull dereferenceable(16) i8* @"_SM37scala.scalanative.runtime.DoubleArrayD5applyiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30007 = icmp ne i8* %_1, null
  br i1 %_30007, label %_30005.0, label %_30006.0
_30005.0:
  %_30018 = bitcast i8* %_1 to { i8*, i32 }*
  %_30019 = getelementptr { i8*, i32 }, { i8*, i32 }* %_30018, i32 0, i32 1
  %_30008 = bitcast i32* %_30019 to i8*
  %_30020 = bitcast i8* %_30008 to i32*
  %_30004 = load i32, i32* %_30020
  %_30011 = icmp sge i32 %_2, 0
  %_30012 = icmp slt i32 %_2, %_30004
  %_30013 = and i1 %_30011, %_30012
  br i1 %_30013, label %_30009.0, label %_30010.0
_30009.0:
  %_30021 = bitcast i8* %_1 to { i8*, i32, i32, [0 x double] }*
  %_30022 = getelementptr { i8*, i32, i32, [0 x double] }, { i8*, i32, i32, [0 x double] }* %_30021, i32 0, i32 3, i32 %_2
  %_30014 = bitcast double* %_30022 to i8*
  %_30023 = bitcast i8* %_30014 to double*
  %_30001 = load double, double* %_30023
  %_30003 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D11boxToDoubledL16java.lang.DoubleEO"(i8* null, double %_30001)
  ret i8* %_30003
_30006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_30010.0:
  %_30016 = phi i32 [%_2, %_30005.0]
  %_30024 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_30024(i8* null, i32 %_30016)
  unreachable
}

define double @"_SM37scala.scalanative.runtime.DoubleArrayD5applyidEO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp slt i32 %_2, 0
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  %_90003 = icmp ne i8* %_1, null
  br i1 %_90003, label %_90001.0, label %_90002.0
_90001.0:
  %_90011 = bitcast i8* %_1 to { i8*, i32 }*
  %_90012 = getelementptr { i8*, i32 }, { i8*, i32 }* %_90011, i32 0, i32 1
  %_90004 = bitcast i32* %_90012 to i8*
  %_90013 = bitcast i8* %_90004 to i32*
  %_50001 = load i32, i32* %_90013
  %_50003 = icmp sge i32 %_2, %_50001
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_50003, %_90001.0], [true, %_40000.0]
  br i1 %_60001, label %_70000.0, label %_80000.0
_80000.0:
  %_90005 = and i32 3, 31
  %_80005 = shl i32 %_2, %_90005
  %_80006 = add i32 %_80005, 16
  %_80007 = call i64 @"_SM10scala.Int$D8int2longijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM10scala.Int$G8instance" to i8*), i32 %_80006)
  %_80009 = bitcast i8* %_1 to i8*
  %_90014 = bitcast i8* %_80009 to i8*
  %_90015 = getelementptr i8, i8* %_90014, i64 %_80007
  %_80010 = bitcast i8* %_90015 to i8*
  %_90016 = bitcast i8* %_80010 to double*
  %_80011 = load double, double* %_90016
  br label %_90000.0
_90000.0:
  ret double %_80011
_70000.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.runtime.package$G8instance" to i8*), i32 %_2)
  br label %_90007.0
_90002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_90007.0:
  %_90017 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_90017(i8* null)
  unreachable
}

define i8* @"_SM37scala.scalanative.runtime.DoubleArrayD5atRawiR_EO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = icmp slt i32 %_2, 0
  br i1 %_30002, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  %_90005 = icmp ne i8* %_1, null
  br i1 %_90005, label %_90003.0, label %_90004.0
_90003.0:
  %_90013 = bitcast i8* %_1 to { i8*, i32 }*
  %_90014 = getelementptr { i8*, i32 }, { i8*, i32 }* %_90013, i32 0, i32 1
  %_90006 = bitcast i32* %_90014 to i8*
  %_90015 = bitcast i8* %_90006 to i32*
  %_50001 = load i32, i32* %_90015
  %_50003 = icmp sge i32 %_2, %_50001
  br label %_60000.0
_60000.0:
  %_60001 = phi i1 [%_50003, %_90003.0], [true, %_40000.0]
  br i1 %_60001, label %_70000.0, label %_80000.0
_80000.0:
  %_90007 = and i32 3, 31
  %_80005 = shl i32 %_2, %_90007
  %_80006 = add i32 %_80005, 16
  %_80007 = call i64 @"_SM10scala.Int$D8int2longijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM10scala.Int$G8instance" to i8*), i32 %_80006)
  br label %_90000.0
_90000.0:
  %_90001 = bitcast i8* %_1 to i8*
  %_90016 = bitcast i8* %_90001 to i8*
  %_90017 = getelementptr i8, i8* %_90016, i64 %_80007
  %_90002 = bitcast i8* %_90017 to i8*
  ret i8* %_90002
_70000.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.runtime.package$G8instance" to i8*), i32 %_2)
  br label %_90009.0
_90004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_90009.0:
  %_90018 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_90018(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM37scala.scalanative.runtime.DoubleArrayD6strideL32scala.scalanative.unsigned.ULongEO"(i8* %_1) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20003 = call i32 @"_SM35scala.scalanative.unsigned.package$D15UnsignedRichIntiiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM35scala.scalanative.unsigned.package$G8instance" to i8*), i32 8)
  %_20004 = call dereferenceable_or_null(16) i8* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$D17toULong$extensioniL32scala.scalanative.unsigned.ULongEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM51scala.scalanative.unsigned.package$UnsignedRichInt$G8instance" to i8*), i32 %_20003)
  ret i8* %_20004
}

define nonnull dereferenceable(8) i8* @"_SM37scala.scalanative.runtime.DoubleArrayD6updateiL16java.lang.ObjectuEO"(i8* %_1, i32 %_2, i8* %_3) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40018 = bitcast i8* bitcast (double (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D13unboxToDoubleL16java.lang.ObjectdEO" to i8*) to double (i8*, i8*)*
  %_40001 = call double %_40018(i8* null, i8* dereferenceable_or_null(8) %_3)
  %_40007 = icmp ne i8* %_1, null
  br i1 %_40007, label %_40005.0, label %_40006.0
_40005.0:
  %_40019 = bitcast i8* %_1 to { i8*, i32 }*
  %_40020 = getelementptr { i8*, i32 }, { i8*, i32 }* %_40019, i32 0, i32 1
  %_40008 = bitcast i32* %_40020 to i8*
  %_40021 = bitcast i8* %_40008 to i32*
  %_40004 = load i32, i32* %_40021
  %_40011 = icmp sge i32 %_2, 0
  %_40012 = icmp slt i32 %_2, %_40004
  %_40013 = and i1 %_40011, %_40012
  br i1 %_40013, label %_40009.0, label %_40010.0
_40009.0:
  %_40022 = bitcast i8* %_1 to { i8*, i32, i32, [0 x double] }*
  %_40023 = getelementptr { i8*, i32, i32, [0 x double] }, { i8*, i32, i32, [0 x double] }* %_40022, i32 0, i32 3, i32 %_2
  %_40014 = bitcast double* %_40023 to i8*
  %_40024 = bitcast i8* %_40014 to double*
  store double %_40001, double* %_40024
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_40006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_40010.0:
  %_40016 = phi i32 [%_2, %_40005.0]
  %_40025 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_40025(i8* null, i32 %_40016)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM37scala.scalanative.runtime.DoubleArrayD6updateiduEO"(i8* %_1, i32 %_2, double %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40002 = icmp slt i32 %_2, 0
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  br label %_70000.0
_60000.0:
  %_100003 = icmp ne i8* %_1, null
  br i1 %_100003, label %_100001.0, label %_100002.0
_100001.0:
  %_100012 = bitcast i8* %_1 to { i8*, i32 }*
  %_100013 = getelementptr { i8*, i32 }, { i8*, i32 }* %_100012, i32 0, i32 1
  %_100004 = bitcast i32* %_100013 to i8*
  %_100014 = bitcast i8* %_100004 to i32*
  %_60001 = load i32, i32* %_100014
  %_60003 = icmp sge i32 %_2, %_60001
  br label %_70000.0
_70000.0:
  %_70001 = phi i1 [%_60003, %_100001.0], [true, %_50000.0]
  br i1 %_70001, label %_80000.0, label %_90000.0
_90000.0:
  %_100005 = and i32 3, 31
  %_90005 = shl i32 %_2, %_100005
  %_90006 = add i32 %_90005, 16
  %_90007 = call i64 @"_SM10scala.Int$D8int2longijEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM10scala.Int$G8instance" to i8*), i32 %_90006)
  %_90009 = bitcast i8* %_1 to i8*
  %_100015 = bitcast i8* %_90009 to i8*
  %_100016 = getelementptr i8, i8* %_100015, i64 %_90007
  %_90010 = bitcast i8* %_100016 to i8*
  %_100017 = bitcast i8* %_90010 to double*
  store double %_3, double* %_100017
  br label %_100000.0
_100000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_80000.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM34scala.scalanative.runtime.package$G8instance" to i8*), i32 %_2)
  br label %_100008.0
_100002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_100008.0:
  %_100018 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_100018(i8* null)
  unreachable
}

define nonnull dereferenceable(48) i8* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$D5applyL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL28scala.scalanative.unsafe.TagL37scala.scalanative.unsafe.Tag$CStruct5EO"(i8* %_1, i8* %_2, i8* %_3, i8* %_4, i8* %_5, i8* %_6) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_70000.0:
  %_70002 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM37scala.scalanative.unsafe.Tag$CStruct5G4type" to i8*), i64 48)
  %_70018 = bitcast i8* %_70002 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_70019 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_70018, i32 0, i32 1
  %_70009 = bitcast i8** %_70019 to i8*
  %_70020 = bitcast i8* %_70009 to i8**
  store i8* %_6, i8** %_70020
  %_70021 = bitcast i8* %_70002 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_70022 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_70021, i32 0, i32 2
  %_70011 = bitcast i8** %_70022 to i8*
  %_70023 = bitcast i8* %_70011 to i8**
  store i8* %_5, i8** %_70023
  %_70024 = bitcast i8* %_70002 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_70025 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_70024, i32 0, i32 3
  %_70013 = bitcast i8** %_70025 to i8*
  %_70026 = bitcast i8* %_70013 to i8**
  store i8* %_4, i8** %_70026
  %_70027 = bitcast i8* %_70002 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_70028 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_70027, i32 0, i32 4
  %_70015 = bitcast i8** %_70028 to i8*
  %_70029 = bitcast i8* %_70015 to i8**
  store i8* %_3, i8** %_70029
  %_70030 = bitcast i8* %_70002 to { i8*, i8*, i8*, i8*, i8*, i8* }*
  %_70031 = getelementptr { i8*, i8*, i8*, i8*, i8*, i8* }, { i8*, i8*, i8*, i8*, i8*, i8* }* %_70030, i32 0, i32 5
  %_70017 = bitcast i8** %_70031 to i8*
  %_70032 = bitcast i8* %_70017 to i8**
  store i8* %_2, i8** %_70032
  ret i8* %_70002
}

define nonnull dereferenceable(32) i8* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$D8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-281" to i8*)
}

define nonnull dereferenceable(8) i8* @"_SM38scala.scalanative.unsafe.Tag$CStruct5$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define i32 @"_SM40java.nio.file.StandardOpenOption$$anon$7D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM40java.nio.file.StandardOpenOption$$anon$7D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM40java.nio.file.StandardOpenOption$$anon$7D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.Map$EmptyMap$D3getL16java.lang.ObjectL12scala.OptionEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM11scala.None$G4load"()
  ret i8* %_30001
}

define i32 @"_SM40scala.collection.immutable.Map$EmptyMap$D4sizeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i32 0
}

define i8* @"_SM40scala.collection.immutable.Map$EmptyMap$D5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  call i8* @"_SM40scala.collection.immutable.Map$EmptyMap$D5applyL16java.lang.ObjectnEO"(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_2)
  br label %_30003.0
_30003.0:
  %_30006 = bitcast i8* bitcast (i8* (i8*)* @"_SM34scala.scalanative.runtime.package$D14throwUndefinednEO" to i8*) to i8* (i8*)*
  call i8* %_30006(i8* null)
  unreachable
}

define i8* @"_SM40scala.collection.immutable.Map$EmptyMap$D5applyL16java.lang.ObjectnEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30005 = icmp eq i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-263" to i8*), null
  br i1 %_30005, label %_40000.0, label %_50000.0
_40000.0:
  br label %_60000.0
_50000.0:
  br label %_60000.0
_60000.0:
  %_60001 = phi i8* [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-263" to i8*), %_50000.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_40000.0]
  %_60003 = icmp eq i8* %_2, null
  br i1 %_60003, label %_70000.0, label %_80000.0
_70000.0:
  br label %_90000.0
_80000.0:
  %_190003 = icmp ne i8* %_2, null
  br i1 %_190003, label %_190001.0, label %_190002.0
_190001.0:
  %_190014 = bitcast i8* %_2 to i8**
  %_190004 = load i8*, i8** %_190014
  %_190015 = bitcast i8* %_190004 to { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }*
  %_190016 = getelementptr { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }, { { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* %_190015, i32 0, i32 4, i32 1
  %_190005 = bitcast i8** %_190016 to i8*
  %_190017 = bitcast i8* %_190005 to i8**
  %_80002 = load i8*, i8** %_190017
  %_190018 = bitcast i8* %_80002 to i8* (i8*)*
  %_80003 = call dereferenceable_or_null(32) i8* %_190018(i8* dereferenceable_or_null(8) %_2)
  br label %_90000.0
_90000.0:
  %_90001 = phi i8* [%_80003, %_190001.0], [bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-259" to i8*), %_70000.0]
  %_190019 = bitcast i8* bitcast (i8* (i8*, i8*)* @"_SM16java.lang.StringD6concatL16java.lang.StringL16java.lang.StringEO" to i8*) to i8* (i8*, i8*)*
  %_90002 = call dereferenceable_or_null(32) i8* %_190019(i8* nonnull dereferenceable(32) %_60001, i8* dereferenceable_or_null(32) %_90001)
  br label %_180000.0
_180000.0:
  %_180001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM32java.util.NoSuchElementExceptionG4type" to i8*), i64 40)
  %_190020 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_190021 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_190020, i32 0, i32 5
  %_190007 = bitcast i1* %_190021 to i8*
  %_190022 = bitcast i8* %_190007 to i1*
  store i1 true, i1* %_190022
  %_190023 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_190024 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_190023, i32 0, i32 4
  %_190009 = bitcast i1* %_190024 to i8*
  %_190025 = bitcast i8* %_190009 to i1*
  store i1 true, i1* %_190025
  %_190026 = bitcast i8* %_180001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_190027 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_190026, i32 0, i32 3
  %_190011 = bitcast i8** %_190027 to i8*
  %_190028 = bitcast i8* %_190011 to i8**
  store i8* %_90002, i8** %_190028
  %_180005 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_180001)
  br label %_190000.0
_190000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_180001)
  unreachable
_190002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM40scala.collection.immutable.Map$EmptyMap$D7isEmptyzEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i1 true
}

define dereferenceable_or_null(8) i8* @"_SM40scala.collection.immutable.Map$EmptyMap$D8iteratorL25scala.collection.IteratorEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(16) i8* @"_SM26scala.collection.Iterator$G4load"()
  %_20002 = call dereferenceable_or_null(8) i8* @"_SM26scala.collection.Iterator$D5emptyL25scala.collection.IteratorEO"(i8* nonnull dereferenceable(16) %_20001)
  ret i8* %_20002
}

define dereferenceable_or_null(8) i8* @"_SM40scala.collection.immutable.Map$EmptyMap$D9getOrElseL16java.lang.ObjectL15scala.Function0L16java.lang.ObjectEO"(i8* %_1, i8* %_2, i8* %_3) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_40006 = icmp ne i8* %_3, null
  br i1 %_40006, label %_40004.0, label %_40005.0
_40004.0:
  %_40013 = bitcast i8* %_3 to i8**
  %_40007 = load i8*, i8** %_40013
  %_40014 = bitcast i8* %_40007 to { i8*, i32, i32, i8* }*
  %_40015 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_40014, i32 0, i32 2
  %_40008 = bitcast i32* %_40015 to i8*
  %_40016 = bitcast i8* %_40008 to i32*
  %_40009 = load i32, i32* %_40016
  %_40017 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_40018 = getelementptr i8*, i8** %_40017, i32 1529
  %_40010 = bitcast i8** %_40018 to i8*
  %_40019 = bitcast i8* %_40010 to i8**
  %_40020 = getelementptr i8*, i8** %_40019, i32 %_40009
  %_40011 = bitcast i8** %_40020 to i8*
  %_40021 = bitcast i8* %_40011 to i8**
  %_40002 = load i8*, i8** %_40021
  %_40022 = bitcast i8* %_40002 to i8* (i8*)*
  %_40003 = call dereferenceable_or_null(8) i8* %_40022(i8* dereferenceable_or_null(8) %_3)
  ret i8* %_40003
_40005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM40scala.collection.immutable.Map$EmptyMap$D9knownSizeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i32 0
}

define dereferenceable_or_null(8) i8* @"_SM40scala.collection.immutable.Map$EmptyMap$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 107
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 8}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM40scala.collection.immutable.Map$EmptyMap$G4type" to i8*), i64 8)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.Map$EmptyMap$RE"(i8* dereferenceable_or_null(8) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.Map$EmptyMap$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.MapOpsD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.MapFactoryDefaultsD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.MapD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.immutable.IterableD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM33scala.collection.immutable.MapOpsD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.immutable.MapD6$init$uEO"(i8* dereferenceable_or_null(8) %_1)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(8) i8* @"_SM41scala.reflect.ClassManifestDeprecatedApisD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM42scala.collection.immutable.ArraySeq$ofCharD11unsafeArrayL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i8* }*
  %_30008 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30007, i32 0, i32 1
  %_30005 = bitcast i8** %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i8**
  %_30001 = load i8*, i8** %_30009, !dereferenceable_or_null !{i64 8}
  ret i8* %_30001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(16) i8* @"_SM42scala.collection.immutable.ArraySeq$ofCharD5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30005 = bitcast i8* bitcast (i32 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D10unboxToIntL16java.lang.ObjectiEO" to i8*) to i32 (i8*, i8*)*
  %_30001 = call i32 %_30005(i8* null, i8* dereferenceable_or_null(8) %_2)
  %_30002 = call i16 @"_SM42scala.collection.immutable.ArraySeq$ofCharD5applyicEO"(i8* dereferenceable_or_null(16) %_1, i32 %_30001)
  %_30004 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8* null, i16 %_30002)
  ret i8* %_30004
}

define nonnull dereferenceable(16) i8* @"_SM42scala.collection.immutable.ArraySeq$ofCharD5applyiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call i16 @"_SM42scala.collection.immutable.ArraySeq$ofCharD5applyicEO"(i8* dereferenceable_or_null(16) %_1, i32 %_2)
  %_30003 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8* null, i16 %_30001)
  ret i8* %_30003
}

define i16 @"_SM42scala.collection.immutable.ArraySeq$ofCharD5applyicEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_40004 = icmp ne i8* %_1, null
  br i1 %_40004, label %_40002.0, label %_40003.0
_40002.0:
  %_40019 = bitcast i8* %_1 to { i8*, i8* }*
  %_40020 = getelementptr { i8*, i8* }, { i8*, i8* }* %_40019, i32 0, i32 1
  %_40005 = bitcast i8** %_40020 to i8*
  %_40021 = bitcast i8* %_40005 to i8**
  %_40001 = load i8*, i8** %_40021, !dereferenceable_or_null !{i64 8}
  %_40008 = icmp ne i8* %_40001, null
  br i1 %_40008, label %_40007.0, label %_40003.0
_40007.0:
  %_40022 = bitcast i8* %_40001 to { i8*, i32 }*
  %_40023 = getelementptr { i8*, i32 }, { i8*, i32 }* %_40022, i32 0, i32 1
  %_40009 = bitcast i32* %_40023 to i8*
  %_40024 = bitcast i8* %_40009 to i32*
  %_40006 = load i32, i32* %_40024
  %_40012 = icmp sge i32 %_2, 0
  %_40013 = icmp slt i32 %_2, %_40006
  %_40014 = and i1 %_40012, %_40013
  br i1 %_40014, label %_40010.0, label %_40011.0
_40010.0:
  %_40025 = bitcast i8* %_40001 to { i8*, i32, i32, [0 x i16] }*
  %_40026 = getelementptr { i8*, i32, i32, [0 x i16] }, { i8*, i32, i32, [0 x i16] }* %_40025, i32 0, i32 3, i32 %_2
  %_40015 = bitcast i16* %_40026 to i8*
  %_40027 = bitcast i8* %_40015 to i16*
  %_30001 = load i16, i16* %_40027
  ret i16 %_30001
_40003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_40011.0:
  %_40017 = phi i32 [%_2, %_40007.0]
  %_40028 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_40028(i8* null, i32 %_40017)
  unreachable
}

define i1 @"_SM42scala.collection.immutable.ArraySeq$ofCharD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_100005 = icmp eq i8* %_2, null
  br i1 %_100005, label %_100002.0, label %_100003.0
_100002.0:
  br label %_100004.0
_100003.0:
  %_100026 = bitcast i8* %_2 to i8**
  %_100006 = load i8*, i8** %_100026
  %_100007 = icmp eq i8* %_100006, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM42scala.collection.immutable.ArraySeq$ofCharG4type" to i8*)
  br label %_100004.0
_100004.0:
  %_40002 = phi i1 [%_100007, %_100003.0], [false, %_100002.0]
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_100011 = icmp eq i8* %_2, null
  br i1 %_100011, label %_100009.0, label %_100008.0
_100008.0:
  %_100027 = bitcast i8* %_2 to i8**
  %_100012 = load i8*, i8** %_100027
  %_100013 = icmp eq i8* %_100012, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM42scala.collection.immutable.ArraySeq$ofCharG4type" to i8*)
  br i1 %_100013, label %_100009.0, label %_100010.0
_100009.0:
  %_50001 = bitcast i8* %_2 to i8*
  %_100016 = icmp ne i8* %_1, null
  br i1 %_100016, label %_100014.0, label %_100015.0
_100014.0:
  %_100028 = bitcast i8* %_1 to { i8*, i8* }*
  %_100029 = getelementptr { i8*, i8* }, { i8*, i8* }* %_100028, i32 0, i32 1
  %_100017 = bitcast i8** %_100029 to i8*
  %_100030 = bitcast i8* %_100017 to i8**
  %_70001 = load i8*, i8** %_100030, !dereferenceable_or_null !{i64 8}
  %_100019 = icmp ne i8* %_50001, null
  br i1 %_100019, label %_100018.0, label %_100015.0
_100018.0:
  %_100031 = bitcast i8* %_50001 to { i8*, i8* }*
  %_100032 = getelementptr { i8*, i8* }, { i8*, i8* }* %_100031, i32 0, i32 1
  %_100020 = bitcast i8** %_100032 to i8*
  %_100033 = bitcast i8* %_100020 to i8**
  %_80001 = load i8*, i8** %_100033, !dereferenceable_or_null !{i64 8}
  %_50002 = call i1 @"_SM16java.util.ArraysD6equalsLAc_LAc_zEo"(i8* dereferenceable_or_null(8) %_70001, i8* dereferenceable_or_null(8) %_80001)
  br label %_90000.0
_60000.0:
  br label %_100000.0
_100000.0:
  %_100001 = call i1 @"_SM20scala.collection.SeqD6equalsL16java.lang.ObjectzEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_2)
  br label %_90000.0
_90000.0:
  %_90001 = phi i1 [%_100001, %_100000.0], [%_50002, %_100018.0]
  ret i1 %_90001
_100015.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_100010.0:
  %_100022 = phi i8* [%_2, %_100008.0]
  %_100023 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [7 x i8*] }* @"_SM42scala.collection.immutable.ArraySeq$ofCharG4type" to i8*), %_100008.0]
  %_100034 = bitcast i8* %_100022 to i8**
  %_100024 = load i8*, i8** %_100034
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_100024, i8* %_100023)
  unreachable
}

define i32 @"_SM42scala.collection.immutable.ArraySeq$ofCharD6lengthiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30010 = bitcast i8* %_1 to { i8*, i8* }*
  %_30011 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30010, i32 0, i32 1
  %_30005 = bitcast i8** %_30011 to i8*
  %_30012 = bitcast i8* %_30005 to i8**
  %_30001 = load i8*, i8** %_30012, !dereferenceable_or_null !{i64 8}
  %_30007 = icmp ne i8* %_30001, null
  br i1 %_30007, label %_30006.0, label %_30003.0
_30006.0:
  %_30013 = bitcast i8* %_30001 to { i8*, i32 }*
  %_30014 = getelementptr { i8*, i32 }, { i8*, i32 }* %_30013, i32 0, i32 1
  %_30008 = bitcast i32* %_30014 to i8*
  %_30015 = bitcast i8* %_30008 to i32*
  %_20001 = load i32, i32* %_30015
  ret i32 %_20001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM42scala.collection.immutable.ArraySeq$ofCharD8hashCodeiEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(24) i8* @"_SM31scala.util.hashing.MurmurHash3$G4load"()
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i8* }*
  %_30008 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30007, i32 0, i32 1
  %_30005 = bitcast i8** %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i8**
  %_30001 = load i8*, i8** %_30009, !dereferenceable_or_null !{i64 8}
  %_20002 = call i32 @"_SM31scala.util.hashing.MurmurHash3$D19arraySeqHash$mCc$spLAc_iEO"(i8* nonnull dereferenceable(24) %_20001, i8* dereferenceable_or_null(8) %_30001)
  ret i32 %_20002
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(32) i8* @"_SM42scala.collection.immutable.ArraySeq$ofCharD8iteratorL25scala.collection.IteratorEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_60009 = icmp ne i8* %_1, null
  br i1 %_60009, label %_60007.0, label %_60008.0
_60007.0:
  %_60026 = bitcast i8* %_1 to { i8*, i8* }*
  %_60027 = getelementptr { i8*, i8* }, { i8*, i8* }* %_60026, i32 0, i32 1
  %_60010 = bitcast i8** %_60027 to i8*
  %_60028 = bitcast i8* %_60010 to i8**
  %_30001 = load i8*, i8** %_60028, !dereferenceable_or_null !{i64 8}
  %_60001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM46scala.collection.ArrayOps$ArrayIterator$mcC$spG4type" to i8*), i64 32)
  %_60029 = bitcast i8* %_60001 to { i8*, i32, i32, i8* }*
  %_60030 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60029, i32 0, i32 3
  %_60012 = bitcast i8** %_60030 to i8*
  %_60031 = bitcast i8* %_60012 to i8**
  store i8* %_30001, i8** %_60031
  %_60032 = bitcast i8* %_60001 to { i8*, i32, i32, i8*, i8* }*
  %_60033 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_60032, i32 0, i32 4
  %_60014 = bitcast i8** %_60033 to i8*
  %_60034 = bitcast i8* %_60014 to i8**
  store i8* %_30001, i8** %_60034
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(32) %_60001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(32) %_60001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IteratorD6$init$uEO"(i8* nonnull dereferenceable(32) %_60001)
  %_60035 = bitcast i8* %_60001 to { i8*, i32, i32, i8* }*
  %_60036 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60035, i32 0, i32 2
  %_60019 = bitcast i32* %_60036 to i8*
  %_60037 = bitcast i8* %_60019 to i32*
  store i32 0, i32* %_60037
  %_60038 = bitcast i8* %_60001 to { i8*, i32, i32, i8* }*
  %_60039 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60038, i32 0, i32 3
  %_60020 = bitcast i8** %_60039 to i8*
  %_60040 = bitcast i8* %_60020 to i8**
  %_50003 = load i8*, i8** %_60040, !dereferenceable_or_null !{i64 8}
  %_50004 = call i32 @"_SM27scala.runtime.ScalaRunTime$D12array_lengthL16java.lang.ObjectiEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM27scala.runtime.ScalaRunTime$G8instance" to i8*), i8* dereferenceable_or_null(8) %_50003)
  %_60041 = bitcast i8* %_60001 to { i8*, i32, i32, i8* }*
  %_60042 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_60041, i32 0, i32 1
  %_60022 = bitcast i32* %_60042 to i8*
  %_60043 = bitcast i8* %_60022 to i32*
  store i32 %_50004, i32* %_60043
  call nonnull dereferenceable(8) i8* @"_SM21scala.runtime.StaticsD12releaseFenceuEo"()
  call nonnull dereferenceable(8) i8* @"_SM21scala.runtime.StaticsD12releaseFenceuEo"()
  ret i8* %_60001
_60008.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM42scala.collection.immutable.ArraySeq$ofCharD9addStringL38scala.collection.mutable.StringBuilderL16java.lang.StringL16java.lang.StringL16java.lang.StringL38scala.collection.mutable.StringBuilderEO"(i8* %_1, i8* %_2, i8* %_3, i8* %_4, i8* %_5) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_60000.0:
  %_120010 = icmp ne i8* %_1, null
  br i1 %_120010, label %_120008.0, label %_120009.0
_120008.0:
  %_120034 = bitcast i8* %_1 to { i8*, i8* }*
  %_120035 = getelementptr { i8*, i8* }, { i8*, i8* }* %_120034, i32 0, i32 1
  %_120011 = bitcast i8** %_120035 to i8*
  %_120036 = bitcast i8* %_120011 to i8**
  %_70001 = load i8*, i8** %_120036, !dereferenceable_or_null !{i64 8}
  %_120001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [6 x i8*] }* @"_SM40scala.collection.mutable.ArraySeq$ofCharG4type" to i8*), i64 16)
  %_120037 = bitcast i8* %_120001 to { i8*, i8* }*
  %_120038 = getelementptr { i8*, i8* }, { i8*, i8* }* %_120037, i32 0, i32 1
  %_120013 = bitcast i8** %_120038 to i8*
  %_120039 = bitcast i8* %_120013 to i8**
  store i8* %_70001, i8** %_120039
  call nonnull dereferenceable(8) i8* @"_SM29scala.collection.IterableOnceD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM32scala.collection.IterableOnceOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.IterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.IterableFactoryDefaultsD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM25scala.collection.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM15scala.Function1D6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM21scala.PartialFunctionD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM23scala.collection.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM20scala.collection.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM33scala.collection.mutable.IterableD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM34scala.collection.mutable.CloneableD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM31scala.collection.mutable.SeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM28scala.collection.mutable.SeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM30scala.collection.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM27scala.collection.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.mutable.IndexedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM35scala.collection.mutable.IndexedSeqD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  call nonnull dereferenceable(8) i8* @"_SM38scala.collection.StrictOptimizedSeqOpsD6$init$uEO"(i8* nonnull dereferenceable(16) %_120001)
  %_60002 = call dereferenceable_or_null(16) i8* @"_SM40scala.collection.mutable.ArraySeq$ofCharD9addStringL38scala.collection.mutable.StringBuilderL16java.lang.StringL16java.lang.StringL16java.lang.StringL38scala.collection.mutable.StringBuilderEO"(i8* nonnull dereferenceable(16) %_120001, i8* dereferenceable_or_null(16) %_2, i8* dereferenceable_or_null(32) %_3, i8* dereferenceable_or_null(32) %_4, i8* dereferenceable_or_null(32) %_5)
  ret i8* %_60002
_120009.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM42scala.reflect.ManifestFactory$UnitManifestD12runtimeClassL15java.lang.ClassEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.VoidD4TYPEL15java.lang.ClassEo"()
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM42scala.reflect.ManifestFactory$UnitManifestD8newArrayiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM42scala.reflect.ManifestFactory$UnitManifestD8newArrayiLAL23scala.runtime.BoxedUnit_EO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define nonnull dereferenceable(8) i8* @"_SM42scala.reflect.ManifestFactory$UnitManifestD8newArrayiLAL23scala.runtime.BoxedUnit_EO"(i8* %_1, i32 %_2) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM38scala.scalanative.runtime.ObjectArray$D5allociL37scala.scalanative.runtime.ObjectArrayEO"(i8* bitcast ({ i8* }* @"_SM38scala.scalanative.runtime.ObjectArray$G8instance" to i8*), i32 %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(32) i8* @"_SM43java.util.FormatterCompanionImpl$LocaleInfoD15zeroDigitStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20005 = bitcast i8* bitcast (i16 (i8*)* @"_SM48java.util.FormatterCompanionImpl$RootLocaleInfo$D9zeroDigitcEO" to i8*) to i16 (i8*)*
  %_20001 = call i16 %_20005(i8* dereferenceable_or_null(16) %_1)
  %_20003 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D14boxToCharactercL19java.lang.CharacterEO"(i8* null, i16 %_20001)
  %_20006 = bitcast i8* bitcast (i8* (i8*)* @"_SM19java.lang.CharacterD8toStringL16java.lang.StringEO" to i8*) to i8* (i8*)*
  %_20004 = call dereferenceable_or_null(32) i8* %_20006(i8* nonnull dereferenceable(16) %_20003)
  ret i8* %_20004
}

define dereferenceable_or_null(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD18strictOptimizedMapL32scala.collection.mutable.BuilderL15scala.Function1L16java.lang.ObjectEO"(i8* %_1, i8* %_2, i8* %_3) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_40000.0:
  %_80006 = icmp ne i8* %_1, null
  br i1 %_80006, label %_80004.0, label %_80005.0
_80004.0:
  %_80048 = bitcast i8* %_1 to i8**
  %_80007 = load i8*, i8** %_80048
  %_80049 = bitcast i8* %_80007 to { i8*, i32, i32, i8* }*
  %_80050 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_80049, i32 0, i32 2
  %_80008 = bitcast i32* %_80050 to i8*
  %_80051 = bitcast i8* %_80008 to i32*
  %_80009 = load i32, i32* %_80051
  %_80052 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_80053 = getelementptr i8*, i8** %_80052, i32 446
  %_80010 = bitcast i8** %_80053 to i8*
  %_80054 = bitcast i8* %_80010 to i8**
  %_80055 = getelementptr i8*, i8** %_80054, i32 %_80009
  %_80011 = bitcast i8** %_80055 to i8*
  %_80056 = bitcast i8* %_80011 to i8**
  %_40002 = load i8*, i8** %_80056
  %_80057 = bitcast i8* %_40002 to i8* (i8*)*
  %_40003 = call dereferenceable_or_null(8) i8* %_80057(i8* dereferenceable_or_null(8) %_1)
  br label %_50000.0
_50000.0:
  %_80013 = icmp ne i8* %_40003, null
  br i1 %_80013, label %_80012.0, label %_80005.0
_80012.0:
  %_80058 = bitcast i8* %_40003 to i8**
  %_80014 = load i8*, i8** %_80058
  %_80059 = bitcast i8* %_80014 to { i8*, i32, i32, i8* }*
  %_80060 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_80059, i32 0, i32 2
  %_80015 = bitcast i32* %_80060 to i8*
  %_80061 = bitcast i8* %_80015 to i32*
  %_80016 = load i32, i32* %_80061
  %_80062 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_80063 = getelementptr i8*, i8** %_80062, i32 4359
  %_80017 = bitcast i8** %_80063 to i8*
  %_80064 = bitcast i8* %_80017 to i8**
  %_80065 = getelementptr i8*, i8** %_80064, i32 %_80016
  %_80018 = bitcast i8** %_80065 to i8*
  %_80066 = bitcast i8* %_80018 to i8**
  %_50002 = load i8*, i8** %_80066
  %_80067 = bitcast i8* %_50002 to i1 (i8*)*
  %_50003 = call i1 %_80067(i8* dereferenceable_or_null(8) %_40003)
  br i1 %_50003, label %_60000.0, label %_70000.0
_60000.0:
  %_80020 = icmp ne i8* %_40003, null
  br i1 %_80020, label %_80019.0, label %_80005.0
_80019.0:
  %_80068 = bitcast i8* %_40003 to i8**
  %_80021 = load i8*, i8** %_80068
  %_80069 = bitcast i8* %_80021 to { i8*, i32, i32, i8* }*
  %_80070 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_80069, i32 0, i32 2
  %_80022 = bitcast i32* %_80070 to i8*
  %_80071 = bitcast i8* %_80022 to i32*
  %_80023 = load i32, i32* %_80071
  %_80072 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_80073 = getelementptr i8*, i8** %_80072, i32 4219
  %_80024 = bitcast i8** %_80073 to i8*
  %_80074 = bitcast i8* %_80024 to i8**
  %_80075 = getelementptr i8*, i8** %_80074, i32 %_80023
  %_80025 = bitcast i8** %_80075 to i8*
  %_80076 = bitcast i8* %_80025 to i8**
  %_60002 = load i8*, i8** %_80076
  %_80077 = bitcast i8* %_60002 to i8* (i8*)*
  %_60003 = call dereferenceable_or_null(8) i8* %_80077(i8* dereferenceable_or_null(8) %_40003)
  %_80027 = icmp ne i8* %_3, null
  br i1 %_80027, label %_80026.0, label %_80005.0
_80026.0:
  %_80078 = bitcast i8* %_3 to i8**
  %_80028 = load i8*, i8** %_80078
  %_80079 = bitcast i8* %_80028 to { i8*, i32, i32, i8* }*
  %_80080 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_80079, i32 0, i32 2
  %_80029 = bitcast i32* %_80080 to i8*
  %_80081 = bitcast i8* %_80029 to i32*
  %_80030 = load i32, i32* %_80081
  %_80082 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_80083 = getelementptr i8*, i8** %_80082, i32 1010
  %_80031 = bitcast i8** %_80083 to i8*
  %_80084 = bitcast i8* %_80031 to i8**
  %_80085 = getelementptr i8*, i8** %_80084, i32 %_80030
  %_80032 = bitcast i8** %_80085 to i8*
  %_80086 = bitcast i8* %_80032 to i8**
  %_60005 = load i8*, i8** %_80086
  %_80087 = bitcast i8* %_60005 to i8* (i8*, i8*)*
  %_60006 = call dereferenceable_or_null(8) i8* %_80087(i8* dereferenceable_or_null(8) %_3, i8* dereferenceable_or_null(8) %_60003)
  %_80034 = icmp ne i8* %_2, null
  br i1 %_80034, label %_80033.0, label %_80005.0
_80033.0:
  %_80088 = bitcast i8* %_2 to i8**
  %_80035 = load i8*, i8** %_80088
  %_80089 = bitcast i8* %_80035 to { i8*, i32, i32, i8* }*
  %_80090 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_80089, i32 0, i32 2
  %_80036 = bitcast i32* %_80090 to i8*
  %_80091 = bitcast i8* %_80036 to i32*
  %_80037 = load i32, i32* %_80091
  %_80092 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_80093 = getelementptr i8*, i8** %_80092, i32 3748
  %_80038 = bitcast i8** %_80093 to i8*
  %_80094 = bitcast i8* %_80038 to i8**
  %_80095 = getelementptr i8*, i8** %_80094, i32 %_80037
  %_80039 = bitcast i8** %_80095 to i8*
  %_80096 = bitcast i8* %_80039 to i8**
  %_60008 = load i8*, i8** %_80096
  %_80097 = bitcast i8* %_60008 to i8* (i8*, i8*)*
  %_60009 = call dereferenceable_or_null(8) i8* %_80097(i8* dereferenceable_or_null(8) %_2, i8* dereferenceable_or_null(8) %_60006)
  br label %_50000.0
_70000.0:
  br label %_80000.0
_80000.0:
  %_80041 = icmp ne i8* %_2, null
  br i1 %_80041, label %_80040.0, label %_80005.0
_80040.0:
  %_80098 = bitcast i8* %_2 to i8**
  %_80042 = load i8*, i8** %_80098
  %_80099 = bitcast i8* %_80042 to { i8*, i32, i32, i8* }*
  %_80100 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_80099, i32 0, i32 2
  %_80043 = bitcast i32* %_80100 to i8*
  %_80101 = bitcast i8* %_80043 to i32*
  %_80044 = load i32, i32* %_80101
  %_80102 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_80103 = getelementptr i8*, i8** %_80102, i32 3648
  %_80045 = bitcast i8** %_80103 to i8*
  %_80104 = bitcast i8* %_80045 to i8**
  %_80105 = getelementptr i8*, i8** %_80104, i32 %_80044
  %_80046 = bitcast i8** %_80105 to i8*
  %_80106 = bitcast i8* %_80046 to i8**
  %_80002 = load i8*, i8** %_80106
  %_80107 = bitcast i8* %_80002 to i8* (i8*)*
  %_80003 = call dereferenceable_or_null(8) i8* %_80107(i8* dereferenceable_or_null(8) %_2)
  ret i8* %_80003
_80005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD3mapL15scala.Function1L16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30012 = icmp ne i8* %_1, null
  br i1 %_30012, label %_30010.0, label %_30011.0
_30010.0:
  %_30033 = bitcast i8* %_1 to i8**
  %_30013 = load i8*, i8** %_30033
  %_30034 = bitcast i8* %_30013 to { i8*, i32, i32, i8* }*
  %_30035 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30034, i32 0, i32 2
  %_30014 = bitcast i32* %_30035 to i8*
  %_30036 = bitcast i8* %_30014 to i32*
  %_30015 = load i32, i32* %_30036
  %_30037 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30038 = getelementptr i8*, i8** %_30037, i32 2343
  %_30016 = bitcast i8** %_30038 to i8*
  %_30039 = bitcast i8* %_30016 to i8**
  %_30040 = getelementptr i8*, i8** %_30039, i32 %_30015
  %_30017 = bitcast i8** %_30040 to i8*
  %_30041 = bitcast i8* %_30017 to i8**
  %_30002 = load i8*, i8** %_30041
  %_30042 = bitcast i8* %_30002 to i8* (i8*)*
  %_30003 = call dereferenceable_or_null(8) i8* %_30042(i8* dereferenceable_or_null(8) %_1)
  %_30019 = icmp ne i8* %_30003, null
  br i1 %_30019, label %_30018.0, label %_30011.0
_30018.0:
  %_30043 = bitcast i8* %_30003 to i8**
  %_30020 = load i8*, i8** %_30043
  %_30044 = bitcast i8* %_30020 to { i8*, i32, i32, i8* }*
  %_30045 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30044, i32 0, i32 2
  %_30021 = bitcast i32* %_30045 to i8*
  %_30046 = bitcast i8* %_30021 to i32*
  %_30022 = load i32, i32* %_30046
  %_30047 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30048 = getelementptr i8*, i8** %_30047, i32 1190
  %_30023 = bitcast i8** %_30048 to i8*
  %_30049 = bitcast i8* %_30023 to i8**
  %_30050 = getelementptr i8*, i8** %_30049, i32 %_30022
  %_30024 = bitcast i8** %_30050 to i8*
  %_30051 = bitcast i8* %_30024 to i8**
  %_30005 = load i8*, i8** %_30051
  %_30052 = bitcast i8* %_30005 to i8* (i8*)*
  %_30006 = call dereferenceable_or_null(8) i8* %_30052(i8* dereferenceable_or_null(8) %_30003)
  %_30026 = icmp ne i8* %_1, null
  br i1 %_30026, label %_30025.0, label %_30011.0
_30025.0:
  %_30053 = bitcast i8* %_1 to i8**
  %_30027 = load i8*, i8** %_30053
  %_30054 = bitcast i8* %_30027 to { i8*, i32, i32, i8* }*
  %_30055 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30054, i32 0, i32 2
  %_30028 = bitcast i32* %_30055 to i8*
  %_30056 = bitcast i8* %_30028 to i32*
  %_30029 = load i32, i32* %_30056
  %_30057 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_30058 = getelementptr i8*, i8** %_30057, i32 2990
  %_30030 = bitcast i8** %_30058 to i8*
  %_30059 = bitcast i8* %_30030 to i8**
  %_30060 = getelementptr i8*, i8** %_30059, i32 %_30029
  %_30031 = bitcast i8** %_30060 to i8*
  %_30061 = bitcast i8* %_30031 to i8**
  %_30008 = load i8*, i8** %_30061
  %_30062 = bitcast i8* %_30008 to i8* (i8*, i8*, i8*)*
  %_30009 = call dereferenceable_or_null(8) i8* %_30062(i8* dereferenceable_or_null(8) %_1, i8* dereferenceable_or_null(8) %_30006, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30009
_30011.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM43scala.collection.StrictOptimizedIterableOpsD6$init$uEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD5elemsL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_30008 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_30007, i32 0, i32 4
  %_30005 = bitcast i8** %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i8**
  %_30001 = load i8*, i8** %_30009, !dereferenceable_or_null !{i64 8}
  ret i8* %_30001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(32) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6addOneL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(32) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6addOneL16java.lang.ObjectL43scala.collection.mutable.ArrayBuilder$ofRefEO"(i8* dereferenceable_or_null(32) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(32) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6addOneL16java.lang.ObjectL43scala.collection.mutable.ArrayBuilder$ofRefEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_80005 = icmp ne i8* %_1, null
  br i1 %_80005, label %_80003.0, label %_80004.0
_80003.0:
  %_80035 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_80036 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_80035, i32 0, i32 1
  %_80006 = bitcast i32* %_80036 to i8*
  %_80037 = bitcast i8* %_80006 to i32*
  %_40001 = load i32, i32* %_80037
  %_30002 = add i32 %_40001, 1
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.ArrayBuilderD10ensureSizeiuEO"(i8* dereferenceable_or_null(32) %_1, i32 %_30002)
  %_80009 = icmp ne i8* %_1, null
  br i1 %_80009, label %_80008.0, label %_80004.0
_80008.0:
  %_80038 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_80039 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_80038, i32 0, i32 4
  %_80010 = bitcast i8** %_80039 to i8*
  %_80040 = bitcast i8* %_80010 to i8**
  %_50001 = load i8*, i8** %_80040, !dereferenceable_or_null !{i64 8}
  %_80012 = icmp ne i8* %_1, null
  br i1 %_80012, label %_80011.0, label %_80004.0
_80011.0:
  %_80041 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_80042 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_80041, i32 0, i32 1
  %_80013 = bitcast i32* %_80042 to i8*
  %_80043 = bitcast i8* %_80013 to i32*
  %_60001 = load i32, i32* %_80043
  %_80017 = icmp ne i8* %_50001, null
  br i1 %_80017, label %_80016.0, label %_80004.0
_80016.0:
  %_80044 = bitcast i8* %_50001 to { i8*, i32 }*
  %_80045 = getelementptr { i8*, i32 }, { i8*, i32 }* %_80044, i32 0, i32 1
  %_80018 = bitcast i32* %_80045 to i8*
  %_80046 = bitcast i8* %_80018 to i32*
  %_80015 = load i32, i32* %_80046
  %_80021 = icmp sge i32 %_60001, 0
  %_80022 = icmp slt i32 %_60001, %_80015
  %_80023 = and i1 %_80021, %_80022
  br i1 %_80023, label %_80019.0, label %_80020.0
_80019.0:
  %_80047 = bitcast i8* %_50001 to { i8*, i32, i32, [0 x i8*] }*
  %_80048 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_80047, i32 0, i32 3, i32 %_60001
  %_80024 = bitcast i8** %_80048 to i8*
  %_80049 = bitcast i8* %_80024 to i8**
  store i8* %_2, i8** %_80049
  %_80026 = icmp ne i8* %_1, null
  br i1 %_80026, label %_80025.0, label %_80004.0
_80025.0:
  %_80050 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_80051 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_80050, i32 0, i32 1
  %_80027 = bitcast i32* %_80051 to i8*
  %_80052 = bitcast i8* %_80027 to i32*
  %_70001 = load i32, i32* %_80052
  %_80001 = add i32 %_70001, 1
  %_80030 = icmp ne i8* %_1, null
  br i1 %_80030, label %_80029.0, label %_80004.0
_80029.0:
  %_80053 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_80054 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_80053, i32 0, i32 1
  %_80031 = bitcast i32* %_80054 to i8*
  %_80055 = bitcast i8* %_80031 to i32*
  store i32 %_80001, i32* %_80055
  ret i8* %_1
_80004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_80020.0:
  %_80033 = phi i32 [%_60001, %_80016.0]
  %_80056 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_80056(i8* null, i32 %_80033)
  unreachable
}

define i1 @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_190004 = icmp eq i8* %_2, null
  br i1 %_190004, label %_190001.0, label %_190002.0
_190001.0:
  br label %_190003.0
_190002.0:
  %_190034 = bitcast i8* %_2 to i8**
  %_190005 = load i8*, i8** %_190034
  %_190006 = icmp eq i8* %_190005, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefG4type" to i8*)
  br label %_190003.0
_190003.0:
  %_40002 = phi i1 [%_190006, %_190002.0], [false, %_190001.0]
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_190010 = icmp eq i8* %_2, null
  br i1 %_190010, label %_190008.0, label %_190007.0
_190007.0:
  %_190035 = bitcast i8* %_2 to i8**
  %_190011 = load i8*, i8** %_190035
  %_190012 = icmp eq i8* %_190011, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefG4type" to i8*)
  br i1 %_190012, label %_190008.0, label %_190009.0
_190008.0:
  %_50001 = bitcast i8* %_2 to i8*
  %_190015 = icmp ne i8* %_1, null
  br i1 %_190015, label %_190013.0, label %_190014.0
_190013.0:
  %_190036 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_190037 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_190036, i32 0, i32 1
  %_190016 = bitcast i32* %_190037 to i8*
  %_190038 = bitcast i8* %_190016 to i32*
  %_70001 = load i32, i32* %_190038
  %_190018 = icmp ne i8* %_50001, null
  br i1 %_190018, label %_190017.0, label %_190014.0
_190017.0:
  %_190039 = bitcast i8* %_50001 to { i8*, i32, i32 }*
  %_190040 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_190039, i32 0, i32 1
  %_190019 = bitcast i32* %_190040 to i8*
  %_190041 = bitcast i8* %_190019 to i32*
  %_80001 = load i32, i32* %_190041
  %_50003 = icmp eq i32 %_70001, %_80001
  br i1 %_50003, label %_90000.0, label %_100000.0
_90000.0:
  %_190021 = icmp ne i8* %_1, null
  br i1 %_190021, label %_190020.0, label %_190014.0
_190020.0:
  %_190042 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_190043 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_190042, i32 0, i32 4
  %_190022 = bitcast i8** %_190043 to i8*
  %_190044 = bitcast i8* %_190022 to i8**
  %_110001 = load i8*, i8** %_190044, !dereferenceable_or_null !{i64 8}
  %_90002 = icmp eq i8* %_110001, null
  br i1 %_90002, label %_120000.0, label %_130000.0
_120000.0:
  %_190024 = icmp ne i8* %_50001, null
  br i1 %_190024, label %_190023.0, label %_190014.0
_190023.0:
  %_190045 = bitcast i8* %_50001 to { i8*, i32, i32, i8*, i8* }*
  %_190046 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_190045, i32 0, i32 4
  %_190025 = bitcast i8** %_190046 to i8*
  %_190047 = bitcast i8* %_190025 to i8**
  %_140001 = load i8*, i8** %_190047, !dereferenceable_or_null !{i64 8}
  %_120002 = icmp eq i8* %_140001, null
  br label %_150000.0
_130000.0:
  %_190027 = icmp ne i8* %_50001, null
  br i1 %_190027, label %_190026.0, label %_190014.0
_190026.0:
  %_190048 = bitcast i8* %_50001 to { i8*, i32, i32, i8*, i8* }*
  %_190049 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_190048, i32 0, i32 4
  %_190028 = bitcast i8** %_190049 to i8*
  %_190050 = bitcast i8* %_190028 to i8**
  %_160001 = load i8*, i8** %_190050, !dereferenceable_or_null !{i64 8}
  %_130001 = call i1 @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO"(i8* dereferenceable_or_null(8) %_110001, i8* dereferenceable_or_null(8) %_160001)
  br label %_150000.0
_150000.0:
  %_150001 = phi i1 [%_130001, %_190026.0], [%_120002, %_190023.0]
  br label %_170000.0
_100000.0:
  br label %_170000.0
_170000.0:
  %_170001 = phi i1 [false, %_100000.0], [%_150001, %_150000.0]
  br label %_180000.0
_60000.0:
  br label %_190000.0
_190000.0:
  br label %_180000.0
_180000.0:
  %_180001 = phi i1 [false, %_190000.0], [%_170001, %_170000.0]
  ret i1 %_180001
_190014.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_190009.0:
  %_190030 = phi i8* [%_2, %_190007.0]
  %_190031 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefG4type" to i8*), %_190007.0]
  %_190051 = bitcast i8* %_190030 to i8**
  %_190032 = load i8*, i8** %_190051
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_190032, i8* %_190031)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6resizeiuEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD7mkArrayiLAL16java.lang.Object_EPT43scala.collection.mutable.ArrayBuilder$ofRef"(i8* dereferenceable_or_null(32) %_1, i32 %_2)
  %_50005 = icmp ne i8* %_1, null
  br i1 %_50005, label %_50003.0, label %_50004.0
_50003.0:
  %_50012 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_50013 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_50012, i32 0, i32 4
  %_50006 = bitcast i8** %_50013 to i8*
  %_50014 = bitcast i8* %_50006 to i8**
  store i8* %_30001, i8** %_50014
  %_50009 = icmp ne i8* %_1, null
  br i1 %_50009, label %_50008.0, label %_50004.0
_50008.0:
  %_50015 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_50016 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_50015, i32 0, i32 2
  %_50010 = bitcast i32* %_50016 to i8*
  %_50017 = bitcast i8* %_50010 to i32*
  store i32 %_2, i32* %_50017
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_50004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6resultL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(8) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6resultLAL16java.lang.Object_EO"(i8* dereferenceable_or_null(32) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD6resultLAL16java.lang.Object_EO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_150004 = icmp ne i8* %_1, null
  br i1 %_150004, label %_150002.0, label %_150003.0
_150002.0:
  %_150027 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_150028 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_150027, i32 0, i32 2
  %_150005 = bitcast i32* %_150028 to i8*
  %_150029 = bitcast i8* %_150005 to i32*
  %_30001 = load i32, i32* %_150029
  %_20002 = icmp ne i32 %_30001, 0
  br i1 %_20002, label %_40000.0, label %_50000.0
_40000.0:
  %_150007 = icmp ne i8* %_1, null
  br i1 %_150007, label %_150006.0, label %_150003.0
_150006.0:
  %_150030 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_150031 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_150030, i32 0, i32 2
  %_150008 = bitcast i32* %_150031 to i8*
  %_150032 = bitcast i8* %_150008 to i32*
  %_60001 = load i32, i32* %_150032
  %_150010 = icmp ne i8* %_1, null
  br i1 %_150010, label %_150009.0, label %_150003.0
_150009.0:
  %_150033 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_150034 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_150033, i32 0, i32 1
  %_150011 = bitcast i32* %_150034 to i8*
  %_150035 = bitcast i8* %_150011 to i32*
  %_70001 = load i32, i32* %_150035
  %_40002 = icmp eq i32 %_60001, %_70001
  br label %_80000.0
_50000.0:
  br label %_80000.0
_80000.0:
  %_80001 = phi i1 [false, %_50000.0], [%_40002, %_150009.0]
  br i1 %_80001, label %_90000.0, label %_100000.0
_90000.0:
  %_150014 = icmp ne i8* %_1, null
  br i1 %_150014, label %_150013.0, label %_150003.0
_150013.0:
  %_150036 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_150037 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_150036, i32 0, i32 2
  %_150015 = bitcast i32* %_150037 to i8*
  %_150038 = bitcast i8* %_150015 to i32*
  store i32 0, i32* %_150038
  %_150017 = icmp ne i8* %_1, null
  br i1 %_150017, label %_150016.0, label %_150003.0
_150016.0:
  %_150039 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_150040 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_150039, i32 0, i32 4
  %_150018 = bitcast i8** %_150040 to i8*
  %_150041 = bitcast i8* %_150018 to i8**
  %_120001 = load i8*, i8** %_150041, !dereferenceable_or_null !{i64 8}
  %_150021 = icmp ne i8* %_1, null
  br i1 %_150021, label %_150020.0, label %_150003.0
_150020.0:
  %_150042 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_150043 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_150042, i32 0, i32 4
  %_150022 = bitcast i8** %_150043 to i8*
  %_150044 = bitcast i8* %_150022 to i8**
  store i8* null, i8** %_150044
  br label %_140000.0
_100000.0:
  %_150024 = icmp ne i8* %_1, null
  br i1 %_150024, label %_150023.0, label %_150003.0
_150023.0:
  %_150045 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_150046 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_150045, i32 0, i32 1
  %_150025 = bitcast i32* %_150046 to i8*
  %_150047 = bitcast i8* %_150025 to i32*
  %_150001 = load i32, i32* %_150047
  %_100001 = call dereferenceable_or_null(8) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD7mkArrayiLAL16java.lang.Object_EPT43scala.collection.mutable.ArrayBuilder$ofRef"(i8* dereferenceable_or_null(32) %_1, i32 %_150001)
  br label %_140000.0
_140000.0:
  %_140001 = phi i8* [%_100001, %_150023.0], [%_120001, %_150020.0]
  ret i8* %_140001
_150003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD7mkArrayiLAL16java.lang.Object_EPT43scala.collection.mutable.ArrayBuilder$ofRef"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_170004 = icmp ne i8* %_1, null
  br i1 %_170004, label %_170002.0, label %_170003.0
_170002.0:
  %_170039 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_170040 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_170039, i32 0, i32 2
  %_170005 = bitcast i32* %_170040 to i8*
  %_170041 = bitcast i8* %_170005 to i32*
  %_40001 = load i32, i32* %_170041
  %_30002 = icmp eq i32 %_40001, %_2
  br i1 %_30002, label %_50000.0, label %_60000.0
_50000.0:
  %_170007 = icmp ne i8* %_1, null
  br i1 %_170007, label %_170006.0, label %_170003.0
_170006.0:
  %_170042 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_170043 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_170042, i32 0, i32 2
  %_170008 = bitcast i32* %_170043 to i8*
  %_170044 = bitcast i8* %_170008 to i32*
  %_70001 = load i32, i32* %_170044
  %_50002 = icmp sgt i32 %_70001, 0
  br label %_80000.0
_60000.0:
  br label %_80000.0
_80000.0:
  %_80001 = phi i1 [false, %_60000.0], [%_50002, %_170006.0]
  br i1 %_80001, label %_90000.0, label %_100000.0
_90000.0:
  %_170010 = icmp ne i8* %_1, null
  br i1 %_170010, label %_170009.0, label %_170003.0
_170009.0:
  %_170045 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_170046 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_170045, i32 0, i32 4
  %_170011 = bitcast i8** %_170046 to i8*
  %_170047 = bitcast i8* %_170011 to i8**
  %_110001 = load i8*, i8** %_170047, !dereferenceable_or_null !{i64 8}
  br label %_120000.0
_100000.0:
  %_170013 = icmp ne i8* %_1, null
  br i1 %_170013, label %_170012.0, label %_170003.0
_170012.0:
  %_170048 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_170049 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_170048, i32 0, i32 4
  %_170014 = bitcast i8** %_170049 to i8*
  %_170050 = bitcast i8* %_170014 to i8**
  %_130001 = load i8*, i8** %_170050, !dereferenceable_or_null !{i64 8}
  %_100002 = icmp eq i8* %_130001, null
  br i1 %_100002, label %_140000.0, label %_150000.0
_140000.0:
  %_170016 = icmp ne i8* %_1, null
  br i1 %_170016, label %_170015.0, label %_170003.0
_170015.0:
  %_170051 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_170052 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_170051, i32 0, i32 3
  %_170017 = bitcast i8** %_170052 to i8*
  %_170053 = bitcast i8* %_170017 to i8**
  %_140001 = load i8*, i8** %_170053, !dereferenceable_or_null !{i64 8}
  %_170019 = icmp ne i8* %_140001, null
  br i1 %_170019, label %_170018.0, label %_170003.0
_170018.0:
  %_170054 = bitcast i8* %_140001 to i8**
  %_170020 = load i8*, i8** %_170054
  %_170055 = bitcast i8* %_170020 to { i8*, i32, i32, i8* }*
  %_170056 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_170055, i32 0, i32 2
  %_170021 = bitcast i32* %_170056 to i8*
  %_170057 = bitcast i8* %_170021 to i32*
  %_170022 = load i32, i32* %_170057
  %_170058 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_170059 = getelementptr i8*, i8** %_170058, i32 1988
  %_170023 = bitcast i8** %_170059 to i8*
  %_170060 = bitcast i8* %_170023 to i8**
  %_170061 = getelementptr i8*, i8** %_170060, i32 %_170022
  %_170024 = bitcast i8** %_170061 to i8*
  %_170062 = bitcast i8* %_170024 to i8**
  %_140003 = load i8*, i8** %_170062
  %_170063 = bitcast i8* %_140003 to i8* (i8*, i32)*
  %_140004 = call dereferenceable_or_null(8) i8* %_170063(i8* dereferenceable_or_null(8) %_140001, i32 %_2)
  %_170028 = icmp eq i8* %_140004, null
  br i1 %_170028, label %_170026.0, label %_170025.0
_170025.0:
  %_170064 = bitcast i8* %_140004 to i8**
  %_170029 = load i8*, i8** %_170064
  %_170030 = icmp eq i8* %_170029, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.ObjectArrayG4type" to i8*)
  br i1 %_170030, label %_170026.0, label %_170027.0
_170026.0:
  %_140005 = bitcast i8* %_140004 to i8*
  br label %_160000.0
_150000.0:
  %_170032 = icmp ne i8* %_1, null
  br i1 %_170032, label %_170031.0, label %_170003.0
_170031.0:
  %_170065 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_170066 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_170065, i32 0, i32 4
  %_170033 = bitcast i8** %_170066 to i8*
  %_170067 = bitcast i8* %_170033 to i8**
  %_170001 = load i8*, i8** %_170067, !dereferenceable_or_null !{i64 8}
  %_150001 = call dereferenceable_or_null(8) i8* @"_SM16java.util.ArraysD6copyOfLAL16java.lang.Object_iLAL16java.lang.Object_Eo"(i8* dereferenceable_or_null(8) %_170001, i32 %_2)
  br label %_160000.0
_160000.0:
  %_160001 = phi i8* [%_150001, %_170031.0], [%_140005, %_170026.0]
  br label %_120000.0
_120000.0:
  %_120001 = phi i8* [%_160001, %_160000.0], [%_110001, %_170009.0]
  ret i8* %_120001
_170003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_170027.0:
  %_170035 = phi i8* [%_140004, %_170025.0]
  %_170036 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM37scala.scalanative.runtime.ObjectArrayG4type" to i8*), %_170025.0]
  %_170068 = bitcast i8* %_170035 to i8**
  %_170037 = load i8*, i8** %_170068
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_170037, i8* %_170036)
  unreachable
}

define nonnull dereferenceable(32) i8* @"_SM43scala.collection.mutable.ArrayBuilder$ofRefD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-283" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM44scala.collection.Iterator$$anon$21$$Lambda$1D5applyL16java.lang.ObjectEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20006 = icmp ne i8* %_1, null
  br i1 %_20006, label %_20004.0, label %_20005.0
_20004.0:
  %_20012 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_20013 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_20012, i32 0, i32 1
  %_20007 = bitcast i8** %_20013 to i8*
  %_20014 = bitcast i8* %_20007 to i8**
  %_20001 = load i8*, i8** %_20014, !dereferenceable_or_null !{i64 24}
  %_20009 = icmp ne i8* %_1, null
  br i1 %_20009, label %_20008.0, label %_20005.0
_20008.0:
  %_20015 = bitcast i8* %_1 to { i8*, i8*, i8* }*
  %_20016 = getelementptr { i8*, i8*, i8* }, { i8*, i8*, i8* }* %_20015, i32 0, i32 2
  %_20010 = bitcast i8** %_20016 to i8*
  %_20017 = bitcast i8* %_20010 to i8**
  %_20002 = load i8*, i8** %_20017, !dereferenceable_or_null !{i64 8}
  %_20003 = call dereferenceable_or_null(8) i8* @"_SM34scala.collection.Iterator$$anon$21D17$anonfun$addOne$1L16java.lang.ObjectL25scala.collection.IteratorEPT34scala.collection.Iterator$$anon$21"(i8* dereferenceable_or_null(24) %_20001, i8* dereferenceable_or_null(8) %_20002)
  ret i8* %_20003
_20005.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD5elemsL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_30004 = icmp ne i8* %_1, null
  br i1 %_30004, label %_30002.0, label %_30003.0
_30002.0:
  %_30007 = bitcast i8* %_1 to { i8*, i32, i32, i8* }*
  %_30008 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30007, i32 0, i32 3
  %_30005 = bitcast i8** %_30008 to i8*
  %_30009 = bitcast i8* %_30005 to i8**
  %_30001 = load i8*, i8** %_30009, !dereferenceable_or_null !{i64 8}
  ret i8* %_30001
_30003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(24) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6addOneL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30003 = bitcast i8* bitcast (i64 (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D11unboxToLongL16java.lang.ObjectjEO" to i8*) to i64 (i8*, i8*)*
  %_30001 = call i64 %_30003(i8* null, i8* dereferenceable_or_null(8) %_2)
  %_30002 = call dereferenceable_or_null(24) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6addOnejL44scala.collection.mutable.ArrayBuilder$ofLongEO"(i8* dereferenceable_or_null(24) %_1, i64 %_30001)
  ret i8* %_30002
}

define dereferenceable_or_null(24) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6addOnejL44scala.collection.mutable.ArrayBuilder$ofLongEO"(i8* %_1, i64 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_80005 = icmp ne i8* %_1, null
  br i1 %_80005, label %_80003.0, label %_80004.0
_80003.0:
  %_80035 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_80036 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_80035, i32 0, i32 1
  %_80006 = bitcast i32* %_80036 to i8*
  %_80037 = bitcast i8* %_80006 to i32*
  %_40001 = load i32, i32* %_80037
  %_30002 = add i32 %_40001, 1
  call nonnull dereferenceable(8) i8* @"_SM37scala.collection.mutable.ArrayBuilderD10ensureSizeiuEO"(i8* dereferenceable_or_null(24) %_1, i32 %_30002)
  %_80009 = icmp ne i8* %_1, null
  br i1 %_80009, label %_80008.0, label %_80004.0
_80008.0:
  %_80038 = bitcast i8* %_1 to { i8*, i32, i32, i8* }*
  %_80039 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_80038, i32 0, i32 3
  %_80010 = bitcast i8** %_80039 to i8*
  %_80040 = bitcast i8* %_80010 to i8**
  %_50001 = load i8*, i8** %_80040, !dereferenceable_or_null !{i64 8}
  %_80012 = icmp ne i8* %_1, null
  br i1 %_80012, label %_80011.0, label %_80004.0
_80011.0:
  %_80041 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_80042 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_80041, i32 0, i32 1
  %_80013 = bitcast i32* %_80042 to i8*
  %_80043 = bitcast i8* %_80013 to i32*
  %_60001 = load i32, i32* %_80043
  %_80017 = icmp ne i8* %_50001, null
  br i1 %_80017, label %_80016.0, label %_80004.0
_80016.0:
  %_80044 = bitcast i8* %_50001 to { i8*, i32 }*
  %_80045 = getelementptr { i8*, i32 }, { i8*, i32 }* %_80044, i32 0, i32 1
  %_80018 = bitcast i32* %_80045 to i8*
  %_80046 = bitcast i8* %_80018 to i32*
  %_80015 = load i32, i32* %_80046
  %_80021 = icmp sge i32 %_60001, 0
  %_80022 = icmp slt i32 %_60001, %_80015
  %_80023 = and i1 %_80021, %_80022
  br i1 %_80023, label %_80019.0, label %_80020.0
_80019.0:
  %_80047 = bitcast i8* %_50001 to { i8*, i32, i32, [0 x i64] }*
  %_80048 = getelementptr { i8*, i32, i32, [0 x i64] }, { i8*, i32, i32, [0 x i64] }* %_80047, i32 0, i32 3, i32 %_60001
  %_80024 = bitcast i64* %_80048 to i8*
  %_80049 = bitcast i8* %_80024 to i64*
  store i64 %_2, i64* %_80049
  %_80026 = icmp ne i8* %_1, null
  br i1 %_80026, label %_80025.0, label %_80004.0
_80025.0:
  %_80050 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_80051 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_80050, i32 0, i32 1
  %_80027 = bitcast i32* %_80051 to i8*
  %_80052 = bitcast i8* %_80027 to i32*
  %_70001 = load i32, i32* %_80052
  %_80001 = add i32 %_70001, 1
  %_80030 = icmp ne i8* %_1, null
  br i1 %_80030, label %_80029.0, label %_80004.0
_80029.0:
  %_80053 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_80054 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_80053, i32 0, i32 1
  %_80031 = bitcast i32* %_80054 to i8*
  %_80055 = bitcast i8* %_80031 to i32*
  store i32 %_80001, i32* %_80055
  ret i8* %_1
_80004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_80020.0:
  %_80033 = phi i32 [%_60001, %_80016.0]
  %_80056 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_80056(i8* null, i32 %_80033)
  unreachable
}

define i1 @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_190004 = icmp eq i8* %_2, null
  br i1 %_190004, label %_190001.0, label %_190002.0
_190001.0:
  br label %_190003.0
_190002.0:
  %_190034 = bitcast i8* %_2 to i8**
  %_190005 = load i8*, i8** %_190034
  %_190006 = icmp eq i8* %_190005, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongG4type" to i8*)
  br label %_190003.0
_190003.0:
  %_40002 = phi i1 [%_190006, %_190002.0], [false, %_190001.0]
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_190010 = icmp eq i8* %_2, null
  br i1 %_190010, label %_190008.0, label %_190007.0
_190007.0:
  %_190035 = bitcast i8* %_2 to i8**
  %_190011 = load i8*, i8** %_190035
  %_190012 = icmp eq i8* %_190011, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongG4type" to i8*)
  br i1 %_190012, label %_190008.0, label %_190009.0
_190008.0:
  %_50001 = bitcast i8* %_2 to i8*
  %_190015 = icmp ne i8* %_1, null
  br i1 %_190015, label %_190013.0, label %_190014.0
_190013.0:
  %_190036 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_190037 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_190036, i32 0, i32 1
  %_190016 = bitcast i32* %_190037 to i8*
  %_190038 = bitcast i8* %_190016 to i32*
  %_70001 = load i32, i32* %_190038
  %_190018 = icmp ne i8* %_50001, null
  br i1 %_190018, label %_190017.0, label %_190014.0
_190017.0:
  %_190039 = bitcast i8* %_50001 to { i8*, i32, i32 }*
  %_190040 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_190039, i32 0, i32 1
  %_190019 = bitcast i32* %_190040 to i8*
  %_190041 = bitcast i8* %_190019 to i32*
  %_80001 = load i32, i32* %_190041
  %_50003 = icmp eq i32 %_70001, %_80001
  br i1 %_50003, label %_90000.0, label %_100000.0
_90000.0:
  %_190021 = icmp ne i8* %_1, null
  br i1 %_190021, label %_190020.0, label %_190014.0
_190020.0:
  %_190042 = bitcast i8* %_1 to { i8*, i32, i32, i8* }*
  %_190043 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_190042, i32 0, i32 3
  %_190022 = bitcast i8** %_190043 to i8*
  %_190044 = bitcast i8* %_190022 to i8**
  %_110001 = load i8*, i8** %_190044, !dereferenceable_or_null !{i64 8}
  %_90002 = icmp eq i8* %_110001, null
  br i1 %_90002, label %_120000.0, label %_130000.0
_120000.0:
  %_190024 = icmp ne i8* %_50001, null
  br i1 %_190024, label %_190023.0, label %_190014.0
_190023.0:
  %_190045 = bitcast i8* %_50001 to { i8*, i32, i32, i8* }*
  %_190046 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_190045, i32 0, i32 3
  %_190025 = bitcast i8** %_190046 to i8*
  %_190047 = bitcast i8* %_190025 to i8**
  %_140001 = load i8*, i8** %_190047, !dereferenceable_or_null !{i64 8}
  %_120002 = icmp eq i8* %_140001, null
  br label %_150000.0
_130000.0:
  %_190027 = icmp ne i8* %_50001, null
  br i1 %_190027, label %_190026.0, label %_190014.0
_190026.0:
  %_190048 = bitcast i8* %_50001 to { i8*, i32, i32, i8* }*
  %_190049 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_190048, i32 0, i32 3
  %_190028 = bitcast i8** %_190049 to i8*
  %_190050 = bitcast i8* %_190028 to i8**
  %_160001 = load i8*, i8** %_190050, !dereferenceable_or_null !{i64 8}
  %_130001 = call i1 @"_SM16java.lang.ObjectD12scala_$eq$eqL16java.lang.ObjectzEO"(i8* dereferenceable_or_null(8) %_110001, i8* dereferenceable_or_null(8) %_160001)
  br label %_150000.0
_150000.0:
  %_150001 = phi i1 [%_130001, %_190026.0], [%_120002, %_190023.0]
  br label %_170000.0
_100000.0:
  br label %_170000.0
_170000.0:
  %_170001 = phi i1 [false, %_100000.0], [%_150001, %_150000.0]
  br label %_180000.0
_60000.0:
  br label %_190000.0
_190000.0:
  br label %_180000.0
_180000.0:
  %_180001 = phi i1 [false, %_190000.0], [%_170001, %_170000.0]
  ret i1 %_180001
_190014.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_190009.0:
  %_190030 = phi i8* [%_2, %_190007.0]
  %_190031 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongG4type" to i8*), %_190007.0]
  %_190051 = bitcast i8* %_190030 to i8**
  %_190032 = load i8*, i8** %_190051
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_190032, i8* %_190031)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6resizeiuEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_50012 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD7mkArrayiLAj_EPT44scala.collection.mutable.ArrayBuilder$ofLong" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_50012(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  %_50005 = icmp ne i8* %_1, null
  br i1 %_50005, label %_50003.0, label %_50004.0
_50003.0:
  %_50013 = bitcast i8* %_1 to { i8*, i32, i32, i8* }*
  %_50014 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_50013, i32 0, i32 3
  %_50006 = bitcast i8** %_50014 to i8*
  %_50015 = bitcast i8* %_50006 to i8**
  store i8* %_30001, i8** %_50015
  %_50009 = icmp ne i8* %_1, null
  br i1 %_50009, label %_50008.0, label %_50004.0
_50008.0:
  %_50016 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_50017 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_50016, i32 0, i32 2
  %_50010 = bitcast i32* %_50017 to i8*
  %_50018 = bitcast i8* %_50010 to i32*
  store i32 %_2, i32* %_50018
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_50004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(8) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6resultL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(8) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6resultLAj_EO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD6resultLAj_EO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_150004 = icmp ne i8* %_1, null
  br i1 %_150004, label %_150002.0, label %_150003.0
_150002.0:
  %_150027 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_150028 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_150027, i32 0, i32 2
  %_150005 = bitcast i32* %_150028 to i8*
  %_150029 = bitcast i8* %_150005 to i32*
  %_30001 = load i32, i32* %_150029
  %_20002 = icmp ne i32 %_30001, 0
  br i1 %_20002, label %_40000.0, label %_50000.0
_40000.0:
  %_150007 = icmp ne i8* %_1, null
  br i1 %_150007, label %_150006.0, label %_150003.0
_150006.0:
  %_150030 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_150031 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_150030, i32 0, i32 2
  %_150008 = bitcast i32* %_150031 to i8*
  %_150032 = bitcast i8* %_150008 to i32*
  %_60001 = load i32, i32* %_150032
  %_150010 = icmp ne i8* %_1, null
  br i1 %_150010, label %_150009.0, label %_150003.0
_150009.0:
  %_150033 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_150034 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_150033, i32 0, i32 1
  %_150011 = bitcast i32* %_150034 to i8*
  %_150035 = bitcast i8* %_150011 to i32*
  %_70001 = load i32, i32* %_150035
  %_40002 = icmp eq i32 %_60001, %_70001
  br label %_80000.0
_50000.0:
  br label %_80000.0
_80000.0:
  %_80001 = phi i1 [false, %_50000.0], [%_40002, %_150009.0]
  br i1 %_80001, label %_90000.0, label %_100000.0
_90000.0:
  %_150014 = icmp ne i8* %_1, null
  br i1 %_150014, label %_150013.0, label %_150003.0
_150013.0:
  %_150036 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_150037 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_150036, i32 0, i32 2
  %_150015 = bitcast i32* %_150037 to i8*
  %_150038 = bitcast i8* %_150015 to i32*
  store i32 0, i32* %_150038
  %_150017 = icmp ne i8* %_1, null
  br i1 %_150017, label %_150016.0, label %_150003.0
_150016.0:
  %_150039 = bitcast i8* %_1 to { i8*, i32, i32, i8* }*
  %_150040 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_150039, i32 0, i32 3
  %_150018 = bitcast i8** %_150040 to i8*
  %_150041 = bitcast i8* %_150018 to i8**
  %_120001 = load i8*, i8** %_150041, !dereferenceable_or_null !{i64 8}
  %_150021 = icmp ne i8* %_1, null
  br i1 %_150021, label %_150020.0, label %_150003.0
_150020.0:
  %_150042 = bitcast i8* %_1 to { i8*, i32, i32, i8* }*
  %_150043 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_150042, i32 0, i32 3
  %_150022 = bitcast i8** %_150043 to i8*
  %_150044 = bitcast i8* %_150022 to i8**
  store i8* null, i8** %_150044
  br label %_140000.0
_100000.0:
  %_150024 = icmp ne i8* %_1, null
  br i1 %_150024, label %_150023.0, label %_150003.0
_150023.0:
  %_150045 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_150046 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_150045, i32 0, i32 1
  %_150025 = bitcast i32* %_150046 to i8*
  %_150047 = bitcast i8* %_150025 to i32*
  %_150001 = load i32, i32* %_150047
  %_150048 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD7mkArrayiLAj_EPT44scala.collection.mutable.ArrayBuilder$ofLong" to i8*) to i8* (i8*, i32)*
  %_100001 = call dereferenceable_or_null(8) i8* %_150048(i8* dereferenceable_or_null(24) %_1, i32 %_150001)
  br label %_140000.0
_140000.0:
  %_140001 = phi i8* [%_100001, %_150023.0], [%_120001, %_150020.0]
  ret i8* %_140001
_150003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD7mkArrayiLAj_EPT44scala.collection.mutable.ArrayBuilder$ofLong"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(8) i8* @"_SM36scala.scalanative.runtime.LongArray$D5allociL35scala.scalanative.runtime.LongArrayEO"(i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.LongArray$G8instance" to i8*), i32 %_2)
  %_90004 = icmp ne i8* %_1, null
  br i1 %_90004, label %_90002.0, label %_90003.0
_90002.0:
  %_90014 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_90015 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_90014, i32 0, i32 1
  %_90005 = bitcast i32* %_90015 to i8*
  %_90016 = bitcast i8* %_90005 to i32*
  %_40001 = load i32, i32* %_90016
  %_30003 = icmp sgt i32 %_40001, 0
  br i1 %_30003, label %_50000.0, label %_60000.0
_50000.0:
  %_50001 = call dereferenceable_or_null(80) i8* @"_SM12scala.Array$G4load"()
  %_90007 = icmp ne i8* %_1, null
  br i1 %_90007, label %_90006.0, label %_90003.0
_90006.0:
  %_90017 = bitcast i8* %_1 to { i8*, i32, i32, i8* }*
  %_90018 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_90017, i32 0, i32 3
  %_90008 = bitcast i8** %_90018 to i8*
  %_90019 = bitcast i8* %_90008 to i8**
  %_70001 = load i8*, i8** %_90019, !dereferenceable_or_null !{i64 8}
  %_90010 = icmp ne i8* %_1, null
  br i1 %_90010, label %_90009.0, label %_90003.0
_90009.0:
  %_90020 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_90021 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_90020, i32 0, i32 1
  %_90011 = bitcast i32* %_90021 to i8*
  %_90022 = bitcast i8* %_90011 to i32*
  %_80001 = load i32, i32* %_90022
  call nonnull dereferenceable(8) i8* @"_SM12scala.Array$D4copyL16java.lang.ObjectiL16java.lang.ObjectiiuEO"(i8* nonnull dereferenceable(80) %_50001, i8* dereferenceable_or_null(8) %_70001, i32 0, i8* nonnull dereferenceable(8) %_30001, i32 0, i32 %_80001)
  br label %_90000.0
_60000.0:
  br label %_90000.0
_90000.0:
  ret i8* %_30001
_90003.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(32) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofLongD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-285" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD5elemsL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = bitcast i8* bitcast (i8* (i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD5elemsLAL23scala.runtime.BoxedUnit_EO" to i8*) to i8* (i8*)*
  %_20001 = call dereferenceable_or_null(8) i8* %_20002(i8* dereferenceable_or_null(16) %_1)
  ret i8* %_20001
}

define i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD5elemsLAL23scala.runtime.BoxedUnit_EO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  br label %_110000.0
_110000.0:
  %_110001 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM39java.lang.UnsupportedOperationExceptionG4type" to i8*), i64 40)
  %_120006 = bitcast i8* %_110001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_120007 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_120006, i32 0, i32 5
  %_120002 = bitcast i1* %_120007 to i8*
  %_120008 = bitcast i8* %_120002 to i1*
  store i1 true, i1* %_120008
  %_120009 = bitcast i8* %_110001 to { i8*, i8*, i8*, i8*, i1, i1 }*
  %_120010 = getelementptr { i8*, i8*, i8*, i8*, i1, i1 }, { i8*, i8*, i8*, i8*, i1, i1 }* %_120009, i32 0, i32 4
  %_120004 = bitcast i1* %_120010 to i8*
  %_120011 = bitcast i8* %_120004 to i1*
  store i1 true, i1* %_120011
  %_110004 = call dereferenceable_or_null(40) i8* @"_SM19java.lang.ThrowableD16fillInStackTraceL19java.lang.ThrowableEO"(i8* nonnull dereferenceable(40) %_110001)
  br label %_120000.0
_120000.0:
  call i8* @"scalanative_throw"(i8* nonnull dereferenceable(40) %_110001)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6addAllL29scala.collection.IterableOnceL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(16) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6addAllL29scala.collection.IterableOnceL44scala.collection.mutable.ArrayBuilder$ofUnitEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(16) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6addAllL29scala.collection.IterableOnceL37scala.collection.mutable.ArrayBuilderEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30001 = call dereferenceable_or_null(16) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6addAllL29scala.collection.IterableOnceL44scala.collection.mutable.ArrayBuilder$ofUnitEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_2)
  ret i8* %_30001
}

define dereferenceable_or_null(16) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6addAllL29scala.collection.IterableOnceL44scala.collection.mutable.ArrayBuilder$ofUnitEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_50005 = icmp ne i8* %_1, null
  br i1 %_50005, label %_50003.0, label %_50004.0
_50003.0:
  %_50026 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_50027 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_50026, i32 0, i32 1
  %_50006 = bitcast i32* %_50027 to i8*
  %_50028 = bitcast i8* %_50006 to i32*
  %_40001 = load i32, i32* %_50028
  %_50008 = icmp ne i8* %_2, null
  br i1 %_50008, label %_50007.0, label %_50004.0
_50007.0:
  %_50029 = bitcast i8* %_2 to i8**
  %_50009 = load i8*, i8** %_50029
  %_50030 = bitcast i8* %_50009 to { i8*, i32, i32, i8* }*
  %_50031 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_50030, i32 0, i32 2
  %_50010 = bitcast i32* %_50031 to i8*
  %_50032 = bitcast i8* %_50010 to i32*
  %_50011 = load i32, i32* %_50032
  %_50033 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_50034 = getelementptr i8*, i8** %_50033, i32 446
  %_50012 = bitcast i8** %_50034 to i8*
  %_50035 = bitcast i8* %_50012 to i8**
  %_50036 = getelementptr i8*, i8** %_50035, i32 %_50011
  %_50013 = bitcast i8** %_50036 to i8*
  %_50037 = bitcast i8* %_50013 to i8**
  %_30002 = load i8*, i8** %_50037
  %_50038 = bitcast i8* %_30002 to i8* (i8*)*
  %_30003 = call dereferenceable_or_null(8) i8* %_50038(i8* dereferenceable_or_null(8) %_2)
  %_50015 = icmp ne i8* %_30003, null
  br i1 %_50015, label %_50014.0, label %_50004.0
_50014.0:
  %_50039 = bitcast i8* %_30003 to i8**
  %_50016 = load i8*, i8** %_50039
  %_50040 = bitcast i8* %_50016 to { i8*, i32, i32, i8* }*
  %_50041 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_50040, i32 0, i32 2
  %_50017 = bitcast i32* %_50041 to i8*
  %_50042 = bitcast i8* %_50017 to i32*
  %_50018 = load i32, i32* %_50042
  %_50043 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_50044 = getelementptr i8*, i8** %_50043, i32 1698
  %_50019 = bitcast i8** %_50044 to i8*
  %_50045 = bitcast i8* %_50019 to i8**
  %_50046 = getelementptr i8*, i8** %_50045, i32 %_50018
  %_50020 = bitcast i8** %_50046 to i8*
  %_50047 = bitcast i8* %_50020 to i8**
  %_30005 = load i8*, i8** %_50047
  %_50048 = bitcast i8* %_30005 to i32 (i8*)*
  %_30006 = call i32 %_50048(i8* dereferenceable_or_null(8) %_30003)
  %_50001 = add i32 %_40001, %_30006
  %_50023 = icmp ne i8* %_1, null
  br i1 %_50023, label %_50022.0, label %_50004.0
_50022.0:
  %_50049 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_50050 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_50049, i32 0, i32 1
  %_50024 = bitcast i32* %_50050 to i8*
  %_50051 = bitcast i8* %_50024 to i32*
  store i32 %_50001, i32* %_50051
  ret i8* %_1
_50004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6addOneL16java.lang.ObjectL33scala.collection.mutable.GrowableEO"(i8* %_1, i8* %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30006 = icmp eq i8* %_2, null
  br i1 %_30006, label %_30004.0, label %_30003.0
_30003.0:
  %_30017 = bitcast i8* %_2 to i8**
  %_30007 = load i8*, i8** %_30017
  %_30018 = bitcast i8* %_30007 to { i8*, i32, i32, i8* }*
  %_30019 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_30018, i32 0, i32 1
  %_30008 = bitcast i32* %_30019 to i8*
  %_30020 = bitcast i8* %_30008 to i32*
  %_30009 = load i32, i32* %_30020
  %_30010 = icmp sle i32 222, %_30009
  %_30011 = icmp sle i32 %_30009, 223
  %_30012 = and i1 %_30010, %_30011
  br i1 %_30012, label %_30004.0, label %_30005.0
_30004.0:
  %_30001 = bitcast i8* %_2 to i8*
  %_30002 = call dereferenceable_or_null(16) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6addOneL23scala.runtime.BoxedUnitL44scala.collection.mutable.ArrayBuilder$ofUnitEO"(i8* dereferenceable_or_null(16) %_1, i8* dereferenceable_or_null(8) %_30001)
  ret i8* %_30002
_30005.0:
  %_30013 = phi i8* [%_2, %_30003.0]
  %_30014 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM23scala.runtime.BoxedUnitG4type" to i8*), %_30003.0]
  %_30021 = bitcast i8* %_30013 to i8**
  %_30015 = load i8*, i8** %_30021
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_30015, i8* %_30014)
  unreachable
}

define dereferenceable_or_null(16) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6addOneL23scala.runtime.BoxedUnitL44scala.collection.mutable.ArrayBuilder$ofUnitEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_50005 = icmp ne i8* %_1, null
  br i1 %_50005, label %_50003.0, label %_50004.0
_50003.0:
  %_50012 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_50013 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_50012, i32 0, i32 1
  %_50006 = bitcast i32* %_50013 to i8*
  %_50014 = bitcast i8* %_50006 to i32*
  %_40001 = load i32, i32* %_50014
  %_50001 = add i32 %_40001, 1
  %_50009 = icmp ne i8* %_1, null
  br i1 %_50009, label %_50008.0, label %_50004.0
_50008.0:
  %_50015 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_50016 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_50015, i32 0, i32 1
  %_50010 = bitcast i32* %_50016 to i8*
  %_50017 = bitcast i8* %_50010 to i32*
  store i32 %_50001, i32* %_50017
  ret i8* %_1
_50004.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6equalsL16java.lang.ObjectzEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  br label %_40000.0
_40000.0:
  %_100004 = icmp eq i8* %_2, null
  br i1 %_100004, label %_100001.0, label %_100002.0
_100001.0:
  br label %_100003.0
_100002.0:
  %_100025 = bitcast i8* %_2 to i8**
  %_100005 = load i8*, i8** %_100025
  %_100006 = icmp eq i8* %_100005, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitG4type" to i8*)
  br label %_100003.0
_100003.0:
  %_40002 = phi i1 [%_100006, %_100002.0], [false, %_100001.0]
  br i1 %_40002, label %_50000.0, label %_60000.0
_50000.0:
  %_100010 = icmp eq i8* %_2, null
  br i1 %_100010, label %_100008.0, label %_100007.0
_100007.0:
  %_100026 = bitcast i8* %_2 to i8**
  %_100011 = load i8*, i8** %_100026
  %_100012 = icmp eq i8* %_100011, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitG4type" to i8*)
  br i1 %_100012, label %_100008.0, label %_100009.0
_100008.0:
  %_50001 = bitcast i8* %_2 to i8*
  %_100015 = icmp ne i8* %_1, null
  br i1 %_100015, label %_100013.0, label %_100014.0
_100013.0:
  %_100027 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_100028 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_100027, i32 0, i32 1
  %_100016 = bitcast i32* %_100028 to i8*
  %_100029 = bitcast i8* %_100016 to i32*
  %_70001 = load i32, i32* %_100029
  %_100018 = icmp ne i8* %_50001, null
  br i1 %_100018, label %_100017.0, label %_100014.0
_100017.0:
  %_100030 = bitcast i8* %_50001 to { i8*, i32, i32 }*
  %_100031 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_100030, i32 0, i32 1
  %_100019 = bitcast i32* %_100031 to i8*
  %_100032 = bitcast i8* %_100019 to i32*
  %_80001 = load i32, i32* %_100032
  %_50003 = icmp eq i32 %_70001, %_80001
  br label %_90000.0
_60000.0:
  br label %_100000.0
_100000.0:
  br label %_90000.0
_90000.0:
  %_90001 = phi i1 [false, %_100000.0], [%_50003, %_100017.0]
  ret i1 %_90001
_100014.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_100009.0:
  %_100021 = phi i8* [%_2, %_100007.0]
  %_100022 = phi i8* [bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [8 x i8*] }* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitG4type" to i8*), %_100007.0]
  %_100033 = bitcast i8* %_100021 to i8**
  %_100023 = load i8*, i8** %_100033
  call i8* @"_SM34scala.scalanative.runtime.package$D14throwClassCastR_R_nEO"(i8* null, i8* %_100023, i8* %_100022)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6resizeiuEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define dereferenceable_or_null(8) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6resultL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20002 = bitcast i8* bitcast (i8* (i8*)* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6resultLAL23scala.runtime.BoxedUnit_EO" to i8*) to i8* (i8*)*
  %_20001 = call dereferenceable_or_null(8) i8* %_20002(i8* dereferenceable_or_null(16) %_1)
  ret i8* %_20001
}

define nonnull dereferenceable(8) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD6resultLAL23scala.runtime.BoxedUnit_EO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_90003 = icmp ne i8* %_1, null
  br i1 %_90003, label %_90001.0, label %_90002.0
_90001.0:
  %_90021 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_90022 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_90021, i32 0, i32 1
  %_90004 = bitcast i32* %_90022 to i8*
  %_90023 = bitcast i8* %_90004 to i32*
  %_30001 = load i32, i32* %_90023
  %_20002 = call dereferenceable_or_null(8) i8* @"_SM38scala.scalanative.runtime.ObjectArray$D5allociL37scala.scalanative.runtime.ObjectArrayEO"(i8* bitcast ({ i8* }* @"_SM38scala.scalanative.runtime.ObjectArray$G8instance" to i8*), i32 %_30001)
  br label %_40000.0
_40000.0:
  %_40001 = phi i32 [0, %_90001.0], [%_60003, %_90012.0]
  %_90007 = icmp ne i8* %_1, null
  br i1 %_90007, label %_90006.0, label %_90002.0
_90006.0:
  %_90024 = bitcast i8* %_1 to { i8*, i32, i32 }*
  %_90025 = getelementptr { i8*, i32, i32 }, { i8*, i32, i32 }* %_90024, i32 0, i32 1
  %_90008 = bitcast i32* %_90025 to i8*
  %_90026 = bitcast i8* %_90008 to i32*
  %_80001 = load i32, i32* %_90026
  %_40003 = icmp slt i32 %_40001, %_80001
  br i1 %_40003, label %_60000.0, label %_70000.0
_60000.0:
  %_90027 = bitcast i8* %_20002 to { i8*, i32 }*
  %_90028 = getelementptr { i8*, i32 }, { i8*, i32 }* %_90027, i32 0, i32 1
  %_90011 = bitcast i32* %_90028 to i8*
  %_90029 = bitcast i8* %_90011 to i32*
  %_90010 = load i32, i32* %_90029
  %_90014 = icmp sge i32 %_40001, 0
  %_90015 = icmp slt i32 %_40001, %_90010
  %_90016 = and i1 %_90014, %_90015
  br i1 %_90016, label %_90012.0, label %_90013.0
_90012.0:
  %_90030 = bitcast i8* %_20002 to { i8*, i32, i32, [0 x i8*] }*
  %_90031 = getelementptr { i8*, i32, i32, [0 x i8*] }, { i8*, i32, i32, [0 x i8*] }* %_90030, i32 0, i32 3, i32 %_40001
  %_90017 = bitcast i8** %_90031 to i8*
  %_90032 = bitcast i8* %_90017 to i8**
  store i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*), i8** %_90032
  %_60003 = add i32 %_40001, 1
  br label %_40000.0
_70000.0:
  br label %_90000.0
_90000.0:
  ret i8* %_20002
_90002.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_90013.0:
  %_90019 = phi i32 [%_40001, %_60000.0]
  %_90033 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  call i8* %_90033(i8* null, i32 %_90019)
  unreachable
}

define nonnull dereferenceable(32) i8* @"_SM44scala.collection.mutable.ArrayBuilder$ofUnitD8toStringL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-287" to i8*)
}

define dereferenceable_or_null(24) i8* @"_SM44scala.reflect.ManifestFactory$ShortManifest$G4load"() noinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_1.0:
  %_12 = bitcast i8* bitcast ([194 x i8*]* @"__modules" to i8*) to i8**
  %_13 = getelementptr i8*, i8** %_12, i32 76
  %_4 = bitcast i8** %_13 to i8*
  %_14 = bitcast i8* %_4 to i8**
  %_5 = load i8*, i8** %_14, !dereferenceable_or_null !{i64 24}
  %_6 = icmp ne i8* %_5, null
  br i1 %_6, label %_2.0, label %_3.0
_2.0:
  ret i8* %_5
_3.0:
  %_7 = call i8* @"scalanative_alloc_small"(i8* bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [4 x i8*] }* @"_SM44scala.reflect.ManifestFactory$ShortManifest$G4type" to i8*), i64 24)
  %_15 = bitcast i8* %_4 to i8**
  store i8* %_7, i8** %_15
  call nonnull dereferenceable(8) i8* @"_SM44scala.reflect.ManifestFactory$ShortManifest$RE"(i8* dereferenceable_or_null(24) %_7)
  ret i8* %_7
}

define nonnull dereferenceable(8) i8* @"_SM44scala.reflect.ManifestFactory$ShortManifest$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_40010 = icmp ne i8* %_1, null
  br i1 %_40010, label %_40008.0, label %_40009.0
_40008.0:
  %_40020 = bitcast i8* %_1 to { i8*, i32, i8* }*
  %_40021 = getelementptr { i8*, i32, i8* }, { i8*, i32, i8* }* %_40020, i32 0, i32 2
  %_40011 = bitcast i8** %_40021 to i8*
  %_40022 = bitcast i8* %_40011 to i8**
  store i8* bitcast ({ i8*, i8*, i32, i32, i32 }* @"_SM7__constG3-289" to i8*), i8** %_40022
  call nonnull dereferenceable(8) i8* @"_SM41scala.reflect.ClassManifestDeprecatedApisD6$init$uEO"(i8* dereferenceable_or_null(24) %_1)
  call nonnull dereferenceable(8) i8* @"_SM22scala.reflect.ClassTagD6$init$uEO"(i8* dereferenceable_or_null(24) %_1)
  call nonnull dereferenceable(8) i8* @"_SM22scala.reflect.ManifestD6$init$uEO"(i8* dereferenceable_or_null(24) %_1)
  %_40005 = call i32 @"_SM16java.lang.SystemD16identityHashCodeL16java.lang.ObjectiEo"(i8* dereferenceable_or_null(24) %_1)
  %_40017 = icmp ne i8* %_1, null
  br i1 %_40017, label %_40016.0, label %_40009.0
_40016.0:
  %_40023 = bitcast i8* %_1 to { i8*, i32, i8* }*
  %_40024 = getelementptr { i8*, i32, i8* }, { i8*, i32, i8* }* %_40023, i32 0, i32 1
  %_40018 = bitcast i32* %_40024 to i8*
  %_40025 = bitcast i8* %_40018 to i32*
  store i32 %_40005, i32* %_40025
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_40009.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define double @"_SM46scala.collection.ArrayOps$ArrayIterator$mcD$spD11next$mcD$spdEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_2.0:
  br label %_5.0
_5.0:
  %_36 = icmp ne i8* %_1, null
  br i1 %_36, label %_33.0, label %_34.0
_33.0:
  %_119 = bitcast i8* %_1 to { i8*, i32, i32, i8*, i8* }*
  %_120 = getelementptr { i8*, i32, i32, i8*, i8* }, { i8*, i32, i32, i8*, i8* }* %_119, i32 0, i32 4
  %_38 = bitcast i8** %_120 to i8*
  %_121 = bitcast i8* %_38 to i8**
  %_10 = load i8*, i8** %_121, !dereferenceable_or_null !{i64 8}
  %_44 = icmp ne i8* %_1, null
  br i1 %_44, label %_41.0, label %_42.0
_41.0:
  %_122 = bitcast i8* %_1 to { i8*, i32, i32, i8* }*
  %_123 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_122, i32 0, i32 2
  %_46 = bitcast i32* %_123 to i8*
  %_124 = bitcast i8* %_46 to i32*
  %_12 = load i32, i32* %_124
  %_53 = icmp ne i8* %_10, null
  br i1 %_53, label %_50.0, label %_51.0
_50.0:
  %_125 = bitcast i8* %_10 to { i8*, i32 }*
  %_126 = getelementptr { i8*, i32 }, { i8*, i32 }* %_125, i32 0, i32 1
  %_55 = bitcast i32* %_126 to i8*
  %_127 = bitcast i8* %_55 to i32*
  %_49 = load i32, i32* %_127
  %_60 = icmp sge i32 %_12, 0
  %_62 = icmp slt i32 %_12, %_49
  %_64 = and i1 %_60, %_62
  br i1 %_64, label %_57.0, label %_58.0
_57.0:
  %_128 = bitcast i8* %_10 to { i8*, i32, i32, [0 x double] }*
  %_129 = getelementptr { i8*, i32, i32, [0 x double] }, { i8*, i32, i32, [0 x double] }* %_128, i32 0, i32 3, i32 %_12
  %_66 = bitcast double* %_129 to i8*
  %_130 = bitcast i8* %_66 to double*
  %_14 = load double, double* %_130
  %_72 = icmp ne i8* %_1, null
  br i1 %_72, label %_69.0, label %_70.0
_69.0:
  %_131 = bitcast i8* %_1 to { i8*, i32, i32, i8* }*
  %_132 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_131, i32 0, i32 2
  %_74 = bitcast i32* %_132 to i8*
  %_133 = bitcast i8* %_74 to i32*
  %_16 = load i32, i32* %_133
  %_18 = add i32 %_16, 1
  %_83 = icmp ne i8* %_1, null
  br i1 %_83, label %_80.0, label %_81.0
_80.0:
  %_134 = bitcast i8* %_1 to { i8*, i32, i32, i8* }*
  %_135 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_134, i32 0, i32 2
  %_85 = bitcast i32* %_135 to i8*
  %_136 = bitcast i8* %_85 to i32*
  store i32 %_18, i32* %_136
  br label %_6.0
_3.0:
  %_7 = phi i8* [%_19, %_78.0], [%_17, %_76.0], [%_15, %_68.0], [%_13, %_48.0], [%_11, %_40.0], [%_9, %_32.0]
  %_91 = icmp eq i8* %_7, null
  br i1 %_91, label %_88.0, label %_89.0
_88.0:
  br label %_90.0
_89.0:
  %_137 = bitcast i8* %_7 to i8**
  %_92 = load i8*, i8** %_137
  %_93 = icmp eq i8* %_92, bitcast ({ { i8*, i32, i32, i8* }, i32, i32, { i8* }, [5 x i8*] }* @"_SM40java.lang.ArrayIndexOutOfBoundsExceptionG4type" to i8*)
  br label %_90.0
_90.0:
  %_21 = phi i1 [%_93, %_89.0], [false, %_88.0]
  br i1 %_21, label %_22.0, label %_23.0
_22.0:
  %_26 = call dereferenceable_or_null(16) i8* @"_SM26scala.collection.Iterator$G4load"()
  %_138 = bitcast i8* bitcast (i8* (i8*)* @"_SM26scala.collection.Iterator$D5emptyL25scala.collection.IteratorEO" to i8*) to i8* (i8*)*
  %_28 = call dereferenceable_or_null(8) i8* %_138(i8* nonnull dereferenceable(16) %_26)
  %_96 = icmp ne i8* %_28, null
  br i1 %_96, label %_94.0, label %_95.0
_94.0:
  %_139 = bitcast i8* %_28 to i8**
  %_97 = load i8*, i8** %_139
  %_140 = bitcast i8* %_97 to { i8*, i32, i32, i8* }*
  %_141 = getelementptr { i8*, i32, i32, i8* }, { i8*, i32, i32, i8* }* %_140, i32 0, i32 2
  %_98 = bitcast i32* %_141 to i8*
  %_142 = bitcast i8* %_98 to i32*
  %_99 = load i32, i32* %_142
  %_143 = bitcast i8* bitcast ([4582 x i8*]* @"_ST10__dispatch" to i8*) to i8**
  %_144 = getelementptr i8*, i8** %_143, i32 4219
  %_100 = bitcast i8** %_144 to i8*
  %_145 = bitcast i8* %_100 to i8**
  %_146 = getelementptr i8*, i8** %_145, i32 %_99
  %_101 = bitcast i8** %_146 to i8*
  %_147 = bitcast i8* %_101 to i8**
  %_29 = load i8*, i8** %_147
  %_148 = bitcast i8* %_29 to i8* (i8*)*
  %_30 = call dereferenceable_or_null(8) i8* %_148(i8* dereferenceable_or_null(8) %_28)
  %_149 = bitcast i8* bitcast (double (i8*, i8*)* @"_SM27scala.runtime.BoxesRunTime$D13unboxToDoubleL16java.lang.ObjectdEO" to i8*) to double (i8*, i8*)*
  %_31 = call double %_149(i8* null, i8* dereferenceable_or_null(8) %_30)
  br label %_6.0
_23.0:
  %_103 = icmp ne i8* %_7, null
  br i1 %_103, label %_102.0, label %_95.0
_102.0:
  call i8* @"scalanative_throw"(i8* dereferenceable_or_null(8) %_7)
  unreachable
_6.0:
  %_8 = phi double [%_14, %_80.0], [%_31, %_94.0]
  ret double %_8
_34.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_34.1 unwind label %_105.landingpad
_34.1:
  unreachable
_42.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_42.1 unwind label %_107.landingpad
_42.1:
  unreachable
_51.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_51.1 unwind label %_109.landingpad
_51.1:
  unreachable
_70.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_70.1 unwind label %_111.landingpad
_70.1:
  unreachable
_81.0:
  invoke i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null) to label %_81.1 unwind label %_113.landingpad
_81.1:
  unreachable
_95.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
_58.0:
  %_116 = phi i32 [%_12, %_50.0]
  %_150 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM34scala.scalanative.runtime.package$D16throwOutOfBoundsinEO" to i8*) to i8* (i8*, i32)*
  invoke i8* %_150(i8* null, i32 %_116) to label %_58.1 unwind label %_117.landingpad
_58.1:
  unreachable
_32.0:
  %_9 = phi i8* [%_35, %_35.landingpad.succ], [%_105, %_105.landingpad.succ], [%_37, %_37.landingpad.succ], [%_39, %_39.landingpad.succ]
  br label %_3.0
_40.0:
  %_11 = phi i8* [%_43, %_43.landingpad.succ], [%_107, %_107.landingpad.succ], [%_45, %_45.landingpad.succ], [%_47, %_47.landingpad.succ]
  br label %_3.0
_48.0:
  %_13 = phi i8* [%_52, %_52.landingpad.succ], [%_109, %_109.landingpad.succ], [%_54, %_54.landingpad.succ], [%_56, %_56.landingpad.succ], [%_59, %_59.landingpad.succ], [%_61, %_61.landingpad.succ], [%_63, %_63.landingpad.succ], [%_117, %_117.landingpad.succ], [%_65, %_65.landingpad.succ], [%_67, %_67.landingpad.succ]
  br label %_3.0
_68.0:
  %_15 = phi i8* [%_71, %_71.landingpad.succ], [%_111, %_111.landingpad.succ], [%_73, %_73.landingpad.succ], [%_75, %_75.landingpad.succ]
  br label %_3.0
_76.0:
  %_17 = phi i8* [%_77, %_77.landingpad.succ]
  br label %_3.0
_78.0:
  %_19 = phi i8* [%_82, %_82.landingpad.succ], [%_113, %_113.landingpad.succ], [%_84, %_84.landingpad.succ], [%_86, %_86.landingpad.succ]
  br label %_3.0
_35.landingpad:
  %_151 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_152 = extractvalue { i8*, i32 } %_151, 0
  %_153 = extractvalue { i8*, i32 } %_151, 1
  %_154 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_155 = icmp eq i32 %_153, %_154
  br i1 %_155, label %_35.landingpad.succ, label %_35.landingpad.fail
_35.landingpad.succ:
  %_156 = call i8* @__cxa_begin_catch(i8* %_152)
  %_157 = bitcast i8* %_156 to i8**
  %_158 = getelementptr i8*, i8** %_157, i32 1
  %_35 = load i8*, i8** %_158
  call void @__cxa_end_catch()
  br label %_32.0
_35.landingpad.fail:
  resume { i8*, i32 } %_151
_37.landingpad:
  %_159 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_160 = extractvalue { i8*, i32 } %_159, 0
  %_161 = extractvalue { i8*, i32 } %_159, 1
  %_162 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_163 = icmp eq i32 %_161, %_162
  br i1 %_163, label %_37.landingpad.succ, label %_37.landingpad.fail
_37.landingpad.succ:
  %_164 = call i8* @__cxa_begin_catch(i8* %_160)
  %_165 = bitcast i8* %_164 to i8**
  %_166 = getelementptr i8*, i8** %_165, i32 1
  %_37 = load i8*, i8** %_166
  call void @__cxa_end_catch()
  br label %_32.0
_37.landingpad.fail:
  resume { i8*, i32 } %_159
_39.landingpad:
  %_167 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_168 = extractvalue { i8*, i32 } %_167, 0
  %_169 = extractvalue { i8*, i32 } %_167, 1
  %_170 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_171 = icmp eq i32 %_169, %_170
  br i1 %_171, label %_39.landingpad.succ, label %_39.landingpad.fail
_39.landingpad.succ:
  %_172 = call i8* @__cxa_begin_catch(i8* %_168)
  %_173 = bitcast i8* %_172 to i8**
  %_174 = getelementptr i8*, i8** %_173, i32 1
  %_39 = load i8*, i8** %_174
  call void @__cxa_end_catch()
  br label %_32.0
_39.landingpad.fail:
  resume { i8*, i32 } %_167
_43.landingpad:
  %_175 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_176 = extractvalue { i8*, i32 } %_175, 0
  %_177 = extractvalue { i8*, i32 } %_175, 1
  %_178 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_179 = icmp eq i32 %_177, %_178
  br i1 %_179, label %_43.landingpad.succ, label %_43.landingpad.fail
_43.landingpad.succ:
  %_180 = call i8* @__cxa_begin_catch(i8* %_176)
  %_181 = bitcast i8* %_180 to i8**
  %_182 = getelementptr i8*, i8** %_181, i32 1
  %_43 = load i8*, i8** %_182
  call void @__cxa_end_catch()
  br label %_40.0
_43.landingpad.fail:
  resume { i8*, i32 } %_175
_45.landingpad:
  %_183 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_184 = extractvalue { i8*, i32 } %_183, 0
  %_185 = extractvalue { i8*, i32 } %_183, 1
  %_186 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_187 = icmp eq i32 %_185, %_186
  br i1 %_187, label %_45.landingpad.succ, label %_45.landingpad.fail
_45.landingpad.succ:
  %_188 = call i8* @__cxa_begin_catch(i8* %_184)
  %_189 = bitcast i8* %_188 to i8**
  %_190 = getelementptr i8*, i8** %_189, i32 1
  %_45 = load i8*, i8** %_190
  call void @__cxa_end_catch()
  br label %_40.0
_45.landingpad.fail:
  resume { i8*, i32 } %_183
_47.landingpad:
  %_191 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_192 = extractvalue { i8*, i32 } %_191, 0
  %_193 = extractvalue { i8*, i32 } %_191, 1
  %_194 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_195 = icmp eq i32 %_193, %_194
  br i1 %_195, label %_47.landingpad.succ, label %_47.landingpad.fail
_47.landingpad.succ:
  %_196 = call i8* @__cxa_begin_catch(i8* %_192)
  %_197 = bitcast i8* %_196 to i8**
  %_198 = getelementptr i8*, i8** %_197, i32 1
  %_47 = load i8*, i8** %_198
  call void @__cxa_end_catch()
  br label %_40.0
_47.landingpad.fail:
  resume { i8*, i32 } %_191
_52.landingpad:
  %_199 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_200 = extractvalue { i8*, i32 } %_199, 0
  %_201 = extractvalue { i8*, i32 } %_199, 1
  %_202 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_203 = icmp eq i32 %_201, %_202
  br i1 %_203, label %_52.landingpad.succ, label %_52.landingpad.fail
_52.landingpad.succ:
  %_204 = call i8* @__cxa_begin_catch(i8* %_200)
  %_205 = bitcast i8* %_204 to i8**
  %_206 = getelementptr i8*, i8** %_205, i32 1
  %_52 = load i8*, i8** %_206
  call void @__cxa_end_catch()
  br label %_48.0
_52.landingpad.fail:
  resume { i8*, i32 } %_199
_54.landingpad:
  %_207 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_208 = extractvalue { i8*, i32 } %_207, 0
  %_209 = extractvalue { i8*, i32 } %_207, 1
  %_210 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_211 = icmp eq i32 %_209, %_210
  br i1 %_211, label %_54.landingpad.succ, label %_54.landingpad.fail
_54.landingpad.succ:
  %_212 = call i8* @__cxa_begin_catch(i8* %_208)
  %_213 = bitcast i8* %_212 to i8**
  %_214 = getelementptr i8*, i8** %_213, i32 1
  %_54 = load i8*, i8** %_214
  call void @__cxa_end_catch()
  br label %_48.0
_54.landingpad.fail:
  resume { i8*, i32 } %_207
_56.landingpad:
  %_215 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_216 = extractvalue { i8*, i32 } %_215, 0
  %_217 = extractvalue { i8*, i32 } %_215, 1
  %_218 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_219 = icmp eq i32 %_217, %_218
  br i1 %_219, label %_56.landingpad.succ, label %_56.landingpad.fail
_56.landingpad.succ:
  %_220 = call i8* @__cxa_begin_catch(i8* %_216)
  %_221 = bitcast i8* %_220 to i8**
  %_222 = getelementptr i8*, i8** %_221, i32 1
  %_56 = load i8*, i8** %_222
  call void @__cxa_end_catch()
  br label %_48.0
_56.landingpad.fail:
  resume { i8*, i32 } %_215
_59.landingpad:
  %_223 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_224 = extractvalue { i8*, i32 } %_223, 0
  %_225 = extractvalue { i8*, i32 } %_223, 1
  %_226 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_227 = icmp eq i32 %_225, %_226
  br i1 %_227, label %_59.landingpad.succ, label %_59.landingpad.fail
_59.landingpad.succ:
  %_228 = call i8* @__cxa_begin_catch(i8* %_224)
  %_229 = bitcast i8* %_228 to i8**
  %_230 = getelementptr i8*, i8** %_229, i32 1
  %_59 = load i8*, i8** %_230
  call void @__cxa_end_catch()
  br label %_48.0
_59.landingpad.fail:
  resume { i8*, i32 } %_223
_61.landingpad:
  %_231 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_232 = extractvalue { i8*, i32 } %_231, 0
  %_233 = extractvalue { i8*, i32 } %_231, 1
  %_234 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_235 = icmp eq i32 %_233, %_234
  br i1 %_235, label %_61.landingpad.succ, label %_61.landingpad.fail
_61.landingpad.succ:
  %_236 = call i8* @__cxa_begin_catch(i8* %_232)
  %_237 = bitcast i8* %_236 to i8**
  %_238 = getelementptr i8*, i8** %_237, i32 1
  %_61 = load i8*, i8** %_238
  call void @__cxa_end_catch()
  br label %_48.0
_61.landingpad.fail:
  resume { i8*, i32 } %_231
_63.landingpad:
  %_239 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_240 = extractvalue { i8*, i32 } %_239, 0
  %_241 = extractvalue { i8*, i32 } %_239, 1
  %_242 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_243 = icmp eq i32 %_241, %_242
  br i1 %_243, label %_63.landingpad.succ, label %_63.landingpad.fail
_63.landingpad.succ:
  %_244 = call i8* @__cxa_begin_catch(i8* %_240)
  %_245 = bitcast i8* %_244 to i8**
  %_246 = getelementptr i8*, i8** %_245, i32 1
  %_63 = load i8*, i8** %_246
  call void @__cxa_end_catch()
  br label %_48.0
_63.landingpad.fail:
  resume { i8*, i32 } %_239
_65.landingpad:
  %_247 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_248 = extractvalue { i8*, i32 } %_247, 0
  %_249 = extractvalue { i8*, i32 } %_247, 1
  %_250 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_251 = icmp eq i32 %_249, %_250
  br i1 %_251, label %_65.landingpad.succ, label %_65.landingpad.fail
_65.landingpad.succ:
  %_252 = call i8* @__cxa_begin_catch(i8* %_248)
  %_253 = bitcast i8* %_252 to i8**
  %_254 = getelementptr i8*, i8** %_253, i32 1
  %_65 = load i8*, i8** %_254
  call void @__cxa_end_catch()
  br label %_48.0
_65.landingpad.fail:
  resume { i8*, i32 } %_247
_67.landingpad:
  %_255 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_256 = extractvalue { i8*, i32 } %_255, 0
  %_257 = extractvalue { i8*, i32 } %_255, 1
  %_258 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_259 = icmp eq i32 %_257, %_258
  br i1 %_259, label %_67.landingpad.succ, label %_67.landingpad.fail
_67.landingpad.succ:
  %_260 = call i8* @__cxa_begin_catch(i8* %_256)
  %_261 = bitcast i8* %_260 to i8**
  %_262 = getelementptr i8*, i8** %_261, i32 1
  %_67 = load i8*, i8** %_262
  call void @__cxa_end_catch()
  br label %_48.0
_67.landingpad.fail:
  resume { i8*, i32 } %_255
_71.landingpad:
  %_263 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_264 = extractvalue { i8*, i32 } %_263, 0
  %_265 = extractvalue { i8*, i32 } %_263, 1
  %_266 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_267 = icmp eq i32 %_265, %_266
  br i1 %_267, label %_71.landingpad.succ, label %_71.landingpad.fail
_71.landingpad.succ:
  %_268 = call i8* @__cxa_begin_catch(i8* %_264)
  %_269 = bitcast i8* %_268 to i8**
  %_270 = getelementptr i8*, i8** %_269, i32 1
  %_71 = load i8*, i8** %_270
  call void @__cxa_end_catch()
  br label %_68.0
_71.landingpad.fail:
  resume { i8*, i32 } %_263
_73.landingpad:
  %_271 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_272 = extractvalue { i8*, i32 } %_271, 0
  %_273 = extractvalue { i8*, i32 } %_271, 1
  %_274 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_275 = icmp eq i32 %_273, %_274
  br i1 %_275, label %_73.landingpad.succ, label %_73.landingpad.fail
_73.landingpad.succ:
  %_276 = call i8* @__cxa_begin_catch(i8* %_272)
  %_277 = bitcast i8* %_276 to i8**
  %_278 = getelementptr i8*, i8** %_277, i32 1
  %_73 = load i8*, i8** %_278
  call void @__cxa_end_catch()
  br label %_68.0
_73.landingpad.fail:
  resume { i8*, i32 } %_271
_75.landingpad:
  %_279 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_280 = extractvalue { i8*, i32 } %_279, 0
  %_281 = extractvalue { i8*, i32 } %_279, 1
  %_282 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_283 = icmp eq i32 %_281, %_282
  br i1 %_283, label %_75.landingpad.succ, label %_75.landingpad.fail
_75.landingpad.succ:
  %_284 = call i8* @__cxa_begin_catch(i8* %_280)
  %_285 = bitcast i8* %_284 to i8**
  %_286 = getelementptr i8*, i8** %_285, i32 1
  %_75 = load i8*, i8** %_286
  call void @__cxa_end_catch()
  br label %_68.0
_75.landingpad.fail:
  resume { i8*, i32 } %_279
_77.landingpad:
  %_287 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_288 = extractvalue { i8*, i32 } %_287, 0
  %_289 = extractvalue { i8*, i32 } %_287, 1
  %_290 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_291 = icmp eq i32 %_289, %_290
  br i1 %_291, label %_77.landingpad.succ, label %_77.landingpad.fail
_77.landingpad.succ:
  %_292 = call i8* @__cxa_begin_catch(i8* %_288)
  %_293 = bitcast i8* %_292 to i8**
  %_294 = getelementptr i8*, i8** %_293, i32 1
  %_77 = load i8*, i8** %_294
  call void @__cxa_end_catch()
  br label %_76.0
_77.landingpad.fail:
  resume { i8*, i32 } %_287
_82.landingpad:
  %_295 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_296 = extractvalue { i8*, i32 } %_295, 0
  %_297 = extractvalue { i8*, i32 } %_295, 1
  %_298 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_299 = icmp eq i32 %_297, %_298
  br i1 %_299, label %_82.landingpad.succ, label %_82.landingpad.fail
_82.landingpad.succ:
  %_300 = call i8* @__cxa_begin_catch(i8* %_296)
  %_301 = bitcast i8* %_300 to i8**
  %_302 = getelementptr i8*, i8** %_301, i32 1
  %_82 = load i8*, i8** %_302
  call void @__cxa_end_catch()
  br label %_78.0
_82.landingpad.fail:
  resume { i8*, i32 } %_295
_84.landingpad:
  %_303 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_304 = extractvalue { i8*, i32 } %_303, 0
  %_305 = extractvalue { i8*, i32 } %_303, 1
  %_306 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_307 = icmp eq i32 %_305, %_306
  br i1 %_307, label %_84.landingpad.succ, label %_84.landingpad.fail
_84.landingpad.succ:
  %_308 = call i8* @__cxa_begin_catch(i8* %_304)
  %_309 = bitcast i8* %_308 to i8**
  %_310 = getelementptr i8*, i8** %_309, i32 1
  %_84 = load i8*, i8** %_310
  call void @__cxa_end_catch()
  br label %_78.0
_84.landingpad.fail:
  resume { i8*, i32 } %_303
_86.landingpad:
  %_311 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_312 = extractvalue { i8*, i32 } %_311, 0
  %_313 = extractvalue { i8*, i32 } %_311, 1
  %_314 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_315 = icmp eq i32 %_313, %_314
  br i1 %_315, label %_86.landingpad.succ, label %_86.landingpad.fail
_86.landingpad.succ:
  %_316 = call i8* @__cxa_begin_catch(i8* %_312)
  %_317 = bitcast i8* %_316 to i8**
  %_318 = getelementptr i8*, i8** %_317, i32 1
  %_86 = load i8*, i8** %_318
  call void @__cxa_end_catch()
  br label %_78.0
_86.landingpad.fail:
  resume { i8*, i32 } %_311
_105.landingpad:
  %_319 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_320 = extractvalue { i8*, i32 } %_319, 0
  %_321 = extractvalue { i8*, i32 } %_319, 1
  %_322 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_323 = icmp eq i32 %_321, %_322
  br i1 %_323, label %_105.landingpad.succ, label %_105.landingpad.fail
_105.landingpad.succ:
  %_324 = call i8* @__cxa_begin_catch(i8* %_320)
  %_325 = bitcast i8* %_324 to i8**
  %_326 = getelementptr i8*, i8** %_325, i32 1
  %_105 = load i8*, i8** %_326
  call void @__cxa_end_catch()
  br label %_32.0
_105.landingpad.fail:
  resume { i8*, i32 } %_319
_107.landingpad:
  %_327 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_328 = extractvalue { i8*, i32 } %_327, 0
  %_329 = extractvalue { i8*, i32 } %_327, 1
  %_330 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_331 = icmp eq i32 %_329, %_330
  br i1 %_331, label %_107.landingpad.succ, label %_107.landingpad.fail
_107.landingpad.succ:
  %_332 = call i8* @__cxa_begin_catch(i8* %_328)
  %_333 = bitcast i8* %_332 to i8**
  %_334 = getelementptr i8*, i8** %_333, i32 1
  %_107 = load i8*, i8** %_334
  call void @__cxa_end_catch()
  br label %_40.0
_107.landingpad.fail:
  resume { i8*, i32 } %_327
_109.landingpad:
  %_335 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_336 = extractvalue { i8*, i32 } %_335, 0
  %_337 = extractvalue { i8*, i32 } %_335, 1
  %_338 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_339 = icmp eq i32 %_337, %_338
  br i1 %_339, label %_109.landingpad.succ, label %_109.landingpad.fail
_109.landingpad.succ:
  %_340 = call i8* @__cxa_begin_catch(i8* %_336)
  %_341 = bitcast i8* %_340 to i8**
  %_342 = getelementptr i8*, i8** %_341, i32 1
  %_109 = load i8*, i8** %_342
  call void @__cxa_end_catch()
  br label %_48.0
_109.landingpad.fail:
  resume { i8*, i32 } %_335
_111.landingpad:
  %_343 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_344 = extractvalue { i8*, i32 } %_343, 0
  %_345 = extractvalue { i8*, i32 } %_343, 1
  %_346 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_347 = icmp eq i32 %_345, %_346
  br i1 %_347, label %_111.landingpad.succ, label %_111.landingpad.fail
_111.landingpad.succ:
  %_348 = call i8* @__cxa_begin_catch(i8* %_344)
  %_349 = bitcast i8* %_348 to i8**
  %_350 = getelementptr i8*, i8** %_349, i32 1
  %_111 = load i8*, i8** %_350
  call void @__cxa_end_catch()
  br label %_68.0
_111.landingpad.fail:
  resume { i8*, i32 } %_343
_113.landingpad:
  %_351 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_352 = extractvalue { i8*, i32 } %_351, 0
  %_353 = extractvalue { i8*, i32 } %_351, 1
  %_354 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_355 = icmp eq i32 %_353, %_354
  br i1 %_355, label %_113.landingpad.succ, label %_113.landingpad.fail
_113.landingpad.succ:
  %_356 = call i8* @__cxa_begin_catch(i8* %_352)
  %_357 = bitcast i8* %_356 to i8**
  %_358 = getelementptr i8*, i8** %_357, i32 1
  %_113 = load i8*, i8** %_358
  call void @__cxa_end_catch()
  br label %_78.0
_113.landingpad.fail:
  resume { i8*, i32 } %_351
_117.landingpad:
  %_359 = landingpad { i8*, i32 } catch i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*)
  %_360 = extractvalue { i8*, i32 } %_359, 0
  %_361 = extractvalue { i8*, i32 } %_359, 1
  %_362 = call i32 @llvm.eh.typeid.for(i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN11scalanative16ExceptionWrapperE to i8*))
  %_363 = icmp eq i32 %_361, %_362
  br i1 %_363, label %_117.landingpad.succ, label %_117.landingpad.fail
_117.landingpad.succ:
  %_364 = call i8* @__cxa_begin_catch(i8* %_360)
  %_365 = bitcast i8* %_364 to i8**
  %_366 = getelementptr i8*, i8** %_365, i32 1
  %_117 = load i8*, i8** %_366
  call void @__cxa_end_catch()
  br label %_48.0
_117.landingpad.fail:
  resume { i8*, i32 } %_359
}

define nonnull dereferenceable(16) i8* @"_SM46scala.collection.ArrayOps$ArrayIterator$mcD$spD4nextL16java.lang.ObjectEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call double @"_SM46scala.collection.ArrayOps$ArrayIterator$mcD$spD4nextdEO"(i8* dereferenceable_or_null(32) %_1)
  %_20003 = call dereferenceable_or_null(16) i8* @"_SM27scala.runtime.BoxesRunTime$D11boxToDoubledL16java.lang.DoubleEO"(i8* null, double %_20001)
  ret i8* %_20003
}

define double @"_SM46scala.collection.ArrayOps$ArrayIterator$mcD$spD4nextdEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call double @"_SM46scala.collection.ArrayOps$ArrayIterator$mcD$spD11next$mcD$spdEO"(i8* dereferenceable_or_null(32) %_1)
  ret double %_20001
}

define nonnull dereferenceable(8) i8* @"_SM4MainD4mainLAL16java.lang.String_uEo"(i8* %_1) inlinehint personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  call nonnull dereferenceable(8) i8* @"_SM5Main$D4mainLAL16java.lang.String_uEO"(i8* nonnull dereferenceable(8) bitcast ({ i8* }* @"_SM5Main$G8instance" to i8*), i8* dereferenceable_or_null(8) %_1)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}

define nonnull dereferenceable(8) i8* @"_SM50scala.collection.immutable.VectorBuilder$$Lambda$1D5applyL16java.lang.ObjectL16java.lang.ObjectEO"(i8* %_1, i8* %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30007 = icmp ne i8* %_1, null
  br i1 %_30007, label %_30005.0, label %_30006.0
_30005.0:
  %_30011 = bitcast i8* %_1 to { i8*, i8* }*
  %_30012 = getelementptr { i8*, i8* }, { i8*, i8* }* %_30011, i32 0, i32 1
  %_30008 = bitcast i8** %_30012 to i8*
  %_30013 = bitcast i8* %_30008 to i8**
  %_30002 = load i8*, i8** %_30013, !dereferenceable_or_null !{i64 72}
  %_30003 = bitcast i8* %_2 to i8*
  call nonnull dereferenceable(8) i8* @"_SM40scala.collection.immutable.VectorBuilderD20$anonfun$addVector$1LAL16java.lang.Object_uEPT40scala.collection.immutable.VectorBuilder"(i8* dereferenceable_or_null(72) %_30002, i8* dereferenceable_or_null(8) %_30003)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_30006.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i32 @"_SM51java.nio.file.attribute.PosixFilePermission$$anon$1D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM51java.nio.file.attribute.PosixFilePermission$$anon$1D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM51java.nio.file.attribute.PosixFilePermission$$anon$1D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define i32 @"_SM51java.nio.file.attribute.PosixFilePermission$$anon$9D12productArityiEO"(i8* %_1) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call i32 @"_SM23scala.runtime.EnumValueD12productArityiEO"(i8* dereferenceable_or_null(24) %_1)
  ret i32 %_20001
}

define dereferenceable_or_null(32) i8* @"_SM51java.nio.file.attribute.PosixFilePermission$$anon$9D13productPrefixL16java.lang.StringEO"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  %_20001 = call dereferenceable_or_null(32) i8* @"_SM14java.lang.EnumD4nameL16java.lang.StringEO"(i8* dereferenceable_or_null(24) %_1)
  ret i8* %_20001
}

define dereferenceable_or_null(8) i8* @"_SM51java.nio.file.attribute.PosixFilePermission$$anon$9D14productElementiL16java.lang.ObjectEO"(i8* %_1, i32 %_2) alwaysinline personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30002 = bitcast i8* bitcast (i8* (i8*, i32)* @"_SM23scala.runtime.EnumValueD14productElementiL16java.lang.ObjectEO" to i8*) to i8* (i8*, i32)*
  %_30001 = call dereferenceable_or_null(8) i8* %_30002(i8* dereferenceable_or_null(24) %_1, i32 %_2)
  ret i8* %_30001
}

define nonnull dereferenceable(8) i8* @"_SM65scala.scalanative.runtime.ieee754tostring.ryu.RyuFloat$$$Lambda$2D13apply$mcVI$spiuEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30010 = icmp ne i8* %_1, null
  br i1 %_30010, label %_30008.0, label %_30009.0
_30008.0:
  %_30029 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30030 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30029, i32 0, i32 1
  %_30011 = bitcast i8** %_30030 to i8*
  %_30031 = bitcast i8* %_30011 to i8**
  %_30001 = load i8*, i8** %_30031, !dereferenceable_or_null !{i64 48}
  %_30013 = icmp ne i8* %_1, null
  br i1 %_30013, label %_30012.0, label %_30009.0
_30012.0:
  %_30032 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30033 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30032, i32 0, i32 2
  %_30014 = bitcast i8** %_30033 to i8*
  %_30034 = bitcast i8* %_30014 to i8**
  %_30002 = load i8*, i8** %_30034, !dereferenceable_or_null !{i64 16}
  %_30016 = icmp ne i8* %_1, null
  br i1 %_30016, label %_30015.0, label %_30009.0
_30015.0:
  %_30035 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30036 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30035, i32 0, i32 3
  %_30017 = bitcast i32* %_30036 to i8*
  %_30037 = bitcast i8* %_30017 to i32*
  %_30003 = load i32, i32* %_30037
  %_30019 = icmp ne i8* %_1, null
  br i1 %_30019, label %_30018.0, label %_30009.0
_30018.0:
  %_30038 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30039 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30038, i32 0, i32 4
  %_30020 = bitcast i8** %_30039 to i8*
  %_30040 = bitcast i8* %_30020 to i8**
  %_30004 = load i8*, i8** %_30040, !dereferenceable_or_null !{i64 8}
  %_30022 = icmp ne i8* %_1, null
  br i1 %_30022, label %_30021.0, label %_30009.0
_30021.0:
  %_30041 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30042 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30041, i32 0, i32 5
  %_30023 = bitcast i8** %_30042 to i8*
  %_30043 = bitcast i8* %_30023 to i8**
  %_30005 = load i8*, i8** %_30043, !dereferenceable_or_null !{i64 16}
  %_30025 = icmp ne i8* %_1, null
  br i1 %_30025, label %_30024.0, label %_30009.0
_30024.0:
  %_30044 = bitcast i8* %_1 to { i8*, i8*, i8*, i32, i8*, i8*, i32 }*
  %_30045 = getelementptr { i8*, i8*, i8*, i32, i8*, i8*, i32 }, { i8*, i8*, i8*, i32, i8*, i8*, i32 }* %_30044, i32 0, i32 6
  %_30026 = bitcast i32* %_30045 to i8*
  %_30046 = bitcast i8* %_30026 to i32*
  %_30006 = load i32, i32* %_30046
  call nonnull dereferenceable(8) i8* @"_SM55scala.scalanative.runtime.ieee754tostring.ryu.RyuFloat$D24floatToString$$anonfun$2L20scala.runtime.IntRefiLAc_L20scala.runtime.IntRefiiuEPT55scala.scalanative.runtime.ieee754tostring.ryu.RyuFloat$"(i8* dereferenceable_or_null(48) %_30001, i8* dereferenceable_or_null(16) %_30002, i32 %_30003, i8* dereferenceable_or_null(8) %_30004, i8* dereferenceable_or_null(16) %_30005, i32 %_30006, i32 %_2)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_30009.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define nonnull dereferenceable(8) i8* @"_SM66scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$$$Lambda$6D13apply$mcVI$spiuEO"(i8* %_1, i32 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  %_30010 = icmp ne i8* %_1, null
  br i1 %_30010, label %_30008.0, label %_30009.0
_30008.0:
  %_30029 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i32, i8*, i8* }*
  %_30030 = getelementptr { i8*, i8*, i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i8*, i8*, i32, i8*, i8* }* %_30029, i32 0, i32 1
  %_30011 = bitcast i8** %_30030 to i8*
  %_30031 = bitcast i8* %_30011 to i8**
  %_30001 = load i8*, i8** %_30031, !dereferenceable_or_null !{i64 24}
  %_30013 = icmp ne i8* %_1, null
  br i1 %_30013, label %_30012.0, label %_30009.0
_30012.0:
  %_30032 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i32, i8*, i8* }*
  %_30033 = getelementptr { i8*, i8*, i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i8*, i8*, i32, i8*, i8* }* %_30032, i32 0, i32 2
  %_30014 = bitcast i8** %_30033 to i8*
  %_30034 = bitcast i8* %_30014 to i8**
  %_30002 = load i8*, i8** %_30034, !dereferenceable_or_null !{i64 16}
  %_30016 = icmp ne i8* %_1, null
  br i1 %_30016, label %_30015.0, label %_30009.0
_30015.0:
  %_30035 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i32, i8*, i8* }*
  %_30036 = getelementptr { i8*, i8*, i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i8*, i8*, i32, i8*, i8* }* %_30035, i32 0, i32 3
  %_30017 = bitcast i8** %_30036 to i8*
  %_30037 = bitcast i8* %_30017 to i8**
  %_30003 = load i8*, i8** %_30037, !dereferenceable_or_null !{i64 16}
  %_30019 = icmp ne i8* %_1, null
  br i1 %_30019, label %_30018.0, label %_30009.0
_30018.0:
  %_30038 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i32, i8*, i8* }*
  %_30039 = getelementptr { i8*, i8*, i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i8*, i8*, i32, i8*, i8* }* %_30038, i32 0, i32 4
  %_30020 = bitcast i32* %_30039 to i8*
  %_30040 = bitcast i8* %_30020 to i32*
  %_30004 = load i32, i32* %_30040
  %_30022 = icmp ne i8* %_1, null
  br i1 %_30022, label %_30021.0, label %_30009.0
_30021.0:
  %_30041 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i32, i8*, i8* }*
  %_30042 = getelementptr { i8*, i8*, i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i8*, i8*, i32, i8*, i8* }* %_30041, i32 0, i32 5
  %_30023 = bitcast i8** %_30042 to i8*
  %_30043 = bitcast i8* %_30023 to i8**
  %_30005 = load i8*, i8** %_30043, !dereferenceable_or_null !{i64 8}
  %_30025 = icmp ne i8* %_1, null
  br i1 %_30025, label %_30024.0, label %_30009.0
_30024.0:
  %_30044 = bitcast i8* %_1 to { i8*, i8*, i8*, i8*, i32, i8*, i8* }*
  %_30045 = getelementptr { i8*, i8*, i8*, i8*, i32, i8*, i8* }, { i8*, i8*, i8*, i8*, i32, i8*, i8* }* %_30044, i32 0, i32 6
  %_30026 = bitcast i8** %_30045 to i8*
  %_30046 = bitcast i8* %_30026 to i8**
  %_30006 = load i8*, i8** %_30046, !dereferenceable_or_null !{i64 16}
  call nonnull dereferenceable(8) i8* @"_SM56scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$D25doubleToString$$anonfun$6L20scala.runtime.IntRefL21scala.runtime.LongRefiLAc_L20scala.runtime.IntRefiuEPT56scala.scalanative.runtime.ieee754tostring.ryu.RyuDouble$"(i8* dereferenceable_or_null(24) %_30001, i8* dereferenceable_or_null(16) %_30002, i8* dereferenceable_or_null(16) %_30003, i32 %_30004, i8* dereferenceable_or_null(8) %_30005, i8* dereferenceable_or_null(16) %_30006, i32 %_2)
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
_30009.0:
  call i8* @"_SM34scala.scalanative.runtime.package$D16throwNullPointernEO"(i8* null)
  unreachable
}

define i1 @"_SM75scala.scalanative.runtime.ieee754tostring.ryu.RyuRoundingMode$Conservative$D16acceptLowerBoundzzEO"(i8* %_1, i1 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i1 false
}

define i1 @"_SM75scala.scalanative.runtime.ieee754tostring.ryu.RyuRoundingMode$Conservative$D16acceptUpperBoundzzEO"(i8* %_1, i1 %_2) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_30000.0:
  ret i1 false
}

define nonnull dereferenceable(8) i8* @"_SM75scala.scalanative.runtime.ieee754tostring.ryu.RyuRoundingMode$Conservative$RE"(i8* %_1) personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
_20000.0:
  ret i8* bitcast ({ i8* }* @"_SM36scala.scalanative.runtime.BoxedUnit$G8instance" to i8*)
}