class MyQueue(maxSize: Int):
  private var queueBox: Array[Any] = new Array[Any](maxSize)
  private var front: Int = 0
  private var rear: Int = -1
  private var numOfItems: Int = 0
  def insert(data: Any): Unit = {
    if rear==maxSize-1 then
      rear = -1
    end if
    rear=rear+1
    queueBox(rear) = data
    numOfItems += 1
  }
  def remove(): Any = {
    val tempData: Any = queueBox(front)
    front=front+1
    if front==maxSize then
      front = 0
    end if
    numOfItems=numOfItems-1
    tempData
  }
  def peekFront(): Any = {
    queueBox(front)
  }
  def isEmpty(): Boolean = {
    numOfItems == 0
  }
  def isFull(): Boolean = {
    numOfItems == maxSize
  }
end MyQueue

@main def test()=main()
def main()=
  val myQueue = new MyQueue(10)
  myQueue.insert(5)
  myQueue.insert(10)
  myQueue.insert(15)
  while !myQueue.isEmpty() do
    println(myQueue.remove())
  end while
end main
