package pak1:
  package pak2:
    var s:Int=0
  end pak2
  package object pak2:   //Package object
    var t:Int=1
  end pak2
end pak1

@main def test()=main()
def main()=
  pak1.pak2.s=5
  import pak1._
  pak2.s=5
  import pak1.pak2._
  s=5
  pak1.pak2.t=1
end main
