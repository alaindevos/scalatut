// Singleton Object , only one, static, created at first use , methods are static

class Companion:
  var mydynamic:Int=0
  def this(d:Int)=
    this()
    this.mydynamic=d
  end this
end Companion

object Companion:
  def apply(d:Int)=
    new Companion(d)
  end apply
  def unapply(c:Companion):String=s"${c.mydynamic}"
end Companion

object Counter:
  private var value:Int=0    //Static variable, only one in the program
  var mypublic:Int=0
  def addone()={value+=1;()}
  def getval()=value
  def setval(value:Int)={this.value=value;()} //static function
end Counter

class TimeStamp(val seconds:Long)

object TimeStamp:
  def apply(minutes:Int,seconds:Int):TimeStamp=
    new TimeStamp(minutes*60+seconds)
  end apply
end TimeStamp

@main def test()=main()
def main()=
  val t:TimeStamp=TimeStamp(3,55)
  Counter.addone()
  val i:Int=Counter.getval()
  var c1:Companion=new Companion()
  var c2:Companion=Companion.apply(5)
  var c3:Companion=Companion(5)        // Short notation
end main
