// List : Ordered immutable linear sequence of elements
@main
def test()=
  val nums1=List.range(0,10)
  val nums2=(1 to 10).toList

  case class Person(name:String,age:Int)

  val p1:Person=Person(name="Alain",age=20)
  val p2:Person=Person(name="Eddy",age=30)
  val p3a:List[Person]=List[Person]() //empty list
  val p3b:List[Person]=List.empty[Person] //empty list
  val contacts1a:List[Person]=Nil
  val contacts1b:List[Person]=List()
  val contacts2:List[Person]=List(p1,p2)
  val contacts3:List[Person]= ::(p1, ::(p2,Nil))
  val contacts4:List[Person]=p1::p2::Nil
  val contacts5:List[Person]=contacts4 :+ p1  //Append
  val contacts6:List[Person]=p1 +:contacts5   //Prepend
  val contacts7:List[Person]=contacts6 ++ contacts6 //Concatenate
  val contacts8:List[Person]=contacts6 ::: contacts6 //Concatenate
  val pa:Person=contacts7.head
  val pb:Person=contacts7.last
  val contactsa:List[Person]=p1 :: contacts7 //Prepend
  val li1:List[Int]=List(1,2,3).map(_*2)
  val li2:List[Int]=List(1,2,3).filter(_ % 2 == 1)

  for(x<-contacts7){ x match
    case Person(aname:String,anage:Int) => println(aname+" "+anage.toString)
  }


  def getnames1(pl:List[Person]):List[String]= pl match
    case Nil => Nil
    case hd::tl => hd.name :: getnames1(tl)
  end getnames1

  def getnames2(pl:List[Person]):List[String]=
    pl.map(c=>c.name)
  end getnames2

  val s:Int=contacts7.size
  val b1:Boolean=contacts7.isEmpty
  val b2:Boolean=contacts7.nonEmpty
  val b3:Boolean=contacts7.contains(p1)
  val b4:Boolean=contacts7.exists(_.name=="Alain")

  val p3:Person=contacts7.apply(0)
  val p4:Person=contacts7(0)
  val po:Option[Person]=contacts7.headOption //First element

  def findbyname(pl:List[Person],aname:String):Option[Person]=
    pl.find(p=> p.name.startsWith(aname))
  end findbyname

  val pl1:List[Person]=contacts7.take(2).drop(1)
  val pl2:List[Person]=contacts7.filter(c=> c.name=="Alain")

  val pl3:List[Person]=contacts7.distinct  //Remove duplicates

  val s1:List[Int]=List(7,5,3).sorted
  val s2:List[Int]=List(7,5,3).reverse
  val s3:List[String]=List("Alain","Eddy").sortWith(_.length < _.length)
  val i1:Int=List(7,5,3).sum
  val pl4:List[Person]=contacts7.sortBy(c => (c.age))

  val a = List(5, 1, 4, 3, 2)
  val c1 = a.filter(_ > 2)
  val c2 = a.take(2)
  val c3 = a.drop(2)
  val c4 = a diff List(1)
  a.foreach(println)

end test
