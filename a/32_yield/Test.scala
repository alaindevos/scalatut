@main def test()=main()
def main()=
  val array=Array(1,2,3,4)
  val array2:Array[Int]=
    for(x<-array)
      yield (x*x)
    end for
  val names=List("Alain","Eddy")
  val x=for(name<-names) yield {
    val y=name.drop(1)
    val z=y.capitalize
    z
  }
end main
