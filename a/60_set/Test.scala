//Set : unordered group of distinct values
@main def test()=main()
def main()=
  import scala.collection.immutable._
  val s1:Set[Int]=Set(1,2,3)
  s1.foreach{ x => println(x)}
  val s2:Set[Int]=s1 + 4
  val s3:Set[Int]=s2 - 2
  val sx=s1 ++ s2

  case class Person(name:String,age:Int)
  def getnames(p:Set[Person]):Set[String]=
    p.map(x => x.name)
  end getnames
  val s4:Set[Int]=s3.map(x => 2*x)
  val s5:Set[Int]=s2 union s3
  val s6:Set[Int]=s2 intersect s3
  val s7:Set[Int]=s2 diff s3
  val b:Boolean=s7.contains(4)

  val s8:Set[Int]=Set(4,3,2,1)

  import scala.collection.mutable.{Set=>MSet}
  val set1=MSet[Int]()
  set1 +=1
  set1 ++=Vector(4,5)
  set1 --=Array(4,5)
  set1.remove(1)

end main
