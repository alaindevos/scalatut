//Trait is interface & abstract class & contains default methods

trait Animal:
  def eat(food:String):Unit
  def eat2(food:String):Unit={println(food)}
end Animal

trait Nameable(name:String):
  var id:String=""
end Nameable

class Dog(val name:String)  extends Animal with Nameable(name):
  def eat(food:String):Unit=
    val s=s"I'm a "+ this.name+"and I eat " + food
    println(s)
  end eat
  override def eat2(food:String):Unit={println("Woof")}
end Dog

//Sealed : Fixed number of implementations
sealed trait Currency
object USD extends Currency
object EUR extends Currency

def exchangerate(c:Currency):Double=
    c match
      case USD => 1.0
      case EUR => 1.5
end exchangerate

sealed trait Topping
case object Cheese extends Topping
case object Pepperoni extends Topping

@main def test()=main()
def main()=

  var toppings=scala.collection.mutable.ArrayBuffer[Topping]()
  toppings += Cheese
  toppings += Pepperoni

  val d:Dog=Dog("Dog")
  d.id="5321"
  d.eat("Dogfood")
end main
