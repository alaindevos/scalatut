package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f
object CubeCalculator:
  def cube(x: Int): Int = x * x * x

val emptylist = List()

extension (xs:List[Int]) @targetName ("eaddafter") def eaddafter(x:Int): List[Int] = xs :+x

def addafter(x: Int,xs:List[Int]): List[Int] =
  xs :+ x

class myList[T]:
  var head: List[Int] = List()
  def addbefore(data: Int): Unit =
    head = data :: head
  def printList(): Unit =
    var current = head
    while current != emptylist do
      print(current.head.toString + " ")
      current = current.tail
    println()

@main private def main(args: String*): Int =
  var list: myList[Int] = new myList[Int]
  list.addbefore(1)
  list.addbefore(2)
  list.addbefore(3)
  list.printList()
  val newList0=List()
  val newList1=addafter(1,newList0)
  val newList2=addafter(2,newList1)
  val newList3=newList2.eaddafter(3)
  val newList4=newList3.eaddafter(4)
  println(newList4)
  0
