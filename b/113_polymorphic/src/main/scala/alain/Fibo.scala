package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f

def abs(n:Int):Int =
  if n<0 then -n
  else  n

def formatResult[A](name:String , n:A , f : A => A):Unit =
  val result =f(n)
  val msg = f"The $name of $n is $result"
  msg |> println

@main private def main(args: String*): Int =
  var myArgs: Seq[String] = args
  println("Hello World")
  formatResult("Absolute value",-42,abs)
  0

