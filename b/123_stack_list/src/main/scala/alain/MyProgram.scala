package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f
object CubeCalculator:
  def cube(x:Int):Int = x * x * x

class Node[T](val data: T, var next: Node[T] = null)

class myList[T] :
  var head: Node[T] = null
  def add(data: T): Unit =
    val newNode = new Node(data)
    if head == null then
      head = newNode
    else
      var current = head
      while current.next != null do
        current = current.next
      current.next = newNode

  def printList(): Unit =
    var current = head
    while current != null do
      print (current.data.toString + " ")
      current = current.next
    println()


@main private def main(args: String*): Int =
  val list : myList[Int] = new myList[Int]
  list.add(1)
  list.add(2)
  list.add(3)
  list.printList() // Output: 1 2 3
  0
