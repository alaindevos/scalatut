package alain
/*
import scala.Array
import scala.annotation._
import scala.collection.immutable._
import collection.mutable.{Map=>MMap}
import scala.language.postfixOps
import scala.collection.immutable.List._
import scala.collection.immutable.Nil
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer
import collection.mutable.ArrayDeque
import scala.util.matching.Regex
import scala.util.matching.Regex.MatchIterator
import scala.util.Success
import scala.util.Try
import fansi.Color.* 
import fansi.Str
import sys.process._
import java.io.PrintWriter
import java.lang.ArithmeticException
import scalafx.scene.control.*
import scalafx.scene.layout.HBox
import scalafx.event.ActionEvent
import scalafx.scene.layout.StackPane
import scalafx.scene.canvas.{Canvas, GraphicsContext}
import scalafx.Includes.*
import scalafx.application.JFXApp3
import scalafx.scene.Scene
import scalafx.scene.paint._
import scalafx.scene.paint.Color
import scalafx.scene.shape.{Circle, Rectangle}
import scala.math._
*/
import scala.annotation.targetName
import scala.util.chaining.*

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

@main private def main(args: String*): Int =
  val maybeName: Option[String] = Some("Alice")
  // val maybeName: Option[String] = None
  val maybeLength: Option[Int] = 
    maybeName.flatMap { name =>Some(name.length) }
  println(maybeLength) // Output: Some(5) or None
  0
