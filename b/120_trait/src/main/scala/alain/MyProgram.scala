package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f
object CubeCalculator:
  def cube(x:Int):Int = x * x * x

trait TAnimal:
  val Name:String
  def Talk(s:String):Unit

class Chicken(name : String) extends TAnimal:
  val Name:String=name
  def Talk(msg:String):Unit =
    printf("My name is %s\n",Name)
    printf("Cluck: %s\n",msg)
  
extension (a: TAnimal) @targetName("animalFunction") def animalFunction():Unit =
  printf("I am the animalFunction\n")
  printf("In the AnimalFunction i am called: %s",a.Name)
  a.Talk("Koekoek from animalFunction")

@main private def main(args: String*): Int =
  println("Hello World")
  val c:Chicken=Chicken("Clucky")
  "Koekoek" |> c.Talk
  c.animalFunction()
  0
