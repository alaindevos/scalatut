package alain

import slick.jdbc.PostgresProfile.api.*

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.concurrent.duration.{Duration, MILLISECONDS}
import scala.concurrent.{Await, Future}

val db = Database.forConfig("postgres")

final case class Person(firstname: String, lastname: String)

class Persons(tag: Tag) extends Table[Person](tag, "person") {
  override def * = (firstname, lastname).mapTo[Person]

  def firstname = column[String]("firstname")

  def lastname = column[String]("lastname")
}

object main {
  def main(args: Array[String]): Unit = {
    lazy val persons = TableQuery[Persons]
    val fansiStr: fansi.Str = fansi.Color.Red("This should be red")
    println(fansiStr)
    val currentDateTime: LocalDateTime = LocalDateTime.now()
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val f = currentDateTime.format(formatter)
    val myAction: DBIO[Seq[Person]] = persons.result
    println("Current Time:" + currentDateTime)

    println("QUERY-----------------------------------------------------------------")
    val myFuture: Future[Seq[Person]] = db.run(myAction)
    val myResults: Seq[Person] = Await.result(myFuture, Duration(2000, MILLISECONDS))
    val aPerson: Person = Person("Jean", "Luc")
    println(myResults)

    println("INSERT----------------------------------------------------------------")
    val insertQuery = persons += aPerson
    val myFuture2: Future[Int] = db.run(insertQuery)
    val myAction3: DBIO[Seq[Person]] = persons.filter(_.firstname.like("Jean")).result
    Await.result(myFuture2, Duration(2000, MILLISECONDS))

    println("FILTER----------------------------------------------------------------")
    val myFuture3: Future[Seq[Person]] = db.run(myAction3)
    val myResults3: Seq[Person] = Await.result(myFuture3, Duration(2000, MILLISECONDS))

    println("SQL-------------------------------------------------------------------")
    val myAction4 = sql"""select c.firstname, c.lastname from person c""".as[(String, String)]
    println(myResults3)
    val myFuture4 = db.run(myAction4)
    val myResults4: Seq[(String, String)] = Await.result(myFuture4, Duration(2000, MILLISECONDS))
    println(myResults4)
  }
}