import scalafx.scene.control.*
import scalafx.event.ActionEvent
import scalafx.scene.layout.StackPane
import scalafx.Includes.*

import scalafx.scene.canvas.{Canvas, GraphicsContext}
import scalafx.scene.layout.{HBox,Pane}
import scalafx.application.JFXApp3
import scalafx.scene.Scene
import scalafx.scene.paint._
import scalafx.scene.paint.Color
import scalafx.scene.shape.{Circle, Rectangle}
import scala.math._

def intToColor(argb: Int): Color = {
  val alpha = (argb >> 24) & 0xFF
  val red = (argb >> 16) & 0xFF
  val green = (argb >> 8) & 0xFF
  val blue = argb & 0xFF
  Color.rgb(red, green, blue, alpha / 255.0)}

val rectangle1 = new Rectangle { x = 50 ;  y = 50 ; width = 100 ; height = 100 ; fill = Color.Blue }

def addchildren(myroot:Pane): Unit =
  myroot.children.add(rectangle1)

def addSine(myroot:Pane): Unit =
  for (_x2 <- 1 to 600) {
    var _x = _x2
    var _y = (1 + sin(_x2 / 30.0)) * 100
    val i = 192 * 256 * 256 * 256 + _x * 256 * 256 + (256 - _x) * 256 + _x / 2
    val p = intToColor(i);
    var myrect = new Rectangle { x = _x ; y = _y ; width = 2 ; height = 2 ; fill = p }
    myroot.children.add(myrect)}

object MyProgram extends JFXApp3 {
  val myroot = new Pane {}
  override def start(): Unit = {
    stage = new JFXApp3.PrimaryStage {
      title = "MyProgram"
      width = 800
      height = 600
      scene = new Scene(myroot)
    }}
  addchildren(myroot)
  addSine(myroot)}
