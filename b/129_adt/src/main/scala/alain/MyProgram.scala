package alain

import scala.annotation.targetName
import scala.util.chaining.*

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

class MyInt(_myint: Int):
  var myInt: Int = _myint

class MyString(_mystring: String):
  var myString: String = _mystring

type StringOrInt = MyString | MyInt

class CStringOrInt(_si: StringOrInt):
   var si:StringOrInt = _si

extension (x: CStringOrInt) @targetName("printstringorint") def printstringorint: Unit =
  x.si match
    case y:MyInt => printf("%d\n", y.myInt)
    case y: MyString => printf("%s\n", y.myString)

@main private def main(args: String*): Int =
  var si1:CStringOrInt=CStringOrInt(MyInt(5))
  var si2:CStringOrInt=CStringOrInt(MyString("five"))
  si1.printstringorint
  si2.printstringorint
  0
