scalaVersion := "3.4.0"
version := "1.0"
name := "mytest"
organization := "com.alain"
val slickVersion = "3.5.0"
libraryDependencies ++= Seq("com.lihaoyi" %% "fansi" % "0.4.0" ,
                            "org.slf4j" % "slf4j-nop" % "2.0.13" ,
                            "org.postgresql" % "postgresql" % "42.7.3" ,
                            "com.github.tminglei" %% "slick-pg" % "0.22.2",
                            "com.github.tminglei" %% "slick-pg_play-json" % "0.22.2" ,
                            "com.typesafe.slick" %% "slick" % slickVersion ,
                            "com.typesafe.slick" %% "slick-codegen" % slickVersion ,
                            "com.typesafe.slick" %% "slick-hikaricp" % slickVersion ,
                            //org.postgresql.ds.PGSimpleDataSource dependency
                            )
