package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*
import scala.annotation.tailrec

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f
object CubeCalculator:
  def cube(x: Int): Int = x * x * x

@main private def main(args: String*): Int =
  lazy val x = {
    println("Computing x")
    42
  }
  println("Before x")
  println(x)
  println("After x")
  0