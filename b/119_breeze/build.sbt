name := "fiboApp"
version := "1"
lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "alain",
      scalaVersion := "3.3.4"
    )),
    name := "test"
  )

libraryDependencies ++=
  Seq("org.scalatest" %% "scalatest" % "3.2.19" % Test,
      "org.scalanlp" %% "breeze" % "2.1.0",
      "org.scalanlp" %% "breeze-viz" % "2.1.0")
