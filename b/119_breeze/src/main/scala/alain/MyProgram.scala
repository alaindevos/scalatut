package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*
import breeze.linalg._
import breeze.plot._

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

@main private def main(args: String*): Int =
  println("Hello World")
  val f = Figure()
  
  val p = f.subplot(0)
  val x = linspace(0.0, 1.0)
  p += plot(x, x ^:^ 2.0)
  p += plot(x, x ^:^ 3.0, '.')
  p.xlabel = "x axis"
  p.ylabel = "y axis"
  f.saveas("lines.png") // save current figure as a .png, eps and pdf also supportedls

  0
