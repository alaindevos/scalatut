object Main extends App {
  import cats._
  import cats.data._
  import cats.effect._
  import cats.implicits._
  import doobie._
  import doobie.implicits._
  import doobie.util.ExecutionContexts
  import doobie.postgres._
  import doobie.postgres.implicits._

  // This is just for testing. Consider using cats.effect.IOApp instead of calling
  // unsafe methods directly.
  import cats.effect.unsafe.implicits.global

  // A transactor that gets connections from java.sql.DriverManager and executes blocking operations
  // on an our synchronous EC. See the chapter on connection handling for more info.
  val xa = Transactor.fromDriverManager[IO](
    "org.postgresql.Driver",     // driver classname
    "jdbc:postgresql:world",     // connect URL (driver-specific)
    "x",                  // user
    "x"                       // password
  )
  case class Country(code: String, name: String, population: Long)

  def find(n: String): ConnectionIO[Option[Country]] =
    sql"select code, name, population from country where name = $n".query[Country].option
  val res:Option[Country]=find("France").transact(xa).unsafeRunSync()
  sql"select name from country"
    .query[String]    // Query0[String]
    .stream
    .take(5)
    .compile.toList
    .transact(xa)     // IO[List[String]]
    .unsafeRunSync()  // List[String]
    .foreach(println)
  println("Hello, World!")
  val program2=sql"select 42".query[Int].unique
  val i=program2.transact(xa).unsafeRunSync()
  print(i)
}
