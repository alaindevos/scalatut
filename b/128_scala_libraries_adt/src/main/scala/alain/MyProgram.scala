package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*
import scala.annotation.tailrec
import scala.language.postfixOps
import breeze.linalg._
import breeze.plot._
import cats._
import cats.data._
import cats.effect._
import cats.effect.unsafe.implicits._
import cats.effect.unsafe.implicits.global
import cats.implicits._
import collection.mutable.ArrayDeque
import collection.mutable.{Map=>MMap}
import doodle.core._
import doodle.image._
import doodle.image.syntax._
import doodle.image.syntax.all._
import doodle.image.syntax.core._
import doodle.java2d._
import fansi.Color.* 
import fansi.Str
import java.io.PrintWriter
import java.lang.ArithmeticException
import java.time.format.DateTimeFormatter
import java.time.LocalDateTime
import java.util.Calendar
import java.util.Date
import scala.annotation._
import scala.annotation.newMain
import scala.annotation.tailrec
import scala.annotation.targetName
import scala.Array
import scala.collection.immutable._
import scala.collection.immutable.List._
import scala.collection.immutable.Nil
import scala.collection.immutable.SortedMap
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap
import scala.collection.mutable.Iterable
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Map
import scala.collection.mutable.Queue
import scala.collection.mutable.{Set=>MSet}
import scala.collection.mutable.Stack
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.{Duration, MILLISECONDS}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.BufferedSource
import scala.io.Source
import scala.language.postfixOps
import scala.util.chaining.*
import scala.util.Failure
import scala.util.{Failure,Success}
import scala.util.matching.Regex
import scala.util.matching.Regex.MatchIterator
import scala.util.Sorting.quickSort
import scala.util.Success
import scala.util.Try
import slick.jdbc.PostgresProfile.api.*
import sys.process._
import java.time.LocalDateTime
import scala.concurrent.{Await, Future}
import scala.swing._

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

class MyInt(_myint: Int):
  var myInt: Int = _myint

class MyString(_mystring: String):
  var myString: String = _mystring

type StringOrInt = MyString | MyInt

class CStringOrInt(_si: StringOrInt):
   var si:StringOrInt = _si

extension (x: CStringOrInt) @targetName("printstringorint") def printstringorint: Unit =
  x.si match
    case y:MyInt => printf("%d\n", y.myInt)
    case y: MyString => printf("%s\n", y.myString)

@main private def main(args: String*): Int =
  var si1:CStringOrInt=CStringOrInt(MyInt(5))
  var si2:CStringOrInt=CStringOrInt(MyString("five"))
  si1.printstringorint
  si2.printstringorint
  0
