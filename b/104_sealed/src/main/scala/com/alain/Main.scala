package com.alain

object Main {
  def main(args: Array[String]): Unit =
    sealed trait StringOrInt
    case class MyInt(x:Int) extends StringOrInt
    case class MyString(s:String ) extends StringOrInt

    var x:StringOrInt=MyInt(5)
    x = MyString("Hallo")

    enum StringOrInt2:
      case MyInt(x:Int)
      case MyString(s:String)

    var y: StringOrInt = MyInt(5)
    y = MyString("Hallo")
}
