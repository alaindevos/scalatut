package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

enum intList:
    case Nil
    case Cons(head:Int,tail:intList)

object intList:
  def apply(as:Int*):intList =
    if as.isEmpty then Nil
    else Cons(as.head,apply(as.tail*))

  def sum(myIntList :intList):Int =
    myIntList match
      case Nil => 0
      case Cons(x,xs) => x + sum(xs)

  def foldRight(x:intList,acc:Int, f:(Int,Int) => Int):Int =
    x match
      case Nil => acc
      case Cons(x,xs) => f(x,foldRight(xs,acc,f))

  def sum2(myIntList:intList):Int =
    foldRight(myIntList,0,(x,y)=>x+y)

@main private def main(args: String*): Int =
  val x:intList =  intList.Nil
  val y:intList =  intList.Cons(5,intList.Nil)
  val z:intList =  intList.Cons(3,intList.Cons(4,intList.Nil))
  println("Sum: %d " format (intList.sum(z)))
  println("Sum: %d " format (intList.sum2(z)))
  0

