package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

enum aList[+A]:
    case Nil
    case Cons(head:A,tail:aList[A])

object aList:
  def apply[A](x:A*):aList[A] =
    if x.isEmpty then Nil
    else Cons(x.head,apply(x.tail*))

  def sum[A](myIntList :aList[A]):Int =
    myIntList match
      case Nil => 0
      case Cons(x,xs) => x.toString.toInt + sum(xs)

  def foldRight[A](x:aList[A], acc:A, f:(Int,Int) => Int):Int =
    x match
      case Nil => acc.toString.toInt
      case Cons(x,xs) => f(x.toString.toInt,foldRight(xs,acc,f))

  def sum2(myIntList:aList[Int]):Int =
    foldRight(myIntList,0.toString.toInt,(x,y)=>x+y)

@main private def main(args: String*): Int =
  val x:aList[Int] =  aList.Nil
  val y:aList[Int] =  aList.Cons(5,aList.Nil)
  val z:aList[Int] =  aList.Cons(3,aList.Cons(4,aList.Nil))
  println("Sum: %d " format (aList.sum(z)))
  println("Sum: %d " format (aList.sum2(z)))
  0

