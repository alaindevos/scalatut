package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*
import scala.collection.immutable.List._
import scala.collection.immutable.Nil

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

// Mononid : Associative algebra
trait Monoid[A]:
  def combine(a1 : A,a2 : A) : A
  def empty : A

@main private def main(args: String*): Int =
  val StringMonoid:Monoid[String] = new:
    def combine(s1:String,s2:String):String = s1 + s2
    val empty:String =""

  val IntMonoid:Monoid[Int] = new:
    def combine(i1:Int,i2:Int):Int = i1 + i2
    val empty:Int = 0

  val IntListMonoid:Monoid[List[Int]] = new:
    def combine(l1:List[Int],l2:List[Int]) = l1 ++ l2
    val empty = Nil

  println(StringMonoid.combine("Alain "," De Vos "))
  println(IntMonoid.combine(5,3))
  0
