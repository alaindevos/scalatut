package com.alain
import scala.util.Try
import scala.util.Success
import scala.util.Failure
import java.lang.ArithmeticException
object Main {
    def mydivide(x:Int):Try[Int]=Try {16/x}

    def myprint(a:Try[Int]) : Unit =
        val ret = a match
            case Success(b) => b
            case Failure(c:ArithmeticException) =>
                println(c)
                0
            case _ => 0
        println(ret)

    def main(args:Array[String]):Unit=
        val fansiStr:fansi.Str = fansi.Color.Red("This should be red")
        println(fansiStr)
        val x=mydivide(4)
        val y=mydivide(0)
        println(x)
        println(y)
        myprint(x)
        myprint(y)

}
