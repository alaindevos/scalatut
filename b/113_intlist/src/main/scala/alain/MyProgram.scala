package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f

def compose1[A, B, C](f: B => C, g: A => B): A => C =
  x => f(g(x))

def add2(n:Int):Int = n+100
def mul3(n:Int):Int = n*33
extension [A,B] (x: A => B) def compose2[C](g:B => C): A=> C = y => g(x(y))

@main private def main(args: String*): Int =
  val x= (compose1 (mul3,add2)) (0)
  val y= mul3 compose add2
  val z= y(0)
  println(x) // OK
  println(z) // ERROR FUNCTION mul3 not run
  0

