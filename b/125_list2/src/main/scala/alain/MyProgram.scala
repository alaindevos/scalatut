package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*
import scala.collection.mutable.ListBuffer

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f
object CubeCalculator:
  def cube(x: Int): Int = x * x * x


@main private def main(args: String*): Int =
  // Create an empty mutable list
  val mutableList = ListBuffer[Int]()
  // Add elements to the list
  mutableList += 1
  mutableList += 2
  mutableList += 3
  // Add multiple elements at once
  mutableList ++= List(4, 5, 6)
  // Print the list
  println(mutableList)
  // Remove an element
  mutableList -= 3
  // Print the modified list
  println(mutableList)
  // Update an element
  mutableList(1) = 10
  // Print the updated list
  println(mutableList)

  // Convert the mutable list to an immutable list
  val immutableList = mutableList.toList
  // Print the immutable list
  println(immutableList)
  0
