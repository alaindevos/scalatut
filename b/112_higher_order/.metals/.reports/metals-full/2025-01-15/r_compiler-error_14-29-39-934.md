file://<WORKSPACE>/src/main/scala/alain/Fibo.scala
### scala.MatchError: TypeDef(B,TypeBoundsTree(EmptyTree,EmptyTree,EmptyTree)) (of class dotty.tools.dotc.ast.Trees$TypeDef)

occurred in the presentation compiler.

presentation compiler configuration:


action parameters:
offset: 234
uri: file://<WORKSPACE>/src/main/scala/alain/Fibo.scala
text:
```scala
package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f

t@@

def abs(n:Int):Int =
  if n<0 then -n
  else  n

def formatResult(name:String , n:Int , f : Int => Int):Unit =
  val msg1 = "The %s of %d is %d"
  val msg2 = msg1 format (name,n,f(n))
  msg2 |> println

@main private def main(args: String*): Int =
  var myArgs: Seq[String] = args
  println("Hello World")
  formatResult("Absolute value",-42,abs)
  0


```



#### Error stacktrace:

```
dotty.tools.pc.completions.KeywordsCompletions$.checkTemplateForNewParents$$anonfun$2(KeywordsCompletions.scala:218)
	scala.Option.map(Option.scala:242)
	dotty.tools.pc.completions.KeywordsCompletions$.checkTemplateForNewParents(KeywordsCompletions.scala:215)
	dotty.tools.pc.completions.KeywordsCompletions$.contribute(KeywordsCompletions.scala:44)
	dotty.tools.pc.completions.Completions.completions(Completions.scala:124)
	dotty.tools.pc.completions.CompletionProvider.completions(CompletionProvider.scala:90)
	dotty.tools.pc.ScalaPresentationCompiler.complete$$anonfun$1(ScalaPresentationCompiler.scala:146)
```
#### Short summary: 

scala.MatchError: TypeDef(B,TypeBoundsTree(EmptyTree,EmptyTree,EmptyTree)) (of class dotty.tools.dotc.ast.Trees$TypeDef)