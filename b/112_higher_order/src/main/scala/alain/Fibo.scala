package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f

type inttoint = Int => Int
type intinttoint = (Int,Int)=>Int

def abs(n:Int):Int =
  if n<0 then -n
  else  n

def sum(a: Int,b:Int): Int =
  a+b

def formatResult1(name:String , n:Int , f : inttoint):Unit =
  val msg1 = "The %s of %d is %d"
  val msg2 = msg1 format (name,n,f(n))
  msg2 |> println

def formatResult2(a: Int, b:Int,f: intinttoint): Unit =
  val msg1 = "The sum of %d %d is %d"
  val msg2 = msg1 format(a,b, f(a,b))
  msg2 |> println


@main private def main(args: String*): Int =
  var myArgs: Seq[String] = args
  println("Hello World")
  formatResult1("Absolute value",-42,abs)
  formatResult2(3,5,sum)
  0

