name := "myApplication"
version := "1"
lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "alain",
      scalaVersion := "3.3.4"
    )),
    name := "test"
  )

libraryDependencies ++=
  Seq(
//	"org.scalafx"            %% "scalafx"                  % "21.0.0-R32"    ,
//  "org.scalanlp"           %% "breeze"                   % "2.1.0"  ,
//  "org.scalanlp"           %% "breeze-viz"               % "2.1.0"  ,
//	"com.github.tminglei"    %% "slick-pg"                 % "0.22.2" ,
//	"com.github.tminglei"    %% "slick-pg_play-json"       % "0.22.2" ,
//	"org.creativescala"      %% "doodle"                   % "0.26.0" ,
//	"org.postgresql"          % "postgresql"               % "42.7.4" ,
//	"org.scala-lang.modules" %% "scala-parser-combinators" % "2.4.0"  ,
//	"org.scala-lang.modules" %% "scala-swing"              % "3.0.0"  ,
//	"org.typelevel"          %% "cats-core"                % "2.12.0" ,
//	"org.typelevel"          %% "cats-effect"              % "3.5.7"  ,
	"com.lihaoyi"            %% "fansi"                    % "0.5.0"  ,
	"org.scalatest"          %% "scalatest"                % "3.2.19" % Test ,
	"org.slf4j"               % "slf4j-nop"                % "2.0.16" ,
//	"org.scalameta"          %% "munit"                    % "0.7.29" % Test

	)
