package com.alain

object MyClass {
  var counter: Int = 0
}

class MyClass(var x: Int) {
  MyClass.counter = MyClass.counter + 1

}


object Main {
    var c: MyClass = MyClass(1)
    var d: MyClass = MyClass(2)
    var e: MyClass = MyClass(3)

  def main(args: Array[String]): Unit = println("Start")
    println(MyClass.counter)
    println("Stop")

}
