import scalafx.scene.control.*
import scalafx.scene.layout.HBox
import scalafx.event.ActionEvent
import scalafx.scene.layout.StackPane
import scalafx.scene.canvas.{Canvas, GraphicsContext}
import scalafx.Includes.*

import scalafx.application.JFXApp3
import scalafx.scene.Scene
import scalafx.scene.paint._
import scalafx.scene.paint.Color
import scalafx.scene.shape.{Circle, Rectangle}
import scala.math._

def intToColor(argb: Int): Color = {
  val alpha = (argb >> 24) & 0xFF
  val red = (argb >> 16) & 0xFF
  val green = (argb >> 8) & 0xFF
  val blue = argb & 0xFF

  Color.rgb(red, green, blue, alpha / 255.0)
}
var rl:List[Rectangle]=
  var x1=Rectangle(1, 1, 2, 2)
  var x2: List[Rectangle] = List(x1)
  for (a <- 1 to 350) {
    val y = ((sin(a / 30.0) + 1) * 100).toInt
    val z: Rectangle = Rectangle(a, y, 2, 2)
    val i=128*256*256*256+a*256*256+(256-a)*256+a/2
    val p=intToColor(i);
    z.setFill(p)
    x2 = z :: x2
  }
  x2

var aScene: Scene = new Scene(400, 400)
  {
    fill = Color.White
    content=rl
  }

object MyProgram extends JFXApp3 {
  override def start(): Unit = {
    stage = new JFXApp3.PrimaryStage {
      title = "MyProgram"
      scene = aScene
    }
  }
}
