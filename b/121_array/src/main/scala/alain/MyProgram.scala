package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f
object CubeCalculator:
  def cube(x: Int): Int = x * x * x

@main private def main(args: String*): Int =
  println("Hello World")
  val arr: Array[Array[Int]] = Array.ofDim[Int](4, 6)

  // Initialize the elements of the array
  for (i <- 0 until 4; j <- 0 until 6) {
    arr(i)(j) = i * j
  }

  // Access and print the elements of the array
  println("Printing elements of 2D array:")
  for (i <- 0 until 4) {
    printf("\n")
    for (j <- 0 until 6) {
      printf(" || %d %d %d || ", i, j, arr(i)(j))
    }
  }
  0
