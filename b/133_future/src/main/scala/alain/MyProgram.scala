package alain
/*
import scala.Array
import scala.annotation._
import scala.collection.immutable._
import collection.mutable.{Map=>MMap}
import scala.language.postfixOps
import scala.collection.immutable.List._
import scala.collection.immutable.Nil
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer
import collection.mutable.ArrayDeque
import scala.util.matching.Regex
import scala.util.matching.Regex.MatchIterator
import scala.util.Success
import scala.util.Try
import fansi.Color.* 
import fansi.Str
import sys.process._
import java.io.PrintWriter
import java.lang.ArithmeticException
import scalafx.scene.control.*
import scalafx.scene.layout.HBox
import scalafx.event.ActionEvent
import scalafx.scene.layout.StackPane
import scalafx.scene.canvas.{Canvas, GraphicsContext}
import scalafx.Includes.*
import scalafx.application.JFXApp3
import scalafx.scene.Scene
import scalafx.scene.paint._
import scalafx.scene.paint.Color
import scalafx.scene.shape.{Circle, Rectangle}
import scala.math._
*/
import scala.annotation.targetName
import scala.util.chaining.*
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}
import scala.io.StdIn.readLine
extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f

object CubeCalculator:
  def cube(x: Int): Int = x * x * x

object ForComprehensionDemo {
  def longRunningTask(i: Int): Future[Int] = Future {
    Thread.sleep(1000)
    i * 2
  }
}

@main private def main(args: String*): Int =
  println("Hello")
  val result: Future[Int] = for {
    a <- ForComprehensionDemo.longRunningTask(10)
    b <- ForComprehensionDemo.longRunningTask(20)
  } yield a + b
  result.onComplete {
    case Success(value) => println(s"Result: $value")
    case Failure(exception) => println(s"Error: ${exception.getMessage}")
  }
  val i=readLine()
  0
