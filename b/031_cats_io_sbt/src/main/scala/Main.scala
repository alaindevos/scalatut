object Main extends App:
	println("Hello, World!")
	import cats.effect.unsafe.implicits._
	import cats.effect._
	def printmessage():IO[Unit]=
		val p=IO.println("Hello CATS")
		p
	end printmessage
	val d:IO[Unit]=printmessage()
	d.unsafeRunSync()
end Main
