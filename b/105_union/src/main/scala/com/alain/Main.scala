package com.alain

object Main {
  type IntOrString = Int | String
  var x: IntOrString = 5

  def main(args: Array[String]): Unit = {

    def printIt(x: IntOrString): Unit = x match {
      case s: String => println(s)
      case i: Int => println(i.toString)
    }

    printIt(x)
    x = "Hallo"
    printIt(x)
  }
}
