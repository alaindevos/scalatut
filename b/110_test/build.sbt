lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "alain",
      scalaVersion := "3.4.0"
    )),
    name := "test"
  )

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.18" % Test
