package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*
import scala.annotation.tailrec

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f
object CubeCalculator:
  def cube(x: Int): Int = x * x * x


@main private def main(args: String*): Int =
  // Create an infinite lazy list of integers
  val intLazyList: LazyList[Int] = LazyList.from(1)
  // Take the first 10 integers from the lazy list
  val first10Ints = intLazyList.take(10)
  // Print the first 10 integers
  println(first10Ints.toList)
  // Create a lazy list of even numbers
  val evenLazyList: LazyList[Int] = LazyList.from(2, 2)
  // Take the first 5 even numbers from the lazy list
  val first5EvenNumbers = evenLazyList.take(5)
  // Print the first 5 even numbers
  println(first5EvenNumbers.toList)
  // Create a lazy list of Fibonacci numbers
  def fibs: LazyList[Int] = {
    def loop(a: Int, b: Int): LazyList[Int] = a #:: loop(b, a + b)
    loop(0, 1)
  }
  // Take the first 10 Fibonacci numbers from the lazy list
  val first10Fibs = fibs.take(10)
  // Print the first 10 Fibonacci numbers
  println(first10Fibs.toList)
  0