package com.alain

extension [A, B](a: A) infix def |>(f: A => B): B = f(a)

object Main :
  val fansiStr: fansi.Str = fansi.Color.Red("This should be red")

  def iszero(x: Int): Option[Int] =
    if x == 0 then
      None
    else
      Some(x)

  def printiszero(o: Option[Int]): Int = o match
    case None => 0
    case Some(x) => x

  def main(args: Array[String]): Unit =
    println(fansiStr)
    0 |> iszero |> printiszero |> println
    1 |> iszero |> printiszero |> println
    ()

