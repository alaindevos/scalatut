package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*

extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f
object CubeCalculator:
  def cube(x:Int):Int = x * x * x

class PreferredPrompt(val preference:String)

object JillsPrefs:
  given prompt: PreferredPrompt = PreferredPrompt("Your wish> ")

import alain.JillsPrefs.prompt

object Greeter:
  def greet(name:String)(using prompt:PreferredPrompt) =
    println(s"Welcome, $name. The system is ready.")
    println(prompt.preference)

@main private def main(args: String*): Int =
  println("Hello World")
  Greeter.greet("Jill")
  0
