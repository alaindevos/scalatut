package alain

import scala.Array
import scala.annotation.targetName
import scala.util.chaining.*


object CubeCalculator:
  def cube(x: Int): Int = x * x * x


extension [A](a: A) @targetName("|>") def |>[B](f: A => B): B = a pipe f

@main private def main(args: String*): Int = 
  var myArgs: Seq[String] = args
  println("Hello World")
  val s = "Chain"
  s pipe println
  s |> println
  0

